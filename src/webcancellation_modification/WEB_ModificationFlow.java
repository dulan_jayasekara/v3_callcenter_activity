package webcancellation_modification;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.controller.PG_Properties;

import webcom.model.WEB_ActivityDetails;
import webcom.types.WEB_TourOperatorType;

public class WEB_ModificationFlow {
	
	WEB_ActivityDetails activitydetails = new WEB_ActivityDetails();
	private WebDriver driver;
	private String ddd;
	
	public WEB_ModificationFlow(WEB_ActivityDetails activityDetails, WebDriver driver) {
		
		this.driver = driver;
		this.activitydetails = activityDetails;
		
	}
	
//	public ModificationFlow(String DDDDDDDDD , WebDriver driver) {
//		
//		this.driver = driver;
//		this.ddd = DDDDDDDDD;
//		
//	}
	public WEB_Modification modificationFlow(WebDriver Driver) throws InterruptedException{
		
		WEB_Modification modDetails = new WEB_Modification();
		
		try {
			
			driver.get(PG_Properties.getProperty("Baseurl")+ "/operations/reservation/ModificationCancellationInfoPage.do?module=operations");
			
			//driver.findElement(By.id("reservationId")).sendKeys(ddd);
			driver.findElement(By.id("reservationId")).sendKeys(activitydetails.getReservationNo());
			driver.findElement(By.xpath(".//*[@id='reservationId_lkup']")).click();
			driver.switchTo().defaultContent();
			driver.switchTo().frame("lookup");
			driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
			Thread.sleep(1000);
			driver.switchTo().defaultContent();
			
			//activitydetails.setUserType("Net_Cash");
			
			String reservationNo_2 = driver.findElement(By.xpath(".//*[@id='reservationno_activity']")).getText().replace(" ", "");
			modDetails.setModificationReservationNo(reservationNo_2);
			
			String modifyStatus = driver.findElement(By.xpath(".//*[@id='cancelationmodificaton_activity']/a")).getText();
			modDetails.setModificationStatus(modifyStatus);
			
			String modifyComment = driver.findElement(By.xpath(".//*[@id='cancelationendcommend_activity']")).getText();
			modDetails.setModificationComment(modifyComment);
			
			
			if(modifyStatus.equals("Yes")){
				
				driver.findElement(By.xpath(".//*[@id='cancelationmodificaton_activity']/a")).click();	
				Thread.sleep(1500);
				
				////////
				
				
				String customerName = driver.findElement(By.id("custname")).getAttribute("value");
				modDetails.setSummary_GuestLastname(customerName);
				
				String rseervationNo_3 = driver.findElement(By.id("reservationid")).getAttribute("value");
				modDetails.setSummary_ReservNo(rseervationNo_3);
				
				String summary_reservationNo = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[4]/tbody/tr/td/table[3]/tbody/tr/td/table[1]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[1]/td[2]/table/tbody/tr/td")).getText();
				modDetails.setSummary_reservationNo(summary_reservationNo);
				
				String summary_reservationDate = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[4]/tbody/tr/td/table[3]/tbody/tr/td/table[1]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]/table/tbody/tr/td")).getText();
				modDetails.setSummary_reservationDate(summary_reservationDate);
				
				String summary_ProgramName = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[4]/tbody/tr/td/table[3]/tbody/tr/td/table[3]/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[1]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[1]/td[2]/table/tbody/tr/td")).getText();
				modDetails.setSummary_ProgramName(summary_ProgramName);
				
				String summary_SupplierName = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[4]/tbody/tr/td/table[3]/tbody/tr/td/table[3]/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[1]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]/table/tbody/tr/td")).getText();
				modDetails.setSummary_SupplierName(summary_SupplierName);
				
				String summary_Status = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[4]/tbody/tr/td/table[3]/tbody/tr/td/table[3]/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[1]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[4]/td[2]/table/tbody/tr/td")).getText();
				modDetails.setSummary_Status(summary_Status);
				
				String summary_BookingStatus = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[4]/tbody/tr/td/table[3]/tbody/tr/td/table[3]/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[1]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[5]/td[2]/table/tbody/tr/td")).getText();
				modDetails.setSummary_BookingStatus(summary_BookingStatus);
				
				String summary_activityName_2 = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[4]/tbody/tr/td/table[3]/tbody/tr/td/table[3]/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[2]/tbody/tr[3]/td/table/tbody/tr[2]/td[1]/table/tbody/tr/td")).getText();
				modDetails.setSummary_activityName_2(summary_activityName_2);
				
				String summary_ratePlan = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[4]/tbody/tr/td/table[3]/tbody/tr/td/table[3]/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[2]/tbody/tr[3]/td/table/tbody/tr[2]/td[2]/table/tbody/tr/td")).getText();
				modDetails.setSummary_ratePlan(summary_ratePlan);
				
				String summary_QTY = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[4]/tbody/tr/td/table[3]/tbody/tr/td/table[3]/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[2]/tbody/tr[3]/td/table/tbody/tr[2]/td[5]/table/tbody/tr/td")).getText();
				modDetails.setSummary_QTY(summary_QTY);
				
				String summary_Date = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[4]/tbody/tr/td/table[3]/tbody/tr/td/table[3]/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[2]/tbody/tr[3]/td/table/tbody/tr[2]/td[3]/table/tbody/tr/td")).getText();
				modDetails.setSummary_Date(summary_Date);
				
				String summary_Session = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[4]/tbody/tr/td/table[3]/tbody/tr/td/table[3]/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[2]/tbody/tr[3]/td/table/tbody/tr[2]/td[4]/table/tbody/tr/td")).getText();
				modDetails.setSummary_Session(summary_Session);
				
				String summary_RateUSD = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[4]/tbody/tr/td/table[3]/tbody/tr/td/table[3]/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[2]/tbody/tr[3]/td/table/tbody/tr[2]/td[6]/table/tbody/tr/td")).getText().split("\\.")[0];
				modDetails.setSummary_RateUSD(summary_RateUSD);
				
				String summary_TotalValue = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[4]/tbody/tr/td/table[3]/tbody/tr/td/table[3]/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[2]/tbody/tr[3]/td/table/tbody/tr[2]/td[7]/table/tbody/tr/td")).getText().split("\\.")[0];
				modDetails.setSummary_TotalValue(summary_TotalValue);
				
				String summary_RateCurrency = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[4]/tbody/tr/td/table[3]/tbody/tr/td/table[3]/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[2]/tbody/tr[3]/td/table/tbody/tr[1]/td[6]/table/tbody/tr/td")).getText().substring(16, 19);
				modDetails.setSummary_RateCurrency(summary_RateCurrency);
				
				String summary_TotalValueCurrency = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[4]/tbody/tr/td/table[3]/tbody/tr/td/table[3]/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[2]/tbody/tr[3]/td/table/tbody/tr[1]/td[7]/table/tbody/tr/td")).getText().substring(8, 11);
				modDetails.setSummary_TotalValueCurrency(summary_TotalValueCurrency);
				
				
				String summary_ProgramTotalValue = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[4]/tbody/tr/td/table[3]/tbody/tr/td/table[3]/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[2]/tbody/tr[3]/td/table/tbody/tr[3]/td[2]/table/tbody/tr/td")).getText().split("\\.")[0];
				modDetails.setSummary_ProgramTotalValue(summary_ProgramTotalValue);
				
				String summary_ProgramTotalCost = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[4]/tbody/tr/td/table[3]/tbody/tr/td/table[3]/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[2]/tbody/tr[3]/td/table/tbody/tr[4]/td[2]/div/table/tbody/tr/td")).getText().split("\\.")[0];
				modDetails.setSummary_ProgramTotalCost(summary_ProgramTotalCost);
				
				String summary_CustomerNotes = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[4]/tbody/tr/td/table[3]/tbody/tr/td/table[3]/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[3]/tbody/tr[3]/td/table/tbody/tr[1]/td/table/tbody/tr/td[2]/table/tbody/tr/td")).getText();
				modDetails.setSummary_CustomerNotes(summary_CustomerNotes);
				
//				String summary_InternalNotes = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[4]/tbody/tr/td/table[3]/tbody/tr/td/table[3]/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[3]/tbody/tr[3]/td/table/tbody/tr[1]/td/table/tbody/tr/td[4]/table/tbody/tr/td")).getText();
//				modDetails.setSummary_InternalNotes(summary_InternalNotes);
//				
//				String summary_ProgramNotes = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[4]/tbody/tr/td/table[3]/tbody/tr/td/table[3]/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[3]/tbody/tr[3]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr/td")).getText();
//				modDetails.setSummary_ProgramNotes(summary_ProgramNotes);
				
				String summary_CusEmail = driver.findElement(By.id("programEmail[0]")).getAttribute("value");
				modDetails.setSummary_CusEmail(summary_CusEmail);
				
				if (activitydetails.getUserType() == WEB_TourOperatorType.WEB_DC) {
					
					String summary_Title = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[4]/tbody/tr/td/table[3]/tbody/tr/td/table[3]/tbody/tr[7]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[1]/td[2]")).getText().substring(1);
					modDetails.setSummary_Title(summary_Title);
					
					String summary_FName = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[4]/tbody/tr/td/table[3]/tbody/tr/td/table[3]/tbody/tr[7]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[1]/td[4]")).getText().substring(1);
					modDetails.setSummary_FName(summary_FName);
					
					String summary_LName = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[4]/tbody/tr/td/table[3]/tbody/tr/td/table[3]/tbody/tr[7]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[1]/td[6]")).getText().substring(1);
					modDetails.setSummary_LName(summary_LName);
					
					String summary_TP;
					String summary_address_2;
					
					try {
						summary_TP = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[4]/tbody/tr/td/table[3]/tbody/tr/td/table[3]/tbody/tr[7]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[4]/td[4]")).getText().substring(1);
						modDetails.setSummary_TP(summary_TP);
						
						summary_address_2 = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[4]/tbody/tr/td/table[3]/tbody/tr/td/table[3]/tbody/tr[7]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td[4]")).getText().substring(1);
						modDetails.setSummary_address_2(summary_address_2);
						
					} catch (Exception e1) {
						summary_TP = "Not Available";
						summary_address_2 = "Not Available";
						modDetails.setSummary_TP(summary_TP);
						modDetails.setSummary_address_2(summary_address_2);
					}
										
					String summary_Email = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[4]/tbody/tr/td/table[3]/tbody/tr/td/table[3]/tbody/tr[7]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td[6]")).getText().substring(1);
					modDetails.setSummary_Email(summary_Email);
					
					String summary_address = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[4]/tbody/tr/td/table[3]/tbody/tr/td/table[3]/tbody/tr[7]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td[2]")).getText().substring(1);
					modDetails.setSummary_address(summary_address);
					
					String summary_country = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[4]/tbody/tr/td/table[3]/tbody/tr/td/table[3]/tbody/tr[7]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[3]/td[4]")).getText().substring(1);
					modDetails.setSummary_country(summary_country);
					
					String summary_city = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[4]/tbody/tr/td/table[3]/tbody/tr/td/table[3]/tbody/tr[7]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[3]/td[6]")).getText().substring(1);
					modDetails.setSummary_city(summary_city);
					
					
					if (summary_country.equals("USA")) {
						
						String summary_state = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[4]/tbody/tr/td/table[3]/tbody/tr/td/table[3]/tbody/tr[7]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[4]/td[2]")).getText().substring(1);
						modDetails.setSummary_state(summary_state);				
					}
					
					if (summary_country.equals("Canada")) {
						
						String summary_state = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[4]/tbody/tr/td/table[3]/tbody/tr/td/table[3]/tbody/tr[7]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[4]/td[2]")).getText().substring(1);
						modDetails.setSummary_state(summary_state);			
					}
					
					if (summary_country.equals("Australia")) {
						
						String summary_state = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[4]/tbody/tr/td/table[3]/tbody/tr/td/table[3]/tbody/tr[7]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[4]/td[2]")).getText().substring(1);
						modDetails.setSummary_state(summary_state);			
					}
					
				}else{
					
					String summary_Title = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[4]/tbody/tr/td/table[3]/tbody/tr/td/table[3]/tbody/tr[10]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[1]/td[2]")).getText().substring(1);
					modDetails.setSummary_Title(summary_Title);
					
					String summary_FName = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[4]/tbody/tr/td/table[3]/tbody/tr/td/table[3]/tbody/tr[10]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[1]/td[4]")).getText().substring(1);
					modDetails.setSummary_FName(summary_FName);
					
					String summary_LName = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[4]/tbody/tr/td/table[3]/tbody/tr/td/table[3]/tbody/tr[10]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[1]/td[6]")).getText().substring(1);
					modDetails.setSummary_LName(summary_LName);
					
					String summary_TP;
					String summary_address_2;
					
					try {
						summary_TP = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[4]/tbody/tr/td/table[3]/tbody/tr/td/table[3]/tbody/tr[10]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td[2]")).getText().substring(1);
						modDetails.setSummary_TP(summary_TP);
						
						summary_address_2 = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[4]/tbody/tr/td/table[3]/tbody/tr/td/table[3]/tbody/tr[10]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td[4]")).getText().substring(1);
						modDetails.setSummary_address_2(summary_address_2);
						
					} catch (Exception e1) {
						summary_TP = "Not Available";
						summary_address_2 = "Not Available";
						modDetails.setSummary_TP(summary_TP);
						modDetails.setSummary_address_2(summary_address_2);
					}
										
					String summary_Email = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[4]/tbody/tr/td/table[3]/tbody/tr/td/table[3]/tbody/tr[10]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td[6]")).getText().substring(1);
					modDetails.setSummary_Email(summary_Email);
					
					String summary_address = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[4]/tbody/tr/td/table[3]/tbody/tr/td/table[3]/tbody/tr[10]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td[2]")).getText().substring(1);
					modDetails.setSummary_address(summary_address);
					
					String summary_country = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[4]/tbody/tr/td/table[3]/tbody/tr/td/table[3]/tbody/tr[10]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[3]/td[4]")).getText().substring(1);
					modDetails.setSummary_country(summary_country);
					
					String summary_city = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[4]/tbody/tr/td/table[3]/tbody/tr/td/table[3]/tbody/tr[10]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[3]/td[6]")).getText().substring(1);
					modDetails.setSummary_city(summary_city);
					
					
					if (summary_country.equals("USA")) {
						
						String summary_state = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[4]/tbody/tr/td/table[3]/tbody/tr/td/table[3]/tbody/tr[10]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[4]/td[2]")).getText().substring(1);
						modDetails.setSummary_state(summary_state);				
					}
					
					if (summary_country.equals("Canada")) {
						
						String summary_state = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[4]/tbody/tr/td/table[3]/tbody/tr/td/table[3]/tbody/tr[10]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[4]/td[2]")).getText().substring(1);
						modDetails.setSummary_state(summary_state);			
					}
					
					if (summary_country.equals("Australia")) {
						
						String summary_state = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[4]/tbody/tr/td/table[3]/tbody/tr/td/table[3]/tbody/tr[10]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[4]/td[2]")).getText().substring(1);
						modDetails.setSummary_state(summary_state);			
					}
					
				}
				
				
				
				
				driver.findElement(By.xpath(".//*[@id='ManagerOverride_lkinid']")).click();
				Thread.sleep(1000);
				
				////////////////////////////////////////////////////////////////////////////////////////////////
				
				WebElement contractType = driver.findElement(By.xpath("/html/body/table/tbody/tr[2]/td/div/div/form/table[3]/tbody/tr/td/table[6]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[3]/tbody/tr/td/input"));
				String idString = contractType.getAttribute("name");
				String id = idString.split("_")[1];
				
				String MO_guestLastName = driver.findElement(By.id("custname")).getAttribute("value");
				modDetails.setMO_guestLastName(MO_guestLastName);
				
				String MO_reservationNo_1 = driver.findElement(By.id("reservationid")).getAttribute("value");
				modDetails.setMO_reservationNo_1(MO_reservationNo_1);
				
				String MO_programName_1 = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[6]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[1]/tbody/tr/td/table[2]/tbody/tr[1]/td[2]")).getText().split(": ")[1];
				modDetails.setMO_programName_1(MO_programName_1);
				
				String MO_activityName_1 = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[6]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[1]/tbody/tr/td/table[2]/tbody/tr[3]/td/table/tbody/tr[2]/td[1]")).getText().substring(1);
				modDetails.setMO_activityName_1(MO_activityName_1);
			
				String MO_activityDate = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[6]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[1]/tbody/tr/td/table[2]/tbody/tr[3]/td/table/tbody/tr[2]/td[2]")).getText().substring(1);
				modDetails.setMO_activityDate(MO_activityDate);
				
				String MO_rate = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[6]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[1]/tbody/tr/td/table[2]/tbody/tr[3]/td/table/tbody/tr[2]/td[3]")).getText().substring(1);
				modDetails.setMO_rate(MO_rate);
				
				String MO_qty = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[6]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[1]/tbody/tr/td/table[2]/tbody/tr[3]/td/table/tbody/tr[2]/td[4]")).getText().substring(1);
				modDetails.setMO_qty(MO_qty);
				
				String MO_activityRate = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[6]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[1]/tbody/tr/td/table[2]/tbody/tr[3]/td/table/tbody/tr[2]/td[5]")).getText().split("\\.")[0].substring(1);
				modDetails.setMO_activityRate(MO_activityRate);
				
				String MO_activityRateCurrency = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[6]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[1]/tbody/tr/td/table[2]/tbody/tr[3]/td/table/tbody/tr[1]/td[5]/span")).getText().substring(16, 19);
				modDetails.setMO_activityRateCurrency(MO_activityRateCurrency);
				
				String MO_Total = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[6]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[1]/tbody/tr/td/table[2]/tbody/tr[3]/td/table/tbody/tr[2]/td[6]")).getText().split("\\.")[0].substring(1);
				modDetails.setMO_Total(MO_Total);
				
				String MO_TotalCurrency = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[6]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[1]/tbody/tr/td/table[2]/tbody/tr[3]/td/table/tbody/tr[1]/td[6]/span")).getText().substring(8, 11);
				modDetails.setMO_TotalCurrency(MO_TotalCurrency);
				
				String MO_TotalBookingValue = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[6]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[1]/tbody/tr/td/table[2]/tbody/tr[3]/td/table/tbody/tr[3]/td[2]")).getText().split("\\.")[0].substring(1);
				modDetails.setMO_TotalBookingValue(MO_TotalBookingValue);
				
				String MO_TotalCost;
				
				try {
					 MO_TotalCost = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[6]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[1]/tbody/tr/td/table[2]/tbody/tr[3]/td/table/tbody/tr[4]/td[2]")).getText().split("\\.")[0].substring(1);
					modDetails.setMO_TotalCost(MO_TotalCost);
				} catch (Exception e1) {
					MO_TotalCost = "Not Available";
					modDetails.setMO_TotalCost(MO_TotalCost);
				}
				
				
				
				
				String MO_ManagerActivityName_1 = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[6]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[3]/tbody/tr/td/table[1]/tbody/tr/td/table[2]/tbody/tr/td/div/table/tbody/tr[2]/td[1]")).getText().substring(1);
				modDetails.setMO_ManagerActivityName_1(MO_ManagerActivityName_1);
				
				String MO_ManagerActivityDate = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[6]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[3]/tbody/tr/td/table[1]/tbody/tr/td/table[2]/tbody/tr/td/div/table/tbody/tr[2]/td[2]")).getText().substring(1);
				modDetails.setMO_ManagerActivityDate(MO_ManagerActivityDate);
				
				String MO_ManagerRate = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[6]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[3]/tbody/tr/td/table[1]/tbody/tr/td/table[2]/tbody/tr/td/div/table/tbody/tr[2]/td[3]")).getText().substring(1);
				modDetails.setMO_ManagerRate(MO_ManagerRate);
				
				//
				

				String dailyRateCurrency = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[6]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[3]/tbody/tr/td/table[1]/tbody/tr/td/table[2]/tbody/tr/td/div/table/tbody/tr[1]/td[4]/span")).getText().substring(13, 16);
				modDetails.setMO_DailyRateCurrency(dailyRateCurrency);
				
//				String netRateCurrency = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[6]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[3]/tbody/tr/td/table[1]/tbody/tr/td/table[2]/tbody/tr/td/div/table/tbody/tr[1]/td[5]/span")).getText().substring(11, 14);
//				modDetails.setMO_NetRateCurrrency(netRateCurrency);
				
				//
				
				String MO_NewTotalBookingValue = driver.findElement(By.xpath(".//*[@id='totalSellRate_"+id+"']")).getText().split("\\.")[0];
				modDetails.setMO_NewTotalBookingValue(MO_NewTotalBookingValue);
				
				String MO_NewTotalBookingCost = driver.findElement(By.xpath(".//*[@id='totalNetRate_"+id+"']")).getText().split("\\.")[0];
				modDetails.setMO_NewTotalBookingCost(MO_NewTotalBookingCost);
				
				
				driver.findElement(By.xpath(".//*[@id='RemoveActivity_lkinid']")).click();
				Thread.sleep(1000);
				
				
				///////////////////////////////////////////////////////////////////////////////////
				
				String RA_guestLastName = driver.findElement(By.id("custname")).getAttribute("value");
				modDetails.setRA_guestLastName(RA_guestLastName);
				
				String RA_reservationNo_1 = driver.findElement(By.id("reservationid")).getAttribute("value");
				modDetails.setRA_reservationNo_1(RA_reservationNo_1);
				
				String RA_programName_1 = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[3]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[1]/td[2]")).getText().split(": ")[1];
				modDetails.setRA_programName_1(RA_programName_1);
					
				String RA_activityName_1 = driver.findElement(By.xpath(".//*[@id='"+id+"']/tbody/tr[2]/td[2]")).getText().substring(1);
				modDetails.setRA_activityName_1(RA_activityName_1);
				
				String RA_activityDate = driver.findElement(By.xpath(".//*[@id='"+id+"']/tbody/tr[2]/td[3]")).getText().substring(1);
				modDetails.setRA_activityDate(RA_activityDate);
				
				String RA_session = driver.findElement(By.xpath(".//*[@id='"+id+"']/tbody/tr[2]/td[4]")).getText().substring(1);
				modDetails.setRA_session(RA_session);
				
				String RA_rate = driver.findElement(By.xpath(".//*[@id='"+id+"']/tbody/tr[2]/td[5]")).getText().substring(1);
				modDetails.setRA_rate(RA_rate);
				
				String RA_qty = driver.findElement(By.xpath(".//*[@id='"+id+"']/tbody/tr[2]/td[6]")).getText().substring(1);
				modDetails.setRA_qty(RA_qty);
				
				
				String RA_activityRate = driver.findElement(By.xpath(".//*[@id='"+id+"']/tbody/tr[2]/td[7]")).getText().split("\\.")[0].substring(1);
				modDetails.setRA_activityRate(RA_activityRate);
				
				String RA_activityRateCurrency = driver.findElement(By.xpath(".//*[@id='"+id+"']/tbody/tr[1]/td[7]/span")).getText().substring(16, 19);
				modDetails.setRA_activityRateCurrency(RA_activityRateCurrency);
				
				String RA_Total = driver.findElement(By.xpath(".//*[@id='"+id+"']/tbody/tr[2]/td[8]")).getText().split("\\.")[0].substring(1);
				modDetails.setRA_Total(RA_Total);
				
				String RA_TotalCurrency = driver.findElement(By.xpath(".//*[@id='"+id+"']/tbody/tr[1]/td[8]/span")).getText().substring(8, 11);
				modDetails.setRA_TotalCurrency(RA_TotalCurrency);
				
				String RA_TotalBookingValue = driver.findElement(By.xpath(".//*[@id='"+id+"']/tbody/tr[3]/td[2]")).getText().split("\\.")[0].substring(1);
				modDetails.setRA_TotalBookingValue(RA_TotalBookingValue);
				
				String RA_TotalCost;
				try {
					RA_TotalCost = driver.findElement(By.xpath(".//*[@id='"+id+"']/tbody/tr[4]/td[2]")).getText().split("\\.")[0].substring(1);
					modDetails.setRA_TotalCost(RA_TotalCost);
				} catch (Exception e1) {
					RA_TotalCost = "Not Available";
					modDetails.setRA_TotalCost(RA_TotalCost);
				}
				
				
				driver.findElement(By.xpath(".//*[@id='AddActivity_lkinid']")).click();
				Thread.sleep(1000);
				
				/////////////////////////////////////////////////////////////////////////////////////
				
				String AA_guestLastName = driver.findElement(By.id("custname")).getAttribute("value");
				modDetails.setAA_guestLastName(AA_guestLastName);
				
				String AA_reservationNo_1 = driver.findElement(By.id("reservationid")).getAttribute("value");
				modDetails.setAA_reservationNo_1(AA_reservationNo_1);
				
				
				
				Select Page_Date = new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("searchdate_Day_ID")));
				WebElement Page_DateEle = Page_Date.getFirstSelectedOption();	
				String cDate = Page_DateEle.getText();
				
				Select Page_Maonth = new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("searchdate_Month_ID")));
				WebElement Page_MaonthEle = Page_Maonth.getFirstSelectedOption();	
				String cMonth = Page_MaonthEle.getText();
				
				Select Page_Year = new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("searchdate_Year_ID")));
				WebElement Page_YearEle = Page_Year.getFirstSelectedOption();	
				String cYear = Page_YearEle.getText();
				
				String AA_ReservationDate = ""+cDate+"-"+cMonth+"-"+cYear+"";
				modDetails.setAA_ReservationDate(AA_ReservationDate);
				
				
				
				String AA_programName_1 = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[6]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[1]/tbody/tr/td/table[2]/tbody/tr[1]/td[2]")).getText().split(": ")[1];
				modDetails.setAA_programName_1(AA_programName_1);
				
				String AA_activityName_1 = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[6]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[1]/tbody/tr/td/table[2]/tbody/tr[3]/td/table/tbody/tr[2]/td[1]")).getText().substring(1);
				modDetails.setAA_activityName_1(AA_activityName_1);
				
				String AA_activityDate = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[6]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[1]/tbody/tr/td/table[2]/tbody/tr[3]/td/table/tbody/tr[2]/td[2]")).getText().substring(1);
				modDetails.setAA_activityDate(AA_activityDate);
				
				String AA_session = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[6]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[1]/tbody/tr/td/table[2]/tbody/tr[3]/td/table/tbody/tr[2]/td[3]")).getText().substring(1);
				modDetails.setAA_session(AA_session);
				
				String AA_rate = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[6]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[1]/tbody/tr/td/table[2]/tbody/tr[3]/td/table/tbody/tr[2]/td[4]")).getText().substring(1);
				modDetails.setAA_rate(AA_rate);
				
				String AA_qty = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[6]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[1]/tbody/tr/td/table[2]/tbody/tr[3]/td/table/tbody/tr[2]/td[5]")).getText().substring(1);
				modDetails.setAA_qty(AA_qty);
				
				String AA_activityRate = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[6]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[1]/tbody/tr/td/table[2]/tbody/tr[3]/td/table/tbody/tr[2]/td[6]")).getText().split("\\.")[0].substring(1);
				modDetails.setAA_activityRate(AA_activityRate);
				
				String AA_activityRateCurrency = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[6]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[1]/tbody/tr/td/table[2]/tbody/tr[3]/td/table/tbody/tr[1]/td[6]/span")).getText().substring(16, 19);
				modDetails.setAA_activityRateCurrency(AA_activityRateCurrency);
				
				String AA_Total = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[6]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[1]/tbody/tr/td/table[2]/tbody/tr[3]/td/table/tbody/tr[2]/td[7]")).getText().split("\\.")[0].substring(1);
				modDetails.setAA_Total(AA_Total);
				
				String AA_TotalCurrency = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[6]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[1]/tbody/tr/td/table[2]/tbody/tr[3]/td/table/tbody/tr[1]/td[7]/span")).getText().substring(8, 11);
				modDetails.setAA_TotalCurrency(AA_TotalCurrency);
				
				
				
				String AA_TotalBookingValue = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[6]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[1]/tbody/tr/td/table[2]/tbody/tr[3]/td/table/tbody/tr[3]/td[2]")).getText().split("\\.")[0].substring(1);
				modDetails.setAA_TotalBookingValue(AA_TotalBookingValue);
				
				String AA_TotalCost;
				try {
					AA_TotalCost = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[6]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[1]/tbody/tr/td/table[2]/tbody/tr[3]/td/table/tbody/tr[4]/td[2]")).getText().split("\\.")[0].substring(1);
					modDetails.setAA_TotalCost(AA_TotalCost);
				} catch (Exception e1) {
					AA_TotalCost = "Not Available";
					modDetails.setAA_TotalCost(AA_TotalCost);
				}
				
				
				driver.findElement(By.xpath(".//*[@id='ActivityDate/Qty_lkinid']")).click();
				Thread.sleep(1000);
				
				
				/////////////////////////////////////////////////////////////////////////////////////
				
				String ADQ_guestLastName = driver.findElement(By.id("custname")).getAttribute("value");
				modDetails.setADQ_guestLastName(ADQ_guestLastName);
				
				String ADQ_reservationNo_1 = driver.findElement(By.id("reservationid")).getAttribute("value");
				modDetails.setADQ_reservationNo_1(ADQ_reservationNo_1);
				
				
				Select Page_DateADQ = new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("searchdate_Day_ID")));
				WebElement Page_DateEleADQ = Page_DateADQ.getFirstSelectedOption();	
				String cDateADQ = Page_DateEleADQ.getText();
				
				Select Page_MaonthADQ = new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("searchdate_Month_ID")));
				WebElement Page_MaonthEleADQ = Page_MaonthADQ.getFirstSelectedOption();	
				String cMonthADQ = Page_MaonthEleADQ.getText();
				
				Select Page_YearADQ = new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("searchdate_Year_ID")));
				WebElement Page_YearEleADQ = Page_YearADQ.getFirstSelectedOption();	
				String cYearADQ = Page_YearEleADQ.getText();
				
				String ADQ_ReservationDate = ""+cDateADQ+"-"+cMonthADQ+"-"+cYearADQ+"";
				modDetails.setADQ_ReservationDate(ADQ_ReservationDate);
				
				
				
				String ADQ_programName_1 = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[6]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[1]/tbody/tr/td/table[2]/tbody/tr[1]/td[2]")).getText().split(": ")[1];
				modDetails.setADQ_programName_1(ADQ_programName_1);
				
				String ADQ_SupplierName;
				try {
					ADQ_SupplierName = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[6]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[1]/tbody/tr/td/table[2]/tbody/tr[2]/td[2]")).getText().split(": ")[1];
					modDetails.setADQ_SupplierName(ADQ_SupplierName);
				} catch (Exception e) {
					ADQ_SupplierName = "Not Available";
					modDetails.setADQ_SupplierName(ADQ_SupplierName);
				}
								
				String ADQ_activityName_1 = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[6]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[2]/tbody/tr[1]/td/table/tbody/tr[2]/td[2]")).getText().substring(1);
				modDetails.setADQ_activityName_1(ADQ_activityName_1);
				
				String ADQ_activityDate = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[6]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[2]/tbody/tr[1]/td/table/tbody/tr[2]/td[3]")).getText().substring(1);
				modDetails.setADQ_activityDate(ADQ_activityDate);
				
				String ADQ_session = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[6]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[2]/tbody/tr[1]/td/table/tbody/tr[2]/td[4]")).getText().substring(1);
				modDetails.setADQ_session(ADQ_session);
				
				String ADQ_rate = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[6]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[2]/tbody/tr[1]/td/table/tbody/tr[2]/td[5]")).getText().substring(1);
				modDetails.setADQ_rate(ADQ_rate);
				
				String ADQ_qty = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[6]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[2]/tbody/tr[1]/td/table/tbody/tr[2]/td[6]")).getText().substring(1);
				modDetails.setADQ_qty(ADQ_qty);
				
				String ADQ_activityRate = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[6]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[2]/tbody/tr[1]/td/table/tbody/tr[2]/td[7]")).getText().split("\\.")[0].substring(1);
				modDetails.setADQ_activityRate(ADQ_activityRate);
				
				String ADQ_activityRateCurrency = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[6]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[2]/tbody/tr[1]/td/table/tbody/tr[1]/td[7]/span")).getText().substring(16, 19);
				modDetails.setADQ_activityRateCurrency(ADQ_activityRateCurrency);
				
				String ADQ_Total = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[6]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[2]/tbody/tr[1]/td/table/tbody/tr[2]/td[8]")).getText().split("\\.")[0].substring(1);
				modDetails.setADQ_Total(ADQ_Total);
				
				String ADQ_TotalCurrency = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[6]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[2]/tbody/tr[1]/td/table/tbody/tr[1]/td[8]/span")).getText().substring(8, 11);
				modDetails.setADQ_TotalCurrency(ADQ_TotalCurrency);
				
				String ADQ_TotalBookingValue = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[6]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[2]/tbody/tr[1]/td/table/tbody/tr[3]/td[2]")).getText().split("\\.")[0].substring(1);
				modDetails.setADQ_TotalBookingValue(ADQ_TotalBookingValue);
				
				String ADQ_TotalCost;
				try {
					ADQ_TotalCost = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[6]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table[2]/tbody/tr[1]/td/table/tbody/tr[4]/td[2]")).getText().split("\\.")[0].substring(1);
					modDetails.setADQ_TotalCost(ADQ_TotalCost);
				} catch (Exception e) {
					ADQ_TotalCost = "Not Available";
					modDetails.setADQ_TotalCost(ADQ_TotalCost);
				}
				
				
				driver.findElement(By.xpath(".//*[@id='GuestDetails_lkinid']")).click();
				Thread.sleep(1000);
				
				/////////////////////////////////////////////////////////////////////////////////////
				
				String GD_guestLastName = driver.findElement(By.id("custname")).getAttribute("value");
				modDetails.setGD_guestLastName(GD_guestLastName);
				
				String GD_reservationNo_1 = driver.findElement(By.id("reservationid")).getAttribute("value");
				modDetails.setGD_reservationNo_1(GD_reservationNo_1);
				
				Select GD_Title = new org.openqa.selenium.support.ui.Select(driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[5]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[1]/td[2]/select")));
				WebElement GD_TitleELE = GD_Title.getFirstSelectedOption();	
				String GD_Title2 = GD_TitleELE.getText();
				modDetails.setGD_Title(GD_Title2);
				
				
				
				String GD_FName = driver.findElement(By.id("cusFirstName")).getAttribute("value");
				modDetails.setGD_FName(GD_FName);
				
				String GD_LName = driver.findElement(By.id("cusLastName")).getAttribute("value");
				modDetails.setGD_LName(GD_LName);
				
				String GD_TP = driver.findElement(By.id("cusPhone")).getAttribute("value");
				modDetails.setGD_TP(GD_TP);
				
				String GD_Email = driver.findElement(By.id("cusEmail")).getAttribute("value");
				modDetails.setGD_Email(GD_Email);
				
				String GD_address = driver.findElement(By.id("cusAddress1")).getAttribute("value");
				modDetails.setGD_address(GD_address);
				
				String GD_address_2 = driver.findElement(By.id("cusAddress2")).getAttribute("value");
				modDetails.setGD_address_2(GD_address_2);
				
				Select GD_Country = new org.openqa.selenium.support.ui.Select(driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[5]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[3]/td[4]/select")));
				WebElement GD_CountryELE = GD_Country.getFirstSelectedOption();	
				String GD_Country2 = GD_CountryELE.getText();
				modDetails.setGD_country(GD_Country2);
				
				String GD_city = driver.findElement(By.id("cusCity")).getAttribute("value");
				modDetails.setGD_city(GD_city);
				
				
				if (GD_Country2.equals("USA")) {
					
					String GD_state = driver.findElement(By.id("cusState")).getAttribute("value");
					modDetails.setGD_state(GD_state);		
				}
				
				if (GD_Country2.equals("Canada")) {
					
					String GD_state = driver.findElement(By.id("cusState")).getAttribute("value");
					modDetails.setGD_state(GD_state);
				}
				
				if (GD_Country2.equals("Australia")) {
					
					String GD_state = driver.findElement(By.id("cusState")).getAttribute("value");
					modDetails.setGD_state(GD_state);	
				}
			}
			
			
			
		} catch (Exception e) {
			modDetails.setModReportLoaded(false);
		}
		
		return modDetails;
	}
	

}
