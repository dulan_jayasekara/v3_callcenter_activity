package cancellation_modification;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.controller.PG_Properties;
import com.model.ActivityDetails;

public class CancellationFlow {
	
	private WebDriver driver;
	ActivityDetails activitydetails 	= new ActivityDetails();
	ArrayList<String> policyList = new ArrayList<String>();
	
	public CancellationFlow(ActivityDetails activityDetails, WebDriver driver){
		
		this.driver = driver;
		this.activitydetails = activityDetails;
		
	}
	
	public Cancellation cancellationFlow(WebDriver Driver) throws InterruptedException{
		
		Cancellation cancellationDetails = new Cancellation();
		WebDriverWait wait = new WebDriverWait(driver, 90);	
		
		try{
		
			driver.get(PG_Properties.getProperty("Baseurl")+ "/operations/reservation/ModificationCancellationInfoPage.do?module=operations");
			
			driver.findElement(By.id("reservationId")).sendKeys(activitydetails.getReservationNo());
			driver.findElement(By.xpath(".//*[@id='reservationId_lkup']")).click();
			driver.switchTo().defaultContent();
			driver.switchTo().frame("lookup");
			Thread.sleep(1000);
			driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
			Thread.sleep(1000);
			driver.switchTo().defaultContent();
			
			String reservationNo_2 = driver.findElement(By.xpath(".//*[@id='reservationno_activity']")).getText().replace(" ", "");
			cancellationDetails.setReservationNo_1(reservationNo_2);
			
			String cancellationStatus = driver.findElement(By.xpath(".//*[@id='cancellationload_activity']/a")).getText();
			cancellationDetails.setCancellationStatus(cancellationStatus);
			
			String cancellationComment = driver.findElement(By.xpath("html/body/table/tbody/tr[2]/td/form/table/tbody/tr[3]/td/center/table[2]/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[3]/td[4]")).getText();
			cancellationDetails.setCancellationComment(cancellationComment);
			
			if(cancellationStatus.equals("Yes")){
				
				driver.findElement(By.xpath(".//*[@id='cancellationload_activity']/a")).click();
				Thread.sleep(1000);
				
				String customerName = driver.findElement(By.id("custname")).getAttribute("value");
				cancellationDetails.setCustomerName(customerName);
				
				String rseervationNo_3 = driver.findElement(By.id("reservationid")).getAttribute("value");
				cancellationDetails.setRseervationNo_3(rseervationNo_3);
				
				
				Boolean chargeOnline_Y = driver.findElement(By.id("element_td_chargeOnline_Y")).isEnabled();

				   if(chargeOnline_Y.booleanValue()==true){
					   cancellationDetails.setPaymentType("Online");
				   }
				   else{
					   cancellationDetails.setPaymentType("Offline");
				   }
				
				 
				
				String programName_1 = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[8]/tbody/tr[1]/td/table[1]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[1]/td[2]")).getText().split(": ")[1];
				cancellationDetails.setProgramName_1(programName_1);
					
				String activityName_1 = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[8]/tbody/tr[1]/td/table[1]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td[2]")).getText().split(": ")[1];
				cancellationDetails.setActivityName_1(activityName_1);
					
				String reservationDate = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[8]/tbody/tr[1]/td/table[1]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[1]/td[4]")).getText().split(": ")[1].split(" ")[0];
				cancellationDetails.setReservationDate(reservationDate);
					
				String activity_Session = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[8]/tbody/tr[1]/td/table[1]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td[4]")).getText().split(": ")[1];
				cancellationDetails.setActivity_Session(activity_Session);
				
//				driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[9]/tbody/tr[1]/td/table[1]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[3]/td/a/u")).click();			
				
				String programName_2 = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[8]/tbody/tr[1]/td/table[6]/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td[1]")).getText().substring(1);
				cancellationDetails.setProgramName_2(programName_2);
				
				String activityName_2 = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[8]/tbody/tr[1]/td/table[6]/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[3]/td[2]")).getText().substring(1);
				cancellationDetails.setActivityName_2(activityName_2);
				
				String activity_ratePlan = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[8]/tbody/tr[1]/td/table[6]/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[3]/td[3]")).getText().substring(1);
				cancellationDetails.setActivity_ratePlan(activity_ratePlan);
				
				String activity_QTY = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[8]/tbody/tr[1]/td/table[6]/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[3]/td[4]")).getText().replace(" ", "");
				cancellationDetails.setActivity_QTY(activity_QTY);
				
				
				String activity_RateUSD = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[8]/tbody/tr[1]/td/table[6]/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[3]/td[5]")).getText().split("\\.")[0];
				cancellationDetails.setActivity_RateUSD(activity_RateUSD);
				
				String activity_TotalValue = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[8]/tbody/tr[1]/td/table[6]/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[3]/td[6]")).getText().split("\\.")[0].replace(" ", "");
				cancellationDetails.setActivity_TotalValue(activity_TotalValue);
				
				String activity_RateCurrency = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[8]/tbody/tr[1]/td/table[6]/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[1]/td[5]/span")).getText().substring(16, 19);
				cancellationDetails.setActivity_RateCurrency(activity_RateCurrency);
				
				String activity_TotalValueCurrency = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[8]/tbody/tr[1]/td/table[6]/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[1]/td[6]/span")).getText().substring(8, 11);
				cancellationDetails.setActivity_TotalValueCurrency(activity_TotalValueCurrency);
				
				
				
				String CancellationCharge_program = driver.findElement(By.id("procancfeearr[0]")).getAttribute("value");
				cancellationDetails.setCancellationCharge_program(CancellationCharge_program);
				
				String CancellationCharge_customer = driver.findElement(By.id("customerCancellationFeeArr[0]")).getAttribute("value");
				cancellationDetails.setCancellationCharge_customer(CancellationCharge_customer);
				
				String programCancellationNo = driver.findElement(By.id("cancellationnoarr[0]")).getAttribute("value");
				cancellationDetails.setProgramCancellationNo(programCancellationNo);
				
				String customer_Mail = driver.findElement(By.id("programEmail[0]")).getAttribute("value");
				cancellationDetails.setCustomer_Mail(customer_Mail);
				
				
				String total_ProgramCancellationCharge = driver.findElement(By.id("totalProgramCancelfee")).getAttribute("value");
				cancellationDetails.setTotal_ProgramCancellationCharge(total_ProgramCancellationCharge);
				
				String total_CustomerCancellationCharge = driver.findElement(By.id("totalCustomerCancelfee")).getAttribute("value");
				cancellationDetails.setTotal_CustomerCancellationCharge(total_CustomerCancellationCharge);
				
				String additional_cancellationCharge = driver.findElement(By.id("addicancfeearr[0]")).getAttribute("value");
				cancellationDetails.setAdditional_cancellationCharge(additional_cancellationCharge);
				
				String GSTCharge = driver.findElement(By.id("gstchargearr[0]")).getAttribute("value");
				cancellationDetails.setGSTCharge(GSTCharge);
				
				String TotalCharge = driver.findElement(By.id("totalcancfeearr[0]")).getAttribute("value");
				cancellationDetails.setTotalCharge(TotalCharge);
				
				
				String cancellationReason = driver.findElement(By.id("cancellationreasonarr[0]")).getText();
				cancellationDetails.setCancellationReason(cancellationReason);
				
				String internalNotes = driver.findElement(By.xpath(".//*[@id='txt_Int_Notesarr[0]']")).getAttribute("value");
				cancellationDetails.setInternalNotes(internalNotes);
				
				try {
					Thread.sleep(1000);
					driver.findElement(By.xpath(".//*[@id='saveButId']")).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")));
					driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
					Thread.sleep(1000);
					
				} catch (Exception e) {
					
					e.printStackTrace();
				}
				
				
			}
			
		}catch(Exception e){
			cancellationDetails.setCancelReportLoaded(false);
			
		}
		
		
	
		return cancellationDetails;
	}

}
