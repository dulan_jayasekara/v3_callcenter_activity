package cancellation_modification;

public class Modification {
	
	private String modificationReservationNo;
	private String modificationStatus;
	private String modificationComment;
	private boolean isModReportLoaded = true; 
	private String summary_ReservNo;
	private String summary_GuestLastname;
	private String summary_reservationNo;
	private String summary_reservationDate;
	private String summary_ProgramName;
	private String summary_SupplierName;
	private String summary_Status;
	private String summary_BookingStatus;
	private String summary_activityName_2;
	private String summary_ratePlan;
	private String summary_QTY;
	private String summary_Date;
	private String summary_Session;
	private String summary_RateUSD;
	private String summary_TotalValue;
	private String summary_RateCurrency;
	private String summary_TotalValueCurrency;
	private String summary_ProgramTotalValue;
	private String summary_ProgramTotalCost;
	private String summary_CustomerNotes;
	private String summary_InternalNotes;
	private String summary_ProgramNotes;
	private String summary_CusEmail;
	private String summary_Title;
	private String summary_FName;
	private String summary_LName;
	private String summary_TP;
	private String summary_Email;
	private String summary_address;
	private String summary_address_2;
	private String summary_country;
	private String summary_city;
	private String summary_state;
	
	
	private String MO_guestLastName;
	private String MO_reservationNo_1;
	private String MO_programName_1;
	private String MO_activityName_1;
	private String MO_activityDate;
	private String MO_rate;
	private String MO_qty;
	private String MO_activityRate;
	private String MO_activityRateCurrency;
	private String MO_Total;
	private String MO_TotalCurrency;
	private String MO_TotalBookingValue;
	private String MO_TotalCost;
	private String MO_ManagerActivityName_1;
	private String MO_ManagerActivityDate;
	private String MO_ManagerRate;
	private String MO_DailyRate;
	private String MO_DailyRateCurrency;
	private String MO_NetRate;
	private String MO_NetRateCurrrency;
	private String MO_NewTotalBookingValue;
	private String MO_NewTotalBookingCost;
	
	private String RA_guestLastName;
	private String RA_reservationNo_1;
	private String RA_programName_1;
	private String RA_activityName_1;
	private String RA_activityDate;
	private String RA_session;
	private String RA_rate;
	private String RA_qty;
	private String RA_activityRate;
	private String RA_activityRateCurrency;
	private String RA_Total;
	private String RA_TotalCurrency;
	private String RA_TotalBookingValue;
	private String RA_TotalCost;
	
	private String AA_guestLastName;
	private String AA_reservationNo_1;
	private String AA_ReservationDate;
	private String AA_programName_1;
	private String AA_activityName_1;
	private String AA_activityDate;
	private String AA_session;
	private String AA_rate;
	private String AA_qty;
	private String AA_activityRate;
	private String AA_activityRateCurrency;
	private String AA_Total;
	private String AA_TotalCurrency;
	private String AA_TotalBookingValue;
	private String AA_TotalCost;
	
	
	private String ADQ_guestLastName;
	private String ADQ_reservationNo_1;
	private String ADQ_ReservationDate;
	private String ADQ_programName_1;
	private String ADQ_SupplierName;
	private String ADQ_activityName_1;
	private String ADQ_activityDate;
	private String ADQ_session;
	private String ADQ_rate;
	private String ADQ_qty;
	private String ADQ_activityRate;
	private String ADQ_activityRateCurrency;
	private String ADQ_Total;
	private String ADQ_TotalCurrency;
	private String ADQ_TotalBookingValue;
	private String ADQ_TotalCost;
	
	private String GD_guestLastName;
	private String GD_reservationNo_1;
	private String GD_Title;
	private String GD_FName;
	private String GD_LName;
	private String GD_TP;
	private String GD_Email;
	private String GD_address;
	private String GD_address_2;
	private String GD_country;
	private String GD_city;
	private String GD_state;
	
	
	public boolean isModReportLoaded() {
		return isModReportLoaded;
	}
	public void setModReportLoaded(boolean isModReportLoaded) {
		this.isModReportLoaded = isModReportLoaded;
	}
	public String getMO_DailyRate() {
		return MO_DailyRate;
	}
	public void setMO_DailyRate(String mO_DailyRate) {
		MO_DailyRate = mO_DailyRate;
	}
	public String getMO_DailyRateCurrency() {
		return MO_DailyRateCurrency;
	}
	public void setMO_DailyRateCurrency(String mO_DailyRateCurrency) {
		MO_DailyRateCurrency = mO_DailyRateCurrency;
	}
	public String getMO_NetRate() {
		return MO_NetRate;
	}
	public void setMO_NetRate(String mO_NetRate) {
		MO_NetRate = mO_NetRate;
	}
	public String getMO_NetRateCurrrency() {
		return MO_NetRateCurrrency;
	}
	public void setMO_NetRateCurrrency(String mO_NetRateCurrrency) {
		MO_NetRateCurrrency = mO_NetRateCurrrency;
	}
	public String getModificationReservationNo() {
		return modificationReservationNo;
	}
	public void setModificationReservationNo(String modificationReservationNo) {
		this.modificationReservationNo = modificationReservationNo;
	}
	public String getModificationStatus() {
		return modificationStatus;
	}
	public void setModificationStatus(String modificationStatus) {
		this.modificationStatus = modificationStatus;
	}
	public String getModificationComment() {
		return modificationComment;
	}
	public void setModificationComment(String modificationComment) {
		this.modificationComment = modificationComment;
	}
	public String getSummary_ReservNo() {
		return summary_ReservNo;
	}
	public void setSummary_ReservNo(String summary_ReservNo) {
		this.summary_ReservNo = summary_ReservNo;
	}
	public String getSummary_GuestLastname() {
		return summary_GuestLastname;
	}
	public void setSummary_GuestLastname(String summary_GuestLastname) {
		this.summary_GuestLastname = summary_GuestLastname;
	}
	public String getSummary_reservationNo() {
		return summary_reservationNo;
	}
	public void setSummary_reservationNo(String summary_reservationNo) {
		this.summary_reservationNo = summary_reservationNo;
	}
	public String getSummary_reservationDate() {
		return summary_reservationDate;
	}
	public void setSummary_reservationDate(String summary_reservationDate) {
		this.summary_reservationDate = summary_reservationDate;
	}
	public String getSummary_ProgramName() {
		return summary_ProgramName;
	}
	public void setSummary_ProgramName(String summary_ProgramName) {
		this.summary_ProgramName = summary_ProgramName;
	}
	public String getSummary_SupplierName() {
		return summary_SupplierName;
	}
	public void setSummary_SupplierName(String summary_SupplierName) {
		this.summary_SupplierName = summary_SupplierName;
	}
	public String getSummary_Status() {
		return summary_Status;
	}
	public void setSummary_Status(String summary_Status) {
		this.summary_Status = summary_Status;
	}
	public String getSummary_BookingStatus() {
		return summary_BookingStatus;
	}
	public void setSummary_BookingStatus(String summary_BookingStatus) {
		this.summary_BookingStatus = summary_BookingStatus;
	}
	public String getSummary_activityName_2() {
		return summary_activityName_2;
	}
	public void setSummary_activityName_2(String summary_activityName_2) {
		this.summary_activityName_2 = summary_activityName_2;
	}
	public String getSummary_ratePlan() {
		return summary_ratePlan;
	}
	public void setSummary_ratePlan(String summary_ratePlan) {
		this.summary_ratePlan = summary_ratePlan;
	}
	public String getSummary_QTY() {
		return summary_QTY;
	}
	public void setSummary_QTY(String summary_QTY) {
		this.summary_QTY = summary_QTY;
	}
	public String getSummary_Date() {
		return summary_Date;
	}
	public void setSummary_Date(String summary_Date) {
		this.summary_Date = summary_Date;
	}
	public String getSummary_Session() {
		return summary_Session;
	}
	public void setSummary_Session(String summary_Session) {
		this.summary_Session = summary_Session;
	}
	public String getSummary_RateUSD() {
		return summary_RateUSD;
	}
	public void setSummary_RateUSD(String summary_RateUSD) {
		this.summary_RateUSD = summary_RateUSD;
	}
	public String getSummary_TotalValue() {
		return summary_TotalValue;
	}
	public void setSummary_TotalValue(String summary_TotalValue) {
		this.summary_TotalValue = summary_TotalValue;
	}
	public String getSummary_RateCurrency() {
		return summary_RateCurrency;
	}
	public void setSummary_RateCurrency(String summary_RateCurrency) {
		this.summary_RateCurrency = summary_RateCurrency;
	}
	public String getSummary_TotalValueCurrency() {
		return summary_TotalValueCurrency;
	}
	public void setSummary_TotalValueCurrency(String summary_TotalValueCurrency) {
		this.summary_TotalValueCurrency = summary_TotalValueCurrency;
	}
	public String getSummary_ProgramTotalValue() {
		return summary_ProgramTotalValue;
	}
	public void setSummary_ProgramTotalValue(String summary_ProgramTotalValue) {
		this.summary_ProgramTotalValue = summary_ProgramTotalValue;
	}
	public String getSummary_ProgramTotalCost() {
		return summary_ProgramTotalCost;
	}
	public void setSummary_ProgramTotalCost(String summary_ProgramTotalCost) {
		this.summary_ProgramTotalCost = summary_ProgramTotalCost;
	}
	public String getSummary_CustomerNotes() {
		return summary_CustomerNotes;
	}
	public void setSummary_CustomerNotes(String summary_CustomerNotes) {
		this.summary_CustomerNotes = summary_CustomerNotes;
	}
	public String getSummary_InternalNotes() {
		return summary_InternalNotes;
	}
	public void setSummary_InternalNotes(String summary_InternalNotes) {
		this.summary_InternalNotes = summary_InternalNotes;
	}
	public String getSummary_ProgramNotes() {
		return summary_ProgramNotes;
	}
	public void setSummary_ProgramNotes(String summary_ProgramNotes) {
		this.summary_ProgramNotes = summary_ProgramNotes;
	}
	public String getSummary_CusEmail() {
		return summary_CusEmail;
	}
	public void setSummary_CusEmail(String summary_CusEmail) {
		this.summary_CusEmail = summary_CusEmail;
	}
	public String getSummary_Title() {
		return summary_Title;
	}
	public void setSummary_Title(String summary_Title) {
		this.summary_Title = summary_Title;
	}
	public String getSummary_FName() {
		return summary_FName;
	}
	public void setSummary_FName(String summary_FName) {
		this.summary_FName = summary_FName;
	}
	public String getSummary_LName() {
		return summary_LName;
	}
	public void setSummary_LName(String summary_LName) {
		this.summary_LName = summary_LName;
	}
	public String getSummary_TP() {
		return summary_TP;
	}
	public void setSummary_TP(String summary_TP) {
		this.summary_TP = summary_TP;
	}
	public String getSummary_Email() {
		return summary_Email;
	}
	public void setSummary_Email(String summary_Email) {
		this.summary_Email = summary_Email;
	}
	public String getSummary_address() {
		return summary_address;
	}
	public void setSummary_address(String summary_address) {
		this.summary_address = summary_address;
	}
	public String getSummary_address_2() {
		return summary_address_2;
	}
	public void setSummary_address_2(String summary_address_2) {
		this.summary_address_2 = summary_address_2;
	}
	public String getSummary_country() {
		return summary_country;
	}
	public void setSummary_country(String summary_country) {
		this.summary_country = summary_country;
	}
	public String getSummary_city() {
		return summary_city;
	}
	public void setSummary_city(String summary_city) {
		this.summary_city = summary_city;
	}
	public String getSummary_state() {
		return summary_state;
	}
	public void setSummary_state(String summary_state) {
		this.summary_state = summary_state;
	}
	public String getMO_guestLastName() {
		return MO_guestLastName;
	}
	public void setMO_guestLastName(String mO_guestLastName) {
		MO_guestLastName = mO_guestLastName;
	}
	public String getMO_reservationNo_1() {
		return MO_reservationNo_1;
	}
	public void setMO_reservationNo_1(String mO_reservationNo_1) {
		MO_reservationNo_1 = mO_reservationNo_1;
	}
	public String getMO_programName_1() {
		return MO_programName_1;
	}
	public void setMO_programName_1(String mO_programName_1) {
		MO_programName_1 = mO_programName_1;
	}
	public String getMO_activityName_1() {
		return MO_activityName_1;
	}
	public void setMO_activityName_1(String mO_activityName_1) {
		MO_activityName_1 = mO_activityName_1;
	}
	public String getMO_activityDate() {
		return MO_activityDate;
	}
	public void setMO_activityDate(String mO_activityDate) {
		MO_activityDate = mO_activityDate;
	}
	public String getMO_rate() {
		return MO_rate;
	}
	public void setMO_rate(String mO_rate) {
		MO_rate = mO_rate;
	}
	public String getMO_qty() {
		return MO_qty;
	}
	public void setMO_qty(String mO_qty) {
		MO_qty = mO_qty;
	}
	public String getMO_activityRate() {
		return MO_activityRate;
	}
	public void setMO_activityRate(String mO_activityRate) {
		MO_activityRate = mO_activityRate;
	}
	public String getMO_activityRateCurrency() {
		return MO_activityRateCurrency;
	}
	public void setMO_activityRateCurrency(String mO_activityRateCurrency) {
		MO_activityRateCurrency = mO_activityRateCurrency;
	}
	public String getMO_Total() {
		return MO_Total;
	}
	public void setMO_Total(String mO_Total) {
		MO_Total = mO_Total;
	}
	public String getMO_TotalCurrency() {
		return MO_TotalCurrency;
	}
	public void setMO_TotalCurrency(String mO_TotalCurrency) {
		MO_TotalCurrency = mO_TotalCurrency;
	}
	public String getMO_TotalBookingValue() {
		return MO_TotalBookingValue;
	}
	public void setMO_TotalBookingValue(String mO_TotalBookingValue) {
		MO_TotalBookingValue = mO_TotalBookingValue;
	}
	public String getMO_TotalCost() {
		return MO_TotalCost;
	}
	public void setMO_TotalCost(String mO_TotalCost) {
		MO_TotalCost = mO_TotalCost;
	}
	public String getMO_ManagerActivityName_1() {
		return MO_ManagerActivityName_1;
	}
	public void setMO_ManagerActivityName_1(String mO_ManagerActivityName_1) {
		MO_ManagerActivityName_1 = mO_ManagerActivityName_1;
	}
	public String getMO_ManagerActivityDate() {
		return MO_ManagerActivityDate;
	}
	public void setMO_ManagerActivityDate(String mO_ManagerActivityDate) {
		MO_ManagerActivityDate = mO_ManagerActivityDate;
	}
	public String getMO_ManagerRate() {
		return MO_ManagerRate;
	}
	public void setMO_ManagerRate(String mO_ManagerRate) {
		MO_ManagerRate = mO_ManagerRate;
	}
	public String getMO_NewTotalBookingValue() {
		return MO_NewTotalBookingValue;
	}
	public void setMO_NewTotalBookingValue(String mO_NewTotalBookingValue) {
		MO_NewTotalBookingValue = mO_NewTotalBookingValue;
	}
	public String getMO_NewTotalBookingCost() {
		return MO_NewTotalBookingCost;
	}
	public void setMO_NewTotalBookingCost(String mO_NewTotalBookingCost) {
		MO_NewTotalBookingCost = mO_NewTotalBookingCost;
	}
	public String getRA_guestLastName() {
		return RA_guestLastName;
	}
	public void setRA_guestLastName(String rA_guestLastName) {
		RA_guestLastName = rA_guestLastName;
	}
	public String getRA_reservationNo_1() {
		return RA_reservationNo_1;
	}
	public void setRA_reservationNo_1(String rA_reservationNo_1) {
		RA_reservationNo_1 = rA_reservationNo_1;
	}
	public String getRA_programName_1() {
		return RA_programName_1;
	}
	public void setRA_programName_1(String rA_programName_1) {
		RA_programName_1 = rA_programName_1;
	}
	public String getRA_activityName_1() {
		return RA_activityName_1;
	}
	public void setRA_activityName_1(String rA_activityName_1) {
		RA_activityName_1 = rA_activityName_1;
	}
	public String getRA_activityDate() {
		return RA_activityDate;
	}
	public void setRA_activityDate(String rA_activityDate) {
		RA_activityDate = rA_activityDate;
	}
	public String getRA_session() {
		return RA_session;
	}
	public void setRA_session(String rA_session) {
		RA_session = rA_session;
	}
	public String getRA_rate() {
		return RA_rate;
	}
	public void setRA_rate(String rA_rate) {
		RA_rate = rA_rate;
	}
	public String getRA_qty() {
		return RA_qty;
	}
	public void setRA_qty(String rA_qty) {
		RA_qty = rA_qty;
	}
	public String getRA_activityRate() {
		return RA_activityRate;
	}
	public void setRA_activityRate(String rA_activityRate) {
		RA_activityRate = rA_activityRate;
	}
	public String getRA_activityRateCurrency() {
		return RA_activityRateCurrency;
	}
	public void setRA_activityRateCurrency(String rA_activityRateCurrency) {
		RA_activityRateCurrency = rA_activityRateCurrency;
	}
	public String getRA_Total() {
		return RA_Total;
	}
	public void setRA_Total(String rA_Total) {
		RA_Total = rA_Total;
	}
	public String getRA_TotalCurrency() {
		return RA_TotalCurrency;
	}
	public void setRA_TotalCurrency(String rA_TotalCurrency) {
		RA_TotalCurrency = rA_TotalCurrency;
	}
	public String getRA_TotalBookingValue() {
		return RA_TotalBookingValue;
	}
	public void setRA_TotalBookingValue(String rA_TotalBookingValue) {
		RA_TotalBookingValue = rA_TotalBookingValue;
	}
	public String getRA_TotalCost() {
		return RA_TotalCost;
	}
	public void setRA_TotalCost(String rA_TotalCost) {
		RA_TotalCost = rA_TotalCost;
	}
	public String getAA_guestLastName() {
		return AA_guestLastName;
	}
	public void setAA_guestLastName(String aA_guestLastName) {
		AA_guestLastName = aA_guestLastName;
	}
	public String getAA_reservationNo_1() {
		return AA_reservationNo_1;
	}
	public void setAA_reservationNo_1(String aA_reservationNo_1) {
		AA_reservationNo_1 = aA_reservationNo_1;
	}
	public String getAA_ReservationDate() {
		return AA_ReservationDate;
	}
	public void setAA_ReservationDate(String aA_ReservationDate) {
		AA_ReservationDate = aA_ReservationDate;
	}
	public String getAA_programName_1() {
		return AA_programName_1;
	}
	public void setAA_programName_1(String aA_programName_1) {
		AA_programName_1 = aA_programName_1;
	}
	public String getAA_activityName_1() {
		return AA_activityName_1;
	}
	public void setAA_activityName_1(String aA_activityName_1) {
		AA_activityName_1 = aA_activityName_1;
	}
	public String getAA_activityDate() {
		return AA_activityDate;
	}
	public void setAA_activityDate(String aA_activityDate) {
		AA_activityDate = aA_activityDate;
	}
	public String getAA_session() {
		return AA_session;
	}
	public void setAA_session(String aA_session) {
		AA_session = aA_session;
	}
	public String getAA_rate() {
		return AA_rate;
	}
	public void setAA_rate(String aA_rate) {
		AA_rate = aA_rate;
	}
	public String getAA_qty() {
		return AA_qty;
	}
	public void setAA_qty(String aA_qty) {
		AA_qty = aA_qty;
	}
	public String getAA_activityRate() {
		return AA_activityRate;
	}
	public void setAA_activityRate(String aA_activityRate) {
		AA_activityRate = aA_activityRate;
	}
	public String getAA_activityRateCurrency() {
		return AA_activityRateCurrency;
	}
	public void setAA_activityRateCurrency(String aA_activityRateCurrency) {
		AA_activityRateCurrency = aA_activityRateCurrency;
	}
	public String getAA_Total() {
		return AA_Total;
	}
	public void setAA_Total(String aA_Total) {
		AA_Total = aA_Total;
	}
	public String getAA_TotalCurrency() {
		return AA_TotalCurrency;
	}
	public void setAA_TotalCurrency(String aA_TotalCurrency) {
		AA_TotalCurrency = aA_TotalCurrency;
	}
	public String getAA_TotalBookingValue() {
		return AA_TotalBookingValue;
	}
	public void setAA_TotalBookingValue(String aA_TotalBookingValue) {
		AA_TotalBookingValue = aA_TotalBookingValue;
	}
	public String getAA_TotalCost() {
		return AA_TotalCost;
	}
	public void setAA_TotalCost(String aA_TotalCost) {
		AA_TotalCost = aA_TotalCost;
	}
	public String getADQ_guestLastName() {
		return ADQ_guestLastName;
	}
	public void setADQ_guestLastName(String aDQ_guestLastName) {
		ADQ_guestLastName = aDQ_guestLastName;
	}
	public String getADQ_reservationNo_1() {
		return ADQ_reservationNo_1;
	}
	public void setADQ_reservationNo_1(String aDQ_reservationNo_1) {
		ADQ_reservationNo_1 = aDQ_reservationNo_1;
	}
	public String getADQ_ReservationDate() {
		return ADQ_ReservationDate;
	}
	public void setADQ_ReservationDate(String aDQ_ReservationDate) {
		ADQ_ReservationDate = aDQ_ReservationDate;
	}
	public String getADQ_programName_1() {
		return ADQ_programName_1;
	}
	public void setADQ_programName_1(String aDQ_programName_1) {
		ADQ_programName_1 = aDQ_programName_1;
	}
	public String getADQ_SupplierName() {
		return ADQ_SupplierName;
	}
	public void setADQ_SupplierName(String aDQ_SupplierName) {
		ADQ_SupplierName = aDQ_SupplierName;
	}
	public String getADQ_activityName_1() {
		return ADQ_activityName_1;
	}
	public void setADQ_activityName_1(String aDQ_activityName_1) {
		ADQ_activityName_1 = aDQ_activityName_1;
	}
	public String getADQ_activityDate() {
		return ADQ_activityDate;
	}
	public void setADQ_activityDate(String aDQ_activityDate) {
		ADQ_activityDate = aDQ_activityDate;
	}
	public String getADQ_session() {
		return ADQ_session;
	}
	public void setADQ_session(String aDQ_session) {
		ADQ_session = aDQ_session;
	}
	public String getADQ_rate() {
		return ADQ_rate;
	}
	public void setADQ_rate(String aDQ_rate) {
		ADQ_rate = aDQ_rate;
	}
	public String getADQ_qty() {
		return ADQ_qty;
	}
	public void setADQ_qty(String aDQ_qty) {
		ADQ_qty = aDQ_qty;
	}
	public String getADQ_activityRate() {
		return ADQ_activityRate;
	}
	public void setADQ_activityRate(String aDQ_activityRate) {
		ADQ_activityRate = aDQ_activityRate;
	}
	public String getADQ_activityRateCurrency() {
		return ADQ_activityRateCurrency;
	}
	public void setADQ_activityRateCurrency(String aDQ_activityRateCurrency) {
		ADQ_activityRateCurrency = aDQ_activityRateCurrency;
	}
	public String getADQ_Total() {
		return ADQ_Total;
	}
	public void setADQ_Total(String aDQ_Total) {
		ADQ_Total = aDQ_Total;
	}
	public String getADQ_TotalCurrency() {
		return ADQ_TotalCurrency;
	}
	public void setADQ_TotalCurrency(String aDQ_TotalCurrency) {
		ADQ_TotalCurrency = aDQ_TotalCurrency;
	}
	public String getADQ_TotalBookingValue() {
		return ADQ_TotalBookingValue;
	}
	public void setADQ_TotalBookingValue(String aDQ_TotalBookingValue) {
		ADQ_TotalBookingValue = aDQ_TotalBookingValue;
	}
	public String getADQ_TotalCost() {
		return ADQ_TotalCost;
	}
	public void setADQ_TotalCost(String aDQ_TotalCost) {
		ADQ_TotalCost = aDQ_TotalCost;
	}
	public String getGD_guestLastName() {
		return GD_guestLastName;
	}
	public void setGD_guestLastName(String gD_guestLastName) {
		GD_guestLastName = gD_guestLastName;
	}
	public String getGD_reservationNo_1() {
		return GD_reservationNo_1;
	}
	public void setGD_reservationNo_1(String gD_reservationNo_1) {
		GD_reservationNo_1 = gD_reservationNo_1;
	}
	public String getGD_Title() {
		return GD_Title;
	}
	public void setGD_Title(String gD_Title) {
		GD_Title = gD_Title;
	}
	public String getGD_FName() {
		return GD_FName;
	}
	public void setGD_FName(String gD_FName) {
		GD_FName = gD_FName;
	}
	public String getGD_LName() {
		return GD_LName;
	}
	public void setGD_LName(String gD_LName) {
		GD_LName = gD_LName;
	}
	public String getGD_TP() {
		return GD_TP;
	}
	public void setGD_TP(String gD_TP) {
		GD_TP = gD_TP;
	}
	public String getGD_Email() {
		return GD_Email;
	}
	public void setGD_Email(String gD_Email) {
		GD_Email = gD_Email;
	}
	public String getGD_address() {
		return GD_address;
	}
	public void setGD_address(String gD_address) {
		GD_address = gD_address;
	}
	public String getGD_address_2() {
		return GD_address_2;
	}
	public void setGD_address_2(String gD_address_2) {
		GD_address_2 = gD_address_2;
	}
	public String getGD_country() {
		return GD_country;
	}
	public void setGD_country(String gD_country) {
		GD_country = gD_country;
	}
	public String getGD_city() {
		return GD_city;
	}
	public void setGD_city(String gD_city) {
		GD_city = gD_city;
	}
	public String getGD_state() {
		return GD_state;
	}
	public void setGD_state(String gD_state) {
		GD_state = gD_state;
	}
	
	
	

}
