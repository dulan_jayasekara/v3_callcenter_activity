package webreservationreport;

public class WEB_Reservation {
	
	private boolean isReservationReportLoaded  = true;
	public String BookingDate;
	public String CanPolicyDueDate;
	public String CustomerName_1;
	public String SupplierName;
	public String ProductType;
	public String BookingStatus;
	public String BookingChannel;
	public String CustomerName_2;
	public String CustomerTP;
	public String VoucherIssued;
	public String InvoiceIssued;
	public String InvoiceIssuedDate;
	public String LpoRequired;
	public String LpoProvided;
	public String LpoNo;
	
	public String SellingCurrency_1;
	public String TotalRate;
	public String CreditCardFee;
	public String BookingFeeCharges;
	public String AmountPaid;
	
	public String SellingCurrency_2;
	public String GrossOrderValue;
	public String AgentCommission;
	public String NetOrderValue;
	public String TotalCost;
	
	public String BaseCurrency;
	public String BaseCurrency_NetOrgerValue;
	public String BaseCurrency_TotalCost;
	public String PageTotal_1;
	public String PageTotal_2;
	public String GrandTotal_1;
	public String GrandTotal_2;
	
	public String paymentType;
	public String payRefernce;
	public String authCode;
	
	
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public String getPayRefernce() {
		return payRefernce;
	}
	public void setPayRefernce(String payRefernce) {
		this.payRefernce = payRefernce;
	}
	public String getAuthCode() {
		return authCode;
	}
	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}
	public boolean isReservationReportLoaded() {
		return isReservationReportLoaded;
	}
	public void setReservationReportLoaded(boolean isReservationReportLoaded) {
		this.isReservationReportLoaded = isReservationReportLoaded;
	}
	public String getBookingDate() {
		return BookingDate;
	}
	public void setBookingDate(String bookingDate) {
		BookingDate = bookingDate;
	}
	public String getCanPolicyDueDate() {
		return CanPolicyDueDate;
	}
	public void setCanPolicyDueDate(String canPolicyDueDate) {
		CanPolicyDueDate = canPolicyDueDate;
	}
	public String getCustomerName_1() {
		return CustomerName_1;
	}
	public void setCustomerName_1(String customerName_1) {
		CustomerName_1 = customerName_1;
	}
	public String getSupplierName() {
		return SupplierName;
	}
	public void setSupplierName(String supplierName) {
		SupplierName = supplierName;
	}
	public String getProductType() {
		return ProductType;
	}
	public void setProductType(String productType) {
		ProductType = productType;
	}
	public String getBookingStatus() {
		return BookingStatus;
	}
	public void setBookingStatus(String bookingStatus) {
		BookingStatus = bookingStatus;
	}
	public String getBookingChannel() {
		return BookingChannel;
	}
	public void setBookingChannel(String bookingChannel) {
		BookingChannel = bookingChannel;
	}
	public String getCustomerName_2() {
		return CustomerName_2;
	}
	public void setCustomerName_2(String customerName_2) {
		CustomerName_2 = customerName_2;
	}
	public String getCustomerTP() {
		return CustomerTP;
	}
	public void setCustomerTP(String customerTP) {
		CustomerTP = customerTP;
	}
	public String getVoucherIssued() {
		return VoucherIssued;
	}
	public void setVoucherIssued(String voucherIssued) {
		VoucherIssued = voucherIssued;
	}
	public String getInvoiceIssued() {
		return InvoiceIssued;
	}
	public void setInvoiceIssued(String invoiceIssued) {
		InvoiceIssued = invoiceIssued;
	}
	public String getInvoiceIssuedDate() {
		return InvoiceIssuedDate;
	}
	public void setInvoiceIssuedDate(String invoiceIssuedDate) {
		InvoiceIssuedDate = invoiceIssuedDate;
	}
	public String getLpoRequired() {
		return LpoRequired;
	}
	public void setLpoRequired(String lpoRequired) {
		LpoRequired = lpoRequired;
	}
	public String getLpoProvided() {
		return LpoProvided;
	}
	public void setLpoProvided(String lpoProvided) {
		LpoProvided = lpoProvided;
	}
	public String getLpoNo() {
		return LpoNo;
	}
	public void setLpoNo(String lpoNo) {
		LpoNo = lpoNo;
	}
	public String getSellingCurrency_1() {
		return SellingCurrency_1;
	}
	public void setSellingCurrency_1(String sellingCurrency_1) {
		SellingCurrency_1 = sellingCurrency_1;
	}
	public String getTotalRate() {
		return TotalRate;
	}
	public void setTotalRate(String totalRate) {
		TotalRate = totalRate;
	}
	public String getCreditCardFee() {
		return CreditCardFee;
	}
	public void setCreditCardFee(String creditCardFee) {
		CreditCardFee = creditCardFee;
	}
	public String getBookingFeeCharges() {
		return BookingFeeCharges;
	}
	public void setBookingFeeCharges(String bookingFeeCharges) {
		BookingFeeCharges = bookingFeeCharges;
	}
	public String getAmountPaid() {
		return AmountPaid;
	}
	public void setAmountPaid(String amountPaid) {
		AmountPaid = amountPaid;
	}
	public String getSellingCurrency_2() {
		return SellingCurrency_2;
	}
	public void setSellingCurrency_2(String sellingCurrency_2) {
		SellingCurrency_2 = sellingCurrency_2;
	}
	public String getGrossOrderValue() {
		return GrossOrderValue;
	}
	public void setGrossOrderValue(String grossOrderValue) {
		GrossOrderValue = grossOrderValue;
	}
	public String getAgentCommission() {
		return AgentCommission;
	}
	public void setAgentCommission(String agentCommission) {
		AgentCommission = agentCommission;
	}
	public String getNetOrderValue() {
		return NetOrderValue;
	}
	public void setNetOrderValue(String netOrderValue) {
		NetOrderValue = netOrderValue;
	}
	public String getTotalCost() {
		return TotalCost;
	}
	public void setTotalCost(String totalCost) {
		TotalCost = totalCost;
	}
	public String getBaseCurrency() {
		return BaseCurrency;
	}
	public void setBaseCurrency(String baseCurrency) {
		BaseCurrency = baseCurrency;
	}
	public String getBaseCurrency_NetOrgerValue() {
		return BaseCurrency_NetOrgerValue;
	}
	public void setBaseCurrency_NetOrgerValue(String baseCurrency_NetOrgerValue) {
		BaseCurrency_NetOrgerValue = baseCurrency_NetOrgerValue;
	}
	public String getBaseCurrency_TotalCost() {
		return BaseCurrency_TotalCost;
	}
	public void setBaseCurrency_TotalCost(String baseCurrency_TotalCost) {
		BaseCurrency_TotalCost = baseCurrency_TotalCost;
	}
	public String getPageTotal_1() {
		return PageTotal_1;
	}
	public void setPageTotal_1(String pageTotal_1) {
		PageTotal_1 = pageTotal_1;
	}
	public String getPageTotal_2() {
		return PageTotal_2;
	}
	public void setPageTotal_2(String pageTotal_2) {
		PageTotal_2 = pageTotal_2;
	}
	public String getGrandTotal_1() {
		return GrandTotal_1;
	}
	public void setGrandTotal_1(String grandTotal_1) {
		GrandTotal_1 = grandTotal_1;
	}
	public String getGrandTotal_2() {
		return GrandTotal_2;
	}
	public void setGrandTotal_2(String grandTotal_2) {
		GrandTotal_2 = grandTotal_2;
	}
	

	
	
}
