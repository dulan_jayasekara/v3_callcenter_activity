package bookinglistreport;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.controller.PG_Properties;
import com.model.*;
import com.types.TourOperatorType;


public class LoadBookingDetails {
	
	private WebDriver driver;
	private String currentDateforImages;
	private ActivityDetails activitydetails;
	private ArrayList<String> cancelPolicy;
	private ActivityInventoryRecords inventory;
	private TourOperatorType  UserTypeDC_B2B;
	
	
	public LoadBookingDetails(ActivityDetails activityDetails, ActivityInventoryRecords inventoryD, WebDriver Driver){
		
		this.activitydetails = activityDetails;
		this.inventory=inventoryD;
		this.driver = Driver;
	}

	public BookingList getBookingCardList(WebDriver Driver) throws InterruptedException, IOException{
		
		DateFormat dateFormatcurrentDate = new SimpleDateFormat("yyyy_MM_dd");
		Calendar cal = Calendar.getInstance();
		currentDateforImages = dateFormatcurrentDate.format(cal.getTime());
		WebDriverWait wait = new WebDriverWait(driver, 90);
		cancelPolicy = new ArrayList<String>();
		UserTypeDC_B2B = inventory.getUserTypeDC_B2B();
		BookingList bookingList = new BookingList();
		
		driver.get(PG_Properties.getProperty("Baseurl")+ "/reports/operational/mainReport.do?reportId=39&reportName=Booking List Report");		
		Thread.sleep(2000);
		
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");	
		
		driver.findElement(By.id("reservationno")).clear();
		driver.findElement(By.id("reservationno")).sendKeys(activitydetails.getReservationNo());
		driver.findElement(By.id("reservationno_lkup")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("lookup");
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
		Thread.sleep(1000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");
		
		driver.findElement(By.xpath("html/body/span/table/tbody/tr/td/table/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[3]/a/img")).click();
		Thread.sleep(4000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");
		
		try {
			
			if (driver.findElements(By.id("reportData1")).size() != 0) {
				
				driver.switchTo().defaultContent();
				driver.switchTo().frame("reportIframe");
				
				WebElement element = driver.findElement(By.className("reportDataRows0"));
		        
		        List<WebElement>  columnlist  = element.findElements(By.tagName("td"));
		       
		        bookingList.setBList_Reservation_Number(columnlist.get(0).getText());
		        bookingList.setBList_Reservation_Date(columnlist.get(1).getText());
		        bookingList.setBList_Reservation_Time(columnlist.get(2).getText());
		        bookingList.setBList_Customer_Type(columnlist.get(3).getText());       
		        bookingList.setBList_PaymentType(columnlist.get(4).getText());
		        
		        bookingList.setBList_Customer_Name(columnlist.get(6).getText());
		        bookingList.setBList_LeadGuest_Name(columnlist.get(7).getText());
		        bookingList.setBList_DateOf_FirstElement(columnlist.get(8).getText());
		        bookingList.setBList_DateOf_Cxl_Deadline(columnlist.get(9).getText());
		        bookingList.setBList_City_Name_Country(columnlist.get(10).getText());
		        bookingList.setBList_Product_Type(columnlist.get(11).getText());              
		        bookingList.setBList_Status(driver.findElement(By.xpath(".//*[@id='reportData1']/tbody/tr[5]/td[14]")).getText());
		        
		       
		        WebElement invElement = driver.findElement(By.xpath(".//*[@id='reportData1']/tbody/tr[5]/td[15]"));
		        String inColor = invElement.findElement(By.tagName("img")).getAttribute("id");
		        WebElement payElement = driver.findElement(By.xpath(".//*[@id='reportData1']/tbody/tr[5]/td[16]"));
		        String payColor = payElement.findElement(By.tagName("img")).getAttribute("id");
		        WebElement vouElement = driver.findElement(By.xpath(".//*[@id='reportData1']/tbody/tr[5]/td[17]"));
		        String vouColor = vouElement.findElement(By.tagName("img")).getAttribute("id");
		        
		        if(vouColor.contains("green"))
		        	bookingList.setBList_Voucher_Issued(true);
		        else
		        	bookingList.setBList_Voucher_Issued(false);
		        
		        if(inColor.contains("green"))
		        	bookingList.setBList_Invoice_Issued(true);
		        else
		        	bookingList.setBList_Invoice_Issued(false);
		        
		        if(payColor.contains("green"))
		        	bookingList.setBList_Payment_Receieved(true);
		        else
		        	bookingList.setBList_Payment_Receieved(false);
		        
		        
		        driver.findElement(By.xpath(".//*[@id='reportData1']/tbody/tr[5]/td[1]/a")).click();
				
		        ////////
		        
		       /* if (inventory.getUserTypeDC_B2B().equalsIgnoreCase("Net_cash") || inventory.getUserTypeDC_B2B().equalsIgnoreCase("Net_cash") || inventory.getUserTypeDC_B2B().equalsIgnoreCase("Net_cash")) {
					
				}*/
		        
		        
		        ///////
		        
				driver.switchTo().defaultContent();
				driver.switchTo().frame("dialogwindow");
				
				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("reservationDetailsId")));
				File bookingLists = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		        FileUtils.copyFile(bookingLists, new File(""+currentDateforImages+"/"+activitydetails.getScenarioCount()+"_BookingList.png"));
				
				
		        WebElement reservationDetails = driver.findElement(By.id("reservationDetailsId"));
		        List<WebElement>  reservationListTr  = reservationDetails.findElements(By.tagName("tr"));
		                    
		        for (int i = 0; i < reservationListTr.size(); i++) {
		        	                					
		        	switch (i) {
		        	
					case 0:
						List<WebElement>  reservationListTd0  = reservationListTr.get(0).findElements(By.tagName("td"));
						bookingList.setBCard_Res_ReservationNumber(reservationListTd0.get(3).getText());
						bookingList.setBCard_Res_Reservation_Date(reservationListTd0.get(6).getText());	
						
						String BCard_Res_PM;
						try {
							BCard_Res_PM	 = driver.findElement(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[7]/td[4]")).getText();
						} catch (Exception e) {
							BCard_Res_PM = "Not Available";
						}
						
						String BCard_Res_PR;
						try {
							BCard_Res_PR = driver.findElement(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[8]/td[4]")).getText();
						} catch (Exception e) {
							BCard_Res_PR = "Not Available";
						}
						
						
						bookingList.setBCard_Res_PaymentMethod(BCard_Res_PM);						
						bookingList.setBCard_Res_PaymentReference(BCard_Res_PR);	
						
						break;
					case 1:
						List<WebElement>  reservationListTd1  = reservationListTr.get(1).findElements(By.tagName("td"));
						bookingList.setBCard_Res_Origin_City_Country(reservationListTd1.get(3).getText());
			            bookingList.setBCard_Res_FirstElement_Date(reservationListTd1.get(6).getText());				
						break;				
					case 2:
						List<WebElement>  reservationListTd2  = reservationListTr.get(2).findElements(By.tagName("td"));
						bookingList.setBCard_Res_Destination_City_Country(reservationListTd2.get(3).getText());
			            bookingList.setBCard_Res_Cancel_Deadline(reservationListTd2.get(6).getText());				
						break;
						
					case 3:
						List<WebElement>  reservationListTd3  = reservationListTr.get(3).findElements(By.tagName("td"));
						bookingList.setBCard_Res_Products_Booked(reservationListTd3.get(3).getText());			
						break;				
					case 4:
						List<WebElement>  reservationListTd4  = reservationListTr.get(4).findElements(By.tagName("td"));
						bookingList.setBCard_Res_Booking_Status(reservationListTd4.get(3).getText());			
						break;
					case 5:
						List<WebElement>  reservationListTd5  = reservationListTr.get(5).findElements(By.tagName("td"));
						bookingList.setBCard_Res_FailedSector(reservationListTd5.get(3).getText());			
						break;
						
					case 6:
						String BCard_Res_CurrencyType = driver.findElement(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[10]/td[7]")).getText();
						bookingList.setBCard_Res_CurrencyType(BCard_Res_CurrencyType);			
						break;
						///////				
					case 7:
						String BCard_Res_SubTotal = driver.findElement(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[11]/td[7]")).getText();
						bookingList.setBCard_Res_SubTotal(BCard_Res_SubTotal);		
						break;
										
					case 8:
						String BCard_Res_BookingFee = driver.findElement(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[12]/td[7]")).getText();
						bookingList.setBCard_Res_BookingFee(BCard_Res_BookingFee);			
						break;
					case 9:
						String BCard_Res_Tax_Other = driver.findElement(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[13]/td[7]")).getText();
						bookingList.setBCard_Res_Tax_Other(BCard_Res_Tax_Other);			
						break;
					case 10:
						String BCard_Res_Credit_Fee  = driver.findElement(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[14]/td[7]")).getText();
						bookingList.setBCard_Res_Credit_Fee(BCard_Res_Credit_Fee);			
						break;
					case 11:
						String BCard_Res_Total_Booking_Fee = driver.findElement(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[15]/td[7]")).getText();
						bookingList.setBCard_Res_Total_Booking_Fee(BCard_Res_Total_Booking_Fee);		
						break;
					case 12:
						String BCard_Res_Total_Amount_Payable = driver.findElement(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[16]/td[7]")).getText();
						bookingList.setBCard_Res_Total_Amount_Payable(BCard_Res_Total_Amount_Payable);			
						break;
					case 13:
						String BCard_Res_Amount_Payable_CheckIn = driver.findElement(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[17]/td[7]")).getText();
						bookingList.setBCard_Res_Amount_Payable_CheckIn(BCard_Res_Amount_Payable_CheckIn);			
						break;
					case 14:
						String BCard_Res_AmountPaid = driver.findElement(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[18]/td[7]")).getText();
						bookingList.setBCard_Res_AmountPaid(BCard_Res_AmountPaid);			
						break;	
						
					}			
				}
		        
		        //////
		        
		        try {			
		        	if (driver.findElements(By.id("agentdetailsid")).size() != 0) {
						
		        		bookingList.setAgentShows(true);
		        		
		        		String BCard_Agent_AgentName = driver.findElement(By.xpath(".//*[@id='agentdetailsid']/table/tbody/tr[1]/td[4]")).getText();
		        		bookingList.setBCard_Agent_AgentName(BCard_Agent_AgentName);	
		        		
		        		String BCard_Agent_UserName = driver.findElement(By.xpath(".//*[@id='agentdetailsid']/table/tbody/tr[2]/td[4]")).getText();
		        		bookingList.setBCard_Agent_UserName(BCard_Agent_UserName);	
		        		
		        		String BCard_Agent_AgentType = driver.findElement(By.xpath(".//*[@id='agentdetailsid']/table/tbody/tr[3]/td[4]")).getText();
		        		bookingList.setBCard_Agent_AgentType(BCard_Agent_AgentType);	
		        		
		        		String lpoReq = driver.findElement(By.xpath(".//*[@id='agentdetailsid']/table/tbody/tr[4]/td[4]")).getText();
		        		bookingList.setLpoReq(lpoReq);	
		        		
		        		String lpoProvide = driver.findElement(By.xpath(".//*[@id='agentdetailsid']/table/tbody/tr[5]/td[4]")).getText();
		        		bookingList.setLpoProvide(lpoProvide);	
		        		
		        		String beforeCreditBalance = driver.findElement(By.xpath(".//*[@id='agentdetailsid']/table/tbody/tr[2]/td[7]")).getText().replace(",", "").split("\\.")[0];
		        		bookingList.setBeforeCreditBalance(beforeCreditBalance);	
		        		
		        		if (UserTypeDC_B2B == TourOperatorType.COMCASH){
		        			
		        			String card_Agent_CommissionPercentage = driver.findElement(By.xpath(".//*[@id='agentdetailsid']/table/tbody/tr[4]/td[7]")).getText();
		            		bookingList.setBCard_Agent_CommissionPercentage(card_Agent_CommissionPercentage);	
		            		           		
		            		String card_Agent_CommissionAmount = driver.findElement(By.xpath(".//*[@id='agentdetailsid']/table/tbody/tr[5]/td[7]")).getText().split("\\.")[0];
		            		bookingList.setBCard_Agent_CommissionAmount(card_Agent_CommissionAmount);	
		        			
		        		}
		        		
		        		if (UserTypeDC_B2B == TourOperatorType.COMCREDITLPONO || UserTypeDC_B2B == TourOperatorType.COMCREDITLPOY) {
							
		        			String card_Agent_CommissionPercentage = driver.findElement(By.xpath(".//*[@id='agentdetailsid']/table/tbody/tr[7]/td[7]")).getText();
		            		bookingList.setBCard_Agent_CommissionPercentage(card_Agent_CommissionPercentage);	
		            		           		
		            		String card_Agent_CommissionAmount = driver.findElement(By.xpath(".//*[@id='agentdetailsid']/table/tbody/tr[8]/td[7]")).getText().split("\\.")[0];
		            		bookingList.setBCard_Agent_CommissionAmount(card_Agent_CommissionAmount);	
		            		
						}
		        		
					}
		        	       	
				} catch (Exception e) {
					e.printStackTrace();
				}
		        
		        
		        
		        /////
		        
		        WebElement customerDetails = driver.findElement(By.id("customerDetailsId"));
				 
		        List<WebElement>  customerDetailsListTr  = customerDetails.findElements(By.tagName("tr"));
		        
		        for (int i = 0; i < customerDetailsListTr.size(); i++) {
		        	
		        	switch (i) {
		        	
					case 0:
						List<WebElement>  customerDetailsListTd0  = customerDetailsListTr.get(0).findElements(By.tagName("td"));
						bookingList.setBCard_Guest_FName(customerDetailsListTd0.get(4).getText());
						bookingList.setBCard_Guest_Emergency_Contact(customerDetailsListTd0.get(7).getText());				
						break;				
					case 1:
						List<WebElement>  customerDetailsListTd1  = customerDetailsListTr.get(2).findElements(By.tagName("td"));
						bookingList.setBCard_Guest_Add(customerDetailsListTd1.get(3).getText());
						bookingList.setBCard_Guest_PhoneNumber(customerDetailsListTd1.get(6).getText());				
						break;
						
					case 3:
						List<WebElement>  customerDetailsListTd2  = customerDetailsListTr.get(3).findElements(By.tagName("td"));
						bookingList.setBCard_Guest_Email(customerDetailsListTd2.get(3).getText());			
						break;					
					case 4:
						List<WebElement>  customerDetailsListTd3  = customerDetailsListTr.get(4).findElements(By.tagName("td"));
						bookingList.setBCard_Guest_City(customerDetailsListTd3.get(3).getText());			
						break;
					case 5:
						List<WebElement>  customerDetailsListTd4  = customerDetailsListTr.get(5).findElements(By.tagName("td"));
						bookingList.setBCard_Guest_Country(customerDetailsListTd4.get(3).getText());			
						break;	
		        	}  	
		        }
		        
		        //////
		        
		        driver.findElement(By.xpath(".//*[@id='activityDetailsimgTD']/img")).click();
		        Thread.sleep(2000);
		        
		        WebElement actvityDetails = driver.findElement(By.id("activityDetailsId"));
				 
		        List<WebElement>  actvityDetailsListTr  = actvityDetails.findElements(By.tagName("tr"));
		        
		        for (int i = 0; i < actvityDetailsListTr.size(); i++) {
		        	
		        	switch (i) {
		        	
		        	case 0:
						List<WebElement>  actvityDetailsListTd0  = actvityDetailsListTr.get(3).findElements(By.tagName("td"));
						bookingList.setBCard_Activity_ActivityName(actvityDetailsListTd0.get(3).getText());
						bookingList.setBCard_Activity_DatesBookedFor(actvityDetailsListTd0.get(6).getText());				
						break;
		        	
		        	case 1:
						List<WebElement>  actvityDetailsListTd1  = actvityDetailsListTr.get(4).findElements(By.tagName("td"));
						bookingList.setBCard_Activity_BookingStatus(actvityDetailsListTd1.get(3).getText());		
						break;	
		        	}     	
		        }
				
		        String BCard_Activity_OccupantNames = driver.findElement(By.xpath(".//*[@id='activityDetailsId']/table/tbody/tr[2]/td/table/tbody/tr[2]/td[3]/table/tbody/tr/td[1]")).getText();
		        bookingList.setBCard_Activity_OccupantNames(BCard_Activity_OccupantNames);	
		        	
		        String BCard_Activity_TotalRate  = driver.findElement(By.xpath(".//*[@id='activityDetailsId']/table/tbody/tr[2]/td/table/tbody/tr[2]/td[4]")).getText();
		        bookingList.setBCard_Activity_TotalRate(BCard_Activity_TotalRate);	
		        	
		    	String BCard_Activity_TotalRateCurrrency = driver.findElement(By.xpath(".//*[@id='activityDetailsId']/table/tbody/tr[2]/td/table/tbody/tr[1]/td[5]")).getText().substring(14, 17);
		    	bookingList.setBCard_Activity_TotalRateCurrrency(BCard_Activity_TotalRateCurrrency);	
		    		
		    	String BCard_Activity_qty = driver.findElement(By.xpath(".//*[@id='activityDetailsId']/table/tbody/tr[2]/td/table/tbody/tr[2]/td[5]")).getText();
		    	bookingList.setBCard_Activity_qty(BCard_Activity_qty);	
		    		
		    	String BCard_Activity_Currency = driver.findElement(By.xpath(".//*[@id='activityDetailsId']/table/tbody/tr[5]/td/table/tbody/tr[1]/td[4]")).getText().substring(1, 4);
		    	bookingList.setBCard_Activity_Currency(BCard_Activity_Currency);
		    	
		    	/////BCard_Activity_CreditFee
		    	
		    	
		    	
		    	String BCard_Activity_SubTotal = driver.findElement(By.xpath(".//*[@id='activityDetailsId']/table/tbody/tr[5]/td/table/tbody/tr[2]/td[4]")).getText();
		    	bookingList.setBCard_Activity_SubTotal(BCard_Activity_SubTotal);
		    		  		
		    	String BCard_Activity_Tax = driver.findElement(By.xpath(".//*[@id='activityDetailsId']/table/tbody/tr[5]/td/table/tbody/tr[3]/td[4]")).getText();
		    	bookingList.setBCard_Activity_Tax(BCard_Activity_Tax);	
		    	
//		    	String BCard_Activity_CreditFee = driver.findElement(By.xpath(".//*[@id='activityDetailsId']/table/tbody/tr[5]/td/table/tbody/tr[4]/td[4]")).getText();
//		    	bookingList.setBCard_Activity_CreditFee(BCard_Activity_CreditFee);	
		    		
		    	String BCard_Activity_TotalBookingValue = driver.findElement(By.xpath(".//*[@id='activityDetailsId']/table/tbody/tr[5]/td/table/tbody/tr[4]/td[4]")).getText();
		    	bookingList.setBCard_Activity_TotalBookingValue(BCard_Activity_TotalBookingValue);	
		    		
		    	String BCard_Activity_AmountChargeable = driver.findElement(By.xpath(".//*[@id='activityDetailsId']/table/tbody/tr[5]/td/table/tbody/tr[5]/td[4]")).getText();
		    	bookingList.setBCard_Activity_AmountChargeable(BCard_Activity_AmountChargeable);	
		    		
		    	/*String BCard_Activity_AmountPaid = driver.findElement(By.xpath(".//*[@id='activityDetailsId']/table/tbody/tr[5]/td/table/tbody/tr[6]/td[4]")).getText();
		    	bookingList.setBCard_Activity_AmountPaid(BCard_Activity_AmountPaid);	*/
		    		
		    	String BCard_Activity_AmountDue = driver.findElement(By.xpath(".//*[@id='activityDetailsId']/table/tbody/tr[5]/td/table/tbody/tr[6]/td[4]")).getText();
		    	bookingList.setBCard_Activity_AmountDue(BCard_Activity_AmountDue);
		    	
		        //////////////
		    	
		    	//Open a new window
				String parentHandle = driver.getWindowHandle();
			    driver.findElement(By.xpath(".//*[@id='activityDetailsId']/table/tbody/tr[1]/td/table/tbody/tr[1]/td[1]/a/u")).click();
			    Thread.sleep(2000);
			    
			    for (String winHandle : driver.getWindowHandles()) {
			        driver.switchTo().window(winHandle); 
			    }
			    
			    Thread.sleep(3000);
			    
			    List<String> values = Arrays.asList(driver.findElement(By.xpath(".//*[@id='dialogframetable']/tbody/tr[2]/td")).getText().split("If"));
		        
		        for (int i = 0; i < values.size(); i++) {
					           	       	
		        	if(i == 1){        		
		        		String firstString = values.get(i);
		        		firstString = "If" + firstString;
		        		cancelPolicy.add(firstString);
		        			       		
		        	}
		        	
		        	if(i > 1 && i < (values.size()-1)){
		        		String middleString = values.get(i);
		        		middleString = "If" + middleString;
		        		cancelPolicy.add(middleString);
		        	       	
		        	}
		        	
		        	if (i == (values.size()-1)) {        		
		        		String lastString = values.get(i);
		        		lastString = "If" + lastString;
		        		cancelPolicy.add(lastString);
		       
		        	}    
				}
		    	
		        bookingList.setCancelPolicy(cancelPolicy);
		        System.out.println(cancelPolicy);
			    	 
				Thread.sleep(3000);
				
			    driver.close(); // close newly opened window when done with it
			    driver.switchTo().window(parentHandle); // switch back to the original window
				
			    Thread.sleep(6000);
			    
			    
			    driver.switchTo().defaultContent();
				driver.switchTo().frame("dialogwindow");
					    
			    driver.findElement(By.xpath(".//*[@id='dialogwindowdragdrop']/td[3]/a/img")).click();
			    
			    driver.switchTo().defaultContent();
				driver.switchTo().frame("reportIframe");	
			    
			    if(UserTypeDC_B2B == TourOperatorType.NETCREDITLPON || UserTypeDC_B2B == TourOperatorType.NETCREDITLPOY || UserTypeDC_B2B == TourOperatorType.COMCREDITLPOY || UserTypeDC_B2B == TourOperatorType.COMCREDITLPONO){
			    	
			    	if (activitydetails.getPaymentDetails().equalsIgnoreCase("Pay Offline")) {
					    	    
				    	if (UserTypeDC_B2B == TourOperatorType.NETCREDITLPON || UserTypeDC_B2B == TourOperatorType.NETCREDITLPOY || UserTypeDC_B2B == TourOperatorType.COMCREDITLPOY || UserTypeDC_B2B == TourOperatorType.COMCREDITLPONO) {
				    		
				    		//Pay
				    		
							driver.findElement(By.xpath(".//*[@id='imgporange']")).click();
							Thread.sleep(5000);
							try {

								wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialogwindow")));
								driver.switchTo().frame("dialogwindow");
								Thread.sleep(1000);

								driver.findElement(By.xpath(".//*[@id='payReferNoArray[0]']")).sendKeys("2255889966");
								
								String totalAmount = null;
								
								if (UserTypeDC_B2B == TourOperatorType.NETCREDITLPON || UserTypeDC_B2B == TourOperatorType.NETCREDITLPOY) {
									totalAmount = activitydetails.getPaymentPageAmountbeingProcessed();
								} else {
									totalAmount = Integer.toString(Integer.parseInt(activitydetails.getPaymentPageAmountbeingProcessed()) - Integer.parseInt(activitydetails.getPaymentsPage_AgentCommission())) ;
								}
								
								driver.findElement(By.xpath(".//*[@id='paidAmountArray[0]']")).sendKeys(totalAmount);

								driver.findElement(By.xpath(".//*[@id='paymentNotes']")).sendKeys("Payment Notes");
								
								try {
									String balanceDue = driver.findElement(By.xpath(".//*[@id='dialogframetable']/tbody/tr[2]/td/form/table[2]/tbody/tr[1]/td[2]/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[6]/td")).getText().split(": ")[1].replaceAll(" ", "");
									bookingList.setBalanceDue(balanceDue);
								} catch (Exception e) {
									String balanceDue = "N/A";
									bookingList.setBalanceDue(balanceDue);
								}

								driver.findElement(By.xpath(".//*[@id='saveButId']")).click();
								Thread.sleep(5000);

							} catch (Exception e) {
								e.printStackTrace();
							}
							
						}
				    	
				    	
						driver.switchTo().defaultContent();
						driver.switchTo().frame("reportIframe");	
				    	
						//Issue Voucher
						
				    	driver.findElement(By.xpath(".//*[@id='reportData1']/tbody/tr[5]/td[22]/a/img")).click();
						Thread.sleep(5000);
						
						try {
							
							wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialogwindow")));
							driver.switchTo().frame("dialogwindow");
							Thread.sleep(1000);
							
							driver.findElement(By.xpath(".//*[@id='consultantChecked_Y']")).click();
							
							try {
								if (!(driver.findElement(By.xpath(".//*[@id='paymentGuaranteed']")).isSelected())){
									
								     driver.findElement(By.xpath(".//*[@id='paymentGuaranteed']")).click();
								}
							} catch (Exception e1) {
								e1.printStackTrace();
							}
							
							if (UserTypeDC_B2B == TourOperatorType.NETCREDITLPOY || UserTypeDC_B2B == TourOperatorType.COMCREDITLPOY ) {
								
								driver.findElement(By.xpath(".//*[@id='lpoNo']")).sendKeys("123456");
								bookingList.setBeforeLpoNo("123456");
							}
							
							Thread.sleep(1000);
							//save butt
							driver.findElement(By.xpath(".//*[@id='dialogframetable']/tbody/tr[2]/td/form/table[1]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr/td[3]/input")).click();
							
							driver.switchTo().defaultContent();
							driver.switchTo().frame("reportIframe");
							
							
							driver.findElement(By.xpath(".//*[@id='reportData1']/tbody/tr[5]/td[22]/a/img")).click();
							Thread.sleep(5000);
							
							wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialogwindow")));
							driver.switchTo().frame("dialogwindow");
							Thread.sleep(1000);
							
							//Issue Voucher Button
							driver.findElement(By.xpath(".//*[@id='dialogframetable']/tbody/tr[2]/td/form/table[1]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr/td[4]/a/img")).click();
							Thread.sleep(6000);   
							
							//back to BLR
							
							driver.switchTo().defaultContent();
							driver.switchTo().frame("reportIframe");
							
							wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")));
							driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
							Thread.sleep(1000);
							
							WebElement invElementPaid = driver.findElement(By.xpath(".//*[@id='reportData1']/tbody/tr[5]/td[15]"));
					        String inColorPaid = invElementPaid.findElement(By.tagName("img")).getAttribute("id");
					        WebElement payElementPaid = driver.findElement(By.xpath(".//*[@id='reportData1']/tbody/tr[5]/td[16]"));
					        String payColorPaid = payElementPaid.findElement(By.tagName("img")).getAttribute("id");
					        WebElement vouElementPaid = driver.findElement(By.xpath(".//*[@id='reportData1']/tbody/tr[5]/td[17]"));
					        String vouColorPaid = vouElementPaid.findElement(By.tagName("img")).getAttribute("id");
					        
					        if(vouColorPaid.contains("green"))
					        	bookingList.setPaid_Voucher_Issued(true);
					        else
					        	bookingList.setPaid_Voucher_Issued(false);
					        
					        if(inColorPaid.contains("green"))
					        	bookingList.setPaid_Invoice_Issued(true);
					        else
					        	bookingList.setPaid_Invoice_Issued(false);
					        
					        if(payColorPaid.contains("green"))
					        	bookingList.setPaid_Payment_Receieved(true);
					        else
					        	bookingList.setPaid_Payment_Receieved(false);
					        
					        
					       
					        driver.findElement(By.xpath(".//*[@id='reportData1']/tbody/tr[5]/td[1]/a")).click();
					        driver.switchTo().defaultContent();
							driver.switchTo().frame("dialogwindow");
					        
							try {			
						        	if (driver.findElements(By.id("agentdetailsid")).size() != 0) {
										
						        		String afterCreditBalance = driver.findElement(By.xpath(".//*[@id='agentdetailsid']/table/tbody/tr[2]/td[7]")).getText().replace(",", "").split("\\.")[0];
						        		bookingList.setAfterPayCreditBalance(afterCreditBalance);	
						        		
						        		String lpoNo = driver.findElement(By.xpath(".//*[@id='agentdetailsid']/table/tbody/tr[6]/td[4]")).getText();
						        		bookingList.setLpoNo(lpoNo);	
						        		
									}
						        	       	
							} catch (Exception e) {
									e.printStackTrace();
							}
					        
					   
						} catch (Exception e) {
							e.printStackTrace();
						}		  
			    		
					}
			    }
				
				
				
			}else{
				bookingList.setBookingListReportLoaded(false);
			}
		} catch (Exception e1) {
			bookingList.setBookingListReportLoaded(false);
		}
			
				
		return bookingList;
	}
	
	
	
	
}
