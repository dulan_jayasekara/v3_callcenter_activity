package bookinglistreport;

import java.util.ArrayList;

public class BookingList {

	public boolean isBookingListReportLoaded = true;
	public String BList_Reservation_Number ;
	public String BList_Reservation_Date ;
	public String BList_Reservation_Time ;
	public String BList_Customer_Type ;
	public String BList_Customer_Name ;
	public String BList_LeadGuest_Name ;
	public String BList_DateOf_FirstElement ;
	public String BList_DateOf_Cxl_Deadline ;
	public String BList_City_Name_Country ;
	public String BList_Product_Type ;
	public String BList_VerifiedBy;
	public String BList_Status;
	public boolean BList_Voucher_Issued;
	public boolean BList_Invoice_Issued;
	public boolean BList_Payment_Receieved;
	public String BList_PaymentType;
	private String balanceDue;
	
	public String BCard_Res_ReservationNumber;
	public String BCard_Res_Origin_City_Country;
	public String BCard_Res_Destination_City_Country;
	public String BCard_Res_Products_Booked;
	public String BCard_Res_Booking_Status;
	public String BCard_Res_FailedSector;
	public String BCard_Res_Reservation_Date;
	public String BCard_Res_FirstElement_Date;
	public String BCard_Res_Cancel_Deadline;
	public String BCard_Res_SubTotal;
	public String BCard_Res_BookingFee;
	public String BCard_Res_Tax_Other;
	public String BCard_Res_Credit_Fee;
	public String BCard_Res_Total_Booking_Fee;
	public String BCard_Res_Total_Amount_Payable;
	public String BCard_Res_Amount_Payable_CheckIn;
	public String BCard_Res_AmountPaid;
	public String BCard_Res_CurrencyType;
	
	public String BCard_Res_PaymentMethod = "";
	public String BCard_Res_PaymentReference = "";
	
	public boolean isAgentShows = false;
	public String BCard_Agent_AgentName;
	public String BCard_Agent_UserName;
	public String BCard_Agent_AgentType;
	public String BCard_Agent_CommissionPercentage;
	public String BCard_Agent_CommissionAmount;
	
	
	
	public String getBalanceDue() {
		return balanceDue;
	}
	public void setBalanceDue(String balanceDue) {
		this.balanceDue = balanceDue;
	}
	public boolean isBookingListReportLoaded() {
		return isBookingListReportLoaded;
	}
	public void setBookingListReportLoaded(boolean isBookingListReportLoaded) {
		this.isBookingListReportLoaded = isBookingListReportLoaded;
	}
	public String getBCard_Agent_CommissionPercentage() {
		return BCard_Agent_CommissionPercentage;
	}
	public void setBCard_Agent_CommissionPercentage(String bCard_Agent_CommissionPercentage) {
		BCard_Agent_CommissionPercentage = bCard_Agent_CommissionPercentage;
	}
	public String getBCard_Agent_CommissionAmount() {
		return BCard_Agent_CommissionAmount;
	}
	public void setBCard_Agent_CommissionAmount(String bCard_Agent_CommissionAmount) {
		BCard_Agent_CommissionAmount = bCard_Agent_CommissionAmount;
	}
	public String BCard_Guest_FName;
	public String BCard_Guest_Add;
	public String BCard_Guest_Email;
	public String BCard_Guest_City;
	public String BCard_Guest_Country;
	public String BCard_Guest_Emergency_Contact;
	public String BCard_Guest_PhoneNumber;
	
	public String BCard_Activity_ActivityName;
	public String BCard_Activity_DatesBookedFor;
	public String BCard_Activity_BookingStatus;
	public String BCard_Activity_OccupantNames;
	public String BCard_Activity_TotalRate ;
	public String BCard_Activity_TotalRateCurrrency;  
	public String BCard_Activity_qty;
	public String BCard_Activity_Currency;
	public String BCard_Activity_SubTotal;
	public String BCard_Activity_Tax;
	public String BCard_Activity_CreditFee;
	public String BCard_Activity_TotalBookingValue;
	public String BCard_Activity_AmountChargeable;
	
	public String BCard_Activity_AmountDue;
	
	public String lpoReq;
	public String lpoProvide;
	public String beforeCreditBalance;
	public String afterPayCreditBalance = "";
	public String lpoNo = "";
	public String beforeLpoNo = "";
	public boolean paid_Voucher_Issued;
	public boolean paid_Invoice_Issued;
	public boolean paid_Payment_Receieved;
	
	
	
	private ArrayList<String> cancelPolicy;
	
	
	
	
	public String getBeforeLpoNo() {
		return beforeLpoNo;
	}
	public void setBeforeLpoNo(String beforeLpoNo) {
		this.beforeLpoNo = beforeLpoNo;
	}
	public String getLpoNo() {
		return lpoNo;
	}
	public void setLpoNo(String lpoNo) {
		this.lpoNo = lpoNo;
	}
	public boolean isPaid_Voucher_Issued() {
		return paid_Voucher_Issued;
	}
	public void setPaid_Voucher_Issued(boolean paid_Voucher_Issued) {
		this.paid_Voucher_Issued = paid_Voucher_Issued;
	}
	public boolean isPaid_Invoice_Issued() {
		return paid_Invoice_Issued;
	}
	public void setPaid_Invoice_Issued(boolean paid_Invoice_Issued) {
		this.paid_Invoice_Issued = paid_Invoice_Issued;
	}
	public boolean isPaid_Payment_Receieved() {
		return paid_Payment_Receieved;
	}
	public void setPaid_Payment_Receieved(boolean paid_Payment_Receieved) {
		this.paid_Payment_Receieved = paid_Payment_Receieved;
	}
	public String getLpoReq() {
		return lpoReq;
	}
	public void setLpoReq(String lpoReq) {
		this.lpoReq = lpoReq;
	}
	public String getLpoProvide() {
		return lpoProvide;
	}
	public void setLpoProvide(String lpoProvide) {
		this.lpoProvide = lpoProvide;
	}
	public String getBeforeCreditBalance() {
		return beforeCreditBalance;
	}
	public void setBeforeCreditBalance(String beforeCreditBalance) {
		this.beforeCreditBalance = beforeCreditBalance;
	}
	public String getAfterPayCreditBalance() {
		return afterPayCreditBalance;
	}
	public void setAfterPayCreditBalance(String afterPayCreditBalance) {
		this.afterPayCreditBalance = afterPayCreditBalance;
	}
	public String getBCard_Res_PaymentMethod() {
		return BCard_Res_PaymentMethod;
	}
	public void setBCard_Res_PaymentMethod(String bCard_Res_PaymentMethod) {
		BCard_Res_PaymentMethod = bCard_Res_PaymentMethod;
	}
	public String getBCard_Res_PaymentReference() {
		return BCard_Res_PaymentReference;
	}
	public void setBCard_Res_PaymentReference(String bCard_Res_PaymentReference) {
		BCard_Res_PaymentReference = bCard_Res_PaymentReference;
	}
	public String getBList_PaymentType() {
		return BList_PaymentType;
	}
	public void setBList_PaymentType(String bList_PaymentType) {
		BList_PaymentType = bList_PaymentType;
	}
	public boolean isAgentShows() {
		return isAgentShows;
	}
	public void setAgentShows(boolean isAgentShows) {
		this.isAgentShows = isAgentShows;
	}
	public String getBCard_Agent_AgentName() {
		return BCard_Agent_AgentName;
	}
	public void setBCard_Agent_AgentName(String bCard_Agent_AgentName) {
		BCard_Agent_AgentName = bCard_Agent_AgentName;
	}
	public String getBCard_Agent_UserName() {
		return BCard_Agent_UserName;
	}
	public void setBCard_Agent_UserName(String bCard_Agent_UserName) {
		BCard_Agent_UserName = bCard_Agent_UserName;
	}
	public String getBCard_Agent_AgentType() {
		return BCard_Agent_AgentType;
	}
	public void setBCard_Agent_AgentType(String bCard_Agent_AgentType) {
		BCard_Agent_AgentType = bCard_Agent_AgentType;
	}
	public String getBCard_Activity_CreditFee() {
		return BCard_Activity_CreditFee;
	}
	public void setBCard_Activity_CreditFee(String bCard_Activity_CreditFee) {
		BCard_Activity_CreditFee = bCard_Activity_CreditFee;
	}
	public ArrayList<String> getCancelPolicy() {
		return cancelPolicy;
	}
	public void setCancelPolicy(ArrayList<String> cancelPolicy) {
		this.cancelPolicy = cancelPolicy;
	}
	public String getBList_Reservation_Number() {
		return BList_Reservation_Number;
	}
	public void setBList_Reservation_Number(String bList_Reservation_Number) {
		BList_Reservation_Number = bList_Reservation_Number;
	}
	public String getBList_Reservation_Date() {
		return BList_Reservation_Date;
	}
	public void setBList_Reservation_Date(String bList_Reservation_Date) {
		BList_Reservation_Date = bList_Reservation_Date;
	}
	public String getBList_Reservation_Time() {
		return BList_Reservation_Time;
	}
	public void setBList_Reservation_Time(String bList_Reservation_Time) {
		BList_Reservation_Time = bList_Reservation_Time;
	}
	public String getBList_Customer_Type() {
		return BList_Customer_Type;
	}
	public void setBList_Customer_Type(String bList_Customer_Type) {
		BList_Customer_Type = bList_Customer_Type;
	}
	public String getBList_Customer_Name() {
		return BList_Customer_Name;
	}
	public void setBList_Customer_Name(String bList_Customer_Name) {
		BList_Customer_Name = bList_Customer_Name;
	}
	public String getBList_LeadGuest_Name() {
		return BList_LeadGuest_Name;
	}
	public void setBList_LeadGuest_Name(String bList_LeadGuest_Name) {
		BList_LeadGuest_Name = bList_LeadGuest_Name;
	}
	public String getBList_DateOf_FirstElement() {
		return BList_DateOf_FirstElement;
	}
	public void setBList_DateOf_FirstElement(String bList_DateOf_FirstElement) {
		BList_DateOf_FirstElement = bList_DateOf_FirstElement;
	}
	public String getBList_DateOf_Cxl_Deadline() {
		return BList_DateOf_Cxl_Deadline;
	}
	public void setBList_DateOf_Cxl_Deadline(String bList_DateOf_Cxl_Deadline) {
		BList_DateOf_Cxl_Deadline = bList_DateOf_Cxl_Deadline;
	}
	public String getBList_City_Name_Country() {
		return BList_City_Name_Country;
	}
	public void setBList_City_Name_Country(String bList_City_Name_Country) {
		BList_City_Name_Country = bList_City_Name_Country;
	}
	public String getBList_Product_Type() {
		return BList_Product_Type;
	}
	public void setBList_Product_Type(String bList_Product_Type) {
		BList_Product_Type = bList_Product_Type;
	}
	public String getBList_VerifiedBy() {
		return BList_VerifiedBy;
	}
	public void setBList_VerifiedBy(String bList_VerifiedBy) {
		BList_VerifiedBy = bList_VerifiedBy;
	}
	public String getBList_Status() {
		return BList_Status;
	}
	public void setBList_Status(String bList_Status) {
		BList_Status = bList_Status;
	}
	public boolean isBList_Voucher_Issued() {
		return BList_Voucher_Issued;
	}
	public void setBList_Voucher_Issued(boolean bList_Voucher_Issued) {
		BList_Voucher_Issued = bList_Voucher_Issued;
	}
	public boolean isBList_Invoice_Issued() {
		return BList_Invoice_Issued;
	}
	public void setBList_Invoice_Issued(boolean bList_Invoice_Issued) {
		BList_Invoice_Issued = bList_Invoice_Issued;
	}
	public boolean isBList_Payment_Receieved() {
		return BList_Payment_Receieved;
	}
	public void setBList_Payment_Receieved(boolean bList_Payment_Receieved) {
		BList_Payment_Receieved = bList_Payment_Receieved;
	}
	public String getBCard_Res_ReservationNumber() {
		return BCard_Res_ReservationNumber;
	}
	public void setBCard_Res_ReservationNumber(String bCard_Res_ReservationNumber) {
		BCard_Res_ReservationNumber = bCard_Res_ReservationNumber;
	}
	public String getBCard_Res_Origin_City_Country() {
		return BCard_Res_Origin_City_Country;
	}
	public void setBCard_Res_Origin_City_Country(
			String bCard_Res_Origin_City_Country) {
		BCard_Res_Origin_City_Country = bCard_Res_Origin_City_Country;
	}
	public String getBCard_Res_Destination_City_Country() {
		return BCard_Res_Destination_City_Country;
	}
	public void setBCard_Res_Destination_City_Country(
			String bCard_Res_Destination_City_Country) {
		BCard_Res_Destination_City_Country = bCard_Res_Destination_City_Country;
	}
	public String getBCard_Res_Products_Booked() {
		return BCard_Res_Products_Booked;
	}
	public void setBCard_Res_Products_Booked(String bCard_Res_Products_Booked) {
		BCard_Res_Products_Booked = bCard_Res_Products_Booked;
	}
	public String getBCard_Res_Booking_Status() {
		return BCard_Res_Booking_Status;
	}
	public void setBCard_Res_Booking_Status(String bCard_Res_Booking_Status) {
		BCard_Res_Booking_Status = bCard_Res_Booking_Status;
	}
	public String getBCard_Res_FailedSector() {
		return BCard_Res_FailedSector;
	}
	public void setBCard_Res_FailedSector(String bCard_Res_FailedSector) {
		BCard_Res_FailedSector = bCard_Res_FailedSector;
	}
	public String getBCard_Res_Reservation_Date() {
		return BCard_Res_Reservation_Date;
	}
	public void setBCard_Res_Reservation_Date(String bCard_Res_Reservation_Date) {
		BCard_Res_Reservation_Date = bCard_Res_Reservation_Date;
	}
	public String getBCard_Res_FirstElement_Date() {
		return BCard_Res_FirstElement_Date;
	}
	public void setBCard_Res_FirstElement_Date(String bCard_Res_FirstElement_Date) {
		BCard_Res_FirstElement_Date = bCard_Res_FirstElement_Date;
	}
	public String getBCard_Res_Cancel_Deadline() {
		return BCard_Res_Cancel_Deadline;
	}
	public void setBCard_Res_Cancel_Deadline(String bCard_Res_Cancel_Deadline) {
		BCard_Res_Cancel_Deadline = bCard_Res_Cancel_Deadline;
	}
	public String getBCard_Res_SubTotal() {
		return BCard_Res_SubTotal;
	}
	public void setBCard_Res_SubTotal(String bCard_Res_SubTotal) {
		BCard_Res_SubTotal = bCard_Res_SubTotal;
	}
	public String getBCard_Res_BookingFee() {
		return BCard_Res_BookingFee;
	}
	public void setBCard_Res_BookingFee(String bCard_Res_BookingFee) {
		BCard_Res_BookingFee = bCard_Res_BookingFee;
	}
	public String getBCard_Res_Tax_Other() {
		return BCard_Res_Tax_Other;
	}
	public void setBCard_Res_Tax_Other(String bCard_Res_Tax_Other) {
		BCard_Res_Tax_Other = bCard_Res_Tax_Other;
	}
	public String getBCard_Res_Credit_Fee() {
		return BCard_Res_Credit_Fee;
	}
	public void setBCard_Res_Credit_Fee(String bCard_Res_Credit_Fee) {
		BCard_Res_Credit_Fee = bCard_Res_Credit_Fee;
	}
	public String getBCard_Res_Total_Booking_Fee() {
		return BCard_Res_Total_Booking_Fee;
	}
	public void setBCard_Res_Total_Booking_Fee(String bCard_Res_Total_Booking_Fee) {
		BCard_Res_Total_Booking_Fee = bCard_Res_Total_Booking_Fee;
	}
	public String getBCard_Res_Total_Amount_Payable() {
		return BCard_Res_Total_Amount_Payable;
	}
	public void setBCard_Res_Total_Amount_Payable(
			String bCard_Res_Total_Amount_Payable) {
		BCard_Res_Total_Amount_Payable = bCard_Res_Total_Amount_Payable;
	}
	public String getBCard_Res_Amount_Payable_CheckIn() {
		return BCard_Res_Amount_Payable_CheckIn;
	}
	public void setBCard_Res_Amount_Payable_CheckIn(
			String bCard_Res_Amount_Payable_CheckIn) {
		BCard_Res_Amount_Payable_CheckIn = bCard_Res_Amount_Payable_CheckIn;
	}
	public String getBCard_Res_AmountPaid() {
		return BCard_Res_AmountPaid;
	}
	public void setBCard_Res_AmountPaid(String bCard_Res_AmountPaid) {
		BCard_Res_AmountPaid = bCard_Res_AmountPaid;
	}
	public String getBCard_Res_CurrencyType() {
		return BCard_Res_CurrencyType;
	}
	public void setBCard_Res_CurrencyType(String bCard_Res_CurrencyType) {
		BCard_Res_CurrencyType = bCard_Res_CurrencyType;
	}
	public String getBCard_Guest_FName() {
		return BCard_Guest_FName;
	}
	public void setBCard_Guest_FName(String bCard_Guest_FName) {
		BCard_Guest_FName = bCard_Guest_FName;
	}
	public String getBCard_Guest_Add() {
		return BCard_Guest_Add;
	}
	public void setBCard_Guest_Add(String bCard_Guest_Add) {
		BCard_Guest_Add = bCard_Guest_Add;
	}
	public String getBCard_Guest_Email() {
		return BCard_Guest_Email;
	}
	public void setBCard_Guest_Email(String bCard_Guest_Email) {
		BCard_Guest_Email = bCard_Guest_Email;
	}
	public String getBCard_Guest_City() {
		return BCard_Guest_City;
	}
	public void setBCard_Guest_City(String bCard_Guest_City) {
		BCard_Guest_City = bCard_Guest_City;
	}
	public String getBCard_Guest_Country() {
		return BCard_Guest_Country;
	}
	public void setBCard_Guest_Country(String bCard_Guest_Country) {
		BCard_Guest_Country = bCard_Guest_Country;
	}
	public String getBCard_Guest_Emergency_Contact() {
		return BCard_Guest_Emergency_Contact;
	}
	public void setBCard_Guest_Emergency_Contact(
			String bCard_Guest_Emergency_Contact) {
		BCard_Guest_Emergency_Contact = bCard_Guest_Emergency_Contact;
	}
	public String getBCard_Guest_PhoneNumber() {
		return BCard_Guest_PhoneNumber;
	}
	public void setBCard_Guest_PhoneNumber(String bCard_Guest_PhoneNumber) {
		BCard_Guest_PhoneNumber = bCard_Guest_PhoneNumber;
	}
	public String getBCard_Activity_ActivityName() {
		return BCard_Activity_ActivityName;
	}
	public void setBCard_Activity_ActivityName(String bCard_Activity_ActivityName) {
		BCard_Activity_ActivityName = bCard_Activity_ActivityName;
	}
	public String getBCard_Activity_DatesBookedFor() {
		return BCard_Activity_DatesBookedFor;
	}
	public void setBCard_Activity_DatesBookedFor(
			String bCard_Activity_DatesBookedFor) {
		BCard_Activity_DatesBookedFor = bCard_Activity_DatesBookedFor;
	}
	public String getBCard_Activity_BookingStatus() {
		return BCard_Activity_BookingStatus;
	}
	public void setBCard_Activity_BookingStatus(String bCard_Activity_BookingStatus) {
		BCard_Activity_BookingStatus = bCard_Activity_BookingStatus;
	}
	public String getBCard_Activity_OccupantNames() {
		return BCard_Activity_OccupantNames;
	}
	public void setBCard_Activity_OccupantNames(String bCard_Activity_OccupantNames) {
		BCard_Activity_OccupantNames = bCard_Activity_OccupantNames;
	}
	public String getBCard_Activity_TotalRate() {
		return BCard_Activity_TotalRate;
	}
	public void setBCard_Activity_TotalRate(String bCard_Activity_TotalRate) {
		BCard_Activity_TotalRate = bCard_Activity_TotalRate;
	}
	public String getBCard_Activity_TotalRateCurrrency() {
		return BCard_Activity_TotalRateCurrrency;
	}
	public void setBCard_Activity_TotalRateCurrrency(
			String bCard_Activity_TotalRateCurrrency) {
		BCard_Activity_TotalRateCurrrency = bCard_Activity_TotalRateCurrrency;
	}
	public String getBCard_Activity_qty() {
		return BCard_Activity_qty;
	}
	public void setBCard_Activity_qty(String bCard_Activity_qty) {
		BCard_Activity_qty = bCard_Activity_qty;
	}
	public String getBCard_Activity_Currency() {
		return BCard_Activity_Currency;
	}
	public void setBCard_Activity_Currency(String bCard_Activity_Currency) {
		BCard_Activity_Currency = bCard_Activity_Currency;
	}
	public String getBCard_Activity_SubTotal() {
		return BCard_Activity_SubTotal;
	}
	public void setBCard_Activity_SubTotal(String bCard_Activity_SubTotal) {
		BCard_Activity_SubTotal = bCard_Activity_SubTotal;
	}
	public String getBCard_Activity_Tax() {
		return BCard_Activity_Tax;
	}
	public void setBCard_Activity_Tax(String bCard_Activity_Tax) {
		BCard_Activity_Tax = bCard_Activity_Tax;
	}
	public String getBCard_Activity_TotalBookingValue() {
		return BCard_Activity_TotalBookingValue;
	}
	public void setBCard_Activity_TotalBookingValue(
			String bCard_Activity_TotalBookingValue) {
		BCard_Activity_TotalBookingValue = bCard_Activity_TotalBookingValue;
	}
	public String getBCard_Activity_AmountChargeable() {
		return BCard_Activity_AmountChargeable;
	}
	public void setBCard_Activity_AmountChargeable(
			String bCard_Activity_AmountChargeable) {
		BCard_Activity_AmountChargeable = bCard_Activity_AmountChargeable;
	}
	
	public String getBCard_Activity_AmountDue() {
		return BCard_Activity_AmountDue;
	}
	public void setBCard_Activity_AmountDue(String bCard_Activity_AmountDue) {
		BCard_Activity_AmountDue = bCard_Activity_AmountDue;
	}
	
	
	
	
	
	
	
	
	
	
	
}
