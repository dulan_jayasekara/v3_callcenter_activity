package inventoryreport;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.controller.PG_Properties;
import com.model.ActivityDetails;
import com.model.Search;

public class InventoryInfo {
	
	private WebDriver driver;
	Search searchDetails = new Search();
	ArrayList<String> bookedIn = new ArrayList<String>();
	ArrayList<String> availableIn = new ArrayList<String>();
	Inventory inventoryDetails = new Inventory();
	
	public InventoryInfo(Search searchDetails, WebDriver Driver){
		
		this.driver = Driver;	
		this.searchDetails = searchDetails;
	}
	
	public Inventory getInventoryReport() throws InterruptedException, ParseException{
		
		WebDriverWait wait = new WebDriverWait(driver, 90);	
			
		String activityDate = searchDetails.getActivityDate();
		
		String actDate = activityDate.split("-")[0];
		String actMonth = activityDate.split("-")[1];
		String actYear = activityDate.split("-")[2];
		
		String activityName = searchDetails.getActivityName().split(" - ")[0];
		
		try {
			
			driver.get(PG_Properties.getProperty("Baseurl")+ "/reports/operational/mainReport.do?reportId=11&reportName=Inventory Report");
			Thread.sleep(2000);
			
			
			
			driver.switchTo().defaultContent();
			driver.switchTo().frame("reportIframe");	
			
			driver.findElement(By.id("programname")).clear();
			driver.findElement(By.id("programname")).sendKeys(activityName);
			driver.findElement(By.id("programname_lkup")).click();
			driver.switchTo().defaultContent();
			driver.switchTo().frame("lookup");
			Thread.sleep(1000);
			driver.findElement(By.xpath(".//*[@id='row-1']/td")).click();
			Thread.sleep(1000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame("reportIframe");
			
			new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("reportfrom_Day_ID"))).selectByVisibleText(actDate);
			new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("reportfrom_Month_ID"))).selectByVisibleText(actMonth);
			new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("reportfrom_Year_ID"))).selectByVisibleText(actYear);
			
			new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("reportto_Day_ID"))).selectByVisibleText(actDate);
			new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("reportto_Month_ID"))).selectByVisibleText(actMonth);
			new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("reportto_Year_ID"))).selectByVisibleText(actYear);
			
			driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
			Thread.sleep(1000);
			
			String activityDateRes = driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[1]")).getText();
			DateFormat dfTo = new SimpleDateFormat("dd-MMM-yyyy");
			SimpleDateFormat formatInTo = new SimpleDateFormat("dd-MMM-yy");
			Date instanceTo = formatInTo.parse(activityDateRes);  		
			String activityDatess = dfTo.format(instanceTo);	
			inventoryDetails.setActivityDate(activityDatess);
			
			String totalInventory = driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[9]")).getText().replace(" ", "");
			inventoryDetails.setTotalInventory(totalInventory);
			
			
		} catch (Exception e) {
			inventoryDetails.setActivityDate("Not Available");
			inventoryDetails.setTotalInventory("0");
		}
		
		
		
		
		return inventoryDetails;
	}
	
	public Inventory beforeInventoryCount(Inventory inventoryDetails){
		
		String 	bookedInventory;	
		String availableInventory;
		
		try {
			bookedInventory = driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[10]")).getText().replace(" ", "");
			availableInventory = driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[12]")).getText().replace(" ", "");
			
		} catch (Exception e) {
			bookedInventory = "0";
			availableInventory = "0";
		}
		
		bookedIn.add(bookedInventory);
		inventoryDetails.setBookedInventory(bookedIn);
		availableIn.add(availableInventory);
		inventoryDetails.setAvailableInventory(availableIn);
		
		
		return inventoryDetails;
	}
	

	public Inventory afterInventoryCount(Inventory inventoryDetails){
		
		String 	bookedInventory;	
		String availableInventory;
		
		try {
			bookedInventory = driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[10]")).getText().replace(" ", "");
			availableInventory = driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[12]")).getText().replace(" ", "");
			
		} catch (Exception e) {
			bookedInventory = "0";
			availableInventory = "0";
		}
		
	
		bookedIn.add(bookedInventory);
		inventoryDetails.setBookedInventory(bookedIn);
		availableIn.add(availableInventory);
		inventoryDetails.setAvailableInventory(availableIn);
		
		return inventoryDetails;
	}
	
}
