package inventoryreport;

import java.util.ArrayList;

public class Inventory {
	
	private String activityDate;
	private String totalInventory;
	public ArrayList<String> bookedInventory;
	public ArrayList<String> availableInventory;
	
	
	public String getTotalInventory() {
		return totalInventory;
	}
	public void setTotalInventory(String totalInventory) {
		this.totalInventory = totalInventory;
	}

	public ArrayList<String> getBookedInventory() {
		return bookedInventory;
	}
	public void setBookedInventory(ArrayList<String> bookedInventory) {
		this.bookedInventory = bookedInventory;
	}
	public ArrayList<String> getAvailableInventory() {
		return availableInventory;
	}
	public void setAvailableInventory(ArrayList<String> availableInventory) {
		this.availableInventory = availableInventory;
	}
	public String getActivityDate() {
		return activityDate;
	}
	public void setActivityDate(String activityDate) {
		this.activityDate = activityDate;
	}
	
	
	

}
