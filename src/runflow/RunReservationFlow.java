package runflow;

import inventoryreport.Inventory;
import inventoryreport.InventoryInfo;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import reservationreport.LoadReservations;
import reservationreport.Reservation;
import webbookingconfirmation.WEB_BookingConfirmation;
import webbookingconfirmation.WEB_LoadBookingConfirmationDetails;
import webbookingconfirmation.WEB_Quotation;
import webbookinglistreport.WEB_BookingList;
import webbookinglistreport.WEB_LoadBookingDetails;
import webcancellation_modification.WEB_Cancellation;
import webcancellation_modification.WEB_CancellationFlow;
import webcancellation_modification.WEB_Modification;
import webcancellation_modification.WEB_ModificationFlow;
import webcom.controller.WEB_ExcelReader;
import webcom.controller.WEB_LoadDetails;
import webcom.controller.WEB_ThirdLoadDetails;
import webcom.model.WEB_ActivityDetails;
import webcom.model.WEB_RateChecker;
import webcom.model.WEB_Search;
import webcom.model.WEB_ThirdSearch;
import webcom.reader.WEB_ReportsReader;
import webcom.reader.WEB_TORatesLogics;
import webcom.reader.WEB_ThirdWebReservationFlow;
import webcom.reader.WEB_WebReservationFlow;
import webreservationreport.WEB_LoadReservations;
import webreservationreport.WEB_Reservation;
import bookingconfirmation.BookingConfirmation;
import bookingconfirmation.LoadBookingConfirmationDetails;
import bookingconfirmation.Quotation;
import bookinglistreport.BookingList;
import bookinglistreport.LoadBookingDetails;
import cancellation_modification.Cancellation;
import cancellation_modification.CancellationFlow;
import cancellation_modification.Modification;
import cancellation_modification.ModificationFlow;

import com.controller.LoadDetails;
import com.controller.PG_Properties;
import com.model.ActivityDetails;
import com.model.ActivityInventoryRecords;
import com.model.PaymentDetails;
import com.model.RateChecker;
import com.model.Search;
import com.opera.core.systems.OperaDriver;
import com.reader.ExcelReader;
import com.reader.ReportsReader;
import com.reader.ReservationInfo;
import com.reader.TORatesLogics;


public class RunReservationFlow {
	
	private WebDriver driver         = null;
	private FirefoxProfile profile;
	private LoadDetails detailsLoder;	
	private RateChecker rateCheck;
	private ArrayList<Map<Integer, String>> Sheetlist; 	
	private ArrayList<Search>  	SearchTypes;
	private ArrayList<Search>  	SearchTypes1;
	private ArrayList<PaymentDetails>   PaymentDetailsTypes;
	private ArrayList<ActivityInventoryRecords>  	ActivityInventoryRecordTypes;	
	private StringBuffer PrintWriter;
	private int TestCaseCount;
	private int i=0;	
	private String currentDateforImages;	
	private WEB_LoadDetails web_loadDetails;
	private WEB_ThirdLoadDetails web_thirdLoadDetails;
	private StringBuffer  web_PrintWriter;
	private int web_TestCaseCount;
	private TreeMap<String, WEB_Search>   web_SearchList;
	private TreeMap<String, WEB_ThirdSearch>   web_SearchList_Third;	
	private int j=0;	
	private ArrayList<Map<Integer, String>> web_sheetlist, web_thirdPartySheetlist;	
	private Map<Integer, String> web_activitySearchInfoMap 		= null;
	private Map<Integer, String> web_activityPaymentsMap 		= null;
	private Map<Integer, String> web_activityInventoryMap 		= null;
	private Map<Integer, String> web_activitySearchInfoMap_Third 		= null;
	private Map<Integer, String> web_activityPaymentsMap_Third 			= null;
	private Map<Integer, String> web_activityInventoryMap_Third 		= null;
		
	
	@Before
	public void setUp() throws MalformedURLException{
		
		if (PG_Properties.getProperty("ReservationType").equalsIgnoreCase("CC")) {
			
			if (PG_Properties.getProperty("RunMode").equalsIgnoreCase("Local")) {
				profile 		= new FirefoxProfile(new File(PG_Properties.getProperty("Firefox.Profile")));
				driver 			= new FirefoxDriver(profile);
			
			} else {
				
				DesiredCapabilities capabilities = new DesiredCapabilities();
				
				try {
					if (PG_Properties.getProperty("Browser").equalsIgnoreCase("firefox")) {
						
						profile 		= new FirefoxProfile(new File(PG_Properties.getProperty("Firefox.Profile")));
						driver 			= new FirefoxDriver(profile);
					
					}else if(PG_Properties.getProperty("Browser").equalsIgnoreCase("chrome")){
						
						System.setProperty("webdriver.chrome.driver","D:\\chromedriver.exe");
						driver = new ChromeDriver();
						  				
					}else if(PG_Properties.getProperty("Browser").equalsIgnoreCase("ie")){
						
						try {
							System.setProperty("webdriver.ie.driver","D:\\IEDriverServer.exe");
							driver = new InternetExplorerDriver();
						
						} catch (Exception e) {					
							e.printStackTrace();
						}
							
					}else if(PG_Properties.getProperty("Browser").equalsIgnoreCase("safari")){
						
						try {
							driver = new SafariDriver();
							
						} catch (Exception e) {
							e.printStackTrace();
						}
						
										
					}else{				
					
						capabilities.setCapability("opera.binary", "C://Program Files (x86)//Opera//opera.exe");
						capabilities.setCapability("opera.log.level", "CONFIG");
						driver = new OperaDriver(capabilities);
						
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				
							
				try {
					capabilities.setCapability("name", "Selenium Test Example");
					capabilities.setCapability("build", "1.0");
					capabilities.setCapability("browser_api_name", ""+PG_Properties.getProperty("BrowserVersion")+"");
					capabilities.setCapability("os_api_name", ""+PG_Properties.getProperty("OSVersion")+"");
					capabilities.setCapability("screen_resolution", "1024x768");
					capabilities.setCapability("record_video", "true");
					capabilities.setCapability("record_network", "true");
					capabilities.setCapability("record_snapshot", "false");
					
					driver = new RemoteWebDriver(new URL("http://surani%40rezgateway.com:u6e72a2194362230@hub.crossbrowsertesting.com:80/wd/hub"), capabilities);
				} catch (Exception e) {
					
					e.printStackTrace();
				}

			}
			
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			
			Sheetlist 		= new ArrayList<Map<Integer, String>>(); 	
			SearchTypes  	= new ArrayList<Search>();
			SearchTypes1  	= new ArrayList<Search>();
			PaymentDetailsTypes				= new ArrayList<PaymentDetails>();
			ActivityInventoryRecordTypes  	= new ArrayList<ActivityInventoryRecords>();
			rateCheck 		= new RateChecker();
			
			
			//reading excel
			ExcelReader Reader = new ExcelReader();		
			Sheetlist            = Reader.init(PG_Properties.getProperty("ExcelPath.ActivityReservation"));
			detailsLoder         = new LoadDetails(Sheetlist);
			        
			SearchTypes   					= detailsLoder.loadActivityReservation();
			SearchTypes1   					= detailsLoder.loadActivityReservation();
			PaymentDetailsTypes 			= detailsLoder.loadPaymentDetails();
			ActivityInventoryRecordTypes 	= detailsLoder.loadActivityInventoryRecords();
							
			PrintWriter = new StringBuffer();
			
		} else {



			if (PG_Properties.getProperty("RunMode").equalsIgnoreCase("Local")) {
				profile 		= new FirefoxProfile(new File(PG_Properties.getProperty("Firefox.Profile")));
				driver 			= new FirefoxDriver(profile);
			
			} else {
				
				DesiredCapabilities capabilities = new DesiredCapabilities();
				
				if (PG_Properties.getProperty("Browser").equalsIgnoreCase("firefox")) {
					
					profile 		= new FirefoxProfile(new File(PG_Properties.getProperty("Firefox.Profile")));
					driver 			= new FirefoxDriver(profile);
				
				}else if(PG_Properties.getProperty("Browser").equalsIgnoreCase("chrome")){
					
					System.setProperty("webdriver.chrome.driver","D:\\chromedriver.exe");
					driver = new ChromeDriver();
					  				
				}else if(PG_Properties.getProperty("Browser").equalsIgnoreCase("ie")){
					
					try {
						System.setProperty("webdriver.ie.driver","D:\\IEDriverServer.exe");
						driver = new InternetExplorerDriver();
					
					} catch (Exception e) {					
						e.printStackTrace();
					}
						
				}else if(PG_Properties.getProperty("Browser").equalsIgnoreCase("safari")){
					
					driver = new SafariDriver();
									
				}else{				
				
					capabilities.setCapability("opera.binary", "C://Program Files (x86)//Opera//opera.exe");
					capabilities.setCapability("opera.log.level", "CONFIG");
					driver = new OperaDriver(capabilities);
					
				}
				
							
				try {
					
					capabilities.setCapability("name", "Selenium Test Example");
					capabilities.setCapability("build", "1.0");
					capabilities.setCapability("browser_api_name", ""+PG_Properties.getProperty("BrowserVersion")+"");
					capabilities.setCapability("os_api_name", ""+PG_Properties.getProperty("OSVersion")+"");
					capabilities.setCapability("screen_resolution", "1024x768");
					capabilities.setCapability("record_video", "true");
					capabilities.setCapability("record_network", "true");
					capabilities.setCapability("record_snapshot", "false");
					
					driver = new RemoteWebDriver(new URL("http://surani%40rezgateway.com:u6e72a2194362230@hub.crossbrowsertesting.com:80/wd/hub"), capabilities);
				
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
			
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			
			web_sheetlist = new ArrayList<Map<Integer, String>>();
			web_thirdPartySheetlist = new ArrayList<Map<Integer, String>>();
			web_PrintWriter = new StringBuffer();
			WEB_ExcelReader reader = new WEB_ExcelReader();	
			
			web_sheetlist 	= reader.init(PG_Properties.getProperty("WEB.ExcelPath.ActivityReservation"));
			web_thirdPartySheetlist = reader.init(PG_Properties.getProperty("Activity.ThirdParty"));
			
			web_loadDetails = new WEB_LoadDetails(web_sheetlist);
			web_thirdLoadDetails = new WEB_ThirdLoadDetails(web_thirdPartySheetlist);
		
			web_activitySearchInfoMap 	= web_sheetlist.get(0);
			web_activityPaymentsMap		= web_sheetlist.get(1);
			web_activityInventoryMap   	= web_sheetlist.get(2);
			
			web_activitySearchInfoMap_Third 	= web_thirdPartySheetlist.get(0);
			web_activityPaymentsMap_Third		= web_thirdPartySheetlist.get(1);
			web_activityInventoryMap_Third   	= web_thirdPartySheetlist.get(2);
			
			
					
			try {
				web_SearchList = web_loadDetails.loadActivityReservation(web_activitySearchInfoMap);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			try {
				web_SearchList = web_loadDetails.loadPaymentDetails(web_activityPaymentsMap, web_SearchList);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			try {
				web_SearchList = web_loadDetails.loadActivityInventoryRecords(web_activityInventoryMap, web_SearchList);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			//
			
			try {
				web_SearchList_Third = web_thirdLoadDetails.loadActivityReservation(web_activitySearchInfoMap_Third);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			try {
				web_SearchList_Third = web_thirdLoadDetails.loadPaymentDetails(web_activityPaymentsMap_Third, web_SearchList_Third);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			try {
				web_SearchList_Third = web_thirdLoadDetails.loadActivityInventoryRecords(web_activityInventoryMap_Third, web_SearchList_Third);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}		
						 		
	}
	
	
	@Test
	public void  runTest() throws InterruptedException, IOException, ParseException{
		
		Calendar cal1 = Calendar.getInstance();
		DateFormat dateFormatImages = new SimpleDateFormat("yyyy_MM_dd");
		currentDateforImages = dateFormatImages.format(cal1.getTime());
		
		if (PG_Properties.getProperty("ReservationType").equalsIgnoreCase("CC")) {
			
			DateFormat dateFormatcurrentDate = new SimpleDateFormat("dd-MMM-yyyy");
			Calendar cal = Calendar.getInstance();
			String currentDateforMatch = dateFormatcurrentDate.format(cal.getTime()); 
			
			PrintWriter.append("<html class='report'><head><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"> </head>");
			PrintWriter.append("<div class='header'><img height=\"19px\" src=\"menu_rez.png\"><p class='Hedding1'>Internal Activity Reservation Report - CC - ["+currentDateforMatch+"]</p></div>");
			PrintWriter.append("<body>");	
					
			PrintWriter.append("<br><br><table ><tr><th>Test Case No</th><th>Test Description</th><th>Expected</th><th>Actual Values</th><th>Test Status</th></tr>");
			TestCaseCount = 1;
			
			WebDriverWait wait = new WebDriverWait(driver, 120);	
			Iterator<Search>  ActivityReservationIterator  	= SearchTypes.iterator();
			Iterator<Search>  SearchIterator  				= SearchTypes1.iterator();
			Iterator<PaymentDetails>  PaymentDetailsIterator  				= PaymentDetailsTypes.iterator();
			Iterator<ActivityInventoryRecords>  ActivityInventoryIterator  	= ActivityInventoryRecordTypes.iterator();
			
			Map<Integer,ActivityInventoryRecords> inventoryMap=LoadDetails.convertIntoMap(ActivityInventoryRecordTypes);
			
			PrintWriter.append("<tr><td>"+TestCaseCount+"</td> <td>System Availability</td>");
			PrintWriter.append("<td>Should Be Available</td>");
			
			if (isCCPageLoaded(driver) == true) {
						
				PrintWriter.append("<td>Page Loaded..!!!!</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
				
				//login
				TestCaseCount++;
				PrintWriter.append("<tr><td>"+TestCaseCount+"</td> <td>Login Status</td>");
				PrintWriter.append("<td>Should login successfully</td>");
				
				if (login(driver) == true) {
					
					PrintWriter.append("<td>Success..!!!</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
					while(ActivityReservationIterator.hasNext())
					{						
						TestCaseCount++;
						PrintWriter.append("<tr><td>"+TestCaseCount+"</td> <td>Reservation flow for Activities</td>");
						PrintWriter.append("<td>Search No :- "+(TestCaseCount-2)+"</td>");
						i++;
						
						ActivityDetails activityDetails = new ActivityDetails();
						Reservation reservationdetails = new Reservation(); 				
						BookingConfirmation confirmationDetails = new BookingConfirmation();
						Modification modDetails = new Modification();
						Cancellation cDetails = new Cancellation();
						BookingList bookingList = new BookingList();
						Inventory inventoryDetails = new Inventory();
						Quotation quotationDetails = new Quotation();
										
						Map<String, String> currencyMap = rateCheck.getExchangeRates(driver);
						ActivityInventoryRecords inventory=null;
						Search search=ActivityReservationIterator.next();
						
					
						try {
																			
							int scenarioCount= Integer.parseInt(search.getScenarioCount());				
							System.out.println("Search scenario count : "+scenarioCount);
							System.out.println("changed");
							System.out.println("changed");
													
							if(inventoryMap.containsKey( scenarioCount)){
								
								inventory=inventoryMap.get(scenarioCount);
								System.out.println("inventory scenario Count : "+inventory.getScenarioCount()+"   search scenario count: "+scenarioCount);
								
							}else{
								System.out.println("====>>>Inventory not found for the search scenario count "+scenarioCount+"<<<========");
								
							}
									
							
							if(search.getBooking_channel().equals("CC")){
								
								TORatesLogics trLogics = new TORatesLogics(search, inventory);	
								ActivityDetails actDetails = trLogics.getTORatesCalculation();						
								InventoryInfo inventoryInfo = new InventoryInfo(search, driver);						
								inventoryDetails = inventoryInfo.getInventoryReport();
								inventoryInfo.beforeInventoryCount(inventoryDetails);
								
								//Search Engine
								ReservationInfo reservationInfo = new ReservationInfo(search, driver, inventory);
								reservationInfo.bookingEngine(driver, actDetails);							
								activityDetails = reservationInfo.searchResults(PaymentDetailsIterator.next(), ActivityInventoryIterator.next(), driver);
								
								if (activityDetails.isActivityResultsAvailble() == true) {
									
									LoadReservations loadReservations  = new LoadReservations(activityDetails, driver);						
									reservationdetails = loadReservations.getReservationReportDetails(driver);
									
									LoadBookingConfirmationDetails loadConfirmationDetails = new LoadBookingConfirmationDetails(activityDetails, driver);						
									confirmationDetails = loadConfirmationDetails.getConfirmationDetails(driver);
									
									ModificationFlow mFlow = new ModificationFlow(activityDetails, driver);						
									modDetails = mFlow.modificationFlow(driver);
									
									inventoryInfo.getInventoryReport();
									inventoryInfo.afterInventoryCount(inventoryDetails);
									
									LoadBookingDetails loadBookingDetails = new LoadBookingDetails(activityDetails, inventory, driver);						
									bookingList = loadBookingDetails.getBookingCardList(driver);
																
									CancellationFlow cFlow= new CancellationFlow(activityDetails, driver);					
									cDetails = cFlow.cancellationFlow(driver);
									
									if (search.getQuotaionReq().equalsIgnoreCase("yes")) {
										
										quotationDetails = loadConfirmationDetails.getQuotationMail(driver);
																			
									}
									
									ReportsReader reportsReader = new ReportsReader(activityDetails, inventory, currencyMap, SearchIterator.next(), bookingList, confirmationDetails, reservationdetails, inventoryDetails , cDetails, modDetails, quotationDetails, driver);				
									reportsReader.getActvivtyInventoryReport(driver);
									reportsReader.getcancellationModificationSummary(driver);
									
									PrintWriter.append("<td>Success >> Reservation No :- "+activityDetails.getReservationNo()+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");									
									
								}else{
									
									ReportsReader reportsReader = new ReportsReader(activityDetails, inventory, currencyMap, SearchIterator.next(), bookingList, confirmationDetails, reservationdetails, inventoryDetails , cDetails, modDetails, quotationDetails,driver);				
									reportsReader.getActvivtyInventoryReport(driver);
									reportsReader.getcancellationModificationSummary(driver);
									
									PrintWriter.append("<td>Results not Available</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
									
								}
															
							}
							
			 				
							
						} catch (Exception e) {
							System.out.println(e.getMessage());
							
							File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					        FileUtils.copyFile(scrFile, new File("ScreenShots/FailedImage_"+i+".png"));
					        
					        PrintWriter.append("<td>Error :- [[ "+e.toString()+" ]]</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							
							try {
								Alert alert = driver.switchTo().alert();
								alert.accept();	
							} catch (Exception e2) {
								 System.out.println("alert was not present");
							}
							
							ReportsReader reportsReader = new ReportsReader(activityDetails, inventory, currencyMap, SearchIterator.next(), bookingList, confirmationDetails, reservationdetails, inventoryDetails , cDetails, modDetails, quotationDetails, driver);				
							reportsReader.getActvivtyInventoryReport(driver);
							reportsReader.getcancellationModificationSummary(driver);
																			
																					
						}
						
						openNewBrowser();
							
					}
					
							
				} else {
					
					PrintWriter.append("<td>Login Errorrr..!!!!</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					
				}
						
					
			}else{
				
				PrintWriter.append("<td>Not Available</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");				
			}
			
			
			PrintWriter.append("</table>");
		
		
		} else {
		
			DateFormat dateFormatcurrentDate = new SimpleDateFormat("dd-MMM-yyyy");
			Calendar cal = Calendar.getInstance();
			String currentDateforMatch = dateFormatcurrentDate.format(cal.getTime());
			WEB_WebReservationFlow webreservationFlow = new WEB_WebReservationFlow(driver);
			WEB_ThirdWebReservationFlow thirdWebReservationFlow = new WEB_ThirdWebReservationFlow(driver);
			WEB_RateChecker rateCheck = new WEB_RateChecker();
			
			web_PrintWriter.append("<html class='report'><head><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"> </head>");
			web_PrintWriter.append("<div class='header'><img height=\"19px\" src=\"menu_rez.png\"><p class='Hedding1'>Internal Activity Reservation Report - WEB - ["+currentDateforMatch+"]</p></div>");
			web_PrintWriter.append("<body>");		
			web_PrintWriter.append("<br><br>");
			
			web_TestCaseCount = 1;
			web_PrintWriter.append("<br><br><table ><tr><th>Test Case No</th><th>Test Description</th><th>Expected Results</th><th>Actual Results</th> <th>Test Status</th></tr>");
					
			Iterator<Map.Entry<String, WEB_Search>> it = (Iterator<Entry<String, WEB_Search>>) web_SearchList.entrySet().iterator();
			Iterator<Map.Entry<String, WEB_ThirdSearch>> it_Third = (Iterator<Entry<String, WEB_ThirdSearch>>) web_SearchList_Third.entrySet().iterator();
			
			web_PrintWriter.append("<tr><td>"+web_TestCaseCount+"</td> <td>System Availability</td>");
			web_PrintWriter.append("<td>Should Be Loaded</td>");
			
			if (isCCPageLoaded(driver) == true) {
				
				web_PrintWriter.append("<td>Page Loaded..!!!!</td>");
				web_PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
				
				//Search scenarios for Internal
				if (PG_Properties.getProperty("ResultsType").equalsIgnoreCase("Internal")) {
									
					while(it.hasNext()){
						
						WEB_Search searchInfo  = it.next().getValue();
						webreservationFlow.addSearchScenario(searchInfo);
						WEB_Reservation reservationdetails = new WEB_Reservation();
						WEB_BookingConfirmation confirmationDetails = new WEB_BookingConfirmation();
						WEB_Modification modDetails = new WEB_Modification();
						WEB_BookingList bookingList = new WEB_BookingList();
						Map<String, String> currencyMap = new HashMap<String, String>();
						WEB_Cancellation cDetails = new WEB_Cancellation();
						WEB_ActivityDetails activityDetails = new WEB_ActivityDetails();
						WEB_Quotation quotationDetails = new WEB_Quotation();
						j++;
								
						try {
							
							WEB_TORatesLogics trLogics = new WEB_TORatesLogics(searchInfo);	
							WEB_ActivityDetails actDetails = trLogics.getTORatesCalculation();	
							driver = webreservationFlow.searchEngine(driver, actDetails);
							activityDetails = webreservationFlow.searchResults(driver);
							
							web_TestCaseCount++;
							web_PrintWriter.append("<tr><td>"+web_TestCaseCount+"</td> <td>Login Status</td>");
							web_PrintWriter.append("<td>Should login successfully</td>");
											
							if (login(driver) == true){
								
								web_PrintWriter.append("<td>Success..!!!</td>");
								web_PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
								
								web_TestCaseCount++;
								web_PrintWriter.append("<tr><td>"+web_TestCaseCount+"</td> <td>Activity WEB Reservations</td>");
								web_PrintWriter.append("<td>Reservation should be made</td>");
								
//								ModificationFlow mFlow = new ModificationFlow("A5443W100615", driver);					
//								modDetails = mFlow.modificationFlow(driver);
								
//								LoadBookingDetails loadBookingDetails = new LoadBookingDetails("A0042W091015", searchInfo, driver);					
//								bookingList = loadBookingDetails.getBookingCardList(driver);
							
								if (activityDetails.isResultsAvailable() == true) {
									
									if (searchInfo.getQuotationReq().equalsIgnoreCase("Yes")) {
										
										currencyMap = rateCheck.getExchangeRates(driver);
										quotationDetails = webreservationFlow.getQuotationMail(driver);
										
										WEB_ReportsReader reportsReader = new WEB_ReportsReader(activityDetails, quotationDetails, currencyMap, searchInfo, bookingList, confirmationDetails, reservationdetails, cDetails, modDetails ,driver);				
										reportsReader.getActvivtyInventoryReport(driver);
										reportsReader.getcancellationModificationSummary(driver);
										
										web_PrintWriter.append("<td>ACTIVITY >> "+searchInfo.getActivityName()+" -> Quotation No ["+activityDetails.getQuote_QuotationNo()+"] :- Done</td>");
										web_PrintWriter.append("<td class='Passed'>PASS</td><tr>");
										
										
									} else {
										
										currencyMap = rateCheck.getExchangeRates(driver);
										
										WEB_LoadReservations loadReservations  = new WEB_LoadReservations(activityDetails, driver);					
										reservationdetails = loadReservations.getReservationReportDetails(driver);
										
										WEB_LoadBookingConfirmationDetails loadConfirmationDetails = new WEB_LoadBookingConfirmationDetails(activityDetails, driver);					
										confirmationDetails = loadConfirmationDetails.getConfirmationDetails(driver);
										
										WEB_ModificationFlow mFlow = new WEB_ModificationFlow(activityDetails, driver);					
										modDetails = mFlow.modificationFlow(driver);
										
										WEB_LoadBookingDetails loadBookingDetails = new WEB_LoadBookingDetails(activityDetails, searchInfo, driver);					
										bookingList = loadBookingDetails.getBookingCardList(driver);
										
										WEB_CancellationFlow cFlow= new WEB_CancellationFlow(activityDetails, driver);					
										cDetails = cFlow.cancellationFlow(driver);
										
										WEB_ReportsReader reportsReader = new WEB_ReportsReader(activityDetails, quotationDetails, currencyMap, searchInfo, bookingList, confirmationDetails, reservationdetails, cDetails, modDetails ,driver);				
										reportsReader.getActvivtyInventoryReport(driver);
										reportsReader.getcancellationModificationSummary(driver);
										
										web_PrintWriter.append("<td>ACTIVITY >> "+searchInfo.getActivityName()+" -> Reservation No ["+activityDetails.getReservationNo()+"] :- Done</td>");
										web_PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
									}
																
								}else{
									
									WEB_ReportsReader reportsReader = new WEB_ReportsReader(activityDetails, quotationDetails, currencyMap, searchInfo, bookingList, confirmationDetails, reservationdetails, cDetails, modDetails ,driver);				
									reportsReader.getActvivtyInventoryReport(driver);
									reportsReader.getcancellationModificationSummary(driver);
									
									web_PrintWriter.append("<td>ACTIVITY >> No Results Available</td>");
									web_PrintWriter.append("<td class='Failed'>FAIL</td><tr>");		
									
								}				
								
							}else{
								
								web_PrintWriter.append("<td>Login Errorrr..!!!!</td>");
								web_PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
						
						
						} catch (Exception e) {
							e.printStackTrace();
					        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					        FileUtils.copyFile(scrFile, new File("ScreenShots/FailedImage_"+j+".png"));
					        
					        web_PrintWriter.append("<td>Error :- [["+e.toString()+"]]</td>");
					        web_PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
							
							WEB_ReportsReader reportsReader = new WEB_ReportsReader(activityDetails, quotationDetails, currencyMap, searchInfo, bookingList, confirmationDetails, reservationdetails, cDetails, modDetails ,driver);				
							reportsReader.getActvivtyInventoryReport(driver);
							reportsReader.getcancellationModificationSummary(driver);
							
							
						}	
					}
					
				}
				
				/*
				//Search scenarios for ThirdParty
				if (PG_Properties.getProperty("ResultsType").equalsIgnoreCase("ThirdParty")) {
									
					while(it_Third.hasNext()){
						
						ThirdSearch searchInfo  = it_Third.next().getValue();
						thirdWebReservationFlow.addSearchScenario(searchInfo);					
						i++;
								
						try {
							
							Reservation reservationdetails = new Reservation();
							BookingConfirmation confirmationDetails = new BookingConfirmation();
							Modification modDetails = new Modification();
							BookingList bookingList = new BookingList();
							Map<String, String> currencyMap = new HashMap<String, String>();
							Cancellation cDetails = new Cancellation();
							CancellationPolicyResponse cancelPolicy_Response = new CancellationPolicyResponse();
							
							driver = thirdWebReservationFlow.searchEngine(driver);
							ActivityDetails activityDetails = thirdWebReservationFlow.searchResults(driver);
							
							TestCaseCount++;
							PrintWriter.append("<tr><td>"+TestCaseCount+"</td> <td>Login Status</td>");
							PrintWriter.append("<td>Should login successfully</td>");
											
							if (login(driver) == true){
								
								PrintWriter.append("<td>Success..!!!</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
								
								TestCaseCount++;
								PrintWriter.append("<tr><td>"+TestCaseCount+"</td> <td>Activity WEB Reservations</td>");
								PrintWriter.append("<td>Reservation should be made</td>");
							
								if (activityDetails.isResultsAvailable() == true) {
									
									currencyMap = rateCheck.getExchangeRates(driver);
									
									LoadReservations loadReservations  = new LoadReservations(activityDetails, driver);					
									reservationdetails = loadReservations.getReservationReportDetails(driver);
									
									LoadBookingConfirmationDetails loadConfirmationDetails = new LoadBookingConfirmationDetails(activityDetails, driver);					
									confirmationDetails = loadConfirmationDetails.getConfirmationDetails(driver);
									
									ModificationFlow mFlow = new ModificationFlow(activityDetails, driver);					
									modDetails = mFlow.modificationFlow(driver);
									
									LoadBookingDetails loadBookingDetails = new LoadBookingDetails(activityDetails, driver);					
									bookingList = loadBookingDetails.getBookingCardList(driver);
									
									CancellationFlow cFlow= new CancellationFlow(activityDetails, driver);					
									cDetails = cFlow.cancellationFlow(driver);
																									
									if (PG_Properties.getProperty("SupplierType").equals("GTA")) {
										
										GTAReader gtaReader = new GTAReader(activityDetails, searchInfo, currencyMap, driver);
										gtaReader.readAvailabilityRequests();
										gtaReader.readAvailabilityResponses();
										gtaReader.getGTAAvailabilityReport(searchInfo , driver);
										
										gtaReader.readProgramRequest();
										gtaReader.readProgramResponse();		
										gtaReader.getGTAProgramReport(searchInfo, driver);
										
										gtaReader.readCancellationPolicyRequests();
										cancelPolicy_Response = gtaReader.readCancellationPolicyResponse();
										gtaReader.getGTACancellationPolicyReport(searchInfo, driver);
										
										gtaReader.readReservationRequest();
										gtaReader.readReservationResponse();
										gtaReader.getGTAReservationReport(searchInfo, driver);
																				
									}
									
									ThirdReportsReader reportsReader = new ThirdReportsReader(activityDetails, currencyMap, searchInfo, bookingList, confirmationDetails, reservationdetails, cDetails, modDetails , cancelPolicy_Response, driver);				
									reportsReader.getActvivtyInventoryReport(driver);
									reportsReader.getcancellationModificationSummary(driver);
									
									PrintWriter.append("<td>ACTIVITY >> "+searchInfo.getActivityName()+" -> Reservation No ["+activityDetails.getReservationNo()+"] :- Done</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
									
									
									
																
								}else{
									
									ThirdReportsReader reportsReader = new ThirdReportsReader(activityDetails, currencyMap, searchInfo, bookingList, confirmationDetails, reservationdetails, cDetails, modDetails , cancelPolicy_Response, driver);				
									reportsReader.getActvivtyInventoryReport(driver);
									reportsReader.getcancellationModificationSummary(driver);
									
									PrintWriter.append("<td>ACTIVITY >> No Results Available</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");		
									
								}					
								
							}else{
								
								PrintWriter.append("<td>Login Errorrr..!!!!</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
							
						
						} catch (Exception e) {
							e.printStackTrace();
					        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					        FileUtils.copyFile(scrFile, new File("ScreenShots/FailedImage_"+i+".png"));
					        
							PrintWriter.append("<td>Error :- [["+e.toString()+"]]</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
							
						}	
					}
					
				}
				*/
				
						
			}else{
				
				web_PrintWriter.append("<td>Page is Not Available</td>");
				web_PrintWriter.append("<td class='Failed'>FAIL</td><tr>");			
			}
			
			
			
			
			web_PrintWriter.append("</table>");
		
			
		}
		
		
		
		
	}
	
	
	
	@After
	public void tearDown() throws IOException{
		
		if (PG_Properties.getProperty("ReservationType").equalsIgnoreCase("CC")) {
			
			PrintWriter.append("</body></html>");			  
			BufferedWriter bwr = new BufferedWriter(new FileWriter(new File(PG_Properties.getProperty("HtmlReport.ActivityReservation"))));
			bwr.write(PrintWriter.toString());
			bwr.flush();
			bwr.close();
		    driver.quit();
			
		}else{
			
			web_PrintWriter.append("</body></html>");
			BufferedWriter bwr = new BufferedWriter(new FileWriter(new File(PG_Properties.getProperty("ActivityInternalReport"))));
			bwr.write(web_PrintWriter.toString());
			bwr.flush();
			bwr.close();
		    driver.quit();
		}
		
		
	}
	
	public boolean isCCPageLoaded(WebDriver driver_1){
		
		driver = driver_1;
		driver.get(PG_Properties.getProperty("Baseurl") + "/admin/common/LoginPage.do");
		
		try {			
			WebElement idElement = driver.findElement(By.id("user_id"));
			idElement.isDisplayed();
			return true;
			
		} catch (Exception e) {
			return false;
		}		
	}
	
	
	public boolean login(WebDriver driver_1) throws IOException{

		driver = driver_1;
				
		WebDriverWait wait = new WebDriverWait(driver, 120);
		driver.get(PG_Properties.getProperty("Baseurl") + "/admin/common/LoginPage.do");
		driver.findElement(By.id("user_id")).clear();
		driver.findElement(By.id("user_id")).sendKeys(PG_Properties.getProperty("DC_UserName"));
		driver.findElement(By.id("password")).clear();
		driver.findElement(By.id("password")).sendKeys(PG_Properties.getProperty("DC_Password"));
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File(""+currentDateforImages+"/LoginPage.png"));     
		driver.findElement(By.id("loginbutton")).click();
		
		try {			
			driver.findElement(By.id("mainmenurezglogo"));
			return true;
		} catch (Exception e) {
			System.out.println("Login Fail...");
			return false;
		}		
	}
	
	
	
	
	public void openNewBrowser() throws IOException{
		
		String parentHandle = driver.getWindowHandle();
		driver.switchTo().window(parentHandle);
		driver.close();	
		profile = new FirefoxProfile(new File(PG_Properties.getProperty("Firefox.Profile")));
		driver = new FirefoxDriver(profile);
		isCCPageLoaded(driver);
		login(driver);
		
	}
	
}
