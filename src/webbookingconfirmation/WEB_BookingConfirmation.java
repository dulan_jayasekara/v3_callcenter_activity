package webbookingconfirmation;

import java.util.ArrayList;

public class WEB_BookingConfirmation {

	private boolean isCustomerConfirmationMailLoaded  = true;
	private boolean isCustomerVoucherMailLoaded  = true;
	private boolean isSupplier  = true;
	
	public String ActivityVaoucherRefNo;
	public String CCE_BookingRef;
	public String CCE_BookingNo;
	public String CCE_ActivityType;
	public String CCE_BookingStatus;
	public String CCE_Period;
	public String CCE_RateType;
	public String CCE_Rate;
	public String CCE_QTY;
	public String CCE_TotalValue;
	public String CCE_TotalValue_Currency;
	
	public String CCE_CurrencyType_1;
	public String CCE_SubTotal;
	public String CCE_TotalTaxOther_1;
	public String CCE_TotalBookingValue;
	public String CCE_AmountPayableNow;
	public String CCE_AmountDue;
	
	public String CCE_CurrencyType_2;
	public String CCE_SubTotal_2;
	public String CCE_TotalTaxOther_2;
	
	public String CCE_TotalBookingValue_3;
	public String CCE_TotalBookingValue_Currency;
	public String CCE_AmountPocessed;
	public String CCE_AmountDue_2;
	
	public String CCE_FName;
	public String CCE_LName;
	public String CCE_TP;
	public String CCE_Email;
	public String CCE_address;
	public String CCE_address_2;
	public String CCE_country;
	public String CCE_city;
	
	public ArrayList<String> CCE_CusTitle;
	public ArrayList<String> CCE_CusFName;
	public ArrayList<String> CCE_CusLName;
	
	public String CCE_Occupancy_ActivityName;
	public String CCE_Occupancy_FName;
	public String CCE_Occupancy_LName;
	
	public String CCE_PaymentTotalValue;
	
	public ArrayList<String> CanellationPolicy;
	public int cancelPolicyCount;
	
	public String CCE_ActivityNote;
	public String CCE_CustomerNote;
	
	public String CCE_Tel_1;
	public String CCE_Tel_2;
	public String CCE_Email_1;
	public String CCE_Email_2;
	public String CCE_Website;
	public String CCE_Fax;
	public String CCE_CompanyName;
	
	
	public String CVE_LeadPassenger;
	public String CVE_IssueDate;
	public String CVE_Address1;
	public String CVE_City;
	public String CVE_BookingNo;
	
	public String CVE_ActivityName;
	public String CVE_bookingStatus;
	public String CVE_SupplierName;
	public String CVE_SupplierAdd;
	public String CVE_SupplierTP;
	public String CVE_ProgramCity;
	
	public String CVE_ActivityTransferDes;
	public String CVE_Duration;
	public String CVE_RatePlan;
	public String CVE_Qty;
	public String CVE_ServiceDate;
	
	public String CVE_CustomerNotes;
	
	public String CVE_Tel_1;
	public String CVE_Tel_2;
	public String CVE_Email_1;
	public String CVE_Email_2;
	public String CVE_Website;
	public String CVE_Fax;
	public String CVE_CompanyName;
	
	public String SupplierMAIL_BookingNo;
	public String SupplierMAIL_IssueDate;
	public String SupplierMAIL_ActivityName;
	public String SupplierMAIL_BookingStatus;
	public String SupplierMAIL_ProgramCity;
	public String SupplierMAIL_Duration;
	public String SupplierMAIL_QTY;
	public String SupplierMAIL_ServicyDate;
	public String SupplierMAIL_ActivityRate;
	public String SupplierMAIL_CustomerTP;
	public String SupplierMAIL_PassengerName;
	public String SupplierMAIL_Tel_1;
	public String SupplierMAIL_Tel_2;
	public String SupplierMAIL_Email_1;
	public String SupplierMAIL_Email_2;
	public String SupplierMAIL_Website;
	public String SupplierMAIL_Fax;
	public String SupplierMAIL_CompanyName;
	
	
	
	
	public String getSupplierMAIL_PassengerName() {
		return SupplierMAIL_PassengerName;
	}
	public void setSupplierMAIL_PassengerName(String supplierMAIL_PassengerName) {
		SupplierMAIL_PassengerName = supplierMAIL_PassengerName;
	}
	public boolean isCustomerConfirmationMailLoaded() {
		return isCustomerConfirmationMailLoaded;
	}
	public void setCustomerConfirmationMailLoaded(
			boolean isCustomerConfirmationMailLoaded) {
		this.isCustomerConfirmationMailLoaded = isCustomerConfirmationMailLoaded;
	}
	public boolean isCustomerVoucherMailLoaded() {
		return isCustomerVoucherMailLoaded;
	}
	public void setCustomerVoucherMailLoaded(boolean isCustomerVoucherMailLoaded) {
		this.isCustomerVoucherMailLoaded = isCustomerVoucherMailLoaded;
	}
	public boolean isSupplier() {
		return isSupplier;
	}
	public void setSupplier(boolean isSupplier) {
		this.isSupplier = isSupplier;
	}
	public String getCCE_Tel_1() {
		return CCE_Tel_1;
	}
	public void setCCE_Tel_1(String cCE_Tel_1) {
		CCE_Tel_1 = cCE_Tel_1;
	}
	public String getCCE_Tel_2() {
		return CCE_Tel_2;
	}
	public void setCCE_Tel_2(String cCE_Tel_2) {
		CCE_Tel_2 = cCE_Tel_2;
	}
	public String getCCE_Email_1() {
		return CCE_Email_1;
	}
	public void setCCE_Email_1(String cCE_Email_1) {
		CCE_Email_1 = cCE_Email_1;
	}
	public String getCCE_Email_2() {
		return CCE_Email_2;
	}
	public void setCCE_Email_2(String cCE_Email_2) {
		CCE_Email_2 = cCE_Email_2;
	}
	public String getCCE_Website() {
		return CCE_Website;
	}
	public void setCCE_Website(String cCE_Website) {
		CCE_Website = cCE_Website;
	}
	public String getCCE_Fax() {
		return CCE_Fax;
	}
	public void setCCE_Fax(String cCE_Fax) {
		CCE_Fax = cCE_Fax;
	}
	public String getCCE_CompanyName() {
		return CCE_CompanyName;
	}
	public void setCCE_CompanyName(String cCE_CompanyName) {
		CCE_CompanyName = cCE_CompanyName;
	}
	public String getCVE_Tel_1() {
		return CVE_Tel_1;
	}
	public void setCVE_Tel_1(String cVE_Tel_1) {
		CVE_Tel_1 = cVE_Tel_1;
	}
	public String getCVE_Tel_2() {
		return CVE_Tel_2;
	}
	public void setCVE_Tel_2(String cVE_Tel_2) {
		CVE_Tel_2 = cVE_Tel_2;
	}
	public String getCVE_Email_1() {
		return CVE_Email_1;
	}
	public void setCVE_Email_1(String cVE_Email_1) {
		CVE_Email_1 = cVE_Email_1;
	}
	public String getCVE_Email_2() {
		return CVE_Email_2;
	}
	public void setCVE_Email_2(String cVE_Email_2) {
		CVE_Email_2 = cVE_Email_2;
	}
	public String getCVE_Website() {
		return CVE_Website;
	}
	public void setCVE_Website(String cVE_Website) {
		CVE_Website = cVE_Website;
	}
	public String getCVE_Fax() {
		return CVE_Fax;
	}
	public void setCVE_Fax(String cVE_Fax) {
		CVE_Fax = cVE_Fax;
	}
	public String getCVE_CompanyName() {
		return CVE_CompanyName;
	}
	public void setCVE_CompanyName(String cVE_CompanyName) {
		CVE_CompanyName = cVE_CompanyName;
	}
	public String getSupplierMAIL_Tel_1() {
		return SupplierMAIL_Tel_1;
	}
	public void setSupplierMAIL_Tel_1(String supplierMAIL_Tel_1) {
		SupplierMAIL_Tel_1 = supplierMAIL_Tel_1;
	}
	public String getSupplierMAIL_Tel_2() {
		return SupplierMAIL_Tel_2;
	}
	public void setSupplierMAIL_Tel_2(String supplierMAIL_Tel_2) {
		SupplierMAIL_Tel_2 = supplierMAIL_Tel_2;
	}
	public String getSupplierMAIL_Email_1() {
		return SupplierMAIL_Email_1;
	}
	public void setSupplierMAIL_Email_1(String supplierMAIL_Email_1) {
		SupplierMAIL_Email_1 = supplierMAIL_Email_1;
	}
	public String getSupplierMAIL_Email_2() {
		return SupplierMAIL_Email_2;
	}
	public void setSupplierMAIL_Email_2(String supplierMAIL_Email_2) {
		SupplierMAIL_Email_2 = supplierMAIL_Email_2;
	}
	public String getSupplierMAIL_Website() {
		return SupplierMAIL_Website;
	}
	public void setSupplierMAIL_Website(String supplierMAIL_Website) {
		SupplierMAIL_Website = supplierMAIL_Website;
	}
	public String getSupplierMAIL_Fax() {
		return SupplierMAIL_Fax;
	}
	public void setSupplierMAIL_Fax(String supplierMAIL_Fax) {
		SupplierMAIL_Fax = supplierMAIL_Fax;
	}
	public String getSupplierMAIL_CompanyName() {
		return SupplierMAIL_CompanyName;
	}
	public void setSupplierMAIL_CompanyName(String supplierMAIL_CompanyName) {
		SupplierMAIL_CompanyName = supplierMAIL_CompanyName;
	}
	public ArrayList<String> getCCE_CusTitle() {
		return CCE_CusTitle;
	}
	public void setCCE_CusTitle(ArrayList<String> cCE_CusTitle) {
		CCE_CusTitle = cCE_CusTitle;
	}
	public ArrayList<String> getCCE_CusFName() {
		return CCE_CusFName;
	}
	public void setCCE_CusFName(ArrayList<String> cCE_CusFName) {
		CCE_CusFName = cCE_CusFName;
	}
	public ArrayList<String> getCCE_CusLName() {
		return CCE_CusLName;
	}
	public void setCCE_CusLName(ArrayList<String> cCE_CusLName) {
		CCE_CusLName = cCE_CusLName;
	}
	public String getSupplierMAIL_IssueDate() {
		return SupplierMAIL_IssueDate;
	}
	public void setSupplierMAIL_IssueDate(String supplierMAIL_IssueDate) {
		SupplierMAIL_IssueDate = supplierMAIL_IssueDate;
	}
	public String getCVE_LeadPassenger() {
		return CVE_LeadPassenger;
	}
	public void setCVE_LeadPassenger(String cVE_LeadPassenger) {
		CVE_LeadPassenger = cVE_LeadPassenger;
	}
	public String getCVE_IssueDate() {
		return CVE_IssueDate;
	}
	public void setCVE_IssueDate(String cVE_IssueDate) {
		CVE_IssueDate = cVE_IssueDate;
	}
	public String getCVE_Address1() {
		return CVE_Address1;
	}
	public void setCVE_Address1(String cVE_Address1) {
		CVE_Address1 = cVE_Address1;
	}
	public String getCVE_City() {
		return CVE_City;
	}
	public void setCVE_City(String cVE_City) {
		CVE_City = cVE_City;
	}
	public String getCVE_BookingNo() {
		return CVE_BookingNo;
	}
	public void setCVE_BookingNo(String cVE_BookingNo) {
		CVE_BookingNo = cVE_BookingNo;
	}
	public String getCVE_ActivityName() {
		return CVE_ActivityName;
	}
	public void setCVE_ActivityName(String cVE_ActivityName) {
		CVE_ActivityName = cVE_ActivityName;
	}
	public String getCVE_bookingStatus() {
		return CVE_bookingStatus;
	}
	public void setCVE_bookingStatus(String cVE_bookingStatus) {
		CVE_bookingStatus = cVE_bookingStatus;
	}
	public String getCVE_SupplierName() {
		return CVE_SupplierName;
	}
	public void setCVE_SupplierName(String cVE_SupplierName) {
		CVE_SupplierName = cVE_SupplierName;
	}
	public String getCVE_SupplierAdd() {
		return CVE_SupplierAdd;
	}
	public void setCVE_SupplierAdd(String cVE_SupplierAdd) {
		CVE_SupplierAdd = cVE_SupplierAdd;
	}
	public String getCVE_SupplierTP() {
		return CVE_SupplierTP;
	}
	public void setCVE_SupplierTP(String cVE_SupplierTP) {
		CVE_SupplierTP = cVE_SupplierTP;
	}
	public String getCVE_ProgramCity() {
		return CVE_ProgramCity;
	}
	public void setCVE_ProgramCity(String cVE_ProgramCity) {
		CVE_ProgramCity = cVE_ProgramCity;
	}
	public String getCVE_ActivityTransferDes() {
		return CVE_ActivityTransferDes;
	}
	public void setCVE_ActivityTransferDes(String cVE_ActivityTransferDes) {
		CVE_ActivityTransferDes = cVE_ActivityTransferDes;
	}
	public String getCVE_Duration() {
		return CVE_Duration;
	}
	public void setCVE_Duration(String cVE_Duration) {
		CVE_Duration = cVE_Duration;
	}
	public String getCVE_RatePlan() {
		return CVE_RatePlan;
	}
	public void setCVE_RatePlan(String cVE_RatePlan) {
		CVE_RatePlan = cVE_RatePlan;
	}
	public String getCVE_Qty() {
		return CVE_Qty;
	}
	public void setCVE_Qty(String cVE_Qty) {
		CVE_Qty = cVE_Qty;
	}
	public String getCVE_ServiceDate() {
		return CVE_ServiceDate;
	}
	public void setCVE_ServiceDate(String cVE_ServiceDate) {
		CVE_ServiceDate = cVE_ServiceDate;
	}
	public String getCVE_CustomerNotes() {
		return CVE_CustomerNotes;
	}
	public void setCVE_CustomerNotes(String cVE_CustomerNotes) {
		CVE_CustomerNotes = cVE_CustomerNotes;
	}
	public String getSupplierMAIL_BookingNo() {
		return SupplierMAIL_BookingNo;
	}
	public void setSupplierMAIL_BookingNo(String supplierMAIL_BookingNo) {
		SupplierMAIL_BookingNo = supplierMAIL_BookingNo;
	}
	public String getSupplierMAIL_ActivityName() {
		return SupplierMAIL_ActivityName;
	}
	public void setSupplierMAIL_ActivityName(String supplierMAIL_ActivityName) {
		SupplierMAIL_ActivityName = supplierMAIL_ActivityName;
	}
	public String getSupplierMAIL_BookingStatus() {
		return SupplierMAIL_BookingStatus;
	}
	public void setSupplierMAIL_BookingStatus(String supplierMAIL_BookingStatus) {
		SupplierMAIL_BookingStatus = supplierMAIL_BookingStatus;
	}
	public String getSupplierMAIL_ProgramCity() {
		return SupplierMAIL_ProgramCity;
	}
	public void setSupplierMAIL_ProgramCity(String supplierMAIL_ProgramCity) {
		SupplierMAIL_ProgramCity = supplierMAIL_ProgramCity;
	}
	public String getSupplierMAIL_Duration() {
		return SupplierMAIL_Duration;
	}
	public void setSupplierMAIL_Duration(String supplierMAIL_Duration) {
		SupplierMAIL_Duration = supplierMAIL_Duration;
	}
	public String getSupplierMAIL_QTY() {
		return SupplierMAIL_QTY;
	}
	public void setSupplierMAIL_QTY(String supplierMAIL_QTY) {
		SupplierMAIL_QTY = supplierMAIL_QTY;
	}
	public String getSupplierMAIL_ServicyDate() {
		return SupplierMAIL_ServicyDate;
	}
	public void setSupplierMAIL_ServicyDate(String supplierMAIL_ServicyDate) {
		SupplierMAIL_ServicyDate = supplierMAIL_ServicyDate;
	}
	public String getSupplierMAIL_ActivityRate() {
		return SupplierMAIL_ActivityRate;
	}
	public void setSupplierMAIL_ActivityRate(String supplierMAIL_ActivityRate) {
		SupplierMAIL_ActivityRate = supplierMAIL_ActivityRate;
	}
	public String getSupplierMAIL_CustomerTP() {
		return SupplierMAIL_CustomerTP;
	}
	public void setSupplierMAIL_CustomerTP(String supplierMAIL_CustomerTP) {
		SupplierMAIL_CustomerTP = supplierMAIL_CustomerTP;
	}
	public String getCCE_ActivityNote() {
		return CCE_ActivityNote;
	}
	public void setCCE_ActivityNote(String cCE_ActivityNote) {
		CCE_ActivityNote = cCE_ActivityNote;
	}
	public String getCCE_CustomerNote() {
		return CCE_CustomerNote;
	}
	public void setCCE_CustomerNote(String cCE_CustomerNote) {
		CCE_CustomerNote = cCE_CustomerNote;
	}
	public int getCancelPolicyCount() {
		return cancelPolicyCount;
	}
	public void setCancelPolicyCount(int cancelPolicyCount) {
		this.cancelPolicyCount = cancelPolicyCount;
	}
	public String getActivityVaoucherRefNo() {
		return ActivityVaoucherRefNo;
	}
	public void setActivityVaoucherRefNo(String activityVaoucherRefNo) {
		ActivityVaoucherRefNo = activityVaoucherRefNo;
	}
	public String getCCE_BookingRef() {
		return CCE_BookingRef;
	}
	public void setCCE_BookingRef(String cCE_BookingRef) {
		CCE_BookingRef = cCE_BookingRef;
	}
	public String getCCE_BookingNo() {
		return CCE_BookingNo;
	}
	public void setCCE_BookingNo(String cCE_BookingNo) {
		CCE_BookingNo = cCE_BookingNo;
	}
	public String getCCE_ActivityType() {
		return CCE_ActivityType;
	}
	public void setCCE_ActivityType(String cCE_ActivityType) {
		CCE_ActivityType = cCE_ActivityType;
	}
	public String getCCE_BookingStatus() {
		return CCE_BookingStatus;
	}
	public void setCCE_BookingStatus(String cCE_BookingStatus) {
		CCE_BookingStatus = cCE_BookingStatus;
	}
	public String getCCE_Period() {
		return CCE_Period;
	}
	public void setCCE_Period(String cCE_Period) {
		CCE_Period = cCE_Period;
	}
	public String getCCE_RateType() {
		return CCE_RateType;
	}
	public void setCCE_RateType(String cCE_RateType) {
		CCE_RateType = cCE_RateType;
	}
	public String getCCE_Rate() {
		return CCE_Rate;
	}
	public void setCCE_Rate(String cCE_Rate) {
		CCE_Rate = cCE_Rate;
	}
	public String getCCE_QTY() {
		return CCE_QTY;
	}
	public void setCCE_QTY(String cCE_QTY) {
		CCE_QTY = cCE_QTY;
	}
	public String getCCE_TotalValue() {
		return CCE_TotalValue;
	}
	public void setCCE_TotalValue(String cCE_TotalValue) {
		CCE_TotalValue = cCE_TotalValue;
	}
	public String getCCE_TotalValue_Currency() {
		return CCE_TotalValue_Currency;
	}
	public void setCCE_TotalValue_Currency(String cCE_TotalValue_Currency) {
		CCE_TotalValue_Currency = cCE_TotalValue_Currency;
	}
	public String getCCE_CurrencyType_1() {
		return CCE_CurrencyType_1;
	}
	public void setCCE_CurrencyType_1(String cCE_CurrencyType_1) {
		CCE_CurrencyType_1 = cCE_CurrencyType_1;
	}
	public String getCCE_SubTotal() {
		return CCE_SubTotal;
	}
	public void setCCE_SubTotal(String cCE_SubTotal) {
		CCE_SubTotal = cCE_SubTotal;
	}
	public String getCCE_TotalTaxOther_1() {
		return CCE_TotalTaxOther_1;
	}
	public void setCCE_TotalTaxOther_1(String cCE_TotalTaxOther_1) {
		CCE_TotalTaxOther_1 = cCE_TotalTaxOther_1;
	}
	public String getCCE_TotalBookingValue() {
		return CCE_TotalBookingValue;
	}
	public void setCCE_TotalBookingValue(String cCE_TotalBookingValue) {
		CCE_TotalBookingValue = cCE_TotalBookingValue;
	}
	public String getCCE_AmountPayableNow() {
		return CCE_AmountPayableNow;
	}
	public void setCCE_AmountPayableNow(String cCE_AmountPayableNow) {
		CCE_AmountPayableNow = cCE_AmountPayableNow;
	}
	public String getCCE_AmountDue() {
		return CCE_AmountDue;
	}
	public void setCCE_AmountDue(String cCE_AmountDue) {
		CCE_AmountDue = cCE_AmountDue;
	}
	public String getCCE_CurrencyType_2() {
		return CCE_CurrencyType_2;
	}
	public void setCCE_CurrencyType_2(String cCE_CurrencyType_2) {
		CCE_CurrencyType_2 = cCE_CurrencyType_2;
	}
	public String getCCE_SubTotal_2() {
		return CCE_SubTotal_2;
	}
	public void setCCE_SubTotal_2(String cCE_SubTotal_2) {
		CCE_SubTotal_2 = cCE_SubTotal_2;
	}
	public String getCCE_TotalTaxOther_2() {
		return CCE_TotalTaxOther_2;
	}
	public void setCCE_TotalTaxOther_2(String cCE_TotalTaxOther_2) {
		CCE_TotalTaxOther_2 = cCE_TotalTaxOther_2;
	}
	public String getCCE_TotalBookingValue_3() {
		return CCE_TotalBookingValue_3;
	}
	public void setCCE_TotalBookingValue_3(String cCE_TotalBookingValue_3) {
		CCE_TotalBookingValue_3 = cCE_TotalBookingValue_3;
	}
	public String getCCE_TotalBookingValue_Currency() {
		return CCE_TotalBookingValue_Currency;
	}
	public void setCCE_TotalBookingValue_Currency(
			String cCE_TotalBookingValue_Currency) {
		CCE_TotalBookingValue_Currency = cCE_TotalBookingValue_Currency;
	}
	public String getCCE_AmountPocessed() {
		return CCE_AmountPocessed;
	}
	public void setCCE_AmountPocessed(String cCE_AmountPocessed) {
		CCE_AmountPocessed = cCE_AmountPocessed;
	}
	public String getCCE_AmountDue_2() {
		return CCE_AmountDue_2;
	}
	public void setCCE_AmountDue_2(String cCE_AmountDue_2) {
		CCE_AmountDue_2 = cCE_AmountDue_2;
	}
	public String getCCE_FName() {
		return CCE_FName;
	}
	public void setCCE_FName(String cCE_FName) {
		CCE_FName = cCE_FName;
	}
	public String getCCE_LName() {
		return CCE_LName;
	}
	public void setCCE_LName(String cCE_LName) {
		CCE_LName = cCE_LName;
	}
	public String getCCE_TP() {
		return CCE_TP;
	}
	public void setCCE_TP(String cCE_TP) {
		CCE_TP = cCE_TP;
	}
	public String getCCE_Email() {
		return CCE_Email;
	}
	public void setCCE_Email(String cCE_Email) {
		CCE_Email = cCE_Email;
	}
	public String getCCE_address() {
		return CCE_address;
	}
	public void setCCE_address(String cCE_address) {
		CCE_address = cCE_address;
	}
	public String getCCE_address_2() {
		return CCE_address_2;
	}
	public void setCCE_address_2(String cCE_address_2) {
		CCE_address_2 = cCE_address_2;
	}
	public String getCCE_country() {
		return CCE_country;
	}
	public void setCCE_country(String cCE_country) {
		CCE_country = cCE_country;
	}
	public String getCCE_city() {
		return CCE_city;
	}
	public void setCCE_city(String cCE_city) {
		CCE_city = cCE_city;
	}
	public String getCCE_Occupancy_ActivityName() {
		return CCE_Occupancy_ActivityName;
	}
	public void setCCE_Occupancy_ActivityName(String cCE_Occupancy_ActivityName) {
		CCE_Occupancy_ActivityName = cCE_Occupancy_ActivityName;
	}
	public String getCCE_Occupancy_FName() {
		return CCE_Occupancy_FName;
	}
	public void setCCE_Occupancy_FName(String cCE_Occupancy_FName) {
		CCE_Occupancy_FName = cCE_Occupancy_FName;
	}
	public String getCCE_Occupancy_LName() {
		return CCE_Occupancy_LName;
	}
	public void setCCE_Occupancy_LName(String cCE_Occupancy_LName) {
		CCE_Occupancy_LName = cCE_Occupancy_LName;
	}
	public String getCCE_PaymentTotalValue() {
		return CCE_PaymentTotalValue;
	}
	public void setCCE_PaymentTotalValue(String cCE_PaymentTotalValue) {
		CCE_PaymentTotalValue = cCE_PaymentTotalValue;
	}
	public ArrayList<String> getCanellationPolicy() {
		return CanellationPolicy;
	}
	public void setCanellationPolicy(ArrayList<String> canellationPolicy) {
		CanellationPolicy = canellationPolicy;
	}
	
	
	
	
}
