package webbookingconfirmation;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import webcom.model.WEB_ActivityDetails;

public class WEB_LoadBookingConfirmationDetails {

	private WebDriver driver;
	private WEB_ActivityDetails activitydetails;
	private ArrayList<String> cancelPolicy;
	private ArrayList<String> paticipanTitle;
	private ArrayList<String> paticipanFName;
	private ArrayList<String> paticipanLName;
	private String currentDateforImages;
	
	public WEB_LoadBookingConfirmationDetails(WEB_ActivityDetails activityDetails, WebDriver Driver){
		
		this.activitydetails = activityDetails;
		this.driver = Driver;
	}

	public WEB_BookingConfirmation getConfirmationDetails(WebDriver Driver) throws InterruptedException, IOException{
		
		WebDriverWait wait = new WebDriverWait(driver, 90);	
		WEB_BookingConfirmationInfo bookingInfo = new WEB_BookingConfirmationInfo(activitydetails, driver);
		bookingInfo.viewBookingConfirmationReport(activitydetails.getReservationNo(), driver);
		WEB_BookingConfirmation confirmationDetails = new WEB_BookingConfirmation();
		DateFormat dateFormatcurrentDate = new SimpleDateFormat("yyyy_MM_dd");
		Calendar cal = Calendar.getInstance();
		currentDateforImages = dateFormatcurrentDate.format(cal.getTime());
		cancelPolicy = new ArrayList<String>();
		paticipanTitle = new ArrayList<String>();
		paticipanFName = new ArrayList<String>();
		paticipanLName = new ArrayList<String>();
		
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");
		
		driver.findElement(By.xpath(".//*[@id='hidResId_1']")).click();
		Thread.sleep(1000);
		
		driver.findElement(By.xpath(".//*[@id='emaillayoutbasedon_customer1']")).click();
		Thread.sleep(2000);
		
		driver.switchTo().frame("Myemailformat");
		
		File CCE = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(CCE, new File(""+currentDateforImages+"/"+activitydetails.getScenarioCount()+"_CCEMail.png"));
		
		if (driver.findElements(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[9]/td")).size() != 0) {
			
			String ActivityVaoucherRefNo = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[9]/td")).getText().substring(35, 47);
			confirmationDetails.setActivityVaoucherRefNo(ActivityVaoucherRefNo);
			
			String CCE_BookingRef = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[3]/td")).getText().split(": ")[1];
			confirmationDetails.setCCE_BookingRef(CCE_BookingRef);
			
			String CCE_BookingNo = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[4]/td")).getText().split(": ")[1];
			confirmationDetails.setCCE_BookingNo(CCE_BookingNo);
			
			String CCE_ActivityType = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[11]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setCCE_ActivityType(CCE_ActivityType);
			
			String CCE_BookingStatus = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[13]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setCCE_BookingStatus(CCE_BookingStatus);
			
			String CCE_Period = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[14]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setCCE_Period(CCE_Period);
			
			String CCE_RateType = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[15]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setCCE_RateType(CCE_RateType);
			
			String CCE_Rate = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[16]/td[2]")).getText().split(": ")[1].replace(",", "").split("\\.")[0];
			confirmationDetails.setCCE_Rate(CCE_Rate);
			
			String CCE_QTY = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[17]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setCCE_QTY(CCE_QTY);
			
			String CCE_TotalValue = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[18]/td[2]/strong")).getText().split(": ")[1].replace(",", "").split("\\.")[0];
			confirmationDetails.setCCE_TotalValue(CCE_TotalValue);
			
			String CCE_TotalValue_Currency = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[18]/td[1]/strong")).getText().substring(7, 10);
			confirmationDetails.setCCE_TotalValue_Currency(CCE_TotalValue_Currency);
			
					 
			String CCE_CurrencyType_1 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[19]/td[7]/table/tbody/tr[1]/td[2]")).getText();
			confirmationDetails.setCCE_CurrencyType_1(CCE_CurrencyType_1);
			
			String CCE_SubTotal = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[20]/td[7]/table/tbody/tr[2]/td[2]")).getText().replace(",", "").split("\\.")[0];
			confirmationDetails.setCCE_SubTotal(CCE_SubTotal);
			
			String CCE_TotalTaxOther_1 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[20]/td[7]/table/tbody/tr[3]/td[2]")).getText().replace(",", "").split("\\.")[0];
			confirmationDetails.setCCE_TotalTaxOther_1(CCE_TotalTaxOther_1);
			
			String CCE_TotalBookingValue = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[21]/td[7]/table/tbody/tr[2]/td[2]")).getText().replace(",", "").split("\\.")[0];
			confirmationDetails.setCCE_TotalBookingValue(CCE_TotalBookingValue);
			
			String CCE_AmountPayableNow = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[21]/td[7]/table/tbody/tr[3]/td[2]")).getText().replace(",", "").split("\\.")[0];
			confirmationDetails.setCCE_AmountPayableNow(CCE_AmountPayableNow);
			
			String CCE_AmountDue = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[21]/td[7]/table/tbody/tr[4]/td[2]")).getText().split("\\.")[0];
			confirmationDetails.setCCE_AmountDue(CCE_AmountDue);
			
			
			String CCE_TotalBookingValue_3 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[21]/td[7]/table/tbody/tr[2]/td[2]")).getText().replace(",", "").split("\\.")[0];
			confirmationDetails.setCCE_TotalBookingValue_3(CCE_TotalBookingValue_3);
			
			String CCE_TotalBookingValue_Currency = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[21]/td[7]/table/tbody/tr[2]/td[1]")).getText().substring(21, 24);
			confirmationDetails.setCCE_TotalBookingValue_Currency(CCE_TotalBookingValue_Currency);
			
			String CCE_AmountPocessed = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[21]/td[7]/table/tbody/tr[3]/td[2]")).getText().replace(",", "").split("\\.")[0];
			confirmationDetails.setCCE_AmountPocessed(CCE_AmountPocessed);
			
			String CCE_AmountDue_2 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[21]/td[7]/table/tbody/tr[4]/td[2]")).getText().split("\\.")[0];
			confirmationDetails.setCCE_AmountDue_2(CCE_AmountDue_2);
			
			
			
			String CCE_FName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[25]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setCCE_FName(CCE_FName);
			
			String CCE_LName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[25]/td[8]")).getText().split(": ")[1];
			confirmationDetails.setCCE_LName(CCE_LName);
			
			String CCE_TP;
			try {
				CCE_TP = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[27]/td[8]")).getText().split(": ")[1];
				confirmationDetails.setCCE_TP(CCE_TP);
			} catch (Exception e1) {
				CCE_TP = "Not Available";
				confirmationDetails.setCCE_TP(CCE_TP);
			}
			
			String CCE_Email = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[30]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setCCE_Email(CCE_Email);
			
			String CCE_address = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[26]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setCCE_address(CCE_address);
			
			
			
			String CCE_country = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[28]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setCCE_country(CCE_country);
			
			String CCE_city = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[29]/td[8]")).getText().split(": ")[1];
			confirmationDetails.setCCE_city(CCE_city);
			
			
			
			String CCE_Occupancy_ActivityName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[33]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setCCE_Occupancy_ActivityName(CCE_Occupancy_ActivityName);
			
			String CCE_Occupancy_FName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[34]/td/table/tbody/tr[3]/td[4]")).getText();
			confirmationDetails.setCCE_Occupancy_FName(CCE_Occupancy_FName);
			
			String CCE_Occupancy_LName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[34]/td/table/tbody/tr[3]/td[5]")).getText();
			confirmationDetails.setCCE_Occupancy_LName(CCE_Occupancy_LName);
		
			
			try {
				try {
					String CCE_PaymentTotalValue = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[39]/td[8]")).getText().split(": ")[1].split("\\.")[0];
					confirmationDetails.setCCE_PaymentTotalValue(CCE_PaymentTotalValue);
				} catch (Exception e) {
					String CCE_PaymentTotalValue = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[39]/td[2]")).getText().split(": ")[1].split("\\.")[0];
					confirmationDetails.setCCE_PaymentTotalValue(CCE_PaymentTotalValue);
				}
				
			} catch (Exception e2) {
				
			}
						
			try {
				String CCE_CustomerNote = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[46]/td")).getText();
				confirmationDetails.setCCE_CustomerNote(CCE_CustomerNote);
			} catch (Exception e2) {
				String CCE_CustomerNote = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[42]/td")).getText();
				confirmationDetails.setCCE_CustomerNote(CCE_CustomerNote);
			}
			
			
			WebElement tableId = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[34]/td/table"));
			List<WebElement> rowListForNames = tableId.findElements(By.tagName("tr"));
			
			for (int i = 3; i <= rowListForNames.size(); i++) {
				
				String title = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[34]/td/table/tbody/tr["+i+"]/td[3]")).getText();
				paticipanTitle.add(title);	
				
				String fName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[34]/td/table/tbody/tr["+i+"]/td[4]")).getText();
				paticipanFName.add(fName);	
				
				String lName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[34]/td/table/tbody/tr["+i+"]/td[5]")).getText();
				paticipanLName.add(lName);
				
			}
			
			confirmationDetails.setCCE_CusTitle(paticipanTitle);
			confirmationDetails.setCCE_CusFName(paticipanFName);
			confirmationDetails.setCCE_CusLName(paticipanLName);
			
			
			List<WebElement> liTypes;
			try {
				
				WebElement cancelElement = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[50]/td/ul"));
				liTypes = cancelElement.findElements(By.tagName("li"));
				
				for (int i = 1; i <= liTypes.size(); i++) {
					
					String policyString = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[50]/td/ul/li["+i+"]")).getText();
					cancelPolicy.add(policyString);	
				}
				
				confirmationDetails.setCanellationPolicy(cancelPolicy);
				
			} catch (Exception e2) {
				
				WebElement cancelElement = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[46]/td/ul"));
				liTypes = cancelElement.findElements(By.tagName("li"));
				
				for (int i = 1; i <= liTypes.size(); i++) {
					
					String policyString = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[46]/td/ul/li["+i+"]")).getText();
					cancelPolicy.add(policyString);	
				}
				
				confirmationDetails.setCanellationPolicy(cancelPolicy);
			}
			
			
			
			
			String CCE_Tel_1 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[1].split(": ")[1];
			confirmationDetails.setCCE_Tel_1(CCE_Tel_1);
			
			String CCE_Tel_2;
			try {
				
				try {
					CCE_Tel_2 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[54]/td")).getText().split(": ")[1];
					confirmationDetails.setCCE_Tel_2(CCE_Tel_2);
				} catch (Exception e) {
					CCE_Tel_2 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[51]/td")).getText().split(": ")[1];
					confirmationDetails.setCCE_Tel_2(CCE_Tel_2);
				}
				
				
				
			} catch (Exception e1) {
				
				try {					
					CCE_Tel_2 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[55]/td")).getText();
					confirmationDetails.setCCE_Tel_2(CCE_Tel_2);
					
				} catch (Exception e) {
					CCE_Tel_2 = "Not Available";
					confirmationDetails.setCCE_Tel_2(CCE_Tel_2);
				}
				
			}
			
			String CCE_Email_1 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[3].split(": ")[1];
			confirmationDetails.setCCE_Email_1(CCE_Email_1);
			try {
				
				try {
					String CCE_Email_2 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[55]/td")).getText().split(": ")[1];
					confirmationDetails.setCCE_Email_2(CCE_Email_2);
				} catch (Exception e) {
					String CCE_Email_2 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[52]/td")).getText().split(": ")[1];
					confirmationDetails.setCCE_Email_2(CCE_Email_2);
				}
				
			} catch (Exception e) {
				String CCE_Email_2 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[56]/td")).getText().split(": ")[1];
				confirmationDetails.setCCE_Email_2(CCE_Email_2);
			}
			String CCE_Website = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[4].split(": ")[1];
			confirmationDetails.setCCE_Website(CCE_Website);
			String CCE_Fax = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[2].split(": ")[1];
			confirmationDetails.setCCE_Fax(CCE_Fax);
			
			try {
				String CCE_CompanyName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[57]/td/div")).getText();
				confirmationDetails.setCCE_CompanyName(CCE_CompanyName);
			} catch (Exception e) {
				String CCE_CompanyName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[53]/td/div")).getText();
				confirmationDetails.setCCE_CompanyName(CCE_CompanyName);
			}
			
			
			
		}else{
			
			confirmationDetails.setCustomerConfirmationMailLoaded(false);	
		}
		
		/////////
		 
		Thread.sleep(500);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");
				
		driver.findElement(By.xpath(".//*[@id='emaillayoutbasedon_customer2']")).click();
		Thread.sleep(2000);
		
		driver.switchTo().frame("Myemailformat");
		
		File CVE = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(CVE, new File(""+currentDateforImages+"/"+activitydetails.getScenarioCount()+"_CVEMail.png"));
				
		if (driver.findElements(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[4]/td[2]")).size() != 0) {
			
			String CVE_LeadPassenger = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[4]/td[2]")).getText();
			confirmationDetails.setCVE_LeadPassenger(CVE_LeadPassenger);
			
			String CVE_IssueDate = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[4]/td[8]")).getText().split(": ")[1];
			confirmationDetails.setCVE_IssueDate(CVE_IssueDate);
			
//			String CVE_Address1 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[5]/td[2]")).getText().split(": ")[1];
//			confirmationDetails.setCVE_Address1(CVE_Address1);
			
			String CVE_City = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[6]/td[2]")).getText();
			confirmationDetails.setCVE_City(CVE_City);
			
			String CVE_BookingNo = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[5]/td[8]")).getText().split(": ")[1];
			confirmationDetails.setCVE_BookingNo(CVE_BookingNo);
			
			String CVE_ActivityName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[9]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setCVE_ActivityName(CVE_ActivityName);
			
			String CVE_bookingStatus = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[10]/td[8]")).getText().split(": ")[1];
			confirmationDetails.setCVE_bookingStatus(CVE_bookingStatus);
			
			
			String CVE_SupplierAdd = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[11]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setCVE_SupplierAdd(CVE_SupplierAdd);
			
			String CVE_SupplierTP = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[12]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setCVE_SupplierTP(CVE_SupplierTP);
			
			String CVE_ProgramCity = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[11]/td[4]")).getText().split(": ")[1];
			confirmationDetails.setCVE_ProgramCity(CVE_ProgramCity);
			
			
			
			String CVE_ActivityTransferDes = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[16]/td[1]")).getText();
			confirmationDetails.setCVE_ActivityTransferDes(CVE_ActivityTransferDes);
			
			String CVE_Duration = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[16]/td[2]")).getText();
			confirmationDetails.setCVE_Duration(CVE_Duration);
			
			String CVE_RatePlan = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[16]/td[3]")).getText();
			confirmationDetails.setCVE_RatePlan(CVE_RatePlan);
			
			String CVE_Qty = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[16]/td[4]")).getText();
			confirmationDetails.setCVE_Qty(CVE_Qty);
			
			String CVE_ServiceDate = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[16]/td[5]")).getText();
			confirmationDetails.setCVE_ServiceDate(CVE_ServiceDate);
			
//			String CVE_CustomerNotes = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[24]/td[1]")).getText();
//			confirmationDetails.setCVE_CustomerNotes(CVE_CustomerNotes);
			
			String CvE_Tel_1 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[1].split(": ")[1];
			confirmationDetails.setCVE_Tel_1(CvE_Tel_1);		
			String CvE_Email_1 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[3].split(": ")[1];
			confirmationDetails.setCVE_Email_1(CvE_Email_1);	
			String CvE_Website = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[4].split(": ")[1];
			confirmationDetails.setCVE_Website(CvE_Website);
			String CvE_Fax = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[2].split(": ")[1];
			confirmationDetails.setCVE_Fax(CvE_Fax);
			
			
			
		}else{
			
			confirmationDetails.setCustomerVoucherMailLoaded(false);
		}
		
		
		//String CVE_CustomerNotes = driver.findElement(By.xpath("")).getText();
		
		/////////////////
		
		Thread.sleep(500);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");
		
		driver.findElement(By.xpath(".//*[@id='emaillayoutbasedon_supplier1']")).click();
		Thread.sleep(2000);
		
		driver.switchTo().frame("Myemailformat");
		
		File supplier = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(supplier, new File(""+currentDateforImages+"/"+activitydetails.getScenarioCount()+"_SupplierMail.png"));
				
		try {
			
			if (driver.findElements(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[2]/td/div")).size() != 0) {
				
				String SupplierMAIL_BookingNo = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[2]/td/div")).getText().split(": ")[1];
				confirmationDetails.setSupplierMAIL_BookingNo(SupplierMAIL_BookingNo);
				
				String issueDate = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[3]/td[8]")).getText().split(": ")[1];
				confirmationDetails.setSupplierMAIL_IssueDate(issueDate);
				
				String SupplierMAIL_Tel_1 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[1].split(": ")[1];
				confirmationDetails.setSupplierMAIL_Tel_1(SupplierMAIL_Tel_1);
				String SupplierMAIL_Email_1 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[3].split(": ")[1];
				confirmationDetails.setSupplierMAIL_Email_1(SupplierMAIL_Email_1);
				String SupplierMAIL_Website = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[4].split(": ")[1];
				confirmationDetails.setSupplierMAIL_Website(SupplierMAIL_Website);
				String SupplierMAIL_Fax = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[2].split(": ")[1];
				confirmationDetails.setSupplierMAIL_Fax(SupplierMAIL_Fax);
				String SupplierMAIL_CompanyName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[30]/td/div")).getText();
				confirmationDetails.setSupplierMAIL_CompanyName(SupplierMAIL_CompanyName);
				
				if(driver.getPageSource().contains("confirmation/acknowledgement")){
					
					String SupplierMAIL_ActivityName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[11]/td[2]")).getText().split(": ")[1];
					confirmationDetails.setSupplierMAIL_ActivityName(SupplierMAIL_ActivityName);
					
					String SupplierMAIL_BookingStatus = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[12]/td[2]")).getText().split(": ")[1];
					confirmationDetails.setSupplierMAIL_BookingStatus(SupplierMAIL_BookingStatus);
					
					String SupplierMAIL_ProgramCity = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[12]/td[8]")).getText().split(": ")[1];
					confirmationDetails.setSupplierMAIL_ProgramCity(SupplierMAIL_ProgramCity);
					
					String SupplierMAIL_Duration = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[15]/td[2]")).getText();
					confirmationDetails.setSupplierMAIL_Duration(SupplierMAIL_Duration);
					
					String SupplierMAIL_QTY = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[15]/td[3]")).getText();
					confirmationDetails.setSupplierMAIL_QTY(SupplierMAIL_QTY);
					
					String SupplierMAIL_ServicyDate = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[15]/td[4]")).getText();
					confirmationDetails.setSupplierMAIL_ServicyDate(SupplierMAIL_ServicyDate);
					
					String SupplierMAIL_ActivityRate = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[15]/td[5]")).getText().split("\\.")[0];
					confirmationDetails.setSupplierMAIL_ActivityRate(SupplierMAIL_ActivityRate);
					
					String SupplierMAIL_CustomerTP = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[19]/td[2]")).getText().split(": ")[1];
					confirmationDetails.setSupplierMAIL_CustomerTP(SupplierMAIL_CustomerTP);
					
					String SupplierMAIL_PN = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[18]/td[1]")).getText();
					confirmationDetails.setSupplierMAIL_PassengerName(SupplierMAIL_PN);
					
				}else{
					
					String SupplierMAIL_ActivityName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[11]/td[2]")).getText().split(": ")[1];
					confirmationDetails.setSupplierMAIL_ActivityName(SupplierMAIL_ActivityName);
					
					String SupplierMAIL_BookingStatus = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[12]/td[2]")).getText().split(": ")[1];
					confirmationDetails.setSupplierMAIL_BookingStatus(SupplierMAIL_BookingStatus);
					
					String SupplierMAIL_ProgramCity = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[12]/td[8]")).getText().split(": ")[1];
					confirmationDetails.setSupplierMAIL_ProgramCity(SupplierMAIL_ProgramCity);
					
					String SupplierMAIL_Duration = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[15]/td[2]")).getText();
					confirmationDetails.setSupplierMAIL_Duration(SupplierMAIL_Duration);
					
					String SupplierMAIL_QTY = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[15]/td[3]")).getText();
					confirmationDetails.setSupplierMAIL_QTY(SupplierMAIL_QTY);
					
					String SupplierMAIL_ServicyDate = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[15]/td[4]")).getText();
					confirmationDetails.setSupplierMAIL_ServicyDate(SupplierMAIL_ServicyDate);
					
					String SupplierMAIL_ActivityRate = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[15]/td[5]")).getText().split("\\.")[0];
					confirmationDetails.setSupplierMAIL_ActivityRate(SupplierMAIL_ActivityRate);
					
					String SupplierMAIL_CustomerTP;
					
					try {
						SupplierMAIL_CustomerTP = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[19]/td[2]")).getText().split(": ")[1];
						confirmationDetails.setSupplierMAIL_CustomerTP(SupplierMAIL_CustomerTP);
					} catch (Exception e) {
						SupplierMAIL_CustomerTP = "Not Available";
						confirmationDetails.setSupplierMAIL_CustomerTP(SupplierMAIL_CustomerTP);
					}
					
					String SupplierMAIL_PN = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[18]/td[1]")).getText();
					confirmationDetails.setSupplierMAIL_PassengerName(SupplierMAIL_PN);
					
				}
			
			}else{
				
				confirmationDetails.setSupplier(false);
			}
			
		} catch (Exception e) {
			
			confirmationDetails.setSupplier(false);
		}
        
        
		
		return confirmationDetails;
		
	}
}
