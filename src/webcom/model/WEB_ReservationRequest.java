package webcom.model;

import java.util.ArrayList;

public class WEB_ReservationRequest {
	
	private String currency = "";
	private String tourDate = "";
	private String tourCity = "";
	private String childAge = "";
	private String paxType = "";
	private ArrayList<String> childAgesList;
	private String url = "";
	
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	public ArrayList<String> getChildAgesList() {
		return childAgesList;
	}
	public void setChildAgesList(ArrayList<String> childAgesList) {
		this.childAgesList = childAgesList;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getTourDate() {
		return tourDate;
	}
	public void setTourDate(String tourDate) {
		this.tourDate = tourDate;
	}
	public String getTourCity() {
		return tourCity;
	}
	public void setTourCity(String tourCity) {
		this.tourCity = tourCity;
	}
	public String getChildAge() {
		return childAge;
	}
	public void setChildAge(String childAge) {
		this.childAge = childAge;
	}
	public String getPaxType() {
		return paxType;
	}
	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}
	
	
	
	

}
