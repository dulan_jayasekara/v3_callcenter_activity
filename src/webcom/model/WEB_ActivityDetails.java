package webcom.model;

import java.util.ArrayList;

import webcom.types.WEB_TourOperatorType;

public class WEB_ActivityDetails {
	
	private String rate_NetRate;
	private String rate_SellRate;
	
	private boolean isBookingEngineLoaded = true;
	private boolean isAddToCart = true;
	private boolean isProgramsAvailabilityActivityTypes = true;
	private boolean isActivityRemoved = true;
	private boolean isCancelPolicy = true;
	private boolean isMoreInfo = true;
	
	private boolean isCountryLoaded = true;
	private boolean isCountryListLoaded = true;
	private boolean isCityLoaded = true;
	private boolean isDateFromLoaded = true;
	private boolean isDateToLoaded = true;
	private boolean isAdultsLoaded = true;
	private boolean isChildLoaded = true;
	private boolean isCalenderAvailble = true;
	private String showAdd;
	private String HideAdd;
	private boolean isActivityType = true;
	private boolean isProgramCat = true;
	private boolean isPreCurrency = true;
	private boolean isPromoCode = true;
	private boolean isCurrencyListLoaded = true;
	private boolean isProgCatListLoaded = true;
	private String traceId;
	
	private String scenarioCount;
	private String showResultsAdd;
	private String HideResultsAdd;
	private boolean isResultsProgramCat = true;
	private boolean isResultsPreCurrency = true;
	private boolean isResultsPromoCode = true;
	private boolean isPriceRangeFilter = true;
	private boolean isProgramAvailablity = true;
	private boolean isActivityNameFilter = true;
	private boolean isResultsCurrencyListLoaded = true;
	private boolean isResultsProgCatListLoaded = true;

	
	private String activityCurrency;
	private String actDescription = "";
	private int daysCount;
	private String mainRate;
	private String CurrentDate;
	private boolean isResultsAvailable;
	private String addAndContinue;
	private int activityCount;
	private String paymentDetails;
	private int resulteBlockSizeOnReq;
	private ArrayList<String> onRequestLabels;
	
	private ArrayList<String> cancelPolicy;
	private ArrayList<String> paymentCancelPolicy;
	
	private String shoppingCart_CartCurrency;
	private String shoppingCart_subTotal;
	private String shoppingCart_TotalTax;
	private String shoppingCart_TotalValue;
	private String shoppingCart_AmountNow;
	private String shoppingCart_AmountCheckIn;
	
	private String shoppingCart_TotalActivityValue;
	private String shoppingCart_MyBasketTotalValue;	
	
	private ArrayList<String> resultsPage_ActivityTypes;
	private ArrayList<String> resultsPage_City;
	private ArrayList<String> resultsPage_Period;
	private ArrayList<String> resultsPage_RateType;
	private ArrayList<String> resultsPage_QTY;
	private ArrayList<String> resultsPage_DailyRate;
	
	private ArrayList<Integer> beforeSorting; 
	private ArrayList<Integer> afterSorting;
	private ArrayList<Integer> beforeSortingDescending; 
	private ArrayList<Integer> afterSortingDescending;
	
	
	private long timeLoadpayment;
	private long timeConfirmBooking;
	private long timeSearchButtClickTime;
	private long timeResultsAvailable;
	private long timeAddedtoCart;
	private long timeAailableClick;
	private long timeFirstSearch, timeFirstResults;
	
	private String cartCurrency;
	private String cartVallue;
	
	private String minPrize;
	private String pageLoadedResults;
	private String maxPrize;
	
	private String paymentPage_Title;
	private String paymentPage_FName;
	private String paymentPage_LName;
	private String paymentPage_TP;
	private String paymentPage_Email;
	private String paymentPage_address;
	private String paymentPage_country;
	private String paymentPage_city;
	private String paymentPage_State = "";
	private String paymentPage_PostalCode = "";
	private String paymentsPage_AgentCommission = "";
	private String Shopping_paymentsPage_AgentCommission = "";
	
	private ArrayList<String> resultsPage_cusTitle;
	private ArrayList<String> resultsPage_cusFName;
	private ArrayList<String> resultsPage_cusLName;
	
	private String paymentBilling_CardTotal;
	private String paymentBilling_CardCurrency;
	private String paymentBilling_subTotal;
	private String paymentBilling_TotalTax;
	private String paymentBilling_TotalValue;
	private String paymentBilling_AmountNow;
	private String paymentBilling_AmountCheckIn;
	private String paymentsPage_DiscountValue1;
	private String paymentsPage_DiscountValue2;
	
	private String customerNotes;
	private String activityNotes;
	
	private String paymentPopUp_ActivityName;
	private String paymentPopUp_Status;
	private String paymentGatewayTotal;
	private WEB_TourOperatorType userType;
	
	//////
	
	private String Quote_QuotationNo;
	private String Quote_Status;
	private String Quote_Currency;
	private String Quote_Mail;
	private String Quote_CityItinary;
	private String Quote_ActivityTypeName;
	private String Quote_QTY;
	private String Quote_ActivityName;
	private String Quote_FName;
	private String Quote_LName;
	private String Quote_TP;
	private String Quote_Email;
	private String Quote_address_1;
	private String Quote_address_2;
	private String Quote_city;
	private String Quote_country;
	private String Quote_state = "";
	private String Quote_postalCode = "";
	
	private String Quote_subTotal;
	private String Quote_taxandOther;
	private String Quote_totalValue;
	private String Quote_TotalGrossPackageValue;
	private String Quote_TotalTax;
	private String Quote_TotalPackageValue;
	
	/////////////////////////////////////////////////////////////////////////
	
	private String confirmation_BookingRefference;
	private String reservationNo = "Not Available";
	private String confirmation_CusMailAddress;
	private String confirmation_BI_ActivityName;
	private String confirmation_BI_City;
	private String confirmation_BI_ActType;
	private String confirmation_BI_BookingStatus;
	private String confirmation_BI_SelectedDate;
	
	private ArrayList<String> confirmation_RateType;
	private ArrayList<String> confirmation_DailyRate;
	private ArrayList<String> confirmation_QTY;
	
	private String confirmation_Currency1;
	private String confirmation_Currency2;
	
	private String confirmation_SubTotal;
	private String confirmation_Tax;
	private String confirmation_Total;
	private String confirmation_SubTotal2;
	private String confirmation_BookingFee;
	private String confirmation_TotalTaxOtherCharges;
	private String confirmation_Total2;
	private String confirmation_AmountNow;
	private String confirmation_AmountDueCheckIn;
	private String confirmationPage_AgentCommission;
	
	private String confirmation_FName = "Not Available";
	private String confirmation_LName = "Not Available";
	private String confirmation_TP = "Not Available";
	private String confirmation_Email = "Not Available";
	private String confirmation_address = "Not Available";
	private String confirmation_country = "Not Available";
	private String confirmation_city = "Not Available";
	private String confirmation_State = "Not Available";
	private String confirmation_postalCode = "Not Available";
	
	private String confirmation_AODActivityName;
	private ArrayList<String> confirmationPage_cusTitle;
	private ArrayList<String> confirmationPage_cusFName;
	private ArrayList<String> confirmationPage_cusLName;
	
	private String confirmation_BillingInfo_TotalValue;
	private ArrayList<String> confirmCancelPolicy;
	
	private String prizeChanged_1;
	private String prizeResults_1;
	private String prizeChanged_2;
	private String prizeResults_2;
	
	private String status = "Available";
	private int gtaActivity;
	private int hbActivity;
	private int rezActivity;
	
	private String toActivity_SubTotal = "";
	private String toActivity_TotalTax = "";
	private String toActivity_TotalValue = "";	
	private String toAgentCommission = "";
	private String confirmationPage_DiscountValue1;
	
	private String paymentType_bookingList = "";
	private String paymentReference_bookingList = "";
	private String authentReference_bookingList = "";
	private String paymentID = "";
	
	
	private String paymentMethod_PaymentsPage;
	private String paymentReference_PaymentsPage;
	
	
	
	
	public String getPaymentMethod_PaymentsPage() {
		return paymentMethod_PaymentsPage;
	}
	public void setPaymentMethod_PaymentsPage(String paymentMethod_PaymentsPage) {
		this.paymentMethod_PaymentsPage = paymentMethod_PaymentsPage;
	}
	public String getPaymentReference_PaymentsPage() {
		return paymentReference_PaymentsPage;
	}
	public void setPaymentReference_PaymentsPage(String paymentReference_PaymentsPage) {
		this.paymentReference_PaymentsPage = paymentReference_PaymentsPage;
	}
	public String getPaymentType_bookingList() {
		return paymentType_bookingList;
	}
	public void setPaymentType_bookingList(String paymentType_bookingList) {
		this.paymentType_bookingList = paymentType_bookingList;
	}
	public String getPaymentReference_bookingList() {
		return paymentReference_bookingList;
	}
	public void setPaymentReference_bookingList(String paymentReference_bookingList) {
		this.paymentReference_bookingList = paymentReference_bookingList;
	}
	public String getAuthentReference_bookingList() {
		return authentReference_bookingList;
	}
	public void setAuthentReference_bookingList(String authentReference_bookingList) {
		this.authentReference_bookingList = authentReference_bookingList;
	}
	public String getPaymentID() {
		return paymentID;
	}
	public void setPaymentID(String paymentID) {
		this.paymentID = paymentID;
	}
	public String getScenarioCount() {
		return scenarioCount;
	}
	public void setScenarioCount(String scenarioCount) {
		this.scenarioCount = scenarioCount;
	}
	public String getConfirmationPage_DiscountValue1() {
		return confirmationPage_DiscountValue1;
	}
	public void setConfirmationPage_DiscountValue1(
			String confirmationPage_DiscountValue1) {
		this.confirmationPage_DiscountValue1 = confirmationPage_DiscountValue1;
	}
	public String getShopping_paymentsPage_AgentCommission() {
		return Shopping_paymentsPage_AgentCommission;
	}
	public void setShopping_paymentsPage_AgentCommission(
			String shopping_paymentsPage_AgentCommission) {
		Shopping_paymentsPage_AgentCommission = shopping_paymentsPage_AgentCommission;
	}
	public String getPaymentsPage_DiscountValue1() {
		return paymentsPage_DiscountValue1;
	}
	public void setPaymentsPage_DiscountValue1(String paymentsPage_DiscountValue1) {
		this.paymentsPage_DiscountValue1 = paymentsPage_DiscountValue1;
	}
	public String getPaymentsPage_DiscountValue2() {
		return paymentsPage_DiscountValue2;
	}
	public void setPaymentsPage_DiscountValue2(String paymentsPage_DiscountValue2) {
		this.paymentsPage_DiscountValue2 = paymentsPage_DiscountValue2;
	}
	public String getPaymentsPage_AgentCommission() {
		return paymentsPage_AgentCommission;
	}
	public void setPaymentsPage_AgentCommission(String paymentsPage_AgentCommission) {
		this.paymentsPage_AgentCommission = paymentsPage_AgentCommission;
	}
	public String getConfirmationPage_AgentCommission() {
		return confirmationPage_AgentCommission;
	}
	public void setConfirmationPage_AgentCommission(
			String confirmationPage_AgentCommission) {
		this.confirmationPage_AgentCommission = confirmationPage_AgentCommission;
	}
	public WEB_TourOperatorType getUserType() {
		return userType;
	}
	public void setUserType(WEB_TourOperatorType userType) {
		this.userType = userType;
	}
	public String getToActivity_SubTotal() {
		return toActivity_SubTotal;
	}
	public void setToActivity_SubTotal(String toActivity_SubTotal) {
		this.toActivity_SubTotal = toActivity_SubTotal;
	}
	public String getToActivity_TotalTax() {
		return toActivity_TotalTax;
	}
	public void setToActivity_TotalTax(String toActivity_TotalTax) {
		this.toActivity_TotalTax = toActivity_TotalTax;
	}
	public String getToActivity_TotalValue() {
		return toActivity_TotalValue;
	}
	public void setToActivity_TotalValue(String toActivity_TotalValue) {
		this.toActivity_TotalValue = toActivity_TotalValue;
	}
	public String getToAgentCommission() {
		return toAgentCommission;
	}
	public void setToAgentCommission(String toAgentCommission) {
		this.toAgentCommission = toAgentCommission;
	}
	public int getRezActivity() {
		return rezActivity;
	}
	public void setRezActivity(int rezActivity) {
		this.rezActivity = rezActivity;
	}
	public int getGtaActivity() {
		return gtaActivity;
	}
	public void setGtaActivity(int gtaActivity) {
		this.gtaActivity = gtaActivity;
	}
	public int getHbActivity() {
		return hbActivity;
	}
	public void setHbActivity(int hbActivity) {
		this.hbActivity = hbActivity;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTraceId() {
		return traceId;
	}
	public void setTraceId(String traceId) {
		this.traceId = traceId;
	}
	public boolean isResultsCurrencyListLoaded() {
		return isResultsCurrencyListLoaded;
	}
	public void setResultsCurrencyListLoaded(boolean isResultsCurrencyListLoaded) {
		this.isResultsCurrencyListLoaded = isResultsCurrencyListLoaded;
	}
	public boolean isResultsProgCatListLoaded() {
		return isResultsProgCatListLoaded;
	}
	public void setResultsProgCatListLoaded(boolean isResultsProgCatListLoaded) {
		this.isResultsProgCatListLoaded = isResultsProgCatListLoaded;
	}
	public boolean isProgCatListLoaded() {
		return isProgCatListLoaded;
	}
	public void setProgCatListLoaded(boolean isProgCatListLoaded) {
		this.isProgCatListLoaded = isProgCatListLoaded;
	}
	public boolean isCurrencyListLoaded() {
		return isCurrencyListLoaded;
	}
	public void setCurrencyListLoaded(boolean isCurrencyListLoaded) {
		this.isCurrencyListLoaded = isCurrencyListLoaded;
	}
	public boolean isAdultsLoaded() {
		return isAdultsLoaded;
	}
	public void setAdultsLoaded(boolean isAdultsLoaded) {
		this.isAdultsLoaded = isAdultsLoaded;
	}
	public boolean isChildLoaded() {
		return isChildLoaded;
	}
	public void setChildLoaded(boolean isChildLoaded) {
		this.isChildLoaded = isChildLoaded;
	}
	public boolean isActivityNameFilter() {
		return isActivityNameFilter;
	}
	public void setActivityNameFilter(boolean isActivityNameFilter) {
		this.isActivityNameFilter = isActivityNameFilter;
	}
	public boolean isPriceRangeFilter() {
		return isPriceRangeFilter;
	}
	public void setPriceRangeFilter(boolean isPriceRangeFilter) {
		this.isPriceRangeFilter = isPriceRangeFilter;
	}
	public boolean isProgramAvailablity() {
		return isProgramAvailablity;
	}
	public void setProgramAvailablity(boolean isProgramAvailablity) {
		this.isProgramAvailablity = isProgramAvailablity;
	}
	public String getShowResultsAdd() {
		return showResultsAdd;
	}
	public void setShowResultsAdd(String showResultsAdd) {
		this.showResultsAdd = showResultsAdd;
	}
	public String getHideResultsAdd() {
		return HideResultsAdd;
	}
	public void setHideResultsAdd(String hideResultsAdd) {
		HideResultsAdd = hideResultsAdd;
	}
	public boolean isResultsProgramCat() {
		return isResultsProgramCat;
	}
	public void setResultsProgramCat(boolean isResultsProgramCat) {
		this.isResultsProgramCat = isResultsProgramCat;
	}
	public boolean isResultsPreCurrency() {
		return isResultsPreCurrency;
	}
	public void setResultsPreCurrency(boolean isResultsPreCurrency) {
		this.isResultsPreCurrency = isResultsPreCurrency;
	}
	public boolean isResultsPromoCode() {
		return isResultsPromoCode;
	}
	public void setResultsPromoCode(boolean isResultsPromoCode) {
		this.isResultsPromoCode = isResultsPromoCode;
	}
	public boolean isActivityType() {
		return isActivityType;
	}
	public void setActivityType(boolean isActivityType) {
		this.isActivityType = isActivityType;
	}
	public boolean isProgramCat() {
		return isProgramCat;
	}
	public void setProgramCat(boolean isProgramCat) {
		this.isProgramCat = isProgramCat;
	}
	public boolean isPreCurrency() {
		return isPreCurrency;
	}
	public void setPreCurrency(boolean isPreCurrency) {
		this.isPreCurrency = isPreCurrency;
	}
	public boolean isPromoCode() {
		return isPromoCode;
	}
	public void setPromoCode(boolean isPromoCode) {
		this.isPromoCode = isPromoCode;
	}
	public String getShowAdd() {
		return showAdd;
	}
	public void setShowAdd(String showAdd) {
		this.showAdd = showAdd;
	}
	public String getHideAdd() {
		return HideAdd;
	}
	public void setHideAdd(String hideAdd) {
		HideAdd = hideAdd;
	}
	public boolean isCountryListLoaded() {
		return isCountryListLoaded;
	}
	public void setCountryListLoaded(boolean isCountryListLoaded) {
		this.isCountryListLoaded = isCountryListLoaded;
	}
	public boolean isCountryLoaded() {
		return isCountryLoaded;
	}
	public void setCountryLoaded(boolean isCountryLoaded) {
		this.isCountryLoaded = isCountryLoaded;
	}
	public boolean isCityLoaded() {
		return isCityLoaded;
	}
	public void setCityLoaded(boolean isCityLoaded) {
		this.isCityLoaded = isCityLoaded;
	}
	public boolean isDateFromLoaded() {
		return isDateFromLoaded;
	}
	public void setDateFromLoaded(boolean isDateFromLoaded) {
		this.isDateFromLoaded = isDateFromLoaded;
	}
	public boolean isDateToLoaded() {
		return isDateToLoaded;
	}
	public void setDateToLoaded(boolean isDateToLoaded) {
		this.isDateToLoaded = isDateToLoaded;
	}
	public boolean isCalenderAvailble() {
		return isCalenderAvailble;
	}
	public void setCalenderAvailble(boolean isCalenderAvailble) {
		this.isCalenderAvailble = isCalenderAvailble;
	}
	public String getQuote_CityItinary() {
		return Quote_CityItinary;
	}
	public void setQuote_CityItinary(String quote_CityItinary) {
		Quote_CityItinary = quote_CityItinary;
	}
	public String getQuote_ActivityTypeName() {
		return Quote_ActivityTypeName;
	}
	public void setQuote_ActivityTypeName(String quote_ActivityTypeName) {
		Quote_ActivityTypeName = quote_ActivityTypeName;
	}
	public String getQuote_QuotationNo() {
		return Quote_QuotationNo;
	}
	public void setQuote_QuotationNo(String quote_QuotationNo) {
		Quote_QuotationNo = quote_QuotationNo;
	}
	public String getQuote_Status() {
		return Quote_Status;
	}
	public void setQuote_Status(String quote_Status) {
		Quote_Status = quote_Status;
	}
	public String getQuote_Currency() {
		return Quote_Currency;
	}
	public void setQuote_Currency(String quote_Currency) {
		Quote_Currency = quote_Currency;
	}
	public String getQuote_Mail() {
		return Quote_Mail;
	}
	public void setQuote_Mail(String quote_Mail) {
		Quote_Mail = quote_Mail;
	}
	public String getQuote_QTY() {
		return Quote_QTY;
	}
	public void setQuote_QTY(String quote_QTY) {
		Quote_QTY = quote_QTY;
	}
	public String getQuote_ActivityName() {
		return Quote_ActivityName;
	}
	public void setQuote_ActivityName(String quote_ActivityName) {
		Quote_ActivityName = quote_ActivityName;
	}
	public String getQuote_FName() {
		return Quote_FName;
	}
	public void setQuote_FName(String quote_FName) {
		Quote_FName = quote_FName;
	}
	public String getQuote_LName() {
		return Quote_LName;
	}
	public void setQuote_LName(String quote_LName) {
		Quote_LName = quote_LName;
	}
	public String getQuote_TP() {
		return Quote_TP;
	}
	public void setQuote_TP(String quote_TP) {
		Quote_TP = quote_TP;
	}
	public String getQuote_Email() {
		return Quote_Email;
	}
	public void setQuote_Email(String quote_Email) {
		Quote_Email = quote_Email;
	}
	public String getQuote_address_1() {
		return Quote_address_1;
	}
	public void setQuote_address_1(String quote_address_1) {
		Quote_address_1 = quote_address_1;
	}
	public String getQuote_address_2() {
		return Quote_address_2;
	}
	public void setQuote_address_2(String quote_address_2) {
		Quote_address_2 = quote_address_2;
	}
	public String getQuote_city() {
		return Quote_city;
	}
	public void setQuote_city(String quote_city) {
		Quote_city = quote_city;
	}
	public String getQuote_country() {
		return Quote_country;
	}
	public void setQuote_country(String quote_country) {
		Quote_country = quote_country;
	}
	public String getQuote_state() {
		return Quote_state;
	}
	public void setQuote_state(String quote_state) {
		Quote_state = quote_state;
	}
	public String getQuote_postalCode() {
		return Quote_postalCode;
	}
	public void setQuote_postalCode(String quote_postalCode) {
		Quote_postalCode = quote_postalCode;
	}
	public String getQuote_subTotal() {
		return Quote_subTotal;
	}
	public void setQuote_subTotal(String quote_subTotal) {
		Quote_subTotal = quote_subTotal;
	}
	public String getQuote_taxandOther() {
		return Quote_taxandOther;
	}
	public void setQuote_taxandOther(String quote_taxandOther) {
		Quote_taxandOther = quote_taxandOther;
	}
	public String getQuote_totalValue() {
		return Quote_totalValue;
	}
	public void setQuote_totalValue(String quote_totalValue) {
		Quote_totalValue = quote_totalValue;
	}
	public String getQuote_TotalGrossPackageValue() {
		return Quote_TotalGrossPackageValue;
	}
	public void setQuote_TotalGrossPackageValue(String quote_TotalGrossPackageValue) {
		Quote_TotalGrossPackageValue = quote_TotalGrossPackageValue;
	}
	public String getQuote_TotalTax() {
		return Quote_TotalTax;
	}
	public void setQuote_TotalTax(String quote_TotalTax) {
		Quote_TotalTax = quote_TotalTax;
	}
	public String getQuote_TotalPackageValue() {
		return Quote_TotalPackageValue;
	}
	public void setQuote_TotalPackageValue(String quote_TotalPackageValue) {
		Quote_TotalPackageValue = quote_TotalPackageValue;
	}
	public boolean isBookingEngineLoaded() {
		return isBookingEngineLoaded;
	}
	public void setBookingEngineLoaded(boolean isBookingEngineLoaded) {
		this.isBookingEngineLoaded = isBookingEngineLoaded;
	}
	public boolean isAddToCart() {
		return isAddToCart;
	}
	public void setAddToCart(boolean isAddToCart) {
		this.isAddToCart = isAddToCart;
	}
	public boolean isProgramsAvailabilityActivityTypes() {
		return isProgramsAvailabilityActivityTypes;
	}
	public void setProgramsAvailabilityActivityTypes(
			boolean isProgramsAvailabilityActivityTypes) {
		this.isProgramsAvailabilityActivityTypes = isProgramsAvailabilityActivityTypes;
	}
	public boolean isActivityRemoved() {
		return isActivityRemoved;
	}
	public void setActivityRemoved(boolean isActivityRemoved) {
		this.isActivityRemoved = isActivityRemoved;
	}
	public boolean isCancelPolicy() {
		return isCancelPolicy;
	}
	public void setCancelPolicy(boolean isCancelPolicy) {
		this.isCancelPolicy = isCancelPolicy;
	}
	public boolean isMoreInfo() {
		return isMoreInfo;
	}
	public void setMoreInfo(boolean isMoreInfo) {
		this.isMoreInfo = isMoreInfo;
	}
	public ArrayList<Integer> getBeforeSortingDescending() {
		return beforeSortingDescending;
	}
	public void setBeforeSortingDescending(
			ArrayList<Integer> beforeSortingDescending) {
		this.beforeSortingDescending = beforeSortingDescending;
	}
	public ArrayList<Integer> getAfterSortingDescending() {
		return afterSortingDescending;
	}
	public void setAfterSortingDescending(ArrayList<Integer> afterSortingDescending) {
		this.afterSortingDescending = afterSortingDescending;
	}
	public String getPageLoadedResults() {
		return pageLoadedResults;
	}
	public void setPageLoadedResults(String pageLoadedResults) {
		this.pageLoadedResults = pageLoadedResults;
	}
	public String getPrizeChanged_1() {
		return prizeChanged_1;
	}
	public void setPrizeChanged_1(String prizeChanged_1) {
		this.prizeChanged_1 = prizeChanged_1;
	}
	public String getPrizeResults_1() {
		return prizeResults_1;
	}
	public void setPrizeResults_1(String prizeResults_1) {
		this.prizeResults_1 = prizeResults_1;
	}
	public String getPrizeChanged_2() {
		return prizeChanged_2;
	}
	public void setPrizeChanged_2(String prizeChanged_2) {
		this.prizeChanged_2 = prizeChanged_2;
	}
	public String getPrizeResults_2() {
		return prizeResults_2;
	}
	public void setPrizeResults_2(String prizeResults_2) {
		this.prizeResults_2 = prizeResults_2;
	}
	public String getMinPrize() {
		return minPrize;
	}
	public void setMinPrize(String minPrize) {
		this.minPrize = minPrize;
	}
	public String getMaxPrize() {
		return maxPrize;
	}
	public void setMaxPrize(String maxPrize) {
		this.maxPrize = maxPrize;
	}
	public ArrayList<String> getOnRequestLabels() {
		return onRequestLabels;
	}
	public void setOnRequestLabels(ArrayList<String> onRequestLabels) {
		this.onRequestLabels = onRequestLabels;
	}
	public int getResulteBlockSizeOnReq() {
		return resulteBlockSizeOnReq;
	}
	public void setResulteBlockSizeOnReq(int resulteBlockSizeOnReq) {
		this.resulteBlockSizeOnReq = resulteBlockSizeOnReq;
	}
	public ArrayList<Integer> getBeforeSorting() {
		return beforeSorting;
	}
	public void setBeforeSorting(ArrayList<Integer> beforeSorting) {
		this.beforeSorting = beforeSorting;
	}
	public ArrayList<Integer> getAfterSorting() {
		return afterSorting;
	}
	public void setAfterSorting(ArrayList<Integer> afterSorting) {
		this.afterSorting = afterSorting;
	}
	public String getPaymentDetails() {
		return paymentDetails;
	}
	public void setPaymentDetails(String paymentDetails) {
		this.paymentDetails = paymentDetails;
	}
	public String getPaymentPage_PostalCode() {
		return paymentPage_PostalCode;
	}
	public void setPaymentPage_PostalCode(String paymentPage_PostalCode) {
		this.paymentPage_PostalCode = paymentPage_PostalCode;
	}
	public String getConfirmation_postalCode() {
		return confirmation_postalCode;
	}
	public void setConfirmation_postalCode(String confirmation_postalCode) {
		this.confirmation_postalCode = confirmation_postalCode;
	}
	public String getConfirmation_CusMailAddress() {
		return confirmation_CusMailAddress;
	}
	public void setConfirmation_CusMailAddress(String confirmation_CusMailAddress) {
		this.confirmation_CusMailAddress = confirmation_CusMailAddress;
	}
	public String getShoppingCart_CartCurrency() {
		return shoppingCart_CartCurrency;
	}
	public void setShoppingCart_CartCurrency(String shoppingCart_CartCurrency) {
		this.shoppingCart_CartCurrency = shoppingCart_CartCurrency;
	}
	public String getShoppingCart_subTotal() {
		return shoppingCart_subTotal;
	}
	public void setShoppingCart_subTotal(String shoppingCart_subTotal) {
		this.shoppingCart_subTotal = shoppingCart_subTotal;
	}
	public String getShoppingCart_TotalTax() {
		return shoppingCart_TotalTax;
	}
	public void setShoppingCart_TotalTax(String shoppingCart_TotalTax) {
		this.shoppingCart_TotalTax = shoppingCart_TotalTax;
	}
	public String getShoppingCart_TotalValue() {
		return shoppingCart_TotalValue;
	}
	public void setShoppingCart_TotalValue(String shoppingCart_TotalValue) {
		this.shoppingCart_TotalValue = shoppingCart_TotalValue;
	}
	public String getShoppingCart_AmountNow() {
		return shoppingCart_AmountNow;
	}
	public void setShoppingCart_AmountNow(String shoppingCart_AmountNow) {
		this.shoppingCart_AmountNow = shoppingCart_AmountNow;
	}
	public String getShoppingCart_AmountCheckIn() {
		return shoppingCart_AmountCheckIn;
	}
	public void setShoppingCart_AmountCheckIn(String shoppingCart_AmountCheckIn) {
		this.shoppingCart_AmountCheckIn = shoppingCart_AmountCheckIn;
	}
	public String getPaymentBilling_CardCurrency() {
		return paymentBilling_CardCurrency;
	}
	public void setPaymentBilling_CardCurrency(String paymentBilling_CardCurrency) {
		this.paymentBilling_CardCurrency = paymentBilling_CardCurrency;
	}
	public String getPaymentBilling_CardTotal() {
		return paymentBilling_CardTotal;
	}
	public void setPaymentBilling_CardTotal(String paymentBilling_CardTotal) {
		this.paymentBilling_CardTotal = paymentBilling_CardTotal;
	}
	public String getPaymentBilling_subTotal() {
		return paymentBilling_subTotal;
	}
	public void setPaymentBilling_subTotal(String paymentBilling_subTotal) {
		this.paymentBilling_subTotal = paymentBilling_subTotal;
	}
	public String getPaymentBilling_TotalTax() {
		return paymentBilling_TotalTax;
	}
	public void setPaymentBilling_TotalTax(String paymentBilling_TotalTax) {
		this.paymentBilling_TotalTax = paymentBilling_TotalTax;
	}
	public String getPaymentBilling_TotalValue() {
		return paymentBilling_TotalValue;
	}
	public void setPaymentBilling_TotalValue(String paymentBilling_TotalValue) {
		this.paymentBilling_TotalValue = paymentBilling_TotalValue;
	}
	public String getPaymentBilling_AmountNow() {
		return paymentBilling_AmountNow;
	}
	public void setPaymentBilling_AmountNow(String paymentBilling_AmountNow) {
		this.paymentBilling_AmountNow = paymentBilling_AmountNow;
	}
	public String getPaymentBilling_AmountCheckIn() {
		return paymentBilling_AmountCheckIn;
	}
	public void setPaymentBilling_AmountCheckIn(String paymentBilling_AmountCheckIn) {
		this.paymentBilling_AmountCheckIn = paymentBilling_AmountCheckIn;
	}
	public String getRate_NetRate() {
		return rate_NetRate;
	}
	public void setRate_NetRate(String rate_NetRate) {
		this.rate_NetRate = rate_NetRate;
	}
	public String getRate_SellRate() {
		return rate_SellRate;
	}
	public void setRate_SellRate(String rate_SellRate) {
		this.rate_SellRate = rate_SellRate;
	}
	public String getConfirmation_BookingRefference() {
		return confirmation_BookingRefference;
	}
	public void setConfirmation_BookingRefference(
			String confirmation_BookingRefference) {
		this.confirmation_BookingRefference = confirmation_BookingRefference;
	}
	public String getReservationNo() {
		return reservationNo;
	}
	public void setReservationNo(String reservationNo) {
		this.reservationNo = reservationNo;
	}
	public String getConfirmation_BI_ActivityName() {
		return confirmation_BI_ActivityName;
	}
	public void setConfirmation_BI_ActivityName(String confirmation_BI_ActivityName) {
		this.confirmation_BI_ActivityName = confirmation_BI_ActivityName;
	}
	public String getConfirmation_BI_City() {
		return confirmation_BI_City;
	}
	public void setConfirmation_BI_City(String confirmation_BI_City) {
		this.confirmation_BI_City = confirmation_BI_City;
	}
	public String getConfirmation_BI_ActType() {
		return confirmation_BI_ActType;
	}
	public void setConfirmation_BI_ActType(String confirmation_BI_ActType) {
		this.confirmation_BI_ActType = confirmation_BI_ActType;
	}
	public String getConfirmation_BI_BookingStatus() {
		return confirmation_BI_BookingStatus;
	}
	public void setConfirmation_BI_BookingStatus(
			String confirmation_BI_BookingStatus) {
		this.confirmation_BI_BookingStatus = confirmation_BI_BookingStatus;
	}
	public String getConfirmation_BI_SelectedDate() {
		return confirmation_BI_SelectedDate;
	}
	public void setConfirmation_BI_SelectedDate(String confirmation_BI_SelectedDate) {
		this.confirmation_BI_SelectedDate = confirmation_BI_SelectedDate;
	}
	public ArrayList<String> getConfirmation_RateType() {
		return confirmation_RateType;
	}
	public void setConfirmation_RateType(ArrayList<String> confirmation_RateType) {
		this.confirmation_RateType = confirmation_RateType;
	}
	public ArrayList<String> getConfirmation_DailyRate() {
		return confirmation_DailyRate;
	}
	public void setConfirmation_DailyRate(ArrayList<String> confirmation_DailyRate) {
		this.confirmation_DailyRate = confirmation_DailyRate;
	}
	public ArrayList<String> getConfirmation_QTY() {
		return confirmation_QTY;
	}
	public void setConfirmation_QTY(ArrayList<String> confirmation_QTY) {
		this.confirmation_QTY = confirmation_QTY;
	}
	public String getConfirmation_Currency1() {
		return confirmation_Currency1;
	}
	public void setConfirmation_Currency1(String confirmation_Currency1) {
		this.confirmation_Currency1 = confirmation_Currency1;
	}
	public String getConfirmation_Currency2() {
		return confirmation_Currency2;
	}
	public void setConfirmation_Currency2(String confirmation_Currency2) {
		this.confirmation_Currency2 = confirmation_Currency2;
	}
	public String getConfirmation_SubTotal() {
		return confirmation_SubTotal;
	}
	public void setConfirmation_SubTotal(String confirmation_SubTotal) {
		this.confirmation_SubTotal = confirmation_SubTotal;
	}
	public String getConfirmation_Tax() {
		return confirmation_Tax;
	}
	public void setConfirmation_Tax(String confirmation_Tax) {
		this.confirmation_Tax = confirmation_Tax;
	}
	public String getConfirmation_Total() {
		return confirmation_Total;
	}
	public void setConfirmation_Total(String confirmation_Total) {
		this.confirmation_Total = confirmation_Total;
	}
	public String getConfirmation_SubTotal2() {
		return confirmation_SubTotal2;
	}
	public void setConfirmation_SubTotal2(String confirmation_SubTotal2) {
		this.confirmation_SubTotal2 = confirmation_SubTotal2;
	}
	public String getConfirmation_BookingFee() {
		return confirmation_BookingFee;
	}
	public void setConfirmation_BookingFee(String confirmation_BookingFee) {
		this.confirmation_BookingFee = confirmation_BookingFee;
	}
	public String getConfirmation_TotalTaxOtherCharges() {
		return confirmation_TotalTaxOtherCharges;
	}
	public void setConfirmation_TotalTaxOtherCharges(
			String confirmation_TotalTaxOtherCharges) {
		this.confirmation_TotalTaxOtherCharges = confirmation_TotalTaxOtherCharges;
	}
	public String getConfirmation_Total2() {
		return confirmation_Total2;
	}
	public void setConfirmation_Total2(String confirmation_Total2) {
		this.confirmation_Total2 = confirmation_Total2;
	}
	public String getConfirmation_AmountNow() {
		return confirmation_AmountNow;
	}
	public void setConfirmation_AmountNow(String confirmation_AmountNow) {
		this.confirmation_AmountNow = confirmation_AmountNow;
	}
	public String getConfirmation_AmountDueCheckIn() {
		return confirmation_AmountDueCheckIn;
	}
	public void setConfirmation_AmountDueCheckIn(
			String confirmation_AmountDueCheckIn) {
		this.confirmation_AmountDueCheckIn = confirmation_AmountDueCheckIn;
	}
	public String getConfirmation_FName() {
		return confirmation_FName;
	}
	public void setConfirmation_FName(String confirmation_FName) {
		this.confirmation_FName = confirmation_FName;
	}
	public String getConfirmation_LName() {
		return confirmation_LName;
	}
	public void setConfirmation_LName(String confirmation_LName) {
		this.confirmation_LName = confirmation_LName;
	}
	public String getConfirmation_TP() {
		return confirmation_TP;
	}
	public void setConfirmation_TP(String confirmation_TP) {
		this.confirmation_TP = confirmation_TP;
	}
	public String getConfirmation_Email() {
		return confirmation_Email;
	}
	public void setConfirmation_Email(String confirmation_Email) {
		this.confirmation_Email = confirmation_Email;
	}
	public String getConfirmation_address() {
		return confirmation_address;
	}
	public void setConfirmation_address(String confirmation_address) {
		this.confirmation_address = confirmation_address;
	}
	public String getConfirmation_country() {
		return confirmation_country;
	}
	public void setConfirmation_country(String confirmation_country) {
		this.confirmation_country = confirmation_country;
	}
	public String getConfirmation_city() {
		return confirmation_city;
	}
	public void setConfirmation_city(String confirmation_city) {
		this.confirmation_city = confirmation_city;
	}
	public String getConfirmation_State() {
		return confirmation_State;
	}
	public void setConfirmation_State(String confirmation_State) {
		this.confirmation_State = confirmation_State;
	}
	public String getConfirmation_AODActivityName() {
		return confirmation_AODActivityName;
	}
	public void setConfirmation_AODActivityName(String confirmation_AODActivityName) {
		this.confirmation_AODActivityName = confirmation_AODActivityName;
	}
	public ArrayList<String> getConfirmationPage_cusTitle() {
		return confirmationPage_cusTitle;
	}
	public void setConfirmationPage_cusTitle(
			ArrayList<String> confirmationPage_cusTitle) {
		this.confirmationPage_cusTitle = confirmationPage_cusTitle;
	}
	public ArrayList<String> getConfirmationPage_cusFName() {
		return confirmationPage_cusFName;
	}
	public void setConfirmationPage_cusFName(
			ArrayList<String> confirmationPage_cusFName) {
		this.confirmationPage_cusFName = confirmationPage_cusFName;
	}
	public ArrayList<String> getConfirmationPage_cusLName() {
		return confirmationPage_cusLName;
	}
	public void setConfirmationPage_cusLName(
			ArrayList<String> confirmationPage_cusLName) {
		this.confirmationPage_cusLName = confirmationPage_cusLName;
	}
	public String getConfirmation_BillingInfo_TotalValue() {
		return confirmation_BillingInfo_TotalValue;
	}
	public void setConfirmation_BillingInfo_TotalValue(
			String confirmation_BillingInfo_TotalValue) {
		this.confirmation_BillingInfo_TotalValue = confirmation_BillingInfo_TotalValue;
	}
	public String getPaymentGatewayTotal() {
		return paymentGatewayTotal;
	}
	public void setPaymentGatewayTotal(String paymentGatewayTotal) {
		this.paymentGatewayTotal = paymentGatewayTotal;
	}
	public String getPaymentPopUp_ActivityName() {
		return paymentPopUp_ActivityName;
	}
	public void setPaymentPopUp_ActivityName(String paymentPopUp_ActivityName) {
		this.paymentPopUp_ActivityName = paymentPopUp_ActivityName;
	}
	public String getPaymentPopUp_Status() {
		return paymentPopUp_Status;
	}
	public void setPaymentPopUp_Status(String paymentPopUp_Status) {
		this.paymentPopUp_Status = paymentPopUp_Status;
	}
	public String getCustomerNotes() {
		return customerNotes;
	}
	public void setCustomerNotes(String customerNotes) {
		this.customerNotes = customerNotes;
	}
	public String getActivityNotes() {
		return activityNotes;
	}
	public void setActivityNotes(String activityNotes) {
		this.activityNotes = activityNotes;
	}
	public ArrayList<String> getResultsPage_cusTitle() {
		return resultsPage_cusTitle;
	}
	public void setResultsPage_cusTitle(ArrayList<String> resultsPage_cusTitle) {
		this.resultsPage_cusTitle = resultsPage_cusTitle;
	}
	public ArrayList<String> getResultsPage_cusFName() {
		return resultsPage_cusFName;
	}
	public void setResultsPage_cusFName(ArrayList<String> resultsPage_cusFName) {
		this.resultsPage_cusFName = resultsPage_cusFName;
	}
	public ArrayList<String> getResultsPage_cusLName() {
		return resultsPage_cusLName;
	}
	public void setResultsPage_cusLName(ArrayList<String> resultsPage_cusLName) {
		this.resultsPage_cusLName = resultsPage_cusLName;
	}
	public int getActivityCount() {
		return activityCount;
	}
	public void setActivityCount(int activityCount) {
		this.activityCount = activityCount;
	}
	public String getPaymentPage_Title() {
		return paymentPage_Title;
	}
	public void setPaymentPage_Title(String paymentPage_Title) {
		this.paymentPage_Title = paymentPage_Title;
	}
	public String getPaymentPage_FName() {
		return paymentPage_FName;
	}
	public void setPaymentPage_FName(String paymentPage_FName) {
		this.paymentPage_FName = paymentPage_FName;
	}
	public String getPaymentPage_LName() {
		return paymentPage_LName;
	}
	public void setPaymentPage_LName(String paymentPage_LName) {
		this.paymentPage_LName = paymentPage_LName;
	}
	public String getPaymentPage_TP() {
		return paymentPage_TP;
	}
	public void setPaymentPage_TP(String paymentPage_TP) {
		this.paymentPage_TP = paymentPage_TP;
	}
	public String getPaymentPage_Email() {
		return paymentPage_Email;
	}
	public void setPaymentPage_Email(String paymentPage_Email) {
		this.paymentPage_Email = paymentPage_Email;
	}
	public String getPaymentPage_address() {
		return paymentPage_address;
	}
	public void setPaymentPage_address(String paymentPage_address) {
		this.paymentPage_address = paymentPage_address;
	}
	public String getPaymentPage_country() {
		return paymentPage_country;
	}
	public void setPaymentPage_country(String paymentPage_country) {
		this.paymentPage_country = paymentPage_country;
	}
	public String getPaymentPage_city() {
		return paymentPage_city;
	}
	public void setPaymentPage_city(String paymentPage_city) {
		this.paymentPage_city = paymentPage_city;
	}
	public String getPaymentPage_State() {
		return paymentPage_State;
	}
	public void setPaymentPage_State(String paymentPage_State) {
		this.paymentPage_State = paymentPage_State;
	}
	public String getAddAndContinue() {
		return addAndContinue;
	}
	public void setAddAndContinue(String addAndContinue) {
		this.addAndContinue = addAndContinue;
	}
	public String getCartCurrency() {
		return cartCurrency;
	}
	public void setCartCurrency(String cartCurrency) {
		this.cartCurrency = cartCurrency;
	}
	public String getCartVallue() {
		return cartVallue;
	}
	public void setCartVallue(String cartVallue) {
		this.cartVallue = cartVallue;
	}
	
	public boolean isResultsAvailable() {
		return isResultsAvailable;
	}
	public void setResultsAvailable(boolean isResultsAvailable) {
		this.isResultsAvailable = isResultsAvailable;
	}
	
	public String getActivityCurrency() {
		return activityCurrency;
	}
	public void setActivityCurrency(String activityCurrency) {
		this.activityCurrency = activityCurrency;
	}
	public String getActDescription() {
		return actDescription;
	}
	public void setActDescription(String actDescription) {
		this.actDescription = actDescription;
	}
	public ArrayList<String> getCancelPolicy() {
		return cancelPolicy;
	}
	public void setCancelPolicy(ArrayList<String> cancelPolicy) {
		this.cancelPolicy = cancelPolicy;
	}
	public ArrayList<String> getPaymentCancelPolicy() {
		return paymentCancelPolicy;
	}
	public void setPaymentCancelPolicy(ArrayList<String> paymentCancelPolicy) {
		this.paymentCancelPolicy = paymentCancelPolicy;
	}
	public ArrayList<String> getConfirmCancelPolicy() {
		return confirmCancelPolicy;
	}
	public void setConfirmCancelPolicy(ArrayList<String> confirmCancelPolicy) {
		this.confirmCancelPolicy = confirmCancelPolicy;
	}
	public long getTimeLoadpayment() {
		return timeLoadpayment;
	}
	public void setTimeLoadpayment(long timeLoadpayment) {
		this.timeLoadpayment = timeLoadpayment;
	}
	public long getTimeConfirmBooking() {
		return timeConfirmBooking;
	}
	public void setTimeConfirmBooking(long timeConfirmBooking) {
		this.timeConfirmBooking = timeConfirmBooking;
	}
	public long getTimeSearchButtClickTime() {
		return timeSearchButtClickTime;
	}
	public void setTimeSearchButtClickTime(long timeSearchButtClickTime) {
		this.timeSearchButtClickTime = timeSearchButtClickTime;
	}
	public long getTimeResultsAvailable() {
		return timeResultsAvailable;
	}
	public void setTimeResultsAvailable(long timeResultsAvailable) {
		this.timeResultsAvailable = timeResultsAvailable;
	}
	public long getTimeAddedtoCart() {
		return timeAddedtoCart;
	}
	public void setTimeAddedtoCart(long timeAddedtoCart) {
		this.timeAddedtoCart = timeAddedtoCart;
	}
	public long getTimeAailableClick() {
		return timeAailableClick;
	}
	public void setTimeAailableClick(long timeAailableClick) {
		this.timeAailableClick = timeAailableClick;
	}
	public long getTimeFirstSearch() {
		return timeFirstSearch;
	}
	public void setTimeFirstSearch(long timeFirstSearch) {
		this.timeFirstSearch = timeFirstSearch;
	}
	public long getTimeFirstResults() {
		return timeFirstResults;
	}
	public void setTimeFirstResults(long timeFirstResults) {
		this.timeFirstResults = timeFirstResults;
	}
	
	public int getDaysCount() {
		return daysCount;
	}
	public void setDaysCount(int daysCount) {
		this.daysCount = daysCount;
	}
	
	public String getMainRate() {
		return mainRate;
	}
	public void setMainRate(String mainRate) {
		this.mainRate = mainRate;
	}
	
	public String getCurrentDate() {
		return CurrentDate;
	}
	public void setCurrentDate(String currentDate) {
		CurrentDate = currentDate;
	}
	public ArrayList<String> getResultsPage_ActivityTypes() {
		return resultsPage_ActivityTypes;
	}
	public void setResultsPage_ActivityTypes(
			ArrayList<String> resultsPage_ActivityTypes) {
		this.resultsPage_ActivityTypes = resultsPage_ActivityTypes;
	}
	public ArrayList<String> getResultsPage_City() {
		return resultsPage_City;
	}
	public void setResultsPage_City(ArrayList<String> resultsPage_City) {
		this.resultsPage_City = resultsPage_City;
	}
	public ArrayList<String> getResultsPage_Period() {
		return resultsPage_Period;
	}
	public void setResultsPage_Period(ArrayList<String> resultsPage_Period) {
		this.resultsPage_Period = resultsPage_Period;
	}
	public ArrayList<String> getResultsPage_RateType() {
		return resultsPage_RateType;
	}
	public void setResultsPage_RateType(ArrayList<String> resultsPage_RateType) {
		this.resultsPage_RateType = resultsPage_RateType;
	}
	public ArrayList<String> getResultsPage_QTY() {
		return resultsPage_QTY;
	}
	public void setResultsPage_QTY(ArrayList<String> resultsPage_QTY) {
		this.resultsPage_QTY = resultsPage_QTY;
	}
	public ArrayList<String> getResultsPage_DailyRate() {
		return resultsPage_DailyRate;
	}
	public void setResultsPage_DailyRate(ArrayList<String> resultsPage_DailyRate) {
		this.resultsPage_DailyRate = resultsPage_DailyRate;
	}
	public String getShoppingCart_TotalActivityValue() {
		return shoppingCart_TotalActivityValue;
	}
	public void setShoppingCart_TotalActivityValue(
			String shoppingCart_TotalActivityValue) {
		this.shoppingCart_TotalActivityValue = shoppingCart_TotalActivityValue;
	}
	public String getShoppingCart_MyBasketTotalValue() {
		return shoppingCart_MyBasketTotalValue;
	}
	public void setShoppingCart_MyBasketTotalValue(
			String shoppingCart_MyBasketTotalValue) {
		this.shoppingCart_MyBasketTotalValue = shoppingCart_MyBasketTotalValue;
	}
	
	
	
	
	
	
}
