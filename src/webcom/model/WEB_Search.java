package webcom.model;

import java.util.ArrayList;

public class WEB_Search {

	public String scenarioCount;
	public String sellingCurrency; 
	public String country; 
	public String destination; 
	public String dateFrom; 
	public String dateTo; 
	public String adults; 
	public String children; 
	public String ageOfChildren; 
	public String programCategory; 
	public String preferCurrency; 
	public String promotionCode;
	public String activityName;
	public String activityDate;
	public String scenariotype;
	public String booking_channel;
	public String quotationReq;
	
	private ArrayList<WEB_PaymentDetails> paymentDetailsInfo			= new ArrayList<WEB_PaymentDetails>();
	private ArrayList<WEB_ActivityInfo> inventoryInfo			= new ArrayList<WEB_ActivityInfo>();
	
	public void addPaymentInfo(WEB_PaymentDetails payment)
	{
		paymentDetailsInfo.add(payment);
	}
	
	public void addInventoryInfo(WEB_ActivityInfo inventoryRecords)
	{
		inventoryInfo.add(inventoryRecords);
	}

	
	
	public String getQuotationReq() {
		return quotationReq;
	}

	public void setQuotationReq(String quotationReq) {
		this.quotationReq = quotationReq;
	}

	public ArrayList<WEB_PaymentDetails> getPaymentDetailsInfo() {
		return paymentDetailsInfo;
	}

	public void setPaymentDetailsInfo(ArrayList<WEB_PaymentDetails> paymentDetailsInfo) {
		this.paymentDetailsInfo = paymentDetailsInfo;
	}

	public ArrayList<WEB_ActivityInfo> getInventoryInfo() {
		return inventoryInfo;
	}

	public void setInventoryInfo(ArrayList<WEB_ActivityInfo> inventoryInfo) {
		this.inventoryInfo = inventoryInfo;
	}

	public String getBooking_channel() {
		return booking_channel;
	}
	public void setBooking_channel(String booking_channel) {
		this.booking_channel = booking_channel;
	}
	public String getScenariotype() {
		return scenariotype;
	}
	public void setScenariotype(String scenariotype) {
		this.scenariotype = scenariotype;
	}
	public String getActivityDate() {
		return activityDate;
	}
	public void setActivityDate(String activityDate) {
		this.activityDate = activityDate;
	}
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	public String getSellingCurrency() {
		return sellingCurrency;
	}
	public void setSellingCurrency(String sellingCurrency) {
		this.sellingCurrency = sellingCurrency;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getDateFrom() {
		return dateFrom;
	}
	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}
	public String getDateTo() {
		return dateTo;
	}
	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}
	public String getAdults() {
		return adults;
	}
	public void setAdults(String adults) {
		this.adults = adults;
	}
	public String getChildren() {
		return children;
	}
	public void setChildren(String children) {
		this.children = children;
	}
	public String getAgeOfChildren() {
		return ageOfChildren;
	}
	public void setAgeOfChildren(String ageOfChildren) {
		this.ageOfChildren = ageOfChildren;
	}
	public String getProgramCategory() {
		return programCategory;
	}
	public void setProgramCategory(String programCategory) {
		this.programCategory = programCategory;
	}
	public String getPreferCurrency() {
		return preferCurrency;
	}
	public void setPreferCurrency(String preferCurrency) {
		this.preferCurrency = preferCurrency;
	}
	public String getPromotionCode() {
		return promotionCode;
	}
	public void setPromotionCode(String promotionCode) {
		this.promotionCode = promotionCode;
	}
	public String getScenarioCount() {
		return scenarioCount;
	}
	public void setScenarioCount(String scenarioCount) {
		this.scenarioCount = scenarioCount;
	} 
	
	
	
	
}
