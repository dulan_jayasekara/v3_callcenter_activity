package webcom.model;

import java.util.ArrayList;
import java.util.List;

public class WEB_ProgramResponse {
	
	private String dateFrom = "";
	private String dateTo = "";
	private String activityName = "";
	private String currencyCode = "";
	private String url = "";
	private String hotelRate = "";
	private String childAgeFrom = "";
	private String childAgeTo = "";
	private String adultCount = "";
	private String childCount = "";
	private String destination = "";
	private String actDescription = "";
	private String rate = "";
	private String totalAmount = "";
	private String activityStatus = "";
	
	private String adult_qty = "";
	private String tourDate = "";
	private String itemCity = "";
	private String currency = "";
	private String gross_Total = "";
	private String gross_Adult = "";
	
	private ArrayList<String> gross_Child;
	private ArrayList<String> qty_Child;
	private ArrayList<String> age_Child;
	
	
	
	public ArrayList<String> getGross_Child() {
		return gross_Child;
	}
	public void setGross_Child(ArrayList<String> gross_ChildList) {
		this.gross_Child = gross_ChildList;
	}
	public ArrayList<String> getQty_Child() {
		return qty_Child;
	}
	public void setQty_Child(ArrayList<String> qty_ChildList) {
		this.qty_Child =  qty_ChildList;
	}
	public ArrayList<String> getAge_Child() {
		return age_Child;
	}
	public void setAge_Child(ArrayList<String> age_ChildList) {
		this.age_Child =  age_ChildList;
	}
	public String getAdult_qty() {
		return adult_qty;
	}
	public void setAdult_qty(String adult_qty) {
		this.adult_qty = adult_qty;
	}
	
	public String getTourDate() {
		return tourDate;
	}
	public void setTourDate(String tourDate) {
		this.tourDate = tourDate;
	}
	public String getItemCity() {
		return itemCity;
	}
	public void setItemCity(String itemCity) {
		this.itemCity = itemCity;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getGross_Total() {
		return gross_Total;
	}
	public void setGross_Total(String gross_Total) {
		this.gross_Total = gross_Total;
	}
	public String getGross_Adult() {
		return gross_Adult;
	}
	public void setGross_Adult(String gross_Adult) {
		this.gross_Adult = gross_Adult;
	}
	
	
	public String getActivityStatus() {
		return activityStatus;
	}
	public void setActivityStatus(String activityStatus) {
		this.activityStatus = activityStatus;
	}
	public String getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}
	public String getDateFrom() {
		return dateFrom;
	}
	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}
	public String getDateTo() {
		return dateTo;
	}
	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getHotelRate() {
		return hotelRate;
	}
	public void setHotelRate(String hotelRate) {
		this.hotelRate = hotelRate;
	}
	public String getChildAgeFrom() {
		return childAgeFrom;
	}
	public void setChildAgeFrom(String childAgeFrom) {
		this.childAgeFrom = childAgeFrom;
	}
	public String getChildAgeTo() {
		return childAgeTo;
	}
	public void setChildAgeTo(String childAgeTo) {
		this.childAgeTo = childAgeTo;
	}
	public String getAdultCount() {
		return adultCount;
	}
	public void setAdultCount(String adultCount) {
		this.adultCount = adultCount;
	}
	public String getChildCount() {
		return childCount;
	}
	public void setChildCount(String childCount) {
		this.childCount = childCount;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getActDescription() {
		return actDescription;
	}
	public void setActDescription(String actDescription) {
		this.actDescription = actDescription;
	}
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	

}
