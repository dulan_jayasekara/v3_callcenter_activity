package webcom.model;

import webcom.reader.WEB_TORatesLogics;
import webcom.types.WEB_TourOperatorType;

public class WEB_ActivityInfo {
	
	private String scenarioCount;
	private String activityName;
	private String supplier_Currency;
	private String activityType;
	private String netRate;
	private String paymentPage_SubTotal;
	private String paymentPage_TotalTaxnOther;
	private String paymentPage_Total;
	private String paymentPage_AmountNow;
	private String paymentPage_AmountDue;
	private String profir_Markup;
	private String standardCancell_Type;
	private String standardCancell_Value;
	private String noShow_Type;
	private String noShow_Value;
	private String applyIfLessThan;
	private String cancelBuffer;
	private String supplierName;
	private String supplierAddress;
	private String supplierTP;
	private String inventoryType;
	private String activityDescription;
	private String creditCardFee;
	private String paymentType;
	
	public WEB_TourOperatorType userTypeDC_B2B;
	public String toActivityNetRate;
	public String toActivityPM;
	public String toActivity_SalesTaxType;
	public String toActivity_SalesTaxValue;
	public String toActivity_MisFeeType;
	public String toActivity_MisFeeValue;
	public String discountYesORNo;;
	
	
	
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public String getDiscountYesORNo() {
		return discountYesORNo;
	}
	public void setDiscountYesORNo(String discountYesORNo) {
		this.discountYesORNo = discountYesORNo;
	}
	public WEB_TourOperatorType getUserTypeDC_B2B() {
		return userTypeDC_B2B;
	}
	public void setUserTypeDC_B2B(String userTypeDC_B2B) {
		this.userTypeDC_B2B = WEB_TourOperatorType.getTourOperatorType(userTypeDC_B2B);
	}
	public String getToActivityNetRate() {
		return toActivityNetRate;
	}
	public void setToActivityNetRate(String toActivityNetRate) {
		this.toActivityNetRate = toActivityNetRate;
	}
	public String getToActivityPM() {
		return toActivityPM;
	}
	public void setToActivityPM(String toActivityPM) {
		this.toActivityPM = toActivityPM;
	}
	public String getToActivity_SalesTaxType() {
		return toActivity_SalesTaxType;
	}
	public void setToActivity_SalesTaxType(String toActivity_SalesTaxType) {
		this.toActivity_SalesTaxType = toActivity_SalesTaxType;
	}
	public String getToActivity_SalesTaxValue() {
		return toActivity_SalesTaxValue;
	}
	public void setToActivity_SalesTaxValue(String toActivity_SalesTaxValue) {
		this.toActivity_SalesTaxValue = toActivity_SalesTaxValue;
	}
	public String getToActivity_MisFeeType() {
		return toActivity_MisFeeType;
	}
	public void setToActivity_MisFeeType(String toActivity_MisFeeType) {
		this.toActivity_MisFeeType = toActivity_MisFeeType;
	}
	public String getToActivity_MisFeeValue() {
		return toActivity_MisFeeValue;
	}
	public void setToActivity_MisFeeValue(String toActivity_MisFeeValue) {
		this.toActivity_MisFeeValue = toActivity_MisFeeValue;
	}
	public String getCreditCardFee() {
		return creditCardFee;
	}
	public void setCreditCardFee(String creditCardFee) {
		this.creditCardFee = creditCardFee;
	}
	public String getScenarioCount() {
		return scenarioCount;
	}
	public void setScenarioCount(String scenarioCount) {
		this.scenarioCount = scenarioCount;
	}
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	public String getSupplier_Currency() {
		return supplier_Currency;
	}
	public void setSupplier_Currency(String supplier_Currency) {
		this.supplier_Currency = supplier_Currency;
	}
	public String getActivityType() {
		return activityType;
	}
	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}
	public String getNetRate() {
		return netRate;
	}
	public void setNetRate(String netRate) {
		this.netRate = netRate;
	}
	
	
	public String getPaymentPage_SubTotal() {
		return paymentPage_SubTotal;
	}
	public void setPaymentPage_SubTotal(String paymentPage_SubTotal) {
		this.paymentPage_SubTotal = paymentPage_SubTotal;
	}
	public String getPaymentPage_TotalTaxnOther() {
		return paymentPage_TotalTaxnOther;
	}
	public void setPaymentPage_TotalTaxnOther(String paymentPage_TotalTaxnOther) {
		this.paymentPage_TotalTaxnOther = paymentPage_TotalTaxnOther;
	}
	public String getPaymentPage_Total() {
		return paymentPage_Total;
	}
	public void setPaymentPage_Total(String paymentPage_Total) {
		this.paymentPage_Total = paymentPage_Total;
	}
	public String getPaymentPage_AmountNow() {
		return paymentPage_AmountNow;
	}
	public void setPaymentPage_AmountNow(String paymentPage_AmountNow) {
		this.paymentPage_AmountNow = paymentPage_AmountNow;
	}
	
	public String getPaymentPage_AmountDue() {
		return paymentPage_AmountDue;
	}
	public void setPaymentPage_AmountDue(String paymentPage_AmountDue) {
		this.paymentPage_AmountDue = paymentPage_AmountDue;
	}
	
	public String getProfir_Markup() {
		return profir_Markup;
	}
	public void setProfir_Markup(String profir_Markup) {
		this.profir_Markup = profir_Markup;
	}
	public String getStandardCancell_Type() {
		return standardCancell_Type;
	}
	public void setStandardCancell_Type(String standardCancell_Type) {
		this.standardCancell_Type = standardCancell_Type;
	}
	public String getStandardCancell_Value() {
		return standardCancell_Value;
	}
	public void setStandardCancell_Value(String standardCancell_Value) {
		this.standardCancell_Value = standardCancell_Value;
	}
	public String getNoShow_Type() {
		return noShow_Type;
	}
	public void setNoShow_Type(String noShow_Type) {
		this.noShow_Type = noShow_Type;
	}
	public String getNoShow_Value() {
		return noShow_Value;
	}
	public void setNoShow_Value(String noShow_Value) {
		this.noShow_Value = noShow_Value;
	}
	public String getApplyIfLessThan() {
		return applyIfLessThan;
	}
	public void setApplyIfLessThan(String applyIfLessThan) {
		this.applyIfLessThan = applyIfLessThan;
	}
	public String getCancelBuffer() {
		return cancelBuffer;
	}
	public void setCancelBuffer(String cancelBuffer) {
		this.cancelBuffer = cancelBuffer;
	}
	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	public String getSupplierAddress() {
		return supplierAddress;
	}
	public void setSupplierAddress(String supplierAddress) {
		this.supplierAddress = supplierAddress;
	}
	public String getSupplierTP() {
		return supplierTP;
	}
	public void setSupplierTP(String supplierTP) {
		this.supplierTP = supplierTP;
	}
	public String getInventoryType() {
		return inventoryType;
	}
	public void setInventoryType(String inventoryType) {
		this.inventoryType = inventoryType;
	}
	public String getActivityDescription() {
		return activityDescription;
	}
	public void setActivityDescription(String activityDescription) {
		this.activityDescription = activityDescription;
	}
	
	
	
	
}
