package webcom.model;

public class WEB_AvailabilityResponse {
	
	private String checkIn= "";
	private String checkOut= "";
	private String activityName= "";
	private String currencyCode= "";
	private String url= "";
	private String hotelRate= "";
	private String childAgeFrom= "";
	private String childAgeTo= "";
	private String adultCount= "";
	private String childCount= "";
	private String destination= "";
	private String actDescription= "";
	private String rate= "";
	
	private String city= "";
	private String duration= "";
	private String currencyType= "";
	private String price= "";
	private String confirmation= "";
	private int xmlActivityCount= 0;
	
	
	
	
	public int getXmlActivityCount() {
		return xmlActivityCount;
	}
	public void setXmlActivityCount(int xmlActivityCount) {
		this.xmlActivityCount = xmlActivityCount;
	}
	public String getConfirmation() {
		return confirmation;
	}
	public void setConfirmation(String confirmation) {
		this.confirmation = confirmation;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public String getCurrencyType() {
		return currencyType;
	}
	public void setCurrencyType(String currencyType) {
		this.currencyType = currencyType;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	public String getActDescription() {
		return actDescription;
	}
	public void setActDescription(String actDescription) {
		this.actDescription = actDescription;
	}
	public String getCheckIn() {
		return checkIn;
	}
	public void setCheckIn(String checkIn) {
		this.checkIn = checkIn;
	}
	public String getCheckOut() {
		return checkOut;
	}
	public void setCheckOut(String checkOut) {
		this.checkOut = checkOut;
	}
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getHotelRate() {
		return hotelRate;
	}
	public void setHotelRate(String hotelRate) {
		this.hotelRate = hotelRate;
	}
	public String getChildAgeFrom() {
		return childAgeFrom;
	}
	public void setChildAgeFrom(String childAgeFrom) {
		this.childAgeFrom = childAgeFrom;
	}
	public String getChildAgeTo() {
		return childAgeTo;
	}
	public void setChildAgeTo(String childAgeTo) {
		this.childAgeTo = childAgeTo;
	}
	public String getAdultCount() {
		return adultCount;
	}
	public void setAdultCount(String adultCount) {
		this.adultCount = adultCount;
	}
	public String getChildCount() {
		return childCount;
	}
	public void setChildCount(String childCount) {
		this.childCount = childCount;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	
	
	
}
