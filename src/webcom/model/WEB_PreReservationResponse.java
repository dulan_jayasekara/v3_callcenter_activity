package webcom.model;

public class WEB_PreReservationResponse {
	
	private String dateFrom;
	private String dateTo;
	private String activityName;
	private String currencyCode;
	private String url;
	private String hotelRate;
	private String childAgeFrom;
	private String childAgeTo;
	private String adultCount;
	private String childCount;
	private String destination;
	private String actDescription;
	private String rate;
	private String totalAmount;
	
	
	public String getDateFrom() {
		return dateFrom;
	}
	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}
	public String getDateTo() {
		return dateTo;
	}
	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getHotelRate() {
		return hotelRate;
	}
	public void setHotelRate(String hotelRate) {
		this.hotelRate = hotelRate;
	}
	public String getChildAgeFrom() {
		return childAgeFrom;
	}
	public void setChildAgeFrom(String childAgeFrom) {
		this.childAgeFrom = childAgeFrom;
	}
	public String getChildAgeTo() {
		return childAgeTo;
	}
	public void setChildAgeTo(String childAgeTo) {
		this.childAgeTo = childAgeTo;
	}
	public String getAdultCount() {
		return adultCount;
	}
	public void setAdultCount(String adultCount) {
		this.adultCount = adultCount;
	}
	public String getChildCount() {
		return childCount;
	}
	public void setChildCount(String childCount) {
		this.childCount = childCount;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getActDescription() {
		return actDescription;
	}
	public void setActDescription(String actDescription) {
		this.actDescription = actDescription;
	}
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	public String getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}
	
	

}
