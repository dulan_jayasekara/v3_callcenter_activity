package webcom.reader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.WebDriver;

import com.controller.PG_Properties;
import webcom.model.WEB_ActivityDetails;
import webcom.model.WEB_AvailabilityRequest;
import webcom.model.WEB_AvailabilityResponse;
import webcom.model.WEB_CancellationPolicyRequest;
import webcom.model.WEB_CancellationPolicyResponse;
import webcom.model.WEB_ProgramRequest;
import webcom.model.WEB_ProgramResponse;
import webcom.model.WEB_ReservationRequest;
import webcom.model.WEB_ReservationResponse;
import webcom.model.WEB_Search;
import webcom.model.WEB_ThirdSearch;

public class WEB_GTAReader extends WEB_XmlReader {
	
	
	WEB_ThirdSearch searchResults = new WEB_ThirdSearch();
	WEB_AvailabilityRequest a_Request = new WEB_AvailabilityRequest();
	WEB_AvailabilityResponse a_Response = new WEB_AvailabilityResponse();
	WEB_ProgramRequest p_Request = new WEB_ProgramRequest();
	WEB_ProgramResponse p_Response = new WEB_ProgramResponse();
	WEB_CancellationPolicyRequest cancelPolicy_Req = new WEB_CancellationPolicyRequest();
	WEB_CancellationPolicyResponse cancelPolicy_Response = new WEB_CancellationPolicyResponse();
	WEB_ReservationRequest reser_Req = new WEB_ReservationRequest();
	WEB_ReservationResponse reser_Response = new WEB_ReservationResponse();
	WEB_ActivityDetails activitydetails = new WEB_ActivityDetails();
	Map<String, String> currencyMap = new HashMap<String, String>();
	
	private String noShowMatching, activityMainPolicy, activityCanPolicy1, activityCanPolicy2 ;
	private int rateXML, rateXMLCanPolicy, rateXMLReservation ;
	private String excelCurrency, excelCurrencyCanPolicy, excelCurrencyReservation;
	private double childRate = 0;
	private String excelRateType;
	private final double profitMarkup = 10.0;
	private int paxCountReservation;
	
	Document doc;
	private StringBuffer  PrintWriter;
	private int ScenarioCount = 0;  
	List<String> ageList_AReq, ageList_CanReq, ageList_PReq; 
	List<String> ageList_AResponse, ageList_PResponse ;
	ArrayList<String> gross_ChildList = new ArrayList<String>();
	ArrayList<String> qty_ChildList = new ArrayList<String>();
	ArrayList<String> age_ChildList = new ArrayList<String>();
	ArrayList<String> childAge_resReq = new ArrayList<String>();
	ArrayList<String> childAge_resResponse = new ArrayList<String>();
	private int child_NoH, child_NoH_P, child_NoH_can ;
	private int childCount, childCount_P, childCount_can;
	
	private int TestCaseCount_AReq, TestCaseCount_PReq, TestCaseCount_canReq, TestCaseCount_ResReq;
	private int TestCaseCount_AResponse, TestCaseCount_PResponse, TestCaseCount_canRes, TestCaseCount_ResResponse ;
	

	public WEB_GTAReader(WEB_ActivityDetails activityDetails, WEB_ThirdSearch search ,Map<String, String> CurrencyMap, WebDriver Driver) {
		super(Driver );
		this.activitydetails = activityDetails;
		this.currencyMap = CurrencyMap;
		this.searchResults = search;
		
		ageList_AReq = new ArrayList<String>();
		ageList_CanReq = new ArrayList<String>();
		ageList_PReq = new ArrayList<String>();
		ageList_AResponse = new ArrayList<String>();
		ageList_PResponse = new ArrayList<String>();
	}
	
	
	public WEB_AvailabilityRequest readAvailabilityRequests(){
						
		String windowHandle = null;
		
		try{	
			String date = searchResults.getActivityDate();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat formatIn = new SimpleDateFormat("dd-MMM-yyyy");
			Date instance = formatIn.parse(searchResults.getActivityDate());  		
			String reportDate = df.format(instance);
			
			
			windowHandle = this.getWindow("gtaactivity_AvailabilityRequest_"+reportDate+"_" , driver , activitydetails.getTraceId());
		}
		
		catch (Exception e) {
			e.getMessage();
		}
		
		String PageSource = this.driver.getPageSource();
		doc = Jsoup.parse(PageSource);
		a_Request.setURL(driver.getCurrentUrl());
		
		try {
			
			for (Element e : doc.select("RequestDetails")) {
								
	        	String tourDate = e.getElementsByTag("TourDate").text();
	        	a_Request.setTourDate(tourDate);
	        	
	        	String destination = e.getElementsByTag("ItemDestination").first().attr("DestinationCode"); 
	        	a_Request.setDestination(destination);
	        	
	        	String numberOfAdults = e.getElementsByTag("NumberOfAdults").text();
	        	a_Request.setAdultCount(numberOfAdults);
	        	
	        	Elements ageEle = null;				
				try {				
					ageEle = doc.getElementsByTag("Age");				
				
				} catch (Exception e1) {
					e1.printStackTrace();
				}				
				childCount = ageEle.size();
	        	
	        	
				for (Element e3 : e.select("Children")) {
					for (Element e4 : e3.select("Age")) {
						
						String age = e4.text(); 
						ageList_AReq.add(age);
						child_NoH ++;
						
					}					
				}
	        	        		        	
			}
			
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		ArrayList<String> list = new ArrayList<String>(driver.getWindowHandles());
		driver.close();
		list.remove(list.get(0));
		driver.switchTo().window(windowHandle);
			
		return a_Request;
	}
	
	
	public WEB_AvailabilityResponse readAvailabilityResponses(){
		
		String windowHandle = null;
		
		try{		
			
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat formatIn = new SimpleDateFormat("dd-MMM-yyyy");
			Date instance = formatIn.parse(searchResults.getActivityDate());  		
			String reportDate = df.format(instance);
			
			windowHandle = this.getWindow("gtaactivity_AvailabilityResponse_"+reportDate+"_" ,driver, activitydetails.getTraceId());
		}
		
		catch (Exception e) {
			e.getMessage();
		}
		
		String PageSource = this.driver.getPageSource();
		doc = Jsoup.parse(PageSource);
		a_Response.setUrl(driver.getCurrentUrl());
		
		
		try {
			
			Elements activities = null;		   			
			activities = doc.getElementsByTag("Sightseeing");					
			int count = activities.size();
			a_Response.setXmlActivityCount(count);
						
			//activityName = search.getActivityName();
			String actName = searchResults.getActivityName();
			
			for (Element e : doc.select("Sightseeing")) {
				
				String xmlActivityName = e.getElementsByTag("Item").text();
				
				if(actName.toLowerCase().contains(xmlActivityName.toLowerCase())){
					
					String city = e.getElementsByTag("City").first().attr("Code");
					a_Response.setCity(city);
					
					String duration = e.getElementsByTag("Duration").text();
					a_Response.setDuration(duration);
					
					String currencyType = e.getElementsByTag("ItemPrice").first().attr("Currency");
					a_Response.setCurrencyType(currencyType);
					
					String itemRate = e.getElementsByTag("ItemPrice").text();
					a_Response.setPrice(itemRate);
					
					String confirmation =   e.getElementsByTag("Confirmation").text();
					a_Response.setConfirmation(confirmation);
					
				}
								
			}
			
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		ArrayList<String> list = new ArrayList<String>(driver.getWindowHandles());
		driver.close();
		list.remove(list.get(0));
		driver.switchTo().window(windowHandle);
		
		
		return a_Response;
	}
	
	
	public void getGTAAvailabilityReport(WEB_ThirdSearch search, WebDriver driver){
		
		PrintWriter = new StringBuffer();
		ScenarioCount ++;
		
		try {
			
			PrintWriter.append("<html class='report'><head><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"> </head>");
			PrintWriter.append("<div class='header'><img height=\"19px\" src=\"menu_rez.png\"><p class='Hedding1'>XML Report >> Availability Request Type >> GTA</p></div>");
			PrintWriter.append("<body>");
			
			PrintWriter.append("<br><br>");
			PrintWriter.append("<div style=\"border:1px solid;border-radius:3px 3px 3px 3px;background-color:#8D8D86; width:50%;\">");
			PrintWriter.append("<p class='InfoSup'style='font-weight: bold;'>"+ScenarioCount+")<br></p>");
			PrintWriter.append("<p class='InfoSub'>Current Activity Reservation Criteria</p>");
			PrintWriter.append("<p class='InfoSub'>Country name :- "+search.getCountry()+"</p>");
			PrintWriter.append("<p class='InfoSub'>Date From :- "+search.getDateFrom()+"</p>");
			PrintWriter.append("<p class='InfoSub'>Date To :- "+search.getDateTo()+"</p>");
			PrintWriter.append("</div>");
			PrintWriter.append("<br>");
			
			
			PrintWriter.append("<br><br><table><tr> <th>XML Type</th> <th>Supplier - GTA</th> </tr>");
			PrintWriter.append("<tr> <td>Availability_Request</td> <td> <a href="+a_Request.getURL()+">request</a> </td> <tr>");
			PrintWriter.append("<tr> <td>Availability_Response</td> <td> <a href="+a_Response.getUrl()+">response</a> </td> <tr>");
			PrintWriter.append("</table>");
			
	
			TestCaseCount_AReq = 1;
			PrintWriter.append("<br><br><table ><tr><th>Test Case No</th><th>Test Description</th><th>Expect - [Excel Value]</th><th>Actual - [Xml Result]</th><th>Test Status</th></tr>");
			
			String activDate = search.getActivityDate();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat formatIn = new SimpleDateFormat("dd-MMM-yyyy");
			Date instance = formatIn.parse(activDate);  		
			String activDateFormat = df.format(instance);
			
			//////
			
			PrintWriter.append("<tr> <td>"+TestCaseCount_AReq+"</td> <td>Activity Date</td>");
			PrintWriter.append("<td>"+activDateFormat+"</td>");
						
			if(activDateFormat.equals(a_Request.getTourDate())){
					
				PrintWriter.append("<td>"+a_Request.getTourDate()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+a_Request.getTourDate()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			///////
			
			TestCaseCount_AReq ++;
			PrintWriter.append("<tr><td>"+TestCaseCount_AReq+"</td> <td>Activity Adult Count</td>");
			PrintWriter.append("<td>"+search.getAdults()+"</td>");
						
			if(search.getAdults().equals(a_Request.getAdultCount())){
					
				PrintWriter.append("<td>"+a_Request.getAdultCount()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+a_Request.getAdultCount()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			/////
			
			TestCaseCount_AReq++;
			PrintWriter.append("<tr><td>"+TestCaseCount_AReq+"</td> <td>Activity Child Count</td>");
			PrintWriter.append("<td>"+search.getChildren()+"</td>");
						
			if(Integer.toString(child_NoH).equals(search.getChildren())){
					
				PrintWriter.append("<td>"+child_NoH+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+child_NoH+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			/////
			
			TestCaseCount_AReq++;
			PrintWriter.append("<tr><td>"+TestCaseCount_AReq+"</td> <td>Activity Child Count from the AGE tag</td>");
			PrintWriter.append("<td>"+search.getChildren()+"</td>");
						
			if(Integer.toString(childCount).equals(search.getChildren())){
					
				PrintWriter.append("<td>"+childCount+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+childCount+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			//////
			
			String childAges = search.getAgeOfChildren();
			String[] partsEA = childAges.split("#");
			
			List<String> valueList = new ArrayList<String>();
			
			for (String value : partsEA) {
				valueList.add(value);
			}
			
			for (int i = 0; i < valueList.size(); i++) {
				
				TestCaseCount_AReq++;
				PrintWriter.append("<tr><td>"+TestCaseCount_AReq+"</td> <td>Activity Child ages check - [Child "+(i+1)+"]</td>");
				PrintWriter.append("<td>"+valueList.get(i)+"</td>");
				
				if(ageList_AReq.get(i).equals(valueList.get(i))){
					
					PrintWriter.append("<td>"+ageList_AReq.get(i)+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
				}
				else{
					PrintWriter.append("<td>"+ageList_AReq.get(i)+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
			}
			
			//////
			
			String excelDest = search.getDestination();
			String xmlDest = a_Request.getDestination();	
			
			TestCaseCount_AReq++;
			PrintWriter.append("<tr><td>"+TestCaseCount_AReq+"</td> <td>Activity Destination match</td>");
			PrintWriter.append("<td>"+excelDest+"</td>");
						
			if(excelDest.toLowerCase().contains(xmlDest.toLowerCase())){
					
				PrintWriter.append("<td>"+xmlDest+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+xmlDest+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			PrintWriter.append("</table>");
			
			
		/////////
			
			TestCaseCount_AResponse = 1;	
			
			String xmlDestAres = a_Response.getCity(); 
			
			PrintWriter.append("<br><br>");
			PrintWriter.append("<div class='header'><p class='Hedding1'>XML Report >> Activity Availability Response Type >> GTA</p></div>");
			PrintWriter.append("<br><br><table ><tr><th>Test Case No</th><th>Test Description</th><th>Expect - [Excel Value]</th><th>Actual - [Xml Result]</th><th>Test Status</th></tr>");
			
			//////
			
			PrintWriter.append("<tr><td>"+TestCaseCount_AResponse+"</td> <td>Activity Program availability</td>");
			PrintWriter.append("<td>"+activitydetails.getStatus()+"</td>");
						
			if(activitydetails.getStatus().toLowerCase().equals(a_Response.getConfirmation().toLowerCase())){
					
				PrintWriter.append("<td>"+a_Response.getConfirmation()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+a_Response.getConfirmation()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			//////
			
			TestCaseCount_AResponse ++;
			PrintWriter.append("<tr><td>"+TestCaseCount_AResponse+"</td> <td>Activity city mapping</td>");
			PrintWriter.append("<td>"+search.getDestination()+"</td>");
						
			if(excelDest.toLowerCase().contains(xmlDestAres.toLowerCase())){
					
				PrintWriter.append("<td>"+a_Response.getCity()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+a_Response.getCity()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			////
			
			TestCaseCount_AResponse ++;
			PrintWriter.append("<tr><td>"+TestCaseCount_AResponse+"</td> <td>Activity Currency - Filter</td>");
			PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
						
			if(search.getSellingCurrency().toLowerCase().equals(activitydetails.getActivityCurrency().toLowerCase())){
					
				PrintWriter.append("<td>"+activitydetails.getActivityCurrency()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+activitydetails.getActivityCurrency()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			/////
			
			TestCaseCount_AResponse ++;
			PrintWriter.append("<tr><td>"+TestCaseCount_AResponse+"</td> <td>Activity Currency</td>");
			PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
						
			if(search.getSellingCurrency().toLowerCase().equals(activitydetails.getActivityCurrency().toLowerCase())){
					
				PrintWriter.append("<td>"+activitydetails.getActivityCurrency()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+activitydetails.getActivityCurrency()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			////
			
			TestCaseCount_AResponse ++;
			
			String programDuration = activitydetails.getResultsPage_Period().get(0).split(":")[1];
			
			PrintWriter.append("<tr><td>"+TestCaseCount_AResponse+"</td> <td>Activity program duration</td>");
			PrintWriter.append("<td>"+programDuration+"</td>");
						
			if(programDuration.toLowerCase().contains(a_Response.getDuration().toLowerCase())){
					
				PrintWriter.append("<td>"+a_Response.getDuration()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+a_Response.getDuration()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
			///////
			
			TestCaseCount_AResponse ++;
			
			String rateType = activitydetails.getResultsPage_RateType().get(0);
			
			if (rateType.contains("Children")) {
				
				excelRateType = "Total Package /"+search.getAdults()+" Adult(s) /"+search.getChildren()+" Children";		
			}
			else{
				
				excelRateType = "Total Package /"+search.getAdults()+" Adult(s)";
			}	
			
			PrintWriter.append("<tr><td>"+TestCaseCount_AResponse+"</td> <td>Activity Rate type</td>");
			PrintWriter.append("<td>"+rateType+"</td>");
						
			if(rateType.toLowerCase().equals(excelRateType.toLowerCase())){
					
				PrintWriter.append("<td>"+excelRateType+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+excelRateType+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			//////		
			
			TestCaseCount_AResponse ++;			
			PrintWriter.append("<tr><td>"+TestCaseCount_AResponse+"</td> <td>Activity results page and XML results</td>");
			PrintWriter.append("<td>Results page - "+activitydetails.getGtaActivity()+"</td>");
						
			if(activitydetails.getGtaActivity() == a_Response.getXmlActivityCount()){
					
				PrintWriter.append("<td>XML Results - "+a_Response.getXmlActivityCount()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>XML Results - "+a_Response.getXmlActivityCount()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			////
			
			TestCaseCount_AResponse ++;
			PrintWriter.append("<tr><td>"+TestCaseCount_AResponse+"</td> <td>Activity city mapping - Results Page</td>");
			PrintWriter.append("<td>"+search.getDestination()+"</td>");
						
			if(excelDest.equalsIgnoreCase(a_Response.getCity())){
					
				PrintWriter.append("<td>"+a_Response.getCity()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+a_Response.getCity()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
//			TestCaseCount_AResponse ++;
//			PrintWriter.append("<tr><td>"+TestCaseCount_AResponse+"</td> <td>Activity Date From - Results Page</td>");
//			PrintWriter.append("<td>"+search.getDateFrom()+"</td>");
//						
//			if(search.getDateFrom().equals(activitydetails.getResultsPage_dateFrom())){
//					
//				PrintWriter.append("<td>"+activitydetails.getResultsPage_dateFrom()+"</td>");
//				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
//			}
//			
//			else{
//				PrintWriter.append("<td>"+activitydetails.getResultsPage_dateFrom()+"</td>");
//				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
//			}
//			
//			
//			TestCaseCount_AResponse ++;
//			PrintWriter.append("<tr><td>"+TestCaseCount_AResponse+"</td> <td>Activity Date To - Results Page</td>");
//			PrintWriter.append("<td>"+search.getDateTo()+"</td>");
//						
//			if(search.getDateTo().equals(activitydetails.getResultsPage_dateTo())){
//					
//				PrintWriter.append("<td>"+activitydetails.getResultsPage_dateTo()+"</td>");
//				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
//			}
//			
//			else{
//				PrintWriter.append("<td>"+activitydetails.getResultsPage_dateTo()+"</td>");
//				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
//			}
			
//			TestCaseCount_AResponse ++;
//			PrintWriter.append("<tr><td>"+TestCaseCount_AResponse+"</td> <td>Activity Adult count - Results Page</td>");
//			PrintWriter.append("<td>"+search.getAdults()+"</td>");
//						
//			if(search.getAdults().equals(activitydetails.getResultsPage_adults())){
//					
//				PrintWriter.append("<td>"+activitydetails.getResultsPage_adults()+"</td>");
//				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
//			}
//			
//			else{
//				PrintWriter.append("<td>"+activitydetails.getResultsPage_adults()+"</td>");
//				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
//			}
//			
//			TestCaseCount_AResponse ++;
//			PrintWriter.append("<tr><td>"+TestCaseCount_AResponse+"</td> <td>Activity Child count - Results Page</td>");
//			PrintWriter.append("<td>"+search.getChildren()+"</td>");
//						
//			if(search.getChildren().equals(activitydetails.getResultsPage__childs())){
//					
//				PrintWriter.append("<td>"+activitydetails.getResultsPage__childs()+"</td>");
//				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
//			}
//			
//			else{
//				PrintWriter.append("<td>"+activitydetails.getResultsPage__childs()+"</td>");
//				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
//			}
//			
//			for (int i = 0; i < valueList.size(); i++) {
//				
//				TestCaseCount_AResponse++;
//				PrintWriter.append("<tr><td>"+TestCaseCount_AResponse+"</td> <td>Activity Child ages check - [Child "+(i+1)+" - Results Page]</td>");
//				PrintWriter.append("<td>"+valueList.get(i)+"</td>");
//				
//				if(valueList.get(i).equals(activitydetails.getResultsPage_ageOfChilds().get(i))){
//					
//					PrintWriter.append("<td>"+activitydetails.getResultsPage_ageOfChilds().get(i)+"</td>");
//					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
//				}
//				else{
//					PrintWriter.append("<td>"+activitydetails.getResultsPage_ageOfChilds().get(i)+"</td>");
//					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
//				}
//			}
			
			
			/////
			
			
			TestCaseCount_AResponse ++;			
			PrintWriter.append("<tr><td>"+TestCaseCount_AResponse+"</td> <td>Activity HB results in results page</td>");
			PrintWriter.append("<td>HB Results count - </td>");
						
			if(activitydetails.getHbActivity() > 0){
					
				PrintWriter.append("<td>"+activitydetails.getHbActivity()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+activitydetails.getHbActivity()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
			TestCaseCount_AResponse ++;			
			PrintWriter.append("<tr><td>"+TestCaseCount_AResponse+"</td> <td>Activity GTA results in results page</td>");
			PrintWriter.append("<td>GTA Results count - </td>");
						
			if(activitydetails.getGtaActivity() > 0){
					
				PrintWriter.append("<td>"+activitydetails.getGtaActivity()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+activitydetails.getGtaActivity()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
			/////
			
			TestCaseCount_AResponse ++;			
			PrintWriter.append("<tr><td>"+TestCaseCount_AResponse+"</td> <td>Activity results availability</td>");
			PrintWriter.append("<td>Results Should be available</td>");
						
			String resultsAvailable = Boolean.toString(activitydetails.isResultsAvailable());
			
			if(resultsAvailable.equals("true")){
					
				PrintWriter.append("<td>"+resultsAvailable+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+resultsAvailable+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
			TestCaseCount_AResponse ++;			
			PrintWriter.append("<tr><td>"+TestCaseCount_AResponse+"</td> <td>Activity programs availability</td>");
			PrintWriter.append("<td>Activity Program details should be available</td>");
						
			String programDisplayAvailable = Boolean.toString(activitydetails.isProgramsAvailabilityActivityTypes());
			
			if(programDisplayAvailable.equals("true")){
					
				PrintWriter.append("<td>"+programDisplayAvailable+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+programDisplayAvailable+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
			/////
			
			String dateFromToCal = search.getDateFrom();
			String dateToCal = search.getDateTo();
			SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
			
			Date d1 = null;
			Date d2 = null;
	 
			try {
				d1 = format.parse(dateFromToCal);
				d2 = format.parse(dateToCal);
	 
				//in milliseconds
				long diffD = d2.getTime() - d1.getTime();
				int diffDays = (int)(diffD / (24 * 60 * 60 * 1000));
				
				TestCaseCount_AResponse ++;			
				PrintWriter.append("<tr><td>"+TestCaseCount_AResponse+"</td> <td>Activity days count</td>");
				PrintWriter.append("<td>From :- "+dateFromToCal+" - To :- "+dateToCal+" = "+(diffDays+1)+"</td>");
						
				if(activitydetails.getDaysCount() == (diffDays+1)){
						
					PrintWriter.append("<td>Results page days count - "+activitydetails.getDaysCount()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>Results page days count - "+activitydetails.getDaysCount()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}

	 
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
			////
			
			PrintWriter.append("</table>");
			PrintWriter.append("</body></html>");
			
			BufferedWriter bwr = new BufferedWriter(new FileWriter(new File(PG_Properties.getProperty("GTA-XmlReport.ActivityTypes"))));
			bwr.write(PrintWriter.toString());
			bwr.flush();
			bwr.close();
			
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	
	
	
	public WEB_ProgramRequest readProgramRequest(){
		
		String windowHandle = null;
		
		try{				
			windowHandle = this.getWindow("gtaactivity_ProgramRequest_" ,driver, activitydetails.getTraceId());
		}
		
		catch (Exception e) {
			e.getMessage();
		}
		
		String PageSource = this.driver.getPageSource();
		doc = Jsoup.parse(PageSource);
		p_Request.setURL(driver.getCurrentUrl());
		
		try {
			
			for (Element e : doc.select("Request")) {
						
				String currency = e.getElementsByTag("RequestorPreferences").first().attr("Currency"); 
				p_Request.setCurrency(currency);
				
				String city = e.getElementsByTag("City").text();
				p_Request.setTourCity(city);
				
	        	String tourDate = e.getElementsByTag("TourDate").text();
	        	p_Request.setTourDate(tourDate);
	        	        	
	        	String numberOfAdults = e.getElementsByTag("NumberOfAdults").text();
	        	p_Request.setAdultCount(numberOfAdults);
	        	
	 
	        	Elements ageEle = null;		
	        	
				try {				
					ageEle = doc.getElementsByTag("Age");				
				
				} catch (Exception e1) {
					e1.printStackTrace();
				}				
				childCount_P = ageEle.size();
	        	
				for (Element e3 : e.select("Children")) {
					for (Element e4 : e3.select("Age")) {
						
						String age = e4.text(); 
						ageList_PReq.add(age);
						child_NoH_P ++;
						
					}					
				}
	        	        		        	
	        	System.out.println(ageList_PReq);
	        	
			}
			
					
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		ArrayList<String> list = new ArrayList<String>(driver.getWindowHandles());
		driver.close();
		list.remove(list.get(0));
		driver.switchTo().window(windowHandle);
			
		return p_Request;
				
	}
	
	
	public WEB_ProgramResponse readProgramResponse(){
		
		String windowHandle = null;
		
		try{				
			windowHandle = this.getWindow("gtaactivity_ProgramResponse_" ,driver, activitydetails.getTraceId());
		}
		
		catch (Exception e) {
			e.getMessage();
		}
		
		String PageSource = this.driver.getPageSource();
		doc = Jsoup.parse(PageSource);
		p_Response.setUrl(driver.getCurrentUrl());
		
		
		try {
			
			for (Element e : doc.select("Response")){
			
				String itemCity = e.getElementsByTag("ItemCity").text();
				p_Response.setItemCity(itemCity);
				
				String currency = e.getElementsByTag("ItemPrice").first().attr("Currency"); 
				p_Response.setCurrencyCode(currency);
				
				String gross_Total = e.getElementsByTag("ItemPrice").first().attr("Gross"); 
				p_Response.setGross_Total(gross_Total);
				
				String tourDate = e.getElementsByTag("TourDate").text();
				p_Response.setTourDate(tourDate);
				
				
				Elements PaxTypes = null;		
    			
				PaxTypes = doc.getElementsByTag("PaxType");	
				
				
				for (int i = 0; i < PaxTypes.size(); i++) {
					
					if (i==0) {
						
						String gross_Adult = PaxTypes.get(i).attr("Gross");
						p_Response.setGross_Adult(gross_Adult);
						
						String qty_Adult = PaxTypes.get(i).attr("Quantity");
						p_Response.setAdult_qty(qty_Adult);
					}
					
					else if(0<i) {
						
						String gross_Child = PaxTypes.get(i).attr("Gross");
						gross_ChildList.add(gross_Child);
												
						String qty_Child = PaxTypes.get(i).attr("Quantity");
						qty_ChildList.add(qty_Child);
												
						String age_Child = PaxTypes.get(i).attr("Age");
						age_ChildList.add(age_Child);
						
						
					}					
				}
				
				p_Response.setGross_Child(gross_ChildList);
				p_Response.setQty_Child(qty_ChildList);
				p_Response.setAge_Child(age_ChildList);
				
			}
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		ArrayList<String> list = new ArrayList<String>(driver.getWindowHandles());
		driver.close();
		list.remove(list.get(0));
		driver.switchTo().window(windowHandle);
		
		
		return p_Response;
		
	}
	
	
	public void getGTAProgramReport(WEB_ThirdSearch search, WebDriver driver){

		PrintWriter = new StringBuffer();
		ScenarioCount ++;
		
		try {
			
			PrintWriter.append("<html class='report'><head><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"> </head>");
			PrintWriter.append("<div class='header'><img height=\"19px\" src=\"menu_rez.png\"><p class='Hedding1'>XML Report >> Activity Program Request Type >> GTA</p></div>");
			PrintWriter.append("<body>");
			
			PrintWriter.append("<br><br>");
			PrintWriter.append("<div style=\"border:1px solid;border-radius:3px 3px 3px 3px;background-color:#8D8D86; width:50%;\">");
			PrintWriter.append("<p class='InfoSup'style='font-weight: bold;'>"+ScenarioCount+")<br></p>");
			PrintWriter.append("<p class='InfoSub'>Current Activity Reservation Criteria</p>");
			PrintWriter.append("<p class='InfoSub'>Country name :- "+search.getCountry()+"</p>");
			PrintWriter.append("<p class='InfoSub'>Date From :- "+search.getDateFrom()+"</p>");
			PrintWriter.append("<p class='InfoSub'>Date To :- "+search.getDateTo()+"</p>");
			PrintWriter.append("</div>");
			PrintWriter.append("<br>");
			
			PrintWriter.append("<br><br><table><tr> <th>XML Type</th> <th>Supplier - GTA</th> </tr>");
			PrintWriter.append("<tr> <td>Program_Request</td> <td> <a href="+p_Request.getURL()+">request</a> </td> <tr>");
			PrintWriter.append("<tr> <td>Program_Response</td> <td> <a href="+p_Response.getUrl()+">response</a> </td> <tr>");
			PrintWriter.append("</table>");
			
			
			TestCaseCount_PReq = 1;
			PrintWriter.append("<br><br><table ><tr><th>Test Case No</th><th>Test Description</th><th>Expect - [Excel Value]</th><th>Actual - [Xml Result]</th><th>Test Status</th></tr>");
			
			PrintWriter.append("<tr><td>"+TestCaseCount_PReq+"</td> <td>Activity city mapping</td>");
			PrintWriter.append("<td>"+search.getDestination()+"</td>");
						
			if(search.getDestination().toLowerCase().contains(p_Request.getTourCity().toLowerCase())){
					
				PrintWriter.append("<td>"+p_Request.getTourCity()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+p_Request.getTourCity()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			/////
			
			TestCaseCount_PReq ++;
			
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat formatIn = new SimpleDateFormat("dd-MMM-yyyy");
			Date instance = formatIn.parse(search.getActivityDate());  		
			String reportDate = df.format(instance);
			
			PrintWriter.append("<tr><td>"+TestCaseCount_PReq+"</td> <td>Activity date mapping</td>");
			PrintWriter.append("<td>"+reportDate+"</td>");
						
			if(p_Request.getTourDate().equals(reportDate)){
					
				PrintWriter.append("<td>"+p_Request.getTourDate()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+p_Request.getTourDate()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			////
			
			TestCaseCount_PReq ++;			
			PrintWriter.append("<tr><td>"+TestCaseCount_PReq+"</td> <td>Activity Adult Count</td>");
			PrintWriter.append("<td>"+search.getAdults()+"</td>");
						
			if(search.getAdults().equals(p_Request.getAdultCount())){
					
				PrintWriter.append("<td>"+p_Request.getAdultCount()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+p_Request.getAdultCount()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			/////
			
			TestCaseCount_PReq ++;
			PrintWriter.append("<tr><td>"+TestCaseCount_PReq+"</td> <td>Activity Child Count from AGE tag</td>");
			PrintWriter.append("<td>"+search.getChildren()+"</td>");
						
			if(Integer.toString(childCount_P).equals(search.getChildren())){
					
				PrintWriter.append("<td>"+childCount_P+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+childCount_P+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			/////
			
			TestCaseCount_PReq ++;
			PrintWriter.append("<tr><td>"+TestCaseCount_PReq+"</td> <td>Activity Currency</td>");
			PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
						
			if(search.getSellingCurrency().toLowerCase().equals(activitydetails.getActivityCurrency().toLowerCase())){
					
				PrintWriter.append("<td>"+activitydetails.getActivityCurrency()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+activitydetails.getActivityCurrency()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			/////
			
			String childAges = search.getAgeOfChildren();
			String[] partsEA = childAges.split("#");
			
			List<String> valueList = new ArrayList<String>();
			
			for (String value : partsEA) {
				valueList.add(value);
			}
			
			for (int i = 0; i < valueList.size(); i++) {
				
				TestCaseCount_PReq ++;
				
				PrintWriter.append("<tr><td>"+TestCaseCount_PReq+"</td> <td>Activity Child ages check - [Child "+(i+1)+"]</td>");
				PrintWriter.append("<td>"+valueList.get(i)+"</td>");
				
				if(ageList_PReq.get(i).equals(valueList.get(i))){
					
					PrintWriter.append("<td>"+ageList_PReq.get(i)+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
				}
				else{
					PrintWriter.append("<td>"+ageList_PReq.get(i)+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
			}
			
			
			PrintWriter.append("</table>");
				
			/////
			
			TestCaseCount_PResponse = 1;			
			PrintWriter.append("<br><br>");
			PrintWriter.append("<div class='header'><p class='Hedding1'>XML Report >> Activity Program Response Type >> GTA</p></div>");
			PrintWriter.append("<br><br><table ><tr><th>Test Case No</th><th>Test Description</th><th>Expect - [Excel Value]</th><th>Actual - [Xml Result]</th><th>Test Status</th></tr>");
			
			PrintWriter.append("<tr><td>"+TestCaseCount_PResponse+"</td> <td>Activity city mapping</td>");
			PrintWriter.append("<td>"+search.getDestination()+"</td>");
						
			if(search.getDestination().toLowerCase().contains(p_Response.getItemCity().toLowerCase())){
					
				PrintWriter.append("<td>"+p_Response.getItemCity()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+p_Response.getItemCity()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			//////
			
			TestCaseCount_PResponse ++;
			PrintWriter.append("<tr><td>"+TestCaseCount_PResponse+"</td> <td>Activity Currency</td>");
			PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
						
			if(activitydetails.getActivityCurrency().toLowerCase().equals(search.getSellingCurrency().toLowerCase())){
					
				PrintWriter.append("<td>"+activitydetails.getActivityCurrency()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+activitydetails.getActivityCurrency()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			/////
			
			TestCaseCount_PResponse ++;			
			PrintWriter.append("<tr><td>"+TestCaseCount_PResponse+"</td> <td>Activity Adult Count</td>");
			PrintWriter.append("<td>"+search.getAdults()+"</td>");
						
			if(p_Response.getAdult_qty().equals(search.getAdults())){
					
				PrintWriter.append("<td>"+p_Response.getAdult_qty()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+p_Response.getAdult_qty()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			//////
			
			TestCaseCount_PResponse ++;
			
			DateFormat dfPResponse = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat formatInPResponse = new SimpleDateFormat("dd-MMM-yyyy");
			Date instancePResponse = formatInPResponse.parse(search.getActivityDate());  		
			String reportDatePResponse = dfPResponse.format(instancePResponse);
			
			PrintWriter.append("<tr><td>"+TestCaseCount_PResponse+"</td> <td>Activity date mapping</td>");
			PrintWriter.append("<td>"+reportDatePResponse+"</td>");
						
			if(p_Response.getTourDate().equals(reportDatePResponse)){
					
				PrintWriter.append("<td>"+p_Response.getTourDate()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+p_Response.getTourDate()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			/////
			
			String rateValue = activitydetails.getMainRate();
			excelCurrency = search.getSellingCurrency();
			
			TestCaseCount_PResponse++;
			PrintWriter.append("<tr><td>"+TestCaseCount_PResponse+"</td> <td>Activity Rate Check</td>");
			PrintWriter.append("<td>"+rateValue+"</td>");
			
			if(PG_Properties.getProperty("PortalCurrency").toLowerCase().equals(excelCurrency.toLowerCase())){
				
				Double rateX = (Double.parseDouble(p_Response.getGross_Total()) * (profitMarkup + 100)) / 100;
				rateXML = (int) Math.ceil(rateX);
										
				if(rateValue.equals(Integer.toString(rateXML))){
					
					PrintWriter.append("<td>"+Integer.toString(rateXML)+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+Integer.toString(rateXML)+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}				
			}
			
			else{
				
				for(Entry<String, String> entry: currencyMap.entrySet()) {
					if(excelCurrency.toLowerCase().equals(entry.getKey().toLowerCase())){
						
						//convert into USD		
						Double ratex = ((Double.parseDouble(p_Response.getGross_Total()) * (profitMarkup + 100)) / 100) * Double.parseDouble(entry.getValue()); 
						rateXML = (int) Math.ceil(ratex);
						
						if(rateValue.equals(Integer.toString(rateXML))){
							
							PrintWriter.append("<td>"+Integer.toString(rateXML)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+Integer.toString(rateXML)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}							
					}					
				}			
			}
					
			/////
			
			String childAgesPrespo = search.getAgeOfChildren();
			String[] partsEAPrespo = childAgesPrespo.split("#");
			
			List<String> valueListPrespo = new ArrayList<String>();
			
			for (String value : partsEAPrespo) {
				valueListPrespo.add(value);
			}
			
			for (int i = 0; i < valueListPrespo.size(); i++) {
				
				TestCaseCount_PResponse++;
				PrintWriter.append("<tr><td>"+TestCaseCount_PResponse+"</td> <td>Activity Child ages check - [Child "+(i+1)+"]</td>");
				PrintWriter.append("<td>"+valueListPrespo.get(i)+"</td>");
				
				if(p_Response.getAge_Child().get(i).equals(valueListPrespo.get(i))){
					
					PrintWriter.append("<td>"+p_Response.getAge_Child().get(i)+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
				}
				else{
					PrintWriter.append("<td>"+p_Response.getAge_Child().get(i)+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
			}
			
		
			/////
			
			double totalAmount = Double.parseDouble(p_Response.getGross_Total());
			double adultRate = Double.parseDouble(p_Response.getGross_Adult()) * Double.parseDouble(p_Response.getAdult_qty());
			
			for (int i = 0; i < p_Response.getQty_Child().size() ; i++){
				
				childRate = childRate + (Double.parseDouble(p_Response.getGross_Child().get(i)) * Double.parseDouble(p_Response.getQty_Child().get(i)));
							
			}
			
			double adPlusChild = adultRate + childRate;
			
			TestCaseCount_PResponse++;
			
			PrintWriter.append("<tr><td>"+TestCaseCount_PResponse+"</td> <td>Activity Rates match in xml</td>");
			PrintWriter.append("<td>Total Amount - "+totalAmount+"</td>");
						
			if(totalAmount==adPlusChild){
					
				PrintWriter.append("<td>Adult Amount - "+adultRate+" Child Amount - "+childRate+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>Adult Amount - "+adultRate+" Child Amount - "+childRate+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
					
			/////
			
			
			PrintWriter.append("</table>");
			PrintWriter.append("</body></html>");
			
			BufferedWriter bwr = new BufferedWriter(new FileWriter(new File(PG_Properties.getProperty("GTA-XmlReport.ProgramTypes"))));
			bwr.write(PrintWriter.toString());
			bwr.flush();
			bwr.close();
			
			
		} catch (Exception e) {
			// TODO: handle exception
		}
				
	}
	
	
	
	
	public WEB_CancellationPolicyRequest readCancellationPolicyRequests(){
		
		String windowHandle = null;
		
		try{				
			windowHandle = this.getWindow("gtaactivity_CancelltionPolicyRequest_" ,driver, activitydetails.getTraceId());
		}
		
		catch (Exception e) {
			e.getMessage();
		}
		
		String PageSource = this.driver.getPageSource();
		doc = Jsoup.parse(PageSource);
		cancelPolicy_Req.setURL(driver.getCurrentUrl());
		
		try {
			
			for (Element e : doc.select("Request")) {
				
				String currency = e.getElementsByTag("RequestorPreferences").first().attr("Currency"); 
				cancelPolicy_Req.setCurrency(currency);
				
				String city = e.getElementsByTag("City").text();
				cancelPolicy_Req.setCity(city);
				
				String tourDate = e.getElementsByTag("TourDate").text();
				cancelPolicy_Req.setTourDate(tourDate);
				
				String numberOfAdults = e.getElementsByTag("NumberOfAdults").text();
				cancelPolicy_Req.setAdultCount(numberOfAdults);
	        	
	        	Elements ageEle = null;		
	        	
				try {				
					ageEle = doc.getElementsByTag("Age");				
				
				} catch (Exception e1) {
					e1.printStackTrace();
				}	
				
				childCount_can = ageEle.size();
	        	
	        	
				for (Element e3 : e.select("Children")) {
					for (Element e4 : e3.select("Age")) {
						
						String age = e4.text(); 
						ageList_CanReq.add(age);
						child_NoH_can ++;
						
					}					
				}
				
				
				
					
			}
								
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		ArrayList<String> list = new ArrayList<String>(driver.getWindowHandles());
		driver.close();
		list.remove(list.get(0));
		driver.switchTo().window(windowHandle);
			
		
		return cancelPolicy_Req;
	}
	
	
	public WEB_CancellationPolicyResponse readCancellationPolicyResponse(){
		
		String windowHandle = null;
		
		try{				
			windowHandle = this.getWindow("gtaactivity_CancellationPolicyResponse_" ,driver, activitydetails.getTraceId());
		}
		
		catch (Exception e) {
			e.getMessage();
		}
		
		String PageSource = this.driver.getPageSource();
		doc = Jsoup.parse(PageSource);
		cancelPolicy_Response.setURL(driver.getCurrentUrl());
		
		try {
			
			for (Element e : doc.select("Response")) {
							
				Elements chargeEle = null;		
    			
				chargeEle = e.getElementsByTag("ChargeCondition");	
				
				String conditionType = chargeEle.get(0).attr("Type");
				cancelPolicy_Response.setConditionType(conditionType);
							
				Elements conditionEle = null;		
    			
				conditionEle = e.getElementsByTag("Condition");	
				
				String chargeAmount = conditionEle.get(0).attr("ChargeAmount");
				cancelPolicy_Response.setChargeAmount(chargeAmount);
				
				String currency = conditionEle.get(0).attr("Currency");
				cancelPolicy_Response.setCurrency(currency);
				
				String dateFrom = conditionEle.get(0).attr("FromDate");
				cancelPolicy_Response.setDateFrom(dateFrom);
				
				String dateTo = conditionEle.get(0).attr("ToDate");
				cancelPolicy_Response.setDateTo(dateTo);
				
			}
			
						
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		ArrayList<String> list = new ArrayList<String>(driver.getWindowHandles());
		driver.close();
		list.remove(list.get(0));
		driver.switchTo().window(windowHandle);
	
		
		return cancelPolicy_Response;
	}
	
	
	public void getGTACancellationPolicyReport(WEB_ThirdSearch search, WebDriver driver){
		
		PrintWriter = new StringBuffer();
		ScenarioCount ++;
		
		try {
			
			PrintWriter.append("<html class='report'><head><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"> </head>");
			PrintWriter.append("<div class='header'><img height=\"19px\" src=\"menu_rez.png\"><p class='Hedding1'>XML Report >> Activity Cancellation Policy Request Type >> GTA</p></div>");
			PrintWriter.append("<body>");
			
			PrintWriter.append("<br><br>");
			PrintWriter.append("<div style=\"border:1px solid;border-radius:3px 3px 3px 3px;background-color:#8D8D86; width:50%;\">");
			PrintWriter.append("<p class='InfoSup'style='font-weight: bold;'>"+ScenarioCount+")<br></p>");
			PrintWriter.append("<p class='InfoSub'>Current Activity Reservation Criteria</p>");
			PrintWriter.append("<p class='InfoSub'>Country name :- "+search.getCountry()+"</p>");
			PrintWriter.append("<p class='InfoSub'>Date From :- "+search.getDateFrom()+"</p>");
			PrintWriter.append("<p class='InfoSub'>Date To :- "+search.getDateTo()+"</p>");
			PrintWriter.append("</div>");
			PrintWriter.append("<br>");
			
			PrintWriter.append("<br><br><table><tr> <th>XML Type</th> <th>Supplier - GTA</th> </tr>");
			PrintWriter.append("<tr> <td>CancellationPolicy_Request</td> <td> <a href="+cancelPolicy_Req.getURL()+">request</a> </td> <tr>");
			PrintWriter.append("<tr> <td>CancellationPolicy_Response</td> <td> <a href="+cancelPolicy_Response.getURL()+">response</a> </td> <tr>");
			PrintWriter.append("</table>");
			
			
			TestCaseCount_canReq = 1;
			PrintWriter.append("<br><br><table ><tr><th>Test Case No</th><th>Test Description</th><th>Expect - [Excel Value]</th><th>Actual - [Xml Result]</th><th>Test Status</th></tr>");
			
			PrintWriter.append("<tr><td>"+TestCaseCount_canReq+"</td> <td>Activity city mapping</td>");
			PrintWriter.append("<td>"+search.getDestination()+"</td>");
						
			if(search.getDestination().toLowerCase().contains(cancelPolicy_Req.getCity().toLowerCase())){
					
				PrintWriter.append("<td>"+cancelPolicy_Req.getCity()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+cancelPolicy_Req.getCity()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			/////
			
			TestCaseCount_canReq ++;
			PrintWriter.append("<tr><td>"+TestCaseCount_canReq+"</td> <td>Activity Currency</td>");
			PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
						
			if(search.getSellingCurrency().toLowerCase().equals(activitydetails.getActivityCurrency().toLowerCase())){
					
				PrintWriter.append("<td>"+activitydetails.getActivityCurrency()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+activitydetails.getActivityCurrency()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			//////
			
			TestCaseCount_canReq ++;
			
			DateFormat dfPResponse = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat formatInPResponse = new SimpleDateFormat("dd-MMM-yyyy");
			Date instancePResponse = formatInPResponse.parse(search.getActivityDate());  		
			String reportDatePResponse = dfPResponse.format(instancePResponse);
			
			PrintWriter.append("<tr><td>"+TestCaseCount_canReq+"</td> <td>Activity date mapping</td>");
			PrintWriter.append("<td>"+reportDatePResponse+"</td>");
						
			if(cancelPolicy_Req.getTourDate().equals(reportDatePResponse)){
					
				PrintWriter.append("<td>"+cancelPolicy_Req.getTourDate()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+cancelPolicy_Req.getTourDate()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
			PrintWriter.append("</table>");
			
		//////
			
			TestCaseCount_canRes = 1;	
					
			excelCurrencyCanPolicy = search.getSellingCurrency();
			
			PrintWriter.append("<br><br>");
			PrintWriter.append("<div class='header'><p class='Hedding1'>XML Report >> Activity Cancellation Policy Response Type >> GTA</p></div>");
			PrintWriter.append("<br><br><table ><tr><th>Test Case No</th><th>Test Description</th><th>Expect - [Excel Value]</th><th>Actual - [Xml Result]</th><th>Test Status</th></tr>");
			

			PrintWriter.append("<tr><td>"+TestCaseCount_canRes+"</td> <td>Activity Currency Mapping</td>");
			PrintWriter.append("<td>"+excelCurrencyCanPolicy+"</td>");
						
			if(activitydetails.getActivityCurrency().toLowerCase().equals(excelCurrencyCanPolicy.toLowerCase())){
					
				PrintWriter.append("<td>"+activitydetails.getActivityCurrency()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+activitydetails.getActivityCurrency()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			//////
			
			String rateValue = activitydetails.getMainRate();
			
			
			TestCaseCount_canRes++;
			PrintWriter.append("<tr><td>"+TestCaseCount_canRes+"</td> <td>Activity Rate Check</td>");
			PrintWriter.append("<td>"+rateValue+"</td>");
			
			if(PG_Properties.getProperty("PortalCurrency").toLowerCase().equals(excelCurrencyCanPolicy.toLowerCase())){
				
				Double rateX = (Double.parseDouble(cancelPolicy_Response.getChargeAmount()) * (profitMarkup + 100)) / 100;
				rateXMLCanPolicy = (int) Math.ceil(rateX);
										
				if(rateValue.equals(Integer.toString(rateXMLCanPolicy))){
					
					PrintWriter.append("<td>"+Integer.toString(rateXMLCanPolicy)+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+Integer.toString(rateXMLCanPolicy)+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}				
			}
			
			else{
				
				for(Entry<String, String> entry: currencyMap.entrySet()) {
					if(excelCurrencyCanPolicy.toLowerCase().equals(entry.getKey().toLowerCase())){
						
						//convert into USD		
						Double ratex = ((Double.parseDouble(cancelPolicy_Response.getChargeAmount()) * (profitMarkup + 100)) / 100) * Double.parseDouble(entry.getValue()); 
						rateXMLCanPolicy = (int) Math.ceil(ratex);
						
						if(rateValue.equals(Integer.toString(rateXMLCanPolicy))){
							
							PrintWriter.append("<td>"+Integer.toString(rateXMLCanPolicy)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+Integer.toString(rateXMLCanPolicy)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}							
					}					
				}			
			}
					
			
			///////
			
			DateFormat dateFormatcurrentDate = new SimpleDateFormat("dd-MMM-yyyy");
			Calendar cal = Calendar.getInstance();
			String currentDateforMatch = dateFormatcurrentDate.format(cal.getTime());  //currentDate
			
			String xmlDateTo = cancelPolicy_Response.getDateTo();
			DateFormat dfTo = new SimpleDateFormat("dd-MMM-yyyy");
			SimpleDateFormat formatInTo = new SimpleDateFormat("yyyy-MM-dd");
			Date instanceTo = formatInTo.parse(xmlDateTo);  		
			String reportDateTo = dfTo.format(instanceTo);  //DateTo   25
			
			String xmlDateFr = cancelPolicy_Response.getDateFrom();
			Date instance = formatInTo.parse(xmlDateFr);  		
			String reportDate = dfTo.format(instance);  //Date from
			
			Date instanceMainPol = dfTo.parse(reportDateTo); 
			Calendar calMainPolicy = Calendar.getInstance();
			calMainPolicy.setTime(instanceMainPol);
			calMainPolicy.add(Calendar.DATE, -3);			
			String dateWithBufferDates = dfTo.format(calMainPolicy.getTime());  //3 Days     22
			
			Date instanceMain = dfTo.parse(dateWithBufferDates);
			Calendar calMainPolicyto = Calendar.getInstance();
			calMainPolicyto.setTime(instanceMain);
			calMainPolicyto.add(Calendar.DATE, -1);
			String dateWithBufferDatesTo = dfTo.format(calMainPolicyto.getTime());   //1 Days    21
			
			Date instanceMainPolicy2 = dfTo.parse(reportDateTo);
			calMainPolicyto.setTime(instanceMainPolicy2);
			calMainPolicyto.add(Calendar.DATE, -1);
			String dateWithBufferDatesToPolicy2 = dfTo.format(calMainPolicyto.getTime());     //    24
			
			Date instanceMainPolicy3 = dfTo.parse(reportDate);
			calMainPolicyto.setTime(instanceMainPolicy3);
			calMainPolicyto.add(Calendar.DATE, -1);
			String dateWithBufferDatesToPolicy3 = dfTo.format(calMainPolicyto.getTime());     //    09
					
			double rate = rateXMLCanPolicy;
			DecimalFormat DecimalFormat = new DecimalFormat("#.00");		
			
				
			//Cancel policy 1
			
			if (activitydetails.getCancelPolicy().size() == 2) {
				
				activityCanPolicy1 = activitydetails.getCancelPolicy().get(0);					
				String xmlPolist1 = "If you cancel between "+currentDateforMatch+" and "+dateWithBufferDatesToPolicy3+" you will be charged. "+excelCurrencyCanPolicy+" "+DecimalFormat.format(rate)+" of the activity/transfer and tax charge.";
								
				TestCaseCount_canRes++;
				PrintWriter.append("<tr><td>"+TestCaseCount_canRes+"</td> <td>Activity cancellation policy - 1</td>");
				PrintWriter.append("<td>"+xmlPolist1+"</td>");
				
				if(activityCanPolicy1.toLowerCase().equals(xmlPolist1.toLowerCase())){
					
					PrintWriter.append("<td>"+activityCanPolicy1+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+activityCanPolicy1+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}				
								
			}
			
			
			
			if (activitydetails.getCancelPolicy().size() == 3) {
				
				String policyListOne = activitydetails.getCancelPolicy().get(0);
				activityMainPolicy = "If you cancel between "+currentDateforMatch+" and "+dateWithBufferDatesTo+" you will be refunded your purchase price.";
				
				
				TestCaseCount_canRes++;
				PrintWriter.append("<tr><td>"+TestCaseCount_canRes+"</td> <td>Activity Refundable policy</td>");
				PrintWriter.append("<td>"+policyListOne+"</td>");
				
				if(policyListOne.toLowerCase().equals(activityMainPolicy.toLowerCase())){
					
					PrintWriter.append("<td>"+activityMainPolicy+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+activityMainPolicy+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
								
				
				activityCanPolicy2 = activitydetails.getCancelPolicy().get(1);					
				String xmlPolist2 = "If you cancel between "+dateWithBufferDates+" and "+dateWithBufferDatesToPolicy3+" you will be charged. "+excelCurrencyCanPolicy+" "+DecimalFormat.format(rate)+" of the activity/transfer and tax charge.";
								
				TestCaseCount_canRes++;
				PrintWriter.append("<tr><td>"+TestCaseCount_canRes+"</td> <td>Activity cancellation policy - 1</td>");
				PrintWriter.append("<td>"+xmlPolist2+"</td>");
				
				if(activityCanPolicy2.toLowerCase().equals(xmlPolist2.toLowerCase())){
					
					PrintWriter.append("<td>"+activityCanPolicy2+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+activityCanPolicy2+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}		
				
			}
			
				
			//No Show Applying policy
				
			if (activitydetails.getCancelPolicy().size() == 2) {
				noShowMatching = activitydetails.getCancelPolicy().get(1);
			}
			
			if (activitydetails.getCancelPolicy().size() == 3) {
				noShowMatching = activitydetails.getCancelPolicy().get(2);
			}
			
			if (activitydetails.getCancelPolicy().size() == 4) {
				noShowMatching 	= activitydetails.getCancelPolicy().get(3);
			}
			
			String noShowMatchingXML 	= "If cancelled on or after the "+reportDate+" No Show Fee  applies.";
			
			
			TestCaseCount_canRes++;
			PrintWriter.append("<tr><td>"+TestCaseCount_canRes+"</td> <td>Activity No-Show Fee</td>");
			PrintWriter.append("<td>"+noShowMatchingXML+"</td>");
			
			if(noShowMatching.toLowerCase().equals(noShowMatchingXML.toLowerCase())){
				
				PrintWriter.append("<td>"+noShowMatching+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+noShowMatching+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
			
			/////
			
			
			
			PrintWriter.append("</table>");
			PrintWriter.append("</body></html>");
			
			BufferedWriter bwr = new BufferedWriter(new FileWriter(new File(PG_Properties.getProperty("GTA-XmlReport.CancellationPolicyTypes"))));
			bwr.write(PrintWriter.toString());
			bwr.flush();
			bwr.close();
			
			
			
				
		} catch (Exception e) {
			// TODO: handle exception
		}
				
	}
	
	
	
	
	public WEB_ReservationRequest readReservationRequest(){
	
		String windowHandle = null;
		
		try{	

			windowHandle = this.getWindow("gtaactivity_ReservationRequest_" ,driver, activitydetails.getTraceId());
		}
		
		catch (Exception e) {
			e.getMessage();
		}
		
		String PageSource = this.driver.getPageSource();
		doc = Jsoup.parse(PageSource);
		reser_Req.setUrl(driver.getCurrentUrl());
		
		try {
			
			for (Element e : doc.select("Request")){
	
				String currency = e.getElementsByTag("AddBookingRequest").first().attr("Currency"); 
				reser_Req.setCurrency(currency);	
								
				String tourDate = e.getElementsByTag("BookingDepartureDate").text();
				reser_Req.setTourDate(tourDate);
				
				String itemCity = e.getElementsByTag("ItemCity").first().attr("Code");
				reser_Req.setTourCity(itemCity);
				
				Elements PaxNames = null;		
				
				PaxNames = doc.getElementsByTag("PaxName");	
				
				for (int i = 0; i < PaxNames.size(); i++) {
					
					if(PaxNames.get(i).attr("PaxType").equals("child")){
						
						String childAge = PaxNames.get(i).attr("ChildAge");
						childAge_resReq.add(childAge);
																
					}
				}	
				
				
				reser_Req.setChildAgesList(childAge_resReq);
				
			}
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		ArrayList<String> list = new ArrayList<String>(driver.getWindowHandles());
		driver.close();
		list.remove(list.get(0));
		driver.switchTo().window(windowHandle);
			

		return reser_Req;
	}
	
	
	public WEB_ReservationResponse readReservationResponse(){
	
		String windowHandle = null;
		
		try{	

			windowHandle = this.getWindow("gtaactivity_ReservationResponse_" ,driver, activitydetails.getTraceId());
		}
		
		catch (Exception e) {
			e.getMessage();
		}
		
		String PageSource = this.driver.getPageSource();
		doc = Jsoup.parse(PageSource);
		reser_Response.setUrl(driver.getCurrentUrl());
		
		
		try {
			
			for (Element e : doc.select("Response")){
								
				String currency = e.getElementsByTag("BookingPrice").first().attr("Currency"); 
				reser_Response.setCurrency(currency);
				
				String grossAmount = e.getElementsByTag("BookingPrice").first().attr("Gross"); 
				reser_Response.setGrossValue(grossAmount);
								
				String tourDate = e.getElementsByTag("BookingDepartureDate").text();
				reser_Response.setTourDate(tourDate);
				
				String bookingStatus = e.getElementsByTag("BookingStatus").text();
				reser_Response.setBookingStatus(bookingStatus);
				
				String itemCity = e.getElementsByTag("ItemCity").text();
				reser_Response.setCity(itemCity);
				
				String activityName = e.getElementsByTag("Item").text();
				reser_Response.setActivityName(activityName);
				
				String suppConfirmNo = e.getElementsByTag("ItemConfirmationReference").text();
				reser_Response.setSupplierConfirmationNo(suppConfirmNo);
				
				Elements PaxNames = null;		
    			
				PaxNames = doc.getElementsByTag("PaxName");	
				paxCountReservation = PaxNames.size();
							
				for (int i = 0; i < PaxNames.size(); i++) {
					
					if(PaxNames.get(i).attr("PaxType").equals("child")){
						
						String childAge = PaxNames.get(i).attr("ChildAge");
						childAge_resResponse.add(childAge);
												
					}				
				}	
				
				reser_Response.setChildAgesList(childAge_resResponse);
			}
			
					
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		ArrayList<String> list = new ArrayList<String>(driver.getWindowHandles());
		driver.close();
		list.remove(list.get(0));
		driver.switchTo().window(windowHandle);
		
		
		return reser_Response;
	}
	
	
	public void getGTAReservationReport(WEB_ThirdSearch search, WebDriver driver){
		
		PrintWriter = new StringBuffer();
		ScenarioCount ++;
		
		try {
			
			PrintWriter.append("<html class='report'><head><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"> </head>");
			PrintWriter.append("<div class='header'><img height=\"19px\" src=\"menu_rez.png\"><p class='Hedding1'>XML Report >> Activity Reservation Request Type >> GTA</p></div>");
			PrintWriter.append("<body>");
			
			PrintWriter.append("<br><br>");
			PrintWriter.append("<div style=\"border:1px solid;border-radius:3px 3px 3px 3px;background-color:#8D8D86; width:50%;\">");
			PrintWriter.append("<p class='InfoSup'style='font-weight: bold;'>"+ScenarioCount+")<br></p>");
			PrintWriter.append("<p class='InfoSub'>Current Activity Reservation Criteria</p>");
			PrintWriter.append("<p class='InfoSub'>Country name :- "+search.getCountry()+"</p>");
			PrintWriter.append("<p class='InfoSub'>Date From :- "+search.getDateFrom()+"</p>");
			PrintWriter.append("<p class='InfoSub'>Date To :- "+search.getDateTo()+"</p>");
			PrintWriter.append("</div>");
			PrintWriter.append("<br>");
			
			PrintWriter.append("<br><br><table><tr> <th>XML Type</th> <th>Supplier - GTA</th> </tr>");
			PrintWriter.append("<tr> <td>Reservation_Request</td> <td> <a href="+reser_Req.getUrl()+">request</a> </td> <tr>");
			PrintWriter.append("<tr> <td>Reservation_Response</td> <td> <a href="+reser_Response.getUrl()+">response</a> </td> <tr>");
			PrintWriter.append("</table>");
			
			TestCaseCount_ResReq = 1;
			PrintWriter.append("<br><br><table ><tr><th>Test Case No</th><th>Test Description</th><th>Expect - [Excel Value]</th><th>Actual - [Xml Result]</th><th>Test Status</th></tr>");
			
			PrintWriter.append("<tr><td>"+TestCaseCount_ResReq+"</td> <td>Activity Currency Match</td>");
			PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
						
			if(search.getSellingCurrency().toLowerCase().equals(activitydetails.getActivityCurrency().toLowerCase())){
					
				PrintWriter.append("<td>"+activitydetails.getActivityCurrency()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+activitydetails.getActivityCurrency()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			/////
			
			TestCaseCount_ResReq ++;
			
			DateFormat dfPResponse = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat formatInPResponse = new SimpleDateFormat("dd-MMM-yyyy");
			Date instancePResponse = formatInPResponse.parse(search.getActivityDate());  		
			String reportDatePResponse = dfPResponse.format(instancePResponse);
			
			PrintWriter.append("<tr><td>"+TestCaseCount_ResReq+"</td> <td>Activity Date</td>");
			PrintWriter.append("<td>"+reportDatePResponse+"</td>");
						
			if(reser_Req.getTourDate().equals(reportDatePResponse)){
					
				PrintWriter.append("<td>"+reser_Req.getTourDate()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+reser_Req.getTourDate()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			/////
			
			TestCaseCount_ResReq ++;			
			PrintWriter.append("<tr><td>"+TestCaseCount_ResReq+"</td> <td>Activity city mapping</td>");
			PrintWriter.append("<td>"+search.getDestination()+"</td>");
						
			if(search.getDestination().toLowerCase().contains(reser_Req.getTourCity().toLowerCase())){
					
				PrintWriter.append("<td>"+reser_Req.getTourCity()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+reser_Req.getTourCity()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
			/////
			
			String childAgesPrespo = search.getAgeOfChildren();
			String[] partsEAPrespo = childAgesPrespo.split("#");
			
			List<String> valueListReserReq = new ArrayList<String>();
			
			for (String value : partsEAPrespo) {
				valueListReserReq.add(value);
			}
			
			for (int i = 0; i < valueListReserReq.size(); i++) {
				
				TestCaseCount_ResReq ++;
				PrintWriter.append("<tr><td>"+TestCaseCount_ResReq+"</td> <td>Activity Child ages check - [Child "+(i+1)+"]</td>");
				PrintWriter.append("<td>"+valueListReserReq.get(i)+"</td>");
				
				if(reser_Req.getChildAgesList().get(i).equals(valueListReserReq.get(i))){
					
					PrintWriter.append("<td>"+reser_Req.getChildAgesList().get(i)+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
				}
				else{
					PrintWriter.append("<td>"+reser_Req.getChildAgesList().get(i)+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
			}
			
			
			/////
			
			PrintWriter.append("</table>");
			
		//////////
			
			PrintWriter.append("<br><br>");
			PrintWriter.append("<div class='header'><p class='Hedding1'>XML Report >> Activity Reservation Response Type >> GTA</p></div>");
			PrintWriter.append("<br><br><table ><tr><th>Test Case No</th><th>Test Description</th><th>Expect - [Excel Value]</th><th>Actual - [Xml Result]</th><th>Test Status</th></tr>");
			
			TestCaseCount_ResResponse = 1;
			
			PrintWriter.append("<tr><td>"+TestCaseCount_ResResponse+"</td> <td>Activity Name </td>");
			PrintWriter.append("<td>"+search.getActivityName()+"</td>");
						
			if(reser_Response.getActivityName().contains(search.getActivityName())){
					
				PrintWriter.append("<td>"+reser_Response.getActivityName()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+reser_Response.getActivityName()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
					
			////
			
			TestCaseCount_ResResponse ++;
			PrintWriter.append("<tr><td>"+TestCaseCount_ResResponse+"</td> <td>Activity Booking Status </td>");
			PrintWriter.append("<td>"+activitydetails.getConfirmation_BI_BookingStatus()+"</td>");
						
			if(activitydetails.getConfirmation_BI_BookingStatus().toLowerCase().equals(reser_Response.getBookingStatus().toLowerCase())){
					
				PrintWriter.append("<td>"+reser_Response.getBookingStatus()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+reser_Response.getBookingStatus()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			////
			
			
			
			////
			
			String rateValue = activitydetails.getMainRate();
			excelCurrencyReservation = search.getSellingCurrency();
						
			TestCaseCount_ResResponse++;
			PrintWriter.append("<tr><td>"+TestCaseCount_ResResponse+"</td> <td>Activity Rate Check </td>");
			PrintWriter.append("<td>"+rateValue+"</td>");
			
			if(PG_Properties.getProperty("PortalCurrency").toLowerCase().equals(excelCurrencyReservation.toLowerCase())){
				
				Double rateX = (Double.parseDouble(reser_Response.getGrossValue()) * (profitMarkup + 100)) / 100;
				rateXMLReservation = (int) Math.ceil(rateX);
										
				if(rateValue.equals(Integer.toString(rateXMLReservation))){
					
					PrintWriter.append("<td>"+Integer.toString(rateXMLReservation)+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+Integer.toString(rateXMLReservation)+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}				
			}
			
			else{
				
				for(Entry<String, String> entry: currencyMap.entrySet()) {
					if(excelCurrencyReservation.toLowerCase().equals(entry.getKey().toLowerCase())){
						
						//convert into USD		
						Double ratex = ((Double.parseDouble(reser_Response.getGrossValue()) * (profitMarkup + 100)) / 100) * Double.parseDouble(entry.getValue()); 
						rateXMLReservation = (int) Math.ceil(ratex);
						
						if(rateValue.equals(Integer.toString(rateXMLReservation))){
							
							PrintWriter.append("<td>"+Integer.toString(rateXMLReservation)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+Integer.toString(rateXMLReservation)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}							
					}					
				}			
			}
			
			////
			
			int totalPax = Integer.parseInt(search.getAdults()) + Integer.parseInt(search.getChildren());
			
			TestCaseCount_ResResponse ++;
			PrintWriter.append("<tr><td>"+TestCaseCount_ResResponse+"</td> <td>Activity Pax count </td>");
			PrintWriter.append("<td>"+totalPax+"</td>");
						
			if(totalPax == paxCountReservation){
					
				PrintWriter.append("<td>"+paxCountReservation+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+paxCountReservation+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			////
				
			for (int j = 0; j < valueListReserReq.size(); j++) {
				
				TestCaseCount_ResResponse ++;
				PrintWriter.append("<tr><td>"+TestCaseCount_ResResponse+"</td> <td>Activity Child ages check - [Child "+(j+1)+"]</td>");
				PrintWriter.append("<td>"+valueListReserReq.get(j)+"</td>");
				
				if(reser_Response.getChildAgesList().get(j).equals(valueListReserReq.get(j))){
					
					PrintWriter.append("<td>"+reser_Response.getChildAgesList().get(j)+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
				}
				else{
					PrintWriter.append("<td>"+reser_Response.getChildAgesList().get(j)+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
			}
				
			////
			
			TestCaseCount_ResResponse ++;
			PrintWriter.append("<tr><td>"+TestCaseCount_ResResponse+"</td> <td>Activity Date </td>");
			PrintWriter.append("<td>"+reportDatePResponse+"</td>");
						
			if(reser_Response.getTourDate().equals(reportDatePResponse)){
					
				PrintWriter.append("<td>"+reser_Response.getTourDate()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+reser_Response.getTourDate()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			///
						
			TestCaseCount_ResResponse ++;
			PrintWriter.append("<tr><td>"+TestCaseCount_ResResponse+"</td> <td>Activity Currency Match</td>");
			PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
						
			if(search.getSellingCurrency().toLowerCase().equals(activitydetails.getActivityCurrency().toLowerCase())){
					
				PrintWriter.append("<td>"+activitydetails.getActivityCurrency()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+activitydetails.getActivityCurrency()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			////
			
			TestCaseCount_ResResponse ++;			
			PrintWriter.append("<tr><td>"+TestCaseCount_ResResponse+"</td> <td>Activity city mapping</td>");
			PrintWriter.append("<td>"+search.getDestination()+"</td>");
						
			if(search.getDestination().toLowerCase().contains(reser_Response.getCity().toLowerCase())){
					
				PrintWriter.append("<td>"+reser_Req.getTourCity()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+reser_Req.getTourCity()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			/////
			
			TestCaseCount_ResResponse ++;			
			PrintWriter.append("<tr><td>"+TestCaseCount_ResResponse+"</td> <td>Activity Gross booking Value - Payment Page</td>");
			PrintWriter.append("<td>"+activitydetails.getPaymentBilling_subTotal()+"</td>");
						
			if(rateXMLReservation == (Integer.parseInt(activitydetails.getPaymentBilling_subTotal()))){
					
				PrintWriter.append("<td>"+rateXMLReservation+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+rateXMLReservation+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			/////
			
			if (activitydetails.getPaymentDetails().equals("Pay Online")) {
				
				double PaymentPageGrossValue = Double.parseDouble(activitydetails.getPaymentBilling_subTotal());
				double PaymentPageTax = Double.parseDouble(activitydetails.getPaymentBilling_TotalTax());
				double PaymentPageTotal = Double.parseDouble(activitydetails.getPaymentBilling_TotalValue());
				
				int taxRental = (int)Math.ceil(PaymentPageGrossValue * 0.1);
				
				TestCaseCount_ResResponse ++;			
				PrintWriter.append("<tr><td>"+TestCaseCount_ResResponse+"</td> <td>Activity Tax Value - Payment page</td>");
				PrintWriter.append("<td>"+PaymentPageTax+"</td>");
							
				if(PaymentPageTax == taxRental){
						
					PrintWriter.append("<td>"+taxRental+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+taxRental+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				TestCaseCount_ResResponse ++;			
				PrintWriter.append("<tr><td>"+TestCaseCount_ResResponse+"</td> <td>Activity Total Value - Payment page</td>");
				PrintWriter.append("<td>"+PaymentPageTotal+"</td>");
							
				if(PaymentPageTotal == (PaymentPageGrossValue+taxRental)){
						
					PrintWriter.append("<td>"+(PaymentPageGrossValue+taxRental)+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+(PaymentPageGrossValue+taxRental)+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				TestCaseCount_ResResponse ++;			
				PrintWriter.append("<tr><td>"+TestCaseCount_ResResponse+"</td> <td>Activity Total Payment value - Payment Gateway</td>");
				PrintWriter.append("<td>"+activitydetails.getPaymentGatewayTotal()+"</td>");
							
				if(activitydetails.getPaymentGatewayTotal().equals(activitydetails.getPaymentBilling_TotalValue())){
						
					PrintWriter.append("<td>"+activitydetails.getPaymentGatewayTotal()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+activitydetails.getPaymentGatewayTotal()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
							
			}
			
			/////
			
			
			
			
			
			TestCaseCount_ResResponse ++;			
			PrintWriter.append("<tr><td>"+TestCaseCount_ResResponse+"</td> <td>Customer details - First Name</td>");
			PrintWriter.append("<td>"+activitydetails.getPaymentPage_FName()+"</td>");
						
			if(activitydetails.getPaymentPage_FName().equals(activitydetails.getConfirmation_FName())){
					
				PrintWriter.append("<td>"+activitydetails.getConfirmation_FName()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+activitydetails.getConfirmation_FName()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			TestCaseCount_ResResponse ++;			
			PrintWriter.append("<tr><td>"+TestCaseCount_ResResponse+"</td> <td>Customer details - Last Name</td>");
			PrintWriter.append("<td>"+activitydetails.getPaymentPage_LName()+"</td>");
						
			if(activitydetails.getPaymentPage_LName().equals(activitydetails.getConfirmation_LName())){
					
				PrintWriter.append("<td>"+activitydetails.getConfirmation_LName()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+activitydetails.getConfirmation_LName()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			TestCaseCount_ResResponse ++;			
			PrintWriter.append("<tr><td>"+TestCaseCount_ResResponse+"</td> <td>Customer details - TP No</td>");
			PrintWriter.append("<td>"+activitydetails.getPaymentPage_TP()+"</td>");
						
			if(activitydetails.getPaymentPage_TP().equals(activitydetails.getConfirmation_TP())){
					
				PrintWriter.append("<td>"+activitydetails.getConfirmation_TP()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+activitydetails.getConfirmation_TP()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			TestCaseCount_ResResponse ++;			
			PrintWriter.append("<tr><td>"+TestCaseCount_ResResponse+"</td> <td>Customer details - Email</td>");
			PrintWriter.append("<td>"+activitydetails.getPaymentPage_Email()+"</td>");
						
			if(activitydetails.getPaymentPage_Email().equals(activitydetails.getConfirmation_Email())){
					
				PrintWriter.append("<td>"+activitydetails.getConfirmation_Email()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+activitydetails.getConfirmation_Email()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			TestCaseCount_ResResponse ++;			
			PrintWriter.append("<tr><td>"+TestCaseCount_ResResponse+"</td> <td>Customer details - Address</td>");
			PrintWriter.append("<td>"+activitydetails.getPaymentPage_address()+"</td>");
						
			if(activitydetails.getPaymentPage_address().equals(activitydetails.getConfirmation_address())){
					
				PrintWriter.append("<td>"+activitydetails.getConfirmation_address()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+activitydetails.getConfirmation_address()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			TestCaseCount_ResResponse ++;			
			PrintWriter.append("<tr><td>"+TestCaseCount_ResResponse+"</td> <td>Customer details - Country</td>");
			PrintWriter.append("<td>"+activitydetails.getPaymentPage_country()+"</td>");
						
			if(activitydetails.getPaymentPage_country().equals(activitydetails.getConfirmation_country())){
					
				PrintWriter.append("<td>"+activitydetails.getConfirmation_country()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+activitydetails.getConfirmation_country()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			TestCaseCount_ResResponse ++;			
			PrintWriter.append("<tr><td>"+TestCaseCount_ResResponse+"</td> <td>Customer details - City</td>");
			PrintWriter.append("<td>"+activitydetails.getPaymentPage_city()+"</td>");
						
			if(activitydetails.getPaymentPage_city().equals(activitydetails.getConfirmation_city())){
					
				PrintWriter.append("<td>"+activitydetails.getConfirmation_city()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+activitydetails.getConfirmation_city()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			/////
			
			for (int j = 0; j < activitydetails.getConfirmationPage_cusTitle().size() ; j++) {
				
				TestCaseCount_ResResponse ++;
				PrintWriter.append("<tr><td>"+TestCaseCount_ResResponse+"</td> <td>Customer Title check - [Customer "+(j+1)+"]</td>");
				PrintWriter.append("<td>"+activitydetails.getConfirmationPage_cusTitle().get(j)+"</td>");
				
				if(activitydetails.getConfirmationPage_cusTitle().get(j).equals(activitydetails.getResultsPage_cusTitle().get(j))){
					
					PrintWriter.append("<td>"+activitydetails.getConfirmationPage_cusTitle().get(j)+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
				}
				else{
					PrintWriter.append("<td>"+activitydetails.getConfirmationPage_cusTitle().get(j)+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
			}
			
			for (int j = 0; j < activitydetails.getConfirmationPage_cusFName().size() ; j++) {
				
				TestCaseCount_ResResponse ++;
				PrintWriter.append("<tr><td>"+TestCaseCount_ResResponse+"</td> <td>Customer First Name check - [Customer "+(j+1)+"]</td>");
				PrintWriter.append("<td>"+activitydetails.getConfirmationPage_cusFName().get(j)+"</td>");
				
				if(activitydetails.getConfirmationPage_cusFName().get(j).equals(activitydetails.getResultsPage_cusFName().get(j))){
					
					PrintWriter.append("<td>"+activitydetails.getConfirmationPage_cusFName().get(j)+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
				}
				else{
					PrintWriter.append("<td>"+activitydetails.getConfirmationPage_cusFName().get(j)+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
			}
			
			for (int j = 0; j < activitydetails.getConfirmationPage_cusLName().size() ; j++) {
				
				TestCaseCount_ResResponse ++;
				PrintWriter.append("<tr><td>"+TestCaseCount_ResResponse+"</td> <td>Customer Last Name check - [Customer "+(j+1)+"]</td>");
				PrintWriter.append("<td>"+activitydetails.getConfirmationPage_cusLName().get(j)+"</td>");
				
				if(activitydetails.getConfirmationPage_cusLName().get(j).equals(activitydetails.getResultsPage_cusLName().get(j))){
					
					PrintWriter.append("<td>"+activitydetails.getConfirmationPage_cusLName().get(j)+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
				}
				else{
					PrintWriter.append("<td>"+activitydetails.getConfirmationPage_cusLName().get(j)+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
			}
			
			/////
			
			PrintWriter.append("</table>");
			
			/////
			
			long timeDiff = activitydetails.getTimeConfirmBooking() - activitydetails.getTimeLoadpayment();
			
			PrintWriter.append("<br><br><table><tr> <th>Description</th> <th>Time</th> </tr>");
			PrintWriter.append("<tr> <td>Add to cart button click time - </td> <td>"+activitydetails.getTimeLoadpayment()+"</td> <tr>");
			PrintWriter.append("<tr> <td>Reservation-No available time - </td> <td>"+activitydetails.getTimeConfirmBooking()+"</td> <tr>");
			PrintWriter.append("<tr> <td>Time Diff - </td> <td>"+timeDiff+" s</td> <tr>");
			PrintWriter.append("</table>");
			
			/////
						
			PrintWriter.append("</body></html>");
			
			BufferedWriter bwr = new BufferedWriter(new FileWriter(new File(PG_Properties.getProperty("HB-XmlReport.ReservationTypes"))));
			bwr.write(PrintWriter.toString());
			bwr.flush();
			bwr.close();
			
			
		} catch (Exception e) {
			// TODO: handle exception
		}
			
	}
	
	
	
}
