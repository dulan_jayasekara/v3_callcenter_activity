package webcom.reader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.jmx.Agent;
import org.openqa.selenium.WebDriver;

import com.controller.PG_Properties;

import webbookingconfirmation.WEB_BookingConfirmation;
import webbookingconfirmation.WEB_Quotation;
import webbookinglistreport.WEB_BookingList;
import webcancellation_modification.WEB_Cancellation;
import webcancellation_modification.WEB_Modification;
import webcom.model.WEB_ActivityDetails;
import webcom.model.WEB_ActivityInfo;
import webcom.model.WEB_PaymentDetails;
import webcom.model.WEB_Search;
import webcom.types.WEB_TourOperatorType;
import webreservationreport.WEB_Reservation;
import webreservationreport.WEB_ReservationInfoDetails;

public class WEB_ReportsReader {
	
	private WebDriver driver         	= null;
	WEB_ActivityDetails activitydetails 	= new WEB_ActivityDetails();
	WEB_BookingList bookingList = new WEB_BookingList();
	WEB_BookingConfirmation confirmationDetails = new WEB_BookingConfirmation();
	WEB_Reservation reservationdetails = new WEB_Reservation();
	Map<String, String> currencyMap 	= new HashMap<String, String>();
	WEB_Cancellation cancellationDetails = new WEB_Cancellation();
	WEB_Modification modDetails = new WEB_Modification();
	WEB_Search search = new WEB_Search();
	private StringBuffer  PrintWriter;
	private StringBuffer mailPrinter;
	private int ScenarioCount = 0;  
	private int testCaseCount, testCaseCountBookingList, testCaseCustomerMail, testCaseModify, testQuote, payent_qty, creditTax, discountedVale;
	private int testCaseVoucherMail, testcaseSupplier, testCaseCountReservation, testCaseInventory, testCaseCancel;
	private String noShowMatching, activityMainPolicy, activityCanPolicy1, activityCanPolicy2 ;
	private String paymentNoShowMatching, paymentMainPolicy, paymentCanPolicy1, paymentCanPolicy2 ;
	private String confirmNoShowMatching, confirmMainPolicy, confirmCanPolicy1, confirmCanPolicy2 ;
	private String resPolist1 = null;
	private String policyListOne = null ;
	private String policyListTwo  = null;
	private String noShowActual = null;
	private double rateConvertforUSD = 1.0 ;
	private double rateConvertforSearch = 1.0 ;
	private int ResultsPage_SubTotal, ResultsPage_Taxes, ResultsPage_TotalPayable, netRate, toAgentCommission;
	private int PaymentPage_SubTotal, PaymentPage_Taxes, PaymentPage_TotalPayable, PaymentPage_TotalGrossBookingValue, PaymentPage_TotalTax, PaymentPage_TotalPackageValue, PaymentPage_AmountProcessed, PaymentPage_AmountCheckIn, PaymentPage_CreditCardFee;
	private int ConfirmationPage_SubTotal, ConfirmationPage_Taxes, ConfirmationPage_TotalPayable, Confirmation_TotalGrossBookingValue, ConfirmationPage_TotalTax, ConfirmationPage_TotalPackageValue, ConfirmationPage_AmountProcessed, ConfirmationPage_AmountCheckIn;
	private double totalPayableforNoshow;
	private int totalCostInUsd, amount_onePerson, totalCost;
	private String bookingTotalvalueForAct;
	private int customerQty, pMarkup;
	private String supplierCurrency, discountYesNo;
	private String standardCancell_Type;
	private String standardCancell_Value;
	private String noShow_Type, totalPaymentForUSD, noShow_Value, applyIfLessThan, cancelBuffer, subTotalPaymentForRate;
	private String supplierName;
	private String supplierAddress;
	private String supplierTP, CCFeeForMails;
	private String inventoryType;
	private String activityDescription, currentDateforMatch;
	private String inventoryDetails, paymentType;
	private String ToActivityPM, ToActivityNetRate, ToActivity_SalesTaxType, ToActivity_SalesTaxValue, ToActivity_MisFeeType, ToActivity_MisFeeValue;
	private WEB_TourOperatorType  UserTypeDC_B2B;
	private WEB_Quotation quotationDetails;
	
	public WEB_ReportsReader(WEB_ActivityDetails activityDetails, WEB_Quotation quotationdetails, Map<String, String> CurrencyMap, WEB_Search search , WEB_BookingList bookinglist, WEB_BookingConfirmation ConfirmationDetails, WEB_Reservation reservationdetails,WEB_Cancellation cancellationDetails, WEB_Modification modDetails ,WebDriver Driver) {
		
		this.driver = Driver;
		this.activitydetails = activityDetails;
		this.currencyMap = CurrencyMap;
		this.bookingList = bookinglist;
		this.confirmationDetails = ConfirmationDetails;
		this.reservationdetails = reservationdetails;
		this.search = search;
		this.cancellationDetails=cancellationDetails;
		this.modDetails=modDetails;
		this.quotationDetails=quotationdetails;
	}

	

	public void getActvivtyInventoryReport(WebDriver Driver) throws ParseException {
		
		PrintWriter = new StringBuffer();
		ScenarioCount ++;
		
		DateFormat dateFormatcurrentDate = new SimpleDateFormat("dd-MMM-yyyy");
		Calendar cal = Calendar.getInstance();
		currentDateforMatch = dateFormatcurrentDate.format(cal.getTime()); 
		
		
		ArrayList<WEB_ActivityInfo> activityList = search.getInventoryInfo();
				
		for (WEB_ActivityInfo activityInfo : activityList) {
						
			supplierCurrency = activityInfo.getSupplier_Currency();
			standardCancell_Type = activityInfo.getStandardCancell_Type();
			standardCancell_Value = activityInfo.getStandardCancell_Value();
			noShow_Type = activityInfo.getNoShow_Type();
			noShow_Value = activityInfo.getNoShow_Value();
			applyIfLessThan = activityInfo.getApplyIfLessThan();
			cancelBuffer = activityInfo.getCancelBuffer();
			supplierName = activityInfo.getSupplierName();
			supplierAddress = activityInfo.getSupplierAddress();
			supplierTP = activityInfo.getSupplierTP();
			inventoryDetails = activityInfo.getInventoryType();
			activityDescription = activityInfo.getActivityDescription();
			
			totalPaymentForUSD = activityInfo.getPaymentPage_Total();
			subTotalPaymentForRate = activityInfo.getPaymentPage_SubTotal();
			
			ToActivityPM = activityInfo.getToActivityPM();  
			ToActivityNetRate = activityInfo.getToActivityNetRate(); 
			ToActivity_SalesTaxType = activityInfo.getToActivity_SalesTaxType(); 
			ToActivity_SalesTaxValue = activityInfo.getToActivity_SalesTaxValue(); 
			ToActivity_MisFeeType = activityInfo.getToActivity_MisFeeType(); 
			ToActivity_MisFeeValue = activityInfo.getToActivity_MisFeeValue(); 
			UserTypeDC_B2B = activityInfo.getUserTypeDC_B2B(); 
			CCFeeForMails = activityInfo.getCreditCardFee();
			discountYesNo = activityInfo.getDiscountYesORNo();
			
			paymentType = activityInfo.getPaymentType();
			
			
			if (!(UserTypeDC_B2B == WEB_TourOperatorType.WEB_DC)) {
				pMarkup = Integer.parseInt(activityInfo.getToActivityPM());
			}else{
				pMarkup = Integer.parseInt(activityInfo.getProfir_Markup());
			}
			
		}
		
		double paxCount = Integer.parseInt(search.getAdults()) + Integer.parseInt(search.getChildren());
		
		double profitMarkUP = ((100 + Double.parseDouble(PG_Properties.getProperty("ProfitMarkup"))) / 100);
		
			PrintWriter.append("<html class='report'><head><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"> </head>");
			if (search.getQuotationReq().equalsIgnoreCase("Yes")) {
				PrintWriter.append("<div class='header'><img height=\"19px\" src=\"menu_rez.png\"><p class='Hedding1'>Activity Reservation Report - Quotation No :- ["+activitydetails.getQuote_QuotationNo()+"] - Date ["+currentDateforMatch+"]</p></div>");
			} else {
				PrintWriter.append("<div class='header'><img height=\"19px\" src=\"menu_rez.png\"><p class='Hedding1'>Activity Reservation Report - Reservation No :- ["+activitydetails.getReservationNo()+"] - Date ["+currentDateforMatch+"]</p></div>");				
			}
			
			PrintWriter.append("<body>");		
			PrintWriter.append("<br><br>");
			PrintWriter.append("<div style=\"border:1px solid;border-radius:3px 3px 3px 3px;background-color:#8D8D86; width:50%;\">");
			PrintWriter.append("<p class='InfoSup'style='font-weight: bold;'>"+ScenarioCount+")<br></p>");
			PrintWriter.append("<p class='InfoSub'>Current Activity Reservation Criteria</p>");
			PrintWriter.append("<p class='InfoSub'>Portal URL :- "+PG_Properties.getProperty("Baseurl")+"</p>");
			PrintWriter.append("<p class='InfoSub'>Scenario :- "+search.getScenariotype()+"</p>");
			PrintWriter.append("<p class='InfoSub'>Selling Currency :- "+search.getSellingCurrency()+"</p>");
			PrintWriter.append("<p class='InfoSub'>Destination Name :- "+search.getDestination()+"</p>");
			PrintWriter.append("<p class='InfoSub'>Date From :- "+search.getDateFrom()+"</p>");
			PrintWriter.append("<p class='InfoSub'>Date To :- "+search.getDateTo()+"</p>");
			PrintWriter.append("<p class='InfoSub'>No of Adults :- "+search.getAdults()+"</p>");
			PrintWriter.append("<p class='InfoSub'>No of Children :- "+search.getChildren()+"</p>");
			if (!(UserTypeDC_B2B == WEB_TourOperatorType.WEB_DC)) {
				PrintWriter.append("<p class='InfoSub'>To Type :- "+UserTypeDC_B2B+"</p>");
			
			}else{
				PrintWriter.append("<p class='InfoSub'>User Type :- "+UserTypeDC_B2B+"</p>");
			}
			PrintWriter.append("<p class='InfoSub'>Discount :- "+discountYesNo+"</p>");
			PrintWriter.append("<p class='InfoSub'>Quotation :- "+search.getQuotationReq()+"</p>");
			PrintWriter.append("<p class='InfoSub'>Payment Type :- "+paymentType+"</p>");
			PrintWriter.append("</div>");
			PrintWriter.append("<br>");
			testCaseCount = 0;
			toAgentCommission = 0;
			
			
			if(!(PG_Properties.getProperty("PortalCurrency").toLowerCase().equals(supplierCurrency.toLowerCase()))){
				
				for(Entry<String, String> entry: currencyMap.entrySet()) {
					if(supplierCurrency.toLowerCase().equals(entry.getKey().toLowerCase())){
						
						rateConvertforUSD = Double.parseDouble(entry.getValue());
						
						ArrayList<WEB_ActivityInfo> activityListForRate = search.getInventoryInfo();
						
						for (WEB_ActivityInfo activityInfo : activityListForRate) {
							
							if (!(UserTypeDC_B2B == WEB_TourOperatorType.WEB_DC)) {
								
								ResultsPage_SubTotal =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_SubTotal()) / rateConvertforUSD ));
								ResultsPage_Taxes =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_TotalTax()) / rateConvertforUSD ));						
								ResultsPage_TotalPayable =  (int) Math.ceil((   (Double.parseDouble(activitydetails.getToActivity_SubTotal()) + (Double.parseDouble(activitydetails.getToActivity_TotalTax()))  / rateConvertforUSD )));
									
								PaymentPage_SubTotal =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_SubTotal()) / rateConvertforUSD ));
								PaymentPage_Taxes =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_TotalTax()) / rateConvertforUSD ));
								PaymentPage_TotalPayable =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_TotalValue()) / rateConvertforUSD ));
								
								PaymentPage_TotalGrossBookingValue =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_SubTotal()) / rateConvertforUSD ));
								
								if (search.getQuotationReq().equalsIgnoreCase("No")) {
									PaymentPage_TotalTax =  (int) Math.ceil(((Double.parseDouble(activitydetails.getToActivity_TotalTax()) + Double.parseDouble(activityInfo.getCreditCardFee()) ) / rateConvertforUSD ));
									PaymentPage_TotalPackageValue =  (int) Math.ceil((( Double.parseDouble(activitydetails.getToActivity_TotalValue()) + Double.parseDouble(activityInfo.getCreditCardFee())  ) / rateConvertforUSD ));									
								} else {
									PaymentPage_TotalTax =  (int) Math.ceil(((Double.parseDouble(activitydetails.getToActivity_TotalTax())  ) / rateConvertforUSD ));
									PaymentPage_TotalPackageValue =  (int) Math.ceil((( Double.parseDouble(activitydetails.getToActivity_TotalValue())  ) / rateConvertforUSD ));															
								}
																	
								if (UserTypeDC_B2B == WEB_TourOperatorType.WEB_NETCASH || UserTypeDC_B2B == WEB_TourOperatorType.WEB_NETCREDITLPON || UserTypeDC_B2B == WEB_TourOperatorType.WEB_NETCREDITLPOY) {
									
									PaymentPage_AmountProcessed =  (int) Math.ceil(( ( Double.parseDouble(activitydetails.getToActivity_TotalValue()) + Double.parseDouble(activityInfo.getCreditCardFee()) ) / rateConvertforUSD ));
								}
								
								if (UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCASH || UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCREDITLPONO || UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCREDITLPOY) {
									
									PaymentPage_AmountProcessed =  (int) Math.ceil(( ( Double.parseDouble(activitydetails.getToActivity_TotalValue()) + Double.parseDouble(activityInfo.getCreditCardFee())  - Double.parseDouble(activitydetails.getToAgentCommission())) / rateConvertforUSD ));
								}
								
								PaymentPage_AmountCheckIn =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_AmountDue()) / rateConvertforUSD ));
								PaymentPage_CreditCardFee =  (int) Math.ceil((Double.parseDouble(activityInfo.getCreditCardFee()) / rateConvertforUSD )); 		
								
								ConfirmationPage_SubTotal =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_SubTotal()) / rateConvertforUSD ));
								ConfirmationPage_Taxes =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_TotalTax()) / rateConvertforUSD ));
								ConfirmationPage_TotalPayable =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_TotalValue()) / rateConvertforUSD ));
								
								Confirmation_TotalGrossBookingValue =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_SubTotal()) / rateConvertforUSD ));
								
								if (search.getQuotationReq().equalsIgnoreCase("No")) {
									ConfirmationPage_TotalTax =  (int) Math.ceil(((Double.parseDouble(activitydetails.getToActivity_TotalTax()) + Double.parseDouble(activityInfo.getCreditCardFee()) ) / rateConvertforUSD ));
									ConfirmationPage_TotalPackageValue =  (int) Math.ceil((( Double.parseDouble(activitydetails.getToActivity_TotalValue()) + Double.parseDouble(activityInfo.getCreditCardFee())  ) / rateConvertforUSD ));
									
								}else{
									ConfirmationPage_TotalTax =  (int) Math.ceil(((Double.parseDouble(activitydetails.getToActivity_TotalTax()) ) / rateConvertforUSD ));
									ConfirmationPage_TotalPackageValue =  (int) Math.ceil((( Double.parseDouble(activitydetails.getToActivity_TotalValue())  ) / rateConvertforUSD ));
									
								}
								
								if (UserTypeDC_B2B == WEB_TourOperatorType.WEB_NETCASH || UserTypeDC_B2B == WEB_TourOperatorType.WEB_NETCREDITLPON || UserTypeDC_B2B == WEB_TourOperatorType.WEB_NETCREDITLPOY) {
									
									ConfirmationPage_AmountProcessed =  (int) Math.ceil(( ( Double.parseDouble(activitydetails.getToActivity_TotalValue()) + Double.parseDouble(activityInfo.getCreditCardFee()) ) / rateConvertforUSD ));
								}
								
								if (UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCASH || UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCREDITLPONO || UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCREDITLPOY) {
									
									ConfirmationPage_AmountProcessed =  (int) Math.ceil(( ( Double.parseDouble(activitydetails.getToActivity_TotalValue()) + Double.parseDouble(activityInfo.getCreditCardFee())  - Double.parseDouble(activitydetails.getToAgentCommission())) / rateConvertforUSD ));
								}
								
								ConfirmationPage_AmountCheckIn =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_AmountDue()) / rateConvertforUSD ));
													
								netRate = (int)(((Double.parseDouble(activitydetails.getToActivity_SubTotal()) * 100 ) / (100 + pMarkup))/ rateConvertforUSD );
								
								totalPayableforNoshow =  Double.parseDouble(activitydetails.getToActivity_SubTotal()) / rateConvertforUSD ;
							
								amount_onePerson = (int) Math.ceil(ResultsPage_SubTotal / paxCount);
								
								if (UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCASH || UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCREDITLPONO || UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCREDITLPOY) {
										
									toAgentCommission = (int) Math.ceil((Double.parseDouble(activitydetails.getToAgentCommission()) / rateConvertforUSD ));							
								}
								
														
							}else{
								
								//DC Rates
								
								ResultsPage_SubTotal =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_SubTotal()) / rateConvertforUSD ));
								ResultsPage_Taxes =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_TotalTaxnOther()) / rateConvertforUSD ));						
								ResultsPage_TotalPayable =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_Total()) / rateConvertforUSD ));
									
								PaymentPage_SubTotal =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_SubTotal()) / rateConvertforUSD ));
								PaymentPage_Taxes =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_TotalTaxnOther()) / rateConvertforUSD ));						
								PaymentPage_TotalPayable =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_Total()) / rateConvertforUSD ));				
								PaymentPage_TotalGrossBookingValue =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_Total()) / rateConvertforUSD ));
								PaymentPage_TotalTax =  (int) Math.ceil(( (Double.parseDouble(activityInfo.getPaymentPage_TotalTaxnOther()) + Double.parseDouble(activityInfo.getCreditCardFee()))  / rateConvertforUSD ));
								PaymentPage_TotalPackageValue =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_Total()) / rateConvertforUSD ));							
								PaymentPage_AmountProcessed =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_AmountNow()) / rateConvertforUSD ));
								PaymentPage_AmountCheckIn =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_AmountDue()) / rateConvertforUSD ));							
								PaymentPage_CreditCardFee =  (int) Math.ceil((Double.parseDouble(activityInfo.getCreditCardFee()) / rateConvertforUSD )); 		
								
								ConfirmationPage_SubTotal =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_SubTotal()) / rateConvertforUSD ));
								ConfirmationPage_Taxes =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_TotalTaxnOther()) / rateConvertforUSD ));
								ConfirmationPage_TotalPayable =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_Total()) / rateConvertforUSD ));
								Confirmation_TotalGrossBookingValue =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_Total()) / rateConvertforUSD ));
								ConfirmationPage_TotalTax =  (int) Math.ceil(((Double.parseDouble(activityInfo.getPaymentPage_TotalTaxnOther()) + Double.parseDouble(activityInfo.getCreditCardFee())) / rateConvertforUSD ));
								ConfirmationPage_TotalPackageValue =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_Total()) / rateConvertforUSD ));
								ConfirmationPage_AmountProcessed =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_AmountNow()) / rateConvertforUSD ));
								ConfirmationPage_AmountCheckIn =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_AmountDue()) / rateConvertforUSD ));
								
								netRate = (int)(((Double.parseDouble(activityInfo.getPaymentPage_SubTotal()) * 100 ) / (100 + pMarkup))/ rateConvertforUSD );
								//totalNetRateReservation = (int)((((Double.parseDouble(activityInfo.getPaymentPage_SubTotal()) * 100 ) / (100 + pMarkup)))/ rateConvertforUSD );
								
								totalPayableforNoshow =  Double.parseDouble(activityInfo.getPaymentPage_SubTotal()) / rateConvertforUSD ;
							
								amount_onePerson = (int) Math.ceil(ResultsPage_SubTotal / paxCount);
																
								
							}
							
						}	
					}
				}		
			}
						
			
			if(!(PG_Properties.getProperty("PortalCurrency").toLowerCase().equals(search.getSellingCurrency().toLowerCase()))){
				
				for(Entry<String, String> entry: currencyMap.entrySet()) {
					if(search.getSellingCurrency().toLowerCase().equals(entry.getKey().toLowerCase())){
						
						rateConvertforSearch = Double.parseDouble(entry.getValue());	
						
						ArrayList<WEB_ActivityInfo> activityListForRate = search.getInventoryInfo();
						
						for (WEB_ActivityInfo activityInfo : activityListForRate) {
										
							if (!(UserTypeDC_B2B == WEB_TourOperatorType.WEB_DC)) {
								
								ResultsPage_SubTotal =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_SubTotal()) / rateConvertforUSD ) * rateConvertforSearch);
								ResultsPage_Taxes =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_TotalTax()) / rateConvertforUSD ) * rateConvertforSearch);						
								ResultsPage_TotalPayable =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_SubTotal()) + (Double.parseDouble(activitydetails.getToActivity_TotalTax())) / rateConvertforUSD ) * rateConvertforSearch);
									
								PaymentPage_SubTotal =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_SubTotal()) / rateConvertforUSD ) * rateConvertforSearch);
								PaymentPage_Taxes =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_TotalTax()) / rateConvertforUSD ) * rateConvertforSearch);
								PaymentPage_TotalPayable =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_TotalValue()) / rateConvertforUSD ) * rateConvertforSearch);
								
								PaymentPage_TotalGrossBookingValue =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_SubTotal()) / rateConvertforUSD ) * rateConvertforSearch);
									
								if (search.getQuotationReq().equalsIgnoreCase("No")) {
									PaymentPage_TotalTax =  (int) Math.ceil(((Double.parseDouble(activitydetails.getToActivity_TotalTax()) + Double.parseDouble(activityInfo.getCreditCardFee()) ) / rateConvertforUSD ) * rateConvertforSearch);
									PaymentPage_TotalPackageValue =  (int) Math.ceil((( Double.parseDouble(activitydetails.getToActivity_TotalValue()) + Double.parseDouble(activityInfo.getCreditCardFee())  ) / rateConvertforUSD ) * rateConvertforSearch);								
								} else {
									PaymentPage_TotalTax =  (int) Math.ceil(((Double.parseDouble(activitydetails.getToActivity_TotalTax())  ) / rateConvertforUSD ) * rateConvertforSearch);
									PaymentPage_TotalPackageValue =  (int) Math.ceil((( Double.parseDouble(activitydetails.getToActivity_TotalValue())  ) / rateConvertforUSD ) * rateConvertforSearch);														
								}
															
								if (UserTypeDC_B2B == WEB_TourOperatorType.WEB_NETCASH || UserTypeDC_B2B == WEB_TourOperatorType.WEB_NETCREDITLPON || UserTypeDC_B2B == WEB_TourOperatorType.WEB_NETCREDITLPOY) {
									
									PaymentPage_AmountProcessed =  (int) Math.ceil(( ( Double.parseDouble(activitydetails.getToActivity_TotalValue()) + Double.parseDouble(activityInfo.getCreditCardFee()) ) / rateConvertforUSD ) * rateConvertforSearch);
								}
								
								if (UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCASH || UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCREDITLPONO || UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCREDITLPOY) {
									
									PaymentPage_AmountProcessed =  (int) Math.ceil((( ( Double.parseDouble(activitydetails.getToActivity_TotalValue()) + Double.parseDouble(activityInfo.getCreditCardFee()) - Double.parseDouble(activitydetails.getToAgentCommission()  ))) / rateConvertforUSD ) * rateConvertforSearch);
								}
								

								PaymentPage_AmountCheckIn =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_AmountDue()) / rateConvertforUSD ) * rateConvertforSearch);
								PaymentPage_CreditCardFee =  (int) Math.ceil((Double.parseDouble(activityInfo.getCreditCardFee()) / rateConvertforUSD ) * rateConvertforSearch); 		
								
								ConfirmationPage_SubTotal =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_SubTotal()) / rateConvertforUSD ) * rateConvertforSearch);
								
								if (search.getQuotationReq().equalsIgnoreCase("No")) {
									ConfirmationPage_TotalTax =  (int) Math.ceil(((Double.parseDouble(activitydetails.getToActivity_TotalTax()) + Double.parseDouble(activityInfo.getCreditCardFee()) )  / rateConvertforUSD ) * rateConvertforSearch);
									ConfirmationPage_TotalPackageValue =  (int) Math.ceil((( Double.parseDouble(activitydetails.getToActivity_TotalValue()) + Double.parseDouble(activityInfo.getCreditCardFee())  )  / rateConvertforUSD ) * rateConvertforSearch);
									
								}else{
									ConfirmationPage_TotalTax =  (int) Math.ceil(((Double.parseDouble(activitydetails.getToActivity_TotalTax()) )  / rateConvertforUSD ) * rateConvertforSearch);
									ConfirmationPage_TotalPackageValue =  (int) Math.ceil((( Double.parseDouble(activitydetails.getToActivity_TotalValue())  )  / rateConvertforUSD ) * rateConvertforSearch);
									
								}
								
								Confirmation_TotalGrossBookingValue =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_SubTotal()) / rateConvertforUSD ) * rateConvertforSearch);
								ConfirmationPage_TotalTax =  (int) Math.ceil(((Double.parseDouble(activitydetails.getToActivity_TotalTax()) + Double.parseDouble(activityInfo.getCreditCardFee()) ) / rateConvertforUSD ) * rateConvertforSearch);
								ConfirmationPage_TotalPackageValue =  (int) Math.ceil((( Double.parseDouble(activitydetails.getToActivity_TotalValue()) + Double.parseDouble(activityInfo.getCreditCardFee())  ) / rateConvertforUSD ) * rateConvertforSearch);
								
								if (UserTypeDC_B2B == WEB_TourOperatorType.WEB_NETCASH || UserTypeDC_B2B == WEB_TourOperatorType.WEB_NETCREDITLPON || UserTypeDC_B2B == WEB_TourOperatorType.WEB_NETCREDITLPOY) {
									
									ConfirmationPage_AmountProcessed =  (int) Math.ceil(( ( Double.parseDouble(activitydetails.getToActivity_TotalValue()) + Double.parseDouble(activityInfo.getCreditCardFee()) ) / rateConvertforUSD ) * rateConvertforSearch);
								}
								
								if (UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCASH || UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCREDITLPONO || UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCREDITLPOY) {
									
									ConfirmationPage_AmountProcessed =  (int) Math.ceil((( ( Double.parseDouble(activitydetails.getToActivity_TotalValue()) + Double.parseDouble(activityInfo.getCreditCardFee()) - Double.parseDouble(activitydetails.getToAgentCommission()  ))) / rateConvertforUSD ) * rateConvertforSearch);
								}
								
								
								ConfirmationPage_AmountCheckIn =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_AmountDue()) / rateConvertforUSD ) * rateConvertforSearch);
													
								netRate = (int)((((Double.parseDouble(activitydetails.getToActivity_SubTotal()) * 100 ) / (100 + pMarkup))/ rateConvertforUSD ) * rateConvertforSearch);
								
								totalPayableforNoshow =  ((Double.parseDouble(activitydetails.getToActivity_SubTotal()) / rateConvertforUSD) * rateConvertforSearch) ;
							
								amount_onePerson = (int) Math.ceil(ResultsPage_SubTotal / paxCount);
								
								if (UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCASH || UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCREDITLPONO || UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCREDITLPOY) {
									
									toAgentCommission = (int) Math.ceil((Double.parseDouble(activitydetails.getToAgentCommission()) / rateConvertforUSD ) * rateConvertforSearch);						
								}
								
								
							}else{
								
								//DC Rates
								
								ResultsPage_SubTotal =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_SubTotal()) / rateConvertforUSD ) * rateConvertforSearch);
								ResultsPage_Taxes =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_TotalTaxnOther()) / rateConvertforUSD ) * rateConvertforSearch);						
								ResultsPage_TotalPayable =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_Total()) / rateConvertforUSD ) * rateConvertforSearch);
									
								PaymentPage_SubTotal =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_SubTotal()) / rateConvertforUSD ) * rateConvertforSearch);
								PaymentPage_Taxes =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_TotalTaxnOther()) / rateConvertforUSD ) * rateConvertforSearch);						
								PaymentPage_TotalPayable =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_Total()) / rateConvertforUSD ) * rateConvertforSearch);				
								PaymentPage_TotalGrossBookingValue =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_Total()) / rateConvertforUSD ) * rateConvertforSearch);
								PaymentPage_TotalTax =  (int) Math.ceil(( (Double.parseDouble(activityInfo.getPaymentPage_TotalTaxnOther()) + Double.parseDouble(activityInfo.getCreditCardFee()))  / rateConvertforUSD ) * rateConvertforSearch);
								PaymentPage_TotalPackageValue =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_Total()) / rateConvertforUSD ) * rateConvertforSearch);							
								PaymentPage_AmountProcessed =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_AmountNow()) / rateConvertforUSD ) * rateConvertforSearch);
								PaymentPage_AmountCheckIn =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_AmountDue()) / rateConvertforUSD ) * rateConvertforSearch);							
								PaymentPage_CreditCardFee =  (int) Math.ceil((Double.parseDouble(activityInfo.getCreditCardFee()) / rateConvertforUSD ) * rateConvertforSearch); 		
								
								ConfirmationPage_SubTotal =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_SubTotal()) / rateConvertforUSD ) * rateConvertforSearch);
								ConfirmationPage_Taxes =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_TotalTaxnOther()) / rateConvertforUSD ) * rateConvertforSearch);
								ConfirmationPage_TotalPayable =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_Total()) / rateConvertforUSD ) * rateConvertforSearch);
								Confirmation_TotalGrossBookingValue =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_Total()) / rateConvertforUSD ) * rateConvertforSearch);
								ConfirmationPage_TotalTax =  (int) Math.ceil(((Double.parseDouble(activityInfo.getPaymentPage_TotalTaxnOther()) + Double.parseDouble(activityInfo.getCreditCardFee())) / rateConvertforUSD ) * rateConvertforSearch);
								ConfirmationPage_TotalPackageValue =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_Total()) / rateConvertforUSD ) * rateConvertforSearch);
								ConfirmationPage_AmountProcessed =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_AmountNow()) / rateConvertforUSD ) * rateConvertforSearch);
								ConfirmationPage_AmountCheckIn =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_AmountDue()) / rateConvertforUSD ) * rateConvertforSearch);
								
								netRate = (int)((((Double.parseDouble(activityInfo.getPaymentPage_SubTotal()) * 100 ) / (100 + pMarkup))/ rateConvertforUSD ) * rateConvertforSearch);
								//totalNetRateReservation = (int)((((Double.parseDouble(activityInfo.getPaymentPage_SubTotal()) * 100 ) / (100 + pMarkup)))/ rateConvertforUSD );
								
								totalPayableforNoshow =  ((Double.parseDouble(activityInfo.getPaymentPage_SubTotal()) / rateConvertforUSD) * rateConvertforSearch) ;
							
								amount_onePerson = (int) Math.ceil(ResultsPage_SubTotal / paxCount);
																								
							}
											
						}
					}
				}		
			}
			
			if (search.getSellingCurrency().equalsIgnoreCase(supplierCurrency)) {
				
				ArrayList<WEB_ActivityInfo> activityListForRate = search.getInventoryInfo();
				
				for (WEB_ActivityInfo activityInfo : activityListForRate) {
					
					if (!(UserTypeDC_B2B == WEB_TourOperatorType.WEB_DC)) {
						
						ResultsPage_SubTotal =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_SubTotal())));
						ResultsPage_Taxes =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_TotalTax())));						
						ResultsPage_TotalPayable =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_SubTotal()) + (Double.parseDouble(activitydetails.getToActivity_TotalTax()))));
							
						PaymentPage_SubTotal =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_SubTotal())));
						PaymentPage_Taxes =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_TotalTax())));
						PaymentPage_TotalPayable =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_TotalValue())));
						
						PaymentPage_TotalGrossBookingValue =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_SubTotal())));
						
						if (search.getQuotationReq().equalsIgnoreCase("No")) {
							PaymentPage_TotalTax =  (int) Math.ceil(((Double.parseDouble(activitydetails.getToActivity_TotalTax()) + Double.parseDouble(activityInfo.getCreditCardFee()) )));
							PaymentPage_TotalPackageValue =  (int) Math.ceil((( Double.parseDouble(activitydetails.getToActivity_TotalValue()) + Double.parseDouble(activityInfo.getCreditCardFee()) )));
							
						}else{
							PaymentPage_TotalTax =  (int) Math.ceil(((Double.parseDouble(activitydetails.getToActivity_TotalTax())  )));
							PaymentPage_TotalPackageValue =  (int) Math.ceil((( Double.parseDouble(activitydetails.getToActivity_TotalValue()) )));
							
						}					
						
						if (UserTypeDC_B2B == WEB_TourOperatorType.WEB_NETCASH || UserTypeDC_B2B == WEB_TourOperatorType.WEB_NETCREDITLPON || UserTypeDC_B2B == WEB_TourOperatorType.WEB_NETCREDITLPOY) {
							
							PaymentPage_AmountProcessed =  (int) Math.ceil(((Double.parseDouble(activitydetails.getToActivity_TotalValue()) + Double.parseDouble(activityInfo.getCreditCardFee()) )));						
						}
						
						if (UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCASH || UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCREDITLPONO || UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCREDITLPOY) {
							
							PaymentPage_AmountProcessed =  (int) Math.ceil((((Double.parseDouble(activitydetails.getToActivity_TotalValue()) + Double.parseDouble(activityInfo.getCreditCardFee()) - Double.parseDouble(activitydetails.getToAgentCommission())))));												
						}
											
						PaymentPage_AmountCheckIn =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_AmountDue())));
						PaymentPage_CreditCardFee =  (int) Math.ceil((Double.parseDouble(activityInfo.getCreditCardFee()))); 		
						
						ConfirmationPage_SubTotal =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_SubTotal())));
						ConfirmationPage_Taxes =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_TotalTax())));
						ConfirmationPage_TotalPayable =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_TotalValue())));
						
						Confirmation_TotalGrossBookingValue =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_SubTotal())));
						
						if (search.getQuotationReq().equalsIgnoreCase("No")) {
							ConfirmationPage_TotalTax =  (int) Math.ceil(((Double.parseDouble(activitydetails.getToActivity_TotalTax()) + Double.parseDouble(activityInfo.getCreditCardFee()) )));
							ConfirmationPage_TotalPackageValue =  (int) Math.ceil((( Double.parseDouble(activitydetails.getToActivity_TotalValue()) + Double.parseDouble(activityInfo.getCreditCardFee())  )));
						
						}else{
							ConfirmationPage_TotalTax =  (int) Math.ceil(((Double.parseDouble(activitydetails.getToActivity_TotalTax()))));
							ConfirmationPage_TotalPackageValue =  (int) Math.ceil((( Double.parseDouble(activitydetails.getToActivity_TotalValue()) )));
						
						}
						
												
						if (UserTypeDC_B2B == WEB_TourOperatorType.WEB_NETCASH || UserTypeDC_B2B == WEB_TourOperatorType.WEB_NETCREDITLPON || UserTypeDC_B2B == WEB_TourOperatorType.WEB_NETCREDITLPOY) {
							
							ConfirmationPage_AmountProcessed =  (int) Math.ceil(((Double.parseDouble(activitydetails.getToActivity_TotalValue()) + Double.parseDouble(activityInfo.getCreditCardFee()) )));						
						}
						
						if (UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCASH || UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCREDITLPONO || UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCREDITLPOY) {
							
							ConfirmationPage_AmountProcessed =  (int) Math.ceil((((Double.parseDouble(activitydetails.getToActivity_TotalValue()) + Double.parseDouble(activityInfo.getCreditCardFee()) - Double.parseDouble(activitydetails.getToAgentCommission())))));												
						}
						
						ConfirmationPage_AmountCheckIn =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_AmountDue())));
											
						netRate = (int)(((Double.parseDouble(activitydetails.getToActivity_SubTotal()) * 100 ) / (100 + pMarkup)));
						
						totalPayableforNoshow =  Double.parseDouble(activitydetails.getToActivity_SubTotal()) ;
					
						amount_onePerson = (int) Math.ceil(ResultsPage_SubTotal / paxCount);
						
						if (UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCASH || UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCREDITLPONO || UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCREDITLPOY) {
							
							toAgentCommission = (int) Math.ceil((Double.parseDouble(activitydetails.getToAgentCommission())));
						}
												
					}else{
						
						//DC Rates
						
						ResultsPage_SubTotal =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_SubTotal())));
						ResultsPage_Taxes =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_TotalTaxnOther())));						
						ResultsPage_TotalPayable =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_Total())));
							
						PaymentPage_SubTotal =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_SubTotal())));
						PaymentPage_Taxes =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_TotalTaxnOther())));						
						PaymentPage_TotalPayable =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_Total())));				
						PaymentPage_TotalGrossBookingValue =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_Total())));
						PaymentPage_TotalTax =  (int) Math.ceil(( (Double.parseDouble(activityInfo.getPaymentPage_TotalTaxnOther()) + Double.parseDouble(activityInfo.getCreditCardFee()))));
						PaymentPage_TotalPackageValue =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_Total())));							
						PaymentPage_AmountProcessed =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_AmountNow())));
						PaymentPage_AmountCheckIn =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_AmountDue()) ));							
						PaymentPage_CreditCardFee =  (int) Math.ceil((Double.parseDouble(activityInfo.getCreditCardFee()))); 		
						
						ConfirmationPage_SubTotal =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_SubTotal())));
						ConfirmationPage_Taxes =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_TotalTaxnOther())));
						ConfirmationPage_TotalPayable =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_Total())));
						Confirmation_TotalGrossBookingValue =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_Total()) ));
						ConfirmationPage_TotalTax =  (int) Math.ceil(((Double.parseDouble(activityInfo.getPaymentPage_TotalTaxnOther()) + Double.parseDouble(activityInfo.getCreditCardFee())) ));
						ConfirmationPage_TotalPackageValue =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_Total())));
						ConfirmationPage_AmountProcessed =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_AmountNow())));
						ConfirmationPage_AmountCheckIn =  (int) Math.ceil((Double.parseDouble(activityInfo.getPaymentPage_AmountDue())));
						
						netRate = (int)(((Double.parseDouble(activityInfo.getPaymentPage_SubTotal()) * 100 ) / (100 + pMarkup) ));
						
						totalPayableforNoshow =  Double.parseDouble(activityInfo.getPaymentPage_SubTotal())  ;
					
						amount_onePerson = (int) Math.ceil(ResultsPage_SubTotal / paxCount);
															
					}
				
				}			
			}
			
			
			
			if (activitydetails.isResultsAvailable() == true) {
								
				PrintWriter.append("<br><br><table ><tr><th>Test Case No</th><th>Test Description</th><th>Expect Values - [Excel Value]</th><th>Actual Values</th><th>Test Status</th></tr>");				
				
				////Booking Engine
				PrintWriter.append("<tr><td class='fontiiii'>Booking Engine</td><tr>"); 
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Booking Engine</td>");
				PrintWriter.append("<td>Booking Engine Should be Available</td>");
				
				if (activitydetails.isBookingEngineLoaded() == true) {
					
					PrintWriter.append("<td>Booking Engine Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Booking Engine Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				///
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Country Of Residence</td>");
				PrintWriter.append("<td>Country Of Residence Should be Available</td>");
				
				if (activitydetails.isCountryLoaded() == true) {
					
					PrintWriter.append("<td>Country Of Residence Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Country Of Residence Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Destination</td>");
				PrintWriter.append("<td>Destination Should be Available</td>");
				
				if (activitydetails.isCityLoaded() == true) {
					
					PrintWriter.append("<td>Destination Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Destination Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Departure Date</td>");
				PrintWriter.append("<td>Departure Date Should be Available</td>");
				
				if (activitydetails.isDateFromLoaded() == true) {
					
					PrintWriter.append("<td>Departure Date Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Departure Date Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Arrival Date</td>");
				PrintWriter.append("<td>Arrival Date Should be Available</td>");
				
				if (activitydetails.isDateToLoaded() == true) {
					
					PrintWriter.append("<td>Arrival Date Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Arrival Date Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Adults Drop Down</td>");
				PrintWriter.append("<td>Adults Drop Down Should be Available</td>");
				
				if (activitydetails.isAdultsLoaded() == true) {
					
					PrintWriter.append("<td>Adults Drop Down Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Adults Drop Down Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Childs Drop Down</td>");
				PrintWriter.append("<td>Childs Drop Down Should be Available</td>");
				
				if (activitydetails.isChildLoaded() == true) {
					
					PrintWriter.append("<td>Childs Drop Down Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Childs Drop Down Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				
				
				
				
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Country List loading</td>");
				PrintWriter.append("<td>Country List Should be Available</td>");
				
				if (activitydetails.isCountryListLoaded() == true) {
					
					PrintWriter.append("<td>Country List Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Country List Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Calender loading</td>");
				PrintWriter.append("<td>Calender Should be Available</td>");
				
				if (activitydetails.isCalenderAvailble() == true) {
					
					PrintWriter.append("<td>Calender Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Calender Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Show Additional Search Options link</td>");
				PrintWriter.append("<td>Show Additional Search Options link Should be Available</td>");
				
				if (activitydetails.getShowAdd().replaceAll(" ", "").equalsIgnoreCase("ShowAdditionalSearchOptions")) {
					
					PrintWriter.append("<td>Show Additional Search Options link Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Show Additional Search Options link Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				
				////
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Type loading</td>");
				PrintWriter.append("<td>Activity Type Should be Available</td>");
				
				if (activitydetails.isActivityType() == true) {
					
					PrintWriter.append("<td>Activity Type Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Activity Type Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Program Category loading</td>");
				PrintWriter.append("<td>Program Category Should be Available</td>");
				
				if (activitydetails.isProgramCat() == true) {
					
					PrintWriter.append("<td>Program Category Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Program Category Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Preferred Currency loading</td>");
				PrintWriter.append("<td>Preferred Currency Should be Available</td>");
				
				if (activitydetails.isPreCurrency() == true) {
					
					PrintWriter.append("<td>Preferred Currency Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Preferred Currency Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Promotion Code loading</td>");
				PrintWriter.append("<td>Promotion Code Should be Available</td>");
				
				if (activitydetails.isPromoCode() == true) {
					
					PrintWriter.append("<td>Promotion Code Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Promotion Code Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Currency List loading</td>");
				PrintWriter.append("<td>Currency List loading Should be Available</td>");
				
				if (activitydetails.isCurrencyListLoaded() == true) {
					
					PrintWriter.append("<td>Currency List loading Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Currency List loading Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Program Category List loading</td>");
				PrintWriter.append("<td>Program Category List loading Should be Available</td>");
				
				if (activitydetails.isProgCatListLoaded() == true) {
					
					PrintWriter.append("<td>Program Category List loading Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Program Category List loading Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				
				
				////
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hide Additional Search Options link</td>");
				PrintWriter.append("<td>Hide Additional Search Options link Should be Available</td>");
				
				if (activitydetails.getHideAdd().replaceAll(" ", "").equalsIgnoreCase("HideAdditionalSearchOptions")) {
					
					PrintWriter.append("<td>Hide Additional Search Options link Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Hide Additional Search Options link Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				
				
				
			
				/////  Results Page
				PrintWriter.append("<tr><td class='fontiiii'>Results Page</td><tr>"); 
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Show Additional Filter Options link</td>");
				PrintWriter.append("<td>Show Additional Filter Options link Should be Available</td>");
				
				if (activitydetails.getShowResultsAdd().replaceAll(" ", "").equalsIgnoreCase("Showadditionalfilters")) {
					
					PrintWriter.append("<td>Show Additional Filter Options link Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Show Additional Filter Options link Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Program Category loading</td>");
				PrintWriter.append("<td>Program Category Should be Available</td>");
				
				if (activitydetails.isResultsProgramCat() == true) {
					
					PrintWriter.append("<td>Program Category Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Program Category Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Preferred Currency loading</td>");
				PrintWriter.append("<td>Preferred Currency Should be Available</td>");
				
				if (activitydetails.isResultsPreCurrency() == true) {
					
					PrintWriter.append("<td>Preferred Currency Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Preferred Currency Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Promotion Code loading</td>");
				PrintWriter.append("<td>Promotion Code Should be Available</td>");
				
				if (activitydetails.isResultsPromoCode() == true) {
					
					PrintWriter.append("<td>Promotion Code Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Promotion Code Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Currency List loading</td>");
				PrintWriter.append("<td>Currency List loading Should be Available</td>");
				
				if (activitydetails.isResultsCurrencyListLoaded() == true) {
					
					PrintWriter.append("<td>Currency List loading Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Currency List loading Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Program Category List loading</td>");
				PrintWriter.append("<td>Program Category List loading Should be Available</td>");
				
				if (activitydetails.isResultsProgCatListLoaded() == true) {
					
					PrintWriter.append("<td>Program Category List loading Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Program Category List loading Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hide Additional Filter Options link</td>");
				PrintWriter.append("<td>Hide Additional Filter Options link Should be Available</td>");
				
				if (activitydetails.getHideResultsAdd().replaceAll(" ", "").equalsIgnoreCase("Hideadditionalfilters")) {
					
					PrintWriter.append("<td>Hide Additional Filter Options link Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Hide Additional Filter Options link Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Price range filter loading</td>");
				PrintWriter.append("<td>Price range filter Should be Available</td>");
				
				if (activitydetails.isPriceRangeFilter() == true) {
					
					PrintWriter.append("<td>Price range filter Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Price range filter Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Program Availability loading</td>");
				PrintWriter.append("<td>Program Availability Should be Available</td>");
				
				if (activitydetails.isProgramAvailablity() == true) {
					
					PrintWriter.append("<td>Program Availability Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Program Availability Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Name Filter loading</td>");
				PrintWriter.append("<td>Activity Name Filter Should be Available</td>");
				
				if (activitydetails.isActivityNameFilter() == true) {
					
					PrintWriter.append("<td>Activity Name Filter Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Activity Name Filter Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				
				
				
				
				
				
				
				
						
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity results availability</td>");
				PrintWriter.append("<td>Results Should be available</td>");
							
				String resultsAvailable = Boolean.toString(activitydetails.isResultsAvailable());
				
				if(resultsAvailable.equals("true")){
						
					PrintWriter.append("<td>"+resultsAvailable+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+resultsAvailable+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
								
				String dateFromToCal = search.getDateFrom();
				String dateToCal = search.getDateTo();
				SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
				
				Date d1 = null;
				Date d2 = null;
		 
				try {
					d1 = format.parse(dateFromToCal);
					d2 = format.parse(dateToCal);
		 
					//in milliseconds
					long diffD = d2.getTime() - d1.getTime();
					int diffDays = (int)(diffD / (24 * 60 * 60 * 1000));
							
					testCaseCount ++;	
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity days count</td>");
					PrintWriter.append("<td>From :- "+dateFromToCal+" - To :- "+dateToCal+" = "+(diffDays+1)+"</td>");
							
					if(activitydetails.getDaysCount() == (diffDays+1)){
							
						PrintWriter.append("<td>Results page days count - "+activitydetails.getDaysCount()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>Results page days count - "+activitydetails.getDaysCount()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
	
		 
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Add to Cart</td>");
				PrintWriter.append("<td>Should be add to the cart</td>");
				
				if (activitydetails.isAddToCart() == true) {
					
					PrintWriter.append("<td>Added</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>No activity in the cart</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Remove Activity From Cart</td>");
				PrintWriter.append("<td>Should be remove</td>");
				
				if (activitydetails.isActivityRemoved() == true) {
					
					PrintWriter.append("<td>Removed</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>No activity in the cart</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				int prizeChanged_1 = Integer.parseInt(activitydetails.getPrizeChanged_1());
				int PrizeResults_1 = Integer.parseInt(activitydetails.getPrizeResults_1());
				int prizeChanged_2 = Integer.parseInt(activitydetails.getPrizeChanged_2());
				int PrizeResults_2 = Integer.parseInt(activitydetails.getPrizeResults_2());
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Price Range Filter - Test 1</td>");
				PrintWriter.append("<td>Min Prize is - "+search.getSellingCurrency()+" "+activitydetails.getMinPrize()+"</td>");
				
				if (activitydetails.getMinPrize().equals(activitydetails.getPageLoadedResults())) {
					
					PrintWriter.append("<td>Results Element 1 - "+search.getSellingCurrency()+" "+activitydetails.getPageLoadedResults()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Results Element 1 - "+search.getSellingCurrency()+" "+activitydetails.getPageLoadedResults()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Price Range Filter - Test 2</td>");
				PrintWriter.append("<td>Min Prize set to - "+search.getSellingCurrency()+" "+activitydetails.getPrizeChanged_1()+"</td>");
				
				if (PrizeResults_1 >= prizeChanged_1) {
					
					PrintWriter.append("<td>Results Element 1 - "+search.getSellingCurrency()+" "+activitydetails.getPrizeResults_1()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Results Element 1 - "+search.getSellingCurrency()+" "+activitydetails.getPrizeResults_1()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Price Range Filter - Test 3</td>");
				PrintWriter.append("<td>Max Prize set to - "+search.getSellingCurrency()+" "+activitydetails.getPrizeChanged_2()+"</td>");
				
				if (prizeChanged_2 == PrizeResults_2) {
					
					PrintWriter.append("<td>Results Element 1 - "+search.getSellingCurrency()+" "+activitydetails.getPrizeResults_2()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Results Element 1 - "+search.getSellingCurrency()+" "+activitydetails.getPrizeResults_2()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				
				
				
				////
				
				
				for (int i = 0; i < activitydetails.getResulteBlockSizeOnReq(); i++) {
					
					if (activitydetails.getOnRequestLabels().get(i).contains("Not")) {
						
						testCaseCount ++;			
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>0 - On Request programs available</td>");
						PrintWriter.append("<td>Activity Program Availability - On Request</td>");
						PrintWriter.append("<td>"+activitydetails.getOnRequestLabels().get(i)+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
						
						break;
						
						
					}else{
						
						testCaseCount ++;			
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>"+activitydetails.getResulteBlockSizeOnReq()+" - On Request programs available</td>");
						PrintWriter.append("<td>Activity Program Availability - "+(i+1)+" - On Request</td>");
						
						if (activitydetails.getOnRequestLabels().get(i).replaceAll(" ", "").equalsIgnoreCase("on request".replaceAll(" ", ""))) {
							
							PrintWriter.append("<td>"+activitydetails.getOnRequestLabels().get(i)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
							
						} else {
							PrintWriter.append("<td>"+activitydetails.getOnRequestLabels().get(i)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");		
						}
					}
					
							
				}
				
				
				
				
					
				///
				/*boolean equalsStringDescend = activitydetails.getBeforeSortingDescending().equals(activitydetails.getAfterSortingDescending());
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Results display order - (Highest to Lowest) [Descending]</td>");
				PrintWriter.append("<td>"+activitydetails.getAfterSortingDescending()+"</td>");
							
				try {
					if(equalsStringDescend == true){
							
						PrintWriter.append("<td>"+activitydetails.getBeforeSortingDescending()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getBeforeSortingDescending()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
				} catch (Exception e1) {
					PrintWriter.append("<td>"+e1.getMessage()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				boolean equalsString = activitydetails.getBeforeSorting().equals(activitydetails.getAfterSorting());
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Results display order - (Lowest to Highest) [Ascending]</td>");
				PrintWriter.append("<td>"+activitydetails.getAfterSorting()+"</td>");
							
				try {
					if(equalsString == true){
							
						PrintWriter.append("<td>"+activitydetails.getBeforeSorting()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getBeforeSorting()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
				} catch (Exception e1) {
					PrintWriter.append("<td>"+e1.getMessage()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}*/
				
				
				///
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Currency - Results Page </td>");
				PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
							
				if(search.getSellingCurrency().toLowerCase().equals(activitydetails.getActivityCurrency().toLowerCase())){
						
					PrintWriter.append("<td>"+activitydetails.getActivityCurrency()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+activitydetails.getActivityCurrency()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Cancellation Policy</td>");
				PrintWriter.append("<td>Should be loaded</td>");
				
				if (activitydetails.isCancelPolicy() == true) {
					
					PrintWriter.append("<td>Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");		
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity More info Description - Results Page</td>");
				PrintWriter.append("<td>"+activityDescription+"</td>");
				
				if(activitydetails.getActDescription().replaceAll(" ", "").toLowerCase().contains(activityDescription.replaceAll(" ", "").toLowerCase())){
						
					PrintWriter.append("<td>"+activitydetails.getActDescription()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+activitydetails.getActDescription()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
							
				
				////
				
				String childAges = search.getAgeOfChildren();
				String[] partsEA = childAges.split("#");
				
				List<String> valueList = new ArrayList<String>();
				
				for (String value : partsEA) {
					valueList.add(value);
				}
				
				/////
				
				if (activitydetails.getAddAndContinue().equalsIgnoreCase("true")) {
					
					String currencyNRate = search.getSellingCurrency() + Integer.toString(ResultsPage_SubTotal + ResultsPage_Taxes);
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Results Page - Shopping Basket Sub Total</td>");
					PrintWriter.append("<td>"+currencyNRate+"</td>");
								
					if(currencyNRate.replaceAll(" ", "").equalsIgnoreCase(activitydetails.getCartVallue().replaceAll(" ", ""))){
							
						PrintWriter.append("<td>"+activitydetails.getCartVallue()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getCartVallue()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
				} 
				
					
				/////  Payment Page
				
				PrintWriter.append("<tr><td class='fontiiii'>Payments Page</td><tr>"); 
				
				PrintWriter.append("<tr><td class='fontiiii'>Payments Page - Shopping Basket</td><tr>"); 
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Currency - Payment Page Shopping Basket</td>");
				PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
							
				if(search.getSellingCurrency().toLowerCase().equals(activitydetails.getShoppingCart_CartCurrency().toLowerCase())){
						
					PrintWriter.append("<td>"+activitydetails.getShoppingCart_CartCurrency()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+activitydetails.getShoppingCart_CartCurrency()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				if (UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCASH || UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCREDITLPONO || UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCREDITLPOY) {
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>TO Agent Commission - Payment Page</td>");
					PrintWriter.append("<td>"+toAgentCommission+"</td>");
								
					if(Integer.toString(toAgentCommission).equals(activitydetails.getPaymentsPage_AgentCommission())){
							
						PrintWriter.append("<td>"+activitydetails.getPaymentsPage_AgentCommission()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getPaymentsPage_AgentCommission()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
				
				}
				
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Sub Total - Payment Page Shopping Basket</td>");
				PrintWriter.append("<td>"+PaymentPage_SubTotal+"</td>");
							
				if(Integer.toString(PaymentPage_SubTotal).equals(activitydetails.getShoppingCart_subTotal())){
						
					PrintWriter.append("<td>"+activitydetails.getShoppingCart_subTotal()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+activitydetails.getShoppingCart_subTotal()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Tax and Other Charges - Payment Page Shopping Basket</td>");
				PrintWriter.append("<td>"+PaymentPage_TotalTax+"</td>");
							
				if(Integer.toString(PaymentPage_TotalTax).equals(activitydetails.getShoppingCart_TotalTax())){
						
					PrintWriter.append("<td>"+activitydetails.getShoppingCart_TotalTax()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+activitydetails.getShoppingCart_TotalTax()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				creditTax = (int) Math.ceil(PaymentPage_CreditCardFee);
				int disValue = 0;
				discountedVale = 0;
				
				if (discountYesNo.equalsIgnoreCase("Yes")) {
					
					if (PG_Properties.getProperty("Discount_Type").equalsIgnoreCase("percentage")) {
						
						disValue = Integer.parseInt(PG_Properties.getProperty("Discount_Value"));
						discountedVale = ((PaymentPage_SubTotal * disValue)/100);							
					}
					
					if (PG_Properties.getProperty("Discount_Type").equalsIgnoreCase("value")) {			
						
						disValue = Integer.parseInt(PG_Properties.getProperty("Discount_Value"));
						discountedVale = PaymentPage_SubTotal + disValue;
					}
				
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Discount 1 - Payment Page Shopping Basket</td>");
					PrintWriter.append("<td>"+discountedVale+"</td>");
								
					if(activitydetails.getPaymentsPage_DiscountValue1().contains(Integer.toString((discountedVale)))){
							
						PrintWriter.append("<td>"+activitydetails.getPaymentsPage_DiscountValue1()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getPaymentsPage_DiscountValue1()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					
					if (paymentType.equals("Pay Online")){
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Discount 2 - Payment Page Billing Info</td>");
						PrintWriter.append("<td>"+discountedVale+"</td>");
									
						if(activitydetails.getPaymentsPage_DiscountValue2().contains(Integer.toString((discountedVale)))){
								
							PrintWriter.append("<td>"+activitydetails.getPaymentsPage_DiscountValue2()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+activitydetails.getPaymentsPage_DiscountValue2()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
												
					}
					
				}
				
				
				
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Activity Booking Value - Payment Page Shopping Basket</td>");
				PrintWriter.append("<td>"+PaymentPage_TotalPackageValue+"</td>");
							
				if(Integer.toString(PaymentPage_TotalPackageValue).equals(activitydetails.getShoppingCart_TotalValue())){
						
					PrintWriter.append("<td>"+activitydetails.getShoppingCart_TotalValue()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+activitydetails.getShoppingCart_TotalValue()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				///
				
				
				
				if (paymentType.equals("Pay Online")) {
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Amount being Processed Now - Payment Page Shopping Basket</td>");
					PrintWriter.append("<td>"+PaymentPage_AmountProcessed+"</td>");
								
					if(Integer.toString(PaymentPage_AmountProcessed).equals(activitydetails.getShoppingCart_AmountNow())){
							
						PrintWriter.append("<td>"+activitydetails.getShoppingCart_AmountNow()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getShoppingCart_AmountNow()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					//ShoppingCart_TotalActivityValue
					try {
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Total (My Basket) - Payment Page Shopping Basket</td>");
						PrintWriter.append("<td>"+PaymentPage_AmountProcessed+"</td>");
									
						if(Integer.toString(PaymentPage_AmountProcessed).equals(activitydetails.getShoppingCart_TotalActivityValue())){
								
							PrintWriter.append("<td>"+activitydetails.getShoppingCart_TotalActivityValue()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+activitydetails.getShoppingCart_TotalActivityValue()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
					} catch (Exception e) {
						PrintWriter.append("<td>"+activitydetails.getShoppingCart_TotalActivityValue()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					//My Basket Value
					try {
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>My Basket Total - Payment Page Shopping Basket</td>");
						PrintWriter.append("<td>"+PaymentPage_AmountProcessed+"</td>");
									
						if(Integer.toString(PaymentPage_AmountProcessed).equals(activitydetails.getShoppingCart_MyBasketTotalValue())){
								
							PrintWriter.append("<td>"+activitydetails.getShoppingCart_MyBasketTotalValue()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+activitydetails.getShoppingCart_MyBasketTotalValue()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
					} catch (Exception e) {
						PrintWriter.append("<td>"+activitydetails.getShoppingCart_MyBasketTotalValue()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
				}else{				
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Amount being Processed Now - Payment Page Shopping Basket</td>");
					PrintWriter.append("<td>"+(PaymentPage_AmountProcessed+toAgentCommission)+"</td>");
								
					if(Integer.toString(PaymentPage_AmountProcessed+toAgentCommission).equals(activitydetails.getShoppingCart_AmountNow())){
							
						PrintWriter.append("<td>"+activitydetails.getShoppingCart_AmountNow()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getShoppingCart_AmountNow()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					//ShoppingCart_TotalActivityValue
					try {
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Total (My Basket) - Payment Page Shopping Basket</td>");
						PrintWriter.append("<td>"+(PaymentPage_AmountProcessed+toAgentCommission)+"</td>");
									
						if(Integer.toString(PaymentPage_AmountProcessed+toAgentCommission).equals(activitydetails.getShoppingCart_TotalActivityValue())){
								
							PrintWriter.append("<td>"+activitydetails.getShoppingCart_TotalActivityValue()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+activitydetails.getShoppingCart_TotalActivityValue()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
					} catch (Exception e) {
						PrintWriter.append("<td>"+activitydetails.getShoppingCart_TotalActivityValue()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					//My Basket Value
					try {
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>My Basket Total - Payment Page Shopping Basket</td>");
						PrintWriter.append("<td>"+(PaymentPage_AmountProcessed+toAgentCommission)+"</td>");
									
						if(Integer.toString(PaymentPage_AmountProcessed+toAgentCommission).equals(activitydetails.getShoppingCart_MyBasketTotalValue())){
								
							PrintWriter.append("<td>"+activitydetails.getShoppingCart_MyBasketTotalValue()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+activitydetails.getShoppingCart_MyBasketTotalValue()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
					} catch (Exception e) {
						PrintWriter.append("<td>"+activitydetails.getShoppingCart_MyBasketTotalValue()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
				}
				
				
				
				
				
				
				
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Amount due at Check-In - Payment Page Shopping Basket</td>");
				PrintWriter.append("<td>"+PaymentPage_AmountCheckIn+"</td>");
							
				if(Integer.toString(PaymentPage_AmountCheckIn).equals(activitydetails.getShoppingCart_AmountCheckIn())){
						
					PrintWriter.append("<td>"+activitydetails.getShoppingCart_AmountCheckIn()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+activitydetails.getShoppingCart_AmountCheckIn()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				/////
				
				
				
				int subTotalAllProducts = PaymentPage_SubTotal;
				int totalTaxAllProducts = PaymentPage_Taxes + creditTax ;
				int totalMybasketValue = PaymentPage_SubTotal + totalTaxAllProducts - discountedVale;
				int agentCommission = toAgentCommission;
				
			
				
				if (search.getQuotationReq().equalsIgnoreCase("No")) {
					
					if (!(UserTypeDC_B2B == WEB_TourOperatorType.WEB_DC) ) {
						
						if (!(paymentType.equalsIgnoreCase("Pay Offline"))) {
							
							testCaseCount ++;
							PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Currency - Payment Page Billing Info</td>");
							PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
										
							if(activitydetails.getPaymentBilling_CardCurrency().replaceAll(" ", "").contains(search.getSellingCurrency())){
									
								PrintWriter.append("<td>"+activitydetails.getPaymentBilling_CardCurrency()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else {
								
								PrintWriter.append("<td>"+activitydetails.getPaymentBilling_CardCurrency()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
							}
							
							
							
							
							testCaseCount ++;
							PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Sub Total - Payment Page Billing Info</td>");
							PrintWriter.append("<td>"+subTotalAllProducts+"</td>");
										
							if(activitydetails.getPaymentBilling_subTotal().contains(Integer.toString(subTotalAllProducts))){
									
								PrintWriter.append("<td>"+activitydetails.getPaymentBilling_subTotal()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else {				
								PrintWriter.append("<td>"+activitydetails.getPaymentBilling_subTotal()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
							}
							
							testCaseCount ++;
							PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Taxes and Other Charges - Payment Page Billing Info</td>");
							PrintWriter.append("<td>"+totalTaxAllProducts+"</td>");
										
							if(activitydetails.getPaymentBilling_TotalTax().contains(Integer.toString(totalTaxAllProducts))){
									
								PrintWriter.append("<td>"+activitydetails.getPaymentBilling_TotalTax()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else {				
								PrintWriter.append("<td>"+activitydetails.getPaymentBilling_TotalTax()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
							}
							
							testCaseCount ++;
							PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Value - Payment Page Billing Info</td>");
							PrintWriter.append("<td>"+totalMybasketValue+"</td>");
										
							if(activitydetails.getPaymentBilling_TotalValue().contains(Integer.toString(totalMybasketValue))){
									
								PrintWriter.append("<td>"+activitydetails.getPaymentBilling_TotalValue()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else {				
								PrintWriter.append("<td>"+activitydetails.getPaymentBilling_TotalValue()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
							}
							
							
							if (UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCASH || UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCREDITLPOY || UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCREDITLPONO ) {
								
								testCaseCount ++;
								PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Amount being Processed Now - Payment Page Billing Info</td>");
								PrintWriter.append("<td>"+(totalMybasketValue - agentCommission)+"</td>");
											
								if(activitydetails.getPaymentBilling_AmountNow().contains(Integer.toString(totalMybasketValue - agentCommission))){
										
									PrintWriter.append("<td>"+activitydetails.getPaymentBilling_AmountNow()+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
								}
								
								else {				
									PrintWriter.append("<td>"+activitydetails.getPaymentBilling_AmountNow()+"</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
								}
								
								testCaseCount ++;
								PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Package Booking Value - Payment Page Billing Info</td>");
								PrintWriter.append("<td>"+(totalMybasketValue - agentCommission)+"</td>");
											
								if(activitydetails.getPaymentBilling_CardTotal().contains(Integer.toString((totalMybasketValue - agentCommission)))){
										
									PrintWriter.append("<td>"+activitydetails.getPaymentBilling_CardTotal()+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
								}
								
								else {				
									PrintWriter.append("<td>"+activitydetails.getPaymentBilling_CardTotal()+"</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
								}
												
								
								testCaseCount ++;
								PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Value - Payment Gateway</td>");
								PrintWriter.append("<td>"+(totalMybasketValue - agentCommission)+"</td>");
											
								if(activitydetails.getPaymentGatewayTotal().contains(Integer.toString(totalMybasketValue - agentCommission))){
										
									PrintWriter.append("<td>"+activitydetails.getPaymentGatewayTotal()+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
								}
								
								else {				
									PrintWriter.append("<td>"+activitydetails.getPaymentGatewayTotal()+"</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
								}
								
							
							}else{
								
								testCaseCount ++;
								PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Amount being Processed Now - Payment Page Billing Info</td>");
								PrintWriter.append("<td>"+totalMybasketValue+"</td>");
											
								if(activitydetails.getPaymentBilling_AmountNow().contains(Integer.toString(totalMybasketValue))){
										
									PrintWriter.append("<td>"+activitydetails.getPaymentBilling_AmountNow()+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
								}
								
								else {				
									PrintWriter.append("<td>"+activitydetails.getPaymentBilling_AmountNow()+"</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
								}
								
								testCaseCount ++;
								PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Package Booking Value - Payment Page Billing Info</td>");
								PrintWriter.append("<td>"+(totalMybasketValue)+"</td>");
											
								if(activitydetails.getPaymentBilling_CardTotal().contains(Integer.toString((totalMybasketValue)))){
										
									PrintWriter.append("<td>"+activitydetails.getPaymentBilling_CardTotal()+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
								}
								
								else {				
									PrintWriter.append("<td>"+activitydetails.getPaymentBilling_CardTotal()+"</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
								}
								
								testCaseCount ++;
								PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Value - Payment Gateway</td>");
								PrintWriter.append("<td>"+totalMybasketValue+"</td>");
											
								if(activitydetails.getPaymentGatewayTotal().contains(Integer.toString(totalMybasketValue))){
										
									PrintWriter.append("<td>"+activitydetails.getPaymentGatewayTotal()+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
								}
								
								else {				
									PrintWriter.append("<td>"+activitydetails.getPaymentGatewayTotal()+"</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
								}
								
							}
							
						}
						
						
						
						
						
					
					}else{
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Currency - Payment Page Billing Info</td>");
						PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
									
						if(activitydetails.getPaymentBilling_CardCurrency().replaceAll(" ", "").contains(search.getSellingCurrency())){
								
							PrintWriter.append("<td>"+activitydetails.getPaymentBilling_CardCurrency()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+activitydetails.getPaymentBilling_CardCurrency()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Package Booking Value - Payment Page Billing Info</td>");
						PrintWriter.append("<td>"+totalMybasketValue+"</td>");
									
						if(activitydetails.getPaymentBilling_CardTotal().contains(Integer.toString(totalMybasketValue))){
								
							PrintWriter.append("<td>"+activitydetails.getPaymentBilling_CardTotal()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {				
							PrintWriter.append("<td>"+activitydetails.getPaymentBilling_CardTotal()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Sub Total - Payment Page Billing Info</td>");
						PrintWriter.append("<td>"+subTotalAllProducts+"</td>");
									
						if(activitydetails.getPaymentBilling_subTotal().contains(Integer.toString(subTotalAllProducts))){
								
							PrintWriter.append("<td>"+activitydetails.getPaymentBilling_subTotal()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {				
							PrintWriter.append("<td>"+activitydetails.getPaymentBilling_subTotal()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Taxes and Other Charges - Payment Page Billing Info</td>");
						PrintWriter.append("<td>"+totalTaxAllProducts+"</td>");
									
						if(activitydetails.getPaymentBilling_TotalTax().contains(Integer.toString(totalTaxAllProducts))){
								
							PrintWriter.append("<td>"+activitydetails.getPaymentBilling_TotalTax()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {				
							PrintWriter.append("<td>"+activitydetails.getPaymentBilling_TotalTax()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Value - Payment Page Billing Info</td>");
						PrintWriter.append("<td>"+totalMybasketValue+"</td>");
									
						if(activitydetails.getPaymentBilling_TotalValue().contains(Integer.toString(totalMybasketValue))){
								
							PrintWriter.append("<td>"+activitydetails.getPaymentBilling_TotalValue()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {				
							PrintWriter.append("<td>"+activitydetails.getPaymentBilling_TotalValue()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Amount being Processed Now - Payment Page Billing Info</td>");
						PrintWriter.append("<td>"+totalMybasketValue+"</td>");
									
						if(activitydetails.getPaymentBilling_AmountNow().contains(Integer.toString(totalMybasketValue))){
								
							PrintWriter.append("<td>"+activitydetails.getPaymentBilling_AmountNow()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {				
							PrintWriter.append("<td>"+activitydetails.getPaymentBilling_AmountNow()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Value - Payment Gateway</td>");
						PrintWriter.append("<td>"+totalMybasketValue+"</td>");
									
						if(activitydetails.getPaymentGatewayTotal().contains(Integer.toString(totalMybasketValue))){
								
							PrintWriter.append("<td>"+activitydetails.getPaymentGatewayTotal()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {				
							PrintWriter.append("<td>"+activitydetails.getPaymentGatewayTotal()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
					}
					
					
				}
				
				
				
				if (search.getQuotationReq().equalsIgnoreCase("Yes")) {
					
					getQuotationConfirmationPageDetails();					
					getQuotationMail();
					
				} else {
				
					//// Confirmation Page
				
					PrintWriter.append("<tr><td class='fontiiii'>Confirmation Page</td><tr>"); 	
					
					String refernceName = activitydetails.getPaymentPage_Title() + activitydetails.getPaymentPage_FName() + activitydetails.getPaymentPage_LName();
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Booking Referance - Confirmation Page </td>");
					PrintWriter.append("<td>"+refernceName+"</td>");
					
					if(refernceName.equalsIgnoreCase(activitydetails.getConfirmation_BookingRefference().replaceAll(" ", ""))){
						
						PrintWriter.append("<td>"+activitydetails.getConfirmation_BookingRefference()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {
						
						PrintWriter.append("<td>"+activitydetails.getConfirmation_BookingRefference()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer Email Address - Confirmation Page </td>");
					PrintWriter.append("<td>"+activitydetails.getPaymentPage_Email()+"</td>");
					
					if(activitydetails.getPaymentPage_Email().equalsIgnoreCase(activitydetails.getConfirmation_CusMailAddress())){
						
						PrintWriter.append("<td>"+activitydetails.getConfirmation_CusMailAddress()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {
						
						PrintWriter.append("<td>"+activitydetails.getConfirmation_CusMailAddress()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Name - Confirmation Page</td>");
					PrintWriter.append("<td>"+search.getActivityName()+"</td>");
								
					if(search.getActivityName().replaceAll(" ", "").equalsIgnoreCase(activitydetails.getConfirmation_BI_ActivityName().replaceAll(" ", ""))){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmation_BI_ActivityName()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {
						
						PrintWriter.append("<td>"+activitydetails.getConfirmation_BI_ActivityName()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Program City - Confirmation Page</td>");
					PrintWriter.append("<td>"+search.getDestination()+"</td>");
								
					if(search.getDestination().equalsIgnoreCase(activitydetails.getConfirmation_BI_City())){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmation_BI_City()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {
						
						PrintWriter.append("<td>"+activitydetails.getConfirmation_BI_City()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Usable Date - Confirmation Page</td>");
					PrintWriter.append("<td>"+search.getActivityDate()+"</td>");
								
					if(search.getActivityDate().equalsIgnoreCase(activitydetails.getConfirmation_BI_SelectedDate())){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmation_BI_SelectedDate()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {
						
						PrintWriter.append("<td>"+activitydetails.getConfirmation_BI_SelectedDate()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					
					//////////
					
					for (int i = 0; i < activitydetails.getResultsPage_RateType().size(); i++) {
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Rate type - "+(i+1)+" - Confirmation Page</td>");
						PrintWriter.append("<td>"+activitydetails.getResultsPage_RateType().get(i)+"</td>");
									
						if(activitydetails.getResultsPage_RateType().get(i).equalsIgnoreCase(activitydetails.getConfirmation_RateType().get(i))){
								
							PrintWriter.append("<td>"+activitydetails.getConfirmation_RateType().get(i)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+activitydetails.getConfirmation_RateType().get(i)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Rate - "+(i+1)+" - Confirmation Page</td>");
						PrintWriter.append("<td>"+activitydetails.getResultsPage_DailyRate().get(i)+"</td>");
									
						if(activitydetails.getResultsPage_DailyRate().get(i).equalsIgnoreCase(activitydetails.getConfirmation_DailyRate().get(i))){
								
							PrintWriter.append("<td>"+activitydetails.getConfirmation_DailyRate().get(i)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+activitydetails.getConfirmation_DailyRate().get(i)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity QTY - "+(i+1)+" - Confirmation Page</td>");
						PrintWriter.append("<td>"+Integer.toString(Integer.parseInt(search.getAdults()) + Integer.parseInt(search.getChildren()))+"</td>");
									
						if(Integer.toString(Integer.parseInt(search.getAdults()) + Integer.parseInt(search.getChildren())).equals(activitydetails.getConfirmation_QTY().get(i))){
								
							PrintWriter.append("<td>"+activitydetails.getConfirmation_QTY().get(i)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+activitydetails.getConfirmation_QTY().get(i)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
					}
					
					
					///////////
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Currency-1 - Confirmation Page</td>");
					PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
								
					if(search.getSellingCurrency().equalsIgnoreCase(activitydetails.getConfirmation_Currency1())){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmation_Currency1()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {
						
						PrintWriter.append("<td>"+activitydetails.getConfirmation_Currency1()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Currency-2 - Confirmation Page</td>");
					PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
								
					if(search.getSellingCurrency().equalsIgnoreCase(activitydetails.getConfirmation_Currency2())){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmation_Currency2()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {
						
						PrintWriter.append("<td>"+activitydetails.getConfirmation_Currency2()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					
					///////////
					
					
					if (UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCASH || UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCREDITLPONO || UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCREDITLPOY) {
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>TO Agent Commission - Confirmation Page</td>");
						PrintWriter.append("<td>"+toAgentCommission+"</td>");
									
						if(Integer.toString(toAgentCommission).equals(activitydetails.getConfirmationPage_AgentCommission())){
								
							PrintWriter.append("<td>"+activitydetails.getConfirmationPage_AgentCommission()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+activitydetails.getConfirmationPage_AgentCommission()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
					
					}
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Sub Total - Confirmation Page</td>");
					PrintWriter.append("<td>"+PaymentPage_SubTotal+"</td>");
								
					if(Integer.toString(PaymentPage_SubTotal).equals(activitydetails.getConfirmation_SubTotal2())){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmation_SubTotal2()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getConfirmation_SubTotal2()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Tax and Other Charges - Confirmation Page</td>");
					PrintWriter.append("<td>"+PaymentPage_TotalTax+"</td>");
								
					if(Integer.toString(PaymentPage_TotalTax).equals(activitydetails.getConfirmation_TotalTaxOtherCharges())){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmation_TotalTaxOtherCharges()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getConfirmation_TotalTaxOtherCharges()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					
					if (discountYesNo.equalsIgnoreCase("Yes")) {
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Discount - Confirmation Page</td>");
						PrintWriter.append("<td>"+discountedVale+"</td>");
									
						if(activitydetails.getConfirmationPage_DiscountValue1().contains(Integer.toString((discountedVale)))){
								
							PrintWriter.append("<td>"+activitydetails.getConfirmationPage_DiscountValue1()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+activitydetails.getConfirmationPage_DiscountValue1()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						
						
					}
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Activity Booking Value - Confirmation Page</td>");
					PrintWriter.append("<td>"+PaymentPage_TotalPackageValue+"</td>");
								
					if(Integer.toString(PaymentPage_TotalPackageValue).equals(activitydetails.getConfirmation_Total2())){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmation_Total2()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getConfirmation_Total2()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					///
					
					
					
					if (paymentType.equals("Pay Online")) {
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Amount being Processed Now - Confirmation Page</td>");
						PrintWriter.append("<td>"+PaymentPage_AmountProcessed+"</td>");
									
						if(Integer.toString(PaymentPage_AmountProcessed).equals(activitydetails.getShoppingCart_AmountNow())){
								
							PrintWriter.append("<td>"+activitydetails.getShoppingCart_AmountNow()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+activitydetails.getShoppingCart_AmountNow()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
					}else{	
						
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Amount being Processed Now - Payment Page Shopping Basket</td>");
						PrintWriter.append("<td>"+(PaymentPage_AmountProcessed+toAgentCommission)+"</td>");
									
						if(Integer.toString(PaymentPage_AmountProcessed+toAgentCommission).equals(activitydetails.getShoppingCart_AmountNow())){
								
							PrintWriter.append("<td>"+activitydetails.getShoppingCart_AmountNow()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+activitydetails.getShoppingCart_AmountNow()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
					}
					
					
					
					
					
					
					
					
					
					
					
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Amount due at Check-In - Confirmation Page</td>");
					PrintWriter.append("<td>"+PaymentPage_AmountCheckIn+"</td>");
								
					if(Integer.toString(PaymentPage_AmountCheckIn).equals(activitydetails.getConfirmation_AmountDueCheckIn())){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmation_AmountDueCheckIn()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getConfirmation_AmountDueCheckIn()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					///////////////////////////////////////
					
					
					if (inventoryDetails.toLowerCase().contains("request")) {
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Booking Status - Confirmation Page </td>");
						PrintWriter.append("<td>"+inventoryDetails+"</td>");
						
						if(activitydetails.getConfirmation_BI_BookingStatus().split("-")[1].equalsIgnoreCase(inventoryDetails.split(" ")[1])){
							
							PrintWriter.append("<td>"+activitydetails.getConfirmation_BI_BookingStatus()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+activitydetails.getConfirmation_BI_BookingStatus()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
					} else {
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Booking Status - Confirmation Page </td>");
						PrintWriter.append("<td>Confirmed</td>");
						
						if(activitydetails.getConfirmation_BI_BookingStatus().equalsIgnoreCase("Confirmed")){
							
							PrintWriter.append("<td>"+activitydetails.getConfirmation_BI_BookingStatus()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+activitydetails.getConfirmation_BI_BookingStatus()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
					}
					
				
					/////
					
					testCaseCount ++;			
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [First Name] - Confirmation Page</td>");
					PrintWriter.append("<td>"+activitydetails.getPaymentPage_FName()+"</td>");
								
					if(activitydetails.getPaymentPage_FName().equals(activitydetails.getConfirmation_FName())){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmation_FName()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getConfirmation_FName()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCount ++;			
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [Last Name] - Confirmation Page</td>");
					PrintWriter.append("<td>"+activitydetails.getPaymentPage_LName()+"</td>");
								
					if(activitydetails.getPaymentPage_LName().equals(activitydetails.getConfirmation_LName())){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmation_LName()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getConfirmation_LName()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCount ++;			
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [TP No] - Confirmation Page</td>");
					PrintWriter.append("<td>"+activitydetails.getPaymentPage_TP()+"</td>");
								
					if(activitydetails.getPaymentPage_TP().equals(activitydetails.getConfirmation_TP())){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmation_TP()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getConfirmation_TP()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCount ++;			
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [Email] - Confirmation Page</td>");
					PrintWriter.append("<td>"+activitydetails.getPaymentPage_Email()+"</td>");
								
					if(activitydetails.getPaymentPage_Email().equals(activitydetails.getConfirmation_Email())){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmation_Email()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getConfirmation_Email()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCount ++;			
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [Address] - Confirmation Page</td>");
					PrintWriter.append("<td>"+activitydetails.getPaymentPage_address()+"</td>");
								
					if(activitydetails.getPaymentPage_address().equals(activitydetails.getConfirmation_address())){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmation_address()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getConfirmation_address()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCount ++;			
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [Country] - Confirmation Page</td>");
					PrintWriter.append("<td>"+activitydetails.getPaymentPage_country()+"</td>");
								
					if(activitydetails.getPaymentPage_country().equals(activitydetails.getConfirmation_country())){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmation_country()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getConfirmation_country()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCount ++;			
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [City] - Confirmation Page</td>");
					PrintWriter.append("<td>"+activitydetails.getPaymentPage_city()+"</td>");
								
					if(activitydetails.getPaymentPage_city().equals(activitydetails.getConfirmation_city())){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmation_city()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getConfirmation_city()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					if (activitydetails.getPaymentPage_country().equals("USA")) {
						
						testCaseCount ++;			
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [State] - Confirmation Page</td>");
						PrintWriter.append("<td>"+activitydetails.getPaymentPage_State()+"</td>");
									
						if(activitydetails.getPaymentPage_State().equals(activitydetails.getConfirmation_State())){
								
							PrintWriter.append("<td>"+activitydetails.getConfirmation_State()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+activitydetails.getConfirmation_State()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCount ++;			
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [Postal Code] - Confirmation Page</td>");
						PrintWriter.append("<td>"+activitydetails.getPaymentPage_PostalCode()+"</td>");
									
						if(activitydetails.getPaymentPage_PostalCode().equals(activitydetails.getConfirmation_postalCode())){
								
							PrintWriter.append("<td>"+activitydetails.getConfirmation_postalCode()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+activitydetails.getConfirmation_postalCode()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
					
					}
					
					if (activitydetails.getPaymentPage_country().equals("Canada")) {
						
						testCaseCount ++;			
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [State] - Confirmation Page</td>");
						PrintWriter.append("<td>"+activitydetails.getPaymentPage_State()+"</td>");
									
						if(activitydetails.getPaymentPage_State().equals(activitydetails.getConfirmation_State())){
								
							PrintWriter.append("<td>"+activitydetails.getConfirmation_State()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+activitydetails.getConfirmation_State()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCount ++;			
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [Postal Code] - Confirmation Page</td>");
						PrintWriter.append("<td>"+activitydetails.getPaymentPage_PostalCode()+"</td>");
									
						if(activitydetails.getPaymentPage_PostalCode().equals(activitydetails.getConfirmation_postalCode())){
								
							PrintWriter.append("<td>"+activitydetails.getConfirmation_postalCode()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+activitydetails.getConfirmation_postalCode()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
					}
					
					if (activitydetails.getPaymentPage_country().equals("Australia")) {
						
						testCaseCount ++;			
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [State] - Confirmation Page</td>");
						PrintWriter.append("<td>"+activitydetails.getPaymentPage_State()+"</td>");
									
						if(activitydetails.getPaymentPage_State().equals(activitydetails.getConfirmation_State())){
								
							PrintWriter.append("<td>"+activitydetails.getConfirmation_State()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+activitydetails.getConfirmation_State()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCount ++;			
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [Postal Code] - Confirmation Page</td>");
						PrintWriter.append("<td>"+activitydetails.getPaymentPage_PostalCode()+"</td>");
									
						if(activitydetails.getPaymentPage_PostalCode().equals(activitydetails.getConfirmation_postalCode())){
								
							PrintWriter.append("<td>"+activitydetails.getConfirmation_postalCode()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+activitydetails.getConfirmation_postalCode()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
					
					}
					
					//Activity Occupancy Details
					
					for (int i = 0; i < activitydetails.getResultsPage_cusTitle().size(); i++) {
					
						String paymentPageCusDetials = activitydetails.getResultsPage_cusTitle().get(i) + activitydetails.getResultsPage_cusFName().get(i) + activitydetails.getResultsPage_cusLName().get(i);
						String ConfirmationPageCusDetials = activitydetails.getConfirmationPage_cusTitle().get(i) + activitydetails.getConfirmationPage_cusFName().get(i) + activitydetails.getConfirmationPage_cusLName().get(i);
						
						testCaseCount ++;			
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer Occupancy details - Customer["+(i+1)+"] - Confirmation Page</td>");
						PrintWriter.append("<td>"+paymentPageCusDetials+"</td>");
									
						if(paymentPageCusDetials.equalsIgnoreCase(ConfirmationPageCusDetials)){
								
							PrintWriter.append("<td>"+ConfirmationPageCusDetials+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+ConfirmationPageCusDetials+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
					
					}
					
					////
					
					int bufferDates = Integer.parseInt(applyIfLessThan) + Integer.parseInt(cancelBuffer);
					
					String DateTo = search.getDateTo();
					String activitySelectDate = search.getActivityDate();
					DateFormat dfTo = new SimpleDateFormat("dd-MMM-yyyy");
					SimpleDateFormat formatInTo = new SimpleDateFormat("MM/dd/yyyy");
					Date instanceTo = formatInTo.parse(DateTo);  		
					String reportDateTo = dfTo.format(instanceTo);  //DateTo   
					
					String DateFrom = search.getDateFrom();
					Date instance = formatInTo.parse(DateFrom);  		
					String reportDateFrom = dfTo.format(instance);  //Datefrom
					
					Date instanceMainPol = dfTo.parse(activitySelectDate); 
					Calendar calMainPolicy = Calendar.getInstance();
					calMainPolicy.setTime(instanceMainPol);
					calMainPolicy.add(Calendar.DATE, -1);			
					String dateWithBufferDates_1 = dfTo.format(calMainPolicy.getTime());  // DateTo - 1
					
					Date instanceMainPolicy3 = dfTo.parse(currentDateforMatch);
					Calendar calMainPolicyto = Calendar.getInstance();
					calMainPolicyto.setTime(instanceMainPolicy3);
					calMainPolicyto.add(Calendar.DATE, +1);
					String currentDateforMatch_1 = dfTo.format(calMainPolicyto.getTime());   //Datefrom - 1
					
					Date instanceMainPolicy5 = dfTo.parse(activitySelectDate);
					Calendar policy1 = Calendar.getInstance();
					policy1.setTime(instanceMainPolicy5);
					policy1.add(Calendar.DATE, -(bufferDates+1));
					String dateWithBufferDates_3 = dfTo.format(policy1.getTime());   //Datefrom - [Buffer]
					
					Date instanceMainPolicy6 = dfTo.parse(dateWithBufferDates_3);
					Calendar policy12 = Calendar.getInstance();
					policy12.setTime(instanceMainPolicy6);
					policy12.add(Calendar.DATE, +1);
					String dateWithBufferDates_66 = dfTo.format(policy12.getTime());
					
					Date instanceMainPolicy4 = dfTo.parse(activitySelectDate);
					Calendar calActivityDate = Calendar.getInstance();
					calActivityDate.setTime(instanceMainPolicy4);
					calActivityDate.add(Calendar.DATE, -1);
					String dateWithSelected = dfTo.format(calActivityDate.getTime()); //Activity - 1
					
					String StandardCancellation_based = standardCancell_Type;
					String Noshow_based = noShow_Type;				
					double StandardCancellation_value = 0;				
					double Noshow_value = 0 ;
					
					if (standardCancell_Type.equalsIgnoreCase("Percentage")) {
						StandardCancellation_value = (Double.parseDouble(standardCancell_Value));
					}
					
					if (standardCancell_Type.equalsIgnoreCase("value")) {
						StandardCancellation_value = Math.ceil(Double.parseDouble(standardCancell_Value) + (Double.parseDouble(standardCancell_Value) * ((double)pMarkup/100)));
					}
					
					if (noShow_Type.equalsIgnoreCase("Percentage")) {
						Noshow_value = (Double.parseDouble(noShow_Value));
					}
					
					if (noShow_Type.equalsIgnoreCase("value")) {
						Noshow_value = Math.ceil(Double.parseDouble(noShow_Value) + (Double.parseDouble(noShow_Value) * ((double)pMarkup/100)));
					}
					
					////  Cancel policy 1
					
					PrintWriter.append("<tr><td class='fontiiii'>Cancellation Policies</td><tr>"); 
					
					if (activitydetails.getCancelPolicy().size() == 2) {
						
						if (activitydetails.getCancelPolicy().get(0).contains(",")) {
							
							activityCanPolicy1 = activitydetails.getCancelPolicy().get(0).replace(",", "");
						}
						
						else {
							activityCanPolicy1 = activitydetails.getCancelPolicy().get(0);
						}
						
						
						if (StandardCancellation_based.contains("percentage")) {
							
							if (!(activityCanPolicy1.split(" ")[4].equals(currentDateforMatch))) {
								
								resPolist1 = "If you cancel between "+currentDateforMatch_1+" and "+dateWithBufferDates_1+" you will be charged "+StandardCancellation_value+" % of the purchase price.";
								
							}else {
								
								resPolist1 = "If you cancel between "+currentDateforMatch+" and "+dateWithBufferDates_1+" you will be charged "+StandardCancellation_value+" % of the purchase price.";						
							}
							
								
						} else {
		
							if (!(activityCanPolicy1.split(" ")[4].equals(currentDateforMatch))) {
								
								resPolist1 = "If you cancel between "+currentDateforMatch_1+" and "+dateWithBufferDates_1+" you will be charged "+search.getSellingCurrency()+" "+StandardCancellation_value+" .";
								
							}else {
								
								resPolist1 = "If you cancel between "+currentDateforMatch+" and "+dateWithBufferDates_1+" you will be charged "+search.getSellingCurrency()+" "+StandardCancellation_value+" .";
							}
							
						}
												
	//					testCaseCount++;
	//					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity cancellation policy - 1</td>");
	//					PrintWriter.append("<td>"+resPolist1+"</td>");
	//					
	//					if(activityCanPolicy1.toLowerCase().equals(resPolist1.toLowerCase())){
	//						
	//						PrintWriter.append("<td>"+activityCanPolicy1+"</td>");
	//						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
	//					}
	//					
	//					else{
	//						PrintWriter.append("<td>"+activityCanPolicy1+"</td>");
	//						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
	//					}				
										
					}
					
					////
					
					if (activitydetails.getCancelPolicy().size() == 3) {
						
						if (activitydetails.getCancelPolicy().get(0).contains(",")) {
							
							activityMainPolicy = activitydetails.getCancelPolicy().get(0).replace(",", "");
						}
						
						else {
							activityMainPolicy = activitydetails.getCancelPolicy().get(0);
						}
						
						
						policyListOne = "If you cancel between "+currentDateforMatch+" and "+dateWithBufferDates_3+" you will be refunded your purchase price.";
						
	//					testCaseCount++;
	//					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Refundable policy</td>");
	//					PrintWriter.append("<td>"+policyListOne+"</td>");
	//					
	//					if(policyListOne.toLowerCase().equals(activityMainPolicy.toLowerCase())){
	//						
	//						PrintWriter.append("<td>"+activityMainPolicy+"</td>");
	//						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
	//					}
	//					
	//					else{
	//						PrintWriter.append("<td>"+activityMainPolicy+"</td>");
	//						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
	//					}
										
						
						
						if (activitydetails.getCancelPolicy().get(1).contains(",")) {
							
							activityCanPolicy2 = activitydetails.getCancelPolicy().get(1).replace(",", "");
						}
						
						else {
							activityCanPolicy2 = activitydetails.getCancelPolicy().get(1);
						}
						
						
						if (StandardCancellation_based.contains("percentage")) {
							
							policyListTwo = "If you cancel between "+dateWithBufferDates_66+" and "+dateWithSelected+" you will be charged "+StandardCancellation_value+" % of the purchase price.";	
						
						} else {
		
							policyListTwo = "If you cancel between "+dateWithBufferDates_66+" and "+dateWithSelected+" you will be charged "+search.getSellingCurrency()+" "+StandardCancellation_value+" .";
							
						}
									
	//					testCaseCount++;
	//					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity cancellation policy - 1</td>");
	//					PrintWriter.append("<td>"+policyListTwo+"</td>");
	//					
	//					if(policyListTwo.toLowerCase().equals(activityCanPolicy2.toLowerCase())){
	//						
	//						PrintWriter.append("<td>"+activityCanPolicy2+"</td>");
	//						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
	//					}
	//					
	//					else{
	//						PrintWriter.append("<td>"+activityCanPolicy2+"</td>");
	//						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
	//					}		
						
					}
					
					
					//No Show Applying policy
					
					if (activitydetails.getCancelPolicy().size() == 2) {
						noShowMatching = activitydetails.getCancelPolicy().get(1);
					}
					
					if (activitydetails.getCancelPolicy().size() == 3) {
						noShowMatching = activitydetails.getCancelPolicy().get(2);
					}
					
					if (activitydetails.getCancelPolicy().size() == 4) {
						noShowMatching 	= activitydetails.getCancelPolicy().get(3);
					}
					
												
					double noShowValue = totalPayableforNoshow * (Double.parseDouble(noShow_Value) / 100);
					double roundUpValueNoShow = Math.ceil(noShowValue);
					
					if (Noshow_based.contains("percentage")) {
						
						noShowActual	= "If cancelled on or after the "+search.getActivityDate()+" No Show Fee "+search.getSellingCurrency()+" "+roundUpValueNoShow+" applies.";
						
					} else {
		
						noShowActual	= "If cancelled on or after the "+search.getActivityDate()+" No Show Fee "+search.getSellingCurrency()+" "+Noshow_value+" applies.";
						
					}
					
		
	//				testCaseCount++;
	//				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity No-Show Fee policy</td>");
	//				PrintWriter.append("<td>"+noShowActual+"</td>");
	//				
	//				if(noShowMatching.toLowerCase().equals(noShowActual.toLowerCase())){
	//					
	//					PrintWriter.append("<td>"+noShowMatching+"</td>");
	//					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
	//				}
	//				
	//				else{
	//					PrintWriter.append("<td>"+noShowMatching+"</td>");
	//					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
	//				}
					
				////////
					
					if (activitydetails.getPaymentCancelPolicy().size() == 2) {
					
						if (activitydetails.getPaymentCancelPolicy().get(0).contains(",")) {
							
							paymentCanPolicy1 = activitydetails.getPaymentCancelPolicy().get(0).replace(",", "");
						}
						
						else {
							paymentCanPolicy1 = activitydetails.getPaymentCancelPolicy().get(0);
						}
							
						
						testCaseCount++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity cancellation policy - 1 [Payment Page]</td>");
						PrintWriter.append("<td>"+resPolist1+"</td>");
						
						if(paymentCanPolicy1.toLowerCase().replaceAll(" ", "").equals(resPolist1.toLowerCase().replaceAll(" ", ""))){
							
							PrintWriter.append("<td>"+paymentCanPolicy1+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+paymentCanPolicy1+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}				
										
					}
					
					if (activitydetails.getPaymentCancelPolicy().size() == 3) {
						
						if (activitydetails.getPaymentCancelPolicy().get(0).contains(",")) {
							
							paymentMainPolicy = activitydetails.getPaymentCancelPolicy().get(0).replace(",", "");
						}
						
						else {
							paymentMainPolicy = activitydetails.getPaymentCancelPolicy().get(0);
						}
						
						
						testCaseCount++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Refundable policy [Payment Page]</td>");
						PrintWriter.append("<td>"+policyListOne+"</td>");
						
						if(policyListOne.toLowerCase().equals(paymentMainPolicy.toLowerCase())){
							
							PrintWriter.append("<td>"+paymentMainPolicy+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+paymentMainPolicy+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
										
						
						
						if (activitydetails.getPaymentCancelPolicy().get(1).contains(",")) {
							
							paymentCanPolicy2 = activitydetails.getPaymentCancelPolicy().get(1).replace(",", "");
						}
						
						else {
							paymentCanPolicy2 = activitydetails.getPaymentCancelPolicy().get(1);
						}
						
									
						testCaseCount++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity cancellation policy - 1 [Payment Page]</td>");
						PrintWriter.append("<td>"+policyListTwo+"</td>");
						
						if(policyListTwo.toLowerCase().equals(paymentCanPolicy2.toLowerCase())){
							
							PrintWriter.append("<td>"+paymentCanPolicy2+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+paymentCanPolicy2+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}		
						
					}
					
					
					if (activitydetails.getPaymentCancelPolicy().size() == 2) {
						paymentNoShowMatching = activitydetails.getPaymentCancelPolicy().get(1);
					}
					
					if (activitydetails.getPaymentCancelPolicy().size() == 3) {
						paymentNoShowMatching = activitydetails.getPaymentCancelPolicy().get(2);
					}
					
					if (activitydetails.getPaymentCancelPolicy().size() == 4) {
						paymentNoShowMatching 	= activitydetails.getPaymentCancelPolicy().get(3);
					}
					
					testCaseCount++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity No-Show Fee policy [Payment Page]</td>");
					PrintWriter.append("<td>"+noShowActual+"</td>");
					
					if(paymentNoShowMatching.toLowerCase().equals(noShowActual.toLowerCase())){
						
						PrintWriter.append("<td>"+paymentNoShowMatching+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+paymentNoShowMatching+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
				
				///////
					
					if (activitydetails.getConfirmCancelPolicy().size() == 2) {
					
						if (activitydetails.getConfirmCancelPolicy().get(0).contains(",")) {
							
							confirmCanPolicy1 = activitydetails.getConfirmCancelPolicy().get(0).replace(",", "");
						}
						
						else {
							confirmCanPolicy1 = activitydetails.getConfirmCancelPolicy().get(0);
						}
											
						testCaseCount++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity cancellation policy - 1 [Confirmation Page]</td>");
						PrintWriter.append("<td>"+resPolist1+"</td>");
						
						if(confirmCanPolicy1.toLowerCase().replaceAll(" ", "").equals(resPolist1.toLowerCase().replaceAll(" ", ""))){
							
							PrintWriter.append("<td>"+confirmCanPolicy1+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+confirmCanPolicy1+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}				
										
					}
					
					if (activitydetails.getConfirmCancelPolicy().size() == 3) {
						
						if (activitydetails.getConfirmCancelPolicy().get(0).contains(",")) {
							
							confirmMainPolicy = activitydetails.getConfirmCancelPolicy().get(0).replace(",", "");
						}
						
						else {
							confirmMainPolicy = activitydetails.getConfirmCancelPolicy().get(0);
						}
						
						
						testCaseCount++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Refundable policy [Confirmation Page]</td>");
						PrintWriter.append("<td>"+policyListOne+"</td>");
						
						if(policyListOne.toLowerCase().equals(confirmMainPolicy.toLowerCase())){
							
							PrintWriter.append("<td>"+confirmMainPolicy+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+confirmMainPolicy+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
										
						
						
						if (activitydetails.getConfirmCancelPolicy().get(1).contains(",")) {
							
							confirmCanPolicy2 = activitydetails.getConfirmCancelPolicy().get(1).replace(",", "");
						}
						
						else {
							confirmCanPolicy2 = activitydetails.getConfirmCancelPolicy().get(1);
						}
						
									
						testCaseCount++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity cancellation policy - 1 [Confirmation Page]</td>");
						PrintWriter.append("<td>"+policyListTwo+"</td>");
						
						if(policyListTwo.toLowerCase().equals(confirmCanPolicy2.toLowerCase())){
							
							PrintWriter.append("<td>"+confirmCanPolicy2+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+confirmCanPolicy2+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}		
						
					}
					
					
					if (activitydetails.getConfirmCancelPolicy().size() == 2) {
						confirmNoShowMatching = activitydetails.getConfirmCancelPolicy().get(1);
					}
					
					if (activitydetails.getConfirmCancelPolicy().size() == 3) {
						confirmNoShowMatching = activitydetails.getConfirmCancelPolicy().get(2);
					}
					
					if (activitydetails.getConfirmCancelPolicy().size() == 4) {
						confirmNoShowMatching 	= activitydetails.getConfirmCancelPolicy().get(3);
					}
					
					testCaseCount++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity No-Show Fee policy [Confirmation Page]</td>");
					PrintWriter.append("<td>"+noShowActual+"</td>");
					
					if(confirmNoShowMatching.toLowerCase().equals(noShowActual.toLowerCase())){
						
						PrintWriter.append("<td>"+confirmNoShowMatching+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+confirmNoShowMatching+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					
					
					
					
					PrintWriter.append("</table>");
					
					
			
				
				
					
					///////////////////////////////////////////////////////////////////////////////////////////////////////
					//Booking list report	
					PrintWriter.append("<br><br>");
					PrintWriter.append("<p class='fontStyles'>Booking List Report Summary</p>");
					
					testCaseCountBookingList = 1;
					PrintWriter.append("<br><table ><tr><th>Test Case No</th><th>Test Description</th><th>Expect Values - [Excel Value]</th><th>Actual Values</th><th>Test Status</th></tr>");
					
					if (bookingList.isBookingListReportLoaded() == true) {
					
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking Card - Reservation No</td>");
						PrintWriter.append("<td>"+activitydetails.getReservationNo()+"</td>");
						
						if(activitydetails.getReservationNo().equals(bookingList.getBCard_Res_ReservationNumber())){
							
							PrintWriter.append("<td>"+bookingList.getBCard_Res_ReservationNumber()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+bookingList.getBCard_Res_ReservationNumber()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						String Reservation_Date = bookingList.getBCard_Res_Reservation_Date();
						String FirstElement_Date = bookingList.getBCard_Res_FirstElement_Date();
						String Cancel_Deadline_Date = bookingList.getBCard_Res_Cancel_Deadline();
						String bookedDate = bookingList.getBCard_Activity_DatesBookedFor();
						
						String Reservation_DateNew = bookingList.getBList_Reservation_Date();
						String FirstElement_DateNew = bookingList.getBList_DateOf_FirstElement();
						String Cancel_Deadline_DateNew = bookingList.getBList_DateOf_Cxl_Deadline();
						
						DateFormat dateFForBooking = new SimpleDateFormat("dd-MMM-yyyy");
						SimpleDateFormat formatInToBooking = new SimpleDateFormat("dd/MM/yyyy");
									
						Date instanceReservation_Date = formatInToBooking.parse(Reservation_Date);  
						Date instanceFirstElement_Date = formatInToBooking.parse(FirstElement_Date);
						Date instanceToCancel_Deadline_Date = formatInToBooking.parse(Cancel_Deadline_Date);				
						Date instanceToCancel_bookedDate = formatInToBooking.parse(bookedDate);
				
						String Reservation_Date_con = dateFForBooking.format(instanceReservation_Date);
						String FirstElement_Date_con = dateFForBooking.format(instanceFirstElement_Date);
						String Cancel_Deadline_Date_con = dateFForBooking.format(instanceToCancel_Deadline_Date);
						String bookedDate_con = dateFForBooking.format(instanceToCancel_bookedDate);
						
						///
						
						testCaseCountBookingList++;
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking Card - Reservation Date</td>");
						PrintWriter.append("<td>"+currentDateforMatch+"</td>");
						
						if(currentDateforMatch.equals(Reservation_DateNew)){
							
							PrintWriter.append("<td>"+Reservation_DateNew+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+Reservation_DateNew+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCountBookingList++;
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking Card - Date Of the First Element Used</td>");
						PrintWriter.append("<td>"+search.getActivityDate()+"</td>");
						
						if(search.getActivityDate().equals(FirstElement_DateNew)){
							
							PrintWriter.append("<td>"+FirstElement_DateNew+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+FirstElement_DateNew+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCountBookingList++;
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking Card - Cancellation Deadline</td>");
						PrintWriter.append("<td>"+dateWithBufferDates_3+"</td>");
						
						if(dateWithBufferDates_3.equals(Cancel_Deadline_DateNew)){
							
							PrintWriter.append("<td>"+Cancel_Deadline_DateNew+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+Cancel_Deadline_DateNew+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						
						if (UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCREDITLPONO || UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCREDITLPOY || UserTypeDC_B2B == WEB_TourOperatorType.WEB_NETCREDITLPON || UserTypeDC_B2B == WEB_TourOperatorType.WEB_NETCREDITLPOY) {
							
							String pt;
							if (paymentType.equalsIgnoreCase("Pay Online")) {
								pt = "Credit Card";
							}else{
								pt = "On Credit";
							}
							
							testCaseCountBookingList++;
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking Card - Payment Type</td>");
							PrintWriter.append("<td>"+pt+"</td>");
							
							if(pt.replaceAll(" ", "").equalsIgnoreCase(bookingList.getBList_PaymentType().replaceAll(" ", ""))){
								
								PrintWriter.append("<td>"+bookingList.getBList_PaymentType()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+bookingList.getBList_PaymentType()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
												
						}else{
							
							String pt;
							if (paymentType.equalsIgnoreCase("Pay Online")) {
								pt = "Credit Card";
							}else{
								pt = "On Cash";
							}
							
							if (UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCASH || UserTypeDC_B2B == WEB_TourOperatorType.WEB_NETCASH) {
								
								testCaseCountBookingList++;
								PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking Card - Payment Type</td>");
								PrintWriter.append("<td>"+pt+"</td>");
								
								if(pt.replaceAll(" ", "").equalsIgnoreCase(bookingList.getBList_PaymentType().replaceAll(" ", ""))){
									
									PrintWriter.append("<td>"+bookingList.getBList_PaymentType()+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
								}
								
								else{
									PrintWriter.append("<td>"+bookingList.getBList_PaymentType()+"</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
								}
								
							}else{
								
								testCaseCountBookingList++;
								PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking Card - Payment Type</td>");
								PrintWriter.append("<td>"+pt+"</td>");
								
								if(pt.replaceAll(" ", "").equalsIgnoreCase(bookingList.getBList_PaymentType().replaceAll(" ", ""))){
									
									PrintWriter.append("<td>"+bookingList.getBList_PaymentType()+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
								}
								
								else{
									PrintWriter.append("<td>"+bookingList.getBList_PaymentType()+"</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
								}
								
							}
							
							
						}
						
						
						
						
						if (bookingList.getBList_Status().replaceAll(" ", "").toLowerCase().contains("req")) {
							
							testCaseCountBookingList++;
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking Card - Booking status</td>");
							PrintWriter.append("<td>"+activitydetails.getConfirmation_BI_BookingStatus()+"</td>");
							
							if(activitydetails.getConfirmation_BI_BookingStatus().toLowerCase().contains(bookingList.getBList_Status().toLowerCase())){
								
								PrintWriter.append("<td>"+bookingList.getBList_Status()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+bookingList.getBList_Status()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
						}
						
						if (bookingList.getBList_Status().replaceAll(" ", "").toLowerCase().contains("con")) {
							
							testCaseCountBookingList++;
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking Card - Booking status</td>");
							PrintWriter.append("<td>"+activitydetails.getConfirmation_BI_BookingStatus()+"</td>");
							
							if(activitydetails.getConfirmation_BI_BookingStatus().toLowerCase().contains(bookingList.getBList_Status().toLowerCase().replaceAll(" ", ""))){
								
								PrintWriter.append("<td>"+bookingList.getBList_Status()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+bookingList.getBList_Status()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
						}
						
						
						
						String fullName;
						fullName = activitydetails.getPaymentPage_Title() + activitydetails.getPaymentPage_FName() + " "+activitydetails.getPaymentPage_LName()+"";
						
						testCaseCountBookingList ++;			
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking Card - Customer Name</td>");
						PrintWriter.append("<td>"+fullName+"</td>");
									
						if(fullName.replaceAll(" ", "").equalsIgnoreCase(bookingList.getBList_Customer_Name().replaceAll(" ", ""))){
								
							PrintWriter.append("<td>"+bookingList.getBList_Customer_Name()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+bookingList.getBList_Customer_Name()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						String leadName = activitydetails.getResultsPage_cusTitle().get(0) + activitydetails.getResultsPage_cusFName().get(0) + activitydetails.getResultsPage_cusLName().get(0);
						
						testCaseCountBookingList ++;			
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking Card - Lead Guest Name</td>");
						PrintWriter.append("<td>"+leadName+"</td>");
									
						if(leadName.replaceAll(" ", "").equalsIgnoreCase(bookingList.getBList_LeadGuest_Name().replaceAll(" ", ""))){
								
							PrintWriter.append("<td>"+bookingList.getBList_LeadGuest_Name()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+bookingList.getBList_LeadGuest_Name()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						
						if (paymentType.equals("Pay Online")) {
							
							testCaseCountBookingList ++;			
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking Card - Invoice Issued</td>");
							PrintWriter.append("<td>Green</td>");
										
							if(bookingList.isBList_Invoice_Issued() == true){
									
								PrintWriter.append("<td>Green</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>Not match</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
							testCaseCountBookingList ++;			
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking Card - Payments Issued</td>");
							PrintWriter.append("<td>Green</td>");
										
							if(bookingList.isBList_Payment_Receieved() == true){
									
								PrintWriter.append("<td>Green</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>Not match</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
							testCaseCountBookingList ++;			
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking Card - Voucher Issued</td>");
							PrintWriter.append("<td>Green</td>");
										
							if(bookingList.isBList_Voucher_Issued() == true){
									
								PrintWriter.append("<td>Green</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>Not match</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
												
							
						}else{
							
							if (UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCASH || UserTypeDC_B2B == WEB_TourOperatorType.WEB_NETCASH) {
								
								testCaseCountBookingList ++;			
								PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking Card - Invoice Issued</td>");
								PrintWriter.append("<td>Green</td>");
											
								if(bookingList.isBList_Invoice_Issued() == true){
										
									PrintWriter.append("<td>Green</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
								}
								
								else{
									PrintWriter.append("<td>Not match</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
								}
								
							} else {

								testCaseCountBookingList ++;			
								PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking Card - Invoice Issued</td>");
								PrintWriter.append("<td>Orange</td>");
											
								if(bookingList.isBList_Invoice_Issued() == false){
										
									PrintWriter.append("<td>Orange</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
								}
								
								else{
									PrintWriter.append("<td>Not match</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
								}
								
							}
							
							
							testCaseCountBookingList ++;			
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking Card - Payments Issued</td>");
							PrintWriter.append("<td>Orange</td>");
										
							if(bookingList.isBList_Payment_Receieved() == false){
									
								PrintWriter.append("<td>Orange</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>Not match</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
							testCaseCountBookingList ++;			
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking Card - Voucher Issued</td>");
							PrintWriter.append("<td>Orange</td>");
										
							if(bookingList.isBList_Voucher_Issued() == false){
									
								PrintWriter.append("<td>Orange</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>Not match</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
						}
						
						
								
						
						///
						
						testCaseCountBookingList++;
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Payment Method - Reservation Summary</td>");
						PrintWriter.append("<td>"+activitydetails.getPaymentType_bookingList()+"</td>");
						
						if(bookingList.getBCard_Res_PaymentMethod().toLowerCase().replaceAll(" ", "").contains(activitydetails.getPaymentType_bookingList().toLowerCase().replaceAll(" ", ""))){
							
							PrintWriter.append("<td>"+bookingList.getBCard_Res_PaymentMethod()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+bookingList.getBCard_Res_PaymentMethod()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCountBookingList++;
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Payment Reference - Reservation Summary</td>");
						PrintWriter.append("<td>"+activitydetails.getPaymentReference_bookingList()+"</td>");
						
						if(activitydetails.getPaymentReference_bookingList().equalsIgnoreCase(bookingList.getBCard_Res_PaymentReference())){
							
							PrintWriter.append("<td>"+bookingList.getBCard_Res_PaymentReference()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+bookingList.getBCard_Res_PaymentReference()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						////
						
						testCaseCountBookingList++;
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Reservation Date - Reservation Summary</td>");
						PrintWriter.append("<td>"+currentDateforMatch+"</td>");
						
						if(currentDateforMatch.equals(Reservation_Date_con)){
							
							PrintWriter.append("<td>"+Reservation_Date_con+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+Reservation_Date_con+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCountBookingList++;
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Date Of the First Element Used - Reservation Summary</td>");
						PrintWriter.append("<td>"+search.getActivityDate()+"</td>");
						
						if(search.getActivityDate().equals(FirstElement_Date_con)){
							
							PrintWriter.append("<td>"+FirstElement_Date_con+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+FirstElement_Date_con+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCountBookingList++;
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Cancellation Deadline - Reservation Summary</td>");
						PrintWriter.append("<td>"+dateWithBufferDates_3+"</td>");
						
						if(dateWithBufferDates_3.equals(Cancel_Deadline_Date_con)){
							
							PrintWriter.append("<td>"+Cancel_Deadline_Date_con+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+Cancel_Deadline_Date_con+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						if (bookingList.getBCard_Res_Booking_Status().toLowerCase().contains("req")) {
							
							testCaseCountBookingList++;
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Booking status - Reservation Summary</td>");
							PrintWriter.append("<td>"+activitydetails.getConfirmation_BI_BookingStatus()+"</td>");
							
							if(activitydetails.getConfirmation_BI_BookingStatus().toLowerCase().contains(bookingList.getBCard_Res_Booking_Status().toLowerCase())){
								
								PrintWriter.append("<td>"+bookingList.getBCard_Res_Booking_Status()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+bookingList.getBCard_Res_Booking_Status()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
						}
						
						if (bookingList.getBCard_Res_Booking_Status().toLowerCase().contains("con")) {
							
							testCaseCountBookingList++;
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Booking status - Reservation Summary</td>");
							PrintWriter.append("<td>"+activitydetails.getConfirmation_BI_BookingStatus()+"</td>");
							
							if(activitydetails.getConfirmation_BI_BookingStatus().toLowerCase().contains(bookingList.getBCard_Res_Booking_Status().toLowerCase())){
								
								PrintWriter.append("<td>"+bookingList.getBCard_Res_Booking_Status()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+bookingList.getBCard_Res_Booking_Status()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
						}
						
						
					
						testCaseCountBookingList++;
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Currency 1 - Reservation Summary</td>");
						PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
						
						if(bookingList.getBCard_Res_CurrencyType().contains(search.getSellingCurrency())){
							
							PrintWriter.append("<td>"+bookingList.getBCard_Res_CurrencyType()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+bookingList.getBCard_Res_CurrencyType()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						////
						
						
						String bookingSub = bookingList.getBCard_Res_SubTotal().replace(",", "").split("\\.")[0];
						
						testCaseCountBookingList ++;
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Sub Total - Reservation Summary</td>");
						PrintWriter.append("<td>"+(PaymentPage_SubTotal - discountedVale)+"</td>");
									
						
						if(Integer.toString(PaymentPage_SubTotal - discountedVale).equals(bookingSub)){
								
							PrintWriter.append("<td>"+bookingSub+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+bookingSub+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						String bookingTax = bookingList.getBCard_Res_Tax_Other().split("\\.")[0];
						
						testCaseCountBookingList ++;
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Tax and other charges - Reservation Summary</td>");
						PrintWriter.append("<td>"+PaymentPage_Taxes+"</td>");
								
						
						if(Integer.toString(PaymentPage_Taxes).equals(bookingTax)){
								
							PrintWriter.append("<td>"+bookingTax+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+bookingTax+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						String bookingTotalval = bookingList.getBCard_Res_Total_Booking_Fee().replace(",", "").split("\\.")[0];
						
						testCaseCountBookingList ++;
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Total Booking Value - Reservation Summary</td>");
						PrintWriter.append("<td>"+PaymentPage_TotalPackageValue+"</td>");
								
						
						if(Integer.toString(PaymentPage_TotalPackageValue).equals(bookingTotalval)){
								
							PrintWriter.append("<td>"+bookingTotalval+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+bookingTotalval+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						
						String BCard_Res_Credit_Fee = bookingList.getBCard_Res_Credit_Fee().replace(",", "").split("\\.")[0];
						
						int totalTax = PaymentPage_Taxes + creditTax ;
						int totalAmount = PaymentPage_SubTotal + totalTax - discountedVale;
						
						if (paymentType.equals("Pay Online")) {
							
							testCaseCountBookingList ++;
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Credit Card Tax - Reservation Summary</td>");
							PrintWriter.append("<td>"+creditTax+"</td>");
								
							
							if(Integer.toString(creditTax).equals(BCard_Res_Credit_Fee)){
									
								PrintWriter.append("<td>"+BCard_Res_Credit_Fee+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+BCard_Res_Credit_Fee+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
							
							String  BCard_Res_Amount_Payable_CheckIn = bookingList.getBCard_Res_Total_Booking_Fee().replace(",", "").split("\\.")[0];
							
							testCaseCountBookingList ++;
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Total Amount Payable Upfront - Reservation Summary</td>");
							PrintWriter.append("<td>"+totalAmount+"</td>");
								
							
							if(Integer.toString(totalAmount).equals(BCard_Res_Amount_Payable_CheckIn)){
									
								PrintWriter.append("<td>"+BCard_Res_Amount_Payable_CheckIn+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+BCard_Res_Amount_Payable_CheckIn+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
						}
					
						String amountDue = bookingList.getBCard_Res_Amount_Payable_CheckIn().split("\\.")[0];
						
						if (UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCASH || UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCREDITLPONO || UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCREDITLPOY) {
							
							if (paymentType.equalsIgnoreCase("Pay Online")) {
								
								String amountPaid = bookingList.getBCard_Res_AmountPaid().split("\\.")[0];
								
								testCaseCountBookingList++;
								PrintWriter.append("<tr><td>"+ testCaseCountBookingList+"</td> <td>Booking List Report - Amount Paid - Reservation Summary</td>");
								PrintWriter.append("<td>"+ (PaymentPage_AmountProcessed) + "</td>");
								
								if (Integer.toString(PaymentPage_AmountProcessed).equals(amountPaid)) {

									PrintWriter.append("<td>" + amountPaid+ "</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");
								}

								else {
									PrintWriter.append("<td>" + amountPaid+ "</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
								}
								
								
								testCaseCountBookingList++;
								PrintWriter.append("<tr><td>"+ testCaseCountBookingList+"</td> <td>Booking List Report - Amount due at Check-In - Reservation Summary</td>");
								PrintWriter.append("<td>" + toAgentCommission+ "</td>");
								
								if (Integer.toString(toAgentCommission).equals(amountDue)) {

									PrintWriter.append("<td>" + amountDue + "</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");
								}

								else {
									PrintWriter.append("<td>" + amountDue + "</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
								}
							}else{
								
								testCaseCountBookingList ++;
								PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Amount due at Check-In - Reservation Summary</td>");
								PrintWriter.append("<td>"+PaymentPage_AmountCheckIn+"</td>");
									
								if(Integer.toString(PaymentPage_AmountCheckIn).equals(amountDue)){
										
									PrintWriter.append("<td>"+amountDue+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
								}
								
								else{
									PrintWriter.append("<td>"+amountDue+"</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
								}
								
								String amountPaid = bookingList.getBCard_Res_AmountPaid().split("\\.")[0];
								String offlinePaid = "0";
								testCaseCountBookingList ++;
								PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Amount Paid - Reservation Summary</td>");
								PrintWriter.append("<td>"+offlinePaid+"</td>");
									
								if(offlinePaid.equals(amountPaid)){
										
									PrintWriter.append("<td>"+amountPaid+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
								}
								
								else{
									PrintWriter.append("<td>"+amountPaid+"</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
								}
								
								
								
							}
							
							
						}else{
							
							if (UserTypeDC_B2B == WEB_TourOperatorType.WEB_NETCASH || UserTypeDC_B2B == WEB_TourOperatorType.WEB_NETCREDITLPON || UserTypeDC_B2B == WEB_TourOperatorType.WEB_NETCREDITLPOY) {
								
								if (paymentType.equalsIgnoreCase("Pay Online")) {
									
									String amountPaid = bookingList.getBCard_Res_AmountPaid().split("\\.")[0];
									
									testCaseCountBookingList++;
									PrintWriter.append("<tr><td>"+ testCaseCountBookingList+"</td> <td>Booking List Report - Amount Paid - Reservation Summary</td>");
									PrintWriter.append("<td>"+ (PaymentPage_AmountProcessed) + "</td>");
									
									if (Integer.toString(PaymentPage_AmountProcessed).equals(amountPaid)) {

										PrintWriter.append("<td>" + amountPaid+ "</td>");
										PrintWriter.append("<td class='Passed'>PASS</td><tr>");
									}

									else {
										PrintWriter.append("<td>" + amountPaid+ "</td>");
										PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
									}
									
									String offlineAmountDue = "0";
									testCaseCountBookingList++;
									PrintWriter.append("<tr><td>"+ testCaseCountBookingList+"</td> <td>Booking List Report - Amount due at Check-In - Reservation Summary</td>");
									PrintWriter.append("<td>" + offlineAmountDue+ "</td>");
									
									if (offlineAmountDue.equals(amountDue)) {

										PrintWriter.append("<td>" + amountDue + "</td>");
										PrintWriter.append("<td class='Passed'>PASS</td><tr>");
									}

									else {
										PrintWriter.append("<td>" + amountDue + "</td>");
										PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
									}
									
									
								}else{
									
									testCaseCountBookingList ++;
									PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Amount due at Check-In - Reservation Summary</td>");
									PrintWriter.append("<td>"+PaymentPage_AmountCheckIn+"</td>");
										
									if(Integer.toString(PaymentPage_AmountCheckIn).equals(amountDue)){
											
										PrintWriter.append("<td>"+amountDue+"</td>");
										PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
									}
									
									else{
										PrintWriter.append("<td>"+amountDue+"</td>");
										PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
									}
									
									String amountPaid = bookingList.getBCard_Res_AmountPaid().split("\\.")[0];
									String offlinePaid = "0";
									testCaseCountBookingList ++;
									PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Amount Paid - Reservation Summary</td>");
									PrintWriter.append("<td>"+offlinePaid+"</td>");
										
									if(offlinePaid.equals(amountPaid)){
											
										PrintWriter.append("<td>"+amountPaid+"</td>");
										PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
									}
									
									else{
										PrintWriter.append("<td>"+amountPaid+"</td>");
										PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
									}
									
								}
								
								
								
							}else{
								
								testCaseCountBookingList ++;
								PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Amount due at Check-In - Reservation Summary</td>");
								PrintWriter.append("<td>"+PaymentPage_AmountCheckIn+"</td>");
									
								if(Integer.toString(PaymentPage_AmountCheckIn).equals(amountDue)){
										
									PrintWriter.append("<td>"+amountDue+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
								}
								
								else{
									PrintWriter.append("<td>"+amountDue+"</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
								}
								
								String amountPaid = bookingList.getBCard_Res_AmountPaid().split("\\.")[0];
								testCaseCountBookingList ++;
								PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Amount Paid - Reservation Summary</td>");
								PrintWriter.append("<td>"+totalAmount+"</td>");
									
								if(Integer.toString(totalAmount).equals(amountPaid)){
										
									PrintWriter.append("<td>"+amountPaid+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
								}
								
								else{
									PrintWriter.append("<td>"+amountPaid+"</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
								}
								
							}
							
						
						}
						
						
						
						//////
						
						String fullNamed;
						fullNamed = activitydetails.getPaymentPage_FName() + " "+activitydetails.getPaymentPage_LName()+"";
						
						
						testCaseCountBookingList ++;			
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Customer Name</td>");
						PrintWriter.append("<td>"+fullNamed+"</td>");
									
						if(fullNamed.toLowerCase().contains(bookingList.getBCard_Guest_FName().toLowerCase())){
								
							PrintWriter.append("<td>"+bookingList.getBCard_Guest_FName()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+bookingList.getBCard_Guest_FName()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						
						testCaseCountBookingList ++;			
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Customer - TP No</td>");
						PrintWriter.append("<td>"+activitydetails.getPaymentPage_TP()+"</td>");
									
						if(activitydetails.getPaymentPage_TP().equals(bookingList.getBCard_Guest_Emergency_Contact())){
								
							PrintWriter.append("<td>"+bookingList.getBCard_Guest_Emergency_Contact()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+bookingList.getBCard_Guest_Emergency_Contact()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCountBookingList ++;			
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Customer Email</td>");
						PrintWriter.append("<td>"+activitydetails.getPaymentPage_Email()+"</td>");
									
						if(activitydetails.getPaymentPage_Email().equals(bookingList.getBCard_Guest_Email())){
								
							PrintWriter.append("<td>"+bookingList.getBCard_Guest_Email()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+bookingList.getBCard_Guest_Email()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						String add = activitydetails.getPaymentPage_address();
						
						testCaseCountBookingList ++;			
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Customer Address</td>");
						PrintWriter.append("<td>"+add+"</td>");
									
						if(bookingList.getBCard_Guest_Add().toLowerCase().contains(add.toLowerCase())){
								
							PrintWriter.append("<td>"+bookingList.getBCard_Guest_Add()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+bookingList.getBCard_Guest_Add()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCountBookingList ++;			
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Customer Country</td>");
						PrintWriter.append("<td>"+activitydetails.getPaymentPage_country()+"</td>");
									
						if(activitydetails.getPaymentPage_country().equals(bookingList.getBCard_Guest_Country())){
								
							PrintWriter.append("<td>"+bookingList.getBCard_Guest_Country()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+bookingList.getBCard_Guest_Country()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCountBookingList ++;			
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Customer - City</td>");
						PrintWriter.append("<td>"+activitydetails.getPaymentPage_city()+"</td>");
									
						if(activitydetails.getPaymentPage_city().equals(bookingList.getBCard_Guest_City())){
								
							PrintWriter.append("<td>"+bookingList.getBCard_Guest_City()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+bookingList.getBCard_Guest_City()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						/////
						
						
						if (bookingList.isAgentShows() == true) {
							
							testCaseCountBookingList ++;			
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Agent Name</td>");
							PrintWriter.append("<td>"+PG_Properties.getProperty(UserTypeDC_B2B.toString()).split("/")[2]+"</td>");
										
							if(PG_Properties.getProperty(UserTypeDC_B2B.toString()).split("/")[2].replaceAll(" ", "").equalsIgnoreCase(bookingList.getBCard_Agent_AgentName().replaceAll(" ", ""))){
									
								PrintWriter.append("<td>"+bookingList.getBCard_Agent_AgentName()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+bookingList.getBCard_Agent_AgentName()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
							testCaseCountBookingList ++;			
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - User Name</td>");
							PrintWriter.append("<td>"+PG_Properties.getProperty(UserTypeDC_B2B.toString()).split("/")[0]+"</td>");
										
							if(PG_Properties.getProperty(UserTypeDC_B2B.toString()).split("/")[0].equalsIgnoreCase(bookingList.getBCard_Agent_UserName())){
									
								PrintWriter.append("<td>"+bookingList.getBCard_Agent_UserName()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+bookingList.getBCard_Agent_UserName()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
							String toFromBList = bookingList.getBCard_Agent_AgentType().split(" ")[0];
							
							if (toFromBList.equalsIgnoreCase("Cash")) {
								
								testCaseCountBookingList ++;			
								PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Agent Type</td>");
								PrintWriter.append("<td>Cash To</td>");
											
								if(PG_Properties.getProperty(UserTypeDC_B2B.toString()).split("/")[0].replaceAll(" ", "").toLowerCase().contains(toFromBList.toLowerCase())){
										
									PrintWriter.append("<td>"+PG_Properties.getProperty(UserTypeDC_B2B.toString()).split("/")[0]+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
								}
								
								else{
									PrintWriter.append("<td>"+PG_Properties.getProperty(UserTypeDC_B2B.toString()).split("/")[0]+"</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
								}
								
								
							} else {
								
								testCaseCountBookingList ++;			
								PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Agent Type</td>");
								PrintWriter.append("<td>Credit To</td>");
											
								if(PG_Properties.getProperty(UserTypeDC_B2B.toString()).split("/")[0].replaceAll(" ", "").toLowerCase().contains(toFromBList.toLowerCase())){
										
									PrintWriter.append("<td>"+PG_Properties.getProperty(UserTypeDC_B2B.toString()).split("/")[0]+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
								}
								
								else{
									PrintWriter.append("<td>"+PG_Properties.getProperty(UserTypeDC_B2B.toString()).split("/")[0]+"</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
								}

							}
							
							
							
							if (UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCASH) {
								
								String toAgentTypePerOrValue = PG_Properties.getProperty(UserTypeDC_B2B.toString() + "#Value");
								
								testCaseCountBookingList ++;			
								PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Commission Percentage</td>");
								PrintWriter.append("<td>"+toAgentTypePerOrValue+"</td>");
											
								if(bookingList.getBCard_Agent_CommissionPercentage().contains(toAgentTypePerOrValue)){
										
									PrintWriter.append("<td>"+bookingList.getBCard_Agent_CommissionPercentage()+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
								}
								
								else{
									PrintWriter.append("<td>"+bookingList.getBCard_Agent_CommissionPercentage()+"</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
								}
								
								testCaseCountBookingList ++;			
								PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Commission Amount</td>");
								PrintWriter.append("<td>"+toAgentCommission+"</td>");
											
								if(bookingList.getBCard_Agent_CommissionAmount().contains(Integer.toString(toAgentCommission))){
										
									PrintWriter.append("<td>"+bookingList.getBCard_Agent_CommissionAmount()+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
								}
								
								else{
									PrintWriter.append("<td>"+bookingList.getBCard_Agent_CommissionAmount()+"</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
								}
								

							}
							if (UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCREDITLPONO) {
								
								String toAgentTypePerOrValue = PG_Properties.getProperty(UserTypeDC_B2B.toString() + "#Value");
								
								testCaseCountBookingList ++;			
								PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Commission Percentage</td>");
								PrintWriter.append("<td>"+toAgentTypePerOrValue+"</td>");
											
								if(bookingList.getBCard_Agent_CommissionPercentage().contains(toAgentTypePerOrValue)){
										
									PrintWriter.append("<td>"+bookingList.getBCard_Agent_CommissionPercentage()+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
								}
								
								else{
									PrintWriter.append("<td>"+bookingList.getBCard_Agent_CommissionPercentage()+"</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
								}
								
								testCaseCountBookingList ++;			
								PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Commission Amount</td>");
								PrintWriter.append("<td>"+toAgentCommission+"</td>");
											
								if(bookingList.getBCard_Agent_CommissionAmount().contains(Integer.toString(toAgentCommission))){
										
									PrintWriter.append("<td>"+bookingList.getBCard_Agent_CommissionAmount()+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
								}
								
								else{
									PrintWriter.append("<td>"+bookingList.getBCard_Agent_CommissionAmount()+"</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
								}
								
							}
							if (UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCREDITLPOY) {
								
								String toAgentTypePerOrValue = PG_Properties.getProperty(UserTypeDC_B2B.toString() + "#Value");
								
								testCaseCountBookingList ++;			
								PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Commission Percentage</td>");
								PrintWriter.append("<td>"+toAgentTypePerOrValue+"</td>");
											
								if(bookingList.getBCard_Agent_CommissionPercentage().contains(toAgentTypePerOrValue)){
										
									PrintWriter.append("<td>"+bookingList.getBCard_Agent_CommissionPercentage()+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
								}
								
								else{
									PrintWriter.append("<td>"+bookingList.getBCard_Agent_CommissionPercentage()+"</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
								}
								
								testCaseCountBookingList ++;			
								PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Commission Amount</td>");
								PrintWriter.append("<td>"+toAgentCommission+"</td>");
											
								if(bookingList.getBCard_Agent_CommissionAmount().contains(Integer.toString(toAgentCommission))){
										
									PrintWriter.append("<td>"+bookingList.getBCard_Agent_CommissionAmount()+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
								}
								
								else{
									PrintWriter.append("<td>"+bookingList.getBCard_Agent_CommissionAmount()+"</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
								}
								
							}
							
							
						}
						
						////
						
						if (bookingList.getBCard_Activity_BookingStatus().toLowerCase().contains("req")) {
							
							testCaseCountBookingList ++;			
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Activity Details - Booking status</td>");
							PrintWriter.append("<td>"+activitydetails.getConfirmation_BI_BookingStatus()+"</td>");
										
							if(activitydetails.getConfirmation_BI_BookingStatus().toLowerCase().contains(bookingList.getBCard_Activity_BookingStatus().toLowerCase())){
									
								PrintWriter.append("<td>"+bookingList.getBCard_Activity_BookingStatus()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+bookingList.getBCard_Activity_BookingStatus()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
						}
						
						if (bookingList.getBCard_Activity_BookingStatus().toLowerCase().contains("con")) {
							
							testCaseCountBookingList ++;			
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Activity Details - Booking status</td>");
							PrintWriter.append("<td>"+activitydetails.getConfirmation_BI_BookingStatus()+"</td>");
										
							if(activitydetails.getConfirmation_BI_BookingStatus().toLowerCase().contains(bookingList.getBCard_Activity_BookingStatus().toLowerCase())){
									
								PrintWriter.append("<td>"+bookingList.getBCard_Activity_BookingStatus()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+bookingList.getBCard_Activity_BookingStatus()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
						}
						
						
						testCaseCountBookingList ++;			
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Activity Name</td>");
						PrintWriter.append("<td>"+search.getActivityName()+"</td>");
									
						if(search.getActivityName().toLowerCase().contains(bookingList.getBCard_Activity_ActivityName().toLowerCase())){
								
							PrintWriter.append("<td>"+bookingList.getBCard_Activity_ActivityName()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+bookingList.getBCard_Activity_ActivityName()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						testCaseCountBookingList ++;			
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Activity Details - Date booked</td>");
						PrintWriter.append("<td>"+search.getActivityDate()+"</td>");
									
						if(search.getActivityDate().contains(bookedDate_con)){
								
							PrintWriter.append("<td>"+bookedDate_con+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+bookedDate_con+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						////
						
						testCaseCountBookingList++;
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Activity List Currency</td>");
						PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
						
						if(search.getSellingCurrency().equals(bookingList.getBCard_Activity_Currency())){
							
							PrintWriter.append("<td>"+bookingList.getBCard_Activity_Currency()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+bookingList.getBCard_Activity_Currency()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						String bookingSubTotal = bookingList.getBCard_Activity_SubTotal().split("\\.")[0];
						
						testCaseCountBookingList ++;
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Sub Total (Without tax)</td>");
						PrintWriter.append("<td>"+(PaymentPage_SubTotal - discountedVale)+"</td>");
									
						if(Integer.toString(PaymentPage_SubTotal - discountedVale).equals(bookingSubTotal)){
								
							PrintWriter.append("<td>"+bookingSubTotal+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+bookingSubTotal+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						int totalTaxForAct = PaymentPage_Taxes;
						String bookingTaxOtther = bookingList.getBCard_Activity_Tax().split("\\.")[0];
						
						if (bookingList.getBCard_Activity_TotalBookingValue().contains(",")) {
							bookingTotalvalueForAct = bookingList.getBCard_Activity_TotalBookingValue().split("\\.")[0].replace(",", "");
						}else{
							bookingTotalvalueForAct = bookingList.getBCard_Activity_TotalBookingValue().split("\\.")[0];
						}
						
						
						testCaseCountBookingList ++;
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Tax & Other Charges</td>");
						PrintWriter.append("<td>"+(PaymentPage_Taxes)+"</td>");
									
						if(Integer.toString(PaymentPage_Taxes).equals(bookingTaxOtther)){
								
							PrintWriter.append("<td>"+bookingTaxOtther+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+bookingTaxOtther+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						////////
					
						if (paymentType.equals("Pay Online")) {
							
							
							String BCard_Activity_CreditFee = bookingList.getBCard_Res_Credit_Fee().split("\\.")[0];
							testCaseCountBookingList ++;
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Credit Card Fee</td>");
							PrintWriter.append("<td>"+(creditTax)+"</td>");
										
							if(Integer.toString(creditTax).equals(BCard_Activity_CreditFee)){
									
								PrintWriter.append("<td>"+BCard_Activity_CreditFee+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+BCard_Activity_CreditFee+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
							
							testCaseCountBookingList ++;
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Activity Total Booking Value</td>");
							PrintWriter.append("<td>"+(PaymentPage_SubTotal + totalTaxForAct - discountedVale)+"</td>");
										
							if(Integer.toString(PaymentPage_SubTotal + totalTaxForAct - discountedVale).equals(bookingList.getBCard_Activity_TotalBookingValue().split("\\.")[0])){
									
								PrintWriter.append("<td>"+bookingList.getBCard_Activity_TotalBookingValue().split("\\.")[0]+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+bookingList.getBCard_Activity_TotalBookingValue().split("\\.")[0]+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
							testCaseCountBookingList ++;
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Amount Chargeable Upfront</td>");
							PrintWriter.append("<td>"+(PaymentPage_SubTotal + totalTaxForAct - discountedVale)+"</td>");
										
							if(Integer.toString(PaymentPage_SubTotal + totalTaxForAct - discountedVale).equals(bookingList.getBCard_Activity_AmountChargeable().split("\\.")[0])){
									
								PrintWriter.append("<td>"+bookingList.getBCard_Activity_AmountChargeable().split("\\.")[0]+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+bookingList.getBCard_Activity_AmountChargeable().split("\\.")[0]+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
							/*testCaseCountBookingList ++;
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Amount Paid</td>");
							PrintWriter.append("<td>"+(PaymentPage_SubTotal + totalTaxForAct - discountedVale)+"</td>");
										
							if(Integer.toString(PaymentPage_SubTotal + totalTaxForAct - discountedVale).equals(bookingList.getBCard_Activity_AmountPaid().split("\\.")[0])){
									
								PrintWriter.append("<td>"+bookingList.getBCard_Activity_AmountPaid().split("\\.")[0]+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+bookingList.getBCard_Activity_AmountPaid().split("\\.")[0]+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}*/
							
							testCaseCountBookingList ++;
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Amount Due At Check-In</td>");
							PrintWriter.append("<td>"+PaymentPage_AmountCheckIn+"</td>");
										
							if(Integer.toString(PaymentPage_AmountCheckIn).equals(bookingList.getBCard_Activity_AmountDue().split("\\.")[0])){
									
								PrintWriter.append("<td>"+bookingList.getBCard_Activity_AmountDue().split("\\.")[0]+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+bookingList.getBCard_Activity_AmountDue().split("\\.")[0]+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
							
							
						}else{
							
							int creditNoFee = 0;
							
							String BCard_Activity_CreditFee = bookingList.getBCard_Res_Credit_Fee().split("\\.")[0];
							testCaseCountBookingList ++;
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Credit Card Fee</td>");
							PrintWriter.append("<td>"+(creditNoFee)+"</td>");
										
							if(Integer.toString(creditNoFee).equals(BCard_Activity_CreditFee)){
									
								PrintWriter.append("<td>"+BCard_Activity_CreditFee+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+BCard_Activity_CreditFee+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
							
							testCaseCountBookingList ++;
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Activity Total Booking Value</td>");
							PrintWriter.append("<td>"+(PaymentPage_TotalPackageValue + creditNoFee - discountedVale)+"</td>");
										
							if(Integer.toString(PaymentPage_TotalPackageValue + creditNoFee - discountedVale).equals(bookingList.getBCard_Activity_TotalBookingValue().split("\\.")[0])){
									
								PrintWriter.append("<td>"+bookingList.getBCard_Activity_TotalBookingValue().split("\\.")[0]+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+bookingList.getBCard_Activity_TotalBookingValue().split("\\.")[0]+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
							testCaseCountBookingList ++;
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Amount Chargeable Upfront</td>");
							PrintWriter.append("<td>"+(PaymentPage_TotalPackageValue + creditNoFee - discountedVale)+"</td>");
										
							if(Integer.toString(PaymentPage_TotalPackageValue + creditNoFee - discountedVale).equals(bookingList.getBCard_Activity_AmountChargeable().split("\\.")[0])){
									
								PrintWriter.append("<td>"+bookingList.getBCard_Activity_AmountChargeable().split("\\.")[0]+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+bookingList.getBCard_Activity_AmountChargeable().split("\\.")[0]+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
						
							/*testCaseCountBookingList ++;
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Amount Paid</td>");
							PrintWriter.append("<td>"+(PaymentPage_TotalPackageValue + creditNoFee - discountedVale)+"</td>");
										
							if(Integer.toString(PaymentPage_TotalPackageValue + creditNoFee - discountedVale).equals(bookingList.getBCard_Activity_AmountPaid().split("\\.")[0])){
									
								PrintWriter.append("<td>"+bookingList.getBCard_Activity_AmountPaid().split("\\.")[0]+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+bookingList.getBCard_Activity_AmountPaid().split("\\.")[0]+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}*/
							
							testCaseCountBookingList ++;
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Amount Due At Check-In</td>");
							PrintWriter.append("<td>"+PaymentPage_AmountCheckIn+"</td>");
										
							if(Integer.toString(PaymentPage_AmountCheckIn).equals(bookingList.getBCard_Activity_AmountDue().split("\\.")[0])){
									
								PrintWriter.append("<td>"+bookingList.getBCard_Activity_AmountDue().split("\\.")[0]+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+bookingList.getBCard_Activity_AmountDue().split("\\.")[0]+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
						}
						
						
						///////
						
						
						String personRate = bookingList.getBCard_Activity_TotalRate().split("\\.")[0];
						String acrate = " " + activitydetails.getResultsPage_DailyRate().get(0);
						testCaseCountBookingList ++;
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Person Rate</td>");
						PrintWriter.append("<td>"+acrate+"</td>");
									
						if(acrate.equals(personRate)){
								
							PrintWriter.append("<td>"+personRate+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+personRate+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						int totalPax = Integer.parseInt(search.getAdults()) + Integer.parseInt(search.getChildren());
						String pax = " " + totalPax;
						
						testCaseCountBookingList ++;
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - QTY</td>");
						PrintWriter.append("<td>"+pax+"</td>");
									
						if(pax.equals(bookingList.getBCard_Activity_qty())){
								
							PrintWriter.append("<td>"+bookingList.getBCard_Activity_qty()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+bookingList.getBCard_Activity_qty()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						///////
						
						testCaseCountBookingList ++;
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Payment Cancellation policy and Booking list Report cancellation policy match</td>");
						PrintWriter.append("<td>Payment Page :- "+activitydetails.getCancelPolicy().size()+"</td>");
									
						if(activitydetails.getCancelPolicy().size() == bookingList.getCancelPolicy().size()){
								
							PrintWriter.append("<td>Booking Report :- "+bookingList.getCancelPolicy().size()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>Booking Report :- "+bookingList.getCancelPolicy().size()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						
						if (bookingList.getCancelPolicy().size() == 2) {
							
							String policyList_1 = bookingList.getCancelPolicy().get(0).replace("\n", "");
							String noshowCancel_1 = bookingList.getCancelPolicy().get(1).replace(",", "");
							
							testCaseCountBookingList++;
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking list Report Activity cancellation policy - 1</td>");
							PrintWriter.append("<td>"+resPolist1+"</td>");
							
							try {
								if(policyList_1.toLowerCase().replaceAll(" ", "").equals(resPolist1.toLowerCase().replaceAll(" ", ""))){
									
									PrintWriter.append("<td>"+policyList_1+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
								}
								
								
							} catch (Exception e) {
								PrintWriter.append("<td>"+e.getMessage()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
								
							}	
							
							
							testCaseCountBookingList++;
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking list Report Activity No-Show Fee policy</td>");
							PrintWriter.append("<td>"+noShowActual+"</td>");
							
							if(noshowCancel_1.toLowerCase().replaceAll(" ", "").equals(noShowActual.toLowerCase().replaceAll(" ", ""))){
								
								PrintWriter.append("<td>"+noshowCancel_1+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							else{
								PrintWriter.append("<td>"+noshowCancel_1+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
						}
						
						if (bookingList.getCancelPolicy().size() == 3) {
							
							String policyList_21 = bookingList.getCancelPolicy().get(0).replace("\n", "");
							String policyList_22 = bookingList.getCancelPolicy().get(1).replace("\n", "");
							String noshowCancel_12 = bookingList.getCancelPolicy().get(2).replace(",", "");
							
							testCaseCountBookingList++;
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking list Report Activity Refundable policy</td>");
							PrintWriter.append("<td>"+policyListOne+"</td>");
							
							if(policyListOne.equals(policyList_21)){
								
								PrintWriter.append("<td>"+policyList_21+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							else{
								PrintWriter.append("<td>"+policyList_21+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
								
							}
								
							
							testCaseCountBookingList++;
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking list Report Activity cancellation policy - 1</td>");
							PrintWriter.append("<td>"+policyListTwo+"</td>");
							
							if(policyListTwo.replaceAll(" ", "").contains(policyList_22.replaceAll(" ", ""))){
								
								PrintWriter.append("<td>"+policyList_22+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							else{
								PrintWriter.append("<td>"+policyList_22+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
								
							}
							
							testCaseCountBookingList++;
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking list Report Activity No-Show Fee policy</td>");
							PrintWriter.append("<td>"+noShowActual+"</td>");
							
							if(noshowCancel_12.toLowerCase().replaceAll(" ", "").equals(noShowActual.toLowerCase().replaceAll(" ", ""))){
								
								PrintWriter.append("<td>"+noshowCancel_12+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							
							else{
								PrintWriter.append("<td>"+noshowCancel_12+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
									
						}
						
						//Paid Options
						
						if(UserTypeDC_B2B == WEB_TourOperatorType.WEB_NETCREDITLPON || UserTypeDC_B2B == WEB_TourOperatorType.WEB_NETCREDITLPOY || UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCREDITLPOY || UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCREDITLPONO){
					    	
					    	if (paymentType.equalsIgnoreCase("Pay Offline")) {
						
					    		int beforePaid = Integer.parseInt(bookingList.getBeforeCreditBalance());
					    		int totalAmountBookingList = (PaymentPage_TotalPackageValue);
					    		int balance = beforePaid - totalAmountBookingList; 
					    		
					    		try {
					    			
					    			if (UserTypeDC_B2B == WEB_TourOperatorType.WEB_NETCREDITLPON || UserTypeDC_B2B == WEB_TourOperatorType.WEB_NETCREDITLPOY) {
										
					    				testCaseCountBookingList++;
										PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking list Report - Balance Due</td>");
										PrintWriter.append("<td>"+supplierCurrency+""+(PaymentPage_AmountProcessed)+"</td>");
										
										if(bookingList.getBalanceDue().contains(Integer.toString(PaymentPage_AmountProcessed))){
											
											PrintWriter.append("<td>"+bookingList.getBalanceDue()+"</td>");
											PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
										}
										
										
										else{
											PrintWriter.append("<td>"+bookingList.getBalanceDue()+"</td>");
											PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
										}
					    				
									} else {

										testCaseCountBookingList++;
										PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking list Report - Balance Due</td>");
										PrintWriter.append("<td>"+supplierCurrency+""+(PaymentPage_AmountProcessed)+"</td>");
										
										if(bookingList.getBalanceDue().contains(Integer.toString(PaymentPage_AmountProcessed))){
											
											PrintWriter.append("<td>"+bookingList.getBalanceDue()+"</td>");
											PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
										}
										
										
										else{
											PrintWriter.append("<td>"+bookingList.getBalanceDue()+"</td>");
											PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
										}
										
									}
					    			
									
								} catch (Exception e) {
									e.printStackTrace();
								}
					    		
					    		testCaseCountBookingList++;
								PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking list Report - TO Credit Balance</td>");
								PrintWriter.append("<td>"+beforePaid+"</td>");
								
								if(Integer.toString(beforePaid).equals(bookingList.getAfterPayCreditBalance())){
									
									PrintWriter.append("<td>"+bookingList.getAfterPayCreditBalance()+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
								}
								
								
								else{
									PrintWriter.append("<td>"+bookingList.getAfterPayCreditBalance()+"</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
								}
						
								
								
								if (UserTypeDC_B2B == WEB_TourOperatorType.WEB_NETCREDITLPOY || UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCREDITLPOY ) {
									
									testCaseCountBookingList++;
									PrintWriter.append("<tr><td>" + testCaseCountBookingList + "</td> <td>Booking list Report - LPO No</td>");
									PrintWriter.append("<td>" + bookingList.getBeforeLpoNo() + "</td>");
									
									if (bookingList.getBeforeLpoNo().equalsIgnoreCase(bookingList.getLpoNo())) {

										PrintWriter.append("<td>" + bookingList.getLpoNo() + "</td>");
										PrintWriter.append("<td class='Passed'>PASS</td><tr>");
									}

									else {
										PrintWriter.append("<td>" + bookingList.getLpoNo() + "</td>");
										PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
									}
									
								}
								
								
								testCaseCountBookingList ++;			
								PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking list Report - Invoice Issued [After Paid]</td>");
								PrintWriter.append("<td>Green</td>");
											
								if(bookingList.isPaid_Invoice_Issued() == true){
										
									PrintWriter.append("<td>Green</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
								}
								
								else{
									PrintWriter.append("<td>Not match</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
								}
								
								testCaseCountBookingList ++;			
								PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking list Report - Payments Issued [After Paid]</td>");
								PrintWriter.append("<td>Green</td>");
											
								if(bookingList.isPaid_Payment_Receieved() == true){
										
									PrintWriter.append("<td>Green</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
								}
								
								else{
									PrintWriter.append("<td>Not match</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
								}
								
								testCaseCountBookingList ++;			
								PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking list Report - Voucher Issued [After Paid]</td>");
								PrintWriter.append("<td>Green</td>");
											
								if(bookingList.isPaid_Voucher_Issued() == true){
										
									PrintWriter.append("<td>Green</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
								}
								
								else{
									PrintWriter.append("<td>Not match</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
								}
								
								
					    	}
						}
						
						
						
					
					
					}else{
						
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report</td>");
						PrintWriter.append("<td>Booking List Report should be available</td>");
						PrintWriter.append("<td>Not Available</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");		
						
					}
					
					customerQty = Integer.parseInt(search.getAdults()) + Integer.parseInt(search.getChildren());	
					
					PrintWriter.append("</table>");
					
					
					
					/////////////////////////////////////////////////////////////////////////////////////
					//Customer Confirmation Mail
					
					getMailsInfo(driver);
					
					
					/////////////////////////////////////////////////////////////////////////////////////
					//Supplier Mail
					
					
					PrintWriter.append("<br><br>");
					PrintWriter.append("<p class='fontStyles'>Email - Supplier Email</p>");
					
					testcaseSupplier = 1;
					PrintWriter.append("<br><table ><tr><th>Test Case No</th><th>Test Description</th><th>Expect Values - [Excel Value]</th><th>Actual Values</th><th>Test Status</th></tr>");
					
					if (confirmationDetails.isSupplier() == true) {
					
						PrintWriter.append("<tr><td>"+testcaseSupplier+"</td> <td>Supplier Mail - Booking No</td>");
						PrintWriter.append("<td>"+activitydetails.getReservationNo()+"</td>");
						
						if(activitydetails.getReservationNo().equals(confirmationDetails.getSupplierMAIL_BookingNo())){
							
							PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_BookingNo()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_BookingNo()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
					
						String SupplierMAIL_IssueDate = currentDateforMatch;
					
						testcaseSupplier++;
						PrintWriter.append("<tr><td>"+testcaseSupplier+"</td> <td>Supplier Mail - Booking Issue Date</td>");
						PrintWriter.append("<td>"+SupplierMAIL_IssueDate+"</td>");
						
						if(confirmationDetails.getSupplierMAIL_IssueDate().replace("-", "").equals(SupplierMAIL_IssueDate.replace("-", ""))){
							
							PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_IssueDate()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_IssueDate()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testcaseSupplier++;
						PrintWriter.append("<tr><td>"+testcaseSupplier+"</td> <td>Supplier Mail - Acitivity Name</td>");
						PrintWriter.append("<td>"+search.getActivityName()+"</td>");
						
						if(search.getActivityName().toLowerCase().contains(confirmationDetails.getSupplierMAIL_ActivityName().toLowerCase())){
								
							PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_ActivityName()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_ActivityName()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						testcaseSupplier ++;
						PrintWriter.append("<tr><td>"+testcaseSupplier+"</td> <td>Supplier Mail - Booking Status</td>");
						PrintWriter.append("<td>"+activitydetails.getConfirmation_BI_BookingStatus()+"</td>");
									
						if (activitydetails.getConfirmation_BI_BookingStatus().toLowerCase().contains("request")) {
							
							if(activitydetails.getConfirmation_BI_BookingStatus().toLowerCase().contains(confirmationDetails.getSupplierMAIL_BookingStatus().toLowerCase())){
								
								PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_BookingStatus()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else {
								
								PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_BookingStatus()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
							}
							
						} else {
		
							if(activitydetails.getConfirmation_BI_BookingStatus().toLowerCase().contains(confirmationDetails.getSupplierMAIL_BookingStatus().toLowerCase())){
								
								PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_BookingStatus()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else {
								
								PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_BookingStatus()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
							}
						}
						
						
						testcaseSupplier ++;
						PrintWriter.append("<tr><td>"+testcaseSupplier+"</td> <td>Supplier Mail - Program City</td>");
						PrintWriter.append("<td>"+search.getDestination()+"</td>");
									
						if(search.getDestination().toLowerCase().contains(confirmationDetails.getSupplierMAIL_ProgramCity().toLowerCase())){
								
							PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_ProgramCity()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_ProgramCity()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						///
						
						
						/*PrintWriter.append("<tr><td>"+testcaseSupplier+"</td> <td>Supplier Mail - Activity Duration</td>");
						PrintWriter.append("<td>"+activitydetails.getActivitySession()+"</td>");
									
						if(confirmationDetails.getSupplierMAIL_Duration().toLowerCase().contains(activitydetails.getActivitySession().toLowerCase())){
								
							PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_Duration()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_Duration()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}*/
						
						testcaseSupplier ++;
						PrintWriter.append("<tr><td>"+testcaseSupplier+"</td> <td>Supplier Mail - Activity QTY</td>");
						PrintWriter.append("<td>"+customerQty+"</td>");
									
						if(Integer.toString(customerQty).equals(confirmationDetails.getSupplierMAIL_QTY())){
								
							PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_QTY()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_QTY()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
					
						String SupplierMAIL_ServicyDate = search.getActivityDate();
					
						testcaseSupplier ++;
						PrintWriter.append("<tr><td>"+testcaseSupplier+"</td> <td>Supplier Mail - Activity Service Date</td>");
						PrintWriter.append("<td>"+SupplierMAIL_ServicyDate+"</td>");
									
						if(SupplierMAIL_ServicyDate.replace("-", "").equals(confirmationDetails.getSupplierMAIL_ServicyDate().replace("-", ""))){
								
							PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_ServicyDate()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_ServicyDate()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						/////
						
						int activityrate = ((PaymentPage_SubTotal * 100 ) / (100 + pMarkup));
						
						testcaseSupplier ++;
						PrintWriter.append("<tr><td>"+testcaseSupplier+"</td> <td>Supplier Mail - Activity Rate [Without PM]</td>");
						PrintWriter.append("<td>"+activityrate+"</td>");
									
						if(Integer.toString(activityrate).equals(confirmationDetails.getSupplierMAIL_ActivityRate())){
								
							PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_ActivityRate()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_ActivityRate()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						testcaseSupplier ++;
						PrintWriter.append("<tr><td>"+testcaseSupplier+"</td> <td>Supplier Mail - Customer TP</td>");
						PrintWriter.append("<td>"+activitydetails.getPaymentPage_TP()+"</td>");
									
						if(activitydetails.getPaymentPage_TP().replaceAll("-", "").equals(confirmationDetails.getSupplierMAIL_CustomerTP())){
								
							PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_CustomerTP()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_CustomerTP()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						
						if (UserTypeDC_B2B == WEB_TourOperatorType.WEB_DC) {
							
							testcaseSupplier ++;			
							PrintWriter.append("<tr><td>"+testcaseSupplier+"</td> <td>Supplier Mail - Tel 1</td>");
							PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Tel")+"</td>");
										
							if(PG_Properties.getProperty("Portal.Tel").toLowerCase().equals(confirmationDetails.getSupplierMAIL_Tel_1().toLowerCase())){
									
								PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_Tel_1()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_Tel_1()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
							
							testcaseSupplier ++;			
							PrintWriter.append("<tr><td>"+testcaseSupplier+"</td> <td>Supplier Mail - Email 1</td>");
							PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Email")+"</td>");
										
							if(PG_Properties.getProperty("Portal.Email").toLowerCase().equals(confirmationDetails.getSupplierMAIL_Email_1().toLowerCase())){
									
								PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_Email_1()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_Email_1()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
							
							testcaseSupplier ++;			
							PrintWriter.append("<tr><td>"+testcaseSupplier+"</td> <td>Supplier Mail - Website</td>");
							PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Website")+"</td>");
										
							if(PG_Properties.getProperty("Portal.Website").toLowerCase().equals(confirmationDetails.getSupplierMAIL_Website().toLowerCase())){
									
								PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_Website()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_Website()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
							testcaseSupplier ++;			
							PrintWriter.append("<tr><td>"+testcaseSupplier+"</td> <td>Supplier Mail - Fax</td>");
							PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Fax")+"</td>");
										
							if(PG_Properties.getProperty("Portal.Fax").toLowerCase().equals(confirmationDetails.getSupplierMAIL_Fax().toLowerCase())){
									
								PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_Fax()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_Fax()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
							testcaseSupplier ++;			
							PrintWriter.append("<tr><td>"+testcaseSupplier+"</td> <td>Supplier Mail - Company Name</td>");
							PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Name")+"</td>");
										
							if(PG_Properties.getProperty("Portal.Name").toLowerCase().equals(confirmationDetails.getSupplierMAIL_CompanyName().toLowerCase())){
									
								PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_CompanyName()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_CompanyName()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
						}else{
							
							testcaseSupplier ++;			
							PrintWriter.append("<tr><td>"+testcaseSupplier+"</td> <td>Supplier Mail - Tel 1</td>");
							PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.TelTO")+"</td>");
										
							if(PG_Properties.getProperty("Portal.TelTO").toLowerCase().equals(confirmationDetails.getSupplierMAIL_Tel_1().toLowerCase())){
									
								PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_Tel_1()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_Tel_1()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
							
							testcaseSupplier ++;			
							PrintWriter.append("<tr><td>"+testcaseSupplier+"</td> <td>Supplier Mail - Email 1</td>");
							PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.EmailTO")+"</td>");
										
							if(PG_Properties.getProperty("Portal.EmailTO").toLowerCase().equals(confirmationDetails.getSupplierMAIL_Email_1().toLowerCase())){
									
								PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_Email_1()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_Email_1()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
							
							testcaseSupplier ++;			
							PrintWriter.append("<tr><td>"+testcaseSupplier+"</td> <td>Supplier Mail - Website</td>");
							PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.WebsiteTO")+"</td>");
										
							if(PG_Properties.getProperty("Portal.WebsiteTO").toLowerCase().equals(confirmationDetails.getSupplierMAIL_Website().toLowerCase())){
									
								PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_Website()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_Website()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
							testcaseSupplier ++;			
							PrintWriter.append("<tr><td>"+testcaseSupplier+"</td> <td>Supplier Mail - Fax</td>");
							PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.FaxTO")+"</td>");
										
							if(PG_Properties.getProperty("Portal.FaxTO").toLowerCase().equals(confirmationDetails.getSupplierMAIL_Fax().toLowerCase())){
									
								PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_Fax()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_Fax()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
							testcaseSupplier ++;			
							PrintWriter.append("<tr><td>"+testcaseSupplier+"</td> <td>Supplier Mail - Company Name</td>");
							PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.NameTO")+"</td>");
										
							if(PG_Properties.getProperty("Portal.NameTO").toLowerCase().equals(confirmationDetails.getSupplierMAIL_CompanyName().toLowerCase())){
									
								PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_CompanyName()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_CompanyName()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
						}
						
						
						
						
						
						
						
						
					
					}else{
						
						PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Email - Supplier Mail</td>");
						PrintWriter.append("<td>Supplier Mail should be available</td>");
						PrintWriter.append("<td>Not Available</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");		
						
						
					}
					
					PrintWriter.append("</table>");
					
					
					///////////////////////////////////////////////////////////////////////////////////////////////////////
					//Reservation report	
					
					PrintWriter.append("<br><br>");
					PrintWriter.append("<p class='fontStyles'>Reservation Report Summary</p>");
					
					testCaseCountReservation = 1;
					PrintWriter.append("<br><table ><tr><th>Test Case No</th><th>Test Description</th><th>Expect Values - [Excel Value]</th><th>Actual Values</th><th>Test Status</th></tr>");
					
					if (reservationdetails.isReservationReportLoaded() == true) {
						
						PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Booking Date</td>");
						PrintWriter.append("<td>"+currentDateforMatch+"</td>");
									
						if(currentDateforMatch.equals(reservationdetails.getBookingDate())){
								
							PrintWriter.append("<td>"+reservationdetails.getBookingDate()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+reservationdetails.getBookingDate()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						
						if (activitydetails.getCancelPolicy().size() == 3) {
						
							testCaseCountReservation ++;
							PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Cancellation due Date</td>");
							PrintWriter.append("<td>"+dateWithBufferDates_3+"</td>");
										
							if(dateWithBufferDates_3.equals(reservationdetails.getCanPolicyDueDate())){
									
								PrintWriter.append("<td>"+reservationdetails.getCanPolicyDueDate()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else {
								
								PrintWriter.append("<td>"+reservationdetails.getCanPolicyDueDate()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
							}
						
						}else{
							
							testCaseCountReservation ++;
							PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Cancellation due Date</td>");
							PrintWriter.append("<td>"+dateWithBufferDates_1+"</td>");
										
							if(dateWithBufferDates_1.equals(reservationdetails.getCanPolicyDueDate())){
									
								PrintWriter.append("<td>"+reservationdetails.getCanPolicyDueDate()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else {
								
								PrintWriter.append("<td>"+reservationdetails.getCanPolicyDueDate()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
							}
							
						}
						
						
						if (UserTypeDC_B2B == WEB_TourOperatorType.WEB_DC) {
							
							String reservationCusName = ""+activitydetails.getPaymentPage_LName()+" "+activitydetails.getPaymentPage_FName()+"";
							
							testCaseCountReservation ++;
							PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Customer Name</td>");
							PrintWriter.append("<td>"+reservationCusName+"</td>");
										
							if(reservationCusName.equals(reservationdetails.getCustomerName_1())){
									
								PrintWriter.append("<td>"+reservationdetails.getCustomerName_1()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else {
								
								PrintWriter.append("<td>"+reservationdetails.getCustomerName_1()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
							}
							
							
						} else {

							testCaseCountReservation ++;			
							PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Customer Name</td>");
							PrintWriter.append("<td>"+PG_Properties.getProperty(UserTypeDC_B2B.toString()).split("/")[2]+"</td>");
										
							if(PG_Properties.getProperty(UserTypeDC_B2B.toString()).split("/")[2].replaceAll(" ", "").equalsIgnoreCase(reservationdetails.getCustomerName_1().replaceAll(" ", ""))){
									
								PrintWriter.append("<td>"+reservationdetails.getCustomerName_1()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+reservationdetails.getCustomerName_1()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
							
						}
						
						
						testCaseCountReservation ++;
						PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Supplier Name</td>");
						PrintWriter.append("<td>"+supplierName+"</td>");
									
						if(supplierName.equals(reservationdetails.getSupplierName())){
								
							PrintWriter.append("<td>"+reservationdetails.getSupplierName()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+reservationdetails.getSupplierName()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						
						
						testCaseCountReservation ++;
						PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Customer TP</td>");
						PrintWriter.append("<td>"+activitydetails.getPaymentPage_TP()+"</td>");
									
						if(activitydetails.getPaymentPage_TP().equals(reservationdetails.getCustomerTP())){
								
							PrintWriter.append("<td>"+reservationdetails.getCustomerTP()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+reservationdetails.getCustomerTP()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						testCaseCountReservation ++;
						PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Product Type</td>");
						PrintWriter.append("<td>Activities</td>");
									
						if(reservationdetails.getProductType().equalsIgnoreCase("Activities")){
								
							PrintWriter.append("<td>"+reservationdetails.getProductType()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+reservationdetails.getProductType()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						testCaseCountReservation ++;
						PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Booking Status</td>");
						PrintWriter.append("<td>Normal</td>");
									
						if(reservationdetails.getBookingStatus().equalsIgnoreCase("Normal")){
								
							PrintWriter.append("<td>"+reservationdetails.getBookingStatus()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+reservationdetails.getBookingStatus()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						testCaseCountReservation ++;
						PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Booking Channel</td>");
						PrintWriter.append("<td>Website</td>");
									
						if(reservationdetails.getBookingChannel().equalsIgnoreCase("Website")){
								
							PrintWriter.append("<td>"+reservationdetails.getBookingChannel()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+reservationdetails.getBookingChannel()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						
						
						
						
						
						if (paymentType.equals("Pay Online")) {
							
							testCaseCountReservation ++;
							PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Payment Type</td>");
							PrintWriter.append("<td>Credit Card</td>");
										
							if(reservationdetails.getPaymentType().replaceAll(" ", "").equalsIgnoreCase("Credit Card".replaceAll(" ", ""))){
									
								PrintWriter.append("<td>"+reservationdetails.getPaymentType()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else {
								
								PrintWriter.append("<td>"+reservationdetails.getPaymentType()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
							}
							
							testCaseCountReservation ++;
							PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Payment Reference</td>");
							PrintWriter.append("<td>"+activitydetails.getPaymentID()+"</td>");
										
							if(activitydetails.getPaymentID().replaceAll(" ", "").equalsIgnoreCase(reservationdetails.getPayRefernce().replaceAll(" ", ""))){
									
								PrintWriter.append("<td>"+reservationdetails.getPayRefernce()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else {
								
								PrintWriter.append("<td>"+reservationdetails.getPayRefernce()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
							}
							
							testCaseCountReservation ++;
							PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Auth Code</td>");
							PrintWriter.append("<td>"+activitydetails.getAuthentReference_bookingList()+"</td>");
										
							if(activitydetails.getAuthentReference_bookingList().replaceAll(" ", "").equalsIgnoreCase(reservationdetails.getAuthCode().replaceAll(" ", ""))){
									
								PrintWriter.append("<td>"+reservationdetails.getAuthCode()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else {
								
								PrintWriter.append("<td>"+reservationdetails.getAuthCode()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
							}
							
							
							
							
						}
						
						
						
						
						
						
						
						
						
						
						
						
						
						
						
						
						
						
						
						
						
						
						if (paymentType.equals("Pay Online")) {
							
							testCaseCountReservation ++;
							PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Invoice Date</td>");
							PrintWriter.append("<td>"+currentDateforMatch+"</td>");
										
							if(currentDateforMatch.equals(reservationdetails.getInvoiceIssuedDate())){
									
								PrintWriter.append("<td>"+reservationdetails.getInvoiceIssuedDate()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else {
								
								PrintWriter.append("<td>"+reservationdetails.getInvoiceIssuedDate()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
							}
								
						}
						
						testCaseCountReservation ++;
						PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Selling Currency - 1</td>");
						PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
									
						if(search.getSellingCurrency().toLowerCase().equals(reservationdetails.getSellingCurrency_1().toLowerCase())){
								
							PrintWriter.append("<td>"+reservationdetails.getSellingCurrency_1()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+reservationdetails.getSellingCurrency_1()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						
						
						testCaseCountReservation ++;
						PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Total Rate</td>");
						PrintWriter.append("<td>"+Integer.toString(PaymentPage_SubTotal + PaymentPage_Taxes)+"</td>");
									
						if(Integer.toString(PaymentPage_SubTotal + PaymentPage_Taxes).equals(reservationdetails.getTotalRate())){
								
							PrintWriter.append("<td>"+reservationdetails.getTotalRate()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+reservationdetails.getTotalRate()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						
						
						
						if (paymentType.equals("Pay Online")) {
							
							testCaseCountReservation ++;
							PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - CreditCardFee</td>");
							PrintWriter.append("<td>"+creditTax+"</td>");
										
							if(Integer.toString(creditTax).equals(reservationdetails.getCreditCardFee())){
									
								PrintWriter.append("<td>"+reservationdetails.getCreditCardFee()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else {
								
								PrintWriter.append("<td>"+reservationdetails.getCreditCardFee()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
							}
							
							
							
							testCaseCountReservation ++;
							PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Amount paid</td>");
							PrintWriter.append("<td>"+PaymentPage_AmountProcessed+"</td>");
										
							if(Integer.toString(PaymentPage_AmountProcessed).equals(reservationdetails.getAmountPaid())){
									
								PrintWriter.append("<td>"+reservationdetails.getAmountPaid()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else {
								
								PrintWriter.append("<td>"+reservationdetails.getAmountPaid()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
							}
								
						}
						
						
						int offlineCardFee = 0;
						int offlineAmountPaid = 0;
						int offlinePaid = PaymentPage_SubTotal + PaymentPage_Taxes + offlineCardFee - discountedVale;
						
						if (paymentType.equals("Pay Offline")) { 
							
							testCaseCountReservation ++;
							PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - CreditCardFee</td>");
							PrintWriter.append("<td>"+offlineCardFee+"</td>");
										
							if(Integer.toString(offlineCardFee).equals(reservationdetails.getCreditCardFee())){
									
								PrintWriter.append("<td>"+reservationdetails.getCreditCardFee()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else {
								
								PrintWriter.append("<td>"+reservationdetails.getCreditCardFee()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
							}
							
							
							testCaseCountReservation ++;
							PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Amount paid</td>");
							PrintWriter.append("<td>"+offlinePaid+"</td>");
										
							if(Integer.toString(offlinePaid).equals(reservationdetails.getAmountPaid())){
									
								PrintWriter.append("<td>"+reservationdetails.getAmountPaid()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else {
								
								PrintWriter.append("<td>"+reservationdetails.getAmountPaid()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
							}
							
							
						}
						
						/////////
						
						testCaseCountReservation ++;
						PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Selling Currency - 2</td>");
						PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
									
						if(search.getSellingCurrency().toLowerCase().equals(reservationdetails.getSellingCurrency_2().toLowerCase())){
								
							PrintWriter.append("<td>"+reservationdetails.getSellingCurrency_2()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+reservationdetails.getSellingCurrency_2()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						
						if (paymentType.equals("Pay Online")) {
						
							testCaseCountReservation ++;
							PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Gross order Value</td>");
							PrintWriter.append("<td>"+PaymentPage_TotalPackageValue+"</td>");
										
							if(Integer.toString(PaymentPage_TotalPackageValue).equals(reservationdetails.getGrossOrderValue())){
									
								PrintWriter.append("<td>"+reservationdetails.getGrossOrderValue()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else {
								
								PrintWriter.append("<td>"+reservationdetails.getGrossOrderValue()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
							}
							
							
							int netorderValue = PaymentPage_TotalPackageValue - Integer.parseInt(reservationdetails.getAgentCommission()) ;
							
							testCaseCountReservation ++;
							PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Net order Value</td>");
							PrintWriter.append("<td>"+netorderValue+"</td>");
										
							if(Integer.toString(netorderValue).equals(reservationdetails.getNetOrderValue())){
									
								PrintWriter.append("<td>"+reservationdetails.getNetOrderValue()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else {
								
								PrintWriter.append("<td>"+reservationdetails.getNetOrderValue()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
							}
							
							
							int onlineTotalCost = netRate + PaymentPage_TotalTax;
							
							testCaseCountReservation ++;
							PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Total Cost</td>");
							PrintWriter.append("<td>"+onlineTotalCost+"</td>");
										
							if(Integer.toString(onlineTotalCost).equals(reservationdetails.getTotalCost())){
									
								PrintWriter.append("<td>"+reservationdetails.getTotalCost()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else {
								
								PrintWriter.append("<td>"+reservationdetails.getTotalCost()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
							}
							
						}
						
						
						
						if (paymentType.equals("Pay Offline")) { 
							
							testCaseCountReservation ++;
							PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Gross order Value</td>");
							PrintWriter.append("<td>"+offlinePaid+"</td>");
										
							if(Integer.toString(offlinePaid).equals(reservationdetails.getGrossOrderValue())){
									
								PrintWriter.append("<td>"+reservationdetails.getGrossOrderValue()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else {
								
								PrintWriter.append("<td>"+reservationdetails.getGrossOrderValue()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
							}
							
							
							int netorderValue = offlinePaid - toAgentCommission;
							
							testCaseCountReservation ++;
							PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Net order Value</td>");
							PrintWriter.append("<td>"+netorderValue+"</td>");
										
							if(Integer.toString(netorderValue).equals(reservationdetails.getNetOrderValue())){
									
								PrintWriter.append("<td>"+reservationdetails.getNetOrderValue()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else {
								
								PrintWriter.append("<td>"+reservationdetails.getNetOrderValue()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
							}
							
							
							int offlineTotalCost = netRate + PaymentPage_Taxes;
							
							testCaseCountReservation ++;
							PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Total Cost</td>");
							PrintWriter.append("<td>"+offlineTotalCost+"</td>");
										
							if(Integer.toString(offlineTotalCost).equals(reservationdetails.getTotalCost())){
									
								PrintWriter.append("<td>"+reservationdetails.getTotalCost()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else {
								
								PrintWriter.append("<td>"+reservationdetails.getTotalCost()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
							}
								
						}
						
						if (UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCASH || UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCREDITLPOY || UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCREDITLPONO) {
							
							testCaseCountReservation ++;
							PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Agent Commission</td>");
							PrintWriter.append("<td>"+toAgentCommission+"</td>");
										
							if(Integer.toString(toAgentCommission).equals(reservationdetails.getAgentCommission())){
									
								PrintWriter.append("<td>"+reservationdetails.getAgentCommission()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else {
								
								PrintWriter.append("<td>"+reservationdetails.getAgentCommission()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
							}
							
						}else{
							
							testCaseCountReservation ++;
							PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Agent Commission</td>");
							PrintWriter.append("<td>"+toAgentCommission+"</td>");
										
							if(Integer.toString(toAgentCommission).equals(reservationdetails.getAgentCommission())){
									
								PrintWriter.append("<td>"+reservationdetails.getAgentCommission()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else {
								
								PrintWriter.append("<td>"+reservationdetails.getAgentCommission()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
							}
							
						}
						
						
						
						//////
						
						if (paymentType.equals("Pay Online")) {
							
							totalCostInUsd = ((PaymentPage_SubTotal * 100 ) / (100 + pMarkup)) + creditTax + PaymentPage_Taxes;
							
						}
						
						if (paymentType.equals("Pay Offline")) { 
							
							totalCostInUsd = ((PaymentPage_SubTotal * 100 ) / (100 + pMarkup)) + offlineCardFee + ResultsPage_Taxes;
							
						}
					
						
						if((PG_Properties.getProperty("PortalCurrency").toLowerCase().equals(supplierCurrency.toLowerCase()))){
							
							testCaseCountReservation ++;			
							PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Net Order value In USD</td>");
							PrintWriter.append("<td>"+PaymentPage_AmountProcessed+"</td>");
										
							if(Integer.toString(PaymentPage_AmountProcessed).equals(reservationdetails.getBaseCurrency_NetOrgerValue())){
									
								PrintWriter.append("<td>"+reservationdetails.getBaseCurrency_NetOrgerValue()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+reservationdetails.getBaseCurrency_NetOrgerValue()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
							
							
							testCaseCountReservation ++;			
							PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Total cost In USD</td>");
							PrintWriter.append("<td>"+totalCostInUsd+"</td>");
										
							if(Integer.toString(totalCostInUsd).equals(reservationdetails.getBaseCurrency_TotalCost())){
									
								PrintWriter.append("<td>"+reservationdetails.getBaseCurrency_TotalCost()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+reservationdetails.getBaseCurrency_TotalCost()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
							
							
						}
						
						else{
							
							for(Entry<String, String> entry: currencyMap.entrySet()) {
								if(supplierCurrency.toLowerCase().equals(entry.getKey().toLowerCase())){
									
									rateConvertforUSD = Double.parseDouble(entry.getValue());
									
									int ResultsPage_TotalPayable_ForUSD =  (int) Math.ceil((PaymentPage_AmountProcessed) / rateConvertforUSD );
									
									
									testCaseCountReservation ++;			
									PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Net Order value In USD</td>");
									PrintWriter.append("<td>"+ResultsPage_TotalPayable_ForUSD+"</td>");
												
									if(Integer.toString(ResultsPage_TotalPayable_ForUSD).equals(reservationdetails.getBaseCurrency_NetOrgerValue())){
											
										PrintWriter.append("<td>"+reservationdetails.getBaseCurrency_NetOrgerValue()+"</td>");
										PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
									}
									
									else{
										PrintWriter.append("<td>"+reservationdetails.getBaseCurrency_NetOrgerValue()+"</td>");
										PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
									}
									
									///
									
									int tCostInUSD = (int) Math.ceil(totalCostInUsd / rateConvertforUSD );
									
									testCaseCountReservation ++;			
									PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Total cost In USD</td>");
									PrintWriter.append("<td>"+tCostInUSD+"</td>");
												
									if(Integer.toString(tCostInUSD).equals(reservationdetails.getBaseCurrency_TotalCost())){
											
										PrintWriter.append("<td>"+reservationdetails.getBaseCurrency_TotalCost()+"</td>");
										PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
									}
									
									else{
										PrintWriter.append("<td>"+reservationdetails.getBaseCurrency_TotalCost()+"</td>");
										PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
									}
									
								}
							}
						}
						
						
					}else{
						
						PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report</td>");
						PrintWriter.append("<td>Reservation report availability</td>");
						PrintWriter.append("<td>Not Available</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");		
											
					}
					
					
					
					PrintWriter.append("</table>");	
					
				}
				
				
			}
	
	}
	
	
	public void getcancellationModificationSummary(WebDriver Driver) throws IOException{
		
		mailPrinter = new StringBuffer();
		mailPrinter.append(PrintWriter);
		
		if (activitydetails.isResultsAvailable() == true) {
			
			if (search.getQuotationReq().equalsIgnoreCase("No")) {
				
				//Cancellation
				
				mailPrinter.append("<br><br>");
				mailPrinter.append("<p class='fontStyles'>Cancellation Summary</p>");
				
				testCaseCancel = 1;
				mailPrinter.append("<br><table ><tr><th>Test Case No</th><th>Test Description</th><th>Expect Values - [Excel Value]</th><th>Actual Values</th><th>Test Status</th></tr>");
				
				if (cancellationDetails.isCancelReportLoaded() == true) {
					

					mailPrinter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Reservation No</td>");
					mailPrinter.append("<td>"+activitydetails.getReservationNo()+"</td>");
					
					if(activitydetails.getReservationNo().contains(cancellationDetails.getReservationNo_1())){
						
						mailPrinter.append("<td>"+cancellationDetails.getReservationNo_1()+"</td>");
						mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						mailPrinter.append("<td>"+cancellationDetails.getReservationNo_1()+"</td>");
						mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					testCaseCancel++;
					mailPrinter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Cancellation Status</td>");
					
					if(cancellationDetails.getCancellationStatus().equals("Yes")){
						
						mailPrinter.append("<td>Yes</td>");
						mailPrinter.append("<td>"+cancellationDetails.getCancellationStatus()+"</td>");
						mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						
						mailPrinter.append("<td>No</td>");
						mailPrinter.append("<td>"+cancellationDetails.getCancellationStatus()+"</td>");
						mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					testCaseCancel++;
					mailPrinter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Cancellation Comments</td>");
					
					if(cancellationDetails.getCancellationComment().toLowerCase().contains("Cancellation can be done".toLowerCase())){
						
						mailPrinter.append("<td>Cancellation can be done</td>");
						mailPrinter.append("<td>"+cancellationDetails.getCancellationComment()+"</td>");
						mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						mailPrinter.append("<td>Cancellation cannot be done</td>");
						mailPrinter.append("<td>"+cancellationDetails.getCancellationComment()+"</td>");
						mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					
					if(cancellationDetails.getCancellationStatus().equals("Yes")){
						
						testCaseCancel++;
						mailPrinter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Customer Last Name</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_LName()+"</td>");
						
						if(activitydetails.getPaymentPage_LName().equals(cancellationDetails.getCustomerName())){
							
							mailPrinter.append("<td>"+cancellationDetails.getCustomerName()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+cancellationDetails.getCustomerName()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCancel++;
						mailPrinter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Reservation No [Cancel Reservations Page]</td>");
						mailPrinter.append("<td>"+activitydetails.getReservationNo()+"</td>");
						
						if(activitydetails.getReservationNo().contains(cancellationDetails.getRseervationNo_3())){
							
							mailPrinter.append("<td>"+cancellationDetails.getRseervationNo_3()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+cancellationDetails.getRseervationNo_3()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						testCaseCancel++;
						mailPrinter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Activity Program Name</td>");
						mailPrinter.append("<td>"+search.getActivityName()+"</td>");
						
						if(search.getActivityName().toLowerCase().contains(cancellationDetails.getProgramName_1().toLowerCase())){
							
							mailPrinter.append("<td>"+cancellationDetails.getProgramName_1()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+cancellationDetails.getProgramName_1()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						
						testCaseCancel++;
						mailPrinter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Reservation Date</td>");
						mailPrinter.append("<td>"+activitydetails.getCurrentDate()+"</td>");
						
						if(activitydetails.getCurrentDate().equals(cancellationDetails.getReservationDate())){
							
							mailPrinter.append("<td>"+cancellationDetails.getReservationDate()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+cancellationDetails.getReservationDate()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						testCaseCancel++;
						mailPrinter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Activity Program Name-2</td>");
						mailPrinter.append("<td>"+search.getActivityName()+"</td>");
						
						if(search.getActivityName().toLowerCase().contains(cancellationDetails.getProgramName_2().toLowerCase())){
							
							mailPrinter.append("<td>"+cancellationDetails.getProgramName_2()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+cancellationDetails.getProgramName_2()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						
						
						
						
						
						testCaseCancel++;
						mailPrinter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Activity QTY</td>");
						mailPrinter.append("<td>"+customerQty+"</td>");
						
						if(Integer.toString(customerQty).equals(cancellationDetails.getActivity_QTY())){
							
							mailPrinter.append("<td>"+cancellationDetails.getActivity_QTY()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+cancellationDetails.getActivity_QTY()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						///
						
						testCaseCancel++;
						mailPrinter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Activity Rate Value</td>");
						mailPrinter.append("<td>"+amount_onePerson+"</td>");
						
						if(Integer.toString(amount_onePerson).equals(cancellationDetails.getActivity_RateUSD())){
							
							mailPrinter.append("<td>"+cancellationDetails.getActivity_RateUSD()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+cancellationDetails.getActivity_RateUSD()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCancel++;
						mailPrinter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Activity Rate Currency</td>");
						mailPrinter.append("<td>"+search.getSellingCurrency()+"</td>");
						
						if(search.getSellingCurrency().toLowerCase().equals(cancellationDetails.getActivity_RateCurrency().toLowerCase())){
							
							mailPrinter.append("<td>"+cancellationDetails.getActivity_RateCurrency()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+cancellationDetails.getActivity_RateCurrency()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						////
						
						testCaseCancel++;
						mailPrinter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Total Rate Value</td>");
						mailPrinter.append("<td>"+ResultsPage_SubTotal+"</td>");
						
						if(Integer.toString(ResultsPage_SubTotal).equals(cancellationDetails.getActivity_TotalValue())){
							
							mailPrinter.append("<td>"+cancellationDetails.getActivity_TotalValue()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+cancellationDetails.getActivity_TotalValue()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCancel++;
						mailPrinter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Total Rate Currency</td>");
						mailPrinter.append("<td>"+search.getSellingCurrency()+"</td>");
						
						if(search.getSellingCurrency().toLowerCase().equals(cancellationDetails.getActivity_TotalValueCurrency().toLowerCase())){
							
							mailPrinter.append("<td>"+cancellationDetails.getActivity_TotalValueCurrency()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+cancellationDetails.getActivity_TotalValueCurrency()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCancel++;
						mailPrinter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Customer Email</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_Email()+"</td>");
						
						if(activitydetails.getPaymentPage_Email().toLowerCase().equals(cancellationDetails.getCustomer_Mail().toLowerCase())){
							
							mailPrinter.append("<td>"+cancellationDetails.getCustomer_Mail()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+cancellationDetails.getCustomer_Mail()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
//						testCaseCancel++;
//						mailPrinter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Internal Notes</td>");
//						mailPrinter.append("<td>"+activitydetails.getInternalNotes()+"</td>");
//						
//						if(cancellationDetails.getInternalNotes().toLowerCase().contains(activitydetails.getInternalNotes().toLowerCase())){
//							
//							mailPrinter.append("<td>"+cancellationDetails.getInternalNotes()+"</td>");
//							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
//						}
//						
//						else{
//							mailPrinter.append("<td>"+cancellationDetails.getInternalNotes()+"</td>");
//							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
//						}
						
					}
					
					
				} else {
					mailPrinter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Report</td>");
					mailPrinter.append("<td>Cancellation report availability</td>");
					mailPrinter.append("<td>Not Available</td>");
					mailPrinter.append("<td class='Failed'>FAIL</td><tr>");		
					
					
				}
				
				mailPrinter.append("</table>");
				
				
				
				//////////////////////////////////////////////////////////////////////////
				//Modification
				
				
				mailPrinter.append("<br><br>");
				mailPrinter.append("<p class='fontStyles'>Modification Summary</p>");
				
				testCaseModify = 1;
				mailPrinter.append("<br><table><tr><th>Test Case No</th><th>Test Description</th><th>Expect Values - [Excel Value]</th><th>Actual Values</th><th>Test Status</th></tr>");
				
							
				if (modDetails.isModReportLoaded() == true) {
					
					mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification Summary - Modification Status</td>");
					
					if(modDetails.getModificationStatus().equals("Yes")){
						
						mailPrinter.append("<td>Yes</td>");
						mailPrinter.append("<td>"+modDetails.getModificationStatus()+"</td>");
						mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						
						mailPrinter.append("<td>No</td>");
						mailPrinter.append("<td>"+modDetails.getModificationStatus()+"</td>");
						mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					testCaseModify++;
					mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification Summary - Modification Comments</td>");
					
					if(modDetails.getModificationComment().toLowerCase().contains("Modification can be done".toLowerCase())){
						
						mailPrinter.append("<td>Modification can be done</td>");
						mailPrinter.append("<td>"+modDetails.getModificationComment()+"</td>");
						mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						mailPrinter.append("<td>Modification cannot be done</td>");
						mailPrinter.append("<td>"+modDetails.getModificationComment()+"</td>");
						mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					if(modDetails.getModificationStatus().equals("Yes")){
						
						mailPrinter.append("<tr><td class='fontiiii'>Modification - Summary</td><tr>"); 
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Customer Last Name</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_LName()+"</td>");
						
						if(activitydetails.getPaymentPage_LName().equals(modDetails.getSummary_GuestLastname())){
							
							mailPrinter.append("<td>"+modDetails.getSummary_GuestLastname()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_GuestLastname()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Reservation No</td>");
						mailPrinter.append("<td>"+activitydetails.getReservationNo()+"</td>");
						
						if(activitydetails.getReservationNo().contains(modDetails.getSummary_ReservNo())){
							
							mailPrinter.append("<td>"+modDetails.getSummary_ReservNo()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_ReservNo()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Reservation No [Booking Summary]</td>");
						mailPrinter.append("<td>"+activitydetails.getReservationNo()+"</td>");
						
						if(activitydetails.getReservationNo().contains(modDetails.getSummary_reservationNo())){
							
							mailPrinter.append("<td>"+modDetails.getSummary_reservationNo()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_reservationNo()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Booking Date [Booking Summary]</td>");
						mailPrinter.append("<td>"+activitydetails.getCurrentDate()+"</td>");
						
						if(activitydetails.getCurrentDate().equals(modDetails.getSummary_reservationDate())){
							
							mailPrinter.append("<td>"+modDetails.getSummary_reservationDate()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_reservationDate()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify ++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Activity program Name [Program Summary]</td>");
						mailPrinter.append("<td>"+search.getActivityName()+"</td>");
									
						if(search.getActivityName().toLowerCase().contains(modDetails.getSummary_ProgramName().toLowerCase())){
								
							mailPrinter.append("<td>"+modDetails.getSummary_ProgramName()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							mailPrinter.append("<td>"+modDetails.getSummary_ProgramName()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						testCaseModify ++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Supplier Name [Program Summary]</td>");
						mailPrinter.append("<td>"+supplierName+"</td>");
									
						if(supplierName.toLowerCase().contains(modDetails.getSummary_SupplierName().toLowerCase())){
								
							mailPrinter.append("<td>"+modDetails.getSummary_SupplierName()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							mailPrinter.append("<td>"+modDetails.getSummary_SupplierName()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						
//						testCaseModify ++;
//						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Booking Status [Program Summary]</td>");
//						mailPrinter.append("<td>"+activitydetails.getBookingStatus()+"</td>");
//									
//						if(activitydetails.getBookingStatus().toLowerCase().contains(modDetails.getSummary_BookingStatus().toLowerCase())){
//								
//							mailPrinter.append("<td>"+modDetails.getSummary_BookingStatus()+"</td>");
//							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
//						}
//						
//						else {
//							
//							mailPrinter.append("<td>"+modDetails.getSummary_BookingStatus()+"</td>");
//							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");					
//						}
						
//						testCaseModify++;
//						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Activity Name [Activity Details]</td>");
//						mailPrinter.append("<td>"+activitydetails.getActivityName_result()+"</td>");
//						
//						if(modDetails.getSummary_activityName_2().toLowerCase().contains(activitydetails.getActivityName_result().toLowerCase())){
//							
//							mailPrinter.append("<td>"+modDetails.getSummary_activityName_2()+"</td>");
//							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
//						}
//						
//						else{
//							mailPrinter.append("<td>"+modDetails.getSummary_activityName_2()+"</td>");
//							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
//						}
						
//						testCaseModify++;
//						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Activity Rate [Activity Details]</td>");
//						mailPrinter.append("<td>"+activitydetails.getActivityRateType()+"</td>");
//						
//						if(modDetails.getSummary_ratePlan().contains(activitydetails.getActivityRateType())){
//							
//							mailPrinter.append("<td>"+modDetails.getSummary_ratePlan()+"</td>");
//							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
//						}
//						
//						else{
//							mailPrinter.append("<td>"+modDetails.getSummary_ratePlan()+"</td>");
//							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
//						}
						
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Activity Date [Activity Details]</td>");
						mailPrinter.append("<td>"+search.getActivityDate()+"</td>");
						
						if(modDetails.getSummary_Date().equals(search.getActivityDate())){
							
							mailPrinter.append("<td>"+modDetails.getSummary_Date()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_Date()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
//						testCaseModify++;
//						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Activity Session Type [Activity Details]</td>");
//						mailPrinter.append("<td>"+activitydetails.getActivitySession()+"</td>");
//						
//						if(modDetails.getSummary_Session().contains(activitydetails.getActivitySession())){
//							
//							mailPrinter.append("<td>"+modDetails.getSummary_Session()+"</td>");
//							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
//						}
//						
//						else{
//							mailPrinter.append("<td>"+modDetails.getSummary_Session()+"</td>");
//							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
//						}
						
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Activity QTY [Activity Details]</td>");
						mailPrinter.append("<td>"+customerQty+"</td>");
						
						if(Integer.toString(customerQty).equals(modDetails.getSummary_QTY())){
							
							mailPrinter.append("<td>"+modDetails.getSummary_QTY()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_QTY()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Activity Rate [Activity Details]</td>");
						mailPrinter.append("<td>"+amount_onePerson+"</td>");
						
						if(Integer.toString(amount_onePerson).equals(modDetails.getSummary_RateUSD())){
							
							mailPrinter.append("<td>"+modDetails.getSummary_RateUSD()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_RateUSD()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Activity Rate Currency [Activity Details]</td>");
						mailPrinter.append("<td>"+search.getSellingCurrency()+"</td>");
						
						if(search.getSellingCurrency().toLowerCase().equals(modDetails.getSummary_RateCurrency().toLowerCase())){
							
							mailPrinter.append("<td>"+modDetails.getSummary_RateCurrency()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_RateCurrency()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Activity Total [Activity Details]</td>");
						mailPrinter.append("<td>"+PaymentPage_SubTotal+"</td>");
						
						if(Integer.toString(PaymentPage_SubTotal).equals(modDetails.getSummary_TotalValue())){
							
							mailPrinter.append("<td>"+modDetails.getSummary_TotalValue()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_TotalValue()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Activity Total Currency [Activity Details]</td>");
						mailPrinter.append("<td>"+search.getSellingCurrency()+"</td>");
						
						if(search.getSellingCurrency().toLowerCase().equals(modDetails.getSummary_TotalValueCurrency().toLowerCase())){
							
							mailPrinter.append("<td>"+modDetails.getSummary_TotalValueCurrency()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_TotalValueCurrency()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Program Total Value [Activity Details]</td>");
						mailPrinter.append("<td>"+PaymentPage_SubTotal+"</td>");
						
						if(Integer.toString(PaymentPage_SubTotal).equals(modDetails.getSummary_ProgramTotalValue())){
							
							mailPrinter.append("<td>"+modDetails.getSummary_ProgramTotalValue()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_ProgramTotalValue()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						totalCost = netRate;
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Program Total Cost [Activity Details]</td>");
						mailPrinter.append("<td>"+totalCost+"</td>");
						
						if(Integer.toString(totalCost).equals(modDetails.getSummary_ProgramTotalCost())){
							
							mailPrinter.append("<td>"+modDetails.getSummary_ProgramTotalCost()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_ProgramTotalCost()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						///////////
						
						
						testCaseModify ++;			
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Customer Notes [Modification Details]</td>");
						mailPrinter.append("<td>"+activitydetails.getCustomerNotes()+"</td>");
									
						if(activitydetails.getCustomerNotes().equals(modDetails.getSummary_CustomerNotes())){
								
							mailPrinter.append("<td>"+modDetails.getSummary_CustomerNotes()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_CustomerNotes()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						
						
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Customer Email [Modification Details]</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_Email()+"</td>");
						
						if(activitydetails.getPaymentPage_Email().toLowerCase().equals(modDetails.getSummary_CusEmail().toLowerCase())){
							
							mailPrinter.append("<td>"+modDetails.getSummary_CusEmail()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_CusEmail()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						//////////
						
						testCaseModify ++;			
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Customer Title</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_Title()+"</td>");
									
						if(activitydetails.getPaymentPage_Title().equals(modDetails.getSummary_Title())){
								
							mailPrinter.append("<td>"+modDetails.getSummary_Title()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_Title()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify ++;			
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Customer First Name</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_FName()+"</td>");
									
						if(activitydetails.getPaymentPage_FName().equals(modDetails.getSummary_FName())){
								
							mailPrinter.append("<td>"+modDetails.getSummary_FName()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_FName()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify ++;			
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Customer Last Name</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_LName()+"</td>");
									
						if(activitydetails.getPaymentPage_LName().equals(modDetails.getSummary_LName())){
								
							mailPrinter.append("<td>"+modDetails.getSummary_LName()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_LName()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify ++;			
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Customer TP No</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_TP()+"</td>");
									
						if(activitydetails.getPaymentPage_TP().equals(modDetails.getSummary_TP())){
								
							mailPrinter.append("<td>"+modDetails.getSummary_TP()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_TP()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify ++;			
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Customer Email</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_Email()+"</td>");
									
						if(activitydetails.getPaymentPage_Email().contains(modDetails.getSummary_Email())){
								
							mailPrinter.append("<td>"+modDetails.getSummary_Email()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_Email()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify ++;			
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Customer Address</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_address()+"</td>");
									
						if(activitydetails.getPaymentPage_address().equals(modDetails.getSummary_address())){
								
							mailPrinter.append("<td>"+modDetails.getSummary_address()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_address()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
//						testCaseModify ++;			
//						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Customer Address 2</td>");
//						mailPrinter.append("<td>"+activitydetails.getPaymentPage_address_2()+"</td>");
//									
//						if(activitydetails.getPaymentPage_address_2().equals(modDetails.getSummary_address_2())){
//								
//							mailPrinter.append("<td>"+modDetails.getSummary_address_2()+"</td>");
//							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
//						}
//						
//						else{
//							mailPrinter.append("<td>"+modDetails.getSummary_address_2()+"</td>");
//							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
//						}
						
						testCaseModify ++;			
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Customer Country</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_country()+"</td>");
									
						if(activitydetails.getPaymentPage_country().equals(modDetails.getSummary_country())){
								
							mailPrinter.append("<td>"+modDetails.getSummary_country()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_country()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify ++;			
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Customer City</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_city()+"</td>");
									
						if(activitydetails.getPaymentPage_city().equals(modDetails.getSummary_city())){
								
							mailPrinter.append("<td>"+modDetails.getSummary_city()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_city()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
//						if (activitydetails.getConfirmationPage_country().equals("USA")) {
//							
//							testCaseModify ++;			
//							mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Customer State</td>");
//							mailPrinter.append("<td>"+activitydetails.getConfirmationPage_state()+"</td>");
//										
//							if(activitydetails.getPaymentPage_state().equals(modDetails.getSummary_state())){
//									
//								mailPrinter.append("<td>"+modDetails.getSummary_state()+"</td>");
//								mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
//							}
//							
//							else{
//								mailPrinter.append("<td>"+modDetails.getSummary_state()+"</td>");
//								mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
//							}
//						
//						}
//						
//						if (activitydetails.getConfirmationPage_country().equals("Canada")) {
//							
//							testCaseModify ++;			
//							mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Customer State</td>");
//							mailPrinter.append("<td>"+activitydetails.getConfirmationPage_state()+"</td>");
//										
//							if(activitydetails.getPaymentPage_state().equals(modDetails.getSummary_state())){
//									
//								mailPrinter.append("<td>"+modDetails.getSummary_state()+"</td>");
//								mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
//							}
//							
//							else{
//								mailPrinter.append("<td>"+modDetails.getSummary_state()+"</td>");
//								mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
//							}
//							
//						}
//						
//						if (activitydetails.getConfirmationPage_country().equals("Australia")) {
//							
//							testCaseModify ++;			
//							mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Customer State</td>");
//							mailPrinter.append("<td>"+activitydetails.getConfirmationPage_state()+"</td>");
//										
//							if(activitydetails.getPaymentPage_state().equals(modDetails.getSummary_state())){
//									
//								mailPrinter.append("<td>"+modDetails.getSummary_state()+"</td>");
//								mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
//							}
//							
//							else{
//								mailPrinter.append("<td>"+modDetails.getSummary_state()+"</td>");
//								mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
//							}
//						
//						}
						
						
						
						//Manager override
						
						mailPrinter.append("<tr><td class='fontiiii'>Modification - Manager override</td><tr>"); 
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Manager override - Customer Last Name</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_LName()+"</td>");
						
						if(activitydetails.getPaymentPage_LName().equals(modDetails.getMO_guestLastName())){
							
							mailPrinter.append("<td>"+modDetails.getMO_guestLastName()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getMO_guestLastName()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Manager override - Reservation No</td>");
						mailPrinter.append("<td>"+activitydetails.getReservationNo()+"</td>");
						
						if(activitydetails.getReservationNo().contains(modDetails.getMO_reservationNo_1())){
							
							mailPrinter.append("<td>"+modDetails.getMO_reservationNo_1()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getMO_reservationNo_1()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify ++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Manager override - Activity program Name [Program Summary]</td>");
						mailPrinter.append("<td>"+search.getActivityName()+"</td>");
									
						if(search.getActivityName().toLowerCase().contains(modDetails.getMO_programName_1().toLowerCase())){
								
							mailPrinter.append("<td>"+modDetails.getMO_programName_1()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							mailPrinter.append("<td>"+modDetails.getMO_programName_1()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						
//						testCaseModify++;
//						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Manager override - Activity Name [Activity Details]</td>");
//						mailPrinter.append("<td>"+activitydetails.getActivityName_result()+"</td>");
//						
//						if(modDetails.getMO_activityName_1().toLowerCase().contains(activitydetails.getActivityName_result().toLowerCase())){
//							
//							mailPrinter.append("<td>"+modDetails.getMO_activityName_1()+"</td>");
//							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
//						}
//						
//						else{
//							mailPrinter.append("<td>"+modDetails.getMO_activityName_1()+"</td>");
//							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
//						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Manager override - Reservation Date [Activity Details]</td>");
						mailPrinter.append("<td>"+search.getActivityDate()+"</td>");
						
						if(search.getActivityDate().equals(modDetails.getMO_activityDate())){
							
							mailPrinter.append("<td>"+modDetails.getMO_activityDate()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getMO_activityDate()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
//						testCaseModify++;
//						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Manager override - Activity Rate [Activity Details]</td>");
//						mailPrinter.append("<td>"+activitydetails.getActivityRateType()+"</td>");
//						
//						if(modDetails.getMO_rate().contains(activitydetails.getActivityRateType())){
//							
//							mailPrinter.append("<td>"+modDetails.getMO_rate()+"</td>");
//							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
//						}
//						
//						else{
//							mailPrinter.append("<td>"+modDetails.getMO_rate()+"</td>");
//							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
//						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Manager override - Activity QTY [Activity Details]</td>");
						mailPrinter.append("<td>"+customerQty+"</td>");
						
						if(Integer.toString(customerQty).equals(modDetails.getMO_qty())){
							
							mailPrinter.append("<td>"+modDetails.getMO_qty()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getMO_qty()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Manager override - Activity Rate [Activity Details]</td>");
						mailPrinter.append("<td>"+amount_onePerson+"</td>");
						
						if(Integer.toString(amount_onePerson).equals(modDetails.getMO_activityRate())){
							
							mailPrinter.append("<td>"+modDetails.getMO_activityRate()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getMO_activityRate()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Manager override - Activity Rate Currency [Activity Details]</td>");
						mailPrinter.append("<td>"+search.getSellingCurrency()+"</td>");
						
						if(search.getSellingCurrency().toLowerCase().equals(modDetails.getMO_activityRateCurrency().toLowerCase())){
							
							mailPrinter.append("<td>"+modDetails.getMO_activityRateCurrency()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getMO_activityRateCurrency()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Manager override - Activity Total [Activity Details]</td>");
						mailPrinter.append("<td>"+PaymentPage_SubTotal+"</td>");
						
						if(Integer.toString(PaymentPage_SubTotal).equals(modDetails.getMO_Total())){
							
							mailPrinter.append("<td>"+modDetails.getMO_Total()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getMO_Total()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Manager override - Activity Total Currency [Activity Details]</td>");
						mailPrinter.append("<td>"+search.getSellingCurrency()+"</td>");
						
						if(search.getSellingCurrency().toLowerCase().equals(modDetails.getMO_TotalCurrency().toLowerCase())){
							
							mailPrinter.append("<td>"+modDetails.getMO_TotalCurrency()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getMO_TotalCurrency()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Manager override - Total Booking value [Activity Details]</td>");
						mailPrinter.append("<td>"+(PaymentPage_SubTotal - discountedVale)+"</td>");
						
						if(Integer.toString(PaymentPage_SubTotal - discountedVale).equals(modDetails.getMO_TotalBookingValue())){
							
							mailPrinter.append("<td>"+modDetails.getMO_TotalBookingValue()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getMO_TotalBookingValue()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Manager override - Total Cost [Activity Details]</td>");
						mailPrinter.append("<td>"+totalCost+"</td>");
						
						if(Integer.toString(totalCost).equals(modDetails.getMO_TotalCost())){
							
							mailPrinter.append("<td>"+modDetails.getMO_TotalCost()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getMO_TotalCost()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
//						testCaseModify++;
//						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Manager override - Activity Name [Manager Overwrite]</td>");
//						mailPrinter.append("<td>"+activitydetails.getActivityName_result()+"</td>");
//						
//						if(modDetails.getMO_ManagerActivityName_1().toLowerCase().contains(activitydetails.getActivityName_result().toLowerCase())){
//							
//							mailPrinter.append("<td>"+modDetails.getMO_ManagerActivityName_1()+"</td>");
//							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
//						}
//						
//						else{
//							mailPrinter.append("<td>"+modDetails.getMO_ManagerActivityName_1()+"</td>");
//							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
//						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Manager override - Reservation Date [Manager Overwrite]</td>");
						mailPrinter.append("<td>"+search.getActivityDate()+"</td>");
						
						if(search.getActivityDate().equals(modDetails.getMO_ManagerActivityDate())){
							
							mailPrinter.append("<td>"+modDetails.getMO_ManagerActivityDate()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getMO_ManagerActivityDate()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
//						testCaseModify++;
//						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Manager override - Activity Rate [Manager Overwrite]</td>");
//						mailPrinter.append("<td>"+activitydetails.getActivityRateType()+"</td>");
//						
//						if(modDetails.getMO_ManagerRate().contains(activitydetails.getActivityRateType())){
//							
//							mailPrinter.append("<td>"+modDetails.getMO_ManagerRate()+"</td>");
//							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
//						}
//						
//						else{
//							mailPrinter.append("<td>"+modDetails.getMO_ManagerRate()+"</td>");
//							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
//						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Manager override - Daily Rate Currency [Manager Overwrite]</td>");
						mailPrinter.append("<td>"+search.getSellingCurrency()+"</td>");
						
						if(search.getSellingCurrency().toLowerCase().equals(modDetails.getMO_DailyRateCurrency().toLowerCase())){
							
							mailPrinter.append("<td>"+modDetails.getMO_DailyRateCurrency()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getMO_DailyRateCurrency()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Manager override - New Total Booking Value [Manager Overwrite]</td>");
						mailPrinter.append("<td>"+PaymentPage_SubTotal+"</td>");
						
						if(Integer.toString(PaymentPage_SubTotal).equals(modDetails.getMO_NewTotalBookingValue())){
							
							mailPrinter.append("<td>"+modDetails.getMO_NewTotalBookingValue()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getMO_NewTotalBookingValue()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Manager override - New Total Booking Cost [Manager Overwrite]</td>");
						mailPrinter.append("<td>"+totalCost+"</td>");
						
						if(Integer.toString(totalCost).equals(modDetails.getMO_NewTotalBookingCost())){
							
							mailPrinter.append("<td>"+modDetails.getMO_NewTotalBookingCost()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getMO_NewTotalBookingCost()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						//Remove Activity
						mailPrinter.append("<tr><td class='fontiiii'>Modification - Remove Activity</td><tr>"); 
						
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Remove Activity - Customer Last Name</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_LName()+"</td>");
						
						if(activitydetails.getPaymentPage_LName().equals(modDetails.getRA_guestLastName())){
							
							mailPrinter.append("<td>"+modDetails.getRA_guestLastName()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getRA_guestLastName()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Remove Activity - Reservation No</td>");
						mailPrinter.append("<td>"+activitydetails.getReservationNo()+"</td>");
						
						if(activitydetails.getReservationNo().contains(modDetails.getRA_reservationNo_1())){
							
							mailPrinter.append("<td>"+modDetails.getRA_reservationNo_1()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getRA_reservationNo_1()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						///
						
						testCaseModify ++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Remove Activity - Activity program Name [Program Summary]</td>");
						mailPrinter.append("<td>"+search.getActivityName()+"</td>");
									
						if(search.getActivityName().toLowerCase().contains(modDetails.getRA_programName_1().toLowerCase())){
								
							mailPrinter.append("<td>"+modDetails.getRA_programName_1()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							mailPrinter.append("<td>"+modDetails.getRA_programName_1()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						
//						testCaseModify++;
//						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Remove Activity - Activity Name [Program Summary]</td>");
//						mailPrinter.append("<td>"+activitydetails.getActivityName_result()+"</td>");
//						
//						if(modDetails.getRA_activityName_1().toLowerCase().contains(activitydetails.getActivityName_result().toLowerCase())){
//							
//							mailPrinter.append("<td>"+modDetails.getRA_activityName_1()+"</td>");
//							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
//						}
//						
//						else{
//							mailPrinter.append("<td>"+modDetails.getRA_activityName_1()+"</td>");
//							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
//						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Remove Activity - Reservation Date [Program Summary]</td>");
						mailPrinter.append("<td>"+search.getActivityDate()+"</td>");
						
						if(search.getActivityDate().equals(modDetails.getRA_activityDate())){
							
							mailPrinter.append("<td>"+modDetails.getRA_activityDate()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getRA_activityDate()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
//						testCaseModify++;
//						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Remove Activity - Activity Session [Program Summary]</td>");
//						mailPrinter.append("<td>"+activitydetails.getActivitySession()+"</td>");
//						
//						if(modDetails.getRA_session().contains(activitydetails.getActivitySession())){
//							
//							mailPrinter.append("<td>"+modDetails.getRA_session()+"</td>");
//							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
//						}
//						
//						else{
//							mailPrinter.append("<td>"+modDetails.getRA_session()+"</td>");
//							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
//						}
						
//						testCaseModify++;
//						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Remove Activity - Activity Rate [Program Summary]</td>");
//						mailPrinter.append("<td>"+activitydetails.getActivityRateType()+"</td>");
//						
//						if(modDetails.getRA_rate().contains(activitydetails.getActivityRateType())){
//							
//							mailPrinter.append("<td>"+modDetails.getRA_rate()+"</td>");
//							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
//						}
//						
//						else{
//							mailPrinter.append("<td>"+modDetails.getRA_rate()+"</td>");
//							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
//						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Remove Activity - Activity QTY [Program Summary]</td>");
						mailPrinter.append("<td>"+customerQty+"</td>");
						
						if(Integer.toString(customerQty).equals(modDetails.getRA_qty())){
							
							mailPrinter.append("<td>"+modDetails.getRA_qty()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getRA_qty()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Remove Activity - Activity Rate [Program Summary]</td>");
						mailPrinter.append("<td>"+amount_onePerson+"</td>");
						
						if(Integer.toString(amount_onePerson).equals(modDetails.getRA_activityRate())){
							
							mailPrinter.append("<td>"+modDetails.getRA_activityRate()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getRA_activityRate()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Remove Activity - Activity Rate Currency [Program Summary]</td>");
						mailPrinter.append("<td>"+search.getSellingCurrency()+"</td>");
						
						if(search.getSellingCurrency().toLowerCase().equals(modDetails.getRA_activityRateCurrency().toLowerCase())){
							
							mailPrinter.append("<td>"+modDetails.getRA_activityRateCurrency()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getRA_activityRateCurrency()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Remove Activity - Activity Total [Program Summary]</td>");
						mailPrinter.append("<td>"+PaymentPage_SubTotal+"</td>");
						
						if(Integer.toString(PaymentPage_SubTotal).equals(modDetails.getRA_Total())){
							
							mailPrinter.append("<td>"+modDetails.getRA_Total()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getRA_Total()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Remove Activity - Activity Total Currency [Program Summary]</td>");
						mailPrinter.append("<td>"+search.getSellingCurrency()+"</td>");
						
						if(search.getSellingCurrency().toLowerCase().equals(modDetails.getRA_TotalCurrency().toLowerCase())){
							
							mailPrinter.append("<td>"+modDetails.getRA_TotalCurrency()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getRA_TotalCurrency()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						

						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Remove Activity - Total Booking value [Program Summary]</td>");
						mailPrinter.append("<td>"+(PaymentPage_SubTotal - discountedVale)+"</td>");
						
						if(Integer.toString(PaymentPage_SubTotal - discountedVale).equals(modDetails.getRA_TotalBookingValue())){
							
							mailPrinter.append("<td>"+modDetails.getRA_TotalBookingValue()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getRA_TotalBookingValue()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Remove Activity - Total Cost [Program Summary]</td>");
						mailPrinter.append("<td>"+totalCost+"</td>");
						
						if(Integer.toString(totalCost).equals(modDetails.getRA_TotalCost())){
							
							mailPrinter.append("<td>"+modDetails.getRA_TotalCost()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getRA_TotalCost()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						//
						
						mailPrinter.append("<tr><td class='fontiiii'>Modification - Add Activity</td><tr>"); 
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Add Activity - Customer Last Name</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_LName()+"</td>");
						
						if(activitydetails.getPaymentPage_LName().equals(modDetails.getAA_guestLastName())){
							
							mailPrinter.append("<td>"+modDetails.getAA_guestLastName()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getAA_guestLastName()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Add Activity - Reservation No</td>");
						mailPrinter.append("<td>"+activitydetails.getReservationNo()+"</td>");
						
						if(activitydetails.getReservationNo().contains(modDetails.getAA_reservationNo_1())){
							
							mailPrinter.append("<td>"+modDetails.getAA_reservationNo_1()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getAA_reservationNo_1()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						///
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Add Activity - Current Date</td>");
						mailPrinter.append("<td>"+activitydetails.getCurrentDate()+"</td>");
						
						if(activitydetails.getCurrentDate().equals(modDetails.getAA_ReservationDate())){
							
							mailPrinter.append("<td>"+modDetails.getAA_ReservationDate()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getAA_ReservationDate()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						///
						
						testCaseModify ++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Add Activity - Activity program Name [Program Summary]</td>");
						mailPrinter.append("<td>"+search.getActivityName()+"</td>");
									
						if(search.getActivityName().toLowerCase().contains(modDetails.getAA_programName_1().toLowerCase())){
								
							mailPrinter.append("<td>"+modDetails.getAA_programName_1()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							mailPrinter.append("<td>"+modDetails.getAA_programName_1()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						
//						testCaseModify++;
//						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Add Activity - Activity Name [Program Summary]</td>");
//						mailPrinter.append("<td>"+activitydetails.getActivityName_result()+"</td>");
//						
//						if(modDetails.getAA_activityName_1().toLowerCase().contains(activitydetails.getActivityName_result().toLowerCase())){
//							
//							mailPrinter.append("<td>"+modDetails.getAA_activityName_1()+"</td>");
//							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
//						}
//						
//						else{
//							mailPrinter.append("<td>"+modDetails.getAA_activityName_1()+"</td>");
//							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
//						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Add Activity - Reservation Date [Program Summary]</td>");
						mailPrinter.append("<td>"+search.getActivityDate()+"</td>");
						
						if(search.getActivityDate().equals(modDetails.getAA_activityDate())){
							
							mailPrinter.append("<td>"+modDetails.getAA_activityDate()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getAA_activityDate()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
//						testCaseModify++;
//						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Add Activity - Activity Session [Program Summary]</td>");
//						mailPrinter.append("<td>"+activitydetails.getActivitySession()+"</td>");
//						
//						if(modDetails.getAA_session().contains(activitydetails.getActivitySession())){
//							
//							mailPrinter.append("<td>"+modDetails.getAA_session()+"</td>");
//							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
//						}
//						
//						else{
//							mailPrinter.append("<td>"+modDetails.getAA_session()+"</td>");
//							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
//						}
						
//						testCaseModify++;
//						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Add Activity - Activity Rate [Program Summary]</td>");
//						mailPrinter.append("<td>"+activitydetails.getActivityRateType()+"</td>");
//						
//						if(modDetails.getAA_rate().contains(activitydetails.getActivityRateType())){
//							
//							mailPrinter.append("<td>"+modDetails.getAA_rate()+"</td>");
//							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
//						}
//						
//						else{
//							mailPrinter.append("<td>"+modDetails.getAA_rate()+"</td>");
//							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
//						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Add Activity - Activity QTY [Program Summary]</td>");
						mailPrinter.append("<td>"+customerQty+"</td>");
						
						if(Integer.toString(customerQty).equals(modDetails.getAA_qty())){
							
							mailPrinter.append("<td>"+modDetails.getAA_qty()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getAA_qty()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Add Activity - Activity Rate [Program Summary]</td>");
						mailPrinter.append("<td>"+amount_onePerson+"</td>");
						
						if(Integer.toString(amount_onePerson).equals(modDetails.getAA_activityRate())){
							
							mailPrinter.append("<td>"+modDetails.getAA_activityRate()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getAA_activityRate()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Add Activity - Activity Rate Currency [Program Summary]</td>");
						mailPrinter.append("<td>"+search.getSellingCurrency()+"</td>");
						
						if(search.getSellingCurrency().toLowerCase().equals(modDetails.getAA_activityRateCurrency().toLowerCase())){
							
							mailPrinter.append("<td>"+modDetails.getAA_activityRateCurrency()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getAA_activityRateCurrency()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Add Activity - Activity Total [Program Summary]</td>");
						mailPrinter.append("<td>"+PaymentPage_SubTotal+"</td>");
						
						if(Integer.toString(PaymentPage_SubTotal).equals(modDetails.getAA_Total())){
							
							mailPrinter.append("<td>"+modDetails.getAA_Total()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getAA_Total()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Add Activity - Activity Total Currency [Program Summary]</td>");
						mailPrinter.append("<td>"+search.getSellingCurrency()+"</td>");
						
						if(search.getSellingCurrency().toLowerCase().equals(modDetails.getAA_TotalCurrency().toLowerCase())){
							
							mailPrinter.append("<td>"+modDetails.getAA_TotalCurrency()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getAA_TotalCurrency()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						

						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Add Activity - Total Booking value [Program Summary]</td>");
						mailPrinter.append("<td>"+(PaymentPage_SubTotal - discountedVale)+"</td>");
						
						if(Integer.toString(PaymentPage_SubTotal - discountedVale).equals(modDetails.getAA_TotalBookingValue())){
							
							mailPrinter.append("<td>"+modDetails.getAA_TotalBookingValue()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getAA_TotalBookingValue()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Add Activity - Total Cost [Program Summary]</td>");
						mailPrinter.append("<td>"+totalCost+"</td>");
						
						if(Integer.toString(totalCost).equals(modDetails.getAA_TotalCost())){
							
							mailPrinter.append("<td>"+modDetails.getAA_TotalCost()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getAA_TotalCost()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						mailPrinter.append("<tr><td class='fontiiii'>Modification - Activity Date/Qty</td><tr>"); 
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Activity Date/Qty - Customer Last Name</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_LName()+"</td>");
						
						if(activitydetails.getPaymentPage_LName().equals(modDetails.getADQ_guestLastName())){
							
							mailPrinter.append("<td>"+modDetails.getADQ_guestLastName()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getADQ_guestLastName()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Activity Date/Qty - Reservation No</td>");
						mailPrinter.append("<td>"+activitydetails.getReservationNo()+"</td>");
						
						if(activitydetails.getReservationNo().contains(modDetails.getADQ_reservationNo_1())){
							
							mailPrinter.append("<td>"+modDetails.getADQ_reservationNo_1()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getADQ_reservationNo_1()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						///
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Activity Date/Qty - Current Date</td>");
						mailPrinter.append("<td>"+activitydetails.getCurrentDate()+"</td>");
						
						if(activitydetails.getCurrentDate().equals(modDetails.getADQ_ReservationDate())){
							
							mailPrinter.append("<td>"+modDetails.getADQ_ReservationDate()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getADQ_ReservationDate()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						///
						
						testCaseModify ++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Activity Date/Qty - Activity program Name [Program Summary]</td>");
						mailPrinter.append("<td>"+search.getActivityName()+"</td>");
									
						if(search.getActivityName().toLowerCase().contains(modDetails.getADQ_programName_1().toLowerCase())){
								
							mailPrinter.append("<td>"+modDetails.getADQ_programName_1()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							mailPrinter.append("<td>"+modDetails.getADQ_programName_1()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						
						testCaseModify ++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Activity Date/Qty - Supplier Name [Program Summary]</td>");
						mailPrinter.append("<td>"+supplierName+"</td>");
									
						if(supplierName.toLowerCase().contains(modDetails.getADQ_SupplierName().toLowerCase())){
								
							mailPrinter.append("<td>"+modDetails.getADQ_SupplierName()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							mailPrinter.append("<td>"+modDetails.getADQ_SupplierName()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						
//						testCaseModify++;
//						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Activity Date/Qty - Activity Name [Program Summary]</td>");
//						mailPrinter.append("<td>"+activitydetails.getActivityName_result()+"</td>");
//						
//						if(modDetails.getADQ_activityName_1().toLowerCase().contains(activitydetails.getActivityName_result().toLowerCase())){
//							
//							mailPrinter.append("<td>"+modDetails.getADQ_activityName_1()+"</td>");
//							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
//						}
//						
//						else{
//							mailPrinter.append("<td>"+modDetails.getADQ_activityName_1()+"</td>");
//							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
//						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Activity Date/Qty - Reservation Date [Program Summary]</td>");
						mailPrinter.append("<td>"+search.getActivityDate()+"</td>");
						
						if(search.getActivityDate().equals(modDetails.getADQ_activityDate())){
							
							mailPrinter.append("<td>"+modDetails.getADQ_activityDate()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getADQ_activityDate()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
//						testCaseModify++;
//						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Activity Date/Qty - Activity Session [Program Summary]</td>");
//						mailPrinter.append("<td>"+activitydetails.getActivitySession()+"</td>");
//						
//						if(modDetails.getADQ_session().contains(activitydetails.getActivitySession())){
//							
//							mailPrinter.append("<td>"+modDetails.getADQ_session()+"</td>");
//							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
//						}
//						
//						else{
//							mailPrinter.append("<td>"+modDetails.getADQ_session()+"</td>");
//							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
//						}
						
//						testCaseModify++;
//						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Activity Date/Qty - Activity Rate [Program Summary]</td>");
//						mailPrinter.append("<td>"+activitydetails.getActivityRateType()+"</td>");
//						
//						if(modDetails.getADQ_rate().contains(activitydetails.getActivityRateType())){
//							
//							mailPrinter.append("<td>"+modDetails.getADQ_rate()+"</td>");
//							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
//						}
//						
//						else{
//							mailPrinter.append("<td>"+modDetails.getADQ_rate()+"</td>");
//							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
//						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Activity Date/Qty - Activity QTY [Program Summary]</td>");
						mailPrinter.append("<td>"+customerQty+"</td>");
						
						if(Integer.toString(customerQty).equals(modDetails.getADQ_qty())){
							
							mailPrinter.append("<td>"+modDetails.getADQ_qty()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getADQ_qty()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Activity Date/Qty - Activity Rate [Program Summary]</td>");
						mailPrinter.append("<td>"+amount_onePerson+"</td>");
						
						if(Integer.toString(amount_onePerson).equals(modDetails.getADQ_activityRate())){
							
							mailPrinter.append("<td>"+modDetails.getADQ_activityRate()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getADQ_activityRate()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Activity Date/Qty - Activity Rate Currency [Program Summary]</td>");
						mailPrinter.append("<td>"+search.getSellingCurrency()+"</td>");
						
						if(search.getSellingCurrency().toLowerCase().equals(modDetails.getADQ_activityRateCurrency().toLowerCase())){
							
							mailPrinter.append("<td>"+modDetails.getADQ_activityRateCurrency()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getADQ_activityRateCurrency()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Activity Date/Qty - Activity Total [Program Summary]</td>");
						mailPrinter.append("<td>"+PaymentPage_SubTotal+"</td>");
						
						if(Integer.toString(PaymentPage_SubTotal).equals(modDetails.getADQ_Total())){
							
							mailPrinter.append("<td>"+modDetails.getADQ_Total()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getADQ_Total()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Modification - Activity Date/Qty - Total Booking value [Program Summary] - Activity Total Currency [Program Summary]</td>");
						mailPrinter.append("<td>"+search.getSellingCurrency()+"</td>");
						
						if(search.getSellingCurrency().toLowerCase().equals(modDetails.getADQ_TotalCurrency().toLowerCase())){
							
							mailPrinter.append("<td>"+modDetails.getADQ_TotalCurrency()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getADQ_TotalCurrency()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						

						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Activity Date/Qty - Total Booking value [Program Summary]</td>");
						mailPrinter.append("<td>"+(PaymentPage_SubTotal - discountedVale)+"</td>");
						
						if(Integer.toString(PaymentPage_SubTotal - discountedVale).equals(modDetails.getADQ_TotalBookingValue())){
							
							mailPrinter.append("<td>"+modDetails.getADQ_TotalBookingValue()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getADQ_TotalBookingValue()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Activity Date/Qty - Total Cost [Program Summary]</td>");
						mailPrinter.append("<td>"+totalCost+"</td>");
						
						if(Integer.toString(totalCost).equals(modDetails.getADQ_TotalCost())){
							
							mailPrinter.append("<td>"+modDetails.getADQ_TotalCost()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getADQ_TotalCost()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						
						mailPrinter.append("<tr><td class='fontiiii'>Modification - Guest Details</td><tr>"); 
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Guest Details - Customer Last Name</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_LName()+"</td>");
						
						if(activitydetails.getPaymentPage_LName().equals(modDetails.getGD_guestLastName())){
							
							mailPrinter.append("<td>"+modDetails.getGD_guestLastName()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getGD_guestLastName()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Guest Details - Reservation No</td>");
						mailPrinter.append("<td>"+activitydetails.getReservationNo()+"</td>");
						
						if(activitydetails.getReservationNo().contains(modDetails.getGD_reservationNo_1())){
							
							mailPrinter.append("<td>"+modDetails.getGD_reservationNo_1()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getGD_reservationNo_1()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						
						testCaseModify ++;			
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Guest Details - Customer Title</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_Title()+"</td>");
									
						if(modDetails.getGD_Title().contains(activitydetails.getPaymentPage_Title())){
								
							mailPrinter.append("<td>"+modDetails.getGD_Title()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getGD_Title()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify ++;			
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Guest Details - Customer First Name</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_FName()+"</td>");
									
						if(activitydetails.getPaymentPage_FName().equals(modDetails.getGD_FName())){
								
							mailPrinter.append("<td>"+modDetails.getGD_FName()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getGD_FName()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify ++;			
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Guest Details - Customer Last Name</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_LName()+"</td>");
									
						if(activitydetails.getPaymentPage_LName().equals(modDetails.getGD_LName())){
								
							mailPrinter.append("<td>"+modDetails.getGD_LName()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getGD_LName()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify ++;			
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Guest Details - Customer TP No</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_TP()+"</td>");
									
						if(activitydetails.getPaymentPage_TP().equals(modDetails.getGD_TP())){
								
							mailPrinter.append("<td>"+modDetails.getGD_TP()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getGD_TP()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify ++;			
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Guest Details - Customer Email</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_Email()+"</td>");
									
						if(activitydetails.getPaymentPage_Email().contains(modDetails.getGD_Email())){
								
							mailPrinter.append("<td>"+modDetails.getGD_Email()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getGD_Email()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify ++;			
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Guest Details - Customer Address</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_address()+"</td>");
									
						if(activitydetails.getPaymentPage_address().equals(modDetails.getGD_address())){
								
							mailPrinter.append("<td>"+modDetails.getGD_address()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getGD_address()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
//						testCaseModify ++;			
//						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Guest Details - Customer Address 2</td>");
//						mailPrinter.append("<td>"+activitydetails.getPaymentPage_address_2()+"</td>");
//									
//						if(activitydetails.getPaymentPage_address_2().equals(modDetails.getGD_address_2())){
//								
//							mailPrinter.append("<td>"+modDetails.getGD_address_2()+"</td>");
//							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
//						}
//						
//						else{
//							mailPrinter.append("<td>"+modDetails.getGD_address_2()+"</td>");
//							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
//						}
						
						testCaseModify ++;			
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Guest Details - Customer Country</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_country()+"</td>");
									
						if(activitydetails.getPaymentPage_country().equals(modDetails.getGD_country())){
								
							mailPrinter.append("<td>"+modDetails.getGD_country()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getGD_country()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify ++;			
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Guest Details - Customer City</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_city()+"</td>");
									
						if(activitydetails.getPaymentPage_city().equals(modDetails.getGD_city())){
								
							mailPrinter.append("<td>"+modDetails.getGD_city()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getGD_city()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
//						if (activitydetails.getConfirmationPage_country().equals("USA")) {
//							
//							testCaseModify ++;			
//							mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Guest Details - Customer State</td>");
//							mailPrinter.append("<td>"+activitydetails.getConfirmationPage_state()+"</td>");
//										
//							if(activitydetails.getPaymentPage_state().equals(modDetails.getGD_state())){
//									
//								mailPrinter.append("<td>"+modDetails.getGD_state()+"</td>");
//								mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
//							}
//							
//							else{
//								mailPrinter.append("<td>"+modDetails.getGD_state()+"</td>");
//								mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
//							}
//						
//						}
//						
//						if (activitydetails.getConfirmationPage_country().equals("Canada")) {
//							
//							testCaseModify ++;			
//							mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Guest Details - Customer State</td>");
//							mailPrinter.append("<td>"+activitydetails.getConfirmationPage_state()+"</td>");
//										
//							if(activitydetails.getPaymentPage_state().equals(modDetails.getGD_state())){
//									
//								mailPrinter.append("<td>"+modDetails.getGD_state()+"</td>");
//								mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
//							}
//							
//							else{
//								mailPrinter.append("<td>"+modDetails.getGD_state()+"</td>");
//								mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
//							}
//							
//						}
//						
//						if (activitydetails.getConfirmationPage_country().equals("Australia")) {
//							
//							testCaseModify ++;			
//							mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Guest Details - Customer State</td>");
//							mailPrinter.append("<td>"+activitydetails.getConfirmationPage_state()+"</td>");
//										
//							if(activitydetails.getPaymentPage_state().equals(modDetails.getGD_state())){
//									
//								mailPrinter.append("<td>"+modDetails.getGD_state()+"</td>");
//								mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
//							}
//							
//							else{
//								mailPrinter.append("<td>"+modDetails.getGD_state()+"</td>");
//								mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
//							}
//						
//						}
							
					}
					
				} else {
				
					
					mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification Summary - Modification Status</td>");
					mailPrinter.append("<td>Modification report availability</td>");
					mailPrinter.append("<td>Not Available</td>");
					mailPrinter.append("<td class='Failed'>FAIL</td><tr>");		
										
				}
				
				
				mailPrinter.append("</table>");
				
				
				
//				//Inventory Report
//				
//				mailPrinter.append("<br><br>");
//				mailPrinter.append("<p class='fontStyles'>Inventory Report Summary</p>");
//				
//				testCaseInventory = 1;
//				mailPrinter.append("<br><table><tr><th>Test Case No</th><th>Test Description</th><th>Expect Values - [Excel Value]</th><th>Actual Values</th><th>Test Status</th></tr>");
//				
//				mailPrinter.append("<tr><td>"+testCaseInventory+"</td> <td>Inventory Report - Reservation Date</td>");
//				mailPrinter.append("<td>"+search.getActivityDate()+"</td>");
//				
//				if(search.getActivityDate().equals(inventoryDetails.getActivityDate())){
//					
//					mailPrinter.append("<td>"+inventoryDetails.getActivityDate()+"</td>");
//					mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
//				}
//				
//				else{
//					mailPrinter.append("<td>"+inventoryDetails.getActivityDate()+"</td>");
//					mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
//				}
//				
//				int totalInventory = Integer.parseInt(inventoryDetails.getTotalInventory());
//				int inventoryCount = Integer.parseInt(inventory.getInventoryCount()); 
//				
//				int bookedBefore = Integer.parseInt(inventoryDetails.getBookedInventory().get(0));
//				int bookedAfter = Integer.parseInt(inventoryDetails.getBookedInventory().get(1));
//				
//				int availableBefore = Integer.parseInt(inventoryDetails.getAvailableInventory().get(0));
//				int availableAfter = Integer.parseInt(inventoryDetails.getAvailableInventory().get(1));
//				
//				int newBookedAfter = bookedBefore + inventoryCount;
//				int newavailableAfter = availableBefore - inventoryCount;
//				
//				
//				testCaseInventory++;
//				mailPrinter.append("<tr><td>"+testCaseInventory+"</td> <td>Inventory Report - Booked Inventory</td>");
//				mailPrinter.append("<td>"+newBookedAfter+"</td>");
//				
//				if(newBookedAfter == bookedAfter){
//					
//					mailPrinter.append("<td>"+bookedAfter+"</td>");
//					mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
//				}
//				
//				else{
//					mailPrinter.append("<td>"+bookedAfter+"</td>");
//					mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
//				}
//				
//				testCaseInventory++;
//				mailPrinter.append("<tr><td>"+testCaseInventory+"</td> <td>Inventory Report - Available Inventory</td>");
//				mailPrinter.append("<td>"+newavailableAfter+"</td>");
//				
//				if(newavailableAfter == availableAfter){
//					
//					mailPrinter.append("<td>"+availableAfter+"</td>");
//					mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
//				}
//				
//				else{
//					mailPrinter.append("<td>"+availableAfter+"</td>");
//					mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
//				}
//			
	//
//				mailPrinter.append("</table>");
				
				
				
				/////
				
				mailPrinter.append("<br><br>");
				mailPrinter.append("<p class='fontStyles'>Response Times</p>");
				
				long timeDiff2 = activitydetails.getTimeAddedtoCart() - activitydetails.getTimeLoadpayment();
				long timeDiff3 = activitydetails.getTimeConfirmBooking() - activitydetails.getTimeAailableClick();
				long timeDiff4 = activitydetails.getTimeFirstResults() - activitydetails.getTimeFirstSearch();
				
				
				
				mailPrinter.append("<br><br><table><tr> <th>Description</th> <th>Time</th> </tr>");
				mailPrinter.append("<tr> <td>Initial Results Availability time - </td> <td>"+timeDiff4+"</td> <tr>");
				mailPrinter.append("<tr> <td>Add to cart time - </td> <td>"+timeDiff2+"</td> <tr>");
				mailPrinter.append("<tr> <td>Reservation No available time - </td> <td>"+timeDiff3+"</td> <tr>");
				mailPrinter.append("</table>");
				
				////
				
				mailPrinter.append("</body></html>");
				
			}
			
			
			
		}else{
			
			mailPrinter.append("<br><br><table ><tr><th>Test Case No</th><th>Test Description</th><th>Expect Values - [Excel Value]</th><th>Actual Values</th><th>Test Status</th></tr>");
			
			/////  Results Page
			testCaseCount++;
			mailPrinter.append("<tr><td class='fontiiii'>Results Page</td><tr>"); 
			mailPrinter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Results Availability</td>");
			mailPrinter.append("<td>Results Should be available</td>");
			mailPrinter.append("<td>Results are not available</td>");
			mailPrinter.append("<td class='Failed'>FAIL</td><tr>");			
			
	
		}
		
		BufferedWriter bwr = new BufferedWriter(new FileWriter(new File("Report/ReservationReport_"+search.getScenarioCount()+".html")));
		bwr.write(mailPrinter.toString());
		bwr.flush();
		bwr.close();
			
		
	}
	
	
	public void getQuotationConfirmationPageDetails(){
			
		if (search.getQuotationReq().equalsIgnoreCase("Yes")) {
			
			//Quotation Confirmation Page
						
			PrintWriter.append("<tr><td class='fontiiii'>Quotation Confirmation Page</td><tr>"); 
			
			testCaseCount ++;
			PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Quotation Status -  Quotation Confirmation Page </td>");
			PrintWriter.append("<td>QUOTE</td>");
			
			if(activitydetails.getQuote_Status().toLowerCase().contains("QUOTE".toLowerCase())){
				
				PrintWriter.append("<td>"+activitydetails.getQuote_Status()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+activitydetails.getQuote_Status()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			
			
			/*testCaseCount ++;
			PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Currency - Quotation Confirmation Page</td>");
			PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
						
			if(search.getSellingCurrency().toLowerCase().equals(activitydetails.getQuote_Currency().toLowerCase())){
					
				PrintWriter.append("<td>"+activitydetails.getQuote_Currency()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+activitydetails.getQuote_Currency()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}*/
			
			payent_qty = Integer.parseInt(search.getAdults()) + Integer.parseInt(search.getChildren());
			
			testCaseCount ++;
			PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity qty - Quotation Confirmation Page</td>");
			PrintWriter.append("<td>"+payent_qty+"</td>");
						
			if(Integer.toString(payent_qty).equals(activitydetails.getQuote_QTY())){
					
				PrintWriter.append("<td>"+activitydetails.getQuote_QTY()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+activitydetails.getQuote_QTY()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			testCaseCount ++;
			PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Name - Quotation Confirmation Page</td>");
			PrintWriter.append("<td>"+search.getActivityName()+"</td>");
						
			if(search.getActivityName().toLowerCase().contains(activitydetails.getQuote_ActivityName().toLowerCase())){
					
				PrintWriter.append("<td>"+activitydetails.getQuote_ActivityName()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+activitydetails.getQuote_ActivityName()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			
		
			
			testCaseCount ++;
			PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Sub Total - Quotation Confirmation Page</td>");
			PrintWriter.append("<td>"+PaymentPage_SubTotal+"</td>");
						
			if(Integer.toString(PaymentPage_SubTotal).equals(activitydetails.getQuote_subTotal())){
					
				PrintWriter.append("<td>"+activitydetails.getQuote_subTotal()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+activitydetails.getQuote_subTotal()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
			testCaseCount ++;
			PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Tax - Quotation Confirmation Page</td>");
			PrintWriter.append("<td>"+PaymentPage_Taxes+"</td>");
						
			if(Integer.toString(PaymentPage_Taxes).equals(activitydetails.getQuote_taxandOther())){
					
				PrintWriter.append("<td>"+activitydetails.getQuote_taxandOther()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+activitydetails.getQuote_taxandOther()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCaseCount ++;
			PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Activity Booking Value - Quotation Confirmation Page</td>");
			PrintWriter.append("<td>"+(PaymentPage_TotalPackageValue-discountedVale)+"</td>");
						
			if(Integer.toString(PaymentPage_TotalPackageValue-discountedVale).equals(activitydetails.getQuote_totalValue())){
					
				PrintWriter.append("<td>"+activitydetails.getQuote_totalValue()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+activitydetails.getQuote_totalValue()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
			testCaseCount ++;
			PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Gross Package Booking Value - Quotation Confirmation Page</td>");
			PrintWriter.append("<td>"+PaymentPage_SubTotal+"</td>");
						
			if(Integer.toString(PaymentPage_SubTotal).equals(activitydetails.getQuote_TotalGrossPackageValue())){
					
				PrintWriter.append("<td>"+activitydetails.getQuote_TotalGrossPackageValue()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+activitydetails.getQuote_TotalGrossPackageValue()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
			testCaseCount ++;
			PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Taxes And Other Charges - Quotation Confirmation Page</td>");
			PrintWriter.append("<td>"+PaymentPage_Taxes+"</td>");
						
			if(Integer.toString(PaymentPage_Taxes).equals(activitydetails.getQuote_TotalTax())){
					
				PrintWriter.append("<td>"+activitydetails.getQuote_TotalTax()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+activitydetails.getQuote_TotalTax()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
			testCaseCount ++;
			PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Package Booking Value - Quotation Confirmation Page</td>");
			PrintWriter.append("<td>"+(PaymentPage_TotalPackageValue-discountedVale-toAgentCommission)+"</td>");
						
			if(Integer.toString(PaymentPage_TotalPackageValue-discountedVale-toAgentCommission).equals(activitydetails.getQuote_TotalPackageValue())){
					
				PrintWriter.append("<td>"+activitydetails.getQuote_TotalPackageValue()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+activitydetails.getQuote_TotalPackageValue()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
			testCaseCount ++;			
			PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details - First Name [Quotation Confirmation Page]</td>");
			PrintWriter.append("<td>"+activitydetails.getPaymentPage_FName()+"</td>");
						
			if(activitydetails.getPaymentPage_FName().equals(activitydetails.getQuote_FName())){
					
				PrintWriter.append("<td>"+activitydetails.getQuote_FName()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+activitydetails.getQuote_FName()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCaseCount ++;			
			PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details - Last Name [Quotation Confirmation Page]</td>");
			PrintWriter.append("<td>"+activitydetails.getPaymentPage_LName()+"</td>");
						
			if(activitydetails.getPaymentPage_LName().equals(activitydetails.getQuote_LName())){
					
				PrintWriter.append("<td>"+activitydetails.getQuote_LName()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+activitydetails.getQuote_LName()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCaseCount ++;			
			PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details - TP No [Quotation Confirmation Page]</td>");
			PrintWriter.append("<td>"+activitydetails.getPaymentPage_TP()+"</td>");
						
			if(activitydetails.getPaymentPage_TP().equals(activitydetails.getQuote_TP())){
					
				PrintWriter.append("<td>"+activitydetails.getQuote_TP()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+activitydetails.getQuote_TP()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCaseCount ++;			
			PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details - Email [Quotation Confirmation Page]</td>");
			PrintWriter.append("<td>"+activitydetails.getPaymentPage_Email()+"</td>");
						
			if(activitydetails.getPaymentPage_Email().equals(activitydetails.getQuote_Email())){
					
				PrintWriter.append("<td>"+activitydetails.getQuote_Email()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+activitydetails.getQuote_Email()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCaseCount ++;			
			PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details - Address [Quotation Confirmation Page]</td>");
			PrintWriter.append("<td>"+activitydetails.getPaymentPage_address()+"</td>");
						
			if(activitydetails.getPaymentPage_address().equals(activitydetails.getQuote_address_1())){
					
				PrintWriter.append("<td>"+activitydetails.getQuote_address_1()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+activitydetails.getQuote_address_1()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
			
			testCaseCount ++;			
			PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details - Country [Quotation Confirmation Page]</td>");
			PrintWriter.append("<td>"+activitydetails.getPaymentPage_country()+"</td>");
						
			if(activitydetails.getPaymentPage_country().equals(activitydetails.getQuote_country())){
					
				PrintWriter.append("<td>"+activitydetails.getQuote_country()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+activitydetails.getQuote_country()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCaseCount ++;			
			PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details - City [Quotation Confirmation Page]</td>");
			PrintWriter.append("<td>"+activitydetails.getPaymentPage_city()+"</td>");
						
			if(activitydetails.getPaymentPage_city().equals(activitydetails.getQuote_city())){
					
				PrintWriter.append("<td>"+activitydetails.getQuote_city()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+activitydetails.getQuote_city()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			String contryPayPage = activitydetails.getPaymentPage_country();
			
			if (contryPayPage.equalsIgnoreCase("USA") || contryPayPage.equalsIgnoreCase("Canada") || contryPayPage.equalsIgnoreCase("Australia")) {
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details - State [Quotation Confirmation Page]</td>");
				PrintWriter.append("<td>"+activitydetails.getPaymentPage_State()+"</td>");
							
				if(activitydetails.getPaymentPage_State().equals(activitydetails.getQuote_state())){
						
					PrintWriter.append("<td>"+activitydetails.getQuote_state()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+activitydetails.getQuote_state()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details - Postal Code [Quotation Confirmation Page]</td>");
				PrintWriter.append("<td>"+activitydetails.getPaymentPage_PostalCode()+"</td>");
							
				if(activitydetails.getPaymentPage_PostalCode().equals(activitydetails.getQuote_postalCode())){
						
					PrintWriter.append("<td>"+activitydetails.getQuote_postalCode()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+activitydetails.getQuote_postalCode()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				
			}
			
			
			PrintWriter.append("</table>");
			
		}	
							
	}
	
		
	public void getMailsInfo(WebDriver Driver){
			
		PrintWriter.append("<br><br>");
		PrintWriter.append("<p class='fontStyles'>Email - Customer Confirmation Email</p>");
		
		testCaseCustomerMail = 1;
		PrintWriter.append("<br><table ><tr><th>Test Case No</th><th>Test Description</th><th>Expect Values - [Excel Value]</th><th>Actual Values</th><th>Test Status</th></tr>");
		
		if (confirmationDetails.isCustomerConfirmationMailLoaded() == true) {
		
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Booking No</td>");
			PrintWriter.append("<td>"+activitydetails.getReservationNo()+"</td>");
			
			if(activitydetails.getReservationNo().equals(confirmationDetails.getCCE_BookingNo())){
				
				PrintWriter.append("<td>"+confirmationDetails.getCCE_BookingNo()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_BookingNo()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
			testCaseCustomerMail++;
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Acitivity Voucher [Reference Number]</td>");
			PrintWriter.append("<td>"+activitydetails.getReservationNo()+"</td>");
			
			if(activitydetails.getReservationNo().equals(confirmationDetails.getActivityVaoucherRefNo())){
				
				PrintWriter.append("<td>"+confirmationDetails.getActivityVaoucherRefNo()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getActivityVaoucherRefNo()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCaseCustomerMail++;
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Acitivity Name</td>");
			PrintWriter.append("<td>"+search.getActivityName()+"</td>");
			
			if(search.getActivityName().toLowerCase().contains(confirmationDetails.getCCE_ActivityType().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_ActivityType()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getCCE_ActivityType()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			
			/////
			
			testCaseCustomerMail++;
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Booking Status </td>");
			PrintWriter.append("<td>"+activitydetails.getConfirmation_BI_BookingStatus()+"</td>");
						
			if (activitydetails.getConfirmation_BI_BookingStatus().toLowerCase().contains("request")) {
				
				if(activitydetails.getConfirmation_BI_BookingStatus().replace("-", "").toLowerCase().contains(confirmationDetails.getCCE_BookingStatus().replace(" ", "").toLowerCase())){
					
					PrintWriter.append("<td>"+confirmationDetails.getCCE_BookingStatus()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+confirmationDetails.getCCE_BookingStatus()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
			} else {

				if(activitydetails.getConfirmation_BI_BookingStatus().toLowerCase().contains(confirmationDetails.getCCE_BookingStatus().toLowerCase())){
					
					PrintWriter.append("<td>"+confirmationDetails.getCCE_BookingStatus()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+confirmationDetails.getCCE_BookingStatus()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
			}
			
			
			/////
			
			
			/*testCaseCustomerMail ++;
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Activity Rate type </td>");
			PrintWriter.append("<td>"+activitydetails.getActivityRateType()+"</td>");
						
			if(confirmationDetails.getCCE_RateType().toLowerCase().contains(activitydetails.getActivityRateType().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_RateType()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getCCE_RateType()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			testCaseCustomerMail ++;
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Activity Session</td>");
			PrintWriter.append("<td>"+activitydetails.getActivitySession()+"</td>");
						
			if(confirmationDetails.getCCE_Period().toLowerCase().contains(activitydetails.getActivitySession().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_Period()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getCCE_Period()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}*/
			
			testCaseCustomerMail ++;
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Activity Rate</td>");
			PrintWriter.append("<td>"+activitydetails.getResultsPage_DailyRate().get(0)+"</td>");
						
			if(confirmationDetails.getCCE_Rate().equals(activitydetails.getResultsPage_DailyRate().get(0))){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_Rate()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getCCE_Rate()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			customerQty = Integer.parseInt(search.getAdults()) + Integer.parseInt(search.getChildren());
			
			testCaseCustomerMail ++;
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Activity QTY</td>");
			PrintWriter.append("<td>"+customerQty+"</td>");
						
			if(Integer.toString(customerQty).equals(confirmationDetails.getCCE_QTY())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_QTY()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getCCE_QTY()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			testCaseCustomerMail ++;
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Currency 1</td>");
			PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
						
			if(search.getSellingCurrency().toLowerCase().equals(confirmationDetails.getCCE_TotalValue_Currency().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_TotalValue_Currency()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_TotalValue_Currency()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCaseCustomerMail ++;
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Sub Total[Without Tax]</td>");
			PrintWriter.append("<td>"+PaymentPage_SubTotal+"</td>");
						
			if(Integer.toString(PaymentPage_SubTotal).equals(confirmationDetails.getCCE_TotalValue())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_TotalValue()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_TotalValue()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			//////
			
			testCaseCustomerMail ++;
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Currency 2</td>");
			PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
						
			if(search.getSellingCurrency().toLowerCase().equals(confirmationDetails.getCCE_CurrencyType_1().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_CurrencyType_1()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_CurrencyType_1()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCaseCustomerMail ++;
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Sub Total - 2[Without Tax]</td>");
			PrintWriter.append("<td>"+(PaymentPage_SubTotal - discountedVale)+"</td>");
						
			if(Integer.toString(PaymentPage_SubTotal - discountedVale).equals(confirmationDetails.getCCE_SubTotal())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_SubTotal()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_SubTotal()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
			
			if (paymentType.equals("Pay Online")) {
				
				int customerPageTotalTax = PaymentPage_Taxes + (int) Math.ceil(PaymentPage_CreditCardFee);;
				
				testCaseCustomerMail ++;
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Total Tax and other charges</td>");
				PrintWriter.append("<td>"+(customerPageTotalTax)+"</td>");
							
				if(Integer.toString(customerPageTotalTax).equals(confirmationDetails.getCCE_TotalTaxOther_1())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_TotalTaxOther_1()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_TotalTaxOther_1()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				testCaseCustomerMail ++;
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Activity Total Booking Value</td>");
				PrintWriter.append("<td>"+(PaymentPage_SubTotal + customerPageTotalTax - discountedVale)+"</td>");
							
				if(Integer.toString(PaymentPage_SubTotal + customerPageTotalTax - discountedVale).equals(confirmationDetails.getCCE_TotalBookingValue())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_TotalBookingValue()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_TotalBookingValue()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCustomerMail ++;
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Amount payable now</td>");
				PrintWriter.append("<td>"+(PaymentPage_SubTotal + customerPageTotalTax - discountedVale)+"</td>");
							
				if(Integer.toString(PaymentPage_SubTotal + customerPageTotalTax - discountedVale).equals(confirmationDetails.getCCE_AmountPayableNow())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_AmountPayableNow()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_AmountPayableNow()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCustomerMail ++;
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Amount to be paid at utilization</td>");
				PrintWriter.append("<td>"+PaymentPage_AmountCheckIn+"</td>");
							
				if(Integer.toString(PaymentPage_AmountCheckIn).equals(confirmationDetails.getCCE_AmountDue())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_AmountDue()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_AmountDue()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
			}else{
				
				testCaseCustomerMail ++;
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Total Tax and other charges</td>");
				PrintWriter.append("<td>"+(PaymentPage_Taxes)+"</td>");
							
				if(Integer.toString(PaymentPage_Taxes).equals(confirmationDetails.getCCE_TotalTaxOther_1())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_TotalTaxOther_1()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_TotalTaxOther_1()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				testCaseCustomerMail ++;
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Booking List Report - Activity Total Booking Value</td>");
				PrintWriter.append("<td>"+(PaymentPage_SubTotal + PaymentPage_Taxes - discountedVale)+"</td>");
							
				if(Integer.toString(PaymentPage_SubTotal + PaymentPage_Taxes - discountedVale).equals(confirmationDetails.getCCE_TotalBookingValue())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_TotalBookingValue()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_TotalBookingValue()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}	
				
				testCaseCustomerMail ++;
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Amount payable now</td>");
				PrintWriter.append("<td>"+(PaymentPage_SubTotal + PaymentPage_Taxes)+"</td>");
							
				if(Integer.toString(PaymentPage_SubTotal + PaymentPage_Taxes).equals(confirmationDetails.getCCE_AmountPayableNow())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_AmountPayableNow()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_AmountPayableNow()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCustomerMail ++;
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Amount to be paid at utilization</td>");
				PrintWriter.append("<td>"+PaymentPage_AmountCheckIn+"</td>");
							
				if(Integer.toString(PaymentPage_AmountCheckIn).equals(confirmationDetails.getCCE_AmountDue())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_AmountDue()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_AmountDue()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
			}
			
			
			
			/*testCaseCustomerMail ++;
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Sub Total - 3[Without Tax]</td>");
			PrintWriter.append("<td>"+PaymentPage_SubTotal+"</td>");
						
			if(Integer.toString(PaymentPage_SubTotal).equals(confirmationDetails.getCCE_SubTotal_2())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_SubTotal_2()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_SubTotal_2()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}*/
			
			//////
			
			if (paymentType.equals("Pay Online")) {
				
				int customerPageTotalTax = PaymentPage_Taxes + (int) Math.ceil(PaymentPage_CreditCardFee);
				
				/*testCaseCustomerMail ++;
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Total Tax and other charges - 2</td>");
				PrintWriter.append("<td>"+(customerPageTotalTax)+"</td>");
							
				if(Integer.toString(customerPageTotalTax).equals(confirmationDetails.getCCE_TotalTaxOther_2())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_TotalTaxOther_2()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_TotalTaxOther_2()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}*/
				
				
				testCaseCustomerMail ++;
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Total Booking Value - 2</td>");
				PrintWriter.append("<td>"+(PaymentPage_SubTotal + customerPageTotalTax - discountedVale)+"</td>");
							
				if(Integer.toString(PaymentPage_SubTotal + customerPageTotalTax - discountedVale).equals(confirmationDetails.getCCE_TotalBookingValue_3())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_TotalBookingValue_3()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_TotalBookingValue_3()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCustomerMail ++;
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Amount being Processed Now - 2</td>");
				PrintWriter.append("<td>"+(PaymentPage_SubTotal + customerPageTotalTax - discountedVale)+"</td>");
							
				if(Integer.toString(PaymentPage_SubTotal + customerPageTotalTax - discountedVale).equals(confirmationDetails.getCCE_AmountPocessed())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_AmountPocessed()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_AmountPocessed()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCustomerMail ++;
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Amount to be paid at utilization - 2</td>");
				PrintWriter.append("<td>"+PaymentPage_AmountCheckIn+"</td>");
							
				if(Integer.toString(PaymentPage_AmountCheckIn).equals(confirmationDetails.getCCE_AmountDue_2())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_AmountDue_2()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_AmountDue_2()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
			}else{
				
				testCaseCustomerMail ++;
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Total Tax and other charges - 2</td>");
				PrintWriter.append("<td>"+(PaymentPage_Taxes)+"</td>");
							
				if(Integer.toString(PaymentPage_Taxes).equals(confirmationDetails.getCCE_TotalTaxOther_2())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_TotalTaxOther_2()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_TotalTaxOther_2()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				testCaseCustomerMail ++;
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Booking List Report - Total Booking Value - 2</td>");
				PrintWriter.append("<td>"+(PaymentPage_SubTotal + PaymentPage_Taxes)+"</td>");
							
				if(Integer.toString(PaymentPage_SubTotal + PaymentPage_Taxes).equals(confirmationDetails.getCCE_TotalBookingValue_3())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_TotalBookingValue_3()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_TotalBookingValue_3()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}	
				
				
				
				testCaseCustomerMail ++;
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Amount being Processed Now</td>");
				PrintWriter.append("<td>"+(PaymentPage_SubTotal + PaymentPage_Taxes)+"</td>");
							
				if(Integer.toString(PaymentPage_SubTotal + PaymentPage_Taxes).equals(confirmationDetails.getCCE_AmountPocessed())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_AmountPocessed()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_AmountPocessed()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCustomerMail ++;
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Amount to be paid at utilization</td>");
				PrintWriter.append("<td>"+PaymentPage_AmountCheckIn+"</td>");
							
				if(Integer.toString(PaymentPage_AmountCheckIn).equals(confirmationDetails.getCCE_AmountDue_2())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_AmountDue_2()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_AmountDue_2()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
			}
			
			testCaseCustomerMail ++;
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Currency 4</td>");
			PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
						
			if(search.getSellingCurrency().toLowerCase().equals(confirmationDetails.getCCE_TotalBookingValue_Currency().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_TotalBookingValue_Currency()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_TotalBookingValue_Currency()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			/////////
			
			
			
			if (UserTypeDC_B2B == WEB_TourOperatorType.WEB_DC) {
				
				testCaseCustomerMail ++;			
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - First Name</td>");
				PrintWriter.append("<td>"+activitydetails.getPaymentPage_FName()+"</td>");
							
				if(activitydetails.getPaymentPage_FName().equals(confirmationDetails.getCCE_FName())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_FName()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_FName()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCustomerMail ++;			
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Last Name</td>");
				PrintWriter.append("<td>"+activitydetails.getPaymentPage_LName()+"</td>");
							
				if(activitydetails.getPaymentPage_LName().replace(" ", "").equalsIgnoreCase(confirmationDetails.getCCE_LName().replace(" ", ""))){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_LName()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_LName()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
			}else{
				
				testCaseCustomerMail ++;			
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - First Name</td>");
				PrintWriter.append("<td>"+activitydetails.getPaymentPage_FName()+"</td>");
							
				if(activitydetails.getPaymentPage_FName().replace(" ", "").equalsIgnoreCase(confirmationDetails.getCCE_FName().replace(" ", ""))){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_FName()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_FName()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCustomerMail ++;			
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Last Name</td>");
				PrintWriter.append("<td>"+activitydetails.getPaymentPage_LName()+"</td>");
							
				if(activitydetails.getPaymentPage_LName().replace(" ", "").equalsIgnoreCase(confirmationDetails.getCCE_LName().replace(" ", ""))){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_LName()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_LName()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
			}
			
			
			
			
			testCaseCustomerMail ++;			
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - TP No</td>");
			PrintWriter.append("<td>"+activitydetails.getPaymentPage_TP()+"</td>");
						
			if(activitydetails.getPaymentPage_TP().equals(confirmationDetails.getCCE_TP())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_TP()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_TP()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCaseCustomerMail ++;			
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Email</td>");
			PrintWriter.append("<td>"+activitydetails.getPaymentPage_Email()+"</td>");
						
			if(confirmationDetails.getCCE_Email().contains(activitydetails.getPaymentPage_Email())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_Email()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_Email()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCaseCustomerMail ++;			
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Address</td>");
			PrintWriter.append("<td>"+activitydetails.getPaymentPage_address()+"</td>");
						
			if(activitydetails.getPaymentPage_address().equals(confirmationDetails.getCCE_address())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_address()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_address()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
			
			testCaseCustomerMail ++;			
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Country</td>");
			PrintWriter.append("<td>"+activitydetails.getPaymentPage_country()+"</td>");
						
			if(activitydetails.getPaymentPage_country().equals(confirmationDetails.getCCE_country())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_country()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_country()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCaseCustomerMail ++;			
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - City</td>");
			PrintWriter.append("<td>"+activitydetails.getPaymentPage_city()+"</td>");
						
			if(activitydetails.getPaymentPage_city().equals(confirmationDetails.getCCE_city())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_city()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_city()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			/////
			
			for (int i = 0; i < activitydetails.getResultsPage_cusTitle().size(); i++) {
				
				testCaseCustomerMail++;
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Counts - [Customer "+(i+1)+"] Title - Customer Confirmation Email]</td>");
				PrintWriter.append("<td>"+activitydetails.getResultsPage_cusTitle().get(i)+"</td>");
				
				if(activitydetails.getResultsPage_cusTitle().get(i).equals(confirmationDetails.getCCE_CusTitle().get(i))){
					
					PrintWriter.append("<td>"+confirmationDetails.getCCE_CusTitle().get(i)+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
				}
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_CusTitle().get(i)+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
			}
			
			for (int i = 0; i < activitydetails.getResultsPage_cusTitle().size(); i++) {
				
				testCaseCustomerMail++;
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Counts - [Customer "+(i+1)+"] FName- Customer Confirmation Email]</td>");
				PrintWriter.append("<td>"+activitydetails.getResultsPage_cusFName().get(i)+"</td>");
				
				if(activitydetails.getResultsPage_cusFName().get(i).equals(confirmationDetails.getCCE_CusFName().get(i))){
					
					PrintWriter.append("<td>"+confirmationDetails.getCCE_CusFName().get(i)+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
				}
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_CusFName().get(i)+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
			}
			
			for (int i = 0; i < activitydetails.getResultsPage_cusTitle().size(); i++) {
				
				testCaseCustomerMail++;
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Counts - [Customer "+(i+1)+"] LName - Customer Confirmation Email]</td>");
				PrintWriter.append("<td>"+activitydetails.getResultsPage_cusLName().get(i)+"</td>");
				
				if(activitydetails.getResultsPage_cusLName().get(i).equals(confirmationDetails.getCCE_CusLName().get(i))){
					
					PrintWriter.append("<td>"+confirmationDetails.getCCE_CusLName().get(i)+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
				}
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_CusLName().get(i)+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
			}
			
			/////
			
			if (paymentType.equals("Pay Online")) {
				
				if((PG_Properties.getProperty("PortalCurrency").toLowerCase().equals(supplierCurrency.toLowerCase()))){
					
					testCaseCustomerMail ++;			
					PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Total Value In USD</td>");
					PrintWriter.append("<td>"+PaymentPage_AmountProcessed+"</td>");
								
					if(Integer.toString(PaymentPage_AmountProcessed).equals(confirmationDetails.getCCE_PaymentTotalValue())){
							
						PrintWriter.append("<td>"+confirmationDetails.getCCE_PaymentTotalValue()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+confirmationDetails.getCCE_PaymentTotalValue()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
				}
				
				else{
				
					for(Entry<String, String> entry: currencyMap.entrySet()) {
						if(supplierCurrency.toLowerCase().equals(entry.getKey().toLowerCase())){
							
							rateConvertforUSD = Double.parseDouble(entry.getValue());
							
							int ResultsPage_TotalPayable_ForUSD = 0;
							
							if (!(UserTypeDC_B2B == WEB_TourOperatorType.WEB_DC)) {
															
								if (UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCASH || UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCREDITLPONO || UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCREDITLPOY) {
									
									ResultsPage_TotalPayable_ForUSD =  (int) Math.ceil((( Double.parseDouble(activitydetails.getToActivity_TotalTax()) + Double.parseDouble(CCFeeForMails) + (Double.parseDouble(activitydetails.getToActivity_SubTotal()) - Double.parseDouble(activitydetails.getToAgentCommission())) ) / rateConvertforUSD ));																
									
								}
								if (UserTypeDC_B2B == WEB_TourOperatorType.WEB_NETCASH || UserTypeDC_B2B == WEB_TourOperatorType.WEB_NETCREDITLPON || UserTypeDC_B2B == WEB_TourOperatorType.WEB_NETCREDITLPOY) {
									
									ResultsPage_TotalPayable_ForUSD =  (int) Math.ceil((( Double.parseDouble(activitydetails.getToActivity_TotalTax()) + Double.parseDouble(CCFeeForMails) + (Double.parseDouble(activitydetails.getToActivity_SubTotal()) ) ) / rateConvertforUSD ));																
																		
								}
								
							
							}else{
								
								ResultsPage_TotalPayable_ForUSD =  (int) Math.ceil((Double.parseDouble(CCFeeForMails) / rateConvertforUSD ));
							}
							
							
							testCaseCustomerMail ++;			
							PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Total Value In USD</td>");
							PrintWriter.append("<td>USD - "+ResultsPage_TotalPayable_ForUSD+"</td>");
										
							if(Integer.toString(ResultsPage_TotalPayable_ForUSD).equals(confirmationDetails.getCCE_PaymentTotalValue())){
									
								PrintWriter.append("<td>USD - "+confirmationDetails.getCCE_PaymentTotalValue()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>USD - "+confirmationDetails.getCCE_PaymentTotalValue()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
						}
					}
					
					
				}
			}
				
			testCaseCustomerMail ++;			
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Customer Notes</td>");
			PrintWriter.append("<td>"+activitydetails.getCustomerNotes()+"</td>");
						
			if(activitydetails.getCustomerNotes().equals(confirmationDetails.getCCE_CustomerNote())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_CustomerNote()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_CustomerNote()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
			//////////////////////////////
			
			testCaseCustomerMail ++;
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email and Payment page Cancellation policy</td>");
			PrintWriter.append("<td>Payment Page :- "+activitydetails.getCancelPolicy().size()+"</td>");
						
			if(activitydetails.getCancelPolicy().size() == confirmationDetails.getCanellationPolicy().size()){
					
				PrintWriter.append("<td>Confirmation Report :- "+confirmationDetails.getCanellationPolicy().size()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>Confirmation Report :- "+confirmationDetails.getCanellationPolicy().size()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			////
			
			if (activitydetails.getCancelPolicy().size() == 2) {
				
				testCaseCustomerMail ++;			
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Cancellation policy 1</td>");
				PrintWriter.append("<td>"+resPolist1+"</td>");
							
				if(resPolist1.equals(confirmationDetails.getCanellationPolicy().get(0))){
						
					PrintWriter.append("<td>"+confirmationDetails.getCanellationPolicy().get(0)+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCanellationPolicy().get(0)+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCustomerMail ++;			
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - No Show Fee Apply</td>");
				PrintWriter.append("<td>"+noShowActual+"</td>");
							
				if(noShowActual.equals(confirmationDetails.getCanellationPolicy().get(1))){
						
					PrintWriter.append("<td>"+confirmationDetails.getCanellationPolicy().get(1)+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCanellationPolicy().get(1)+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
			}
			
			if (activitydetails.getCancelPolicy().size() == 3) {
				
				testCaseCustomerMail ++;			
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Cancellation policy 1</td>");
				PrintWriter.append("<td>"+policyListOne+"</td>");
							
				if(policyListOne.equals(confirmationDetails.getCanellationPolicy().get(0))){
						
					PrintWriter.append("<td>"+confirmationDetails.getCanellationPolicy().get(0)+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCanellationPolicy().get(0)+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCustomerMail ++;			
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Cancellation policy 2</td>");
				PrintWriter.append("<td>"+policyListTwo+"</td>");
							
				if(policyListTwo.replaceAll(" ", "").contains(confirmationDetails.getCanellationPolicy().get(1).replaceAll(" ", ""))){
						
					PrintWriter.append("<td>"+confirmationDetails.getCanellationPolicy().get(1)+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCanellationPolicy().get(1)+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCustomerMail ++;			
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - No Show Fee Apply</td>");
				PrintWriter.append("<td>"+noShowActual+"</td>");
							
				if(noShowActual.replaceAll(" ", "").equals(confirmationDetails.getCanellationPolicy().get(2).replaceAll(" ", ""))){
						
					PrintWriter.append("<td>"+confirmationDetails.getCanellationPolicy().get(2)+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCanellationPolicy().get(2)+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
			}
			
			////////
			
			if (UserTypeDC_B2B == WEB_TourOperatorType.WEB_DC) {
				
				testCaseCustomerMail ++;			
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Tel 1</td>");
				PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Tel")+"</td>");
							
				if(PG_Properties.getProperty("Portal.Tel").toLowerCase().equals(confirmationDetails.getCCE_Tel_1().toLowerCase())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_Tel_1()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_Tel_1()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCustomerMail ++;			
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Tel 2</td>");
				PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Tel")+"</td>");
							
				if(PG_Properties.getProperty("Portal.Tel").toLowerCase().equals(confirmationDetails.getCCE_Tel_2().toLowerCase())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_Tel_2()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_Tel_2()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCustomerMail ++;			
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Email 1</td>");
				PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Email")+"</td>");
							
				if(PG_Properties.getProperty("Portal.Email").toLowerCase().equals(confirmationDetails.getCCE_Email_1().toLowerCase())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_Email_1()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_Email_1()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCustomerMail ++;			
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Email 2</td>");
				PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Email")+"</td>");
							
				if(PG_Properties.getProperty("Portal.Email").toLowerCase().equals(confirmationDetails.getCCE_Email_2().toLowerCase())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_Email_2()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_Email_2()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCustomerMail ++;			
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Website</td>");
				PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Website")+"</td>");
							
				if(PG_Properties.getProperty("Portal.Website").toLowerCase().equals(confirmationDetails.getCCE_Website().toLowerCase())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_Website()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_Website()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCustomerMail ++;			
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Fax</td>");
				PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Fax")+"</td>");
							
				if(PG_Properties.getProperty("Portal.Fax").toLowerCase().equals(confirmationDetails.getCCE_Fax().toLowerCase())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_Fax()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_Fax()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCustomerMail ++;			
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Company Name</td>");
				PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Name")+"</td>");
							
				if(PG_Properties.getProperty("Portal.Name").toLowerCase().equals(confirmationDetails.getCCE_CompanyName().toLowerCase())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_CompanyName()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_CompanyName()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
			}else{
				
				testCaseCustomerMail ++;			
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Tel 1</td>");
				PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.TelTO")+"</td>");
							
				if(PG_Properties.getProperty("Portal.TelTO").toLowerCase().equals(confirmationDetails.getCCE_Tel_1().toLowerCase())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_Tel_1()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_Tel_1()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCustomerMail ++;			
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Tel 2</td>");
				PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.TelTO")+"</td>");
							
				if(PG_Properties.getProperty("Portal.TelTO").toLowerCase().equals(confirmationDetails.getCCE_Tel_2().toLowerCase())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_Tel_2()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_Tel_2()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCustomerMail ++;			
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Email 1</td>");
				PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.EmailTO")+"</td>");
							
				if(PG_Properties.getProperty("Portal.EmailTO").toLowerCase().equals(confirmationDetails.getCCE_Email_1().toLowerCase())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_Email_1()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_Email_1()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCustomerMail ++;			
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Email 2</td>");
				PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.EmailTO")+"</td>");
							
				if(PG_Properties.getProperty("Portal.EmailTO").toLowerCase().equals(confirmationDetails.getCCE_Email_2().toLowerCase())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_Email_2()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_Email_2()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCustomerMail ++;			
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Website</td>");
				PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.WebsiteTO")+"</td>");
							
				if(PG_Properties.getProperty("Portal.WebsiteTO").toLowerCase().equals(confirmationDetails.getCCE_Website().toLowerCase())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_Website()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_Website()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCustomerMail ++;			
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Fax</td>");
				PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.FaxTO")+"</td>");
							
				if(PG_Properties.getProperty("Portal.FaxTO").toLowerCase().equals(confirmationDetails.getCCE_Fax().toLowerCase())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_Fax()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_Fax()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCustomerMail ++;			
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Company Name</td>");
				PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.NameTO")+"</td>");
							
				if(PG_Properties.getProperty("Portal.NameTO").toLowerCase().equals(confirmationDetails.getCCE_CompanyName().toLowerCase())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_CompanyName()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_CompanyName()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
			}
			
			
		}else{
			
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Email - Customer Confirmation Mail</td>");
			PrintWriter.append("<td>Customer Confirmation Mail should be available</td>");
			PrintWriter.append("<td>Not Available</td>");
			PrintWriter.append("<td class='Failed'>FAIL</td><tr>");			
			
		}
		
		PrintWriter.append("</table>");	
		
		/////////
		
		PrintWriter.append("<br><br>");
		PrintWriter.append("<p class='fontStyles'>Email - Customer Voucher Email</p>");
		
		testCaseVoucherMail = 1;
		PrintWriter.append("<br><table ><tr><th>Test Case No</th><th>Test Description</th><th>Expect Values - [Excel Value]</th><th>Actual Values</th><th>Test Status</th></tr>");
		
		if (confirmationDetails.isCustomerVoucherMailLoaded() == true) {

			
			String leadPassengr = confirmationDetails.getCVE_LeadPassenger().replace(" ", "");
			
			
			String cusName;
			if (UserTypeDC_B2B == WEB_TourOperatorType.WEB_DC) {
				cusName = activitydetails.getPaymentPage_Title() + activitydetails.getPaymentPage_FName() + activitydetails.getPaymentPage_LName();
			} else {
				cusName = activitydetails.getResultsPage_cusTitle().get(0) + activitydetails.getResultsPage_cusFName().get(0) + activitydetails.getResultsPage_cusLName().get(0);
			}
				
			PrintWriter.append("<tr><td>"+testCaseVoucherMail+"</td> <td>Customer Voucher EMail - Lead Passenger Name</td>");
			PrintWriter.append("<td>"+cusName+"</td>");
						
			if(leadPassengr.contains(cusName)){
					
				PrintWriter.append("<td>"+leadPassengr+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+leadPassengr+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			/*testCaseVoucherMail ++;			
			PrintWriter.append("<tr><td>"+testCaseVoucherMail+"</td> <td>Customer Voucher EMail - Address</td>");
			PrintWriter.append("<td>"+activitydetails.getPaymentPage_address()+"</td>");
						
			if(activitydetails.getPaymentPage_address().equals(confirmationDetails.getCVE_Address1())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCVE_Address1()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCVE_Address1()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}*/
			
			testCaseVoucherMail ++;	
			PrintWriter.append("<tr><td>"+testCaseVoucherMail+"</td> <td>Customer Voucher EMail - Booking No</td>");
			PrintWriter.append("<td>"+activitydetails.getReservationNo()+"</td>");
			
			if(activitydetails.getReservationNo().equals(confirmationDetails.getCVE_BookingNo())){
				
				PrintWriter.append("<td>"+confirmationDetails.getCVE_BookingNo()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCVE_BookingNo()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			String cveIssueDate = currentDateforMatch;
			
			testCaseVoucherMail ++;	
			PrintWriter.append("<tr><td>"+testCaseVoucherMail+"</td> <td>Customer Voucher EMail - Booking Issue Date</td>");
			PrintWriter.append("<td>"+cveIssueDate+"</td>");
			
			if(cveIssueDate.replace("-", "").equals(confirmationDetails.getCVE_IssueDate().replace("-", ""))){
				
				PrintWriter.append("<td>"+confirmationDetails.getCVE_IssueDate()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCVE_IssueDate()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCaseVoucherMail++;
			PrintWriter.append("<tr><td>"+testCaseVoucherMail+"</td> <td>Customer Voucher EMail - Acitivity Name</td>");
			PrintWriter.append("<td>"+search.getActivityName()+"</td>");
			
			if(search.getActivityName().toLowerCase().replaceAll(" ", "").contains(confirmationDetails.getCVE_ActivityName().toLowerCase().replaceAll(" ", ""))){
					
				PrintWriter.append("<td>"+confirmationDetails.getCVE_ActivityName()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getCVE_ActivityName()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			///////
			testCaseVoucherMail ++;
			PrintWriter.append("<tr><td>"+testCaseVoucherMail+"</td> <td>Customer Voucher Email - Booking Status</td>");
			PrintWriter.append("<td>"+activitydetails.getConfirmation_BI_BookingStatus()+"</td>");
			
			if (activitydetails.getConfirmation_BI_BookingStatus().toLowerCase().contains("request")) {
				
				if(activitydetails.getConfirmation_BI_BookingStatus().replace("-", "").toLowerCase().contains(confirmationDetails.getCVE_bookingStatus().replace(" ", "").toLowerCase())){
					
					PrintWriter.append("<td>"+confirmationDetails.getCVE_bookingStatus()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+confirmationDetails.getCVE_bookingStatus()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
			} else {

				if(activitydetails.getConfirmation_BI_BookingStatus().toLowerCase().contains(confirmationDetails.getCVE_bookingStatus().toLowerCase())){
					
					PrintWriter.append("<td>"+confirmationDetails.getCVE_bookingStatus()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+confirmationDetails.getCVE_bookingStatus()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
			}
			
			///////
			
			
			
			testCaseVoucherMail ++;
			PrintWriter.append("<tr><td>"+testCaseVoucherMail+"</td> <td>Customer Voucher Email - Program City</td>");
			PrintWriter.append("<td>"+search.getDestination()+"</td>");
						
			if(search.getDestination().toLowerCase().contains(confirmationDetails.getCVE_ProgramCity().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCVE_ProgramCity()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getCVE_ProgramCity()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			////
			
			
			
			testCaseVoucherMail ++;
			PrintWriter.append("<tr><td>"+testCaseVoucherMail+"</td> <td>Customer Voucher Email - Supplier Address</td>");
			PrintWriter.append("<td>"+supplierAddress+"</td>");
						
			if(confirmationDetails.getCVE_SupplierAdd().replaceAll(" ", "").toLowerCase().contains(supplierAddress.toLowerCase().replaceAll(" ", ""))){
					
				PrintWriter.append("<td>"+confirmationDetails.getCVE_SupplierAdd()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getCVE_SupplierAdd()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			testCaseVoucherMail ++;
			PrintWriter.append("<tr><td>"+testCaseVoucherMail+"</td> <td>Customer Voucher Email - Supplier TP</td>");
			PrintWriter.append("<td>"+supplierTP+"</td>");
						
			if(supplierTP.replace("-", "").toLowerCase().equals(confirmationDetails.getCVE_SupplierTP().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCVE_SupplierTP()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getCVE_SupplierTP()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			////
			
			testCaseVoucherMail++;
			PrintWriter.append("<tr><td>"+testCaseVoucherMail+"</td> <td>Customer Voucher EMail - Acitivity Name - 2</td>");
			PrintWriter.append("<td>"+search.getActivityName()+"</td>");
			
			if(search.getActivityName().toLowerCase().contains(confirmationDetails.getCVE_ActivityTransferDes().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCVE_ActivityTransferDes()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getCVE_ActivityTransferDes()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			/*testCaseVoucherMail ++;
			PrintWriter.append("<tr><td>"+testCaseVoucherMail+"</td> <td>Customer Voucher Email - Activity Rate type </td>");
			PrintWriter.append("<td>"+activitydetails.getActivityRateType()+"</td>");
						
			if(confirmationDetails.getCVE_RatePlan().toLowerCase().contains(activitydetails.getActivityRateType().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCVE_RatePlan()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getCVE_RatePlan()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			testCaseVoucherMail ++;
			PrintWriter.append("<tr><td>"+testCaseVoucherMail+"</td> <td>Customer Voucher Email - Activity Session</td>");
			PrintWriter.append("<td>"+activitydetails.getActivitySession()+"</td>");
						
			if(confirmationDetails.getCVE_Duration().toLowerCase().contains(activitydetails.getActivitySession().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCVE_Duration()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getCVE_Duration()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}*/
			
			testCaseVoucherMail ++;
			PrintWriter.append("<tr><td>"+testCaseVoucherMail+"</td> <td>Customer Voucher Email - Activity QTY</td>");
			PrintWriter.append("<td>"+customerQty+"</td>");
						
			if(Integer.toString(customerQty).equals(confirmationDetails.getCVE_Qty())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCVE_Qty()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getCVE_Qty()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
		
			String CVE_ServiceDateChanegd = search.getActivityDate();
		
			testCaseVoucherMail ++;
			PrintWriter.append("<tr><td>"+testCaseVoucherMail+"</td> <td>Customer Voucher Email - Activity Service Date</td>");
			PrintWriter.append("<td>"+CVE_ServiceDateChanegd+"</td>");
						
			if(CVE_ServiceDateChanegd.replace("-", "").equals(confirmationDetails.getCVE_ServiceDate().replace("-", ""))){
					
				PrintWriter.append("<td>"+confirmationDetails.getCVE_ServiceDate()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getCVE_ServiceDate()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			
//			testCaseVoucherMail ++;			
//			PrintWriter.append("<tr><td>"+testCaseVoucherMail+"</td> <td>Customer Voucher Email - Customer Notes</td>");
//			PrintWriter.append("<td>"+activitydetails.getCustomerNotes()+"</td>");
//						
//			if(confirmationDetails.getCVE_CustomerNotes().toLowerCase().contains(activitydetails.getCustomerNotes().toLowerCase())){
//					
//				PrintWriter.append("<td>"+confirmationDetails.getCVE_CustomerNotes()+"</td>");
//				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
//			}
//			
//			else{
//				PrintWriter.append("<td>"+confirmationDetails.getCVE_CustomerNotes()+"</td>");
//				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
//			}
			
			
			
			if (UserTypeDC_B2B == WEB_TourOperatorType.WEB_DC) {
				
				testCaseVoucherMail ++;			
				PrintWriter.append("<tr><td>"+testCaseVoucherMail+"</td> <td>Customer Voucher Email - Tel 1</td>");
				PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Tel")+"</td>");
							
				if(PG_Properties.getProperty("Portal.Tel").toLowerCase().equals(confirmationDetails.getCVE_Tel_1().toLowerCase())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCVE_Tel_1()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCVE_Tel_1()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				testCaseVoucherMail ++;			
				PrintWriter.append("<tr><td>"+testCaseVoucherMail+"</td> <td>Customer Voucher Email - Email 1</td>");
				PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Email")+"</td>");
							
				if(PG_Properties.getProperty("Portal.Email").toLowerCase().equals(confirmationDetails.getCVE_Email_1().toLowerCase())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCVE_Email_1()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCVE_Email_1()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				testCaseVoucherMail ++;			
				PrintWriter.append("<tr><td>"+testCaseVoucherMail+"</td> <td>Customer Voucher Email - Website</td>");
				PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Website")+"</td>");
							
				if(PG_Properties.getProperty("Portal.Website").toLowerCase().equals(confirmationDetails.getCVE_Website().toLowerCase())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCVE_Website()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCVE_Website()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseVoucherMail ++;			
				PrintWriter.append("<tr><td>"+testCaseVoucherMail+"</td> <td>Customer Voucher Email - Fax</td>");
				PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Fax")+"</td>");
							
				if(PG_Properties.getProperty("Portal.Fax").toLowerCase().equals(confirmationDetails.getCVE_Fax().toLowerCase())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCVE_Fax()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCVE_Fax()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
			}else{
				
				testCaseVoucherMail ++;			
				PrintWriter.append("<tr><td>"+testCaseVoucherMail+"</td> <td>Customer Voucher Email - Tel 1</td>");
				PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.TelTO")+"</td>");
							
				if(PG_Properties.getProperty("Portal.TelTO").toLowerCase().equals(confirmationDetails.getCVE_Tel_1().toLowerCase())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCVE_Tel_1()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCVE_Tel_1()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				testCaseVoucherMail ++;			
				PrintWriter.append("<tr><td>"+testCaseVoucherMail+"</td> <td>Customer Voucher Email - Email 1</td>");
				PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.EmailTO")+"</td>");
							
				if(PG_Properties.getProperty("Portal.EmailTO").toLowerCase().equals(confirmationDetails.getCVE_Email_1().toLowerCase())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCVE_Email_1()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCVE_Email_1()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				testCaseVoucherMail ++;			
				PrintWriter.append("<tr><td>"+testCaseVoucherMail+"</td> <td>Customer Voucher Email - Website</td>");
				PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.WebsiteTO")+"</td>");
							
				if(PG_Properties.getProperty("Portal.WebsiteTO").toLowerCase().equals(confirmationDetails.getCVE_Website().toLowerCase())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCVE_Website()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCVE_Website()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseVoucherMail ++;			
				PrintWriter.append("<tr><td>"+testCaseVoucherMail+"</td> <td>Customer Voucher Email - Fax</td>");
				PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.FaxTO")+"</td>");
							
				if(PG_Properties.getProperty("Portal.FaxTO").toLowerCase().equals(confirmationDetails.getCVE_Fax().toLowerCase())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCVE_Fax()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCVE_Fax()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
			}
			
			
		}else{
			
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Email - Customer Voucher Mail</td>");
			PrintWriter.append("<td>Customer Voucher Mail should be available</td>");
			PrintWriter.append("<td>Not Available</td>");
			PrintWriter.append("<td class='Failed'>FAIL</td><tr>");		
			
		}
		
		PrintWriter.append("</table>");	
		
		
		
	}
	
	

	public void getQuotationMail(){
		
		if (search.getQuotationReq().equalsIgnoreCase("yes")) {
			
			PrintWriter.append("<br><br>");
			PrintWriter.append("<p class='fontStyles'>Email - Quotation Email</p>");
			
			testQuote = 1;
			PrintWriter.append("<br><table ><tr><th>Test Case No</th><th>Test Description</th><th>Expect Values - [Excel Value]</th><th>Actual Values</th><th>Test Status</th></tr>");
		
			if (quotationDetails.isQuotationMailSent() == true) {
						
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Quotation Number</td>");
				PrintWriter.append("<td>"+activitydetails.getQuote_QuotationNo()+"</td>");
							
				if(quotationDetails.getRefNo().contains(activitydetails.getQuote_QuotationNo())){
						
					PrintWriter.append("<td>"+quotationDetails.getRefNo()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getRefNo()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
								
				String referenceName = activitydetails.getPaymentPage_Title() + activitydetails.getPaymentPage_FName() + activitydetails.getPaymentPage_LName();
				
				testQuote ++;
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Quotation Number</td>");
				PrintWriter.append("<td>"+referenceName+"</td>");
							
				if(quotationDetails.getReferenceName().contains(referenceName)){
						
					PrintWriter.append("<td>"+quotationDetails.getReferenceName()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getReferenceName()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testQuote++;
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Acitivity Name</td>");
				PrintWriter.append("<td>"+search.getActivityName()+"</td>");
				
				if(search.getActivityName().toLowerCase().contains(quotationDetails.getActivityName().toLowerCase())){
						
					PrintWriter.append("<td>"+quotationDetails.getActivityName()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+quotationDetails.getActivityName()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
								
				String servicyDate = getDayNumberSuffix(search.getActivityDate());
				
				testQuote ++;
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Activity Usable on</td>");
				PrintWriter.append("<td>"+servicyDate+"</td>");
							
				if(servicyDate.replace("-", "").equals(quotationDetails.getActivityDate().replace(" ", ""))){
						
					PrintWriter.append("<td>"+quotationDetails.getActivityDate()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+quotationDetails.getActivityDate()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				
				testQuote ++;
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Activity Session</td>");
				PrintWriter.append("<td>"+activitydetails.getResultsPage_Period().get(0)+"</td>");
							
				if(quotationDetails.getPeriodType().toLowerCase().contains(activitydetails.getResultsPage_Period().get(0).toLowerCase())){
						
					PrintWriter.append("<td>"+quotationDetails.getPeriodType()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+quotationDetails.getPeriodType()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				testQuote ++;
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Activity Rate type</td>");
				PrintWriter.append("<td>"+activitydetails.getResultsPage_RateType().get(0)+"</td>");
							
				if(quotationDetails.getRateType().toLowerCase().contains(activitydetails.getResultsPage_RateType().get(0).toLowerCase())){
						
					PrintWriter.append("<td>"+quotationDetails.getRateType()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+quotationDetails.getRateType()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				testQuote ++;
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Booking Status</td>");
				PrintWriter.append("<td>Quote</td>");
							
				if(quotationDetails.getBookingStatus().equalsIgnoreCase("Quote")){
						
					PrintWriter.append("<td>"+quotationDetails.getBookingStatus()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+quotationDetails.getBookingStatus()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				
				
				int customerQtyQuotation = Integer.parseInt(search.getAdults()) + Integer.parseInt(search.getChildren());
				
				testQuote ++;
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Activity QTY</td>");
				PrintWriter.append("<td>"+customerQtyQuotation+"</td>");
							
				if(Integer.toString(customerQtyQuotation).equals(quotationDetails.getQty())){
						
					PrintWriter.append("<td>"+quotationDetails.getQty()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+quotationDetails.getQty()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				
				testQuote ++;
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Total[Without Tax]</td>");
				PrintWriter.append("<td>"+PaymentPage_SubTotal+"</td>");
							
				if(Integer.toString(PaymentPage_SubTotal).equals(quotationDetails.getTotalRate())){
						
					PrintWriter.append("<td>"+quotationDetails.getTotalRate()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getTotalRate()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				testQuote ++;
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Total Currency</td>");
				PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
							
				if(quotationDetails.getCurrency().contains(search.getSellingCurrency())){
						
					PrintWriter.append("<td>"+quotationDetails.getCurrency()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getCurrency()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				testQuote ++;
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Currency 2</td>");
				PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
							
				if(quotationDetails.getCurrency2().contains(search.getSellingCurrency())){
						
					PrintWriter.append("<td>"+quotationDetails.getCurrency2()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getCurrency2()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				
				testQuote ++;
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Sub Total</td>");
				PrintWriter.append("<td>"+PaymentPage_SubTotal+"</td>");
							
				if(Integer.toString(PaymentPage_SubTotal).equals(quotationDetails.getSubTotal())){
						
					PrintWriter.append("<td>"+quotationDetails.getSubTotal()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getSubTotal()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}

				
				testQuote ++;
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Total Tax and other charges</td>");
				PrintWriter.append("<td>"+(PaymentPage_Taxes)+"</td>");
							
				if(Integer.toString(PaymentPage_Taxes).equals(quotationDetails.getTax())){
						
					PrintWriter.append("<td>"+quotationDetails.getTax()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getTax()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				testQuote ++;
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Activity Total Booking Value</td>");
				PrintWriter.append("<td>"+(PaymentPage_SubTotal + PaymentPage_Taxes)+"</td>");
							
				if(Integer.toString(PaymentPage_SubTotal + PaymentPage_Taxes).equals(quotationDetails.getTotalValue())){
						
					PrintWriter.append("<td>"+quotationDetails.getTotalValue()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getTotalValue()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}	
				
				testQuote ++;
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Amount payable when booking</td>");
				PrintWriter.append("<td>"+(PaymentPage_SubTotal + PaymentPage_Taxes)+"</td>");
							
				if(Integer.toString(PaymentPage_SubTotal + PaymentPage_Taxes).equals(quotationDetails.getAmountPayNow())){
						
					PrintWriter.append("<td>"+quotationDetails.getAmountPayNow()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getAmountPayNow()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testQuote ++;
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Amount payable at check-in</td>");
				PrintWriter.append("<td>"+PaymentPage_AmountCheckIn+"</td>");
							
				if(Integer.toString(PaymentPage_AmountCheckIn).equals(quotationDetails.getPayAtCheckIn())){
						
					PrintWriter.append("<td>"+quotationDetails.getPayAtCheckIn()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getPayAtCheckIn()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				testQuote ++;
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Sub Total</td>");
				PrintWriter.append("<td>"+PaymentPage_SubTotal+"</td>");
							
				if(Integer.toString(PaymentPage_SubTotal).equals(quotationDetails.getSubTotal2())){
						
					PrintWriter.append("<td>"+quotationDetails.getSubTotal2()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getSubTotal2()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}

				
				testQuote ++;
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Total Tax and other charges</td>");
				PrintWriter.append("<td>"+(PaymentPage_Taxes)+"</td>");
							
				if(Integer.toString(PaymentPage_Taxes).equals(quotationDetails.getTax2())){
						
					PrintWriter.append("<td>"+quotationDetails.getTax2()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getTax2()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				testQuote ++;
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Total Booking Value</td>");
				PrintWriter.append("<td>"+(PaymentPage_SubTotal + PaymentPage_Taxes)+"</td>");
							
				if(Integer.toString(PaymentPage_SubTotal + PaymentPage_Taxes).equals(quotationDetails.getTotalBookingValue2())){
						
					PrintWriter.append("<td>"+quotationDetails.getTotalBookingValue2()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getTotalBookingValue2()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}	
				
				testQuote ++;
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Amount payable when booking</td>");
				PrintWriter.append("<td>"+(PaymentPage_SubTotal + PaymentPage_Taxes)+"</td>");
							
				if(Integer.toString(PaymentPage_SubTotal + PaymentPage_Taxes).equals(quotationDetails.getAmountPayNow2())){
						
					PrintWriter.append("<td>"+quotationDetails.getAmountPayNow2()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getAmountPayNow2()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testQuote ++;
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Amount payable at check-in / Utilization</td>");
				PrintWriter.append("<td>"+PaymentPage_AmountCheckIn+"</td>");
							
				if(Integer.toString(PaymentPage_AmountCheckIn).equals(quotationDetails.getPayAtCheckIn2())){
						
					PrintWriter.append("<td>"+quotationDetails.getPayAtCheckIn2()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getPayAtCheckIn2()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				
				if (UserTypeDC_B2B == WEB_TourOperatorType.WEB_DC){
					
					testQuote ++;			
					PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - First Name</td>");
					PrintWriter.append("<td>"+activitydetails.getPaymentPage_FName()+"</td>");
								
					if(activitydetails.getPaymentPage_FName().equals(quotationDetails.getCus_FName())){
							
						PrintWriter.append("<td>"+quotationDetails.getCus_FName()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+quotationDetails.getCus_FName()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testQuote ++;			
					PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Last Name</td>");
					PrintWriter.append("<td>"+activitydetails.getPaymentPage_LName()+"</td>");
								
					if(activitydetails.getPaymentPage_LName().equals(quotationDetails.getCus_LName())){
							
						PrintWriter.append("<td>"+quotationDetails.getCus_LName()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+quotationDetails.getCus_LName()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
				}else{
					
					testQuote ++;			
					PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - First Name</td>");
					PrintWriter.append("<td>"+activitydetails.getPaymentPage_FName()+"</td>");
								
					if(activitydetails.getPaymentPage_FName().equals(quotationDetails.getCus_FName())){
							
						PrintWriter.append("<td>"+quotationDetails.getCus_FName()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+quotationDetails.getCus_FName()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testQuote ++;			
					PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Last Name</td>");
					PrintWriter.append("<td>"+activitydetails.getPaymentPage_LName()+"</td>");
								
					if(activitydetails.getPaymentPage_LName().equals(quotationDetails.getCus_LName())){
							
						PrintWriter.append("<td>"+quotationDetails.getCus_LName()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+quotationDetails.getCus_LName()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
				}
				
				
				
				testQuote ++;			
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - TP No</td>");
				PrintWriter.append("<td>"+activitydetails.getPaymentPage_TP()+"</td>");
							
				if(activitydetails.getPaymentPage_TP().equals(quotationDetails.getCusTP())){
						
					PrintWriter.append("<td>"+quotationDetails.getCusTP()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getCusTP()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				testQuote ++;			
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Emergency No</td>");
				PrintWriter.append("<td>"+activitydetails.getPaymentPage_TP()+"</td>");
							
				if(activitydetails.getPaymentPage_TP().equals(quotationDetails.getCusTP())){
						
					PrintWriter.append("<td>"+quotationDetails.getCusTP()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getCusTP()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				testQuote ++;			
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Email</td>");
				PrintWriter.append("<td>"+activitydetails.getPaymentPage_Email()+"</td>");
							
				if(quotationDetails.getCusEmail().contains(activitydetails.getPaymentPage_Email())){
						
					PrintWriter.append("<td>"+quotationDetails.getCusEmail()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getCusEmail()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testQuote ++;			
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Address</td>");
				PrintWriter.append("<td>"+activitydetails.getPaymentPage_address()+"</td>");
							
				if(activitydetails.getPaymentPage_address().equals(quotationDetails.getCusAddress())){
						
					PrintWriter.append("<td>"+quotationDetails.getCusAddress()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getCusAddress()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				
				testQuote ++;			
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Country</td>");
				PrintWriter.append("<td>"+activitydetails.getPaymentPage_country()+"</td>");
							
				if(activitydetails.getPaymentPage_country().equals(quotationDetails.getCuscountry())){
						
					PrintWriter.append("<td>"+quotationDetails.getCuscountry()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getCuscountry()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testQuote ++;			
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - City</td>");
				PrintWriter.append("<td>"+activitydetails.getPaymentPage_city()+"</td>");
							
				if(activitydetails.getPaymentPage_city().equals(quotationDetails.getCuscity())){
						
					PrintWriter.append("<td>"+quotationDetails.getCuscity()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getCuscity()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				
				
				testQuote ++;		
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Tel</td>");
				PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Tel")+"</td>");
							
				if(PG_Properties.getProperty("Portal.Tel").toLowerCase().equals(quotationDetails.getCompanyTel().toLowerCase())){
						
					PrintWriter.append("<td>"+quotationDetails.getCompanyTel()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getCompanyTel()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				

				testQuote ++;			
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Fax</td>");
				PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Fax")+"</td>");
							
				if(PG_Properties.getProperty("Portal.Fax").toLowerCase().equals(quotationDetails.getCompanyFax().toLowerCase())){
						
					PrintWriter.append("<td>"+quotationDetails.getCompanyFax()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getCompanyFax()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
												
				testQuote ++;			
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Company Name</td>");
				PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Name")+"</td>");
							
				if(PG_Properties.getProperty("Portal.Name").toLowerCase().equals(quotationDetails.getCompanyName().toLowerCase())){
						
					PrintWriter.append("<td>"+quotationDetails.getCompanyName()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getCompanyName()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testQuote ++;			
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Email</td>");
				PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Email")+"</td>");
							
				if(PG_Properties.getProperty("Portal.Email").toLowerCase().equals(quotationDetails.getCompanyEmail().toLowerCase())){
						
					PrintWriter.append("<td>"+quotationDetails.getCompanyEmail()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getCompanyEmail()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testQuote ++;			
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Website</td>");
				PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Website")+"</td>");
							
				if(PG_Properties.getProperty("Portal.Website").toLowerCase().equals(quotationDetails.getCompanyWeb().toLowerCase())){
						
					PrintWriter.append("<td>"+quotationDetails.getCompanyWeb()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getCompanyWeb()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
			}else{
				
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Email - Quotation Mail</td>");
				PrintWriter.append("<td>Quotation Mail should be available</td>");
				PrintWriter.append("<td>Not Available</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");		
				
			}
			
			
			
			PrintWriter.append("</table>");
			
		}
			
	}
	
	
	public String getDateSuffix(String day) { 
		
	        String dateChange = (day.substring(0, 2) + day.substring(5, 13)).replaceAll(" ", "");
			
	        return dateChange;
	}
	
	
	public String getDayNumberSuffix(String date) {
		
		StringBuffer stringBuffer = new StringBuffer(date);		
		int day = Integer.parseInt(date.substring(0, 2));
				
	    if (day >= 11 && day <= 13) {
	        return stringBuffer.insert(2, "th").toString();
	    }
	    switch (day % 10) {
	    case 1:
	        return stringBuffer.insert(2, "st").toString();
	    case 2:
	        return stringBuffer.insert(2, "nd").toString();
	    case 3:
	        return stringBuffer.insert(2, "rd").toString();
	    default:
	        return stringBuffer.insert(2, "th ").toString();
	    }
	    	    
	}
		
	
	
}
