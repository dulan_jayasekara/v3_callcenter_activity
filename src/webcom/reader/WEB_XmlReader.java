package webcom.reader;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import webcom.model.*;

public abstract class WEB_XmlReader {
	
	public WebDriver driver;
	public String traceID;
	
	
	public WEB_XmlReader(WebDriver Newdriver){
	    driver = Newdriver;
	}
	
	
	public WEB_AvailabilityRequest readAvailabilityRequests(){
			
		WEB_AvailabilityRequest a_Request  = new WEB_AvailabilityRequest();
				
		return a_Request;
	}
	
	public WEB_AvailabilityResponse readAvailabilityResponses(){
		
		WEB_AvailabilityResponse a_Response  = new WEB_AvailabilityResponse();
		
		return a_Response;		
	}
	
	public WEB_ProgramRequest getProgramRequests(){
		
		WEB_ProgramRequest p_Request = new WEB_ProgramRequest();
		
		return p_Request;		
	}
	
	public WEB_ProgramResponse getProgramResponses(){
		
		WEB_ProgramResponse p_Response = new WEB_ProgramResponse();
		
		return p_Response;			
	}
	
	public WEB_PreservationRequest getReservationRequests(){
		
		WEB_PreservationRequest r_Request = new WEB_PreservationRequest();
		
		return r_Request;
	}
	
	public WEB_PreReservationResponse getReservationResponses(){
		
		WEB_PreReservationResponse r_Response = new WEB_PreReservationResponse();
		
		return r_Response;
	}

	public WEB_CancellationPolicyRequest getCancellationPolicyRequests(){
		
		WEB_CancellationPolicyRequest cp_req = new WEB_CancellationPolicyRequest();
		
		return cp_req;
		
	}
	
	
	public WEB_CancellationPolicyResponse getCancellationPolicyResponse(){
		
		WEB_CancellationPolicyResponse cp_response = new WEB_CancellationPolicyResponse();
		
		return cp_response;
		
		
	}
	
	public String getWindow(String partialLink, WebDriver Driver, String traceId) throws InterruptedException
	{		
		String WindowHandle = driver.getWindowHandle();
		
		Driver.findElement(By.partialLinkText(partialLink.concat(traceId))).click();
	    
		ArrayList<String> list = new ArrayList<String>(driver.getWindowHandles());
		list.remove(WindowHandle);
		driver.switchTo().window(list.get(0));
		
		return WindowHandle;
		
	}


	

	
	
}
