package webcom.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import webcom.model.WEB_ActivityInfo;
import webcom.model.WEB_PaymentDetails;
import webcom.model.WEB_Search;


public class WEB_LoadDetails {
	
	Map<Integer, String> SearchMap 		= null;
	Map<Integer, String> CustomerMap 	= null;
	Map<Integer, String> InventoryTypeMap 	= null;

	public WEB_LoadDetails(ArrayList<Map<Integer, String>> sheetlist){
		
		SearchMap          		= sheetlist.get(0);
		CustomerMap          	= sheetlist.get(1);
		InventoryTypeMap		= sheetlist.get(2);
	}
	
	public TreeMap<String, WEB_Search> loadActivityReservation(Map<Integer, String> searchInfoDeMap){
		
		Iterator<Map.Entry<Integer, String>> it = searchInfoDeMap.entrySet().iterator();
		TreeMap<String, WEB_Search> activitySearchInfoMap = new TreeMap<String, WEB_Search>();
		
		while(it.hasNext()) {
			
			WEB_Search search = new WEB_Search();
			String[] values = it.next().getValue().split(",");
			
			search.setScenarioCount(values[0]);
			search.setSellingCurrency(values[1]);
			search.setCountry(values[2]);
			search.setDestination(values[3]);
			search.setDateFrom(values[4]);
			search.setDateTo(values[5]);
			search.setAdults(values[6]);
			search.setChildren(values[7]);
			search.setAgeOfChildren(values[8]);
			search.setProgramCategory(values[9]);
			search.setPreferCurrency(values[10]);
			search.setPromotionCode(values[11]);
			search.setActivityName(values[12]);
			search.setActivityDate(values[13]);
			search.setScenariotype(values[14]);
			search.setBooking_channel(values[15]);
			search.setQuotationReq(values[16]);
			
			activitySearchInfoMap.put(values[0], search);
		}
		
		return activitySearchInfoMap;
	}	
	
	public TreeMap<String, WEB_Search> loadPaymentDetails(Map<Integer, String> paymentsMapDe, TreeMap<String, WEB_Search> searchInfoDeMap) {

		Iterator<Map.Entry<Integer, String>> it = paymentsMapDe.entrySet().iterator();
		
		while (it.hasNext()) {
			
			WEB_PaymentDetails paymentDetails = new WEB_PaymentDetails();
			String[] values = it.next().getValue().split(",");

			paymentDetails.setScenarioId(values[0]);
			paymentDetails.setCustomerTitle(values[1]);
			paymentDetails.setCustomerName(values[2]);
			paymentDetails.setCustomerLastName(values[3]);
			paymentDetails.setTel(values[4]);
			paymentDetails.setEmail(values[5]);
			paymentDetails.setAddress(values[6]);
			paymentDetails.setAddress_1(values[7]);
			paymentDetails.setCountry(values[8]);
			paymentDetails.setCity(values[9]);
			paymentDetails.setState(values[10]);
			paymentDetails.setPostalCode(values[11]);
						
			try {				
				searchInfoDeMap.get(values[0]).addPaymentInfo(paymentDetails);
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		return searchInfoDeMap;
	}
	
	
	public TreeMap<String, WEB_Search> loadActivityInventoryRecords(Map<Integer, String> inventoryMap, TreeMap<String, WEB_Search> searchInfoDeMap) {
		
		Iterator<Map.Entry<Integer, String>> it = inventoryMap.entrySet().iterator();
		
		while (it.hasNext()) {
			
			WEB_ActivityInfo activityInventory = new WEB_ActivityInfo();
			String[] values = it.next().getValue().split(",");
			
			activityInventory.setScenarioCount(values[0]);
			activityInventory.setActivityName(values[1]);
			activityInventory.setSupplier_Currency(values[2]);
			activityInventory.setActivityType(values[3]);
			
			activityInventory.setPaymentPage_SubTotal(values[4]);	
			activityInventory.setPaymentPage_TotalTaxnOther(values[5]);				
			activityInventory.setPaymentType(values[6]);
			activityInventory.setCreditCardFee(values[7]);
			activityInventory.setPaymentPage_Total(values[8]);		
			activityInventory.setPaymentPage_AmountNow(values[9]);		
			activityInventory.setPaymentPage_AmountDue(values[10]);	
			
			
			activityInventory.setProfir_Markup(values[11]);	
			activityInventory.setStandardCancell_Type(values[12]);
			activityInventory.setStandardCancell_Value(values[13]);	
			activityInventory.setNoShow_Type(values[14]);
			activityInventory.setNoShow_Value(values[15]);
			
			activityInventory.setApplyIfLessThan(values[16]);	
			activityInventory.setCancelBuffer(values[17]);
			activityInventory.setSupplierName(values[18]);	
			activityInventory.setSupplierAddress(values[19]);	
			activityInventory.setSupplierTP(values[20]);	
			activityInventory.setInventoryType(values[21]);	
			activityInventory.setActivityDescription(values[22]);
			
			activityInventory.setUserTypeDC_B2B(values[23]);
			activityInventory.setToActivityNetRate(values[24]);
			activityInventory.setToActivityPM(values[25]);
			activityInventory.setToActivity_SalesTaxType(values[26]);
			activityInventory.setToActivity_SalesTaxValue(values[27]);
			activityInventory.setToActivity_MisFeeType(values[28]);
			activityInventory.setToActivity_MisFeeValue(values[29]);
			activityInventory.setDiscountYesORNo(values[30]);
			
			
			try {
				searchInfoDeMap.get(values[0]).addInventoryInfo(activityInventory);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return searchInfoDeMap;
	}
	
	public static Map<Integer,WEB_ActivityInfo> convertIntoMap(java.util.List<WEB_ActivityInfo> inventoryList){
		
		Map<Integer,WEB_ActivityInfo> inventoryListMap=new HashMap<Integer, WEB_ActivityInfo>();
		
		for(WEB_ActivityInfo r:inventoryList){			
			inventoryListMap.put(Integer.parseInt(r.getScenarioCount()),r);
		}
		
		return inventoryListMap;
		
	}
	
	
}
