package webcom.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import com.controller.PG_Properties;

import webcom.controller.*;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.read.biff.BiffException;

public class WEB_ExcelReader {

	private static Logger PromoLog = Logger.getLogger(WEB_ExcelReader.class.getName());

	
	public ArrayList<Map<Integer, String>> init(String filePath) {

		DOMConfigurator.configure("log4j.xml");		
		FileInputStream fs = null;
		ArrayList<Map<Integer, String>> returnlist = null;
		
		try {
			PromoLog.debug("Creating new file stream..");
			PromoLog.debug("File path - " + filePath);

			fs = new FileInputStream(new File(filePath));

			PromoLog.info("Filestream created successfully");
			returnlist = contentReading(fs);

		} catch (IOException e) {

			PromoLog.fatal(e.toString());

		} catch (Exception e) {

			PromoLog.fatal(e.toString());

		} finally {
			try {
				fs.close();
			} catch (IOException e) {
				
			}
		}
		return returnlist;
	}

	public ArrayList<Map<Integer, String>> contentReading(
			InputStream fileInputStream) {

		ArrayList<Map<Integer, String>> list = null;
		WorkbookSettings ws = null;
		Workbook workbook = null;
		int totalSheet = 0;

		try {
			PromoLog.info("Initializing the workspace.");
			ws = new WorkbookSettings();
			ws.setLocale(new Locale("en", "EN"));
			workbook = Workbook.getWorkbook(fileInputStream, ws);

			totalSheet = workbook.getNumberOfSheets();
			PromoLog.info("Number of sheets found - " + totalSheet);

			list = new ArrayList<Map<Integer, String>>(totalSheet);

			for (int sheet = 0; sheet < totalSheet; sheet++) {
				PromoLog.debug("Sheet Number - " + sheet);
				list.add(getSheetData(workbook.getSheet(sheet)));
				PromoLog.debug(list.get(sheet).toString());
			}

			workbook.close();
		} catch (IOException e) {
			System.out.println(e);
		} catch (BiffException e) {
			System.out.println(e);
		}

		return list;
	}

	public static void main(String[] args) {
		try {
			System.out.println("begin");
			WEB_ExcelReader xlReader = new WEB_ExcelReader();
			ArrayList<Map<Integer, String>> list1 = xlReader.init(PG_Properties.getProperty("WEB.ExcelPath.ActivityReservation"));
			System.out.println(list1);

			Iterator<Map.Entry<Integer, String>> it = list1.get(0).entrySet().iterator();

			while (it.hasNext()) {
				System.out.print("Valueeee --- \n" + it.next().getValue().split(","));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	
	public Map<Integer, String> getSheetData(Sheet s) {
		
		int rawCount = s.getRows();
		int columnCount = s.getColumns();
		
		Map<Integer, String> map = new HashMap<Integer, String>();
		
			for (int count = 1; count < rawCount; count++) {
				
				Cell[] rawdata = s.getRow(count);
				String CellContent = rawdata[0].getContents();
								
				for (int index = 1; index < columnCount; index++) {
					
					CellContent = CellContent.concat(",").concat(rawdata[index].getContents());
				}

				map.put(count - 1, CellContent);
		}
		
		return map;
			
	}


}