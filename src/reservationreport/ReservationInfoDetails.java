package reservationreport;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.controller.PG_Properties;
import com.model.ActivityDetails;

public class ReservationInfoDetails {

	private  WebDriver driver = null; 
	private FirefoxProfile profile;
	JavascriptExecutor javaScriptExe = (JavascriptExecutor) driver;
	ActivityDetails activitydetails 	= new ActivityDetails();
	Reservation reservationDetails = new Reservation();
	
	
	public ReservationInfoDetails(ActivityDetails activityDetails, WebDriver Driver){
		
		this.driver = Driver;
		this.activitydetails = activityDetails;
	}
	
	public Reservation viewReservationReport(String reservationNo, WebDriver Driver) throws InterruptedException{
		
		WebDriverWait wait = new WebDriverWait(driver, 90);	
		
		driver.get(PG_Properties.getProperty("Baseurl")+ "/reports/operational/mainReport.do?reportId=31&reportName=Reservation Report");
		Thread.sleep(500);
		
		try {
			Alert alert = driver.switchTo().alert();
			alert.accept();
			
		} catch (Exception e) {
			
		}
		
		Thread.sleep(6000);
		driver.switchTo().frame("reportIframe");
	
		//((JavascriptExecutor)driver).executeScript("currentFieldFocus('searchby_reservationno');checkRadioAction(this);");
		
		Thread.sleep(3000);
		
		if (driver.findElements(By.xpath(".//*[@id='searchby_reservationno']")).size() != 0) {
		
			try {
				driver.findElement(By.xpath(".//*[@id='searchby_reservationno']")).click();
			} catch (Exception e) {
				e.printStackTrace();
				driver.findElement(By.xpath(".//*[@id='searchby_reservationno']")).click();
				
			}
			
			driver.switchTo().defaultContent();
			driver.switchTo().frame("reportIframe");
			
			Thread.sleep(3000);
			driver.findElement(By.id("reservationno")).clear();
			driver.findElement(By.id("reservationno")).sendKeys(reservationNo);
			driver.findElement(By.id("reservationno_lkup")).click();
			driver.switchTo().defaultContent();
			driver.switchTo().frame("lookup");
			Thread.sleep(2000);
			driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
			Thread.sleep(2000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame("reportIframe");
			
			driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
			Thread.sleep(2000);
			
			driver.switchTo().defaultContent();
			driver.switchTo().frame("reportIframe");
		
		}else{
			reservationDetails.setReservationReportLoaded(false);
		}
		
		return reservationDetails;
	}
	
	
}
