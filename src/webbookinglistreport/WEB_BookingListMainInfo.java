package webbookinglistreport;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.controller.PG_Properties;

import webcom.controller.*;
import webcom.model.WEB_ActivityDetails;
import webcom.model.WEB_Search;

public class WEB_BookingListMainInfo {
	
	private  WebDriver driver = null; 
	private FirefoxProfile profile;
	JavascriptExecutor javaScriptExe = (JavascriptExecutor) driver;
	WEB_ActivityDetails activitydetails 	= new WEB_ActivityDetails();
	
	
	public WEB_BookingListMainInfo(WEB_ActivityDetails activityDetails, WebDriver Driver){
		
		this.driver = Driver;
		this.activitydetails = activityDetails;
		
	}
	
	
	public WebDriver viewBookingListReport(String reservationNo , WebDriver Driver) throws InterruptedException {
		
		WebDriverWait wait = new WebDriverWait(driver, 90);	
		
		driver.get(PG_Properties.getProperty("Baseurl")+ "/reports/operational/mainReport.do?reportId=39&reportName=Booking List Report");
		
		Thread.sleep(2000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");	
		
		driver.findElement(By.id("reservationno")).clear();
		driver.findElement(By.id("reservationno")).sendKeys(reservationNo);
		driver.findElement(By.id("reservationno_lkup")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("lookup");
		driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
		Thread.sleep(1000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");
		
		driver.findElement(By.xpath("html/body/span/table/tbody/tr/td/table/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[3]/a/img")).click();
		Thread.sleep(1000);
				
		
		
		
		return driver;
	}
	

}
