package bookingconfirmation;

public class Quotation {
	
	private boolean isQuotationMailSent = true;
	private String companyTel = "";
	private String companyFax = "";
	private String companyEmail = "";
	private String companyWeb = "";
	private String companyName = "";
	
	private String refNo = "";
	private String referenceName = "";
	private String activityName = "";
	private String activityDate = "";
	private String periodType = "";
	private String rateType = "";
	private String bookingStatus = "";
	private String personRate = "";
	private String qty = "";
	private String currency = "";
	private String totalRate = "";
	
	private String currency2 = "";
	private String subTotal = "";
	private String tax = "";
	private String totalValue = "";
	private String amountPayNow = "";
	private String payAtCheckIn = "";
	
	private String subTotal2 = "";
	private String tax2 = "";
	private String totalBookingValue2 = "";
	private String amountPayNow2 = "";
	private String payAtCheckIn2 = "";
	
	private String cus_FName = "";
	private String cus_LName = "";
	private String cusTP = "";
	private String cusEmerGencyCon = "";
	private String cusEmail = "";
	private String cusAddress = "";
	private String cusAddress_2 = "";
	private String cuscountry = "";
	private String cuscity = "";
	private String cusState = "";
	private String cusPostalCode = "";
	
	
	
	public String getCusEmerGencyCon() {
		return cusEmerGencyCon;
	}
	public void setCusEmerGencyCon(String cusEmerGencyCon) {
		this.cusEmerGencyCon = cusEmerGencyCon;
	}
	public boolean isQuotationMailSent() {
		return isQuotationMailSent;
	}
	public void setQuotationMailSent(boolean isQuotationMailSent) {
		this.isQuotationMailSent = isQuotationMailSent;
	}
	public String getCompanyTel() {
		return companyTel;
	}
	public void setCompanyTel(String companyTel) {
		this.companyTel = companyTel;
	}
	public String getCompanyFax() {
		return companyFax;
	}
	public void setCompanyFax(String companyFax) {
		this.companyFax = companyFax;
	}
	public String getCompanyEmail() {
		return companyEmail;
	}
	public void setCompanyEmail(String companyEmail) {
		this.companyEmail = companyEmail;
	}
	public String getCompanyWeb() {
		return companyWeb;
	}
	public void setCompanyWeb(String companyWeb) {
		this.companyWeb = companyWeb;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	public String getReferenceName() {
		return referenceName;
	}
	public void setReferenceName(String referenceName) {
		this.referenceName = referenceName;
	}
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	public String getActivityDate() {
		return activityDate;
	}
	public void setActivityDate(String activityDate) {
		this.activityDate = activityDate;
	}
	public String getPeriodType() {
		return periodType;
	}
	public void setPeriodType(String periodType) {
		this.periodType = periodType;
	}
	public String getRateType() {
		return rateType;
	}
	public void setRateType(String rateType) {
		this.rateType = rateType;
	}
	public String getBookingStatus() {
		return bookingStatus;
	}
	public void setBookingStatus(String bookingStatus) {
		this.bookingStatus = bookingStatus;
	}
	public String getPersonRate() {
		return personRate;
	}
	public void setPersonRate(String personRate) {
		this.personRate = personRate;
	}
	public String getQty() {
		return qty;
	}
	public void setQty(String qty) {
		this.qty = qty;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getTotalRate() {
		return totalRate;
	}
	public void setTotalRate(String totalRate) {
		this.totalRate = totalRate;
	}
	public String getCurrency2() {
		return currency2;
	}
	public void setCurrency2(String currency2) {
		this.currency2 = currency2;
	}
	public String getSubTotal() {
		return subTotal;
	}
	public void setSubTotal(String subTotal) {
		this.subTotal = subTotal;
	}
	public String getTax() {
		return tax;
	}
	public void setTax(String tax) {
		this.tax = tax;
	}
	public String getTotalValue() {
		return totalValue;
	}
	public void setTotalValue(String totalValue) {
		this.totalValue = totalValue;
	}
	public String getAmountPayNow() {
		return amountPayNow;
	}
	public void setAmountPayNow(String amountPayNow) {
		this.amountPayNow = amountPayNow;
	}
	public String getPayAtCheckIn() {
		return payAtCheckIn;
	}
	public void setPayAtCheckIn(String payAtCheckIn) {
		this.payAtCheckIn = payAtCheckIn;
	}
	public String getSubTotal2() {
		return subTotal2;
	}
	public void setSubTotal2(String subTotal2) {
		this.subTotal2 = subTotal2;
	}
	public String getTax2() {
		return tax2;
	}
	public void setTax2(String tax2) {
		this.tax2 = tax2;
	}
	public String getTotalBookingValue2() {
		return totalBookingValue2;
	}
	public void setTotalBookingValue2(String totalBookingValue2) {
		this.totalBookingValue2 = totalBookingValue2;
	}
	public String getAmountPayNow2() {
		return amountPayNow2;
	}
	public void setAmountPayNow2(String amountPayNow2) {
		this.amountPayNow2 = amountPayNow2;
	}
	public String getPayAtCheckIn2() {
		return payAtCheckIn2;
	}
	public void setPayAtCheckIn2(String payAtCheckIn2) {
		this.payAtCheckIn2 = payAtCheckIn2;
	}
	public String getCus_FName() {
		return cus_FName;
	}
	public void setCus_FName(String cus_FName) {
		this.cus_FName = cus_FName;
	}
	public String getCus_LName() {
		return cus_LName;
	}
	public void setCus_LName(String cus_LName) {
		this.cus_LName = cus_LName;
	}
	public String getCusTP() {
		return cusTP;
	}
	public void setCusTP(String cusTP) {
		this.cusTP = cusTP;
	}
	public String getCusEmail() {
		return cusEmail;
	}
	public void setCusEmail(String cusEmail) {
		this.cusEmail = cusEmail;
	}
	public String getCusAddress() {
		return cusAddress;
	}
	public void setCusAddress(String cusAddress) {
		this.cusAddress = cusAddress;
	}
	public String getCusAddress_2() {
		return cusAddress_2;
	}
	public void setCusAddress_2(String cusAddress_2) {
		this.cusAddress_2 = cusAddress_2;
	}
	public String getCuscountry() {
		return cuscountry;
	}
	public void setCuscountry(String cuscountry) {
		this.cuscountry = cuscountry;
	}
	public String getCuscity() {
		return cuscity;
	}
	public void setCuscity(String cuscity) {
		this.cuscity = cuscity;
	}
	public String getCusState() {
		return cusState;
	}
	public void setCusState(String cusState) {
		this.cusState = cusState;
	}
	public String getCusPostalCode() {
		return cusPostalCode;
	}
	public void setCusPostalCode(String cusPostalCode) {
		this.cusPostalCode = cusPostalCode;
	}
	
	

}
