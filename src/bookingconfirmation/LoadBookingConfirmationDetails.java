package bookingconfirmation;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.controller.PG_Properties;
import com.model.ActivityDetails;

public class LoadBookingConfirmationDetails {

	private WebDriver driver;	
	private ActivityDetails activitydetails;
	private ArrayList<String> cancelPolicy;
	private ArrayList<String> paticipanTitle;
	private ArrayList<String> paticipanFName;
	private ArrayList<String> paticipanLName;
	private String currentDateforImages;
	
	public LoadBookingConfirmationDetails(ActivityDetails activityDetails, WebDriver Driver){
		
		this.activitydetails = activityDetails;
		this.driver = Driver;
	}
//	public LoadBookingConfirmationDetails(WebDriver Driver){
//		
//		this.driver = Driver;
//	}
	

	public BookingConfirmation getConfirmationDetails(WebDriver Driver) throws InterruptedException, IOException{
		
		DateFormat dateFormatcurrentDate = new SimpleDateFormat("yyyy_MM_dd");
		Calendar cal = Calendar.getInstance();
		currentDateforImages = dateFormatcurrentDate.format(cal.getTime());
		
		cancelPolicy = new ArrayList<String>();
		paticipanTitle = new ArrayList<String>();
		paticipanFName = new ArrayList<String>();
		paticipanLName = new ArrayList<String>();
		
		WebDriverWait wait = new WebDriverWait(driver, 90);	
		BookingConfirmationInfo bookingInfo = new BookingConfirmationInfo(activitydetails, driver);
		bookingInfo.viewBookingConfirmationReport(activitydetails.getReservationNo(), driver);
		BookingConfirmation confirmationDetails = new BookingConfirmation();
		
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");
		
		driver.findElement(By.xpath(".//*[@id='hidResId_1']")).click();
		Thread.sleep(1000);
		
		
		driver.findElement(By.xpath(".//*[@id='emaillayoutbasedon_customer1']")).click();
		
		driver.switchTo().frame("Myemailformat");
		
		File CCE = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(CCE, new File(""+currentDateforImages+"/"+activitydetails.getScenarioCount()+"_CCEMail.png"));
		
		
		if (driver.findElements(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[9]/td")).size() != 0) {
			
			String ActivityVaoucherRefNo = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[9]/td")).getText().substring(35, 47);
			confirmationDetails.setActivityVaoucherRefNo(ActivityVaoucherRefNo);
			
			String CCE_BookingRef = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[3]/td")).getText().split(": ")[1];
			confirmationDetails.setCCE_BookingRef(CCE_BookingRef);
			
			String CCE_BookingNo = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[4]/td")).getText().split(": ")[1];
			confirmationDetails.setCCE_BookingNo(CCE_BookingNo);
			
			String CCE_ActivityType = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[11]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setCCE_ActivityType(CCE_ActivityType);
			
			String CCE_BookingStatus = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[13]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setCCE_BookingStatus(CCE_BookingStatus);
			
			String CCE_Period = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[14]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setCCE_Period(CCE_Period);
			
			String CCE_RateType = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[15]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setCCE_RateType(CCE_RateType);
			
			String CCE_Rate = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[16]/td[2]")).getText().split(": ")[1].replace(",", "").split("\\.")[0];
			confirmationDetails.setCCE_Rate(CCE_Rate);
			
			String CCE_QTY = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[17]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setCCE_QTY(CCE_QTY);
			
			String CCE_TotalValue = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[18]/td[2]/strong")).getText().split(": ")[1].replace(",", "").split("\\.")[0];
			confirmationDetails.setCCE_TotalValue(CCE_TotalValue);
			
			String CCE_TotalValue_Currency = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[18]/td[1]/strong")).getText().substring(7, 10);
			confirmationDetails.setCCE_TotalValue_Currency(CCE_TotalValue_Currency);
			
					 
			String CCE_CurrencyType_1 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[19]/td[7]/table/tbody/tr[1]/td[2]")).getText();
			confirmationDetails.setCCE_CurrencyType_1(CCE_CurrencyType_1);
			
			String CCE_SubTotal = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[20]/td[7]/table/tbody/tr[2]/td[2]")).getText().replace(",", "").split("\\.")[0];
			confirmationDetails.setCCE_SubTotal(CCE_SubTotal);
			
			String CCE_TotalTaxOther_1 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[20]/td[7]/table/tbody/tr[3]/td[2]")).getText().replace(",", "").split("\\.")[0];
			confirmationDetails.setCCE_TotalTaxOther_1(CCE_TotalTaxOther_1);
			
			String CCE_TotalBookingValue = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[21]/td[7]/table/tbody/tr[2]/td[2]")).getText().replace(",", "").split("\\.")[0];
			confirmationDetails.setCCE_TotalBookingValue(CCE_TotalBookingValue);
			
			String CCE_AmountPayableNow = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[21]/td[7]/table/tbody/tr[3]/td[2]")).getText().replace(",", "").split("\\.")[0];
			confirmationDetails.setCCE_AmountPayableNow(CCE_AmountPayableNow);
			
			String CCE_AmountDue = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[21]/td[7]/table/tbody/tr[4]/td[2]")).getText().split("\\.")[0];
			confirmationDetails.setCCE_AmountDue(CCE_AmountDue);
			
			
			String CCE_TotalBookingValue_3 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[21]/td[7]/table/tbody/tr[2]/td[2]")).getText().replace(",", "").split("\\.")[0];
			confirmationDetails.setCCE_TotalBookingValue_3(CCE_TotalBookingValue_3);
			
			String CCE_TotalBookingValue_Currency = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[21]/td[7]/table/tbody/tr[2]/td[1]")).getText().substring(21, 24);
			confirmationDetails.setCCE_TotalBookingValue_Currency(CCE_TotalBookingValue_Currency);
			
			String CCE_AmountPocessed = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[21]/td[7]/table/tbody/tr[3]/td[2]")).getText().replace(",", "").split("\\.")[0];
			confirmationDetails.setCCE_AmountPocessed(CCE_AmountPocessed);
			
			String CCE_AmountDue_2 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[21]/td[7]/table/tbody/tr[4]/td[2]")).getText().split("\\.")[0];
			confirmationDetails.setCCE_AmountDue_2(CCE_AmountDue_2);
			
			
			
			String CCE_FName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[25]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setCCE_FName(CCE_FName);
			
			String CCE_LName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[25]/td[8]")).getText().split(": ")[1];
			confirmationDetails.setCCE_LName(CCE_LName);
			
			String CCE_TP;
			try {
				CCE_TP = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[27]/td[2]")).getText().split(": ")[1];
				confirmationDetails.setCCE_TP(CCE_TP);
			} catch (Exception e1) {
				CCE_TP = "Not Available";
				confirmationDetails.setCCE_TP(CCE_TP);
			}
			
			String CCE_Email = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[30]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setCCE_Email(CCE_Email);
			
			String CCE_address = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[26]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setCCE_address(CCE_address);
			
			String CCE_address_2;
			try {
				CCE_address_2 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[26]/td[8]")).getText().split(": ")[1];
				confirmationDetails.setCCE_address_2(CCE_address_2);
			} catch (Exception e1) {
				CCE_address_2 = "Not Available";
				confirmationDetails.setCCE_address_2(CCE_address_2);
			}
			
			
			
			String CCE_country = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[28]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setCCE_country(CCE_country);
			
			String CCE_city = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[29]/td[8]")).getText().split(": ")[1];
			confirmationDetails.setCCE_city(CCE_city);
			
			
			
			String CCE_Occupancy_ActivityName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[33]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setCCE_Occupancy_ActivityName(CCE_Occupancy_ActivityName);
			
			String CCE_Occupancy_FName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[34]/td/table/tbody/tr[3]/td[4]")).getText();
			confirmationDetails.setCCE_Occupancy_FName(CCE_Occupancy_FName);
			
			String CCE_Occupancy_LName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[34]/td/table/tbody/tr[3]/td[5]")).getText();
			confirmationDetails.setCCE_Occupancy_LName(CCE_Occupancy_LName);
			
			try {
				String CCE_PaymentTotalValue = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[39]/td[8]")).getText().split(": ")[1].split("\\.")[0];
				confirmationDetails.setCCE_PaymentTotalValue(CCE_PaymentTotalValue);
			} catch (Exception e) {
				
				try {
					String CCE_PaymentTotalValue = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[39]/td[2]")).getText().split(": ")[1].split("\\.")[0];
					confirmationDetails.setCCE_PaymentTotalValue(CCE_PaymentTotalValue);
				} catch (Exception e2) {
					e2.getMessage();
				}			
			}
				
			///
			
			if (activitydetails.getUserType().equalsIgnoreCase("NETCASH") || (activitydetails.getUserType().equalsIgnoreCase("NETCREDITLPOY")) || (activitydetails.getUserType().equalsIgnoreCase("NETCREDITLPON"))) {
				
				if (activitydetails.getPaymentDetails().equalsIgnoreCase("Pay Offline")) {
					
					String CCE_CustomerNote = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[37]/td")).getText();
					confirmationDetails.setCCE_CustomerNote(CCE_CustomerNote);
					
					
					WebElement tableId = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[34]/td/table"));
					List<WebElement> rowListForNames = tableId.findElements(By.tagName("tr"));
					
					for (int i = 3; i <= rowListForNames.size(); i++) {
						
						String title = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[34]/td/table/tbody/tr["+i+"]/td[3]")).getText();
						paticipanTitle.add(title);	
						
						String fName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[34]/td/table/tbody/tr["+i+"]/td[4]")).getText();
						paticipanFName.add(fName);	
						
						String lName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[34]/td/table/tbody/tr["+i+"]/td[5]")).getText();
						paticipanLName.add(lName);
						
					}
					
					confirmationDetails.setCCE_CusTitle(paticipanTitle);
					confirmationDetails.setCCE_CusFName(paticipanFName);
					confirmationDetails.setCCE_CusLName(paticipanLName);
					
					
					WebElement cancelElement = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[41]/td/ul"));
					List<WebElement> liTypes = cancelElement.findElements(By.tagName("li"));
					
					
					for (int i = 1; i <= liTypes.size(); i++) {
						
						String policyString = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[41]/td/ul/li["+i+"]")).getText();
						cancelPolicy.add(policyString);	
					}
					
					confirmationDetails.setCanellationPolicy(cancelPolicy);
					
					String CCE_Tel_1 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[1].split(": ")[1];
					confirmationDetails.setCCE_Tel_1(CCE_Tel_1);
					
					String CCE_Tel_2;
					try {
						CCE_Tel_2 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[45]/td")).getText().split(": ")[1];
						confirmationDetails.setCCE_Tel_2(CCE_Tel_2);
					} catch (Exception e1) {
						CCE_Tel_2 = "Not Available";
						confirmationDetails.setCCE_Tel_2(CCE_Tel_2);
					}
					
					String CCE_Email_1 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[3].split(": ")[1];
					confirmationDetails.setCCE_Email_1(CCE_Email_1);
					
					String CCE_Email_2 ;
					String CCE_CompanyName;
					
					try {
						CCE_Email_2 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[46]/td")).getText().split(": ")[1];
						confirmationDetails.setCCE_Email_2(CCE_Email_2);
						CCE_CompanyName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[47]/td/div")).getText();
						confirmationDetails.setCCE_CompanyName(CCE_CompanyName);
					} catch (Exception e) {
						CCE_Email_2 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[47]/td")).getText();
						confirmationDetails.setCCE_Email_2(CCE_Email_2);
						CCE_CompanyName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[48]/td/div")).getText();
						confirmationDetails.setCCE_CompanyName(CCE_CompanyName);
					}
					
					
					String CCE_Website = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[4].split(": ")[1];
					confirmationDetails.setCCE_Website(CCE_Website);
					String CCE_Fax = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[2].split(": ")[1];
					confirmationDetails.setCCE_Fax(CCE_Fax);
					
					
					
					
				}else{
					
					String CCE_CustomerNote = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[41]/td")).getText();
					confirmationDetails.setCCE_CustomerNote(CCE_CustomerNote);
					
					
					WebElement tableId = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[34]/td/table"));
					List<WebElement> rowListForNames = tableId.findElements(By.tagName("tr"));
					
					for (int i = 3; i <= rowListForNames.size(); i++) {
						
						String title = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[34]/td/table/tbody/tr["+i+"]/td[3]")).getText();
						paticipanTitle.add(title);	
						
						String fName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[34]/td/table/tbody/tr["+i+"]/td[4]")).getText();
						paticipanFName.add(fName);	
						
						String lName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[34]/td/table/tbody/tr["+i+"]/td[5]")).getText();
						paticipanLName.add(lName);
						
					}
					
					confirmationDetails.setCCE_CusTitle(paticipanTitle);
					confirmationDetails.setCCE_CusFName(paticipanFName);
					confirmationDetails.setCCE_CusLName(paticipanLName);
					
					
					WebElement cancelElement = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[45]/td/ul"));
					List<WebElement> liTypes = cancelElement.findElements(By.tagName("li"));
					
					
					for (int i = 1; i <= liTypes.size(); i++) {
						
						String policyString = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[45]/td/ul/li["+i+"]")).getText();
						cancelPolicy.add(policyString);	
					}
					
					confirmationDetails.setCanellationPolicy(cancelPolicy);
					
					String CCE_Tel_1 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[1].split(": ")[1];
					confirmationDetails.setCCE_Tel_1(CCE_Tel_1);
					
					String CCE_Tel_2;
					try {
						CCE_Tel_2 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[49]/td")).getText().split(": ")[1];
						confirmationDetails.setCCE_Tel_2(CCE_Tel_2);
					} catch (Exception e1) {
						CCE_Tel_2 = "Not Available";
						confirmationDetails.setCCE_Tel_2(CCE_Tel_2);
					}
					
					String CCE_Email_1 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[3].split(": ")[1];
					confirmationDetails.setCCE_Email_1(CCE_Email_1);
					String CCE_Email_2 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[50]/td")).getText().split(": ")[1];
					confirmationDetails.setCCE_Email_2(CCE_Email_2);
					String CCE_Website = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[4].split(": ")[1];
					confirmationDetails.setCCE_Website(CCE_Website);
					String CCE_Fax = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[2].split(": ")[1];
					confirmationDetails.setCCE_Fax(CCE_Fax);
					String CCE_CompanyName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[51]/td/div")).getText();
					confirmationDetails.setCCE_CompanyName(CCE_CompanyName);
					
				
				}
								
				
			}else{
				
				
				WebElement tableId = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[34]/td/table"));
				List<WebElement> rowListForNames = tableId.findElements(By.tagName("tr"));
				
				for (int i = 3; i <= rowListForNames.size(); i++) {
					
					String title = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[34]/td/table/tbody/tr["+i+"]/td[3]")).getText();
					paticipanTitle.add(title);	
					
					String fName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[34]/td/table/tbody/tr["+i+"]/td[4]")).getText();
					paticipanFName.add(fName);	
					
					String lName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[34]/td/table/tbody/tr["+i+"]/td[5]")).getText();
					paticipanLName.add(lName);
					
				}
				
				confirmationDetails.setCCE_CusTitle(paticipanTitle);
				confirmationDetails.setCCE_CusFName(paticipanFName);
				confirmationDetails.setCCE_CusLName(paticipanLName);
				
				//Check DC and Comm TOs
				
				if (!(activitydetails.getUserType().equalsIgnoreCase("DC"))) {
					
					String CCE_CustomerNote = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[41]/td")).getText();
					confirmationDetails.setCCE_CustomerNote(CCE_CustomerNote);
					
					WebElement cancelElement = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[45]/td/ul"));
					List<WebElement> liTypes = cancelElement.findElements(By.tagName("li"));
										
					for (int i = 1; i <= liTypes.size(); i++) {
						
						String policyString = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[45]/td/ul/li["+i+"]")).getText();
						cancelPolicy.add(policyString);	
					}
					
					confirmationDetails.setCanellationPolicy(cancelPolicy);
					
					String CCE_Tel_1 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[1].split(": ")[1];
					confirmationDetails.setCCE_Tel_1(CCE_Tel_1);
					
					String CCE_Tel_2;
					try {
						CCE_Tel_2 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[50]/td")).getText();
						confirmationDetails.setCCE_Tel_2(CCE_Tel_2);
					} catch (Exception e1) {
						CCE_Tel_2 = "Not Available";
						confirmationDetails.setCCE_Tel_2(CCE_Tel_2);
					}
					
					try {
						String CCE_Email_2 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[51]/td")).getText().split(": ")[1];
						confirmationDetails.setCCE_Email_2(CCE_Email_2);
					} catch (Exception e) {
						String CCE_Email_2 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[51]/td")).getText();
						confirmationDetails.setCCE_Email_2(CCE_Email_2);
					}
					
					try {
						String CCE_CompanyName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[52]/td/div")).getText();
						confirmationDetails.setCCE_CompanyName(CCE_CompanyName);
					} catch (Exception e) {
						String CCE_CompanyName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[52]/td/div")).getText();
						confirmationDetails.setCCE_CompanyName(CCE_CompanyName);
					}
					
					
				} else {

					if (activitydetails.getPaymentDetails().equalsIgnoreCase("Pay Offline")) {
						
						//Changing here 45 to 41,
						
						String CCE_CustomerNote = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[37]/td")).getText();
						confirmationDetails.setCCE_CustomerNote(CCE_CustomerNote);					
						
						WebElement cancelElement = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[41]/td/ul"));
						List<WebElement> liTypes = cancelElement.findElements(By.tagName("li"));
											
						for (int i = 1; i <= liTypes.size(); i++) {
							
							String policyString = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[41]/td/ul/li["+i+"]")).getText();
							cancelPolicy.add(policyString);	
						}
						
						confirmationDetails.setCanellationPolicy(cancelPolicy);
						
						String CCE_Tel_1 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[1].split(": ")[1];
						confirmationDetails.setCCE_Tel_1(CCE_Tel_1);
						
						String CCE_Tel_2;
						try {
							CCE_Tel_2 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[46]/td")).getText().split(": ")[1];
							confirmationDetails.setCCE_Tel_2(CCE_Tel_2);
						} catch (Exception e1) {
							CCE_Tel_2 = "Not Available";
							confirmationDetails.setCCE_Tel_2(CCE_Tel_2);
						}
						
						try {
							String CCE_Email_2 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[47]/td")).getText().split(": ")[1];
							confirmationDetails.setCCE_Email_2(CCE_Email_2);
						} catch (Exception e) {
							String CCE_Email_2 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[47]/td")).getText();
							confirmationDetails.setCCE_Email_2(CCE_Email_2);
						}
						
						try {
							String CCE_CompanyName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[48]/td/div")).getText();
							confirmationDetails.setCCE_CompanyName(CCE_CompanyName);
						} catch (Exception e) {
							String CCE_CompanyName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[48]/td/div")).getText();
							confirmationDetails.setCCE_CompanyName(CCE_CompanyName);
						}
						
					}else{
						
						//Pay online
						
						//Changing here 45 to 41,
						
						String CCE_CustomerNote = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[41]/td")).getText();
						confirmationDetails.setCCE_CustomerNote(CCE_CustomerNote);
						
						WebElement cancelElement = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[45]/td/ul"));
						List<WebElement> liTypes = cancelElement.findElements(By.tagName("li"));
											
						for (int i = 1; i <= liTypes.size(); i++) {
							
							String policyString = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[45]/td/ul/li["+i+"]")).getText();
							cancelPolicy.add(policyString);	
						}
						
						confirmationDetails.setCanellationPolicy(cancelPolicy);
						
						String CCE_Tel_1 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[1].split(": ")[1];
						confirmationDetails.setCCE_Tel_1(CCE_Tel_1);
						
						String CCE_Tel_2;
						try {
							CCE_Tel_2 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[50]/td")).getText();
							confirmationDetails.setCCE_Tel_2(CCE_Tel_2);
						} catch (Exception e1) {
							CCE_Tel_2 = "Not Available";
							confirmationDetails.setCCE_Tel_2(CCE_Tel_2);
						}
						
						try {
							String CCE_Email_2 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[51]/td")).getText().split(": ")[1];
							confirmationDetails.setCCE_Email_2(CCE_Email_2);
						} catch (Exception e) {
							String CCE_Email_2 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[51]/td")).getText();
							confirmationDetails.setCCE_Email_2(CCE_Email_2);
						}
						
						try {
							String CCE_CompanyName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[52]/td/div")).getText();
							confirmationDetails.setCCE_CompanyName(CCE_CompanyName);
						} catch (Exception e) {
							String CCE_CompanyName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[52]/td/div")).getText();
							confirmationDetails.setCCE_CompanyName(CCE_CompanyName);
						}
						
						
					}
					
				}
				
				
				
				String CCE_Email_1 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[3].split(": ")[1];
				confirmationDetails.setCCE_Email_1(CCE_Email_1);			
				String CCE_Website = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[4].split(": ")[1];
				confirmationDetails.setCCE_Website(CCE_Website);
				String CCE_Fax = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[2].split(": ")[1];
				confirmationDetails.setCCE_Fax(CCE_Fax);
				
				
			}
			
			
		}else{
			
			confirmationDetails.setCustomerConfirmationMailLoaded(false);	
		}
		
		/////////
		 
		Thread.sleep(500);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");
				
		driver.findElement(By.xpath(".//*[@id='emaillayoutbasedon_customer2']")).click();
		Thread.sleep(1000);
		
		File CVE = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(CVE, new File(""+currentDateforImages+"/"+activitydetails.getScenarioCount()+"_CVEMail.png"));
				
		driver.switchTo().frame("Myemailformat");
		
		try {
			
			if (driver.findElements(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[4]/td[2]")).size() != 0) {
				
				String CVE_LeadPassenger = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[4]/td[2]")).getText();
				confirmationDetails.setCVE_LeadPassenger(CVE_LeadPassenger);
				
				String CVE_IssueDate = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[4]/td[8]")).getText().split(": ")[1];
				confirmationDetails.setCVE_IssueDate(CVE_IssueDate);
				
//				String CVE_Address1 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[5]/td[2]")).getText().split(": ")[1];
//				confirmationDetails.setCVE_Address1(CVE_Address1);
				
				String CVE_City = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[6]/td[2]")).getText();
				confirmationDetails.setCVE_City(CVE_City);
				
				String CVE_BookingNo = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[5]/td[8]")).getText().split(": ")[1];
				confirmationDetails.setCVE_BookingNo(CVE_BookingNo);
				
				String CVE_ActivityName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[9]/td[2]")).getText().split(": ")[1];
				confirmationDetails.setCVE_ActivityName(CVE_ActivityName);
				
				String CVE_bookingStatus = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[10]/td[8]")).getText().split(": ")[1];
				confirmationDetails.setCVE_bookingStatus(CVE_bookingStatus);
				
				
				String CVE_SupplierAdd = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[11]/td[2]")).getText().split(": ")[1];
				confirmationDetails.setCVE_SupplierAdd(CVE_SupplierAdd);
				
				String CVE_SupplierTP = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[12]/td[2]")).getText().split(": ")[1];
				confirmationDetails.setCVE_SupplierTP(CVE_SupplierTP);
				
				String CVE_ProgramCity = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[11]/td[4]")).getText().split(": ")[1];
				confirmationDetails.setCVE_ProgramCity(CVE_ProgramCity);
				
				
				
				String CVE_ActivityTransferDes = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[16]/td[1]")).getText();
				confirmationDetails.setCVE_ActivityTransferDes(CVE_ActivityTransferDes);
				
				String CVE_Duration = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[16]/td[2]")).getText();
				confirmationDetails.setCVE_Duration(CVE_Duration);
				
				String CVE_RatePlan = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[16]/td[3]")).getText();
				confirmationDetails.setCVE_RatePlan(CVE_RatePlan);
				
				String CVE_Qty = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[16]/td[4]")).getText();
				confirmationDetails.setCVE_Qty(CVE_Qty);
				
				String CVE_ServiceDate = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[16]/td[5]")).getText();
				confirmationDetails.setCVE_ServiceDate(CVE_ServiceDate);
				
//				String CVE_CustomerNotes = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[24]/td[1]")).getText();
//				confirmationDetails.setCVE_CustomerNotes(CVE_CustomerNotes);
				
				String CvE_Tel_1 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[1].split(": ")[1];
				confirmationDetails.setCVE_Tel_1(CvE_Tel_1);		
				String CvE_Email_1 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[3].split(": ")[1];
				confirmationDetails.setCVE_Email_1(CvE_Email_1);	
				String CvE_Website = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[4].split(": ")[1];
				confirmationDetails.setCVE_Website(CvE_Website);
				String CvE_Fax = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[2].split(": ")[1];
				confirmationDetails.setCVE_Fax(CvE_Fax);
				
				
				
			}else{
				
				confirmationDetails.setCustomerVoucherMailLoaded(false);
			}
			
		} catch (Exception e) {
			confirmationDetails.setCustomerVoucherMailLoaded(false);
		}
		
		
		
		
		//String CVE_CustomerNotes = driver.findElement(By.xpath("")).getText();
		
		/////////////////
		
		Thread.sleep(500);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");
		
		driver.findElement(By.xpath(".//*[@id='emaillayoutbasedon_supplier1']")).click();
		Thread.sleep(1000);
		
		File supplier = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(supplier, new File(""+currentDateforImages+"/"+activitydetails.getScenarioCount()+"_SupplierMail.png"));
			
		driver.switchTo().frame("Myemailformat");
		
		if (driver.findElements(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[2]/td/div")).size() != 0) {
			
			String SupplierMAIL_BookingNo = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[2]/td/div")).getText().split(": ")[1];
			confirmationDetails.setSupplierMAIL_BookingNo(SupplierMAIL_BookingNo);
			
			String issueDate = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[3]/td[8]")).getText().split(": ")[1];
			confirmationDetails.setSupplierMAIL_IssueDate(issueDate);
			
			String SupplierMAIL_Tel_1 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[1].split(": ")[1];
			confirmationDetails.setSupplierMAIL_Tel_1(SupplierMAIL_Tel_1);
			String SupplierMAIL_Email_1 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[3].split(": ")[1];
			confirmationDetails.setSupplierMAIL_Email_1(SupplierMAIL_Email_1);
			String SupplierMAIL_Website = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[4].split(": ")[1];
			confirmationDetails.setSupplierMAIL_Website(SupplierMAIL_Website);
			String SupplierMAIL_Fax = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[2].split(": ")[1];
			confirmationDetails.setSupplierMAIL_Fax(SupplierMAIL_Fax);
			String SupplierMAIL_CompanyName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[30]/td/div")).getText();
			confirmationDetails.setSupplierMAIL_CompanyName(SupplierMAIL_CompanyName);
			
			if(driver.getPageSource().contains("confirmation/acknowledgement")){
				
				String SupplierMAIL_ActivityName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[11]/td[2]")).getText().split(": ")[1];
				confirmationDetails.setSupplierMAIL_ActivityName(SupplierMAIL_ActivityName);
				
				String SupplierMAIL_BookingStatus = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[12]/td[2]")).getText().split(": ")[1];
				confirmationDetails.setSupplierMAIL_BookingStatus(SupplierMAIL_BookingStatus);
				
				String SupplierMAIL_ProgramCity = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[12]/td[8]")).getText().split(": ")[1];
				confirmationDetails.setSupplierMAIL_ProgramCity(SupplierMAIL_ProgramCity);
				
				String SupplierMAIL_Duration = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[15]/td[2]")).getText();
				confirmationDetails.setSupplierMAIL_Duration(SupplierMAIL_Duration);
				
				String SupplierMAIL_QTY = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[15]/td[3]")).getText();
				confirmationDetails.setSupplierMAIL_QTY(SupplierMAIL_QTY);
				
				String SupplierMAIL_ServicyDate = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[15]/td[4]")).getText();
				confirmationDetails.setSupplierMAIL_ServicyDate(SupplierMAIL_ServicyDate);
				
				String SupplierMAIL_ActivityRate = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[15]/td[5]")).getText().split("\\.")[0];
				confirmationDetails.setSupplierMAIL_ActivityRate(SupplierMAIL_ActivityRate);
				
				String SupplierMAIL_CustomerTP = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[19]/td[2]")).getText().split(": ")[1];
				confirmationDetails.setSupplierMAIL_CustomerTP(SupplierMAIL_CustomerTP);
				
				String SupplierMAIL_PN = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[18]/td[1]")).getText();
				confirmationDetails.setSupplierMAIL_PassengerName(SupplierMAIL_PN);
				
			}else{
				
				String SupplierMAIL_ActivityName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[11]/td[2]")).getText().split(": ")[1];
				confirmationDetails.setSupplierMAIL_ActivityName(SupplierMAIL_ActivityName);
				
				String SupplierMAIL_BookingStatus = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[12]/td[2]")).getText().split(": ")[1];
				confirmationDetails.setSupplierMAIL_BookingStatus(SupplierMAIL_BookingStatus);
				
				String SupplierMAIL_ProgramCity = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[12]/td[8]")).getText().split(": ")[1];
				confirmationDetails.setSupplierMAIL_ProgramCity(SupplierMAIL_ProgramCity);
				
				String SupplierMAIL_Duration = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[15]/td[2]")).getText();
				confirmationDetails.setSupplierMAIL_Duration(SupplierMAIL_Duration);
				
				String SupplierMAIL_QTY = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[15]/td[3]")).getText();
				confirmationDetails.setSupplierMAIL_QTY(SupplierMAIL_QTY);
				
				String SupplierMAIL_ServicyDate = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[15]/td[4]")).getText();
				confirmationDetails.setSupplierMAIL_ServicyDate(SupplierMAIL_ServicyDate);
				
				String SupplierMAIL_ActivityRate = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[15]/td[5]")).getText().split("\\.")[0];
				confirmationDetails.setSupplierMAIL_ActivityRate(SupplierMAIL_ActivityRate);
				
				String SupplierMAIL_CustomerTP = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[19]/td[2]")).getText().split(": ")[1];
				confirmationDetails.setSupplierMAIL_CustomerTP(SupplierMAIL_CustomerTP);
				
				String SupplierMAIL_PN = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[18]/td[1]")).getText();
				confirmationDetails.setSupplierMAIL_PassengerName(SupplierMAIL_PN);
				
			}
		
		}else{
			
			confirmationDetails.setSupplier(false);
		}
		
		
		
		
		
		
		return confirmationDetails;
		
	}
	
	
	public Quotation getQuotationMail(WebDriver driver2) throws InterruptedException{
		
		Quotation quotationDetails = new Quotation();
		WebDriverWait wait = new WebDriverWait(driver, 200);	
		DateFormat dateFormatcurrentDate = new SimpleDateFormat("dd-MM-yyyy");
		Calendar cal = Calendar.getInstance();
		String currentDate = dateFormatcurrentDate.format(cal.getTime());
		String parentHandle = null;
						
		try {
			
			driver.get(""+PG_Properties.getProperty("Quotation")+""+currentDate+"");
						
			WebElement mailTable = driver.findElement(By.xpath(".//*[@id='filetable']"));
			List<WebElement> trElement = mailTable.findElements(By.tagName("tr"));
			
			for (int i = 4; i < trElement.size(); i++) {
				
				String quotationNumber = driver.findElement(By.xpath(".//*[@id='filetable']/tbody/tr["+i+"]/td[2]/a")).getText();
				//activitydetails.getQuote_QuotationNo()
				if (quotationNumber.contains(activitydetails.getQuote_QuotationNo())) {
					
					driver.findElement(By.xpath(".//*[@id='filetable']/tbody/tr["+i+"]/td[2]/a")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath(".//*[@id='filetable']/tbody/tr[4]/td[2]/a")).click();					
					Thread.sleep(2000);
					
					parentHandle = driver.getWindowHandle();
					for (String winHandle : driver.getWindowHandles()) {
				        driver.switchTo().window(winHandle); 
				    }
					
				    Thread.sleep(3000);
				    
				    File cCEQ = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			        FileUtils.copyFile(cCEQ, new File(""+currentDateforImages+"/"+activitydetails.getScenarioCount()+"_QuotationMail.png"));
					
					String companyTel = driver.findElement(By.xpath("html/body/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[1].split(": ")[1];
					quotationDetails.setCompanyTel(companyTel);
					
					String companyFax = driver.findElement(By.xpath("html/body/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[2].split(": ")[1];
					quotationDetails.setCompanyFax(companyFax);
					
					String companyEmail = driver.findElement(By.xpath("html/body/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[3].split(": ")[1];
					quotationDetails.setCompanyEmail(companyEmail);
					
					String companyWeb = driver.findElement(By.xpath("html/body/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[4].split(": ")[1];
					quotationDetails.setCompanyWeb(companyWeb);
					
					String companyName = driver.findElement(By.xpath("html/body/table/tbody/tr[36]/td/div/strong")).getText();
					quotationDetails.setCompanyName(companyName);
					
					
					String refNo = driver.findElement(By.xpath("html/body/table/tbody/tr[2]/td/div/strong")).getText().split("\\.")[1].replaceAll(" ", "");
					quotationDetails.setRefNo(refNo);
					
					String referenceName = driver.findElement(By.xpath("html/body/table/tbody/tr[4]/td/strong")).getText().replaceAll(" ", "");
					quotationDetails.setReferenceName(referenceName);
					
					String activityName = driver.findElement(By.xpath("html/body/table/tbody/tr[13]/td[2]")).getText().split(": ")[1];
					quotationDetails.setActivityName(activityName);
					
					String activityDate = driver.findElement(By.xpath("html/body/table/tbody/tr[14]/td[2]")).getText().split(": ")[1];
					quotationDetails.setActivityDate(activityDate);
					
					String periodType = driver.findElement(By.xpath("html/body/table/tbody/tr[15]/td[2]")).getText().split(": ")[1];
					quotationDetails.setPeriodType(periodType);
					
					String rateType = driver.findElement(By.xpath("html/body/table/tbody/tr[16]/td[2]")).getText().split(": ")[1];
					quotationDetails.setRateType(rateType);
					
					String bookingStatus = driver.findElement(By.xpath("html/body/table/tbody/tr[17]/td[2]")).getText().split(": ")[1];
					quotationDetails.setBookingStatus(bookingStatus);
					
					String personRate = driver.findElement(By.xpath("html/body/table/tbody/tr[18]/td[2]")).getText().split(": ")[1].split("\\.")[0];
					quotationDetails.setPersonRate(personRate);
					
					String qty = driver.findElement(By.xpath("html/body/table/tbody/tr[19]/td[2]")).getText().split(": ")[1];
					quotationDetails.setQty(qty);
					
					String currency = driver.findElement(By.xpath("html/body/table/tbody/tr[20]/td[1]/strong")).getText().split(" ")[1];
					quotationDetails.setCurrency(currency);
					
					String totalRate = driver.findElement(By.xpath("html/body/table/tbody/tr[20]/td[2]/strong")).getText().split(": ")[1].split("\\.")[0];
					quotationDetails.setTotalRate(totalRate);
					
					
					
					String currency2 = driver.findElement(By.xpath("html/body/table/tbody/tr[22]/td[7]/table/tbody/tr[1]/td[2]")).getText();
					quotationDetails.setCurrency2(currency2);
					
					String subTotal = driver.findElement(By.xpath("html/body/table/tbody/tr[22]/td[7]/table/tbody/tr[2]/td[2]")).getText().split("\\.")[0];
					quotationDetails.setSubTotal(subTotal);
					String tax = driver.findElement(By.xpath("html/body/table/tbody/tr[22]/td[7]/table/tbody/tr[3]/td[2]")).getText().split("\\.")[0];
					quotationDetails.setTax(tax);
					String totalValue = driver.findElement(By.xpath("html/body/table/tbody/tr[22]/td[7]/table/tbody/tr[4]/td[2]")).getText().split("\\.")[0];
					quotationDetails.setTotalValue(totalValue);
					String amountPayNow = driver.findElement(By.xpath("html/body/table/tbody/tr[22]/td[7]/table/tbody/tr[5]/td[2]")).getText().split("\\.")[0];
					quotationDetails.setAmountPayNow(amountPayNow);
					String payAtCheckIn = driver.findElement(By.xpath("html/body/table/tbody/tr[22]/td[7]/table/tbody/tr[6]/td[2]")).getText().split("\\.")[0];
					quotationDetails.setPayAtCheckIn(payAtCheckIn);
					
					
					String subTotal2 = driver.findElement(By.xpath("html/body/table/tbody/tr[24]/td[7]/table/tbody/tr[2]/td[2]")).getText().split("\\.")[0];
					quotationDetails.setSubTotal2(subTotal2);
					String tax2 = driver.findElement(By.xpath("html/body/table/tbody/tr[24]/td[7]/table/tbody/tr[3]/td[2]")).getText().split("\\.")[0];
					quotationDetails.setTax2(tax2);
					String totalBookingValue2 = driver.findElement(By.xpath("html/body/table/tbody/tr[24]/td[7]/table/tbody/tr[5]/td[2]")).getText().split("\\.")[0];
					quotationDetails.setTotalBookingValue2(totalBookingValue2);
					String amountPayNow2 = driver.findElement(By.xpath("html/body/table/tbody/tr[24]/td[7]/table/tbody/tr[6]/td[2]")).getText().split("\\.")[0];
					quotationDetails.setAmountPayNow2(amountPayNow2);
					String payAtCheckIn2 = driver.findElement(By.xpath("html/body/table/tbody/tr[24]/td[7]/table/tbody/tr[7]/td[2]")).getText().split("\\.")[0];
					quotationDetails.setPayAtCheckIn2(payAtCheckIn2);
					
					
					String cus_FName = driver.findElement(By.xpath("html/body/table/tbody/tr[27]/td[2]")).getText().split(": ")[1];
					quotationDetails.setCus_FName(cus_FName);
					String cus_LName = driver.findElement(By.xpath("html/body/table/tbody/tr[27]/td[8]")).getText().split(": ")[1];
					quotationDetails.setCus_LName(cus_LName);
					String cusTP = driver.findElement(By.xpath("html/body/table/tbody/tr[29]/td[2]")).getText().split(": ")[1];
					quotationDetails.setCusTP(cusTP);
					String cusEmerGencyCon = driver.findElement(By.xpath("html/body/table/tbody/tr[29]/td[8]")).getText().split(": ")[1];
					quotationDetails.setCusEmerGencyCon(cusEmerGencyCon);
					String cusEmail = driver.findElement(By.xpath("html/body/table/tbody/tr[32]/td[2]")).getText().split(": ")[1];
					quotationDetails.setCusEmail(cusEmail);
					String cusAddress = driver.findElement(By.xpath("html/body/table/tbody/tr[28]/td[2]")).getText().split(": ")[1];
					quotationDetails.setCusAddress(cusAddress);
					String cusAddress_2 = driver.findElement(By.xpath("html/body/table/tbody/tr[28]/td[8]")).getText().split(": ")[1];
					quotationDetails.setCusAddress_2(cusAddress_2);
					String cuscountry = driver.findElement(By.xpath("html/body/table/tbody/tr[30]/td[2]")).getText().split(": ")[1];
					quotationDetails.setCuscountry(cuscountry);
					String cuscity = driver.findElement(By.xpath("html/body/table/tbody/tr[31]/td[8]")).getText().split(": ")[1];
					quotationDetails.setCuscity(cuscity);				
					
					if (activitydetails.getPaymentPage_country().equalsIgnoreCase("USA") || activitydetails.getPaymentPage_country().equalsIgnoreCase("Canada") || activitydetails.getPaymentPage_country().equalsIgnoreCase("Australia")) {
						
						String cusState = driver.findElement(By.xpath("html/body/table/tbody/tr[30]/td[8]")).getText();
						quotationDetails.setCusState(cusState);
						String cusPostalCode = driver.findElement(By.xpath("html/body/table/tbody/tr[31]/td[2]")).getText();
						quotationDetails.setCusPostalCode(cusPostalCode);
						
					}	
					
					
				    break;
				}
				
				
								
			}
			
			driver.close(); // close newly opened window when done with it
		    driver.switchTo().window(parentHandle); // switch back to the original window
			
		    Thread.sleep(6000);
		    
			
			
			
		} catch (Exception e) {
			quotationDetails.setQuotationMailSent(false);
		}
		
		
		
		return quotationDetails;
	}
}
