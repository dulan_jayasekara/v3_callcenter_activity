package com.types;

public enum TourOperatorType {

	DC,NETCASH, NETCREDITLPOY, NETCREDITLPON, COMCASH, COMCREDITLPOY, COMCREDITLPONO, NONE;

	public static TourOperatorType getTourOperatorType(String touroperator) {

		if (touroperator.trim().equalsIgnoreCase("NETCASH"))
			return TourOperatorType.NETCASH;
		else if (touroperator.trim().equalsIgnoreCase("NETCREDITLPOY"))
			return TourOperatorType.NETCREDITLPOY;
		else if (touroperator.trim().equalsIgnoreCase("NETCREDITLPON"))
			return TourOperatorType.NETCREDITLPON;
		else if (touroperator.trim().equalsIgnoreCase("COMCASH"))
			return TourOperatorType.COMCASH;
		else if (touroperator.trim().equalsIgnoreCase("COMCREDITLPOY"))
			return TourOperatorType.COMCREDITLPOY;
		else if (touroperator.trim().equalsIgnoreCase("COMCREDITLPONO"))
			return TourOperatorType.COMCREDITLPONO;
		else if (touroperator.trim().equalsIgnoreCase("DC"))
			return TourOperatorType.DC;
		else
			return TourOperatorType.NONE;
		
	}

}
