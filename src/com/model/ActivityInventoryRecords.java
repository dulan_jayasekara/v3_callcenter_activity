package com.model;

import com.types.TourOperatorType;

public class ActivityInventoryRecords {
	
	private String scenarioCount;
	private String activityName;
	private String supplierCurrency;
	private String ResultsPage_SubTotal	;
	private String ResultsPage_Taxes;	
	private String ResultsPage_TotalPayable;	
	
	private String PaymentPage_SubTotal;	
	private String PaymentPage_Taxes;	
	private String PaymentPage_TotalPayable;	
	private String PaymentPage_TotalGrossBookingValue;	
	private String PaymentPage_TotalTax	;
	private String PaymentPage_TotalPackageValue;	
	private String PaymentPage_AmountProcessed;
	private String PaymentPage_AmountCheckIn;
	
	private String ConfirmationPage_SubTotal;	
	private String ConfirmationPage_Taxes;
	private String ConfirmationPage_TotalPayable;	
	private String Confirmation_TotalGrossBookingValue;	
	private String ConfirmationPage_TotalTax;	
	private String ConfirmationPage_TotalPackageValue;	
	private String ConfirmationPage_AmountProcessed;
	private String ConfirmationPage_AmountCheckIn;
	
	
	private String StandardCancellation_based;
	private String StandardCancellation_value;
	private String Noshow_based;
	private String Noshow_value;
	private String cancellation_Apply;
	private String cancellation_Buffer;
	
	private String inventoryType;
	private String Supplier_Name;
	private String Supplier_Add;
	private String Supplier_TP;
	private String Profitmarkup;
	
	public String paymentDetails;
	private String creditCard_Fee;
	public String inventoryCount;
	public String ActivityDescription;
	
	private TourOperatorType userTypeDC_B2B;
	public String toActivityNetRate;
	public String toActivityPM;
	public String toActivity_SalesTaxType;
	public String toActivity_SalesTaxValue;
	public String toActivity_MisFeeType;
	public String toActivity_MisFeeValue;
	
	public String activeManagerOverwrite;
	public String discountYesORNo;;
	
	
	
	public String getDiscountYesORNo() {
		return discountYesORNo;
	}
	public void setDiscountYesORNo(String discountYesORNo) {
		this.discountYesORNo = discountYesORNo;
	}
	public String getActiveManagerOverwrite() {
		return activeManagerOverwrite;
	}
	public void setActiveManagerOverwrite(String activeManagerOverwrite) {
		this.activeManagerOverwrite = activeManagerOverwrite;
	}
	public String getToActivityNetRate() {
		return toActivityNetRate;
	}
	public void setToActivityNetRate(String toActivityNetRate) {
		this.toActivityNetRate = toActivityNetRate;
	}
	public String getToActivityPM() {
		return toActivityPM;
	}
	public void setToActivityPM(String toActivityPM) {
		this.toActivityPM = toActivityPM;
	}
	public String getToActivity_SalesTaxType() {
		return toActivity_SalesTaxType;
	}
	public void setToActivity_SalesTaxType(String toActivity_SalesTaxType) {
		this.toActivity_SalesTaxType = toActivity_SalesTaxType;
	}
	public String getToActivity_SalesTaxValue() {
		return toActivity_SalesTaxValue;
	}
	public void setToActivity_SalesTaxValue(String toActivity_SalesTaxValue) {
		this.toActivity_SalesTaxValue = toActivity_SalesTaxValue;
	}
	public String getToActivity_MisFeeType() {
		return toActivity_MisFeeType;
	}
	public void setToActivity_MisFeeType(String toActivity_MisFeeType) {
		this.toActivity_MisFeeType = toActivity_MisFeeType;
	}
	public String getToActivity_MisFeeValue() {
		return toActivity_MisFeeValue;
	}
	public void setToActivity_MisFeeValue(String toActivity_MisFeeValue) {
		this.toActivity_MisFeeValue = toActivity_MisFeeValue;
	}
	
	public TourOperatorType getUserTypeDC_B2B() {
		return userTypeDC_B2B;
	}
	public void setUserTypeDC_B2B(String userTypeDC_B2B) {
		this.userTypeDC_B2B = TourOperatorType.getTourOperatorType(userTypeDC_B2B);
	}
	public String getCreditCard_Fee() {
		return creditCard_Fee;
	}
	public void setCreditCard_Fee(String creditCard_Fee) {
		this.creditCard_Fee = creditCard_Fee;
	}
	public String getActivityDescription() {
		return ActivityDescription;
	}
	public void setActivityDescription(String activityDescription) {
		ActivityDescription = activityDescription;
	}
	public String getInventoryCount() {
		return inventoryCount;
	}
	public void setInventoryCount(String inventoryCount) {
		this.inventoryCount = inventoryCount;
	}
	public String getPaymentDetails() {
		return paymentDetails;
	}
	public void setPaymentDetails(String paymentDetails) {
		this.paymentDetails = paymentDetails;
	}
	public String getProfitmarkup() {
		return Profitmarkup;
	}
	public void setProfitmarkup(String profitmarkup) {
		Profitmarkup = profitmarkup;
	}
	public String getSupplier_Name() {
		return Supplier_Name;
	}
	public void setSupplier_Name(String supplier_Name) {
		Supplier_Name = supplier_Name;
	}
	public String getSupplier_Add() {
		return Supplier_Add;
	}
	public void setSupplier_Add(String supplier_Add) {
		Supplier_Add = supplier_Add;
	}
	public String getSupplier_TP() {
		return Supplier_TP;
	}
	public void setSupplier_TP(String supplier_TP) {
		Supplier_TP = supplier_TP;
	}
	public String getInventoryType() {
		return inventoryType;
	}
	public void setInventoryType(String inventoryType) {
		this.inventoryType = inventoryType;
	}
	public String getCancellation_Apply() {
		return cancellation_Apply;
	}
	public void setCancellation_Apply(String cancellation_Apply) {
		this.cancellation_Apply = cancellation_Apply;
	}
	public String getCancellation_Buffer() {
		return cancellation_Buffer;
	}
	public void setCancellation_Buffer(String cancellation_Buffer) {
		this.cancellation_Buffer = cancellation_Buffer;
	}
	public String getStandardCancellation_based() {
		return StandardCancellation_based;
	}
	public void setStandardCancellation_based(String standardCancellation_based) {
		StandardCancellation_based = standardCancellation_based;
	}
	public String getStandardCancellation_value() {
		return StandardCancellation_value;
	}
	public void setStandardCancellation_value(String standardCancellation_value) {
		StandardCancellation_value = standardCancellation_value;
	}
	public String getNoshow_based() {
		return Noshow_based;
	}
	public void setNoshow_based(String noshow_based) {
		Noshow_based = noshow_based;
	}
	public String getNoshow_value() {
		return Noshow_value;
	}
	public void setNoshow_value(String noshow_value) {
		Noshow_value = noshow_value;
	}
	public String getResultsPage_SubTotal() {
		return ResultsPage_SubTotal;
	}
	public void setResultsPage_SubTotal(String resultsPage_SubTotal) {
		ResultsPage_SubTotal = resultsPage_SubTotal;
	}
	public String getResultsPage_Taxes() {
		return ResultsPage_Taxes;
	}
	public void setResultsPage_Taxes(String resultsPage_Taxes) {
		ResultsPage_Taxes = resultsPage_Taxes;
	}
	public String getResultsPage_TotalPayable() {
		return ResultsPage_TotalPayable;
	}
	public void setResultsPage_TotalPayable(String resultsPage_TotalPayable) {
		ResultsPage_TotalPayable = resultsPage_TotalPayable;
	}
	public String getPaymentPage_SubTotal() {
		return PaymentPage_SubTotal;
	}
	public void setPaymentPage_SubTotal(String paymentPage_SubTotal) {
		PaymentPage_SubTotal = paymentPage_SubTotal;
	}
	public String getPaymentPage_Taxes() {
		return PaymentPage_Taxes;
	}
	public void setPaymentPage_Taxes(String paymentPage_Taxes) {
		PaymentPage_Taxes = paymentPage_Taxes;
	}
	public String getPaymentPage_TotalPayable() {
		return PaymentPage_TotalPayable;
	}
	public void setPaymentPage_TotalPayable(String paymentPage_TotalPayable) {
		PaymentPage_TotalPayable = paymentPage_TotalPayable;
	}
	public String getPaymentPage_TotalGrossBookingValue() {
		return PaymentPage_TotalGrossBookingValue;
	}
	public void setPaymentPage_TotalGrossBookingValue(
			String paymentPage_TotalGrossBookingValue) {
		PaymentPage_TotalGrossBookingValue = paymentPage_TotalGrossBookingValue;
	}
	public String getPaymentPage_TotalTax() {
		return PaymentPage_TotalTax;
	}
	public void setPaymentPage_TotalTax(String paymentPage_TotalTax) {
		PaymentPage_TotalTax = paymentPage_TotalTax;
	}
	public String getPaymentPage_TotalPackageValue() {
		return PaymentPage_TotalPackageValue;
	}
	public void setPaymentPage_TotalPackageValue(
			String paymentPage_TotalPackageValue) {
		PaymentPage_TotalPackageValue = paymentPage_TotalPackageValue;
	}
	public String getPaymentPage_AmountProcessed() {
		return PaymentPage_AmountProcessed;
	}
	public void setPaymentPage_AmountProcessed(String paymentPage_AmountProcessed) {
		PaymentPage_AmountProcessed = paymentPage_AmountProcessed;
	}
	public String getPaymentPage_AmountCheckIn() {
		return PaymentPage_AmountCheckIn;
	}
	public void setPaymentPage_AmountCheckIn(String paymentPage_AmountCheckIn) {
		PaymentPage_AmountCheckIn = paymentPage_AmountCheckIn;
	}
	public String getConfirmationPage_SubTotal() {
		return ConfirmationPage_SubTotal;
	}
	public void setConfirmationPage_SubTotal(String confirmationPage_SubTotal) {
		ConfirmationPage_SubTotal = confirmationPage_SubTotal;
	}
	public String getConfirmationPage_Taxes() {
		return ConfirmationPage_Taxes;
	}
	public void setConfirmationPage_Taxes(String confirmationPage_Taxes) {
		ConfirmationPage_Taxes = confirmationPage_Taxes;
	}
	public String getConfirmationPage_TotalPayable() {
		return ConfirmationPage_TotalPayable;
	}
	public void setConfirmationPage_TotalPayable(
			String confirmationPage_TotalPayable) {
		ConfirmationPage_TotalPayable = confirmationPage_TotalPayable;
	}
	public String getConfirmation_TotalGrossBookingValue() {
		return Confirmation_TotalGrossBookingValue;
	}
	public void setConfirmation_TotalGrossBookingValue(
			String confirmation_TotalGrossBookingValue) {
		Confirmation_TotalGrossBookingValue = confirmation_TotalGrossBookingValue;
	}
	public String getConfirmationPage_TotalTax() {
		return ConfirmationPage_TotalTax;
	}
	public void setConfirmationPage_TotalTax(String confirmationPage_TotalTax) {
		ConfirmationPage_TotalTax = confirmationPage_TotalTax;
	}
	public String getConfirmationPage_TotalPackageValue() {
		return ConfirmationPage_TotalPackageValue;
	}
	public void setConfirmationPage_TotalPackageValue(
			String confirmationPage_TotalPackageValue) {
		ConfirmationPage_TotalPackageValue = confirmationPage_TotalPackageValue;
	}
	public String getConfirmationPage_AmountProcessed() {
		return ConfirmationPage_AmountProcessed;
	}
	public void setConfirmationPage_AmountProcessed(
			String confirmationPage_AmountProcessed) {
		ConfirmationPage_AmountProcessed = confirmationPage_AmountProcessed;
	}
	public String getConfirmationPage_AmountCheckIn() {
		return ConfirmationPage_AmountCheckIn;
	}
	public void setConfirmationPage_AmountCheckIn(
			String confirmationPage_AmountCheckIn) {
		ConfirmationPage_AmountCheckIn = confirmationPage_AmountCheckIn;
	}
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	public String getSupplierCurrency() {
		return supplierCurrency;
	}
	public void setSupplierCurrency(String supplierCurrency) {
		this.supplierCurrency = supplierCurrency;
	}
	public String getScenarioCount() {
		return scenarioCount;
	}
	public void setScenarioCount(String scenarioCount) {
		this.scenarioCount = scenarioCount;
	}
	
	

}
