package com.model;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.controller.PG_Properties;

public class RateChecker {
	
private HashMap<String, String> CurrencyMap = new HashMap<String, String>();
private WebDriver                driver;
private boolean                 Validator   = true;
private int                      Count = 0;


public Map<String, String> getExchangeRates(WebDriver driver)throws IOException
{  
	
	driver.get(PG_Properties.getProperty("Baseurl") + "/admin/setup/CurrencyExchangeRateSetupPage.do?module=admin");
		
    for(int count = 0; Validator; count++)
    {
    	try {
    		
    		String Key   = driver.findElement(By.name("currencyCode["+count+"]")).getAttribute("value");
    		String Value = driver.findElement(By.name("exchangeRate["+count+"]")).getAttribute("value");
    		CurrencyMap.put(Key, Value);
    	
			
		} catch (Exception e) {
			
			Validator = false;
			Count     = count;
			
		}
    		
   
    	
    }
    
	System.out.println(Count);
	
	
	return CurrencyMap;
}




}

