package com.model;

import java.sql.Date;
import java.util.ArrayList;

public class ActivityDetails {
	
	private boolean isActivityResultsAvailble = true;
	private boolean isBookingEngineLoaded = true;
	private boolean isAddToCart = true;
	private boolean isProgramsAvailabilityActivityTypes = true;
	private boolean isActivityRemoved = true;
	private boolean isCancelPolicy = true;
	private boolean isMoreInfo = true;
	
	private boolean isCountryLoaded = true;
	private boolean isCountryListLoaded = true;
	private boolean isCityLoaded = true;
	private boolean isDateFromLoaded = true;
	private boolean isDateToLoaded = true;
	private boolean isAdultsLoaded = true;
	private boolean isChildsLoaded = true;
	private boolean isCalenderAvailble = true;
	private String showAdd;
	private String HideAdd;
	private boolean isActivityType = true;
	private boolean isProgramCat = true;
	private boolean isPreCurrency = true;
	private boolean isPromoCode = true;
	private String scenarioCount;
	private String showResultsAdd;
	private String HideResultsAdd;
	private boolean isResultsProgramCat = true;
	private boolean isResultsPreCurrency = true;
	private boolean isResultsPromoCode = true;
	private boolean isPriceRangeFilter = true;
	private boolean isProgramAvailablity = true;
	private boolean isActivityNameFilter = true;
	
	private boolean isSearchAgainCityLoaded = true;
	private boolean isSearchAgainDateFromLoaded = true;
	private boolean isSearchAgainDateToLoaded = true;
	private boolean isAdultLoaded = true;
	private boolean isChildLoaded = true;
	private boolean isSearchAgainProgramCat = true;
	private boolean isSearchAgainPreCurrency = true;
	private boolean isSearchAgainPromoCode = true;
	
	private String traceId;
	private String activityCurrency;
	private String actDescription;
	private String activityRate;
	private String activityName_result;
	private String activitySession;
	private String activityRateType;
	private String activity_qty;
	private String status;
	private ArrayList<String> cancelPolicy;
	private ArrayList<String> paymentCancelPolicy;
	private ArrayList<String> confirmCancelPolicy;
	private String paymentPageStatus;
	private long timeLoadpayment;
	private long timeConfirmBooking;
	private long timeSearchButtClickTime;
	private long timeResultsAvailable;
	private long timeAddedtoCart;
	private long timeAailableClick;
	private long timeFirstSearch, timeFirstResults;
	private String filtercurrency;
	private String CurrentDate;
	private String CurrentDate_2;
	private String changed_DateFrom;
	private String changed_DateTo;
	private int daysCount;
	private int activityAvailableCount;
	private int activityOnReqCount;
	private int activityTotalCount;
	private String mainRate;
	private String cartActivityName;
	private String cartDate;
	private String cartCurrency;
	private String cartFullAmount;
	private ArrayList<Integer> beforeSorting = null; 
	private ArrayList<Integer> afterSorting = null;
	private String userType;
	
	private String reservationNo = "Not Available";
	private String supplierConNo;
	private String bookingStatus;
	private String bookingPageActivityName;
	
	private String paymentDetails;
	private String paymentPage_subTotal;
	private String paymentPage_TotalTax;
	private String paymentPage_TotalActivityBookingValue;	
	private String paymentGatewayTotalAmount;
	private String paymentPageTax;
	private String paymentPageGrossBookingValue;
	private String paymentPageTotalBookingValue;
	private String paymentPageAmountbeingProcessed;
	private String paymentPageAmountduatCheckIn;
	private String paymentPage_Title;
	private String paymentPage_FName;
	private String paymentPage_LName;
	private String paymentPage_TP;
	private String paymentPage_Email;
	private String paymentPage_address;
	private String paymentPage_address_2;
	private String paymentPage_country;
	private String paymentPage_city;
	private String paymentPage_state;
	private String paymentPage_PostelCode;
	private String paymentPage_Currency_1;
	private String paymentPage_Currency_2;
	private String paymentPage_ActivityName;
	
	private String payOnline_MainTotalBookingValue;
	private String payOnline_MainTotalBookingCurrency;
	private String payOnline_GrossValue;
	private String payOnline_TotalTax;
	private String payOnline_TotalBooking;
	private String payOnline_AmountProNow;
	private String payOnline_Due;
	
	private String paymentsPage_AgentCommission;
	private String paymentsPage_DiscountValue1;
	private String paymentsPage_DiscountValue2;
	
	private String customerNotes;
	private String internalNotes;
	private String programNotes;

	private boolean isResultsAvailable;
	private boolean isProgramDisplayAvailable;
	
	private String resultsPage_destinaton;
	private String resultsPage_dateFrom;
	private String resultsPage_dateTo;
	private String resultsPage_adults;
	private String resultsPage__childs;
	private ArrayList<String> resultsPage_ageOfChilds;
	private String resultsPage_SubTotal;
	private String resultsPage_Taxes;
	private String resultsPage_TotalPayable;
	private String resultsPage_SubTotal_Currency;
	private String resultsPage_Taxes_Currency;
	private String resultsPage_TotalPayable_Currency;
	private String resultsPage_QTY;
	private String resultsPage_BookingStatus;
	
	private String confirmationPage_Currency;
	private String confirmationPage_grossValue;
	private String confirmationPage_tax;
	private String confirmationPage_activityBookingValue;	
	private String confirmationPage_TotalGrossValue;
	private String confirmationPage_TotalTax;
	private String confirmationPage_TotalPackageBookingValue;
	private String confirmationPage_AmountBeingProcessed;
	private String confirmationPage_AmountDueAtCheckin;
	private String confirmationPage_AgentCommission;
	private String confirmationPage_DiscountValue1;
	
	private String cp_reference;
	private String cp_BookingDate;
	private String cp_Currency_1;
	private String cp_Currency_2;
	private String customer_title;
	private String customer_Fname;
	private String customer_Lname;
	
	private String ViewFullDetails_Currency1;
	private String ViewFullDetails_Currency2;
	private String ViewFullDetails_ActivityName;
	private String ViewFullDetails_SubTotal;	
	private String ViewFullDetails_Taxes;
	private String ViewFullDetails_TotalPayable;	
	private String ViewFullDetails_TotalGrossBookingValue;	
	private String ViewFullDetails_TotalTax;	
	private String ViewFullDetails_TotalPackageValue;	
	private String ViewFullDetails_AmountProcessed;
	private String ViewFullDetails_AmountCheckIn;
	private String ViewFullDetails_AgentCommisiion;
	
	private String confirmationPage_ActivityName;
	
	private String confirmationPage_Title;
	private String confirmationPage_FName;
	private String confirmationPage_LName;
	private String confirmationPage_TP;
	private String confirmationPage_Email;
	private String confirmationPage_address;
	private String confirmationPage_address_2;
	private String confirmationPage_country;
	private String confirmationPage_city;
	private String confirmationPage_state;
	private String confirmationPage_PostalCode;
	
	private String confirmationPage_QTY;
	private String confirmationPage_BI_Amount;
	private String confirmationPage_BI_Currency;
	
	private int paymenPagecusCount;
	private ArrayList<String> resultsPage_cusTitle;
	private ArrayList<String> resultsPage_cusFName;
	private ArrayList<String> resultsPage_cusLName;
	private ArrayList<String> confirmationPage_cusTitle;
	private ArrayList<String> confirmationPage_cusFName;
	private ArrayList<String> confirmationPage_cusLName;
	private String mainString;
	
	private String popUP_subTotal;
	private String popUP_TaxNother;
	private String popUP_TotalAcAmount;
	private String popUP_AmountProNow;
	private String popUP_AmountToBePaid;
	
	private String popUP_Total_subTotal;
	private String popUP_Total_TaxNother;
	private String popUP_Total_totalAcAmount;
	private String popUP_Total_AmountProNow;
	private String popUP_Total_AmountToBePaid;
	private String bookOrCart;
	private String paymentPage_Date;
	
	private String originalRate;
	private String activiteMR_Rate;
	private String activiteMR_SubTotal;
	private String activiteMR_AgentCommission;
	private String activiteMR_TotalTax;
	private String activiteMR_SubTotal2;
	private String activiteMR_TotalbookValue;
	private String activiteMR_SellRate;
	
	private String activiteMR_TotalGrossPkgValue;
	private String activiteMR_TotalTaxOtherCharges;
	private String activiteMR_TotalBookingValue;
	private String activiteMR_AmountNow;
	private String activiteMR_DueAtCheckIn;
	
	private String PP_rate;
	private String PP_rateCurrency;
	private String PP_programName_2;
	
	private String Quote_QuotationNo;
	private String Quote_Status;
	private String Quote_Currency;
	private String Quote_QTY;
	private String Quote_ActivityName;
	private String Quote_FName;
	private String Quote_LName;
	private String Quote_TP;
	private String Quote_Email;
	private String Quote_address_1;
	private String Quote_address_2;
	private String Quote_city;
	private String Quote_country;
	private String Quote_state = "";
	private String Quote_postalCode = "";
	
	private String Quote_subTotal;
	private String Quote_taxandOther;
	private String Quote_totalValue;
	private String Quote_TotalGrossPackageValue;
	private String Quote_TotalTax;
	private String Quote_TotalPackageValue;
	
	private String Quote_ViewFullDetails_Currency1;
	private String Quote_ViewFullDetails_Currency2;
	private String Quote_ViewFullDetails_ActivityName;
	private String Quote_ViewFullDetails_SubTotal;	
	private String Quote_ViewFullDetails_Taxes;
	private String Quote_ViewFullDetails_TotalPayable;	
	private String Quote_ViewFullDetails_TotalGrossBookingValue;	
	private String Quote_ViewFullDetails_TotalTax;	
	private String Quote_ViewFullDetails_TotalPackageValue;	
	private String Quote_ViewFullDetails_AmountProcessed;
	private String Quote_ViewFullDetails_AmountCheckIn;
	private String Quote_ViewFullDetails_AgentCommisiion;
	
	private String minPrize;
	private String pageLoadedResults;
	private String maxPrize;
	private String prizeChanged_1;
	private String prizeResults_1;
	private String prizeChanged_2;
	private String prizeResults_2;
	
	private String toActivity_SubTotal = "";
	private String toActivity_TotalTax = "";
	private String toActivity_TotalValue = "";	
	private String toAgentCommission = "";
	
	private String paymentType_bookingList = "";
	private String paymentReference_bookingList = "";
	private String authentReference_bookingList = "";
	private String paymentID = "";
	
	
	
	public String getPaymentID() {
		return paymentID;
	}
	public void setPaymentID(String paymentID) {
		this.paymentID = paymentID;
	}
	public String getAuthentReference_bookingList() {
		return authentReference_bookingList;
	}
	public void setAuthentReference_bookingList(String authentReference_bookingList) {
		this.authentReference_bookingList = authentReference_bookingList;
	}
	public String getPaymentReference_bookingList() {
		return paymentReference_bookingList;
	}
	public void setPaymentReference_bookingList(String paymentReference_bookingList) {
		this.paymentReference_bookingList = paymentReference_bookingList;
	}
	public String getPaymentType_bookingList() {
		return paymentType_bookingList;
	}
	public void setPaymentType_bookingList(String paymentType_bookingList) {
		this.paymentType_bookingList = paymentType_bookingList;
	}
	public String getScenarioCount() {
		return scenarioCount;
	}
	public void setScenarioCount(String scenarioCount) {
		this.scenarioCount = scenarioCount;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getPaymentsPage_DiscountValue1() {
		return paymentsPage_DiscountValue1;
	}
	public void setPaymentsPage_DiscountValue1(String paymentsPage_DiscountValue1) {
		this.paymentsPage_DiscountValue1 = paymentsPage_DiscountValue1;
	}
	public String getPaymentsPage_DiscountValue2() {
		return paymentsPage_DiscountValue2;
	}
	public void setPaymentsPage_DiscountValue2(String paymentsPage_DiscountValue2) {
		this.paymentsPage_DiscountValue2 = paymentsPage_DiscountValue2;
	}
	public String getConfirmationPage_DiscountValue1() {
		return confirmationPage_DiscountValue1;
	}
	public void setConfirmationPage_DiscountValue1(
			String confirmationPage_DiscountValue1) {
		this.confirmationPage_DiscountValue1 = confirmationPage_DiscountValue1;
	}
	public String getActiviteMR_SubTotal2() {
		return activiteMR_SubTotal2;
	}
	public void setActiviteMR_SubTotal2(String activiteMR_SubTotal2) {
		this.activiteMR_SubTotal2 = activiteMR_SubTotal2;
	}
	public String getOriginalRate() {
		return originalRate;
	}
	public void setOriginalRate(String originalRate) {
		this.originalRate = originalRate;
	}
	public String getActiviteMR_Rate() {
		return activiteMR_Rate;
	}
	public void setActiviteMR_Rate(String activiteMR_Rate) {
		this.activiteMR_Rate = activiteMR_Rate;
	}
	public String getActiviteMR_SubTotal() {
		return activiteMR_SubTotal;
	}
	public void setActiviteMR_SubTotal(String activiteMR_SubTotal) {
		this.activiteMR_SubTotal = activiteMR_SubTotal;
	}
	public String getActiviteMR_AgentCommission() {
		return activiteMR_AgentCommission;
	}
	public void setActiviteMR_AgentCommission(String activiteMR_AgentCommission) {
		this.activiteMR_AgentCommission = activiteMR_AgentCommission;
	}
	public String getActiviteMR_TotalTax() {
		return activiteMR_TotalTax;
	}
	public void setActiviteMR_TotalTax(String activiteMR_TotalTax) {
		this.activiteMR_TotalTax = activiteMR_TotalTax;
	}
	public String getActiviteMR_TotalbookValue() {
		return activiteMR_TotalbookValue;
	}
	public void setActiviteMR_TotalbookValue(String activiteMR_TotalbookValue) {
		this.activiteMR_TotalbookValue = activiteMR_TotalbookValue;
	}
	public String getActiviteMR_SellRate() {
		return activiteMR_SellRate;
	}
	public void setActiviteMR_SellRate(String activiteMR_SellRate) {
		this.activiteMR_SellRate = activiteMR_SellRate;
	}
	public String getActiviteMR_TotalGrossPkgValue() {
		return activiteMR_TotalGrossPkgValue;
	}
	public void setActiviteMR_TotalGrossPkgValue(
			String activiteMR_TotalGrossPkgValue) {
		this.activiteMR_TotalGrossPkgValue = activiteMR_TotalGrossPkgValue;
	}
	public String getActiviteMR_TotalTaxOtherCharges() {
		return activiteMR_TotalTaxOtherCharges;
	}
	public void setActiviteMR_TotalTaxOtherCharges(
			String activiteMR_TotalTaxOtherCharges) {
		this.activiteMR_TotalTaxOtherCharges = activiteMR_TotalTaxOtherCharges;
	}
	public String getActiviteMR_TotalBookingValue() {
		return activiteMR_TotalBookingValue;
	}
	public void setActiviteMR_TotalBookingValue(String activiteMR_TotalBookingValue) {
		this.activiteMR_TotalBookingValue = activiteMR_TotalBookingValue;
	}
	public String getActiviteMR_AmountNow() {
		return activiteMR_AmountNow;
	}
	public void setActiviteMR_AmountNow(String activiteMR_AmountNow) {
		this.activiteMR_AmountNow = activiteMR_AmountNow;
	}
	public String getActiviteMR_DueAtCheckIn() {
		return activiteMR_DueAtCheckIn;
	}
	public void setActiviteMR_DueAtCheckIn(String activiteMR_DueAtCheckIn) {
		this.activiteMR_DueAtCheckIn = activiteMR_DueAtCheckIn;
	}
	public String getQuote_ViewFullDetails_AgentCommisiion() {
		return Quote_ViewFullDetails_AgentCommisiion;
	}
	public void setQuote_ViewFullDetails_AgentCommisiion(
			String quote_ViewFullDetails_AgentCommisiion) {
		Quote_ViewFullDetails_AgentCommisiion = quote_ViewFullDetails_AgentCommisiion;
	}
	public String getConfirmationPage_AgentCommission() {
		return confirmationPage_AgentCommission;
	}
	public void setConfirmationPage_AgentCommission(
			String confirmationPage_AgentCommission) {
		this.confirmationPage_AgentCommission = confirmationPage_AgentCommission;
	}
	public String getPaymentsPage_AgentCommission() {
		return paymentsPage_AgentCommission;
	}
	public void setPaymentsPage_AgentCommission(String paymentsPage_AgentCommission) {
		this.paymentsPage_AgentCommission = paymentsPage_AgentCommission;
	}
	public String getViewFullDetails_AgentCommisiion() {
		return ViewFullDetails_AgentCommisiion;
	}
	public void setViewFullDetails_AgentCommisiion(
			String viewFullDetails_AgentCommisiion) {
		ViewFullDetails_AgentCommisiion = viewFullDetails_AgentCommisiion;
	}
	public String getToAgentCommission() {
		return toAgentCommission;
	}
	public void setToAgentCommission(String toAgentCommission) {
		this.toAgentCommission = toAgentCommission;
	}
	public String getToActivity_SubTotal() {
		return toActivity_SubTotal;
	}
	public void setToActivity_SubTotal(String toActivity_SubTotal) {
		this.toActivity_SubTotal = toActivity_SubTotal;
	}
	public String getToActivity_TotalTax() {
		return toActivity_TotalTax;
	}
	public void setToActivity_TotalTax(String toActivity_TotalTax) {
		this.toActivity_TotalTax = toActivity_TotalTax;
	}
	public String getToActivity_TotalValue() {
		return toActivity_TotalValue;
	}
	public void setToActivity_TotalValue(String toActivity_TotalValue) {
		this.toActivity_TotalValue = toActivity_TotalValue;
	}
	public boolean isAdultsLoaded() {
		return isAdultsLoaded;
	}
	public void setAdultsLoaded(boolean isAdultsLoaded) {
		this.isAdultsLoaded = isAdultsLoaded;
	}
	public boolean isChildsLoaded() {
		return isChildsLoaded;
	}
	public void setChildsLoaded(boolean isChildsLoaded) {
		this.isChildsLoaded = isChildsLoaded;
	}
	public boolean isSearchAgainCityLoaded() {
		return isSearchAgainCityLoaded;
	}
	public void setSearchAgainCityLoaded(boolean isSearchAgainCityLoaded) {
		this.isSearchAgainCityLoaded = isSearchAgainCityLoaded;
	}
	public boolean isSearchAgainDateFromLoaded() {
		return isSearchAgainDateFromLoaded;
	}
	public void setSearchAgainDateFromLoaded(boolean isSearchAgainDateFromLoaded) {
		this.isSearchAgainDateFromLoaded = isSearchAgainDateFromLoaded;
	}
	public boolean isSearchAgainDateToLoaded() {
		return isSearchAgainDateToLoaded;
	}
	public void setSearchAgainDateToLoaded(boolean isSearchAgainDateToLoaded) {
		this.isSearchAgainDateToLoaded = isSearchAgainDateToLoaded;
	}
	public boolean isAdultLoaded() {
		return isAdultLoaded;
	}
	public void setAdultLoaded(boolean isAdultLoaded) {
		this.isAdultLoaded = isAdultLoaded;
	}
	public boolean isChildLoaded() {
		return isChildLoaded;
	}
	public void setChildLoaded(boolean isChildLoaded) {
		this.isChildLoaded = isChildLoaded;
	}
	public boolean isSearchAgainProgramCat() {
		return isSearchAgainProgramCat;
	}
	public void setSearchAgainProgramCat(boolean isSearchAgainProgramCat) {
		this.isSearchAgainProgramCat = isSearchAgainProgramCat;
	}
	public boolean isSearchAgainPreCurrency() {
		return isSearchAgainPreCurrency;
	}
	public void setSearchAgainPreCurrency(boolean isSearchAgainPreCurrency) {
		this.isSearchAgainPreCurrency = isSearchAgainPreCurrency;
	}
	public boolean isSearchAgainPromoCode() {
		return isSearchAgainPromoCode;
	}
	public void setSearchAgainPromoCode(boolean isSearchAgainPromoCode) {
		this.isSearchAgainPromoCode = isSearchAgainPromoCode;
	}
	public boolean isCountryLoaded() {
		return isCountryLoaded;
	}
	public void setCountryLoaded(boolean isCountryLoaded) {
		this.isCountryLoaded = isCountryLoaded;
	}
	public boolean isCountryListLoaded() {
		return isCountryListLoaded;
	}
	public void setCountryListLoaded(boolean isCountryListLoaded) {
		this.isCountryListLoaded = isCountryListLoaded;
	}
	public boolean isCityLoaded() {
		return isCityLoaded;
	}
	public void setCityLoaded(boolean isCityLoaded) {
		this.isCityLoaded = isCityLoaded;
	}
	public boolean isDateFromLoaded() {
		return isDateFromLoaded;
	}
	public void setDateFromLoaded(boolean isDateFromLoaded) {
		this.isDateFromLoaded = isDateFromLoaded;
	}
	public boolean isDateToLoaded() {
		return isDateToLoaded;
	}
	public void setDateToLoaded(boolean isDateToLoaded) {
		this.isDateToLoaded = isDateToLoaded;
	}
	public boolean isCalenderAvailble() {
		return isCalenderAvailble;
	}
	public void setCalenderAvailble(boolean isCalenderAvailble) {
		this.isCalenderAvailble = isCalenderAvailble;
	}
	public String getShowAdd() {
		return showAdd;
	}
	public void setShowAdd(String showAdd) {
		this.showAdd = showAdd;
	}
	public String getHideAdd() {
		return HideAdd;
	}
	public void setHideAdd(String hideAdd) {
		HideAdd = hideAdd;
	}
	public boolean isActivityType() {
		return isActivityType;
	}
	public void setActivityType(boolean isActivityType) {
		this.isActivityType = isActivityType;
	}
	public boolean isProgramCat() {
		return isProgramCat;
	}
	public void setProgramCat(boolean isProgramCat) {
		this.isProgramCat = isProgramCat;
	}
	public boolean isPreCurrency() {
		return isPreCurrency;
	}
	public void setPreCurrency(boolean isPreCurrency) {
		this.isPreCurrency = isPreCurrency;
	}
	public boolean isPromoCode() {
		return isPromoCode;
	}
	public void setPromoCode(boolean isPromoCode) {
		this.isPromoCode = isPromoCode;
	}
	public String getShowResultsAdd() {
		return showResultsAdd;
	}
	public void setShowResultsAdd(String showResultsAdd) {
		this.showResultsAdd = showResultsAdd;
	}
	public String getHideResultsAdd() {
		return HideResultsAdd;
	}
	public void setHideResultsAdd(String hideResultsAdd) {
		HideResultsAdd = hideResultsAdd;
	}
	public boolean isResultsProgramCat() {
		return isResultsProgramCat;
	}
	public void setResultsProgramCat(boolean isResultsProgramCat) {
		this.isResultsProgramCat = isResultsProgramCat;
	}
	public boolean isResultsPreCurrency() {
		return isResultsPreCurrency;
	}
	public void setResultsPreCurrency(boolean isResultsPreCurrency) {
		this.isResultsPreCurrency = isResultsPreCurrency;
	}
	public boolean isResultsPromoCode() {
		return isResultsPromoCode;
	}
	public void setResultsPromoCode(boolean isResultsPromoCode) {
		this.isResultsPromoCode = isResultsPromoCode;
	}
	public boolean isPriceRangeFilter() {
		return isPriceRangeFilter;
	}
	public void setPriceRangeFilter(boolean isPriceRangeFilter) {
		this.isPriceRangeFilter = isPriceRangeFilter;
	}
	public boolean isProgramAvailablity() {
		return isProgramAvailablity;
	}
	public void setProgramAvailablity(boolean isProgramAvailablity) {
		this.isProgramAvailablity = isProgramAvailablity;
	}
	public boolean isActivityNameFilter() {
		return isActivityNameFilter;
	}
	public void setActivityNameFilter(boolean isActivityNameFilter) {
		this.isActivityNameFilter = isActivityNameFilter;
	}
	public boolean isMoreInfo() {
		return isMoreInfo;
	}
	public void setMoreInfo(boolean isMoreInfo) {
		this.isMoreInfo = isMoreInfo;
	}
	public boolean isCancelPolicy() {
		return isCancelPolicy;
	}
	public void setCancelPolicy(boolean isCancelPolicy) {
		this.isCancelPolicy = isCancelPolicy;
	}
	public boolean isActivityRemoved() {
		return isActivityRemoved;
	}
	public void setActivityRemoved(boolean isActivityRemoved) {
		this.isActivityRemoved = isActivityRemoved;
	}
	public boolean isProgramsAvailabilityActivityTypes() {
		return isProgramsAvailabilityActivityTypes;
	}
	public void setProgramsAvailabilityActivityTypes(
			boolean isProgramsAvailabilityActivityTypes) {
		this.isProgramsAvailabilityActivityTypes = isProgramsAvailabilityActivityTypes;
	}
	public boolean isAddToCart() {
		return isAddToCart;
	}
	public void setAddToCart(boolean isAddToCart) {
		this.isAddToCart = isAddToCart;
	}
	public boolean isBookingEngineLoaded() {
		return isBookingEngineLoaded;
	}
	public void setBookingEngineLoaded(boolean isBookingEngineLoaded) {
		this.isBookingEngineLoaded = isBookingEngineLoaded;
	}
	public String getMinPrize() {
		return minPrize;
	}
	public void setMinPrize(String minPrize) {
		this.minPrize = minPrize;
	}
	public String getPageLoadedResults() {
		return pageLoadedResults;
	}
	public void setPageLoadedResults(String pageLoadedResults) {
		this.pageLoadedResults = pageLoadedResults;
	}
	public String getMaxPrize() {
		return maxPrize;
	}
	public void setMaxPrize(String maxPrize) {
		this.maxPrize = maxPrize;
	}
	public String getPrizeChanged_1() {
		return prizeChanged_1;
	}
	public void setPrizeChanged_1(String prizeChanged_1) {
		this.prizeChanged_1 = prizeChanged_1;
	}
	public String getPrizeResults_1() {
		return prizeResults_1;
	}
	public void setPrizeResults_1(String prizeResults_1) {
		this.prizeResults_1 = prizeResults_1;
	}
	public String getPrizeChanged_2() {
		return prizeChanged_2;
	}
	public void setPrizeChanged_2(String prizeChanged_2) {
		this.prizeChanged_2 = prizeChanged_2;
	}
	public String getPrizeResults_2() {
		return prizeResults_2;
	}
	public void setPrizeResults_2(String prizeResults_2) {
		this.prizeResults_2 = prizeResults_2;
	}
	public int getActivityTotalCount() {
		return activityTotalCount;
	}
	public void setActivityTotalCount(int activityTotalCount) {
		this.activityTotalCount = activityTotalCount;
	}
	public int getActivityAvailableCount() {
		return activityAvailableCount;
	}
	public void setActivityAvailableCount(int activityAvailableCount) {
		this.activityAvailableCount = activityAvailableCount;
	}
	public int getActivityOnReqCount() {
		return activityOnReqCount;
	}
	public void setActivityOnReqCount(int activityOnReqCount) {
		this.activityOnReqCount = activityOnReqCount;
	}
	public String getChanged_DateFrom() {
		return changed_DateFrom;
	}
	public void setChanged_DateFrom(String changed_DateFrom) {
		this.changed_DateFrom = changed_DateFrom;
	}
	public String getChanged_DateTo() {
		return changed_DateTo;
	}
	public void setChanged_DateTo(String changed_DateTo) {
		this.changed_DateTo = changed_DateTo;
	}
	public ArrayList<Integer> getBeforeSorting() {
		return beforeSorting;
	}
	public void setBeforeSorting(ArrayList<Integer> beforeSorting) {
		this.beforeSorting = beforeSorting;
	}
	public ArrayList<Integer> getAfterSorting() {
		return afterSorting;
	}
	public void setAfterSorting(ArrayList<Integer> afterSorting) {
		this.afterSorting = afterSorting;
	}
	public String getQuote_ViewFullDetails_Currency1() {
		return Quote_ViewFullDetails_Currency1;
	}
	public void setQuote_ViewFullDetails_Currency1(
			String quote_ViewFullDetails_Currency1) {
		Quote_ViewFullDetails_Currency1 = quote_ViewFullDetails_Currency1;
	}
	public String getQuote_ViewFullDetails_Currency2() {
		return Quote_ViewFullDetails_Currency2;
	}
	public void setQuote_ViewFullDetails_Currency2(
			String quote_ViewFullDetails_Currency2) {
		Quote_ViewFullDetails_Currency2 = quote_ViewFullDetails_Currency2;
	}
	public String getQuote_ViewFullDetails_ActivityName() {
		return Quote_ViewFullDetails_ActivityName;
	}
	public void setQuote_ViewFullDetails_ActivityName(
			String quote_ViewFullDetails_ActivityName) {
		Quote_ViewFullDetails_ActivityName = quote_ViewFullDetails_ActivityName;
	}
	public String getQuote_ViewFullDetails_SubTotal() {
		return Quote_ViewFullDetails_SubTotal;
	}
	public void setQuote_ViewFullDetails_SubTotal(
			String quote_ViewFullDetails_SubTotal) {
		Quote_ViewFullDetails_SubTotal = quote_ViewFullDetails_SubTotal;
	}
	public String getQuote_ViewFullDetails_Taxes() {
		return Quote_ViewFullDetails_Taxes;
	}
	public void setQuote_ViewFullDetails_Taxes(String quote_ViewFullDetails_Taxes) {
		Quote_ViewFullDetails_Taxes = quote_ViewFullDetails_Taxes;
	}
	public String getQuote_ViewFullDetails_TotalPayable() {
		return Quote_ViewFullDetails_TotalPayable;
	}
	public void setQuote_ViewFullDetails_TotalPayable(
			String quote_ViewFullDetails_TotalPayable) {
		Quote_ViewFullDetails_TotalPayable = quote_ViewFullDetails_TotalPayable;
	}
	public String getQuote_ViewFullDetails_TotalGrossBookingValue() {
		return Quote_ViewFullDetails_TotalGrossBookingValue;
	}
	public void setQuote_ViewFullDetails_TotalGrossBookingValue(
			String quote_ViewFullDetails_TotalGrossBookingValue) {
		Quote_ViewFullDetails_TotalGrossBookingValue = quote_ViewFullDetails_TotalGrossBookingValue;
	}
	public String getQuote_ViewFullDetails_TotalTax() {
		return Quote_ViewFullDetails_TotalTax;
	}
	public void setQuote_ViewFullDetails_TotalTax(
			String quote_ViewFullDetails_TotalTax) {
		Quote_ViewFullDetails_TotalTax = quote_ViewFullDetails_TotalTax;
	}
	public String getQuote_ViewFullDetails_TotalPackageValue() {
		return Quote_ViewFullDetails_TotalPackageValue;
	}
	public void setQuote_ViewFullDetails_TotalPackageValue(
			String quote_ViewFullDetails_TotalPackageValue) {
		Quote_ViewFullDetails_TotalPackageValue = quote_ViewFullDetails_TotalPackageValue;
	}
	public String getQuote_ViewFullDetails_AmountProcessed() {
		return Quote_ViewFullDetails_AmountProcessed;
	}
	public void setQuote_ViewFullDetails_AmountProcessed(
			String quote_ViewFullDetails_AmountProcessed) {
		Quote_ViewFullDetails_AmountProcessed = quote_ViewFullDetails_AmountProcessed;
	}
	public String getQuote_ViewFullDetails_AmountCheckIn() {
		return Quote_ViewFullDetails_AmountCheckIn;
	}
	public void setQuote_ViewFullDetails_AmountCheckIn(
			String quote_ViewFullDetails_AmountCheckIn) {
		Quote_ViewFullDetails_AmountCheckIn = quote_ViewFullDetails_AmountCheckIn;
	}
	public String getQuote_state() {
		return Quote_state;
	}
	public void setQuote_state(String quote_state) {
		Quote_state = quote_state;
	}
	public String getQuote_postalCode() {
		return Quote_postalCode;
	}
	public void setQuote_postalCode(String quote_postalCode) {
		Quote_postalCode = quote_postalCode;
	}
	public String getQuote_QuotationNo() {
		return Quote_QuotationNo;
	}
	public void setQuote_QuotationNo(String quote_QuotationNo) {
		Quote_QuotationNo = quote_QuotationNo;
	}
	public String getQuote_Status() {
		return Quote_Status;
	}
	public void setQuote_Status(String quote_Status) {
		Quote_Status = quote_Status;
	}
	public String getQuote_Currency() {
		return Quote_Currency;
	}
	public void setQuote_Currency(String quote_Currency) {
		Quote_Currency = quote_Currency;
	}
	public String getQuote_QTY() {
		return Quote_QTY;
	}
	public void setQuote_QTY(String quote_QTY) {
		Quote_QTY = quote_QTY;
	}
	public String getQuote_ActivityName() {
		return Quote_ActivityName;
	}
	public void setQuote_ActivityName(String quote_ActivityName) {
		Quote_ActivityName = quote_ActivityName;
	}
	public String getQuote_FName() {
		return Quote_FName;
	}
	public void setQuote_FName(String quote_FName) {
		Quote_FName = quote_FName;
	}
	public String getQuote_LName() {
		return Quote_LName;
	}
	public void setQuote_LName(String quote_LName) {
		Quote_LName = quote_LName;
	}
	public String getQuote_TP() {
		return Quote_TP;
	}
	public void setQuote_TP(String quote_TP) {
		Quote_TP = quote_TP;
	}
	public String getQuote_Email() {
		return Quote_Email;
	}
	public void setQuote_Email(String quote_Email) {
		Quote_Email = quote_Email;
	}
	public String getQuote_address_1() {
		return Quote_address_1;
	}
	public void setQuote_address_1(String quote_address_1) {
		Quote_address_1 = quote_address_1;
	}
	public String getQuote_address_2() {
		return Quote_address_2;
	}
	public void setQuote_address_2(String quote_address_2) {
		Quote_address_2 = quote_address_2;
	}
	public String getQuote_city() {
		return Quote_city;
	}
	public void setQuote_city(String quote_city) {
		Quote_city = quote_city;
	}
	public String getQuote_country() {
		return Quote_country;
	}
	public void setQuote_country(String quote_country) {
		Quote_country = quote_country;
	}
	public String getQuote_subTotal() {
		return Quote_subTotal;
	}
	public void setQuote_subTotal(String quote_subTotal) {
		Quote_subTotal = quote_subTotal;
	}
	public String getQuote_taxandOther() {
		return Quote_taxandOther;
	}
	public void setQuote_taxandOther(String quote_taxandOther) {
		Quote_taxandOther = quote_taxandOther;
	}
	public String getQuote_totalValue() {
		return Quote_totalValue;
	}
	public void setQuote_totalValue(String quote_totalValue) {
		Quote_totalValue = quote_totalValue;
	}
	public String getQuote_TotalGrossPackageValue() {
		return Quote_TotalGrossPackageValue;
	}
	public void setQuote_TotalGrossPackageValue(String quote_TotalGrossPackageValue) {
		Quote_TotalGrossPackageValue = quote_TotalGrossPackageValue;
	}
	public String getQuote_TotalTax() {
		return Quote_TotalTax;
	}
	public void setQuote_TotalTax(String quote_TotalTax) {
		Quote_TotalTax = quote_TotalTax;
	}
	public String getQuote_TotalPackageValue() {
		return Quote_TotalPackageValue;
	}
	public void setQuote_TotalPackageValue(String quote_TotalPackageValue) {
		Quote_TotalPackageValue = quote_TotalPackageValue;
	}
	public String getPaymentPage_PostelCode() {
		return paymentPage_PostelCode;
	}
	public void setPaymentPage_PostelCode(String paymentPage_PostelCode) {
		this.paymentPage_PostelCode = paymentPage_PostelCode;
	}
	public String getConfirmationPage_PostalCode() {
		return confirmationPage_PostalCode;
	}
	public void setConfirmationPage_PostalCode(String confirmationPage_PostalCode) {
		this.confirmationPage_PostalCode = confirmationPage_PostalCode;
	}
	public boolean isActivityResultsAvailble() {
		return isActivityResultsAvailble;
	}
	public void setActivityResultsAvailble(boolean isActivityResultsAvailble) {
		this.isActivityResultsAvailble = isActivityResultsAvailble;
	}
	public String getPaymentPage_Date() {
		return paymentPage_Date;
	}
	public void setPaymentPage_Date(String paymentPage_Date) {
		this.paymentPage_Date = paymentPage_Date;
	}
	public String getPP_rate() {
		return PP_rate;
	}
	public void setPP_rate(String pP_rate) {
		PP_rate = pP_rate;
	}
	public String getPP_rateCurrency() {
		return PP_rateCurrency;
	}
	public void setPP_rateCurrency(String pP_rateCurrency) {
		PP_rateCurrency = pP_rateCurrency;
	}
	public String getPP_programName_2() {
		return PP_programName_2;
	}
	public void setPP_programName_2(String pP_programName_2) {
		PP_programName_2 = pP_programName_2;
	}
	public String getBookOrCart() {
		return bookOrCart;
	}
	public void setBookOrCart(String bookOrCart) {
		this.bookOrCart = bookOrCart;
	}
	public String getPopUP_subTotal() {
		return popUP_subTotal;
	}
	public void setPopUP_subTotal(String popUP_subTotal) {
		this.popUP_subTotal = popUP_subTotal;
	}
	public String getPopUP_TaxNother() {
		return popUP_TaxNother;
	}
	public void setPopUP_TaxNother(String popUP_TaxNother) {
		this.popUP_TaxNother = popUP_TaxNother;
	}
	public String getPopUP_TotalAcAmount() {
		return popUP_TotalAcAmount;
	}
	public void setPopUP_TotalAcAmount(String popUP_TotalAcAmount) {
		this.popUP_TotalAcAmount = popUP_TotalAcAmount;
	}
	public String getPopUP_AmountProNow() {
		return popUP_AmountProNow;
	}
	public void setPopUP_AmountProNow(String popUP_AmountProNow) {
		this.popUP_AmountProNow = popUP_AmountProNow;
	}
	public String getPopUP_AmountToBePaid() {
		return popUP_AmountToBePaid;
	}
	public void setPopUP_AmountToBePaid(String popUP_AmountToBePaid) {
		this.popUP_AmountToBePaid = popUP_AmountToBePaid;
	}
	public String getPopUP_Total_subTotal() {
		return popUP_Total_subTotal;
	}
	public void setPopUP_Total_subTotal(String popUP_Total_subTotal) {
		this.popUP_Total_subTotal = popUP_Total_subTotal;
	}
	public String getPopUP_Total_TaxNother() {
		return popUP_Total_TaxNother;
	}
	public void setPopUP_Total_TaxNother(String popUP_Total_TaxNother) {
		this.popUP_Total_TaxNother = popUP_Total_TaxNother;
	}
	public String getPopUP_Total_totalAcAmount() {
		return popUP_Total_totalAcAmount;
	}
	public void setPopUP_Total_totalAcAmount(String popUP_Total_totalAcAmount) {
		this.popUP_Total_totalAcAmount = popUP_Total_totalAcAmount;
	}
	public String getPopUP_Total_AmountProNow() {
		return popUP_Total_AmountProNow;
	}
	public void setPopUP_Total_AmountProNow(String popUP_Total_AmountProNow) {
		this.popUP_Total_AmountProNow = popUP_Total_AmountProNow;
	}
	public String getPopUP_Total_AmountToBePaid() {
		return popUP_Total_AmountToBePaid;
	}
	public void setPopUP_Total_AmountToBePaid(String popUP_Total_AmountToBePaid) {
		this.popUP_Total_AmountToBePaid = popUP_Total_AmountToBePaid;
	}
	public String getConfirmationPage_BI_Amount() {
		return confirmationPage_BI_Amount;
	}
	public void setConfirmationPage_BI_Amount(String confirmationPage_BI_Amount) {
		this.confirmationPage_BI_Amount = confirmationPage_BI_Amount;
	}
	public String getConfirmationPage_BI_Currency() {
		return confirmationPage_BI_Currency;
	}
	public void setConfirmationPage_BI_Currency(String confirmationPage_BI_Currency) {
		this.confirmationPage_BI_Currency = confirmationPage_BI_Currency;
	}
	public String getCp_reference() {
		return cp_reference;
	}
	public void setCp_reference(String cp_reference) {
		this.cp_reference = cp_reference;
	}
	public String getCp_BookingDate() {
		return cp_BookingDate;
	}
	public void setCp_BookingDate(String cp_BookingDate) {
		this.cp_BookingDate = cp_BookingDate;
	}
	public String getCp_Currency_1() {
		return cp_Currency_1;
	}
	public void setCp_Currency_1(String cp_Currency_1) {
		this.cp_Currency_1 = cp_Currency_1;
	}
	public String getCp_Currency_2() {
		return cp_Currency_2;
	}
	public void setCp_Currency_2(String cp_Currency_2) {
		this.cp_Currency_2 = cp_Currency_2;
	}
	public String getCustomer_title() {
		return customer_title;
	}
	public void setCustomer_title(String customer_title) {
		this.customer_title = customer_title;
	}
	public String getCustomer_Fname() {
		return customer_Fname;
	}
	public void setCustomer_Fname(String customer_Fname) {
		this.customer_Fname = customer_Fname;
	}
	public String getCustomer_Lname() {
		return customer_Lname;
	}
	public void setCustomer_Lname(String customer_Lname) {
		this.customer_Lname = customer_Lname;
	}
	public String getMainRate() {
		return mainRate;
	}
	public void setMainRate(String mainRate) {
		this.mainRate = mainRate;
	}
	public String getCartActivityName() {
		return cartActivityName;
	}
	public void setCartActivityName(String cartActivityName) {
		this.cartActivityName = cartActivityName;
	}
	public String getCartDate() {
		return cartDate;
	}
	public void setCartDate(String cartDate) {
		this.cartDate = cartDate;
	}
	public String getCartCurrency() {
		return cartCurrency;
	}
	public void setCartCurrency(String cartCurrency) {
		this.cartCurrency = cartCurrency;
	}
	public String getCartFullAmount() {
		return cartFullAmount;
	}
	public void setCartFullAmount(String cartFullAmount) {
		this.cartFullAmount = cartFullAmount;
	}
	public String getMainString() {
		return mainString;
	}
	public void setMainString(String mainString) {
		this.mainString = mainString;
	}
	public String getCurrentDate_2() {
		return CurrentDate_2;
	}
	public void setCurrentDate_2(String currentDate_2) {
		CurrentDate_2 = currentDate_2;
	}
	public String getProgramNotes() {
		return programNotes;
	}
	public void setProgramNotes(String programNotes) {
		this.programNotes = programNotes;
	}
	public String getConfirmationPage_Title() {
		return confirmationPage_Title;
	}
	public void setConfirmationPage_Title(String confirmationPage_Title) {
		this.confirmationPage_Title = confirmationPage_Title;
	}
	public String getConfirmationPage_FName() {
		return confirmationPage_FName;
	}
	public void setConfirmationPage_FName(String confirmationPage_FName) {
		this.confirmationPage_FName = confirmationPage_FName;
	}
	public String getConfirmationPage_LName() {
		return confirmationPage_LName;
	}
	public void setConfirmationPage_LName(String confirmationPage_LName) {
		this.confirmationPage_LName = confirmationPage_LName;
	}
	public String getConfirmationPage_TP() {
		return confirmationPage_TP;
	}
	public void setConfirmationPage_TP(String confirmationPage_TP) {
		this.confirmationPage_TP = confirmationPage_TP;
	}
	public String getConfirmationPage_Email() {
		return confirmationPage_Email;
	}
	public void setConfirmationPage_Email(String confirmationPage_Email) {
		this.confirmationPage_Email = confirmationPage_Email;
	}
	public String getConfirmationPage_address() {
		return confirmationPage_address;
	}
	public void setConfirmationPage_address(String confirmationPage_address) {
		this.confirmationPage_address = confirmationPage_address;
	}
	public String getConfirmationPage_address_2() {
		return confirmationPage_address_2;
	}
	public void setConfirmationPage_address_2(String confirmationPage_address_2) {
		this.confirmationPage_address_2 = confirmationPage_address_2;
	}
	public String getConfirmationPage_state() {
		return confirmationPage_state;
	}
	public void setConfirmationPage_state(String confirmationPage_state) {
		this.confirmationPage_state = confirmationPage_state;
	}
	public String getPaymentPage_state() {
		return paymentPage_state;
	}
	public void setPaymentPage_state(String paymentPage_state) {
		this.paymentPage_state = paymentPage_state;
	}
	public long getTimeFirstSearch() {
		return timeFirstSearch;
	}
	public void setTimeFirstSearch(long timeFirstSearch) {
		this.timeFirstSearch = timeFirstSearch;
	}
	public long getTimeFirstResults() {
		return timeFirstResults;
	}
	public void setTimeFirstResults(long timeFirstResults) {
		this.timeFirstResults = timeFirstResults;
	}
	public String getCurrentDate() {
		return CurrentDate;
	}
	public void setCurrentDate(String currentDate) {
		CurrentDate = currentDate;
	}
	public long getTimeAddedtoCart() {
		return timeAddedtoCart;
	}
	public void setTimeAddedtoCart(long timeAddedtoCart) {
		this.timeAddedtoCart = timeAddedtoCart;
	}
	public long getTimeAailableClick() {
		return timeAailableClick;
	}
	public void setTimeAailableClick(long timeAailableClick) {
		this.timeAailableClick = timeAailableClick;
	}
	public long getTimeSearchButtClickTime() {
		return timeSearchButtClickTime;
	}
	public void setTimeSearchButtClickTime(long timeSearchButtClickTime) {
		this.timeSearchButtClickTime = timeSearchButtClickTime;
	}
	public long getTimeResultsAvailable() {
		return timeResultsAvailable;
	}
	public void setTimeResultsAvailable(long timeResultsAvailable) {
		this.timeResultsAvailable = timeResultsAvailable;
	}
	public ArrayList<String> getConfirmCancelPolicy() {
		return confirmCancelPolicy;
	}
	public void setConfirmCancelPolicy(ArrayList<String> confirmCancelPolicy) {
		this.confirmCancelPolicy = confirmCancelPolicy;
	}
	public ArrayList<String> getPaymentCancelPolicy() {
		return paymentCancelPolicy;
	}
	public void setPaymentCancelPolicy(ArrayList<String> paymentCancelPolicy) {
		this.paymentCancelPolicy = paymentCancelPolicy;
	}
	public String getPayOnline_MainTotalBookingCurrency() {
		return payOnline_MainTotalBookingCurrency;
	}
	public void setPayOnline_MainTotalBookingCurrency(
			String payOnline_MainTotalBookingCurrency) {
		this.payOnline_MainTotalBookingCurrency = payOnline_MainTotalBookingCurrency;
	}
	public int getPaymenPagecusCount() {
		return paymenPagecusCount;
	}
	public void setPaymenPagecusCount(int paymenPagecusCount) {
		this.paymenPagecusCount = paymenPagecusCount;
	}
	public String getCustomerNotes() {
		return customerNotes;
	}
	public void setCustomerNotes(String customerNotes) {
		this.customerNotes = customerNotes;
	}
	public String getInternalNotes() {
		return internalNotes;
	}
	public void setInternalNotes(String internalNotes) {
		this.internalNotes = internalNotes;
	}
	public String getPaymentPage_Title() {
		return paymentPage_Title;
	}
	public void setPaymentPage_Title(String paymentPage_Title) {
		this.paymentPage_Title = paymentPage_Title;
	}
	public String getPaymentPage_address_2() {
		return paymentPage_address_2;
	}
	public void setPaymentPage_address_2(String paymentPage_address_2) {
		this.paymentPage_address_2 = paymentPage_address_2;
	}
	public String getPaymentPage_ActivityName() {
		return paymentPage_ActivityName;
	}
	public void setPaymentPage_ActivityName(String paymentPage_ActivityName) {
		this.paymentPage_ActivityName = paymentPage_ActivityName;
	}
	public String getViewFullDetails_Currency1() {
		return ViewFullDetails_Currency1;
	}
	public void setViewFullDetails_Currency1(String viewFullDetails_Currency1) {
		ViewFullDetails_Currency1 = viewFullDetails_Currency1;
	}
	public String getViewFullDetails_Currency2() {
		return ViewFullDetails_Currency2;
	}
	public void setViewFullDetails_Currency2(String viewFullDetails_Currency2) {
		ViewFullDetails_Currency2 = viewFullDetails_Currency2;
	}
	public String getViewFullDetails_ActivityName() {
		return ViewFullDetails_ActivityName;
	}
	public void setViewFullDetails_ActivityName(String viewFullDetails_ActivityName) {
		ViewFullDetails_ActivityName = viewFullDetails_ActivityName;
	}
	public String getViewFullDetails_SubTotal() {
		return ViewFullDetails_SubTotal;
	}
	public void setViewFullDetails_SubTotal(String viewFullDetails_SubTotal) {
		ViewFullDetails_SubTotal = viewFullDetails_SubTotal;
	}
	public String getViewFullDetails_Taxes() {
		return ViewFullDetails_Taxes;
	}
	public void setViewFullDetails_Taxes(String viewFullDetails_Taxes) {
		ViewFullDetails_Taxes = viewFullDetails_Taxes;
	}
	public String getViewFullDetails_TotalPayable() {
		return ViewFullDetails_TotalPayable;
	}
	public void setViewFullDetails_TotalPayable(String viewFullDetails_TotalPayable) {
		ViewFullDetails_TotalPayable = viewFullDetails_TotalPayable;
	}
	public String getViewFullDetails_TotalGrossBookingValue() {
		return ViewFullDetails_TotalGrossBookingValue;
	}
	public void setViewFullDetails_TotalGrossBookingValue(
			String viewFullDetails_TotalGrossBookingValue) {
		ViewFullDetails_TotalGrossBookingValue = viewFullDetails_TotalGrossBookingValue;
	}
	public String getViewFullDetails_TotalTax() {
		return ViewFullDetails_TotalTax;
	}
	public void setViewFullDetails_TotalTax(String viewFullDetails_TotalTax) {
		ViewFullDetails_TotalTax = viewFullDetails_TotalTax;
	}
	public String getViewFullDetails_TotalPackageValue() {
		return ViewFullDetails_TotalPackageValue;
	}
	public void setViewFullDetails_TotalPackageValue(
			String viewFullDetails_TotalPackageValue) {
		ViewFullDetails_TotalPackageValue = viewFullDetails_TotalPackageValue;
	}
	public String getViewFullDetails_AmountProcessed() {
		return ViewFullDetails_AmountProcessed;
	}
	public void setViewFullDetails_AmountProcessed(
			String viewFullDetails_AmountProcessed) {
		ViewFullDetails_AmountProcessed = viewFullDetails_AmountProcessed;
	}
	public String getViewFullDetails_AmountCheckIn() {
		return ViewFullDetails_AmountCheckIn;
	}
	public void setViewFullDetails_AmountCheckIn(
			String viewFullDetails_AmountCheckIn) {
		ViewFullDetails_AmountCheckIn = viewFullDetails_AmountCheckIn;
	}
	public String getConfirmationPage_Currency() {
		return confirmationPage_Currency;
	}
	public void setConfirmationPage_Currency(String confirmationPage_Currency) {
		this.confirmationPage_Currency = confirmationPage_Currency;
	}
	public String getPaymentPage_Currency_1() {
		return paymentPage_Currency_1;
	}
	public void setPaymentPage_Currency_1(String paymentPage_Currency_1) {
		this.paymentPage_Currency_1 = paymentPage_Currency_1;
	}
	public String getPaymentPage_Currency_2() {
		return paymentPage_Currency_2;
	}
	public void setPaymentPage_Currency_2(String paymentPage_Currency_2) {
		this.paymentPage_Currency_2 = paymentPage_Currency_2;
	}
	public String getResultsPage_SubTotal_Currency() {
		return resultsPage_SubTotal_Currency;
	}
	public void setResultsPage_SubTotal_Currency(
			String resultsPage_SubTotal_Currency) {
		this.resultsPage_SubTotal_Currency = resultsPage_SubTotal_Currency;
	}
	public String getResultsPage_Taxes_Currency() {
		return resultsPage_Taxes_Currency;
	}
	public void setResultsPage_Taxes_Currency(String resultsPage_Taxes_Currency) {
		this.resultsPage_Taxes_Currency = resultsPage_Taxes_Currency;
	}
	public String getResultsPage_TotalPayable_Currency() {
		return resultsPage_TotalPayable_Currency;
	}
	public void setResultsPage_TotalPayable_Currency(
			String resultsPage_TotalPayable_Currency) {
		this.resultsPage_TotalPayable_Currency = resultsPage_TotalPayable_Currency;
	}
	public String getResultsPage_QTY() {
		return resultsPage_QTY;
	}
	public void setResultsPage_QTY(String resultsPage_QTY) {
		this.resultsPage_QTY = resultsPage_QTY;
	}
	public String getResultsPage_BookingStatus() {
		return resultsPage_BookingStatus;
	}
	public void setResultsPage_BookingStatus(String resultsPage_BookingStatus) {
		this.resultsPage_BookingStatus = resultsPage_BookingStatus;
	}
	public String getConfirmationPage_QTY() {
		return confirmationPage_QTY;
	}
	public void setConfirmationPage_QTY(String confirmationPage_QTY) {
		this.confirmationPage_QTY = confirmationPage_QTY;
	}
	
	public String getConfirmationPage_activityBookingValue() {
		return confirmationPage_activityBookingValue;
	}
	public void setConfirmationPage_activityBookingValue(
			String confirmationPage_activityBookingValue) {
		this.confirmationPage_activityBookingValue = confirmationPage_activityBookingValue;
	}
	public String getConfirmationPage_TotalGrossValue() {
		return confirmationPage_TotalGrossValue;
	}
	public void setConfirmationPage_TotalGrossValue(
			String confirmationPage_TotalGrossValue) {
		this.confirmationPage_TotalGrossValue = confirmationPage_TotalGrossValue;
	}
	public String getConfirmationPage_TotalTax() {
		return confirmationPage_TotalTax;
	}
	public void setConfirmationPage_TotalTax(String confirmationPage_TotalTax) {
		this.confirmationPage_TotalTax = confirmationPage_TotalTax;
	}
	public String getConfirmationPage_TotalPackageBookingValue() {
		return confirmationPage_TotalPackageBookingValue;
	}
	public void setConfirmationPage_TotalPackageBookingValue(
			String confirmationPage_TotalPackageBookingValue) {
		this.confirmationPage_TotalPackageBookingValue = confirmationPage_TotalPackageBookingValue;
	}
	public String getConfirmationPage_AmountBeingProcessed() {
		return confirmationPage_AmountBeingProcessed;
	}
	public void setConfirmationPage_AmountBeingProcessed(
			String confirmationPage_AmountBeingProcessed) {
		this.confirmationPage_AmountBeingProcessed = confirmationPage_AmountBeingProcessed;
	}
	public String getConfirmationPage_AmountDueAtCheckin() {
		return confirmationPage_AmountDueAtCheckin;
	}
	public void setConfirmationPage_AmountDueAtCheckin(
			String confirmationPage_AmountDueAtCheckin) {
		this.confirmationPage_AmountDueAtCheckin = confirmationPage_AmountDueAtCheckin;
	}
	
	
	public String getPaymentPage_subTotal() {
		return paymentPage_subTotal;
	}
	public void setPaymentPage_subTotal(String paymentPage_subTotal) {
		this.paymentPage_subTotal = paymentPage_subTotal;
	}
	public String getPaymentPage_TotalTax() {
		return paymentPage_TotalTax;
	}
	public void setPaymentPage_TotalTax(String paymentPage_TotalTax) {
		this.paymentPage_TotalTax = paymentPage_TotalTax;
	}
	public String getPaymentPage_TotalActivityBookingValue() {
		return paymentPage_TotalActivityBookingValue;
	}
	public void setPaymentPage_TotalActivityBookingValue(
			String paymentPage_TotalActivityBookingValue) {
		this.paymentPage_TotalActivityBookingValue = paymentPage_TotalActivityBookingValue;
	}
	public String getPaymentPageAmountbeingProcessed() {
		return paymentPageAmountbeingProcessed;
	}
	public void setPaymentPageAmountbeingProcessed(
			String paymentPageAmountbeingProcessed) {
		this.paymentPageAmountbeingProcessed = paymentPageAmountbeingProcessed;
	}
	public String getPaymentPageAmountduatCheckIn() {
		return paymentPageAmountduatCheckIn;
	}
	public void setPaymentPageAmountduatCheckIn(String paymentPageAmountduatCheckIn) {
		this.paymentPageAmountduatCheckIn = paymentPageAmountduatCheckIn;
	}
	
	public String getResultsPage_SubTotal() {
		return resultsPage_SubTotal;
	}
	public void setResultsPage_SubTotal(String resultsPage_SubTotal) {
		this.resultsPage_SubTotal = resultsPage_SubTotal;
	}
	public String getResultsPage_Taxes() {
		return resultsPage_Taxes;
	}
	public void setResultsPage_Taxes(String resultsPage_Taxes) {
		this.resultsPage_Taxes = resultsPage_Taxes;
	}
	public String getResultsPage_TotalPayable() {
		return resultsPage_TotalPayable;
	}
	public void setResultsPage_TotalPayable(String resultsPage_TotalPayable) {
		this.resultsPage_TotalPayable = resultsPage_TotalPayable;
	}
	
	
	
	
	public ArrayList<String> getConfirmationPage_cusTitle() {
		return confirmationPage_cusTitle;
	}
	public void setConfirmationPage_cusTitle(
			ArrayList<String> confirmationPage_cusTitle) {
		this.confirmationPage_cusTitle = confirmationPage_cusTitle;
	}
	public ArrayList<String> getConfirmationPage_cusFName() {
		return confirmationPage_cusFName;
	}
	public void setConfirmationPage_cusFName(
			ArrayList<String> confirmationPage_cusFName) {
		this.confirmationPage_cusFName = confirmationPage_cusFName;
	}
	public ArrayList<String> getConfirmationPage_cusLName() {
		return confirmationPage_cusLName;
	}
	public void setConfirmationPage_cusLName(
			ArrayList<String> confirmationPage_cusLName) {
		this.confirmationPage_cusLName = confirmationPage_cusLName;
	}
	public ArrayList<String> getResultsPage_cusTitle() {
		return resultsPage_cusTitle;
	}
	public void setResultsPage_cusTitle(ArrayList<String> resultsPage_cusTitle) {
		this.resultsPage_cusTitle = resultsPage_cusTitle;
	}
	public ArrayList<String> getResultsPage_cusFName() {
		return resultsPage_cusFName;
	}
	public void setResultsPage_cusFName(ArrayList<String> resultsPage_cusFName) {
		this.resultsPage_cusFName = resultsPage_cusFName;
	}
	public ArrayList<String> getResultsPage_cusLName() {
		return resultsPage_cusLName;
	}
	public void setResultsPage_cusLName(ArrayList<String> resultsPage_cusLName) {
		this.resultsPage_cusLName = resultsPage_cusLName;
	}
	public String getPaymentPage_FName() {
		return paymentPage_FName;
	}
	public void setPaymentPage_FName(String paymentPage_FName) {
		this.paymentPage_FName = paymentPage_FName;
	}
	public String getPaymentPage_LName() {
		return paymentPage_LName;
	}
	public void setPaymentPage_LName(String paymentPage_LName) {
		this.paymentPage_LName = paymentPage_LName;
	}
	public String getPaymentPage_TP() {
		return paymentPage_TP;
	}
	public void setPaymentPage_TP(String paymentPage_TP) {
		this.paymentPage_TP = paymentPage_TP;
	}
	public String getPaymentPage_Email() {
		return paymentPage_Email;
	}
	public void setPaymentPage_Email(String paymentPage_Email) {
		this.paymentPage_Email = paymentPage_Email;
	}
	public String getPaymentPage_address() {
		return paymentPage_address;
	}
	public void setPaymentPage_address(String paymentPage_address) {
		this.paymentPage_address = paymentPage_address;
	}
	public String getPaymentPage_country() {
		return paymentPage_country;
	}
	public void setPaymentPage_country(String paymentPage_country) {
		this.paymentPage_country = paymentPage_country;
	}
	public String getPaymentPage_city() {
		return paymentPage_city;
	}
	public void setPaymentPage_city(String paymentPage_city) {
		this.paymentPage_city = paymentPage_city;
	}
	
	public String getConfirmationPage_grossValue() {
		return confirmationPage_grossValue;
	}
	public void setConfirmationPage_grossValue(String confirmationPage_grossValue) {
		this.confirmationPage_grossValue = confirmationPage_grossValue;
	}
	public String getConfirmationPage_tax() {
		return confirmationPage_tax;
	}
	public void setConfirmationPage_tax(String confirmationPage_tax) {
		this.confirmationPage_tax = confirmationPage_tax;
	}
	
	public String getConfirmationPage_ActivityName() {
		return confirmationPage_ActivityName;
	}
	public void setConfirmationPage_ActivityName(
			String confirmationPage_ActivityName) {
		this.confirmationPage_ActivityName = confirmationPage_ActivityName;
	}
	
	public String getConfirmationPage_country() {
		return confirmationPage_country;
	}
	public void setConfirmationPage_country(String confirmationPage_country) {
		this.confirmationPage_country = confirmationPage_country;
	}
	public String getConfirmationPage_city() {
		return confirmationPage_city;
	}
	public void setConfirmationPage_city(String confirmationPage_city) {
		this.confirmationPage_city = confirmationPage_city;
	}
	public int getDaysCount() {
		return daysCount;
	}
	public void setDaysCount(int daysCount) {
		this.daysCount = daysCount;
	}
	public String getFiltercurrency() {
		return filtercurrency;
	}
	public void setFiltercurrency(String filtercurrency) {
		this.filtercurrency = filtercurrency;
	}
	public String getResultsPage_destinaton() {
		return resultsPage_destinaton;
	}
	public void setResultsPage_destinaton(String resultsPage_destinaton) {
		this.resultsPage_destinaton = resultsPage_destinaton;
	}
	public String getResultsPage_dateFrom() {
		return resultsPage_dateFrom;
	}
	public void setResultsPage_dateFrom(String resultsPage_dateFrom) {
		this.resultsPage_dateFrom = resultsPage_dateFrom;
	}
	public String getResultsPage_dateTo() {
		return resultsPage_dateTo;
	}
	public void setResultsPage_dateTo(String resultsPage_dateTo) {
		this.resultsPage_dateTo = resultsPage_dateTo;
	}
	public String getResultsPage_adults() {
		return resultsPage_adults;
	}
	public void setResultsPage_adults(String resultsPage_adults) {
		this.resultsPage_adults = resultsPage_adults;
	}
	public String getResultsPage__childs() {
		return resultsPage__childs;
	}
	public void setResultsPage__childs(String resultsPage__childs) {
		this.resultsPage__childs = resultsPage__childs;
	}
	
	public ArrayList<String> getResultsPage_ageOfChilds() {
		return resultsPage_ageOfChilds;
	}
	public void setResultsPage_ageOfChilds(ArrayList<String> resultsPage_ageOfChilds) {
		this.resultsPage_ageOfChilds = resultsPage_ageOfChilds;
	}
	public boolean isResultsAvailable() {
		return isResultsAvailable;
	}
	public void setResultsAvailable(boolean isResultsAvailable) {
		this.isResultsAvailable = isResultsAvailable;
	}
	public boolean isProgramDisplayAvailable() {
		return isProgramDisplayAvailable;
	}
	public void setProgramDisplayAvailable(boolean isProgramDisplayAvailable) {
		this.isProgramDisplayAvailable = isProgramDisplayAvailable;
	}
	
	public String getPaymentDetails() {
		return paymentDetails;
	}
	public void setPaymentDetails(String paymentDetails) {
		this.paymentDetails = paymentDetails;
	}
	
	
	public String getPayOnline_MainTotalBookingValue() {
		return payOnline_MainTotalBookingValue;
	}
	public void setPayOnline_MainTotalBookingValue(
			String payOnline_MainTotalBookingValue) {
		this.payOnline_MainTotalBookingValue = payOnline_MainTotalBookingValue;
	}
	public String getPayOnline_GrossValue() {
		return payOnline_GrossValue;
	}
	public void setPayOnline_GrossValue(String payOnline_GrossValue) {
		this.payOnline_GrossValue = payOnline_GrossValue;
	}
	public String getPayOnline_TotalTax() {
		return payOnline_TotalTax;
	}
	public void setPayOnline_TotalTax(String payOnline_TotalTax) {
		this.payOnline_TotalTax = payOnline_TotalTax;
	}
	public String getPayOnline_TotalBooking() {
		return payOnline_TotalBooking;
	}
	public void setPayOnline_TotalBooking(String payOnline_TotalBooking) {
		this.payOnline_TotalBooking = payOnline_TotalBooking;
	}
	public String getPayOnline_AmountProNow() {
		return payOnline_AmountProNow;
	}
	public void setPayOnline_AmountProNow(String payOnline_AmountProNow) {
		this.payOnline_AmountProNow = payOnline_AmountProNow;
	}
	public String getPayOnline_Due() {
		return payOnline_Due;
	}
	public void setPayOnline_Due(String payOnline_Due) {
		this.payOnline_Due = payOnline_Due;
	}
	public String getPaymentGatewayTotalAmount() {
		return paymentGatewayTotalAmount;
	}
	public void setPaymentGatewayTotalAmount(String paymentGatewayTotalAmount) {
		this.paymentGatewayTotalAmount = paymentGatewayTotalAmount;
	}
	public String getPaymentPageTax() {
		return paymentPageTax;
	}
	public void setPaymentPageTax(String paymentPageTax) {
		this.paymentPageTax = paymentPageTax;
	}
	public String getPaymentPageGrossBookingValue() {
		return paymentPageGrossBookingValue;
	}
	public void setPaymentPageGrossBookingValue(String paymentPageGrossBookingValue) {
		this.paymentPageGrossBookingValue = paymentPageGrossBookingValue;
	}
	public String getPaymentPageTotalBookingValue() {
		return paymentPageTotalBookingValue;
	}
	public void setPaymentPageTotalBookingValue(String paymentPageTotalBookingValue) {
		this.paymentPageTotalBookingValue = paymentPageTotalBookingValue;
	}
	
	
	
	public long getTimeLoadpayment() {
		return timeLoadpayment;
	}
	public void setTimeLoadpayment(long loadPayment) {
		this.timeLoadpayment = loadPayment;
	}
	public long getTimeConfirmBooking() {
		return timeConfirmBooking;
	}
	public void setTimeConfirmBooking(long comfirmBooking) {
		this.timeConfirmBooking = comfirmBooking;
	}
	
	public String getSupplierConNo() {
		return supplierConNo;
	}
	public void setSupplierConNo(String supplierConNo) {
		this.supplierConNo = supplierConNo;
	}
	
	public String getReservationNo() {
		return reservationNo;
	}
	public void setReservationNo(String reservationNo) {
		this.reservationNo = reservationNo;
	}
	public String getBookingStatus() {
		return bookingStatus;
	}
	public void setBookingStatus(String bookingStatus) {
		this.bookingStatus = bookingStatus;
	}
	public String getBookingPageActivityName() {
		return bookingPageActivityName;
	}
	public void setBookingPageActivityName(String bookingPageActivityName) {
		this.bookingPageActivityName = bookingPageActivityName;
	}
	public String getPaymentPageStatus() {
		return paymentPageStatus;
	}
	public void setPaymentPageStatus(String paymentPageStatus) {
		this.paymentPageStatus = paymentPageStatus;
	}
	public String getActivityCurrency() {
		return activityCurrency;
	}
	public void setActivityCurrency(String activityCurrency) {
		this.activityCurrency = activityCurrency;
	}
	public ArrayList<String> getCancelPolicy() {
		return cancelPolicy;
	}
	public void setCancelPolicy(ArrayList<String> cancelPolicy) {
		this.cancelPolicy = cancelPolicy;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTraceId() {
		return traceId;
	}
	public void setTraceId(String traceId) {
		this.traceId = traceId;
	}
	public String getActDescription() {
		return actDescription;
	}
	public void setActDescription(String actDescription) {
		this.actDescription = actDescription;
	}
	public String getActivityRate() {
		return activityRate;
	}
	public void setActivityRate(String activityRate) {
		this.activityRate = activityRate;
	}
	public String getActivityName_result() {
		return activityName_result;
	}
	public void setActivityName_result(String activityName_result) {
		this.activityName_result = activityName_result;
	}
	public String getActivitySession() {
		return activitySession;
	}
	public void setActivitySession(String activitySession) {
		this.activitySession = activitySession;
	}
	public String getActivityRateType() {
		return activityRateType;
	}
	public void setActivityRateType(String activityRateType) {
		this.activityRateType = activityRateType;
	}
	public String getActivity_qty() {
		return activity_qty;
	}
	public void setActivity_qty(String activity_qty) {
		this.activity_qty = activity_qty;
	}
	
	

}


















