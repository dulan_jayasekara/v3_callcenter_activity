package com.reader;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.controller.PG_Properties;
import com.gargoylesoftware.htmlunit.javascript.host.Document;
import com.model.ActivityDetails;
import com.model.ActivityInventoryRecords;
import com.model.PaymentDetails;
import com.model.Search;
import com.types.TourOperatorType;

public class ReservationInfo {
	
	private WebDriver driver = null;
	private FirefoxProfile profile;
	private String traceValue;
	private String activityName;
	private String date1;
	private String currentDate;
	List<String> valueList = new ArrayList<String>();
	private String idOnly, idForPrize, idOnlyAvailable;
	private long resultsAvailable;
	private long searchButtClick;
	private long loadPayment, addedtoCart, availablClick;
	private long comfirmBooking, firstSearch, firstResultsAvail;
	private int childCountFromexcel;
	private int totalPax, activityCount;
	private Search search_Info;
	ArrayList<String> ageList = new ArrayList<String>();
	private int daysCount = 0;
	private String traceId, currentDateforImages, cCountry;
	ArrayList<String> cusTitle = new ArrayList<String>();
	ArrayList<String> cusFName = new ArrayList<String>();
	ArrayList<String> cusLname = new ArrayList<String>();
	ArrayList<String> cusTitleConfirm = new ArrayList<String>();
	ArrayList<String> cusFNameConfirm = new ArrayList<String>();
	ArrayList<String> cusLnameConfirm = new ArrayList<String>();
	private ActivityDetails activityDetails;
	private ActivityInventoryRecords inventoryList;
	private TourOperatorType  UserTypeDC_B2B;
	
	
	public ReservationInfo(Search search, WebDriver driver2, ActivityInventoryRecords inventoryList){
		this.search_Info = search;
		this.inventoryList = inventoryList;
		
		
	}
	
	public void setUp() {
		profile = new FirefoxProfile(new File(PG_Properties.getProperty("Firefox.Profile")));

		driver = new FirefoxDriver(profile);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS); ////entire flow
		
		
	}
	
	public boolean login(WebDriver Driver){

		driver = Driver;
		driver.get(PG_Properties.getProperty("Baseurl") + "/admin/common/LoginPage.do");
		driver.findElement(By.id("user_id")).sendKeys(PG_Properties.getProperty("UserName"));
		driver.findElement(By.id("password")).sendKeys(PG_Properties.getProperty("Password"));
		driver.findElement(By.id("loginbutton")).click();
		
		try {			
			driver.findElement(By.id("mainmenurezglogo"));
			return true;
		} catch (Exception e) {
			System.out.println("Login Fail...");
			return false;
		}		
	}

	
	public void bookingEngine(WebDriver Driver, ActivityDetails activityDetails2) throws InterruptedException, IOException{
		
		this.driver = Driver;		
		this.activityDetails = activityDetails2;
		
		DateFormat dateFormatcurrentDate = new SimpleDateFormat("yyyy_MM_dd");
		Calendar cal = Calendar.getInstance();
		currentDateforImages = dateFormatcurrentDate.format(cal.getTime());
				
		driver.get(PG_Properties.getProperty("Baseurl") + "/operations/reservation/CallCenterWarSearchCriteria.do?module=operations");
		Thread.sleep(1500);
		
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File(""+currentDateforImages+"/"+search_Info.getScenarioCount()+"_BookingEngine.png"));       
		
        driver.switchTo().frame("live_message_frame");	
        
        UserTypeDC_B2B = inventoryList.getUserTypeDC_B2B();
        
        //Select a user type
        
        if (!(UserTypeDC_B2B == TourOperatorType.DC)) {
        	
        	driver.findElement(By.xpath(".//*[@id='tour_operator_radio_WJ_9']")).click();
        	Thread.sleep(1000);
        	       	
        	String TO_Name = PG_Properties.getProperty(UserTypeDC_B2B.toString());
        	Thread.sleep(1000);
        	driver.findElement(By.id("tourOperatorName_WJ_9")).sendKeys(TO_Name);
        	driver.findElement(By.xpath(".//*[@id='B2B / Partner Name_lkup']")).click();			
			Thread.sleep(3000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame("live_message_frame");
			Thread.sleep(4000);
			driver.findElement(By.xpath(".//*[@id='lookup_dialog']/div[1]/ul/li[1]")).click();
			Thread.sleep(1000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame("live_message_frame");    
			Thread.sleep(1000);
			
			File scrFileTO = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(scrFileTO, new File(""+currentDateforImages+"/"+search_Info.getScenarioCount()+"_TOReservations.png"));
        	
        }
        
        Thread.sleep(1500);
        if (UserTypeDC_B2B == TourOperatorType.DC) {
                 
        	new org.openqa.selenium.support.ui.Select(driver.findElement(By.xpath(".//*[@id='select_currency']"))).selectByValue(search_Info.getSellingCurrency());   		
        	Thread.sleep(1000);
        	
        	driver.findElement(By.xpath(".//*[@id='select_currency']")).click();
    		Thread.sleep(500);
    		File scrFile67 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(scrFile67, new File(""+currentDateforImages+"/"+search_Info.getScenarioCount()+"_SellingCurrencyList.png"));
            Thread.sleep(500);
        }
               
          		
		driver.switchTo().frame("bec_container_frame");	
		
		if (driver.findElement(By.id("activities")).isDisplayed() == false) {
			activityDetails.setBookingEngineLoaded(false);
		}
				
		driver.findElement(By.xpath(".//*[@id='activities']")).click();
		Thread.sleep(500);
		
		////
		
		if (driver.findElement(By.id("AC_Country")).isDisplayed() == false) {
			activityDetails.setCountryLoaded(false);
		}
		
		try {
			driver.findElement(By.xpath(".//*[@id='AC_Country']")).click();
			Thread.sleep(500);
			File scrFile66 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile66, new File(""+currentDateforImages+"/"+search_Info.getScenarioCount()+"_CountryList.png"));
			driver.findElement(By.id("activities")).click();
			Thread.sleep(500);
		} catch (WebDriverException e1) {
			e1.printStackTrace();
		}
		
		if (driver.findElement(By.id("activity_Loc")).isDisplayed() == false) {
			activityDetails.setCityLoaded(false);
		}
		if (driver.findElement(By.id("ac_departure_temp")).isDisplayed() == false) {
			activityDetails.setDateFromLoaded(false);
		}
		if (driver.findElement(By.id("ac_arrival_temp")).isDisplayed() == false) {
			activityDetails.setDateToLoaded(false);
		}
		if (driver.findElement(By.id("R1occAdults_A")).isDisplayed() == false) {
			activityDetails.setAdultsLoaded(false);
		}
		if (driver.findElement(By.id("R1occChildren_A")).isDisplayed() == false) {
			activityDetails.setChildsLoaded(false);
		}
		
		
		if (!(driver.findElement(By.xpath(".//*[@id='AC_Country']/option[3]")).getText().equalsIgnoreCase("Albania"))) {
			activityDetails.setCountryListLoaded(false);
		}
		
		driver.findElement(By.xpath(".//*[@id='activityDisplay']/div[1]/div[1]/div[5]/div[1]/div/img")).click();
		Thread.sleep(1000);
		File scrFile22 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile22, new File(""+currentDateforImages+"/"+search_Info.getScenarioCount()+"_BookingEngine_CalenderAvailable.png"));
		if (driver.findElement(By.id("ui-datepicker-div")).isDisplayed() == false) {
			activityDetails.setCalenderAvailble(false);
		}
		
		driver.findElement(By.xpath(".//*[@id='activities']")).click();
		Thread.sleep(1500);
		
		if (driver.findElement(By.id("activityTypeId_a")).isDisplayed() == false) {
			activityDetails.setProgramCat(false);
		}
		
		File scrFile68 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		driver.findElement(By.xpath(".//*[@id='activityTypeId_a']")).click();		
        FileUtils.copyFile(scrFile68, new File(""+currentDateforImages+"/"+search_Info.getScenarioCount()+"_ProgramCategoryList.png"));
        Thread.sleep(500);
		
		
		if (driver.findElement(By.id("AC_consumerCurrencyCode")).isDisplayed() == false) {
			activityDetails.setPreCurrency(false);
		}
		if (driver.findElement(By.id("discountCoupon_No_A")).isDisplayed() == false) {
			activityDetails.setPromoCode(false);
		}
		
		
		////
		
		try {
			new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("AC_Country"))).selectByVisibleText(search_Info.getCountry());
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		////////
		
		String hiddenDest = search_Info.getDestination();		
		driver.findElement(By.id("activity_Loc")).sendKeys(hiddenDest);
		
		JavascriptExecutor javaScriptExe = (JavascriptExecutor) driver;
		javaScriptExe.executeScript("document.getElementsByName('hid_AC_Loc')[0].setAttribute('value','"+hiddenDest+"');");
		
		((JavascriptExecutor)driver).executeScript("$('#ac_departure').val('"+search_Info.getDateFrom()+"');");
     	((JavascriptExecutor)driver).executeScript("$('#ac_arrival').val('"+search_Info.getDateTo()+"');");
				
		///////
		
		/*
		String FromDate = search.getDateFrom();
		String[] partsPF = FromDate.split("/");
		String fMonth = partsPF[0]; 
		String fDay = partsPF[1]; 
		String fYear = partsPF[2]; 
		
		driver.findElement(By.xpath(".//*[@id='activityDisplay']/div[1]/div[1]/div[5]/div[1]/div/img")).click();
		
		WebElement dateWidget = driver.findElement(By.id("ui-datepicker-div"));  
		List<WebElement> rows=dateWidget.findElements(By.tagName("tr"));  
		List<WebElement> columns=dateWidget.findElements(By.tagName("td"));  
		
		for (WebElement cell: columns){  
			   if (cell.getText().equals(fDay)){  
			   cell.findElement(By.linkText(fDay)).click();  
			   break;  
			   }  
		}   
				
		String toDate = search.getDateTo();
		String[] partsPFT = toDate.split("/");
		String tMonth = partsPFT[0]; 
		String tday = partsPFT[1]; 
		String tYear = partsPFT[2]; 
		
		driver.findElement(By.xpath(".//*[@id='activityDisplay']/div[1]/div[1]/div[5]/div[2]/div/img")).click();
		
		WebElement dateWidgetTo = driver.findElement(By.id("ui-datepicker-div"));  
		List<WebElement> rowsTo = dateWidgetTo.findElements(By.tagName("tr"));  
		List<WebElement> columnsTo = dateWidgetTo.findElements(By.tagName("td"));  
		
		for (WebElement cell: columnsTo){    
			   if (cell.getText().equals(tday)){  
			   cell.findElement(By.linkText(tday)).click();  
			   break;  
			   }  
		}   
		*/		
		///////
		
		new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("R1occAdults_A"))).selectByVisibleText(search_Info.getAdults());
		new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("R1occChildren_A"))).selectByVisibleText(search_Info.getChildren());
		
		childCountFromexcel = Integer.parseInt(search_Info.getChildren());
		totalPax = childCountFromexcel + Integer.parseInt(search_Info.getAdults());
		
		if (search_Info.getAgeOfChildren().contains("#")) {
			
			String ages = search_Info.getAgeOfChildren();
			String[] partsEA = ages.split("#");
			for (String value : partsEA) {
				valueList.add(value);
			}
		}
		
		else{
			String ages = search_Info.getAgeOfChildren();
			valueList.add(ages);
		}
		
		for (int i = 0; i < childCountFromexcel; i++) {
			
			new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("A_R1childage_"+(i+1)+""))).selectByVisibleText(valueList.get(i));			
		}
		
		activityName = search_Info.getActivityName();
		date1 = search_Info.getActivityDate();
		activityDetails.setScenarioCount(search_Info.getScenarioCount());
		
		//new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("activityTypeId_a"))).selectByVisibleText(search.getProgramCategory());
		//new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("AC_consumerCurrencyCode"))).selectByVisibleText(search.getPreferCurrency());
		
		//((JavascriptExecutor)driver).executeScript("return validate('formA');");
		/*WebElement element = driver.findElement(By.id("search_btns_h"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);*/
		
		//((JavascriptExecutor)driver).executeScript("return validate('formA');");
		//driver.findElement(By.id("search_btns_h")).click();
		
		JavascriptExecutor js1 = (JavascriptExecutor) driver;
		js1.executeScript("JavaScript:search('A');");
		
		firstSearch = (System.currentTimeMillis() / 1000);	
		Thread.sleep(1500);
	
	}
	
	
	public ActivityDetails searchResults(PaymentDetails paymentDetails, ActivityInventoryRecords inventory, WebDriver Driver) throws InterruptedException, ParseException, IOException{
				
		WebDriverWait wait = new WebDriverWait(driver, 300);	
		
		activityDetails.setTimeFirstSearch(firstSearch);
		
		driver.switchTo().defaultContent();
		driver.switchTo().frame("live_message_frame");
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("bec_container_frame")));
		firstResultsAvail = (System.currentTimeMillis() / 1000);			
		activityDetails.setTimeFirstResults(firstResultsAvail);	
		
		Thread.sleep(2000);
		File scrFile_2 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile_2, new File(""+currentDateforImages+"/"+search_Info.getScenarioCount()+"_ResultsPage.png"));
		
		Calendar cal = Calendar.getInstance();
		Calendar cal1 = Calendar.getInstance();
		
		SimpleDateFormat format1 = new SimpleDateFormat("dd-MM-yyyy");
		currentDate = format1.format(cal.getTime());
		
		SimpleDateFormat formatToDate = new SimpleDateFormat("dd-MMM-yyyy");
		String cDate = formatToDate.format(cal1.getTime());
		activityDetails.setCurrentDate(cDate);
		Thread.sleep(2000);
		
		if (driver.findElements(By.xpath(".//*[@id='progresults_1']")).size() != 0) {
		
			driver.switchTo().defaultContent();
			driver.switchTo().frame("live_message_frame");	
			
			activityCount = Integer.parseInt(driver.findElement(By.xpath(".//*[@id='pagination_WJ_5']/div[1]")).getText().split(" ")[6]);
			System.out.println("Activity count - " + activityCount);
			activityDetails.setActivityTotalCount(activityCount);
			
			////
			
			if (driver.findElement(By.id("price_slider_WJ_16")).isDisplayed() == false) {
				activityDetails.setPriceRangeFilter(false);
			}
			
			if (driver.findElement(By.id("booktype_text_WJ_18")).isDisplayed() == false) {
				activityDetails.setProgramAvailablity(false);
			}
			
			try {
				if (driver.findElement(By.id("name_text_WJ_17")).isDisplayed() == false) {
					activityDetails.setActivityNameFilter(false);
				}
			} catch (Exception e2) {
				activityDetails.setActivityNameFilter(false);
			}
		
			boolean resultsElement = driver.findElement(By.xpath(".//*[@id='progresults_1']")).isDisplayed();
			activityDetails.setResultsAvailable(resultsElement);
			
			int wholepages = (activityCount/10);
			
			if(((activityCount)%10) == 0){				
				System.out.println("Page count - " + wholepages);
			}else{
				wholepages ++;
				System.out.println("Page count - " + wholepages);
			}
				
			
			Thread.sleep(1500);
			String filterCurrency = driver.findElement(By.xpath(".//*[@id='price_text_WJ_16']")).getText().split(" ")[1];
			activityDetails.setFiltercurrency(filterCurrency);
			
			///
			//Activity Price Slider
			String minValue;
			String maxValue;
			
			try {
				minValue = driver.findElement(By.xpath(".//*[@id='price_text_WJ_16']")).getText().split(" ")[2];
				maxValue = driver.findElement(By.xpath(".//*[@id='price_text_WJ_16']")).getText().split(" ")[6];
				activityDetails.setMinPrize(minValue);
				activityDetails.setMaxPrize(maxValue);
			} catch (Exception e2) {
				activityDetails.setMinPrize("Not Available");
				activityDetails.setMaxPrize("Not Available");
			}
			
			//1
			WebElement elemForPrize = driver.findElement(By.id("progresults_1"));								
			WebElement idElementPrize = elemForPrize.findElement(By.id("program_id_1"));
			String idForElementPrize = idElementPrize.getAttribute("value");
			
			String pageLoadedPrize = driver.findElement(By.xpath(".//*[@id='dailyrate_" +idForElementPrize+ "_" +date1+ "']")).getText().split("\n")[1].split(" ")[1];
			activityDetails.setPageLoadedResults(pageLoadedPrize);
				
			
			/*//2
			
			WebElement slider = driver.findElement(By.xpath(".//*[@id='price_slider_WJ_16']/a[1]"));
			Actions move = new Actions(driver);
			Action action = move.dragAndDropBy(slider, 10, 0).build();
			action.perform();
			Thread.sleep(1000);	
			
			WebElement tabEleForPrizeList = driver.findElement(By.id("activity_dv_WJ_7"));	
			String idForElementPrize_1 = tabEleForPrizeList.findElement(By.tagName("table")).getAttribute("id");
			
			WebElement elem_1 = driver.findElement(By.id(idForElementPrize_1));								
			WebElement idElement_1 = elem_1.findElement(By.id(idForElementPrize_1.replace("progresults_", "program_id_")));
			String idForElementPrize_1222 = idElement_1.getAttribute("value");
			
			String prizeChanged = driver.findElement(By.xpath(".//*[@id='price_text_WJ_16']")).getText().split(" ")[2];
			activityDetails.setPrizeChanged_1(prizeChanged);
			String resultsPrize = driver.findElement(By.xpath(".//*[@id='dailyrate_" +idForElementPrize_1222+ "_" +date1+ "']")).getText().split("\n")[1].split(" ")[1];
			activityDetails.setPrizeResults_1(resultsPrize);
				
			
			//3
			
			Actions move2 = new Actions(driver);
			Action action_2 = move2.dragAndDropBy(slider, 200, 0).build();
			action_2.perform();	
			
			WebElement tabEleForPrizeList_1 = driver.findElement(By.id("activity_dv_WJ_7"));	
			String idForElementPrize_2444 = tabEleForPrizeList_1.findElement(By.tagName("table")).getAttribute("id");
			
			WebElement elem_2 = driver.findElement(By.id(idForElementPrize_2444));								
			WebElement idElement_2 = elem_2.findElement(By.id(idForElementPrize_2444.replace("progresults_", "program_id_")));
			String idForElementPrize_2111 = idElement_2.getAttribute("value");
			
			String prizeChanged_100 = driver.findElement(By.xpath(".//*[@id='price_text_WJ_16']")).getText().split(" ")[2];
			activityDetails.setPrizeChanged_2(prizeChanged_100);
			String resultsPrize_100 = driver.findElement(By.xpath(".//*[@id='dailyrate_" +idForElementPrize_2111+ "_" +date1+ "']")).getText().split("\n")[1].split(" ")[1];
			activityDetails.setPrizeResults_2(resultsPrize_100);
			
			Thread.sleep(1500);	
			driver.findElement(By.xpath(".//*[@id='reset_all']")).click();
			Thread.sleep(2500);	*/
			
			
			////
			
			driver.switchTo().frame("bec_container_frame");	
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("search_btns_h")));
			Thread.sleep(2500);
			
			//Search again frame
			
			if (driver.findElement(By.id("activity_Loc")).isDisplayed() == false) {
				activityDetails.setSearchAgainCityLoaded(false);
			}
			
			if (driver.findElement(By.id("ac_departure_temp")).isDisplayed() == false) {
				activityDetails.setSearchAgainDateFromLoaded(false);
			}
			
			if (driver.findElement(By.id("ac_arrival_temp")).isDisplayed() == false) {
				activityDetails.setSearchAgainDateToLoaded(false);
			}
			
			if (driver.findElement(By.id("R1occAdults_A")).isDisplayed() == false) {
				activityDetails.setAdultLoaded(false);
			}
			
			if (driver.findElement(By.id("R1occChildren_A")).isDisplayed() == false) {
				activityDetails.setChildLoaded(false);
			}
			
			if (driver.findElement(By.id("activityTypeId_a")).isDisplayed() == false) {
				activityDetails.setSearchAgainProgramCat(false);
			}
			
			if (driver.findElement(By.id("AC_consumerCurrencyCode")).isDisplayed() == false) {
				activityDetails.setSearchAgainPreCurrency(false);
			}
			
			if (driver.findElement(By.id("discountCoupon_No_A")).isDisplayed() == false) {
				activityDetails.setSearchAgainPromoCode(false);
			}
			
			
			
			
			String resultsPage_destinaton = driver.findElement(By.id("activity_Loc")).getAttribute("value");
			activityDetails.setResultsPage_destinaton(resultsPage_destinaton);
			
			String resultsPage_dateFrom = driver.findElement(By.id("ac_departure")).getAttribute("value");
			activityDetails.setResultsPage_dateFrom(resultsPage_dateFrom);
			
			String resultsPage_dateTo = driver.findElement(By.id("ac_arrival")).getAttribute("value");
			activityDetails.setResultsPage_dateTo(resultsPage_dateTo);
			
			Select selectItem = new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("R1occAdults_A")));
			WebElement ele = selectItem.getFirstSelectedOption();	
			activityDetails.setResultsPage_adults(ele.getText());
			
			Select selectItem1 = new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("R1occChildren_A")));
			WebElement ele1 = selectItem1.getFirstSelectedOption();	
			activityDetails.setResultsPage__childs(ele1.getText());
			
			for (int i = 0; i < childCountFromexcel; i++) {
				
				Select selectItem2 = new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("A_R1childage_"+(i+1)+"")));
				WebElement ele2 = selectItem2.getFirstSelectedOption();
				String values = ele2.getText();	
				ageList.add(values);
			}
			
			activityDetails.setResultsPage_ageOfChilds(ageList);
			
				
			////
							
			driver.switchTo().defaultContent();
			driver.switchTo().frame("live_message_frame");	
			ArrayList<Integer> prizeList = new ArrayList<Integer>();
			ArrayList<Integer> prizeListEqual = new ArrayList<Integer>();
			
			/*
			if (10 <= activityCount) {
				
				for (int j = 1; j <= 10; j++) {
					
					WebElement elem1 = driver.findElement(By.id("progresults_" +j+ ""));								
					WebElement idElement = elem1.findElement(By.id("program_id_"+j+""));
					idForPrize = idElement.getAttribute("value");
					
					int prizeForList = Integer.parseInt(driver.findElement(By.xpath(".//*[@id='dailyrate_" +idForPrize+ "_" +date1+ "']")).getText().split("\n")[1].split(" ")[1]);
					prizeList.add(prizeForList);
					
					//Select first activity
					
					if (j == 1) {
						
						try {
							
							WebElement addToCartElemet = driver.findElement(By.xpath(".//*[@id='dailyrate_" +idForPrize+ "_" +date1+ "']"));
							addToCartElemet.click();
							Thread.sleep(4000);
							
							if (driver.findElements(By.id("activitydetails_"+idForPrize+"")).size() == 0) {
								driver.findElement(By.xpath(".//*[@id='dailyrate_" +idForPrize+ "_" +date1+ "']")).click();
							}
							
							Thread.sleep(2000);
							wait.until(ExpectedConditions.presenceOfElementLocated(By.id("activitydetails_"+idForPrize+"")));
							wait.until(ExpectedConditions.presenceOfElementLocated(By.id("addtocart_continue_"+idForPrize+"_0")));
							Thread.sleep(2500);
							
							driver.findElement(By.xpath(".//*[@id='addtocart_continue_" +idForPrize+"_0']")).click();
							Thread.sleep(5500);
							wait.until(ExpectedConditions.presenceOfElementLocated(By.id("cart_sub_total")));
							
							File scrFile_6 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					        FileUtils.copyFile(scrFile_6, new File(""+currentDateforImages+"/"+search_Info.getScenarioCount()+"_AddToCart.png"));											        
							
							//Remove Activity
							driver.findElement(By.xpath(".//*[@id='remove_a_0_0']")).click();
							Thread.sleep(2000);
													
							File scrFile_7 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					        FileUtils.copyFile(scrFile_7, new File(""+currentDateforImages+"/"+search_Info.getScenarioCount()+"_RemoveCart.png"));							
					        
							Thread.sleep(3500);
								
						} catch (Exception e) {
							activityDetails.setProgramsAvailabilityActivityTypes(false);
							activityDetails.setAddToCart(false);
						}
												
					}					
				}
				
				
			} else {
				
				for (int j = 1; j <= activityCount; j++) {
					
					WebElement elem1 = driver.findElement(By.id("progresults_" +j+ ""));								
					WebElement idElement = elem1.findElement(By.id("program_id_"+j+""));
					idForPrize = idElement.getAttribute("value");
					
					int prizeForList = Integer.parseInt(driver.findElement(By.xpath(".//*[@id='dailyrate_" +idForPrize+ "_" +date1+ "']")).getText().split("\n")[1].split(" ")[1]);
					prizeList.add(prizeForList);
					
					//Select first activity
					
					if (j == 1) {
						
						try {
							
							WebElement addToCartElemet = driver.findElement(By.xpath(".//*[@id='dailyrate_" +idForPrize+ "_" +date1+ "']"));
							addToCartElemet.click();
							Thread.sleep(4000);
							
							if (driver.findElements(By.id("activitydetails_"+idForPrize+"")).size() == 0) {
								driver.findElement(By.xpath(".//*[@id='dailyrate_" +idForPrize+ "_" +date1+ "']")).click();
							}
							
							Thread.sleep(2000);
							wait.until(ExpectedConditions.presenceOfElementLocated(By.id("activitydetails_"+idForPrize+"")));
							wait.until(ExpectedConditions.presenceOfElementLocated(By.id("addtocart_continue_"+idForPrize+"_0")));
							Thread.sleep(2500);
							
							driver.findElement(By.xpath(".//*[@id='addtocart_continue_" +idForPrize+"_0']")).click();
							Thread.sleep(3500);
							wait.until(ExpectedConditions.presenceOfElementLocated(By.id("cart_sub_total")));		
							
							File scrFile_6 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					        FileUtils.copyFile(scrFile_6, new File(""+currentDateforImages+"/"+search_Info.getScenarioCount()+"_AddToCart.png"));
					        							
							//Remove Activity
							driver.findElement(By.xpath(".//*[@id='remove_a_0_0']")).click();
							Thread.sleep(2000);
							File scrFile_7 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					        FileUtils.copyFile(scrFile_7, new File(""+currentDateforImages+"/"+search_Info.getScenarioCount()+"_RemoveCart.png"));							
					        
							Thread.sleep(3500);
							
							
						} catch (Exception e) {
							activityDetails.setProgramsAvailabilityActivityTypes(false);
							activityDetails.setAddToCart(false);
						}
												
					}
				}
			}
			
			prizeListEqual.addAll(prizeList);
			activityDetails.setBeforeSorting(prizeListEqual);						
			Collections.sort(prizeList);
			activityDetails.setAfterSorting(prizeList);
			*/
			
			if (activityDetails.isAddToCart() == true) {
									
				Thread.sleep(1500);
				driver.switchTo().defaultContent();
				driver.switchTo().frame("live_message_frame");	
				driver.switchTo().frame("bec_container_frame");	
				
				//Changing the dates
				
				SimpleDateFormat dateFormat132 = new SimpleDateFormat("MM/dd/yyyy");   
				Calendar cal13 = Calendar.getInstance();  
				Calendar cal2 = Calendar.getInstance();  			
				cal13.setTime( dateFormat132.parse(search_Info.getDateFrom()));    
				cal13.add( Calendar.DATE, 1 );  
				cal2.setTime( dateFormat132.parse(search_Info.getDateTo()));    
				cal2.add( Calendar.DATE, 2 );   
				String convertedDate=dateFormat132.format(cal13.getTime()); 
				String convertedDate222=dateFormat132.format(cal2.getTime()); 
				activityDetails.setChanged_DateFrom(convertedDate);
				activityDetails.setChanged_DateTo(convertedDate222);
				
				((JavascriptExecutor)driver).executeScript("$('#ac_departure').val('"+convertedDate+"');");
		     	((JavascriptExecutor)driver).executeScript("$('#ac_arrival').val('"+convertedDate222+"');");
		     	
		     	driver.switchTo().defaultContent();
				driver.switchTo().frame("live_message_frame");	
				driver.switchTo().frame("bec_container_frame");	
					
				Thread.sleep(1000);
				((JavascriptExecutor)driver).executeScript("search('A');");
			
				searchButtClick = (System.currentTimeMillis() / 1000);			
				activityDetails.setTimeSearchButtClickTime(searchButtClick);	
					
				driver.switchTo().defaultContent();
				driver.switchTo().frame("live_message_frame");	
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='progresults_1']")));
				resultsAvailable = (System.currentTimeMillis() / 1000);			
				activityDetails.setTimeResultsAvailable(resultsAvailable);
				Thread.sleep(3500);
	
							
				/////
				//Avail and onReq count
				int availCNT = 0;
				int onReqCNT = 0;
				
				activityCount = Integer.parseInt(driver.findElement(By.xpath(".//*[@id='pagination_WJ_5']/div[1]")).getText().split(" ")[6]);
				
				
				outerloop : for (int actvityPage = 1; actvityPage <= wholepages ; actvityPage++) {
					for (int i = 1+((actvityPage-1)*10) ; i <= actvityPage*10; i++) {
						
						if (i == (activityCount+1)) {
							
							break outerloop;
							
						} else {
									//internal													
							WebElement elem = driver.findElement(By.id("progresults_" +i+ ""));		
							String acName = elem.findElement(By.className("fleft")).getText();
							WebElement idElementForAvailability = elem.findElement(By.id("program_id_"+i+""));
							idOnlyAvailable = idElementForAvailability.getAttribute("value");
												
							String inventoryType = driver.findElement(By.xpath(".//*[@id='availability_"+idOnlyAvailable+"']")).getText();
							
							if (inventoryType.equalsIgnoreCase("Available")) {
								availCNT ++;
							}else{
								onReqCNT ++;
							}
							
							
						
							if(acName.equals(activityName)){
								
								WebElement daysElement = driver.findElement(By.id("progresults_" +i+ ""));
								List<WebElement> tdList = daysElement.findElements(By.tagName("td"));
							
								for (int m = 0; m < tdList.size(); m++) {
									
									String tdText = tdList.get(m).getText();
									
									if (tdText.contains("-")) {
										daysCount ++;
									}
								}
														
								activityDetails.setDaysCount(daysCount-1);
													
								WebElement idElement = daysElement.findElement(By.id("program_id_"+i+""));
								idOnly = idElement.getAttribute("value");
										
								String activityStatus = driver.findElement(By.xpath(".//*[@id='availability_"+idOnly+"']")).getText();
								activityDetails.setStatus(activityStatus);
								
								String activityCurr = driver.findElement(By.xpath(".//*[@id='dailyrate_" +idOnly+ "_" +date1+ "']")).getText().split("\n")[1];
								String activityCurrency = activityCurr.split(" ")[0];
								activityDetails.setActivityCurrency(activityCurrency);
								Thread.sleep(4000);		
								
								try {
									
									WebElement addToCartElemet = driver.findElement(By.xpath(".//*[@id='dailyrate_" +idOnly+ "_" +date1+ "']"));
									addToCartElemet.click();
									Thread.sleep(4000);
									
									if (driver.findElements(By.id("activitydetails_"+idOnly+"")).size() == 0) {
										driver.findElement(By.xpath(".//*[@id='dailyrate_" +idOnly+ "_" +date1+ "']")).click();
									}
									
									File scrFile_4 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
							        FileUtils.copyFile(scrFile_4, new File(""+currentDateforImages+"/"+search_Info.getScenarioCount()+"_ResultsAvailability.png"));
															        
									Thread.sleep(4000);
									wait.until(ExpectedConditions.presenceOfElementLocated(By.id("activitydetails_"+idOnly+"")));
									wait.until(ExpectedConditions.presenceOfElementLocated(By.id("addtocart_continue_"+idOnly+"_0")));
									Thread.sleep(2500);
									
								} catch (Exception e) {
									driver.findElement(By.xpath(".//*[@id='dailyrate_" +idOnly+ "_" +date1+ "']")).click();
									Thread.sleep(2000);
									wait.until(ExpectedConditions.presenceOfElementLocated(By.id("activitydetails_"+idOnly+"")));
									wait.until(ExpectedConditions.presenceOfElementLocated(By.id("addtocart_continue_"+idOnly+"_0")));
									File scrFile_4 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
							        FileUtils.copyFile(scrFile_4, new File(""+currentDateforImages+"/"+search_Info.getScenarioCount()+"_ResultsAvailability.png"));
									
									Thread.sleep(2500);
								}
								
								
								
								boolean available = driver.findElement(By.id("addtocart_continue_"+idOnly+"_0")).isDisplayed();
								activityDetails.setProgramDisplayAvailable(available);
								Thread.sleep(2500);
								
								String rateValue = driver.findElement(By.xpath(".//*[@id='rat_vl_"+idOnly+"_0']")).getText();
								activityDetails.setActivityRate(rateValue); 
								
								String acNameResults = driver.findElement(By.xpath(".//*[@id='act_nm_"+idOnly+"_0']")).getText();
								activityDetails.setActivityName_result(acNameResults);
								
								String ativtySession = driver.findElement(By.xpath(".//*[@id='ses_nm_"+idOnly+"_0']")).getText();
								activityDetails.setActivitySession(ativtySession);
								
								String rateType = driver.findElement(By.xpath(".//*[@id='rat_nm_"+idOnly+"_0']")).getText();
								activityDetails.setActivityRateType(rateType);
	
								new org.openqa.selenium.support.ui.Select(driver.findElement(By.name("select"))).selectByVisibleText(Integer.toString(totalPax));
																			
								driver.findElement(By.xpath(".//*[@id='cancellationpolicy_"+idOnly+"']")).click();
								wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='cxl_policy_main_WJ_12']")));	
								Thread.sleep(2000);
								
								if (driver.findElements(By.xpath(".//*[@id='cxl_policy_main_WJ_12']")).size() == 0) {
									activityDetails.setCancelPolicy(false);
								}
								
								File scrFile_5 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
						        FileUtils.copyFile(scrFile_5, new File(""+currentDateforImages+"/"+search_Info.getScenarioCount()+"_CancellationPolicy.png"));							
						        driver.findElement(By.xpath(".//*[@id='cancellationpolicy_dlg_WJ_12']/div[1]")).click();
								Thread.sleep(3000);
						        
								driver.findElement(By.xpath(".//*[@id='moreinfo_"+idOnly+"']")).click();
								wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='prog_info_thumb']/img")));
								Thread.sleep(2000);
								
								if (driver.findElements(By.xpath(".//*[@id='prog_info_thumb']/img")).size() == 0) {
									activityDetails.setMoreInfo(false);
								}
								
								File scrFile_3 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
						        FileUtils.copyFile(scrFile_3, new File(""+currentDateforImages+"/"+search_Info.getScenarioCount()+"_MoreInfo.png"));							
						        String description = driver.findElement(By.xpath(".//*[@id='tabs-1']/div[1]")).getText();
								activityDetails.setActDescription(description);
								driver.findElement(By.xpath(".//*[@id='more_info_activities_WJ_11']/div[1]")).click();
								wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='addtocart_continue_" +idOnly+"_0']")));													
								
								driver.findElement(By.xpath(".//*[@id='addtocart_continue_" +idOnly+"_0']")).click();
								Thread.sleep(3500);
								loadPayment = (System.currentTimeMillis() / 1000);			
								activityDetails.setTimeLoadpayment(loadPayment);
								
								Thread.sleep(10500);	
								wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='cart_sub_total']")));													
								
								String resultsPage_SubTotal = driver.findElement(By.xpath(".//*[@id='cart_sub_total']")).getText();
								activityDetails.setResultsPage_SubTotal(resultsPage_SubTotal);
								String resultsPage_Taxes = driver.findElement(By.xpath(".//*[@id='cart_tax']")).getText();
								activityDetails.setResultsPage_Taxes(resultsPage_Taxes);
								String resultsPage_TotalPayable = driver.findElement(By.xpath(".//*[@id='cart_totalpayable']")).getText();
								activityDetails.setResultsPage_TotalPayable(resultsPage_TotalPayable);
								
								String resultsPage_SubTotal_Currency = driver.findElement(By.xpath(".//*[@id='cart_display_WJ_9']/div/table/tbody/tr[3]/td[1]")).getText();
								activityDetails.setResultsPage_SubTotal_Currency(resultsPage_SubTotal_Currency);	
								String resultsPage_Taxes_Currency = driver.findElement(By.xpath(".//*[@id='cart_display_WJ_9']/div/table/tbody/tr[4]/td[1]")).getText();
								activityDetails.setResultsPage_Taxes_Currency(resultsPage_Taxes_Currency);
								String resultsPage_TotalPayable_Currency = driver.findElement(By.xpath(".//*[@id='cart_display_WJ_9']/div/table/tbody/tr[5]/td[1]")).getText();
								activityDetails.setResultsPage_TotalPayable_Currency(resultsPage_TotalPayable_Currency);
								
								addedtoCart = (System.currentTimeMillis() / 1000);			
								activityDetails.setTimeAddedtoCart(addedtoCart);	
								
								}
							}
						}
						
					JavascriptExecutor javaScriptExe1 = (JavascriptExecutor) driver;
					javaScriptExe1.executeScript("doIWC(new IWCParams('PAGING','RESULTS','PAGE','', {'pageNo':" +(actvityPage+1)+ "},'','WJ_5'))");
						
				}
				
				activityDetails.setActivityAvailableCount(availCNT);
				activityDetails.setActivityOnReqCount(onReqCNT);
					
				Thread.sleep(2500);
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='full_details']")));		
				driver.findElement(By.xpath(".//*[@id='full_details']")).click();					
				driver.switchTo().frame("productdetailsframe");
				
				Thread.sleep(10500);
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='programname_cxlpolicy_0']")));	
				File scrFile_8 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		        FileUtils.copyFile(scrFile_8, new File(""+currentDateforImages+"/"+search_Info.getScenarioCount()+"_ViewFullDetailsFrame.png"));
				
				String ViewFullDetails_ActivityName = driver.findElement(By.xpath(".//*[@id='programname_cxlpolicy_0']")).getText();
				activityDetails.setViewFullDetails_ActivityName(ViewFullDetails_ActivityName);
				
				String ViewFullDetails_currency1 = driver.findElement(By.xpath(".//*[@id='activity_info_table']/tbody/tr[4]/td/table/tbody/tr[1]/td[6]")).getText().substring(7,10);;
				activityDetails.setViewFullDetails_Currency1(ViewFullDetails_currency1);
					
				String ViewFullDetails_SubTotal = driver.findElement(By.xpath(".//*[@id='prog_subtotal_0']")).getText();
				activityDetails.setViewFullDetails_SubTotal(ViewFullDetails_SubTotal);
				String ViewFullDetails_Taxes = driver.findElement(By.xpath(".//*[@id='prog_tax_0']")).getText();
				activityDetails.setViewFullDetails_Taxes(ViewFullDetails_Taxes);
				String ViewFullDetails_TotalPayable = driver.findElement(By.xpath(".//*[@id='prog_totalbookval_0']")).getText();
				activityDetails.setViewFullDetails_TotalPayable(ViewFullDetails_TotalPayable);
				
				String ViewFullDetails_TotalGrossBookingValue = driver.findElement(By.xpath(".//*[@id='pkg_totalrate']")).getText();
				activityDetails.setViewFullDetails_TotalGrossBookingValue(ViewFullDetails_TotalGrossBookingValue);
				String ViewFullDetails_TotalTax = driver.findElement(By.xpath(".//*[@id='tottaxamountdivid']")).getText();
				activityDetails.setViewFullDetails_TotalTax(ViewFullDetails_TotalTax);
				String ViewFullDetails_TotalPackageValue = driver.findElement(By.xpath(".//*[@id='totchargeamt']")).getText();
				activityDetails.setViewFullDetails_TotalPackageValue(ViewFullDetails_TotalPackageValue);
				String ViewFullDetails_AmountProcessed = driver.findElement(By.xpath(".//*[@id='payNwpaymentsection']")).getText();
				activityDetails.setViewFullDetails_AmountProcessed(ViewFullDetails_AmountProcessed);
				Thread.sleep(1000);
				
				if (UserTypeDC_B2B == TourOperatorType.COMCASH || UserTypeDC_B2B == TourOperatorType.COMCREDITLPONO || UserTypeDC_B2B == TourOperatorType.COMCREDITLPOY) {
			        
					String ViewFullDetails_currency2 = driver.findElement(By.xpath(".//*[@id='total_transfer_info_table']/tbody/tr[2]/td[2]")).getText().substring(6,9);
					activityDetails.setViewFullDetails_Currency2(ViewFullDetails_currency2);
								
					String ViewFullDetails_AmountCheckIn = driver.findElement(By.xpath(".//*[@id='total_transfer_info_table']/tbody/tr[7]/td[2]")).getText();
					activityDetails.setViewFullDetails_AmountCheckIn(ViewFullDetails_AmountCheckIn);
					
					String ViewFullDetails_AgentComm = driver.findElement(By.xpath(".//*[@id='pkg_agentcommission']/span")).getText();
					activityDetails.setViewFullDetails_AgentCommisiion(ViewFullDetails_AgentComm);
						
				}else{
					
					String ViewFullDetails_currency2 = driver.findElement(By.xpath(".//*[@id='total_transfer_info_table']/tbody/tr[1]/td[2]")).getText().substring(6,9);
					activityDetails.setViewFullDetails_Currency2(ViewFullDetails_currency2);
								
					String ViewFullDetails_AmountCheckIn = driver.findElement(By.xpath(".//*[@id='total_transfer_info_table']/tbody/tr[6]/td[2]")).getText();
					activityDetails.setViewFullDetails_AmountCheckIn(ViewFullDetails_AmountCheckIn);
				}
				
		
				WebElement tableId = driver.findElement(By.id("cxlpolicy_a_0"));
				List<WebElement> rowList = tableId.findElements(By.tagName("li"));
				
				ArrayList<String> policyList = new ArrayList<String>();
				
				for (int j = 1; j <= rowList.size(); j++) {
					
					String cancelLiList = driver.findElement(By.xpath(".//*[@id='cxlpolicy_a_0']/ul/li["+j+"]")).getText();
					policyList.add(cancelLiList);
					
				}
				
				System.out.println(policyList);
				activityDetails.setCancelPolicy(policyList);
				
				driver.switchTo().defaultContent();
				driver.switchTo().frame("live_message_frame");	
				
				Thread.sleep(1000);
				try {
					driver.findElement(By.xpath("html/body/div[9]/div[1]/a/span")).click();
				} catch (Exception e3) {
					driver.findElement(By.xpath("html/body/div[10]/div[1]/a/span")).click();
				}
				Thread.sleep(2000);
				
				driver.findElement(By.id("loadpayment")).click();	
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='activate_mg']/div")));	
				
				
				//Payment Page
				
				String bookingStatusdd = driver.findElement(By.xpath(".//*[@id='progbooktype_0']")).getText().split("Status")[1];
				activityDetails.setResultsPage_BookingStatus(bookingStatusdd);
				activityDetails.setUserType(inventoryList.getUserTypeDC_B2B().toString());
				
				File scrFile_55 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		        FileUtils.copyFile(scrFile_55, new File(""+currentDateforImages+"/"+search_Info.getScenarioCount()+"_PaymentsPage.png"));
								
				String paymentPage_ActivityName = driver.findElement(By.xpath(".//*[@id='proginfo_0']")).getText();
				activityDetails.setPaymentPage_ActivityName(paymentPage_ActivityName);
				
				String qty = driver.findElement(By.xpath(".//*[@id='act_qty_0_0']")).getText();
				activityDetails.setResultsPage_QTY(qty);
				
				String paymentPage_Currency_1 = driver.findElement(By.xpath(".//*[@id='activity_info_table']/tbody/tr[4]/td/table/tbody/tr[1]/td[6]")).getText().substring(7,10);
				activityDetails.setPaymentPage_Currency_1(paymentPage_Currency_1);
				

				if (UserTypeDC_B2B == TourOperatorType.COMCASH || UserTypeDC_B2B == TourOperatorType.COMCREDITLPONO || UserTypeDC_B2B == TourOperatorType.COMCREDITLPOY) {
					
					String paymentPage_Currency_2 = driver.findElement(By.xpath(".//*[@id='total_transfer_info_table']/tbody/tr[2]/td[2]")).getText().substring(6,9);
					activityDetails.setPaymentPage_Currency_2(paymentPage_Currency_2);
					
				}else{
					String paymentPage_Currency_2 = driver.findElement(By.xpath(".//*[@id='total_transfer_info_table']/tbody/tr[1]/td[2]")).getText().substring(6,9);
					activityDetails.setPaymentPage_Currency_2(paymentPage_Currency_2);
				}
				////			
				
				new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("cusTitle"))).selectByVisibleText(paymentDetails.getCustomerTitle());
				
				if (!(UserTypeDC_B2B == TourOperatorType.DC)) {
			        
					driver.findElement(By.id("cusFName")).sendKeys(paymentDetails.getCustomerName());
					driver.findElement(By.id("cusLName")).sendKeys(paymentDetails.getCustomerLastName());
					driver.findElement(By.id("cusAdd2")).sendKeys(paymentDetails.getAddress_1());
					
					Select Page_country = new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("cusCountry")));
					WebElement Page_countryw = Page_country.getFirstSelectedOption();	
					cCountry = Page_countryw.getText();
					
					
				}
				
				
				
				if (UserTypeDC_B2B == TourOperatorType.DC){
					
					driver.findElement(By.id("cusFName")).sendKeys(paymentDetails.getCustomerName());
					driver.findElement(By.id("cusLName")).sendKeys(paymentDetails.getCustomerLastName());	
					
					driver.findElement(By.id("cusAdd1")).sendKeys(paymentDetails.getAddress());
					driver.findElement(By.id("cusAdd2")).sendKeys(paymentDetails.getAddress_1());
					
					String onLineTp_1 = paymentDetails.getTel().split("-")[0];
					String onLineTp_2 = paymentDetails.getTel().split("-")[1];
					
					driver.findElement(By.id("cusareacodetext")).sendKeys(onLineTp_1);
					driver.findElement(By.id("cusPhonetext")).sendKeys(onLineTp_2);
					
					new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("cusCountry"))).selectByVisibleText(paymentDetails.getCountry());		
					driver.findElement(By.id("cusCity")).sendKeys(paymentDetails.getCity());
					
					driver.findElement(By.xpath(".//*[@id='City_lkup']")).click();
					driver.switchTo().defaultContent();
					driver.switchTo().frame("live_message_frame");
					Thread.sleep(1500);
					driver.findElement(By.xpath(".//*[@id='lookup_dialog']/div[1]/ul/li[1]")).click();
					Thread.sleep(1000);
					driver.switchTo().defaultContent();
					driver.switchTo().frame("live_message_frame");
					
					Select Page_country = new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("cusCountry")));
					WebElement Page_countryw = Page_country.getFirstSelectedOption();	
					cCountry = Page_countryw.getText();
					
					driver.findElement(By.id("cusEmail")).sendKeys(paymentDetails.getEmail());
					
					if (cCountry.equals("USA") || cCountry.equals("Canada") || cCountry.equals("Australia")) {
											
						driver.findElement(By.xpath(".//*[@id='State_lkup']")).click();
						driver.switchTo().defaultContent();
						driver.switchTo().frame("live_message_frame");
						Thread.sleep(1000);
						driver.findElement(By.xpath(".//*[@id='lookup_dialog']/div[1]/ul/li[1]")).click();
						Thread.sleep(1000);
						driver.switchTo().defaultContent();
						driver.switchTo().frame("live_message_frame");
					
						driver.findElement(By.id("cusZip")).sendKeys("11300");			
					}
				}
									
				//Actiity info
				try {
					driver.findElement(By.xpath(".//*[@id='_pickupPoint']")).sendKeys("hotel california");
					Thread.sleep(1000);
				} catch (Exception e2) {
					e2.printStackTrace();
				}
								
				// QUOTATION req
				
				if (search_Info.getQuotaionReq().equalsIgnoreCase("Yes")) {
					
					driver.findElement(By.xpath(".//*[@id='submit_quote']")).click();
					driver.switchTo().defaultContent();
					driver.switchTo().frame("live_message_frame");	
					Thread.sleep(3000);
					wait.until(ExpectedConditions.presenceOfElementLocated(By.id("p_flight_bookings_1")));	
					
					String Quote_QuotationNo = driver.findElement(By.xpath(".//*[@id='p_flight_booking_reservation_no_1']")).getText().split(": ")[1];
					activityDetails.setQuote_QuotationNo(Quote_QuotationNo);
					
					String Quote_Status = driver.findElement(By.xpath(".//*[@id='prog_bookstatus_0']")).getText();
					activityDetails.setQuote_Status(Quote_Status);
					
					String Quote_Currency = driver.findElement(By.xpath(".//*[@id='main_content']/div/div/div/div[1]/table/tbody/tr/td/table/tbody/tr[4]/td[6]")).getText().substring(7,10);
					activityDetails.setQuote_Currency(Quote_Currency);
					
					String Quote_QTY = driver.findElement(By.xpath(".//*[@id='activity_qty_0_0']")).getText();
					activityDetails.setQuote_QTY(Quote_QTY);
					
					String Quote_ActivityName = driver.findElement(By.xpath(".//*[@id='proginfo_0']")).getText().split("- ")[1];
					activityDetails.setQuote_ActivityName(Quote_ActivityName);
					
					String Quote_FName = driver.findElement(By.xpath(".//*[@id='cusfirstname']")).getText();
					activityDetails.setQuote_FName(Quote_FName);
					
					String Quote_LName = driver.findElement(By.xpath(".//*[@id='cuslastname']")).getText();
					activityDetails.setQuote_LName(Quote_LName);
					
					String Quote_TP = driver.findElement(By.xpath(".//*[@id='custelephone']")).getText();
					activityDetails.setQuote_TP(Quote_TP);
					
					String Quote_Email = driver.findElement(By.xpath(".//*[@id='cusemail1']")).getText();
					activityDetails.setQuote_Email(Quote_Email);
					
					String Quote_address_1 = driver.findElement(By.xpath(".//*[@id='cusaddress1']")).getText();
					activityDetails.setQuote_address_1(Quote_address_1);
					
					String Quote_address_2 = driver.findElement(By.xpath(".//*[@id='cusaddress2']")).getText();
					activityDetails.setQuote_address_2(Quote_address_2);
					
					String Quote_city = driver.findElement(By.xpath(".//*[@id='cuscity']")).getText();
					activityDetails.setQuote_city(Quote_city);
					
					String Quote_country = driver.findElement(By.xpath(".//*[@id='cuscountry']")).getText();
					activityDetails.setQuote_country(Quote_country);
					
					if (cCountry.equals("USA")) {
						
						String Quote_state = driver.findElement(By.xpath(".//*[@id='cusstate']")).getText();
						activityDetails.setQuote_state(Quote_state);
						
						String Quote_postalCode = driver.findElement(By.xpath(".//*[@id='cuspostcode']")).getText();
						activityDetails.setQuote_postalCode(Quote_postalCode);
					}
					
					if (cCountry.equals("Canada")) {
						
						String Quote_state = driver.findElement(By.xpath(".//*[@id='cusstate']")).getText();
						activityDetails.setQuote_state(Quote_state);
						
						String Quote_postalCode = driver.findElement(By.xpath(".//*[@id='cuspostcode']")).getText();
						activityDetails.setQuote_postalCode(Quote_postalCode);
					}
					
					if (cCountry.equals("Australia")) {
						
						String Quote_state = driver.findElement(By.xpath(".//*[@id='cusstate']")).getText();
						activityDetails.setQuote_state(Quote_state);
						
						String Quote_postalCode = driver.findElement(By.xpath(".//*[@id='cuspostcode']")).getText();
						activityDetails.setQuote_postalCode(Quote_postalCode);
					}
					
					
					
					String Quote_subTotal = driver.findElement(By.xpath(".//*[@id='activity_subtotal']")).getText();
					activityDetails.setQuote_subTotal(Quote_subTotal);
					
					String Quote_taxandOther = driver.findElement(By.xpath(".//*[@id='activity_tax']")).getText();
					activityDetails.setQuote_taxandOther(Quote_taxandOther);
					
					String Quote_totalValue = driver.findElement(By.xpath(".//*[@id='activity_bookingvalue']")).getText();
					activityDetails.setQuote_totalValue(Quote_totalValue);
					
					String Quote_TotalGrossPackageValue = driver.findElement(By.xpath(".//*[@id='pkg_totalbeforetax']")).getText();
					activityDetails.setQuote_TotalGrossPackageValue(Quote_TotalGrossPackageValue);
					
					String Quote_TotalTax = driver.findElement(By.xpath(".//*[@id='pkg_totaltaxandothercharges']")).getText();
					activityDetails.setQuote_TotalTax(Quote_TotalTax);
					
					String Quote_TotalPackageValue = driver.findElement(By.xpath(".//*[@id='pkg_totalpayable']")).getText();
					activityDetails.setQuote_TotalPackageValue(Quote_TotalPackageValue);
					
					
					
									
					driver.get(PG_Properties.getProperty("Baseurl") + "/operations/reservation/CallCenterWarSearchCriteria.do?module=operations");
					Thread.sleep(2000);
			
					try {
						Alert alert = driver.switchTo().alert();
						alert.accept();
						
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					Thread.sleep(3000);
					driver.switchTo().defaultContent();
					driver.switchTo().frame("live_message_frame");	
					
					/*driver.findElement(By.xpath(".//*[@id='quot_ref_radio_WJ_6']")).click();
					Thread.sleep(8000);*/
					driver.switchTo().defaultContent();
					driver.switchTo().frame("live_message_frame");
					wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='quotationNo_WJ_6']")));
					driver.findElement(By.xpath(".//*[@id='quotationNo_WJ_6']")).sendKeys(activityDetails.getQuote_QuotationNo());
					driver.switchTo().defaultContent();
					driver.switchTo().frame("live_message_frame");
					Thread.sleep(8000);
					driver.findElement(By.xpath(".//*[@id='Quotation Ref. Number_lkup']")).click();
					Thread.sleep(4000);
					driver.findElement(By.xpath(".//*[@id='lookup_dialog']/div[1]/ul/li[1]")).click();
					Thread.sleep(3000);
					driver.switchTo().defaultContent();
					driver.switchTo().frame("live_message_frame");	
					//wait.until(ExpectedConditions.presenceOfElementLocated(By.id("loadpayment")));
					
					Thread.sleep(5000);
					WebElement viewFullEle = driver.findElement(By.id("full_details"));
					String href = viewFullEle.getAttribute("href").split(":")[1];
					((JavascriptExecutor)driver).executeScript(href);
					//driver.findElement(By.xpath(".//*[@id='full_details']")).click();					
					driver.switchTo().frame("productdetailsframe");
					
					
					Thread.sleep(3000);
					String Quote_ViewFullDetails_ActivityName = driver.findElement(By.xpath(".//*[@id='programname_cxlpolicy_0']")).getText();
					activityDetails.setQuote_ViewFullDetails_ActivityName(Quote_ViewFullDetails_ActivityName);
					
					String Quote_ViewFullDetails_currency1 = driver.findElement(By.xpath(".//*[@id='activity_info_table']/tbody/tr[4]/td/table/tbody/tr[1]/td[6]")).getText().substring(7,10);;
					activityDetails.setQuote_ViewFullDetails_Currency1(Quote_ViewFullDetails_currency1);
					
					String Quote_ViewFullDetails_SubTotal = driver.findElement(By.xpath(".//*[@id='prog_subtotal_0']")).getText();
					activityDetails.setQuote_ViewFullDetails_SubTotal(Quote_ViewFullDetails_SubTotal);
					String Quote_ViewFullDetails_Taxes = driver.findElement(By.xpath(".//*[@id='prog_tax_0']")).getText();
					activityDetails.setQuote_ViewFullDetails_Taxes(Quote_ViewFullDetails_Taxes);
					String Quote_ViewFullDetails_TotalPayable = driver.findElement(By.xpath(".//*[@id='prog_totalbookval_0']")).getText();
					activityDetails.setQuote_ViewFullDetails_TotalPayable(Quote_ViewFullDetails_TotalPayable);
					
					String Quote_ViewFullDetails_TotalGrossBookingValue = driver.findElement(By.xpath(".//*[@id='pkg_totalrate']")).getText();
					activityDetails.setQuote_ViewFullDetails_TotalGrossBookingValue(Quote_ViewFullDetails_TotalGrossBookingValue);
					String Quote_ViewFullDetails_TotalTax = driver.findElement(By.xpath(".//*[@id='tottaxamountdivid']")).getText();
					activityDetails.setQuote_ViewFullDetails_TotalTax(Quote_ViewFullDetails_TotalTax);
					String Quote_ViewFullDetails_TotalPackageValue = driver.findElement(By.xpath(".//*[@id='totchargeamt']")).getText();
					activityDetails.setQuote_ViewFullDetails_TotalPackageValue(Quote_ViewFullDetails_TotalPackageValue);
					String Quote_ViewFullDetails_AmountProcessed = driver.findElement(By.xpath(".//*[@id='payNwpaymentsection']")).getText();
					activityDetails.setQuote_ViewFullDetails_AmountProcessed(Quote_ViewFullDetails_AmountProcessed);
					
					if (UserTypeDC_B2B == TourOperatorType.COMCASH || UserTypeDC_B2B == TourOperatorType.COMCREDITLPONO || UserTypeDC_B2B == TourOperatorType.COMCREDITLPOY) {
				        
						String ViewFullDetails_currency2 = driver.findElement(By.xpath(".//*[@id='total_transfer_info_table']/tbody/tr[2]/td[2]")).getText().substring(6,9);
						activityDetails.setQuote_ViewFullDetails_Currency2(ViewFullDetails_currency2);
									
						String ViewFullDetails_AmountCheckIn = driver.findElement(By.xpath(".//*[@id='total_transfer_info_table']/tbody/tr[7]/td[2]")).getText();
						activityDetails.setQuote_ViewFullDetails_AmountCheckIn(ViewFullDetails_AmountCheckIn);
						
						String ViewFullDetails_AgentComm = driver.findElement(By.xpath(".//*[@id='pkg_agentcommission']/span")).getText();
						activityDetails.setQuote_ViewFullDetails_AgentCommisiion(ViewFullDetails_AgentComm);
							
					}else{
						
						String ViewFullDetails_currency2 = driver.findElement(By.xpath(".//*[@id='total_transfer_info_table']/tbody/tr[1]/td[2]")).getText().substring(6,9);
						activityDetails.setQuote_ViewFullDetails_Currency2(ViewFullDetails_currency2);
									
						String ViewFullDetails_AmountCheckIn = driver.findElement(By.xpath(".//*[@id='total_transfer_info_table']/tbody/tr[6]/td[2]")).getText();
						activityDetails.setQuote_ViewFullDetails_AmountCheckIn(ViewFullDetails_AmountCheckIn);
					}
					
					
					driver.switchTo().defaultContent();
					driver.switchTo().frame("live_message_frame");	
					
					Thread.sleep(1000);
					try {
						driver.findElement(By.xpath("html/body/div[9]/div[1]/a/span")).click();
					} catch (Exception e3) {
						driver.findElement(By.xpath("html/body/div[10]/div[1]/a/span")).click();
					}
					Thread.sleep(2000);
					
					Thread.sleep(1500);
					//driver.findElement(By.id("loadpayment")).click();	
					JavascriptExecutor js4 = (JavascriptExecutor) driver;
					js4.executeScript("JavaScript:loadPaymentPage();");
					wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='activate_mg']/div")));
					
					
				}
				
				//Discount coupon
				
				if (inventoryList.getDiscountYesORNo().equalsIgnoreCase("Yes")) {
					
					driver.findElement(By.xpath(".//*[@id='discount_coupon_user_id']")).sendKeys(PG_Properties.getProperty("DiscountCouponNumber"));
					driver.findElement(By.xpath(".//*[@id='discountcoupon_validate']/div")).click();
					Thread.sleep(2000);
					wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("html/body/div[2]/div[3]/div/button")));
					driver.findElement(By.xpath("html/body/div[2]/div[3]/div/button")).click();
					
					
				}
				
				
				List<WebElement> rowLists = driver.findElements(By.className("amo_width_20"));
				
				activityDetails.setPaymenPagecusCount(rowLists.size()-3);
				
				for (int i = 0; i < rowLists.size()-3; i++) {
					
					Select selectItemq = new org.openqa.selenium.support.ui.Select(driver.findElement(By.xpath(".//*[@id='prog_title_0_0_"+i+"']")));
					WebElement eleProgTitle = selectItemq.getFirstSelectedOption();	
					String titleProg = eleProgTitle.getText();
					cusTitle.add(titleProg);
					
					StringBuffer programFname = new StringBuffer();
					programFname.append("FirstName-"+(i+1)+"");	
					cusFName.add(programFname.toString());
					driver.findElement(By.xpath(".//*[@id='prog_fname_0_0_"+i+"']")).clear();
					driver.findElement(By.xpath(".//*[@id='prog_fname_0_0_"+i+"']")).sendKeys(programFname);
							
					StringBuffer programLname = new StringBuffer();
					programLname.append("LastName-"+(i+1)+"");
					cusLname.add(programLname.toString());
					driver.findElement(By.xpath(".//*[@id='prog_lname_0_0_"+i+"']")).clear();
					driver.findElement(By.xpath(".//*[@id='prog_lname_0_0_"+i+"']")).sendKeys(programLname);
										
				}
				
				activityDetails.setResultsPage_cusTitle(cusTitle);
				activityDetails.setResultsPage_cusFName(cusFName);
				activityDetails.setResultsPage_cusLName(cusLname);
				
				try {	
					
					WebElement pickUplement = driver.findElement(By.xpath(".//*[@id='pickupdropoffdetailsnotadd"+idOnly+"']/div[3]/be/be"));
					String pickUpDropOffID = pickUplement.findElement(By.tagName("table")).getAttribute("id").split("Tb-")[1];
										
					//DropOff details
					driver.findElement(By.name("pickuplocationStationPortNameTxt-"+pickUpDropOffID+"")).sendKeys("Colombo");
					driver.findElement(By.xpath(".//*[@id='pickupaddionalinfo-"+pickUpDropOffID+"']")).sendKeys("PickUp Additional Info");
					driver.findElement(By.name("dropofflocationNameStationPortTxt-"+pickUpDropOffID+"")).sendKeys("Hambanthota");
					driver.findElement(By.xpath(".//*[@id='dropoffaddionalinfo-"+pickUpDropOffID+"']")).sendKeys("DropOff Additional Info");
										
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				//Actiity info
				try {
					driver.findElement(By.xpath(".//*[@id='_pickupPoint']")).sendKeys("hotel california");
					Thread.sleep(1000);
				} catch (Exception e2) {
					e2.printStackTrace();
				}
				
				String cusNotes = "You wrongly invoiced and shipped Product A when the customer actually ordered Product B which may or may not be at a different price. To rectify this, you would " +
						"then ship Product B together with a Credit Note for Product A and another invoice for product B. " +
						"This will restore the inventory and Accounts Receivable in your books while billing the customer for the correct item and amount. " +
						"Meanwhile the customer returns the incorrect Product A";
				
				Thread.sleep(500);
				driver.findElement(By.xpath(".//*[@id='txt_cus_notes']")).sendKeys(cusNotes);
				activityDetails.setCustomerNotes(cusNotes);
				
				String internalNotes = "Internal then ship Product B together with a Credit Note for Product.";
				Thread.sleep(500);
				driver.findElement(By.xpath(".//*[@id='intl_notes']")).sendKeys(internalNotes);
				activityDetails.setInternalNotes(internalNotes);
				
				String programNotes = "This will restore the inventory and Accounts Receivable in your books while billing the customer";
				Thread.sleep(500);
				driver.findElement(By.xpath(".//*[@id='act_notes']")).sendKeys(programNotes);
				activityDetails.setProgramNotes(programNotes);
				
				String paymntDetail = inventory.getPaymentDetails();
				activityDetails.setPaymentDetails(paymntDetail);
				
				
				
				Select titleCustomer = new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("cusTitle")));
				WebElement elePaymentPage_title = titleCustomer.getFirstSelectedOption();	
				activityDetails.setPaymentPage_Title(elePaymentPage_title.getText());
				
				String paymentPage_FName =	driver.findElement(By.id("cusFName")).getAttribute("value");
				activityDetails.setPaymentPage_FName(paymentPage_FName);
				String paymentPage_LName = driver.findElement(By.id("cusLName")).getAttribute("value");
				activityDetails.setPaymentPage_LName(paymentPage_LName);
				
				String paymentPage_TP_1 = driver.findElement(By.id("cusareacodetext")).getAttribute("value");
				String paymentPage_TP_2 = driver.findElement(By.id("cusPhonetext")).getAttribute("value");		
				String tpp = paymentPage_TP_1 + "-" + paymentPage_TP_2;
				activityDetails.setPaymentPage_TP(tpp);
				
				String paymentPage_Email = driver.findElement(By.id("cusEmail")).getAttribute("value");
				activityDetails.setPaymentPage_Email(paymentPage_Email);
				String paymentPage_address = driver.findElement(By.id("cusAdd1")).getAttribute("value");
				activityDetails.setPaymentPage_address(paymentPage_address);
				String paymentPage_address_2 = driver.findElement(By.id("cusAdd2")).getAttribute("value");
				activityDetails.setPaymentPage_address_2(paymentPage_address_2);
				
				Select paymentPage_country = new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("cusCountry")));
				WebElement elePaymentPage_country = paymentPage_country.getFirstSelectedOption();
				String cCountrys = elePaymentPage_country.getText();
				activityDetails.setPaymentPage_country(cCountrys);
				
				String paymentPage_city = driver.findElement(By.id("cusCity")).getAttribute("value");
				activityDetails.setPaymentPage_city(paymentPage_city);
				
				if (cCountry.equals("USA")) {
					
					activityDetails.setPaymentPage_state(paymentDetails.getState());
					activityDetails.setPaymentPage_PostelCode("11300");	
				}
				
				if (cCountry.equals("Canada")) {
					
					activityDetails.setPaymentPage_state(paymentDetails.getState());
					activityDetails.setPaymentPage_PostelCode("11300");	
				}
				
				if (cCountry.equals("Australia")) {
					
					activityDetails.setPaymentPage_state(paymentDetails.getState());
					activityDetails.setPaymentPage_PostelCode("11300");	
				}
				
				
				
				
			////////
				
				WebElement paymentCancel = driver.findElement(By.id("cxlpolicy_a_0"));
				List<WebElement> paymentCancelWebelement = paymentCancel.findElements(By.tagName("li"));
				
				ArrayList<String> paymentCancelPolicy = new ArrayList<String>();
				
				for (int j = 1; j <= paymentCancelWebelement.size(); j++) {
					
					String paymentpolicy = driver.findElement(By.xpath(".//*[@id='cxlpolicy_a_0']/ul/li["+j+"]")).getText();
					paymentCancelPolicy.add(paymentpolicy);
					
				}
				
				System.out.println(paymentCancelPolicy);
				activityDetails.setPaymentCancelPolicy(paymentCancelPolicy);
				
				
				//Active manager overwrite
				
				if (inventoryList.getActiveManagerOverwrite().equalsIgnoreCase("Yes")) {
					
					driver.findElement(By.id("user_id")).sendKeys(PG_Properties.getProperty("DC_UserName"));
					driver.findElement(By.id("user_password")).sendKeys(PG_Properties.getProperty("DC_Password"));
					driver.findElement(By.xpath(".//*[@id='activate_mg']/div")).click();
					Thread.sleep(4000);
					wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='activity_info_table']/tbody/tr[8]/td[2]/input")));
					
					String originalRate = driver.findElement(By.xpath(".//*[@id='activity_info_table']/tbody/tr[8]/td[2]/input")).getAttribute("value");
					String changeTo = "555";
					
					((JavascriptExecutor)driver).executeScript("document.getElementsByName('sellRate')[0].setAttribute('value','"+changeTo+"');");
					Thread.sleep(1000);
										
					WebElement sellRateElement = driver.findElement(By.xpath(".//*[@id='activity_info_table']/tbody/tr[8]/td[2]/input"));
					String onchangeText = sellRateElement.getAttribute("onchange");
					
					JavascriptExecutor jsd = (JavascriptExecutor) driver;
					/*jsd.executeScript("$(arguments[0]).change();", sellRateElement);*/
					
					//driver.findElement(By.id("recalculaterates")).click();
	
					jsd.executeScript("recalculateRates();");
					wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='activity_info_table']/tbody/tr[8]/td[2]/input")));
					
					activityDetails.setOriginalRate(changeTo);
					
					String activiteMR_Rate = driver.findElement(By.xpath(".//*[@id='activity_info_table']/tbody/tr[4]/td[4]")).getText();
					activityDetails.setActiviteMR_Rate(activiteMR_Rate);
					
					String activiteMR_SubTotal = driver.findElement(By.xpath(".//*[@id='activity_info_table']/tbody/tr[4]/td[6]")).getText();
					activityDetails.setActiviteMR_SubTotal(activiteMR_SubTotal);
					
//					String activiteMR_AgentCommission = driver.findElement(By.xpath(".//*[@id='activity_info_table']/tbody/tr[9]/td[2]/input")).getText();
//					activityDetails.setActiviteMR_AgentCommission(activiteMR_AgentCommission);
					
									
					String activiteMR_TotalTax = driver.findElement(By.xpath(".//*[@id='activity_info_table']/tbody/tr[11]/td[2]")).getText();
					activityDetails.setActiviteMR_TotalTax(activiteMR_TotalTax);
					
					String activiteMR_SubTotal2 = driver.findElement(By.xpath(".//*[@id='activity_info_table']/tbody/tr[12]/td[2]")).getText();
					activityDetails.setActiviteMR_SubTotal2(activiteMR_SubTotal2);
					
					String activiteMR_TotalbookValue = driver.findElement(By.xpath(".//*[@id='activity_info_table']/tbody/tr[13]/td[2]")).getText();
					activityDetails.setActiviteMR_TotalbookValue(activiteMR_TotalbookValue);
								
					String activiteMR_SellRate = driver.findElement(By.xpath(".//*[@id='activity_info_table']/tbody/tr[8]/td[2]/input")).getAttribute("value");
					activityDetails.setActiviteMR_SellRate(activiteMR_SellRate);
					
					////
					
					String activiteMR_TotalGrossPkgValue = driver.findElement(By.xpath(".//*[@id='pkg_totalrate']")).getText();
					activityDetails.setActiviteMR_TotalGrossPkgValue(activiteMR_TotalGrossPkgValue);
					
					String activiteMR_TotalTaxOtherCharges = driver.findElement(By.xpath(".//*[@id='tottaxamountdivid']")).getText();
					activityDetails.setActiviteMR_TotalTaxOtherCharges(activiteMR_TotalTaxOtherCharges);
					
					String activiteMR_TotalBookingValue = driver.findElement(By.xpath(".//*[@id='totchargeamt']")).getText();
					activityDetails.setActiviteMR_TotalBookingValue(activiteMR_TotalBookingValue);
					
					String activiteMR_AmountNow = driver.findElement(By.xpath(".//*[@id='payNwpaymentsection']")).getText();
					activityDetails.setActiviteMR_AmountNow(activiteMR_AmountNow);
					
					String activiteMR_DueAtCheckIn = driver.findElement(By.xpath(".//*[@id='total_transfer_info_table']/tbody/tr[6]/td[2]")).getText();
					activityDetails.setActiviteMR_DueAtCheckIn(activiteMR_DueAtCheckIn);
					
					//back to original value
					
					driver.findElement(By.xpath(".//*[@id='activity_info_table']/tbody/tr[8]/td[2]/input")).clear();
					driver.findElement(By.xpath(".//*[@id='activity_info_table']/tbody/tr[8]/td[2]/input")).sendKeys(originalRate);
					Thread.sleep(1000);
					driver.findElement(By.xpath(".//*[@id='recalculaterates']/div")).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='activity_info_table']/tbody/tr[8]/td[2]/input")));
					
					
				}
			
				
				
				
				
				
				
				
				////////
				
				if (inventory.getPaymentDetails().equals("Pay Offline")) {               
					
					driver.findElement(By.xpath(".//*[@id='pay_offline']")).click();
					Thread.sleep(6000);
					
					String paymentPage_subTotal = driver.findElement(By.xpath(".//*[@id='prog_subtotal_0']")).getText();
					activityDetails.setPaymentPage_subTotal(paymentPage_subTotal);
					String paymentPage_TotalTax = driver.findElement(By.xpath(".//*[@id='prog_tax_0']")).getText();
					activityDetails.setPaymentPage_TotalTax(paymentPage_TotalTax);
					String paymentPage_TotalActivityBookingValue = driver.findElement(By.xpath(".//*[@id='prog_totalbookval_0']")).getText(); 
					activityDetails.setPaymentPage_TotalActivityBookingValue(paymentPage_TotalActivityBookingValue);
					
					String grossTotal = driver.findElement(By.xpath(".//*[@id='pkg_totalrate']")).getText();
					activityDetails.setPaymentPageGrossBookingValue(grossTotal);			
					String taxValue = driver.findElement(By.xpath(".//*[@id='tottaxamountdivid']")).getText();
					activityDetails.setPaymentPageTax(taxValue);			
					String totalbookingValue = driver.findElement(By.xpath(".//*[@id='totchargeamt']")).getText();
					activityDetails.setPaymentPageTotalBookingValue(totalbookingValue);
					String paymentPageAmountbeingProcessed = driver.findElement(By.xpath(".//*[@id='payNwpaymentsection']")).getText();
					activityDetails.setPaymentPageAmountbeingProcessed(paymentPageAmountbeingProcessed);
					
					if (UserTypeDC_B2B == TourOperatorType.COMCASH || UserTypeDC_B2B == TourOperatorType.COMCREDITLPONO || UserTypeDC_B2B == TourOperatorType.COMCREDITLPOY) {
						
						String paymentPageAgentComm = driver.findElement(By.xpath(".//*[@id='pkg_agentcommission']/span")).getText();
						activityDetails.setPaymentsPage_AgentCommission(paymentPageAgentComm);
						
						if (inventoryList.getDiscountYesORNo().equalsIgnoreCase("Yes")) {
							
							String paymentPageAmountduatCheckIn = driver.findElement(By.xpath(".//*[@id='total_transfer_info_table']/tbody/tr[8]/td[2]")).getText();
							activityDetails.setPaymentPageAmountduatCheckIn(paymentPageAmountduatCheckIn);
							
						}else{
							
							String paymentPageAmountduatCheckIn = driver.findElement(By.xpath(".//*[@id='total_transfer_info_table']/tbody/tr[7]/td[2]")).getText();
							activityDetails.setPaymentPageAmountduatCheckIn(paymentPageAmountduatCheckIn);
							
						}
																	
						
					}else{
						
						if (inventoryList.getDiscountYesORNo().equalsIgnoreCase("Yes")) {
							String paymentPageAmountduatCheckIn = driver.findElement(By.xpath(".//*[@id='total_transfer_info_table']/tbody/tr[7]/td[2]")).getText();
							activityDetails.setPaymentPageAmountduatCheckIn(paymentPageAmountduatCheckIn);		
							
						}else{							
							String paymentPageAmountduatCheckIn = driver.findElement(By.xpath(".//*[@id='total_transfer_info_table']/tbody/tr[6]/td[2]")).getText();
							activityDetails.setPaymentPageAmountduatCheckIn(paymentPageAmountduatCheckIn);									
						}
	
					}
					
					if (inventoryList.getDiscountYesORNo().equalsIgnoreCase("Yes")) {
						
						String PaymentsPage_DiscountValue1 = driver.findElement(By.xpath(".//*[@id='activity_info_table']/tbody/tr[8]/td[2]")).getText();
						activityDetails.setPaymentsPage_DiscountValue1(PaymentsPage_DiscountValue1);
						
					}
					
					if (inventoryList.getDiscountYesORNo().equalsIgnoreCase("Yes")) {
						
						String PaymentsPage_DiscountValue2 = driver.findElement(By.xpath(".//*[@id='totdiscountamt']")).getText();
						activityDetails.setPaymentsPage_DiscountValue2(PaymentsPage_DiscountValue2);
						
					}
					
					///
					
					if (UserTypeDC_B2B == TourOperatorType.DC || UserTypeDC_B2B == TourOperatorType.COMCASH || UserTypeDC_B2B == TourOperatorType.COMCREDITLPONO || UserTypeDC_B2B == TourOperatorType.COMCREDITLPOY) {
						
						try {							
							new Select(driver.findElement(By.id("payment_method_1"))).selectByVisibleText("Cash");
							Thread.sleep(3000);
							driver.findElement(By.xpath(".//*[@id='payment_reference']")).sendKeys("123456789");
							Thread.sleep(2000);
							activityDetails.setPaymentType_bookingList("Cash");
							activityDetails.setPaymentReference_bookingList("123456789");
							
						} catch (Exception e) {
							activityDetails.setPaymentType_bookingList("NotAvailable");
							activityDetails.setPaymentReference_bookingList("NotAvailable");				
						}
						
					}else{
						activityDetails.setPaymentType_bookingList("NotAvailable");
						activityDetails.setPaymentReference_bookingList("NotAvailable");							
					}
						
					
					driver.findElement(By.xpath(".//*[@id='cancellation']")).click();	
					
					//Send mail options
					driver.findElement(By.xpath(".//*[@id='customer_mail_send_yes']")).click();	
					driver.findElement(By.xpath(".//*[@id='voucher_mail_send_yes']")).click();	
					try {
						driver.findElement(By.xpath(".//*[@id='supplier_mail_send_yes']")).click();
					} catch (Exception e1) {
						e1.printStackTrace();
					}	
					
					driver.findElement(By.xpath(".//*[@id='continue_submit']")).click();
					Thread.sleep(10000);
					wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='confirmbooking']/div")));	
					
					driver.findElement(By.xpath(".//*[@id='confirmbooking']/div")).click();	
					
					availablClick = (System.currentTimeMillis() / 1000);			
					activityDetails.setTimeAailableClick(availablClick);	
				
				}
				
			////////
				
				if (inventory.getPaymentDetails().equals("Pay Online")) {
					
					driver.findElement(By.xpath(".//*[@id='pay_online']")).click();
					
					/*Thread.sleep(1000);
					new Select(driver.findElement(By.id("credit_card_type"))).selectByVisibleText("Master");
					Thread.sleep(1000);*/
					
					String paymentPage_subTotal = driver.findElement(By.xpath(".//*[@id='prog_subtotal_0']")).getText();
					activityDetails.setPaymentPage_subTotal(paymentPage_subTotal);
					String paymentPage_TotalTax = driver.findElement(By.xpath(".//*[@id='prog_tax_0']")).getText();
					activityDetails.setPaymentPage_TotalTax(paymentPage_TotalTax);
					String paymentPage_TotalActivityBookingValue = driver.findElement(By.xpath(".//*[@id='prog_totalbookval_0']")).getText(); 
					activityDetails.setPaymentPage_TotalActivityBookingValue(paymentPage_TotalActivityBookingValue);
					
					String grossTotal = driver.findElement(By.xpath(".//*[@id='pkg_totalrate']")).getText();
					activityDetails.setPaymentPageGrossBookingValue(grossTotal);			
					String taxValue = driver.findElement(By.xpath(".//*[@id='tottaxamountdivid']")).getText();
					activityDetails.setPaymentPageTax(taxValue);			
					String totalbookingValue = driver.findElement(By.xpath(".//*[@id='totchargeamt']")).getText();
					activityDetails.setPaymentPageTotalBookingValue(totalbookingValue);
					String paymentPageAmountbeingProcessed = driver.findElement(By.xpath(".//*[@id='payNwpaymentsection']")).getText();
					activityDetails.setPaymentPageAmountbeingProcessed(paymentPageAmountbeingProcessed);
										
					if (UserTypeDC_B2B == TourOperatorType.COMCASH || UserTypeDC_B2B == TourOperatorType.COMCREDITLPONO || UserTypeDC_B2B == TourOperatorType.COMCREDITLPOY) {
						
						String paymentPageAgentComm = driver.findElement(By.xpath(".//*[@id='pkg_agentcommission']/span")).getText();
						activityDetails.setPaymentsPage_AgentCommission(paymentPageAgentComm);
						
						if (inventoryList.getDiscountYesORNo().equalsIgnoreCase("Yes")) {
							String paymentPageAmountduatCheckIn = driver.findElement(By.xpath(".//*[@id='total_transfer_info_table']/tbody/tr[8]/td[2]")).getText();
							activityDetails.setPaymentPageAmountduatCheckIn(paymentPageAmountduatCheckIn);
							
						}else{
							String paymentPageAmountduatCheckIn = driver.findElement(By.xpath(".//*[@id='total_transfer_info_table']/tbody/tr[7]/td[2]")).getText();
							activityDetails.setPaymentPageAmountduatCheckIn(paymentPageAmountduatCheckIn);
							
						}
																	
						
					}else{
						
						if (inventoryList.getDiscountYesORNo().equalsIgnoreCase("Yes")) {
							String paymentPageAmountduatCheckIn = driver.findElement(By.xpath(".//*[@id='total_transfer_info_table']/tbody/tr[7]/td[2]")).getText();
							activityDetails.setPaymentPageAmountduatCheckIn(paymentPageAmountduatCheckIn);	
							
						}else{
							String paymentPageAmountduatCheckIn = driver.findElement(By.xpath(".//*[@id='total_transfer_info_table']/tbody/tr[6]/td[2]")).getText();
							activityDetails.setPaymentPageAmountduatCheckIn(paymentPageAmountduatCheckIn);	
							
						}
						
					}
					
					if (inventoryList.getDiscountYesORNo().equalsIgnoreCase("Yes")) {
						
						String PaymentsPage_DiscountValue1 = driver.findElement(By.xpath(".//*[@id='activity_info_table']/tbody/tr[8]/td[2]")).getText();
						activityDetails.setPaymentsPage_DiscountValue1(PaymentsPage_DiscountValue1);
						
					}
					
					if (inventoryList.getDiscountYesORNo().equalsIgnoreCase("Yes")) {
						
						String PaymentsPage_DiscountValue2 = driver.findElement(By.xpath(".//*[@id='totdiscountamt']")).getText();
						activityDetails.setPaymentsPage_DiscountValue2(PaymentsPage_DiscountValue2);
						
					}
					
											
					String payOnline_MainTotalBookingValue = driver.findElement(By.xpath(".//*[@id='totchgpgamt']")).getText();
					activityDetails.setPayOnline_MainTotalBookingValue(payOnline_MainTotalBookingValue);
					String payOnline_MainTotalBookingCurrency = driver.findElement(By.xpath(".//*[@id='totchgpgcurrency']")).getText().substring(1, 4);
					activityDetails.setPayOnline_MainTotalBookingCurrency(payOnline_MainTotalBookingCurrency);
					String payOnlineGross = driver.findElement(By.xpath(".//*[@id='pkg_depositval']")).getText();
					activityDetails.setPayOnline_GrossValue(payOnlineGross);
					String payOnlineTax = driver.findElement(By.xpath(".//*[@id='totaltaxpgcurrid']")).getText();
					activityDetails.setPayOnline_TotalTax(payOnlineTax);
					String payOnlineTotal = driver.findElement(By.xpath(".//*[@id='totalchargeamtpgcurrid']")).getText(); 	
					activityDetails.setPayOnline_TotalBooking(payOnlineTotal);
					String payOnline_AmountProNow = driver.findElement(By.xpath(".//*[@id='totalpaynowamountpgcurrid']")).getText();
					activityDetails.setPayOnline_AmountProNow(payOnline_AmountProNow);
					String payOnline_Due = driver.findElement(By.xpath(".//*[@id='totalamountatutilizationpgcurrid']")).getText();
					activityDetails.setPayOnline_Due(payOnline_Due);
					
					activityDetails.setPaymentType_bookingList("Credit Card");
										
					driver.findElement(By.xpath(".//*[@id='cancellation']")).click();	
					
					//Send mail options
					driver.findElement(By.xpath(".//*[@id='customer_mail_send_yes']")).click();	
					driver.findElement(By.xpath(".//*[@id='voucher_mail_send_yes']")).click();	
					try {
						driver.findElement(By.xpath(".//*[@id='supplier_mail_send_yes']")).click();
					} catch (Exception e1) {
						e1.printStackTrace();
					}	
					Thread.sleep(1000);
					
					
					driver.findElement(By.xpath(".//*[@id='continue_submit']")).click();		
					Thread.sleep(7000);
					//wait.until(ExpectedConditions.presenceOfElementLocated(By.id("confirmbooking")));	
					
					driver.findElement(By.xpath(".//*[@id='confirmbooking']/div")).click();	
					Thread.sleep(3000);
					
					//payment Gateway
					
					Thread.sleep(20000);
					driver.switchTo().frame("paygatewayFrame");
					wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='first_name']")));
					File scrFile_58 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			        FileUtils.copyFile(scrFile_58, new File(""+currentDateforImages+"/"+search_Info.getScenarioCount()+"_PaymentGateway.png"));
					
					Thread.sleep(2000);
					driver.findElement(By.xpath(".//*[@id='first_name']")).sendKeys("Asiri");
					driver.findElement(By.xpath(".//*[@id='last_name']")).sendKeys("Fodo");
					
					driver.findElement(By.xpath(".//*[@id='account_number']")).sendKeys("4000000000000002");
					driver.findElement(By.xpath(".//*[@id='card_security_code']")).sendKeys("111");
					
					new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("expiration_month_list"))).selectByVisibleText("01");
					new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("expiration_year_list"))).selectByVisibleText("2017");
							
					/*JavascriptExecutor jse = (JavascriptExecutor) driver;
					jse.executeScript("document.getElementById('cardnumberpart1').value = '4000';");
					jse.executeScript("document.getElementById('cardnumberpart2').value = '0000';");
					jse.executeScript("document.getElementById('cardnumberpart3').value = '0000';");
					jse.executeScript("document.getElementById('cardnumberpart4').value = '0002';");*/
					//jse.executeScript("document.getElementById('cv2').value = '625';");
					
					String amount;
										
					try {
						amount = driver.findElement(By.xpath(".//*[@id='hpp-amount']")).getText().split(" ")[1];
					} catch (Exception e) {
						amount = "0";
					}
					
					activityDetails.setPaymentGatewayTotalAmount(amount);
					
					try {
						driver.findElement(By.xpath(".//*[@id='hpp-form-submit']")).click();	
						Thread.sleep(8000);
					} catch (Exception e) {
						//driver.findElement(By.xpath(".//*[@id='main_dev']/div[2]/div[3]/a/div")).click();	
						Thread.sleep(8000);
					}
					
							
					availablClick = (System.currentTimeMillis() / 1000);			
					activityDetails.setTimeAailableClick(availablClick);
										
					try {	
						
						//wait.until(ExpectedConditions.presenceOfElementLocated(By.name("external.field.password")));
						driver.findElement(By.xpath("html/body/table/tbody/tr/td/table/tbody/tr[4]/td/table/tbody/tr[6]/td/form/div/table/tbody/tr/td/font/b/center/table/tbody/tr[2]/td[2]/b/input[1]")).sendKeys("1234");
						driver.findElement(By.xpath("html/body/table/tbody/tr/td/table/tbody/tr[4]/td/table/tbody/tr[6]/td/form/div/table/tbody/tr/td/font/b/center/table/tbody/tr[4]/td[2]/b/input")).click();	
						Thread.sleep(5000);
						
					} catch (Exception e) {
						e.printStackTrace();
					}
					
				}
				
				
				
				//Confirm page
				
				driver.switchTo().defaultContent();
				driver.switchTo().frame("live_message_frame");
				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("img_print_voucher")));	
				String bookingNo = driver.findElement(By.xpath(".//*[@id='p_car_booking_reservation_no_1']")).getText().split(":    ")[1];
				comfirmBooking = (System.currentTimeMillis() / 1000);			
				activityDetails.setTimeConfirmBooking(comfirmBooking);
				activityDetails.setReservationNo(bookingNo);
				
				File scrFile_56 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		        FileUtils.copyFile(scrFile_56, new File(""+currentDateforImages+"/"+search_Info.getScenarioCount()+"_ConfirmationPage.png"));
						
				String bookingStatuss = driver.findElement(By.xpath(".//*[@id='prog_bookstatus_0']")).getText();
				activityDetails.setBookingStatus(bookingStatuss);
				
				String confirmationPage_Currency = driver.findElement(By.xpath(".//*[@id='main_content']/div/div/div/div[1]/table/tbody/tr/td/table/tbody/tr[4]/td[6]")).getText().substring(7,10);
				activityDetails.setConfirmationPage_Currency(confirmationPage_Currency);
				
				String bookingQTY = driver.findElement(By.xpath(".//*[@id='activity_qty_0_0']")).getText();
				activityDetails.setConfirmationPage_QTY(bookingQTY);
				
				String paymentActivityName = driver.findElement(By.xpath(".//*[@id='progname_0_0']")).getText().split("- ")[1];
				activityDetails.setBookingPageActivityName(paymentActivityName);
				
				String grossvalue = driver.findElement(By.xpath(".//*[@id='activity_subtotal']")).getText();
				activityDetails.setConfirmationPage_grossValue(grossvalue);
				String tax = driver.findElement(By.xpath(".//*[@id='activity_tax']")).getText();
				activityDetails.setConfirmationPage_tax(tax);
				String totalBookingValue = driver.findElement(By.xpath(".//*[@id='activity_bookingvalue']")).getText();
				activityDetails.setConfirmationPage_activityBookingValue(totalBookingValue);
				
				String confirmationPage_TotalGrossValue = driver.findElement(By.xpath(".//*[@id='pkg_totalbeforetax']")).getText();
				activityDetails.setConfirmationPage_TotalGrossValue(confirmationPage_TotalGrossValue);
				String confirmationPage_TotalTax = driver.findElement(By.xpath(".//*[@id='pkg_totaltaxandothercharges']")).getText();
				activityDetails.setConfirmationPage_TotalTax(confirmationPage_TotalTax);
				String confirmationPage_TotalPackageBookingValue = driver.findElement(By.xpath(".//*[@id='pkg_totalpayable']")).getText();
				activityDetails.setConfirmationPage_TotalPackageBookingValue(confirmationPage_TotalPackageBookingValue);
				
				if (UserTypeDC_B2B == TourOperatorType.COMCASH || UserTypeDC_B2B == TourOperatorType.COMCREDITLPONO || UserTypeDC_B2B == TourOperatorType.COMCREDITLPOY) {
													
					String confirmationPage_AgentCommi = driver.findElement(By.xpath(".//*[@id='pkg_agentcommission']")).getText();
					activityDetails.setConfirmationPage_AgentCommission(confirmationPage_AgentCommi);
					
					if (inventoryList.getDiscountYesORNo().equalsIgnoreCase("Yes")) {
						String confirmationPage_AmountBeingProcessed = driver.findElement(By.xpath(".//*[@id='main_content']/div/div/div/table[2]/tbody/tr/td/table/tbody/tr[6]/td[2]")).getText();
						activityDetails.setConfirmationPage_AmountBeingProcessed(confirmationPage_AmountBeingProcessed);
						String confirmationPage_AmountDueAtCheckin = driver.findElement(By.xpath(".//*[@id='main_content']/div/div/div/table[2]/tbody/tr/td/table/tbody/tr[7]/td[2]")).getText();
						activityDetails.setConfirmationPage_AmountDueAtCheckin(confirmationPage_AmountDueAtCheckin);
						
					}else{
						String confirmationPage_AmountBeingProcessed = driver.findElement(By.xpath(".//*[@id='main_content']/div/div/div/table[2]/tbody/tr/td/table/tbody/tr[5]/td[2]")).getText();
						activityDetails.setConfirmationPage_AmountBeingProcessed(confirmationPage_AmountBeingProcessed);
						String confirmationPage_AmountDueAtCheckin = driver.findElement(By.xpath(".//*[@id='main_content']/div/div/div/table[2]/tbody/tr/td/table/tbody/tr[6]/td[2]")).getText();
						activityDetails.setConfirmationPage_AmountDueAtCheckin(confirmationPage_AmountDueAtCheckin);
						
					}
				
				}else{
					
					if (inventoryList.getDiscountYesORNo().equalsIgnoreCase("Yes")) {
						String confirmationPage_AmountBeingProcessed = driver.findElement(By.xpath(".//*[@id='main_content']/div/div/div/table[2]/tbody/tr/td/table/tbody/tr[5]/td[2]")).getText();
						activityDetails.setConfirmationPage_AmountBeingProcessed(confirmationPage_AmountBeingProcessed);
						String confirmationPage_AmountDueAtCheckin = driver.findElement(By.xpath(".//*[@id='main_content']/div/div/div/table[2]/tbody/tr/td/table/tbody/tr[6]/td[2]")).getText();
						activityDetails.setConfirmationPage_AmountDueAtCheckin(confirmationPage_AmountDueAtCheckin);
					}else{
						String confirmationPage_AmountBeingProcessed = driver.findElement(By.xpath(".//*[@id='main_content']/div/div/div/table[2]/tbody/tr/td/table/tbody/tr[4]/td[2]")).getText();
						activityDetails.setConfirmationPage_AmountBeingProcessed(confirmationPage_AmountBeingProcessed);
						String confirmationPage_AmountDueAtCheckin = driver.findElement(By.xpath(".//*[@id='main_content']/div/div/div/table[2]/tbody/tr/td/table/tbody/tr[5]/td[2]")).getText();
						activityDetails.setConfirmationPage_AmountDueAtCheckin(confirmationPage_AmountDueAtCheckin);
					}
				}
				
				if (inventoryList.getDiscountYesORNo().equalsIgnoreCase("Yes")) {
					
					String confirmationPage_discount1 = driver.findElement(By.xpath(".//*[@id='pkg_totaldiscountcouponvalue']")).getText();
					activityDetails.setConfirmationPage_DiscountValue1(confirmationPage_discount1);
					
				}
				
						
				String confirmationPage_Title =	driver.findElement(By.xpath(".//*[@id='cusfirstname']")).getText();
				activityDetails.setConfirmationPage_Title(confirmationPage_Title);		
				String confirmationPage_FName =	driver.findElement(By.xpath(".//*[@id='cusfirstname']")).getText();
				activityDetails.setConfirmationPage_FName(confirmationPage_FName);
				String confirmationPage_LName = driver.findElement(By.xpath(".//*[@id='cuslastname']")).getText();
				activityDetails.setConfirmationPage_LName(confirmationPage_LName);
				String confirmationPage_TP = driver.findElement(By.xpath(".//*[@id='custelephone']")).getText();
				activityDetails.setConfirmationPage_TP(confirmationPage_TP);
				String confirmationPage_Email = driver.findElement(By.xpath(".//*[@id='cusemail1']")).getText();
				activityDetails.setConfirmationPage_Email(confirmationPage_Email);
				String confirmationPage_address = driver.findElement(By.xpath(".//*[@id='cusaddress1']")).getText();
				activityDetails.setConfirmationPage_address(confirmationPage_address);
				String confirmationPage_address2 = driver.findElement(By.xpath(".//*[@id='cusaddress2']")).getText();
				activityDetails.setConfirmationPage_address_2(confirmationPage_address2);
				String confirmationPage_country = driver.findElement(By.xpath(".//*[@id='cuscountry']")).getText();
				activityDetails.setConfirmationPage_country(confirmationPage_country);
				String confirmationPage_city = driver.findElement(By.xpath(".//*[@id='cuscity']")).getText();
				activityDetails.setConfirmationPage_city(confirmationPage_city);
				
				if (cCountry.equals("USA")||cCountry.equals("Canada")||cCountry.equals("Australia")) {
					
					String confirmationPage_State       = "Not Available";
					String confirmationPage_PostalCode  = "Not Available";
					
					try {
						confirmationPage_State = driver.findElement(By.xpath(".//*[@id='cusstate']")).getText();
						
					} catch (Exception e) {
						
					}
					try {
						
						confirmationPage_PostalCode = driver.findElement(By.xpath(".//*[@id='cusZip']")).getText();
						
					} catch (Exception e) {
						
					}
					activityDetails.setConfirmationPage_PostalCode(confirmationPage_PostalCode);
					activityDetails.setConfirmationPage_state(confirmationPage_State);	
				}
				
		
				
				
				WebElement transfertableIds = driver.findElement(By.className("table_transfer_participant_confirmation"));
				List<WebElement> transferrowLists = transfertableIds.findElements(By.tagName("tr"));
				
				for (int i = 0; i < transferrowLists.size()-2; i++) {
					
					String title = driver.findElement(By.xpath(".//*[@id='act_title_0_0_"+i+"']")).getText();
					cusTitleConfirm.add(title);
					String fName = driver.findElement(By.xpath(".//*[@id='act_fname_0_0_"+i+"']")).getText();
					cusFNameConfirm.add(fName);
					String lName = driver.findElement(By.xpath(".//*[@id='act_lname_0_0_"+i+"']")).getText();	
					cusLnameConfirm.add(lName);
				}
				
				activityDetails.setConfirmationPage_cusTitle(cusTitleConfirm);
				activityDetails.setConfirmationPage_cusFName(cusFNameConfirm);
				activityDetails.setConfirmationPage_cusLName(cusLnameConfirm);
				
				if (inventory.getPaymentDetails().equals("Pay Online")) {
					activityDetails.setPaymentReference_bookingList(driver.findElement(By.xpath(".//*[@id='paymentrefno']")).getText());
					activityDetails.setAuthentReference_bookingList(driver.findElement(By.xpath(".//*[@id='paymenttrackid']")).getText());
					activityDetails.setPaymentID(driver.findElement(By.xpath(".//*[@id='paymenttrackid']")).getText());
					
				}
				
				
				
			/////
				
				WebElement confirmCancel = driver.findElement(By.id("cxlpolicy_a_0"));
				List<WebElement> confirmCancelWebelement = confirmCancel.findElements(By.tagName("li"));
				
				ArrayList<String> confirmCancelPolicy = new ArrayList<String>();
				
				for (int j = 1; j <= confirmCancelWebelement.size(); j++) {
					
					String policy = driver.findElement(By.xpath(".//*[@id='cxlpolicy_a_0']/ul/li["+j+"]")).getText();
					confirmCancelPolicy.add(policy);
							
				}
				
				System.out.println(confirmCancelPolicy);
				activityDetails.setConfirmCancelPolicy(confirmCancelPolicy);
				
			}
			
			
		} else {
			activityDetails.setActivityResultsAvailble(false);
		}
		
		
		
		return activityDetails;

	}
	
}
