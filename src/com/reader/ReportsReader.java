package com.reader;

import inventoryreport.Inventory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.xerces.parsers.IntegratedParserConfiguration;
import org.openqa.selenium.WebDriver;

import reservationreport.Reservation;
import reservationreport.ReservationInfoDetails;
import bookingconfirmation.BookingConfirmation;
import bookingconfirmation.Quotation;
import bookinglistreport.BookingList;
import cancellation_modification.Cancellation;
import cancellation_modification.Modification;

import com.controller.PG_Properties;
import com.model.ActivityDetails;
import com.model.ActivityInventoryRecords;
import com.model.Search;
import com.types.TourOperatorType;

public class ReportsReader {
	
	private WebDriver driver         	= null;
	private ActivityDetails activitydetails;
	private BookingList bookingList;
	private BookingConfirmation confirmationDetails;
	private Reservation reservationdetails;
	private Map<String, String> currencyMap;
	private Inventory inventoryDetails;
	private Cancellation cancellationDetails;
	private Modification modDetails;
	private ActivityInventoryRecords inventory;
	private Search search;
	private StringBuffer  PrintWriter;
	private StringBuffer mailPrinter;
	private int ScenarioCount = 0;  
	private int testCaseCount, testCaseCountBookingList, testCaseCustomerMail, testCaseModify, payent_qty, testQuote ;
	private int testCaseVoucherMail, testcaseSupplier, testCaseCountReservation, testCaseInventory, testCaseCancel;
	private String noShowMatching, activityMainPolicy, activityCanPolicy1, activityCanPolicy2 ;
	private String paymentNoShowMatching, paymentMainPolicy, paymentCanPolicy1, paymentCanPolicy2 ;
	private String confirmNoShowMatching, confirmMainPolicy, confirmCanPolicy1, confirmCanPolicy2 ;
	private String resPolist1 = null;
	private String policyListOne = null ;
	private String policyListTwo  = null;
	private String noShowActual = null;
	private double rateConvertforUSD = 1.0 ;
	private double rateConvertforSearch = 1.0 ;
	private double noShowConvertforCancelPolicy = 1.0 ;
	private int ResultsPage_SubTotal, ResultsPage_Taxes, ResultsPage_TotalPayable, netRate, toAgentCommission;
	private int PaymentPage_SubTotal, PaymentPage_Taxes, PaymentPage_TotalPayable, PaymentPage_TotalGrossBookingValue, PaymentPage_TotalTax, PaymentPage_TotalPackageValue, PaymentPage_AmountProcessed, PaymentPage_AmountCheckIn, PaymentPage_CreditCardFee;
	private int ConfirmationPage_SubTotal, ConfirmationPage_Taxes, ConfirmationPage_TotalPayable, Confirmation_TotalGrossBookingValue, ConfirmationPage_TotalTax, ConfirmationPage_TotalPackageValue, ConfirmationPage_AmountProcessed, ConfirmationPage_AmountCheckIn;
	private double totalPayableforNoshow;
	private int totalCostInUsd, amount_onePerson, totalCost;
	private String bookingTotalvalueForAct;
	private int customerQty, creditTax, pMarkup, discountedVale;
	private String currentDateforMatch, dateWithBufferDates_3, dateWithBufferDates_1;
	private TourOperatorType  UserTypeDC_B2B;
	private Quotation quotationDetails;
	
	
	public ReportsReader(ActivityDetails activityDetails, ActivityInventoryRecords inventoryD ,Map<String, String> CurrencyMap, Search search , BookingList bookinglist, BookingConfirmation ConfirmationDetails, Reservation reservationdetails,Inventory inventoryDetails , Cancellation cancellationDetails, Modification modDetails, Quotation quotationdetails, WebDriver Driver) {
		
		this.driver = Driver;
		this.activitydetails = activityDetails;
		this.currencyMap = CurrencyMap;
		this.bookingList = bookinglist;
		this.confirmationDetails = ConfirmationDetails;
		this.reservationdetails = reservationdetails;
		this.search = search;
		this.inventoryDetails = inventoryDetails;
		this.cancellationDetails=cancellationDetails;
		this.inventory=inventoryD;
		this.modDetails=modDetails;
		this.quotationDetails = quotationdetails;
	}


	public void getActvivtyInventoryReport(WebDriver Driver) throws ParseException {
		
		PrintWriter = new StringBuffer();
		ScenarioCount ++;
		
		DateFormat dateFormatcurrentDate = new SimpleDateFormat("dd-MMM-yyyy");
		Calendar cal = Calendar.getInstance();
		currentDateforMatch = dateFormatcurrentDate.format(cal.getTime()); 
		UserTypeDC_B2B = inventory.getUserTypeDC_B2B();
			
		
		if (!(UserTypeDC_B2B == TourOperatorType.DC)) {
			pMarkup = Integer.parseInt(inventory.getToActivityPM());		
		}else{
			pMarkup = Integer.parseInt(inventory.getProfitmarkup());
		}
		
		double paxCount = Integer.parseInt(search.getAdults()) + Integer.parseInt(search.getChildren());
		
		double profitMarkUP = ((100 + Double.parseDouble(PG_Properties.getProperty("ProfitMarkup"))) / 100);
		
			PrintWriter.append("<html class='report'><head><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"> </head>");
			PrintWriter.append("<div class='header'><img height=\"19px\" src=\"menu_rez.png\"><p class='Hedding1'>Activity Reservation Report - Reservation No :- ["+activitydetails.getReservationNo()+"] - Date ["+currentDateforMatch+"]</p></div>");
			PrintWriter.append("<body>");		
			PrintWriter.append("<br><br>");
			PrintWriter.append("<div style=\"border:1px solid;border-radius:3px 3px 3px 3px;background-color:#8D8D86; width:50%;\">");
			PrintWriter.append("<p class='InfoSup'style='font-weight: bold;'>"+ScenarioCount+")<br></p>");
			PrintWriter.append("<p class='InfoSub'>Current Activity Reservation Criteria</p>");
			PrintWriter.append("<p class='InfoSub'>Portal URL :- "+PG_Properties.getProperty("Baseurl")+"</p>");
			PrintWriter.append("<p class='InfoSub'>Scenario :- "+search.getScenariotype()+"</p>");
			PrintWriter.append("<p class='InfoSub'>Selling Currency :- "+search.getSellingCurrency()+"</p>");
			PrintWriter.append("<p class='InfoSub'>Destination Name :- "+search.getDestination()+"</p>");
			PrintWriter.append("<p class='InfoSub'>Date From :- "+search.getDateFrom()+"</p>");
			PrintWriter.append("<p class='InfoSub'>Date To :- "+search.getDateTo()+"</p>");
			PrintWriter.append("<p class='InfoSub'>No of Adults :- "+search.getAdults()+"</p>");
			PrintWriter.append("<p class='InfoSub'>No of Children :- "+search.getChildren()+"</p>");
			PrintWriter.append("<p class='InfoSub'>Inventory Type :- "+inventory.getInventoryType()+"</p>");
			
			if (!(UserTypeDC_B2B == TourOperatorType.DC)) {
				PrintWriter.append("<p class='InfoSub'>To Name :- "+inventory.getUserTypeDC_B2B().toString()+"</p>");
			
			}else{
				PrintWriter.append("<p class='InfoSub'>User Type :- "+inventory.getUserTypeDC_B2B().toString()+"</p>");
			}
			PrintWriter.append("<p class='InfoSub'>Discount :- "+inventory.getDiscountYesORNo()+"</p>");
			PrintWriter.append("<p class='InfoSub'>Payment Method :- "+activitydetails.getPaymentDetails()+"</p>");
			PrintWriter.append("<p class='InfoSub'>Quotation :- "+search.getQuotaionReq()+"</p>");
			PrintWriter.append("</div>");
			PrintWriter.append("<br>");
			testCaseCount = 1;
			toAgentCommission = 0;
			
			if(!(PG_Properties.getProperty("PortalCurrency").toLowerCase().equals(inventory.getSupplierCurrency().toLowerCase()))){
				
				for(Entry<String, String> entry: currencyMap.entrySet()) {
					if(inventory.getSupplierCurrency().toLowerCase().equals(entry.getKey().toLowerCase())){
						
						rateConvertforUSD = Double.parseDouble(entry.getValue());
						
						if (!(UserTypeDC_B2B == TourOperatorType.DC)) {
						
							ResultsPage_SubTotal =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_SubTotal()) / rateConvertforUSD ));
							ResultsPage_Taxes =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_TotalTax()) / rateConvertforUSD ));						
							ResultsPage_TotalPayable =  (int) Math.ceil((   (Double.parseDouble(activitydetails.getToActivity_SubTotal()) + (Double.parseDouble(activitydetails.getToActivity_TotalTax()))  / rateConvertforUSD )));
								
							PaymentPage_SubTotal =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_SubTotal()) / rateConvertforUSD ));
							PaymentPage_Taxes =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_TotalTax()) / rateConvertforUSD ));
							PaymentPage_TotalPayable =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_TotalValue()) / rateConvertforUSD ));
							
							PaymentPage_TotalGrossBookingValue =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_SubTotal()) / rateConvertforUSD ));
							PaymentPage_TotalTax =  (int) Math.ceil(((Double.parseDouble(activitydetails.getToActivity_TotalTax()) + Double.parseDouble(inventory.getCreditCard_Fee()) ) / rateConvertforUSD ));
							PaymentPage_TotalPackageValue =  (int) Math.ceil((( Double.parseDouble(activitydetails.getToActivity_TotalValue()) + Double.parseDouble(inventory.getCreditCard_Fee())  ) / rateConvertforUSD ));
														
							if (UserTypeDC_B2B == TourOperatorType.NETCASH || UserTypeDC_B2B == TourOperatorType.NETCREDITLPON || UserTypeDC_B2B == TourOperatorType.NETCREDITLPOY) {
								
								PaymentPage_AmountProcessed =  (int) Math.ceil(( ( Double.parseDouble(activitydetails.getToActivity_TotalValue()) + Double.parseDouble(inventory.getCreditCard_Fee()) ) / rateConvertforUSD ));
							}
							
							if (UserTypeDC_B2B == TourOperatorType.COMCASH || UserTypeDC_B2B == TourOperatorType.COMCREDITLPONO || UserTypeDC_B2B == TourOperatorType.COMCREDITLPOY) {
								
								PaymentPage_AmountProcessed =  (int) Math.ceil(( ( Double.parseDouble(activitydetails.getToActivity_TotalValue()) + Double.parseDouble(inventory.getCreditCard_Fee())  - Double.parseDouble(activitydetails.getToAgentCommission())) / rateConvertforUSD ));
							}
							
							PaymentPage_AmountCheckIn =  (int) Math.ceil((Double.parseDouble(inventory.getPaymentPage_AmountCheckIn()) / rateConvertforUSD ));
							PaymentPage_CreditCardFee =  (int) Math.ceil((Double.parseDouble(inventory.getCreditCard_Fee()) / rateConvertforUSD )); 		
							
							ConfirmationPage_SubTotal =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_SubTotal()) / rateConvertforUSD ));
							ConfirmationPage_Taxes =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_TotalTax()) / rateConvertforUSD ));
							ConfirmationPage_TotalPayable =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_TotalValue()) / rateConvertforUSD ));
							
							Confirmation_TotalGrossBookingValue =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_SubTotal()) / rateConvertforUSD ));
							ConfirmationPage_TotalTax =  (int) Math.ceil(((Double.parseDouble(activitydetails.getToActivity_TotalTax()) + Double.parseDouble(inventory.getCreditCard_Fee()) ) / rateConvertforUSD ));
							ConfirmationPage_TotalPackageValue =  (int) Math.ceil((( Double.parseDouble(activitydetails.getToActivity_TotalValue()) + Double.parseDouble(inventory.getCreditCard_Fee())  ) / rateConvertforUSD ));
							
							if (UserTypeDC_B2B == TourOperatorType.NETCASH || UserTypeDC_B2B == TourOperatorType.NETCREDITLPON || UserTypeDC_B2B == TourOperatorType.NETCREDITLPOY) {
								
								ConfirmationPage_AmountProcessed =  (int) Math.ceil(( ( Double.parseDouble(activitydetails.getToActivity_TotalValue()) + Double.parseDouble(inventory.getCreditCard_Fee()) ) / rateConvertforUSD ));
							}
							
							if (UserTypeDC_B2B == TourOperatorType.COMCASH || UserTypeDC_B2B == TourOperatorType.COMCREDITLPONO || UserTypeDC_B2B == TourOperatorType.COMCREDITLPOY) {
								
								ConfirmationPage_AmountProcessed =  (int) Math.ceil(( ( Double.parseDouble(activitydetails.getToActivity_TotalValue()) + Double.parseDouble(inventory.getCreditCard_Fee())  - Double.parseDouble(activitydetails.getToAgentCommission())) / rateConvertforUSD ));
							}
							
							ConfirmationPage_AmountCheckIn =  (int) Math.ceil((Double.parseDouble(inventory.getPaymentPage_AmountCheckIn()) / rateConvertforUSD ));
												
							netRate = (int)(((Double.parseDouble(activitydetails.getToActivity_SubTotal()) * 100 ) / (100 + pMarkup))/ rateConvertforUSD );
							
							totalPayableforNoshow =  Double.parseDouble(activitydetails.getToActivity_SubTotal()) / rateConvertforUSD ;
						
							amount_onePerson = (int) Math.ceil(ResultsPage_SubTotal / paxCount);
							
							if (UserTypeDC_B2B == TourOperatorType.COMCASH || UserTypeDC_B2B == TourOperatorType.COMCREDITLPONO || UserTypeDC_B2B == TourOperatorType.COMCREDITLPOY) {
									
								toAgentCommission = (int) Math.ceil((Double.parseDouble(activitydetails.getToAgentCommission()) / rateConvertforUSD ));							
							}
							
													
						}else{
							
							//DC Rates
							
							ResultsPage_SubTotal =  (int) Math.ceil((Double.parseDouble(inventory.getResultsPage_SubTotal()) / rateConvertforUSD ));
							ResultsPage_Taxes =  (int) Math.ceil((Double.parseDouble(inventory.getResultsPage_Taxes()) / rateConvertforUSD ));						
							ResultsPage_TotalPayable =  (int) Math.ceil((Double.parseDouble(inventory.getResultsPage_TotalPayable()) / rateConvertforUSD ));
								
							PaymentPage_SubTotal =  (int) Math.ceil((Double.parseDouble(inventory.getPaymentPage_SubTotal()) / rateConvertforUSD ));
							PaymentPage_Taxes =  (int) Math.ceil((Double.parseDouble(inventory.getPaymentPage_Taxes()) / rateConvertforUSD ));
							PaymentPage_TotalPayable =  (int) Math.ceil((Double.parseDouble(inventory.getPaymentPage_TotalPayable()) / rateConvertforUSD ));
							PaymentPage_TotalGrossBookingValue =  (int) Math.ceil((Double.parseDouble(inventory.getPaymentPage_TotalGrossBookingValue()) / rateConvertforUSD ));
							PaymentPage_TotalTax =  (int) Math.ceil((Double.parseDouble(inventory.getPaymentPage_TotalTax()) / rateConvertforUSD ));
							PaymentPage_TotalPackageValue =  (int) Math.ceil((Double.parseDouble(inventory.getPaymentPage_TotalPackageValue()) / rateConvertforUSD ));
							PaymentPage_AmountProcessed =  (int) Math.ceil((Double.parseDouble(inventory.getPaymentPage_AmountProcessed()) / rateConvertforUSD ));
							PaymentPage_AmountCheckIn =  (int) Math.ceil((Double.parseDouble(inventory.getPaymentPage_AmountCheckIn()) / rateConvertforUSD ));
							PaymentPage_CreditCardFee =  (int) Math.ceil((Double.parseDouble(inventory.getCreditCard_Fee()) / rateConvertforUSD )); 		
							
							ConfirmationPage_SubTotal =  (int) Math.ceil((Double.parseDouble(inventory.getConfirmationPage_SubTotal()) / rateConvertforUSD ));
							ConfirmationPage_Taxes =  (int) Math.ceil((Double.parseDouble(inventory.getConfirmationPage_Taxes()) / rateConvertforUSD ));
							ConfirmationPage_TotalPayable =  (int) Math.ceil((Double.parseDouble(inventory.getConfirmationPage_TotalPayable()) / rateConvertforUSD ));
							Confirmation_TotalGrossBookingValue =  (int) Math.ceil((Double.parseDouble(inventory.getConfirmation_TotalGrossBookingValue()) / rateConvertforUSD ));
							ConfirmationPage_TotalTax =  (int) Math.ceil((Double.parseDouble(inventory.getConfirmationPage_TotalTax()) / rateConvertforUSD ));
							ConfirmationPage_TotalPackageValue =  (int) Math.ceil((Double.parseDouble(inventory.getConfirmationPage_TotalPackageValue()) / rateConvertforUSD ));
							ConfirmationPage_AmountProcessed =  (int) Math.ceil((Double.parseDouble(inventory.getConfirmationPage_AmountProcessed()) / rateConvertforUSD ));
							ConfirmationPage_AmountCheckIn =  (int) Math.ceil((Double.parseDouble(inventory.getConfirmationPage_AmountCheckIn()) / rateConvertforUSD ));
							
							netRate = (int)(((Double.parseDouble(inventory.getResultsPage_SubTotal()) * 100 ) / (100 + pMarkup))/ rateConvertforUSD );
							
							totalPayableforNoshow =  Double.parseDouble(inventory.getPaymentPage_SubTotal()) / rateConvertforUSD ;
						
							amount_onePerson = (int) Math.ceil(ResultsPage_SubTotal / paxCount);
							
						}
					}
				}		
			}
						
			
			if(!(PG_Properties.getProperty("PortalCurrency").toLowerCase().equals(search.getSellingCurrency().toLowerCase()))){
				
				for(Entry<String, String> entry: currencyMap.entrySet()) {
					if(search.getSellingCurrency().toLowerCase().equals(entry.getKey().toLowerCase())){
						
						rateConvertforSearch = Double.parseDouble(entry.getValue());			
						
						if (!(UserTypeDC_B2B == TourOperatorType.DC)) {
							
							ResultsPage_SubTotal =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_SubTotal()) / rateConvertforUSD ) * rateConvertforSearch);
							ResultsPage_Taxes =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_TotalTax()) / rateConvertforUSD ) * rateConvertforSearch);						
							ResultsPage_TotalPayable =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_SubTotal()) + (Double.parseDouble(activitydetails.getToActivity_TotalTax())) / rateConvertforUSD ) * rateConvertforSearch);
								
							PaymentPage_SubTotal =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_SubTotal()) / rateConvertforUSD ) * rateConvertforSearch);
							PaymentPage_Taxes =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_TotalTax()) / rateConvertforUSD ) * rateConvertforSearch);
							PaymentPage_TotalPayable =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_TotalValue()) / rateConvertforUSD ) * rateConvertforSearch);
							
							PaymentPage_TotalGrossBookingValue =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_SubTotal()) / rateConvertforUSD ) * rateConvertforSearch);
							PaymentPage_TotalTax =  (int) Math.ceil(((Double.parseDouble(activitydetails.getToActivity_TotalTax()) + Double.parseDouble(inventory.getCreditCard_Fee()) ) / rateConvertforUSD ) * rateConvertforSearch);
							PaymentPage_TotalPackageValue =  (int) Math.ceil((( Double.parseDouble(activitydetails.getToActivity_TotalValue()) + Double.parseDouble(inventory.getCreditCard_Fee())  ) / rateConvertforUSD ) * rateConvertforSearch);
														
							if (UserTypeDC_B2B == TourOperatorType.NETCASH || UserTypeDC_B2B == TourOperatorType.NETCREDITLPON || UserTypeDC_B2B == TourOperatorType.NETCREDITLPOY) {
								
								PaymentPage_AmountProcessed =  (int) Math.ceil(( ( Double.parseDouble(activitydetails.getToActivity_TotalValue()) + Double.parseDouble(inventory.getCreditCard_Fee()) ) / rateConvertforUSD ) * rateConvertforSearch);
							}
							
							if (UserTypeDC_B2B == TourOperatorType.COMCASH || UserTypeDC_B2B == TourOperatorType.COMCREDITLPONO || UserTypeDC_B2B == TourOperatorType.COMCREDITLPOY) {
								
								PaymentPage_AmountProcessed =  (int) Math.ceil((( ( Double.parseDouble(activitydetails.getToActivity_TotalValue()) + Double.parseDouble(inventory.getCreditCard_Fee()) - Double.parseDouble(activitydetails.getToAgentCommission()  ))) / rateConvertforUSD ) * rateConvertforSearch);
							}
							

							PaymentPage_AmountCheckIn =  (int) Math.ceil((Double.parseDouble(inventory.getPaymentPage_AmountCheckIn()) / rateConvertforUSD ) * rateConvertforSearch);
							PaymentPage_CreditCardFee =  (int) Math.ceil((Double.parseDouble(inventory.getCreditCard_Fee()) / rateConvertforUSD ) * rateConvertforSearch); 		
							
							ConfirmationPage_SubTotal =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_SubTotal()) / rateConvertforUSD ) * rateConvertforSearch);
							ConfirmationPage_Taxes =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_TotalTax()) / rateConvertforUSD ) * rateConvertforSearch);
							ConfirmationPage_TotalPayable =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_TotalValue()) / rateConvertforUSD ) * rateConvertforSearch);
							
							Confirmation_TotalGrossBookingValue =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_SubTotal()) / rateConvertforUSD ) * rateConvertforSearch);
							ConfirmationPage_TotalTax =  (int) Math.ceil(((Double.parseDouble(activitydetails.getToActivity_TotalTax()) + Double.parseDouble(inventory.getCreditCard_Fee()) ) / rateConvertforUSD ) * rateConvertforSearch);
							ConfirmationPage_TotalPackageValue =  (int) Math.ceil((( Double.parseDouble(activitydetails.getToActivity_TotalValue()) + Double.parseDouble(inventory.getCreditCard_Fee())  ) / rateConvertforUSD ) * rateConvertforSearch);
							
							if (UserTypeDC_B2B == TourOperatorType.NETCASH || UserTypeDC_B2B == TourOperatorType.NETCREDITLPON || UserTypeDC_B2B == TourOperatorType.NETCREDITLPOY) {
								
								ConfirmationPage_AmountProcessed =  (int) Math.ceil(( ( Double.parseDouble(activitydetails.getToActivity_TotalValue()) + Double.parseDouble(inventory.getCreditCard_Fee()) ) / rateConvertforUSD ) * rateConvertforSearch);
							}
							
							if (UserTypeDC_B2B == TourOperatorType.COMCASH || UserTypeDC_B2B == TourOperatorType.COMCREDITLPONO || UserTypeDC_B2B == TourOperatorType.COMCREDITLPOY) {
								
								ConfirmationPage_AmountProcessed =  (int) Math.ceil((( ( Double.parseDouble(activitydetails.getToActivity_TotalValue()) + Double.parseDouble(inventory.getCreditCard_Fee()) - Double.parseDouble(activitydetails.getToAgentCommission()  ))) / rateConvertforUSD ) * rateConvertforSearch);
							}
							
							
							ConfirmationPage_AmountCheckIn =  (int) Math.ceil((Double.parseDouble(inventory.getPaymentPage_AmountCheckIn()) / rateConvertforUSD ) * rateConvertforSearch);
												
							netRate = (int)((((Double.parseDouble(activitydetails.getToActivity_SubTotal()) * 100 ) / (100 + pMarkup))/ rateConvertforUSD ) * rateConvertforSearch);
							
							totalPayableforNoshow =  ((Double.parseDouble(activitydetails.getToActivity_SubTotal()) / rateConvertforUSD) * rateConvertforSearch) ;
						
							amount_onePerson = (int) Math.ceil(ResultsPage_SubTotal / paxCount);
							
							if (UserTypeDC_B2B == TourOperatorType.COMCASH || UserTypeDC_B2B == TourOperatorType.COMCREDITLPONO || UserTypeDC_B2B == TourOperatorType.COMCREDITLPOY) {
								
								toAgentCommission = (int) Math.ceil((Double.parseDouble(activitydetails.getToAgentCommission()) / rateConvertforUSD ) * rateConvertforSearch);						
							}
							
							
						}else{
							
							//DC Rates
							
							ResultsPage_SubTotal =  (int) Math.ceil((Double.parseDouble(inventory.getResultsPage_SubTotal()) / rateConvertforUSD ) * rateConvertforSearch);
							ResultsPage_Taxes =  (int) Math.ceil((Double.parseDouble(inventory.getResultsPage_Taxes()) / rateConvertforUSD ) * rateConvertforSearch);						
							ResultsPage_TotalPayable =  (int) Math.ceil((Double.parseDouble(inventory.getResultsPage_TotalPayable()) / rateConvertforUSD ) * rateConvertforSearch);
								
							PaymentPage_SubTotal =  (int) Math.ceil((Double.parseDouble(inventory.getPaymentPage_SubTotal()) / rateConvertforUSD ) * rateConvertforSearch);
							PaymentPage_Taxes =  (int) Math.ceil((Double.parseDouble(inventory.getPaymentPage_Taxes()) / rateConvertforUSD ) * rateConvertforSearch);
							PaymentPage_TotalPayable =  (int) Math.ceil((Double.parseDouble(inventory.getPaymentPage_TotalPayable()) / rateConvertforUSD ) * rateConvertforSearch);
							PaymentPage_TotalGrossBookingValue =  (int) Math.ceil((Double.parseDouble(inventory.getPaymentPage_TotalGrossBookingValue()) / rateConvertforUSD ) * rateConvertforSearch);
							PaymentPage_TotalTax =  (int) Math.ceil((Double.parseDouble(inventory.getPaymentPage_TotalTax()) / rateConvertforUSD ) * rateConvertforSearch);
							PaymentPage_TotalPackageValue =  (int) Math.ceil((Double.parseDouble(inventory.getPaymentPage_TotalPackageValue()) / rateConvertforUSD ) * rateConvertforSearch);
							PaymentPage_AmountProcessed =  (int) Math.ceil((Double.parseDouble(inventory.getPaymentPage_AmountProcessed()) / rateConvertforUSD ) * rateConvertforSearch);
							PaymentPage_AmountCheckIn =  (int) Math.ceil((Double.parseDouble(inventory.getPaymentPage_AmountCheckIn()) / rateConvertforUSD ) * rateConvertforSearch);
							PaymentPage_CreditCardFee =  (int) Math.ceil((Double.parseDouble(inventory.getCreditCard_Fee()) / rateConvertforUSD ) * rateConvertforSearch); 		
							
							ConfirmationPage_SubTotal =  (int) Math.ceil((Double.parseDouble(inventory.getConfirmationPage_SubTotal()) / rateConvertforUSD ) * rateConvertforSearch);
							ConfirmationPage_Taxes =  (int) Math.ceil((Double.parseDouble(inventory.getConfirmationPage_Taxes()) / rateConvertforUSD ) * rateConvertforSearch);
							ConfirmationPage_TotalPayable =  (int) Math.ceil((Double.parseDouble(inventory.getConfirmationPage_TotalPayable()) / rateConvertforUSD ) * rateConvertforSearch);
							Confirmation_TotalGrossBookingValue =  (int) Math.ceil((Double.parseDouble(inventory.getConfirmation_TotalGrossBookingValue()) / rateConvertforUSD ) * rateConvertforSearch);
							ConfirmationPage_TotalTax =  (int) Math.ceil((Double.parseDouble(inventory.getConfirmationPage_TotalTax()) / rateConvertforUSD ) * rateConvertforSearch);
							ConfirmationPage_TotalPackageValue =  (int) Math.ceil((Double.parseDouble(inventory.getConfirmationPage_TotalPackageValue()) / rateConvertforUSD ) * rateConvertforSearch);
							ConfirmationPage_AmountProcessed =  (int) Math.ceil((Double.parseDouble(inventory.getConfirmationPage_AmountProcessed()) / rateConvertforUSD ) * rateConvertforSearch);
							ConfirmationPage_AmountCheckIn =  (int) Math.ceil((Double.parseDouble(inventory.getConfirmationPage_AmountCheckIn()) / rateConvertforUSD ) * rateConvertforSearch);
							
							netRate = (int)((((Double.parseDouble(inventory.getResultsPage_SubTotal()) * 100 ) / (100 + pMarkup))/ rateConvertforUSD ) * rateConvertforSearch);
							
							totalPayableforNoshow =  ((Double.parseDouble(inventory.getPaymentPage_SubTotal()) / rateConvertforUSD) * rateConvertforSearch );
						
							amount_onePerson = (int) Math.ceil(ResultsPage_SubTotal / paxCount);
							
						}
					}
				}		
			}
			
			if(inventory.getSupplierCurrency().toLowerCase().equals(search.getSellingCurrency().toLowerCase())){		
						
				if (!(UserTypeDC_B2B == TourOperatorType.DC)) {
					
					ResultsPage_SubTotal =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_SubTotal())));
					ResultsPage_Taxes =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_TotalTax())));						
					ResultsPage_TotalPayable =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_SubTotal()) + (Double.parseDouble(activitydetails.getToActivity_TotalTax()))));
						
					PaymentPage_SubTotal =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_SubTotal())));
					PaymentPage_Taxes =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_TotalTax())));
					PaymentPage_TotalPayable =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_TotalValue())));
					
					PaymentPage_TotalGrossBookingValue =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_SubTotal())));
					PaymentPage_TotalTax =  (int) Math.ceil(((Double.parseDouble(activitydetails.getToActivity_TotalTax()) + Double.parseDouble(inventory.getCreditCard_Fee()) )));
					PaymentPage_TotalPackageValue =  (int) Math.ceil((( Double.parseDouble(activitydetails.getToActivity_TotalValue()) + Double.parseDouble(inventory.getCreditCard_Fee()) )));
					
					if (UserTypeDC_B2B == TourOperatorType.NETCASH || UserTypeDC_B2B == TourOperatorType.NETCREDITLPON || UserTypeDC_B2B == TourOperatorType.NETCREDITLPOY) {
						
						PaymentPage_AmountProcessed =  (int) Math.ceil(((Double.parseDouble(activitydetails.getToActivity_TotalValue()) + Double.parseDouble(inventory.getCreditCard_Fee()) )));						
					}
					
					if (UserTypeDC_B2B == TourOperatorType.COMCASH || UserTypeDC_B2B == TourOperatorType.COMCREDITLPONO || UserTypeDC_B2B == TourOperatorType.COMCREDITLPOY) {
						
						PaymentPage_AmountProcessed =  (int) Math.ceil((((Double.parseDouble(activitydetails.getToActivity_TotalValue()) + Double.parseDouble(inventory.getCreditCard_Fee()) - Double.parseDouble(activitydetails.getToAgentCommission())))));												
					}
										
					PaymentPage_AmountCheckIn =  (int) Math.ceil((Double.parseDouble(inventory.getPaymentPage_AmountCheckIn())));
					PaymentPage_CreditCardFee =  (int) Math.ceil((Double.parseDouble(inventory.getCreditCard_Fee()))); 		
					
					ConfirmationPage_SubTotal =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_SubTotal())));
					ConfirmationPage_Taxes =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_TotalTax())));
					ConfirmationPage_TotalPayable =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_TotalValue())));
					
					Confirmation_TotalGrossBookingValue =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_SubTotal())));
					ConfirmationPage_TotalTax =  (int) Math.ceil(((Double.parseDouble(activitydetails.getToActivity_TotalTax()) + Double.parseDouble(inventory.getCreditCard_Fee()) )));
					ConfirmationPage_TotalPackageValue =  (int) Math.ceil((( Double.parseDouble(activitydetails.getToActivity_TotalValue()) + Double.parseDouble(inventory.getCreditCard_Fee())  )));
				
					if (UserTypeDC_B2B == TourOperatorType.NETCASH || UserTypeDC_B2B == TourOperatorType.NETCREDITLPON || UserTypeDC_B2B == TourOperatorType.NETCREDITLPOY) {
						
						ConfirmationPage_AmountProcessed =  (int) Math.ceil(((Double.parseDouble(activitydetails.getToActivity_TotalValue()) + Double.parseDouble(inventory.getCreditCard_Fee()) )));						
					}
					
					if (UserTypeDC_B2B == TourOperatorType.COMCASH || UserTypeDC_B2B == TourOperatorType.COMCREDITLPONO || UserTypeDC_B2B == TourOperatorType.COMCREDITLPOY) {
						
						ConfirmationPage_AmountProcessed =  (int) Math.ceil((((Double.parseDouble(activitydetails.getToActivity_TotalValue()) + Double.parseDouble(inventory.getCreditCard_Fee()) - Double.parseDouble(activitydetails.getToAgentCommission())))));												
					}
					
					ConfirmationPage_AmountCheckIn =  (int) Math.ceil((Double.parseDouble(inventory.getPaymentPage_AmountCheckIn())));
										
					netRate = (int)(((Double.parseDouble(activitydetails.getToActivity_SubTotal()) * 100 ) / (100 + pMarkup)));
					
					totalPayableforNoshow =  Double.parseDouble(activitydetails.getToActivity_SubTotal()) ;
				
					amount_onePerson = (int) Math.ceil(ResultsPage_SubTotal / paxCount);
					
					if (UserTypeDC_B2B == TourOperatorType.COMCASH || UserTypeDC_B2B == TourOperatorType.COMCREDITLPONO || UserTypeDC_B2B == TourOperatorType.COMCREDITLPOY) {
						
						toAgentCommission = (int) Math.ceil((Double.parseDouble(activitydetails.getToAgentCommission())));
					}
											
				}else{
					
					//DC Rates
					
					ResultsPage_SubTotal =  (int) Math.ceil((Double.parseDouble(inventory.getResultsPage_SubTotal())));
					ResultsPage_Taxes =  (int) Math.ceil((Double.parseDouble(inventory.getResultsPage_Taxes())));						
					ResultsPage_TotalPayable =  (int) Math.ceil((Double.parseDouble(inventory.getResultsPage_TotalPayable())));
						
					PaymentPage_SubTotal =  (int) Math.ceil((Double.parseDouble(inventory.getPaymentPage_SubTotal())));
					PaymentPage_Taxes =  (int) Math.ceil((Double.parseDouble(inventory.getPaymentPage_Taxes())));
					PaymentPage_TotalPayable =  (int) Math.ceil((Double.parseDouble(inventory.getPaymentPage_TotalPayable())));
					PaymentPage_TotalGrossBookingValue =  (int) Math.ceil((Double.parseDouble(inventory.getPaymentPage_TotalGrossBookingValue())));
					PaymentPage_TotalTax =  (int) Math.ceil((Double.parseDouble(inventory.getPaymentPage_TotalTax())));
					PaymentPage_TotalPackageValue =  (int) Math.ceil((Double.parseDouble(inventory.getPaymentPage_TotalPackageValue())));
					PaymentPage_AmountProcessed =  (int) Math.ceil((Double.parseDouble(inventory.getPaymentPage_AmountProcessed())));
					PaymentPage_AmountCheckIn =  (int) Math.ceil((Double.parseDouble(inventory.getPaymentPage_AmountCheckIn())));
					PaymentPage_CreditCardFee =  (int) Math.ceil((Double.parseDouble(inventory.getCreditCard_Fee()))); 		
					
					ConfirmationPage_SubTotal =  (int) Math.ceil((Double.parseDouble(inventory.getConfirmationPage_SubTotal())));
					ConfirmationPage_Taxes =  (int) Math.ceil((Double.parseDouble(inventory.getConfirmationPage_Taxes())));
					ConfirmationPage_TotalPayable =  (int) Math.ceil((Double.parseDouble(inventory.getConfirmationPage_TotalPayable())));
					Confirmation_TotalGrossBookingValue =  (int) Math.ceil((Double.parseDouble(inventory.getConfirmation_TotalGrossBookingValue())));
					ConfirmationPage_TotalTax =  (int) Math.ceil((Double.parseDouble(inventory.getConfirmationPage_TotalTax())));
					ConfirmationPage_TotalPackageValue =  (int) Math.ceil((Double.parseDouble(inventory.getConfirmationPage_TotalPackageValue())));
					ConfirmationPage_AmountProcessed =  (int) Math.ceil((Double.parseDouble(inventory.getConfirmationPage_AmountProcessed())));
					ConfirmationPage_AmountCheckIn =  (int) Math.ceil((Double.parseDouble(inventory.getConfirmationPage_AmountCheckIn())));
					
					netRate = (int)(((Double.parseDouble(inventory.getResultsPage_SubTotal()) * 100 ) / (100 + pMarkup)) );
					
					totalPayableforNoshow =  Double.parseDouble(inventory.getPaymentPage_SubTotal()) ;
				
					amount_onePerson = (int) Math.ceil(ResultsPage_SubTotal / paxCount);
					
				}
					
			}
			
			
			PrintWriter.append("<br><br><table ><tr><th>Test Case No</th><th>Test Description</th><th>Expect Values - [Excel Value]</th><th>Actual Values</th><th>Test Status</th></tr>");
			
			
			if (activitydetails.isActivityResultsAvailble() == true) {
											
				////Booking Engine
				PrintWriter.append("<tr><td class='fontiiii'>Booking Engine</td><tr>"); 
				
							
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Booking Engine</td>");
				PrintWriter.append("<td>Booking Engine Should be Available</td>");
				
				if (activitydetails.isBookingEngineLoaded() == true) {
					
					PrintWriter.append("<td>Booking Engine Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Booking Engine Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Country Of Residence</td>");
				PrintWriter.append("<td>Country Of Residence Should be Available</td>");
				
				if (activitydetails.isCountryLoaded() == true) {
					
					PrintWriter.append("<td>Country Of Residence Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Country Of Residence Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Destination</td>");
				PrintWriter.append("<td>Destination Should be Available</td>");
				
				if (activitydetails.isCityLoaded() == true) {
					
					PrintWriter.append("<td>Destination Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Destination Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Departure Date</td>");
				PrintWriter.append("<td>Departure Date Should be Available</td>");
				
				if (activitydetails.isDateFromLoaded() == true) {
					
					PrintWriter.append("<td>Departure Date Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Departure Date Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Arrival Date</td>");
				PrintWriter.append("<td>Arrival Date Should be Available</td>");
				
				if (activitydetails.isDateToLoaded() == true) {
					
					PrintWriter.append("<td>Arrival Date Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Arrival Date Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Adults Drop Down</td>");
				PrintWriter.append("<td>Adults Drop Down Should be Available</td>");
				
				if (activitydetails.isAdultsLoaded() == true) {
					
					PrintWriter.append("<td>Adults Drop Down Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Adults Drop Down Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Childs Drop Down</td>");
				PrintWriter.append("<td>Childs Drop Down Should be Available</td>");
				
				if (activitydetails.isChildsLoaded() == true) {
					
					PrintWriter.append("<td>Childs Drop Down Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Childs Drop Down Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Country List loading</td>");
				PrintWriter.append("<td>Country List Should be Available</td>");
				
				if (activitydetails.isCountryListLoaded() == true) {
					
					PrintWriter.append("<td>Country List Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Country List Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Calender loading</td>");
				PrintWriter.append("<td>Calender Should be Available</td>");
				
				if (activitydetails.isCalenderAvailble() == true) {
					
					PrintWriter.append("<td>Calender Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Calender Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Program Category loading</td>");
				PrintWriter.append("<td>Program Category Should be Available</td>");
				
				if (activitydetails.isProgramCat() == true) {
					
					PrintWriter.append("<td>Program Category Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Program Category Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Preferred Currency loading</td>");
				PrintWriter.append("<td>Preferred Currency Should be Available</td>");
				
				if (activitydetails.isPreCurrency() == true) {
					
					PrintWriter.append("<td>Preferred Currency Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Preferred Currency Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Promotion Code loading</td>");
				PrintWriter.append("<td>Promotion Code Should be Available</td>");
				
				if (activitydetails.isPromoCode() == true) {
					
					PrintWriter.append("<td>Promotion Code Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Promotion Code Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				
				
				
				
				
				
				
				
				
				
				/////  Results Page
				PrintWriter.append("<tr><td class='fontiiii'>Results Page</td><tr>"); 
				
				if(inventory.getInventoryType().toLowerCase().contains("request")){
					
					testCaseCount ++;	
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Program availability type</td>");
					PrintWriter.append("<td>"+inventory.getInventoryType()+"</td>");
					
					if(activitydetails.getStatus().toLowerCase().equals(inventory.getInventoryType().toLowerCase())){
						
						PrintWriter.append("<td>"+activitydetails.getStatus()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getStatus()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
				}
				
				if (inventory.getInventoryType().toLowerCase().equals("allotments")) {
					
					testCaseCount ++;	
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Program availability type</td>");
					PrintWriter.append("<td>"+inventory.getInventoryType()+"</td>");
					
					if(activitydetails.getStatus().toLowerCase().equals("Available".toLowerCase())){
						
						PrintWriter.append("<td>"+activitydetails.getStatus()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getStatus()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
				}
				
				
				///
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Price range filter loading</td>");
				PrintWriter.append("<td>Price range filter Should be Available</td>");
				
				if (activitydetails.isPriceRangeFilter() == true) {
					
					PrintWriter.append("<td>Price range filter Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Price range filter Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Program Availability loading</td>");
				PrintWriter.append("<td>Program Availability Should be Available</td>");
				
				if (activitydetails.isProgramAvailablity() == true) {
					
					PrintWriter.append("<td>Program Availability Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Program Availability Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Name Filter loading</td>");
				PrintWriter.append("<td>Activity Name Filter Should be Available</td>");
				
				if (activitydetails.isActivityNameFilter() == true) {
					
					PrintWriter.append("<td>Activity Name Filter Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Activity Name Filter Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Price Range Filter - Test 1</td>");
				PrintWriter.append("<td>Min Prize is - "+search.getSellingCurrency()+" "+activitydetails.getMinPrize()+"</td>");
				
				if (activitydetails.getMinPrize().equals(activitydetails.getPageLoadedResults())) {
					
					PrintWriter.append("<td>Results Element 1 - "+search.getSellingCurrency()+" "+activitydetails.getPageLoadedResults()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Results Element 1 - "+search.getSellingCurrency()+" "+activitydetails.getPageLoadedResults()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				/*int prizeChanged_1 = Integer.parseInt(activitydetails.getPrizeChanged_1());
				int PrizeResults_1 = Integer.parseInt(activitydetails.getPrizeResults_1());
				int prizeChanged_2 = Integer.parseInt(activitydetails.getPrizeChanged_2());
				int PrizeResults_2 = Integer.parseInt(activitydetails.getPrizeResults_2());
				
				
				
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Price Range Filter - Test 2</td>");
				PrintWriter.append("<td>Min Prize set to - "+search.getSellingCurrency()+" "+activitydetails.getPrizeChanged_1()+"</td>");
				
				if (PrizeResults_1 >= prizeChanged_1) {
					
					PrintWriter.append("<td>Results Element 1 - "+search.getSellingCurrency()+" "+activitydetails.getPrizeResults_1()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Results Element 1 - "+search.getSellingCurrency()+" "+activitydetails.getPrizeResults_1()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Price Range Filter - Test 3</td>");
				PrintWriter.append("<td>Max Prize set to - "+search.getSellingCurrency()+" "+activitydetails.getPrizeChanged_2()+"</td>");
				
				if (prizeChanged_2 == PrizeResults_2) {
					
					PrintWriter.append("<td>Results Element 1 - "+search.getSellingCurrency()+" "+activitydetails.getPrizeResults_2()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Results Element 1 - "+search.getSellingCurrency()+" "+activitydetails.getPrizeResults_2()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
				}*/
				
				
				///
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity results availability</td>");
				PrintWriter.append("<td>Results Should be available</td>");
							
				String resultsAvailable = Boolean.toString(activitydetails.isResultsAvailable());
				
				if(resultsAvailable.equals("true")){
						
					PrintWriter.append("<td>"+resultsAvailable+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+resultsAvailable+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity programs availability</td>");
				PrintWriter.append("<td>Activity Program details should be available</td>");
							
				String programDisplayAvailable = Boolean.toString(activitydetails.isProgramDisplayAvailable());
				
				if(programDisplayAvailable.equals("true")){
						
					PrintWriter.append("<td>"+programDisplayAvailable+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+programDisplayAvailable+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				////
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Available and On Request Programs availability</td>");
				PrintWriter.append("<td>Activity Programs availability</td>");
							
				
				if(activitydetails.getActivityTotalCount() == activitydetails.getActivityAvailableCount() + activitydetails.getActivityOnReqCount()){
						
					PrintWriter.append("<td>Total Activity "+activitydetails.getActivityTotalCount()+" = Available "+activitydetails.getActivityAvailableCount()+" + OnRequest "+activitydetails.getActivityOnReqCount()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>Total Activity "+activitydetails.getActivityTotalCount()+" = Available "+activitydetails.getActivityAvailableCount()+" + OnRequest "+activitydetails.getActivityOnReqCount()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				///////
				
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Remove Activity From Cart</td>");
				PrintWriter.append("<td>Should be remove</td>");
				
				if (activitydetails.isActivityRemoved() == true) {
					
					PrintWriter.append("<td>Removed</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>No activity in the cart</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity More Info</td>");
				PrintWriter.append("<td>Should be loaded</td>");
				
				if (activitydetails.isMoreInfo() == true) {
					
					PrintWriter.append("<td>Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Cancellation Policy</td>");
				PrintWriter.append("<td>Should be loaded</td>");
				
				if (activitydetails.isCancelPolicy() == true) {
					
					PrintWriter.append("<td>Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				//////
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Currency - Filter</td>");
				PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
							
				if(search.getSellingCurrency().toLowerCase().equals(activitydetails.getFiltercurrency().toLowerCase())){
						
					PrintWriter.append("<td>"+activitydetails.getFiltercurrency()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+activitydetails.getFiltercurrency()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				////
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity CheckIn date changing</td>");
				PrintWriter.append("<td>Original CheckIn Date - "+search.getDateFrom()+"</td>");
				PrintWriter.append("<td>CheckIn Date changed to - "+activitydetails.getChanged_DateFrom()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");				
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity CheckOut date changing</td>");
				PrintWriter.append("<td>Original CheckOut Date - "+search.getDateTo()+"</td>");
				PrintWriter.append("<td>CheckOut Date changed to - "+activitydetails.getChanged_DateTo()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");				
							
				
				////
				
				String dateFromToCal = activitydetails.getChanged_DateFrom();
				String dateToCal = activitydetails.getChanged_DateTo();
				SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
				
				Date d1 = null;
				Date d2 = null;
		 
				try {
					d1 = format.parse(dateFromToCal);
					d2 = format.parse(dateToCal);
		 
					//in milliseconds
					long diffD = d2.getTime() - d1.getTime();
					int diffDays = (int)(diffD / (24 * 60 * 60 * 1000));
					
					testCaseCount ++;			
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity days count</td>");
					PrintWriter.append("<td>From :- "+dateFromToCal+" - To :- "+dateToCal+" = "+(diffDays+1)+"</td>");
							
					if(activitydetails.getDaysCount() == (diffDays+1)){
							
						PrintWriter.append("<td>Results page days count - "+activitydetails.getDaysCount()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>Results page days count - "+activitydetails.getDaysCount()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
	
		 
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				////
				
				
				///
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Currency - Results Page </td>");
				PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
							
				if(search.getSellingCurrency().toLowerCase().equals(activitydetails.getActivityCurrency().toLowerCase())){
						
					PrintWriter.append("<td>"+activitydetails.getActivityCurrency()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+activitydetails.getActivityCurrency()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity More info Description - Results Page</td>");
				PrintWriter.append("<td>"+inventory.getActivityDescription()+"</td>");
				
				if(inventory.getActivityDescription().toLowerCase().contains(activitydetails.getActDescription().toLowerCase())){
						
					PrintWriter.append("<td>"+activitydetails.getActDescription()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+activitydetails.getActDescription()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				/////
				
				PrintWriter.append("<tr><td class='fontiiii'>Search Again Frame</td><tr>"); 
				
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Destination [Search Again Frame]</td>");
				PrintWriter.append("<td>Destination Should be Available</td>");
				
				if (activitydetails.isSearchAgainCityLoaded() == true) {
					
					PrintWriter.append("<td>Destination Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Destination Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Departure Date [Search Again Frame]</td>");
				PrintWriter.append("<td>Departure Date Should be Available</td>");
				
				if (activitydetails.isSearchAgainDateFromLoaded() == true) {
					
					PrintWriter.append("<td>Departure Date Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Departure Date Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Arrival Date [Search Again Frame]</td>");
				PrintWriter.append("<td>Arrival Date Should be Available</td>");
				
				if (activitydetails.isSearchAgainDateToLoaded() == true) {
					
					PrintWriter.append("<td>Arrival Date Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Arrival Date Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Adults DropDown [Search Again Frame]</td>");
				PrintWriter.append("<td>Adults DropDown Should be Available</td>");
				
				if (activitydetails.isAdultLoaded() == true) {
					
					PrintWriter.append("<td>Adults DropDown Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Adults DropDown Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Child DropDown [Search Again Frame]</td>");
				PrintWriter.append("<td>Child DropDown Should be Available</td>");
				
				if (activitydetails.isChildLoaded() == true) {
					
					PrintWriter.append("<td>Child DropDown Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Child DropDown Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				
				
				
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Program Category loading [Search Again Frame]</td>");
				PrintWriter.append("<td>Program Category Should be Available</td>");
				
				if (activitydetails.isSearchAgainProgramCat() == true) {
					
					PrintWriter.append("<td>Program Category Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Program Category Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Preferred Currency loading [Search Again Frame]</td>");
				PrintWriter.append("<td>Preferred Currency Should be Available</td>");
				
				if (activitydetails.isSearchAgainPreCurrency() == true) {
					
					PrintWriter.append("<td>Preferred Currency Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Preferred Currency Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Promotion Code loading [Search Again Frame]</td>");
				PrintWriter.append("<td>Promotion Code Should be Available</td>");
				
				if (activitydetails.isSearchAgainPromoCode() == true) {
					
					PrintWriter.append("<td>Promotion Code Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Promotion Code Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				/////
				
				String getDateTo= search.getDateTo();
				String getDateFrom= search.getDateFrom();
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity City Mapping [Search Again Frame]- Results Page</td>");
				PrintWriter.append("<td>"+search.getDestination()+"</td>");
							
				if(search.getDestination().toLowerCase().contains(activitydetails.getResultsPage_destinaton().toLowerCase())){
						
					PrintWriter.append("<td>"+activitydetails.getResultsPage_destinaton()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+activitydetails.getResultsPage_destinaton()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Date From [Search Again Frame]- Results Page</td>");
				PrintWriter.append("<td>"+getDateFrom+"</td>");
							
				if(getDateFrom.equals(activitydetails.getResultsPage_dateFrom())){
						
					PrintWriter.append("<td>"+activitydetails.getResultsPage_dateFrom()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+activitydetails.getResultsPage_dateFrom()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Date To [Search Again Frame]- Results Page</td>");
				PrintWriter.append("<td>"+getDateTo+"</td>");
							
				if(getDateTo.equals(activitydetails.getResultsPage_dateTo())){
						
					PrintWriter.append("<td>"+activitydetails.getResultsPage_dateTo()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+activitydetails.getResultsPage_dateTo()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Adult count [Search Again Frame]- Results Page</td>");
				PrintWriter.append("<td>"+search.getAdults()+"</td>");
							
				if(search.getAdults().equals(activitydetails.getResultsPage_adults())){
						
					PrintWriter.append("<td>"+activitydetails.getResultsPage_adults()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+activitydetails.getResultsPage_adults()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Child count [Search Again Frame]- Results Page</td>");
				PrintWriter.append("<td>"+search.getChildren()+"</td>");
							
				if(search.getChildren().equals(activitydetails.getResultsPage__childs())){
						
					PrintWriter.append("<td>"+activitydetails.getResultsPage__childs()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+activitydetails.getResultsPage__childs()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				////
				
				String childAges = search.getAgeOfChildren();
				String[] partsEA = childAges.split("#");
				
				List<String> valueList = new ArrayList<String>();
				
				for (String value : partsEA) {
					valueList.add(value);
				}
				
				for (int i = 0; i < valueList.size(); i++) {
					
					testCaseCount++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Child ages check - [Child "+(i+1)+"] [Search Again Frame]- Results Page]</td>");
					PrintWriter.append("<td>"+valueList.get(i)+"</td>");
					
					if(valueList.get(i).equals(activitydetails.getResultsPage_ageOfChilds().get(i))){
						
						PrintWriter.append("<td>"+activitydetails.getResultsPage_ageOfChilds().get(i)+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					}
					else{
						PrintWriter.append("<td>"+activitydetails.getResultsPage_ageOfChilds().get(i)+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
					}
				}
				
				
				
				
				
				
				
				/////
				
				PrintWriter.append("<tr><td class='fontiiii'>Results Page - [Continue]</td><tr>");
				
				
				
				/*boolean equalsString = activitydetails.getBeforeSorting().equals(activitydetails.getAfterSorting());
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Results display order - (Lowest to Highest)</td>");
				PrintWriter.append("<td>"+activitydetails.getAfterSorting()+"</td>");
							
				try {
					
					
					if(equalsString == true){
							
						PrintWriter.append("<td>"+activitydetails.getBeforeSorting()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getBeforeSorting()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
				} catch (Exception e1) {
					PrintWriter.append("<td>"+e1.getMessage()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}*/
				
				
				if (activitydetails.isAddToCart() == true) {
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Sub Total - Results Page</td>");
					PrintWriter.append("<td>"+ResultsPage_SubTotal+"</td>");
								
					if(Integer.toString(ResultsPage_SubTotal).equals(activitydetails.getResultsPage_SubTotal())){
							
						PrintWriter.append("<td>"+activitydetails.getResultsPage_SubTotal()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getResultsPage_SubTotal()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCount ++;
					String sTC = "Sub Total ("+search.getSellingCurrency()+") :";
					
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Sub Total [Currency] - Results Page</td>");
					PrintWriter.append("<td>"+sTC+"</td>");
								
					if(activitydetails.getResultsPage_SubTotal_Currency().toLowerCase().equals(sTC.toLowerCase())){
							
						PrintWriter.append("<td>"+activitydetails.getResultsPage_SubTotal_Currency()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getResultsPage_SubTotal_Currency()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					////
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Tax / Service Fee - Results Page</td>");
					PrintWriter.append("<td>"+ResultsPage_Taxes+"</td>");
								
					if(Integer.toString(ResultsPage_Taxes).equals(activitydetails.getResultsPage_Taxes())){
							
						PrintWriter.append("<td>"+activitydetails.getResultsPage_Taxes()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getResultsPage_Taxes()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCount ++;
					String taxServicefee = "Taxes / Service Fee ("+search.getSellingCurrency()+") :";
					
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Tax / Service Fee [Currency] - Results Page</td>");
					PrintWriter.append("<td>"+taxServicefee+"</td>");
								
					if(activitydetails.getResultsPage_Taxes_Currency().toLowerCase().equals(taxServicefee.toLowerCase())){
							
						PrintWriter.append("<td>"+activitydetails.getResultsPage_SubTotal_Currency()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getResultsPage_SubTotal_Currency()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					////
				
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Payable - Results Page</td>");
					PrintWriter.append("<td>"+ResultsPage_TotalPayable+"</td>");
								
					if(Integer.toString(ResultsPage_TotalPayable).equals(activitydetails.getResultsPage_TotalPayable())){
							
						PrintWriter.append("<td>"+activitydetails.getResultsPage_TotalPayable()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getResultsPage_TotalPayable()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCount ++;
					String totalPay = "Total Payable ("+search.getSellingCurrency()+") :";
					
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Payable [Currency] - Results Page</td>");
					PrintWriter.append("<td>"+totalPay+"</td>");
								
					if(activitydetails.getResultsPage_TotalPayable_Currency().toLowerCase().equals(totalPay.toLowerCase())){
							
						PrintWriter.append("<td>"+activitydetails.getResultsPage_TotalPayable_Currency()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getResultsPage_TotalPayable_Currency()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					////
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity city mapping - Results Page</td>");
					PrintWriter.append("<td>"+search.getDestination()+"</td>");
								
					if(search.getDestination().toLowerCase().contains(activitydetails.getResultsPage_destinaton().toLowerCase())){
							
						PrintWriter.append("<td>"+activitydetails.getResultsPage_destinaton()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getResultsPage_destinaton()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					DateFormat changedfTo = new SimpleDateFormat("MM/dd/yyyy");
					SimpleDateFormat changeformatInTo = new SimpleDateFormat("MM/d/yyyy");
					Date changeinstanceFrom = changeformatInTo.parse(search.getDateFrom());  		
					String getDateFromC = changedfTo.format(changeinstanceFrom);
					
					Date changeinstanceTo = changeformatInTo.parse(search.getDateTo());  		
					String getDateToC = changedfTo.format(changeinstanceTo);
					
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Date From - Results Page</td>");
					PrintWriter.append("<td>"+getDateFromC+"</td>");
								
					if(getDateFromC.equals(activitydetails.getResultsPage_dateFrom())){
							
						PrintWriter.append("<td>"+activitydetails.getResultsPage_dateFrom()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getResultsPage_dateFrom()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Date To - Results Page</td>");
					PrintWriter.append("<td>"+getDateToC+"</td>");
								
					if(getDateToC.equals(activitydetails.getResultsPage_dateTo())){
							
						PrintWriter.append("<td>"+activitydetails.getResultsPage_dateTo()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getResultsPage_dateTo()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Adult count - Results Page</td>");
					PrintWriter.append("<td>"+search.getAdults()+"</td>");
								
					if(search.getAdults().equals(activitydetails.getResultsPage_adults())){
							
						PrintWriter.append("<td>"+activitydetails.getResultsPage_adults()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getResultsPage_adults()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Child count - Results Page</td>");
					PrintWriter.append("<td>"+search.getChildren()+"</td>");
								
					if(search.getChildren().equals(activitydetails.getResultsPage__childs())){
							
						PrintWriter.append("<td>"+activitydetails.getResultsPage__childs()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getResultsPage__childs()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					////
					
					String childAgess = search.getAgeOfChildren();
					String[] partsEAs = childAgess.split("#");
					
					List<String> valueLists = new ArrayList<String>();
					
					for (String value : partsEAs) {
						valueLists.add(value);
					}
					
					for (int i = 0; i < valueLists.size(); i++) {
						
						testCaseCount++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Child ages check - [Child "+(i+1)+" - Results Page]</td>");
						PrintWriter.append("<td>"+valueLists.get(i)+"</td>");
						
						if(valueLists.get(i).equals(activitydetails.getResultsPage_ageOfChilds().get(i))){
							
							PrintWriter.append("<td>"+activitydetails.getResultsPage_ageOfChilds().get(i)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
						}
						else{
							PrintWriter.append("<td>"+activitydetails.getResultsPage_ageOfChilds().get(i)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
						}
					}
					
					////
					
					PrintWriter.append("<tr><td class='fontiiii'>View Full Details Frame</td><tr>"); 
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Name - View Full Details </td>");
					PrintWriter.append("<td>"+inventory.getActivityName()+"</td>");
								
					if(inventory.getActivityName().toLowerCase().contains(activitydetails.getViewFullDetails_ActivityName().toLowerCase())){
							
						PrintWriter.append("<td>"+activitydetails.getViewFullDetails_ActivityName()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {
						
						PrintWriter.append("<td>"+activitydetails.getViewFullDetails_ActivityName()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					/////
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Currency_1 - View Full Details </td>");
					PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
								
					if(search.getSellingCurrency().toLowerCase().equals(activitydetails.getViewFullDetails_Currency1().toLowerCase())){
							
						PrintWriter.append("<td>"+activitydetails.getViewFullDetails_Currency1()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {
						
						PrintWriter.append("<td>"+activitydetails.getViewFullDetails_Currency1()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Currency_2 - View Full Details </td>");
					PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
								
					if(search.getSellingCurrency().toLowerCase().equals(activitydetails.getViewFullDetails_Currency2().toLowerCase())){
							
						PrintWriter.append("<td>"+activitydetails.getViewFullDetails_Currency2()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {
						
						PrintWriter.append("<td>"+activitydetails.getViewFullDetails_Currency2()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					////
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Sub Total - View Full Details</td>");
					PrintWriter.append("<td>"+PaymentPage_SubTotal+"</td>");
								
					if(Integer.toString(PaymentPage_SubTotal).equals(activitydetails.getViewFullDetails_SubTotal())){
							
						PrintWriter.append("<td>"+activitydetails.getViewFullDetails_SubTotal()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getViewFullDetails_SubTotal()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Tax - View Full Details</td>");
					PrintWriter.append("<td>"+PaymentPage_Taxes+"</td>");
								
					if(Integer.toString(PaymentPage_Taxes).equals(activitydetails.getViewFullDetails_Taxes())){
							
						PrintWriter.append("<td>"+activitydetails.getViewFullDetails_Taxes()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getViewFullDetails_Taxes()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Activity Booking Value - View Full Details</td>");
					PrintWriter.append("<td>"+ResultsPage_TotalPayable+"</td>");
								
					if(Integer.toString(ResultsPage_TotalPayable).equals(activitydetails.getViewFullDetails_TotalPayable())){
							
						PrintWriter.append("<td>"+activitydetails.getViewFullDetails_TotalPayable()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getViewFullDetails_TotalPayable()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					///
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Gross Package Booking Value - View Full Details</td>");
					PrintWriter.append("<td>"+PaymentPage_TotalGrossBookingValue+"</td>");
								
					if(Integer.toString(PaymentPage_TotalGrossBookingValue).equals(activitydetails.getViewFullDetails_TotalGrossBookingValue())){
							
						PrintWriter.append("<td>"+activitydetails.getViewFullDetails_TotalGrossBookingValue()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getViewFullDetails_TotalGrossBookingValue()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Taxes And Other Charges - View Full Details</td>");
					PrintWriter.append("<td>"+PaymentPage_Taxes+"</td>");
								
					if(Integer.toString(PaymentPage_Taxes).equals(activitydetails.getViewFullDetails_TotalTax())){
							
						PrintWriter.append("<td>"+activitydetails.getViewFullDetails_TotalTax()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getViewFullDetails_TotalTax()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Booking Value - View Full Details</td>");
					PrintWriter.append("<td>"+ResultsPage_TotalPayable+"</td>");
								
					if(Integer.toString(ResultsPage_TotalPayable).equals(activitydetails.getViewFullDetails_TotalPackageValue())){
							
						PrintWriter.append("<td>"+activitydetails.getViewFullDetails_TotalPackageValue()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getViewFullDetails_TotalPackageValue()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Amount being Processed Now - View Full Details</td>");
					PrintWriter.append("<td>"+ResultsPage_TotalPayable+"</td>");
								
					if(Integer.toString(ResultsPage_TotalPayable).equals(activitydetails.getViewFullDetails_AmountProcessed())){
							
						PrintWriter.append("<td>"+activitydetails.getViewFullDetails_AmountProcessed()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getViewFullDetails_AmountProcessed()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Amount due at Check-In - View Full Details</td>");
					PrintWriter.append("<td>"+PaymentPage_AmountCheckIn+"</td>");
								
					if(Integer.toString(PaymentPage_AmountCheckIn).equals(activitydetails.getViewFullDetails_AmountCheckIn())){
							
						PrintWriter.append("<td>"+activitydetails.getViewFullDetails_AmountCheckIn()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getViewFullDetails_AmountCheckIn()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					
				/////  Payment Page
					
					PrintWriter.append("<tr><td class='fontiiii'>Payment Page</td><tr>"); 
					
							
					if (inventory.getInventoryType().toLowerCase().contains("request")) {
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Booking Status - Payment Page </td>");
						PrintWriter.append("<td>"+inventory.getInventoryType()+"</td>");
						
						if(activitydetails.getResultsPage_BookingStatus().toLowerCase().contains(inventory.getInventoryType().split(" ")[1].toLowerCase())){
							
							PrintWriter.append("<td>"+activitydetails.getResultsPage_BookingStatus()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+activitydetails.getResultsPage_BookingStatus()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
					}
					
					else {
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Booking Status - Payment Page </td>");
						PrintWriter.append("<td>Available to book</td>");
						
						if(activitydetails.getResultsPage_BookingStatus().toLowerCase().contains("Available".toLowerCase())){
							
							PrintWriter.append("<td>"+activitydetails.getResultsPage_BookingStatus()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+activitydetails.getResultsPage_BookingStatus()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
					}
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Name - Payment Page</td>");
					PrintWriter.append("<td>"+inventory.getActivityName()+"</td>");
								
					if(activitydetails.getPaymentPage_ActivityName().toLowerCase().contains(inventory.getActivityName().split(" - ")[0].toLowerCase())){
							
						PrintWriter.append("<td>"+activitydetails.getPaymentPage_ActivityName()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {
						
						PrintWriter.append("<td>"+activitydetails.getPaymentPage_ActivityName()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					testCaseCount ++;
					payent_qty = Integer.parseInt(search.getAdults()) + Integer.parseInt(search.getChildren());
					int qty = Integer.parseInt(activitydetails.getResultsPage_QTY());
					
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity qty - Payment Page</td>");
					PrintWriter.append("<td>"+payent_qty+"</td>");
								
					if(qty == payent_qty){
							
						PrintWriter.append("<td>"+activitydetails.getResultsPage_QTY()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getResultsPage_QTY()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					////
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Currency_1 - Payment Page </td>");
					PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
								
					if(search.getSellingCurrency().toLowerCase().equals(activitydetails.getPaymentPage_Currency_1().toLowerCase())){
							
						PrintWriter.append("<td>"+activitydetails.getPaymentPage_Currency_1()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {
						
						PrintWriter.append("<td>"+activitydetails.getPaymentPage_Currency_1()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Currency_2 - Payment Page </td>");
					PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
								
					if(search.getSellingCurrency().toLowerCase().equals(activitydetails.getPaymentPage_Currency_2().toLowerCase())){
							
						PrintWriter.append("<td>"+activitydetails.getPaymentPage_Currency_2()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {
						
						PrintWriter.append("<td>"+activitydetails.getPaymentPage_Currency_2()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					////
					
					int disValue = 0;
					discountedVale = 0;
					
					if (inventory.getDiscountYesORNo().equalsIgnoreCase("Yes")) {
						
						if (PG_Properties.getProperty("Discount_Type").equalsIgnoreCase("percentage")) {
							
							disValue = Integer.parseInt(PG_Properties.getProperty("Discount_Value"));
							discountedVale = ((PaymentPage_SubTotal * disValue)/100);							
						}
						
						if (PG_Properties.getProperty("Discount_Type").equalsIgnoreCase("value")) {			
							
							disValue = Integer.parseInt(PG_Properties.getProperty("Discount_Value"));
							discountedVale = PaymentPage_SubTotal + disValue;
						}
					
					}
					
					
					//Quotation Page
					getQuotationDetails();
					
					
					
					
					////
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Sub Total - Payment Page</td>");
					PrintWriter.append("<td>"+PaymentPage_SubTotal+"</td>");
								
					if(Integer.toString(PaymentPage_SubTotal).equals(activitydetails.getPaymentPage_subTotal())){
							
						PrintWriter.append("<td>"+activitydetails.getPaymentPage_subTotal()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getPaymentPage_subTotal()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					//
					
					if (inventory.getDiscountYesORNo().equalsIgnoreCase("Yes")) {
												
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Discount 1 - Payment Page</td>");
						PrintWriter.append("<td>"+discountedVale+"</td>");
									
						if(activitydetails.getPaymentsPage_DiscountValue1().contains(Integer.toString((discountedVale)))){
								
							PrintWriter.append("<td>"+activitydetails.getPaymentsPage_DiscountValue1()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+activitydetails.getPaymentsPage_DiscountValue1()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Discount 2 - Payment Page</td>");
						PrintWriter.append("<td>"+discountedVale+"</td>");
									
						if(activitydetails.getPaymentsPage_DiscountValue2().contains(Integer.toString((discountedVale)))){
								
							PrintWriter.append("<td>"+activitydetails.getPaymentsPage_DiscountValue2()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+activitydetails.getPaymentsPage_DiscountValue2()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
					}
					
					
					//
					
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Tax - Payment Page</td>");
					PrintWriter.append("<td>"+PaymentPage_Taxes+"</td>");
								
					if(Integer.toString(PaymentPage_Taxes).equals(activitydetails.getPaymentPage_TotalTax())){
							
						PrintWriter.append("<td>"+activitydetails.getPaymentPage_TotalTax()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getPaymentPage_TotalTax()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Activity Booking Value - Payment Page</td>");
					PrintWriter.append("<td>"+PaymentPage_TotalPayable+"</td>");
								
					if(Integer.toString(PaymentPage_TotalPayable).equals(activitydetails.getPaymentPage_TotalActivityBookingValue())){
							
						PrintWriter.append("<td>"+activitydetails.getPaymentPage_TotalActivityBookingValue()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getPaymentPage_TotalActivityBookingValue()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					///
				
					if (UserTypeDC_B2B == TourOperatorType.COMCASH || UserTypeDC_B2B == TourOperatorType.COMCREDITLPONO || UserTypeDC_B2B == TourOperatorType.COMCREDITLPOY) {
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>TO Agent Commission - Payment Page</td>");
						PrintWriter.append("<td>"+toAgentCommission+"</td>");
									
						if(Integer.toString(toAgentCommission).equals(activitydetails.getPaymentsPage_AgentCommission())){
								
							PrintWriter.append("<td>"+activitydetails.getPaymentsPage_AgentCommission()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+activitydetails.getPaymentsPage_AgentCommission()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
					
					}
								
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Gross Package Booking Value - Payment Page</td>");
					PrintWriter.append("<td>"+PaymentPage_TotalGrossBookingValue+"</td>");
								
					if(Integer.toString(PaymentPage_TotalGrossBookingValue).equals(activitydetails.getPaymentPageGrossBookingValue())){
							
						PrintWriter.append("<td>"+activitydetails.getPaymentPageGrossBookingValue()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getPaymentPageGrossBookingValue()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					if (activitydetails.getPaymentDetails().equals("Pay Online")) {
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Taxes And Other Charges - Payment Page</td>");
						PrintWriter.append("<td>"+PaymentPage_TotalTax+"</td>");
									
						if(Integer.toString(PaymentPage_TotalTax).equals(activitydetails.getPaymentPageTax())){
								
							PrintWriter.append("<td>"+activitydetails.getPaymentPageTax()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+activitydetails.getPaymentPageTax()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
												
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Booking Value - Payment Page</td>");
						PrintWriter.append("<td>"+PaymentPage_TotalPackageValue+"</td>");
									
						if(Integer.toString(PaymentPage_TotalPackageValue).equals(activitydetails.getPaymentPageTotalBookingValue())){
								
							PrintWriter.append("<td>"+activitydetails.getPaymentPageTotalBookingValue()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+activitydetails.getPaymentPageTotalBookingValue()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Amount being Processed Now - Payment Page</td>");
						PrintWriter.append("<td>"+PaymentPage_AmountProcessed+"</td>");
									
						if(Integer.toString(PaymentPage_AmountProcessed).equals(activitydetails.getPaymentPageAmountbeingProcessed())){
								
							PrintWriter.append("<td>"+activitydetails.getPaymentPageAmountbeingProcessed()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+activitydetails.getPaymentPageAmountbeingProcessed()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
					} else {
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Taxes And Other Charges - Payment Page</td>");
						PrintWriter.append("<td>"+(PaymentPage_TotalTax - PaymentPage_CreditCardFee)+"</td>");
									
						if(Integer.toString(PaymentPage_TotalTax - PaymentPage_CreditCardFee).equals(activitydetails.getPaymentPageTax())){
								
							PrintWriter.append("<td>"+activitydetails.getPaymentPageTax()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+activitydetails.getPaymentPageTax()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
												
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Booking Value - Payment Page</td>");
						PrintWriter.append("<td>"+(PaymentPage_TotalPackageValue - PaymentPage_CreditCardFee)+"</td>");
									
						if(Integer.toString(PaymentPage_TotalPackageValue - PaymentPage_CreditCardFee).equals(activitydetails.getPaymentPageTotalBookingValue())){
								
							PrintWriter.append("<td>"+activitydetails.getPaymentPageTotalBookingValue()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+activitydetails.getPaymentPageTotalBookingValue()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Amount being Processed Now - Payment Page</td>");
						PrintWriter.append("<td>"+(PaymentPage_TotalPackageValue - PaymentPage_CreditCardFee)+"</td>");
									
						if(Integer.toString(PaymentPage_TotalPackageValue - PaymentPage_CreditCardFee).equals(activitydetails.getPaymentPageAmountbeingProcessed())){
								
							PrintWriter.append("<td>"+activitydetails.getPaymentPageAmountbeingProcessed()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+activitydetails.getPaymentPageAmountbeingProcessed()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}

					}
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Amount due at Check-In - Payment Page</td>");
					PrintWriter.append("<td>"+PaymentPage_AmountCheckIn+"</td>");
								
					if(Integer.toString(PaymentPage_AmountCheckIn).equals(activitydetails.getPaymentPageAmountduatCheckIn())){
							
						PrintWriter.append("<td>"+activitydetails.getPaymentPageAmountduatCheckIn()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getPaymentPageAmountduatCheckIn()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					if (activitydetails.getPaymentDetails().equals("Pay Online")) {
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Payment Page [Online Payment] - Currency</td>");
						PrintWriter.append("<td>"+PG_Properties.getProperty("PortalCurrency")+"</td>");
									
						if(PG_Properties.getProperty("PortalCurrency").equals(activitydetails.getPayOnline_MainTotalBookingCurrency())){
								
							PrintWriter.append("<td>"+activitydetails.getPayOnline_MainTotalBookingCurrency()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+activitydetails.getPayOnline_MainTotalBookingCurrency()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						///////
						
						
						if((PG_Properties.getProperty("PortalCurrency").toLowerCase().equals(inventory.getSupplierCurrency().toLowerCase()))){
							
							
							testCaseCount ++;
							PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Payment Page [Online Payment] - Main Total Booking Value</td>");
							PrintWriter.append("<td>"+PaymentPage_AmountProcessed+"</td>");
										
							if(Integer.toString(PaymentPage_AmountProcessed).equals(activitydetails.getPayOnline_MainTotalBookingValue())){
									
								PrintWriter.append("<td>"+activitydetails.getPayOnline_MainTotalBookingValue()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+activitydetails.getPayOnline_MainTotalBookingValue()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
							testCaseCount ++;
							PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Payment Page [Online Payment] - Total Gross Package Booking Value</td>");
							PrintWriter.append("<td>"+PaymentPage_SubTotal+"</td>");
										
							if(Integer.toString(PaymentPage_SubTotal).equals(activitydetails.getPayOnline_GrossValue())){
									
								PrintWriter.append("<td>"+activitydetails.getPayOnline_GrossValue()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+activitydetails.getPayOnline_GrossValue()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
							testCaseCount ++;
							PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Payment Page [Online Payment] - Total Taxes and Fees</td>");
							PrintWriter.append("<td>"+PaymentPage_TotalTax+"</td>");
										
							if(Integer.toString(PaymentPage_TotalTax).equals(activitydetails.getPayOnline_TotalTax())){
									
								PrintWriter.append("<td>"+activitydetails.getPayOnline_TotalTax()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+activitydetails.getPayOnline_TotalTax()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
							testCaseCount ++;
							PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Payment Page [Online Payment] - Total Package Booking Value</td>");
							PrintWriter.append("<td>"+PaymentPage_TotalPackageValue+"</td>");
										
							if(Integer.toString(PaymentPage_TotalPackageValue).equals(activitydetails.getPayOnline_TotalBooking())){
									
								PrintWriter.append("<td>"+activitydetails.getPayOnline_TotalBooking()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+activitydetails.getPayOnline_TotalBooking()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
							testCaseCount ++;
							PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Payment Page [Online Payment] - Amount being Processed Now</td>");
							PrintWriter.append("<td>"+PaymentPage_AmountProcessed+"</td>");
										
							if(Integer.toString(PaymentPage_AmountProcessed).equals(activitydetails.getPayOnline_AmountProNow())){
									
								PrintWriter.append("<td>"+activitydetails.getPayOnline_AmountProNow()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+activitydetails.getPayOnline_AmountProNow()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
							testCaseCount ++;
							PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Payment Page [Online Payment] - Amount Due at Check-In</td>");
							PrintWriter.append("<td>"+inventory.getPaymentPage_AmountCheckIn()+"</td>");
										
							if(inventory.getPaymentPage_AmountCheckIn().equals(activitydetails.getPayOnline_Due())){
									
								PrintWriter.append("<td>"+activitydetails.getPayOnline_Due()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+activitydetails.getPayOnline_Due()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
							testCaseCount ++;
							PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Payment Gateway [Online Payment] - Amount</td>");
							PrintWriter.append("<td>"+PaymentPage_AmountProcessed+"</td>");
										
							if(Integer.toString(PaymentPage_AmountProcessed).equals(activitydetails.getPaymentGatewayTotalAmount())){
									
								PrintWriter.append("<td>"+activitydetails.getPaymentGatewayTotalAmount()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+activitydetails.getPaymentGatewayTotalAmount()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
						
						}else{
						
							for(Entry<String, String> entry: currencyMap.entrySet()) {
								if(inventory.getSupplierCurrency().toLowerCase().equals(entry.getKey().toLowerCase())){
									
									double paymentOnlineUSD = Double.parseDouble(entry.getValue());
									
									int PaymentOnline_MainValue = 0;
									int PaymentOnline_TotalGrossBookingValue = 0;
									int PaymentOnline_TotalTax = 0;
									int PaymentOnline_TotalPackageValue = 0;
									int PaymentOnline_AmountProcessed = 0;
									int PaymentOnline_AmountCheckIn = 0;
									
									if (!(UserTypeDC_B2B == TourOperatorType.DC)) {
																				
										if (UserTypeDC_B2B == TourOperatorType.COMCASH || UserTypeDC_B2B == TourOperatorType.COMCREDITLPONO || UserTypeDC_B2B == TourOperatorType.COMCREDITLPOY) {
											
											PaymentOnline_MainValue =  (int) Math.ceil((( Double.parseDouble(activitydetails.getToActivity_TotalTax()) + Double.parseDouble(inventory.getCreditCard_Fee()) + (Double.parseDouble(activitydetails.getToActivity_SubTotal())) - Double.parseDouble(activitydetails.getToAgentCommission()) ) / paymentOnlineUSD ));						
											PaymentOnline_TotalGrossBookingValue =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_SubTotal()) / paymentOnlineUSD ));
											PaymentOnline_TotalTax =  (int) Math.ceil(((Double.parseDouble(activitydetails.getToActivity_TotalTax()) + Double.parseDouble(inventory.getCreditCard_Fee()) ) / paymentOnlineUSD ));
											PaymentOnline_TotalPackageValue =  (int) Math.ceil((( Double.parseDouble(activitydetails.getToActivity_TotalTax()) + Double.parseDouble(inventory.getCreditCard_Fee()) + (Double.parseDouble(activitydetails.getToActivity_SubTotal())) - Double.parseDouble(activitydetails.getToAgentCommission()) ) / paymentOnlineUSD ));
											PaymentOnline_AmountProcessed =  (int) Math.ceil((( Double.parseDouble(activitydetails.getToActivity_TotalTax()) + Double.parseDouble(inventory.getCreditCard_Fee()) + (Double.parseDouble(activitydetails.getToActivity_SubTotal())) - Double.parseDouble(activitydetails.getToAgentCommission()) ) / paymentOnlineUSD ));
											PaymentOnline_AmountCheckIn =  (int) Math.ceil((Double.parseDouble(inventory.getPaymentPage_AmountCheckIn()) / paymentOnlineUSD ));
																					
										}
										
										if (UserTypeDC_B2B == TourOperatorType.NETCASH || UserTypeDC_B2B == TourOperatorType.NETCREDITLPON || UserTypeDC_B2B == TourOperatorType.NETCREDITLPOY) {
										
											PaymentOnline_MainValue =  (int) Math.ceil((( Double.parseDouble(activitydetails.getToActivity_TotalTax()) + Double.parseDouble(inventory.getCreditCard_Fee()) + (Double.parseDouble(activitydetails.getToActivity_SubTotal()) ) ) / paymentOnlineUSD ));						
											PaymentOnline_TotalGrossBookingValue =  (int) Math.ceil((Double.parseDouble(activitydetails.getToActivity_SubTotal()) / paymentOnlineUSD ));
											PaymentOnline_TotalTax =  (int) Math.ceil(((Double.parseDouble(activitydetails.getToActivity_TotalTax()) + Double.parseDouble(inventory.getCreditCard_Fee()) ) / paymentOnlineUSD ));
											PaymentOnline_TotalPackageValue =  (int) Math.ceil((( Double.parseDouble(activitydetails.getToActivity_TotalTax()) + Double.parseDouble(inventory.getCreditCard_Fee()) + (Double.parseDouble(activitydetails.getToActivity_SubTotal()) ) ) / paymentOnlineUSD ));
											PaymentOnline_AmountProcessed =  (int) Math.ceil((( Double.parseDouble(activitydetails.getToActivity_TotalTax()) + Double.parseDouble(inventory.getCreditCard_Fee()) + (Double.parseDouble(activitydetails.getToActivity_SubTotal()) ) ) / paymentOnlineUSD ));
											PaymentOnline_AmountCheckIn =  (int) Math.ceil((Double.parseDouble(inventory.getPaymentPage_AmountCheckIn()) / paymentOnlineUSD ));
																					
										}
										
									
									}else{
										
										PaymentOnline_MainValue =  (int) Math.ceil((Double.parseDouble(inventory.getPaymentPage_TotalPackageValue()) / paymentOnlineUSD ));						
										PaymentOnline_TotalGrossBookingValue =  (int) Math.ceil((Double.parseDouble(inventory.getPaymentPage_TotalGrossBookingValue()) / paymentOnlineUSD ));
										PaymentOnline_TotalTax =  (int) Math.ceil((Double.parseDouble(inventory.getPaymentPage_TotalTax()) / paymentOnlineUSD ));
										PaymentOnline_TotalPackageValue =  (int) Math.ceil((Double.parseDouble(inventory.getPaymentPage_TotalPackageValue()) / paymentOnlineUSD ));
										PaymentOnline_AmountProcessed =  (int) Math.ceil((Double.parseDouble(inventory.getPaymentPage_AmountProcessed()) / paymentOnlineUSD ));
										PaymentOnline_AmountCheckIn =  (int) Math.ceil((Double.parseDouble(inventory.getPaymentPage_AmountCheckIn()) / paymentOnlineUSD ));
																				
									}
									
									
		
									testCaseCount ++;
									PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Payment Page [Online Payment] - Main Total Booking Value</td>");
									PrintWriter.append("<td>"+PaymentOnline_MainValue+"</td>");
												
									if(Integer.toString(PaymentOnline_MainValue).equals(activitydetails.getPayOnline_MainTotalBookingValue())){
											
										PrintWriter.append("<td>"+activitydetails.getPayOnline_MainTotalBookingValue()+"</td>");
										PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
									}
									
									else{
										PrintWriter.append("<td>"+activitydetails.getPayOnline_MainTotalBookingValue()+"</td>");
										PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
									}
									
									testCaseCount ++;
									PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Payment Page [Online Payment] - Total Gross Package Booking Value</td>");
									PrintWriter.append("<td>"+PaymentOnline_TotalGrossBookingValue+"</td>");
												
									if(Integer.toString(PaymentOnline_TotalGrossBookingValue).equals(activitydetails.getPayOnline_GrossValue())){
											
										PrintWriter.append("<td>"+activitydetails.getPayOnline_GrossValue()+"</td>");
										PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
									}
									
									else{
										PrintWriter.append("<td>"+activitydetails.getPayOnline_GrossValue()+"</td>");
										PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
									}
									
									testCaseCount ++;
									PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Payment Page [Online Payment] - Total Taxes and Fees</td>");
									PrintWriter.append("<td>"+PaymentOnline_TotalTax+"</td>");
												
									if(Integer.toString(PaymentOnline_TotalTax).equals(activitydetails.getPayOnline_TotalTax())){
											
										PrintWriter.append("<td>"+activitydetails.getPayOnline_TotalTax()+"</td>");
										PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
									}
									
									else{
										PrintWriter.append("<td>"+activitydetails.getPayOnline_TotalTax()+"</td>");
										PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
									}
									
									testCaseCount ++;
									PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Payment Page [Online Payment] - Total Package Booking Value</td>");
									PrintWriter.append("<td>"+PaymentOnline_TotalPackageValue+"</td>");
												
									if(Integer.toString(PaymentOnline_TotalPackageValue).equals(activitydetails.getPayOnline_TotalBooking())){
											
										PrintWriter.append("<td>"+activitydetails.getPayOnline_TotalBooking()+"</td>");
										PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
									}
									
									else{
										PrintWriter.append("<td>"+activitydetails.getPayOnline_TotalBooking()+"</td>");
										PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
									}
									
									testCaseCount ++;
									PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Payment Page [Online Payment] - Amount being Processed Now</td>");
									PrintWriter.append("<td>"+PaymentOnline_AmountProcessed+"</td>");
												
									if(Integer.toString(PaymentOnline_AmountProcessed).equals(activitydetails.getPayOnline_AmountProNow())){
											
										PrintWriter.append("<td>"+activitydetails.getPayOnline_AmountProNow()+"</td>");
										PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
									}
									
									else{
										PrintWriter.append("<td>"+activitydetails.getPayOnline_AmountProNow()+"</td>");
										PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
									}
									
									testCaseCount ++;
									PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Payment Page [Online Payment] - Amount Due at Check-In</td>");
									PrintWriter.append("<td>"+PaymentOnline_AmountCheckIn+"</td>");
												
									if(Integer.toString(PaymentOnline_AmountCheckIn).equals(activitydetails.getPayOnline_Due())){
											
										PrintWriter.append("<td>"+activitydetails.getPayOnline_Due()+"</td>");
										PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
									}
									
									else{
										PrintWriter.append("<td>"+activitydetails.getPayOnline_Due()+"</td>");
										PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
									}
									
									testCaseCount ++;
									PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Payment Gateway [Online Payment] - Amount</td>");
									PrintWriter.append("<td>"+PaymentOnline_TotalPackageValue+"</td>");
												
									if(Integer.toString(PaymentOnline_TotalPackageValue).equals(activitydetails.getPaymentGatewayTotalAmount())){
											
										PrintWriter.append("<td>"+activitydetails.getPaymentGatewayTotalAmount()+"</td>");
										PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
									}
									
									else{
										PrintWriter.append("<td>"+activitydetails.getPaymentGatewayTotalAmount()+"</td>");
										PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
									}
								
								}
							}				
						}				
					}
					
					
				
					
					//// Confirmation Page
					
					PrintWriter.append("<tr><td class='fontiiii'>Confirmation Page</td><tr>"); 	
					
					if (inventory.getInventoryType().toLowerCase().contains("request")) {
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Booking Status - Confirmation Page </td>");
						PrintWriter.append("<td>"+inventory.getInventoryType()+"</td>");
						
						if(activitydetails.getBookingStatus().toLowerCase().contains(inventory.getInventoryType().split(" ")[1].toLowerCase())){
							
							PrintWriter.append("<td>"+activitydetails.getBookingStatus()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+activitydetails.getBookingStatus()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
					} else {
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Booking Status - Confirmation Page </td>");
						PrintWriter.append("<td>"+activitydetails.getBookingStatus()+"</td>");
						
						if(activitydetails.getBookingStatus().toLowerCase().equals("Confirmed".toLowerCase())){
							
							PrintWriter.append("<td>"+activitydetails.getBookingStatus()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+activitydetails.getBookingStatus()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
					}
					
				
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Name - Confirmation Page</td>");
					PrintWriter.append("<td>"+inventory.getActivityName()+"</td>");
								
					if(inventory.getActivityName().toLowerCase().contains(activitydetails.getBookingPageActivityName().toLowerCase())){
							
						PrintWriter.append("<td>"+activitydetails.getBookingPageActivityName()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {
						
						PrintWriter.append("<td>"+activitydetails.getBookingPageActivityName()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Currency - Confirmation Page </td>");
					PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
								
					if(search.getSellingCurrency().toLowerCase().equals(activitydetails.getConfirmationPage_Currency().toLowerCase())){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmationPage_Currency()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {
						
						PrintWriter.append("<td>"+activitydetails.getConfirmationPage_Currency()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					testCaseCount ++;
					int confirm_qty = Integer.parseInt(search.getAdults()) + Integer.parseInt(search.getChildren());
					int C_qty = Integer.parseInt(activitydetails.getConfirmationPage_QTY());
					
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity qty - Confirmation Page</td>");
					PrintWriter.append("<td>"+confirm_qty+"</td>");
								
					if(C_qty == confirm_qty){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmationPage_QTY()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getConfirmationPage_QTY()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					////
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Sub Total - Confirmation Page</td>");
					PrintWriter.append("<td>"+ConfirmationPage_SubTotal+"</td>");
								
					if(Integer.toString(ConfirmationPage_SubTotal).equals(activitydetails.getConfirmationPage_grossValue())){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmationPage_grossValue()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getConfirmationPage_grossValue()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					///
					if (inventory.getDiscountYesORNo().equalsIgnoreCase("Yes")) {
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Discount - Confirmation Page</td>");
						PrintWriter.append("<td>"+discountedVale+"</td>");
									
						if(activitydetails.getConfirmationPage_DiscountValue1().contains(Integer.toString((discountedVale)))){
								
							PrintWriter.append("<td>"+activitydetails.getConfirmationPage_DiscountValue1()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+activitydetails.getConfirmationPage_DiscountValue1()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						
						
					}
					
					
					
					///
					
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Tax - Confirmation Page</td>");
					PrintWriter.append("<td>"+ConfirmationPage_Taxes+"</td>");
								
					if(Integer.toString(ConfirmationPage_Taxes).equals(activitydetails.getConfirmationPage_tax())){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmationPage_tax()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getConfirmationPage_tax()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Activity Booking Value - Confirmation Page</td>");
					PrintWriter.append("<td>"+ConfirmationPage_TotalPayable+"</td>");
								
					if(Integer.toString(ConfirmationPage_TotalPayable).equals(activitydetails.getConfirmationPage_activityBookingValue())){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmationPage_activityBookingValue()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getConfirmationPage_activityBookingValue()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					///
					
					if (UserTypeDC_B2B == TourOperatorType.COMCASH || UserTypeDC_B2B == TourOperatorType.COMCREDITLPONO || UserTypeDC_B2B == TourOperatorType.COMCREDITLPOY) {
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>TO Agent Commission - Confirmation Page</td>");
						PrintWriter.append("<td>"+toAgentCommission+"</td>");
									
						if(Integer.toString(toAgentCommission).equals(activitydetails.getConfirmationPage_AgentCommission())){
								
							PrintWriter.append("<td>"+activitydetails.getConfirmationPage_AgentCommission()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+activitydetails.getConfirmationPage_AgentCommission()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
					
					}
					
			
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Gross Package Booking Value - Confirmation Page</td>");
					PrintWriter.append("<td>"+Confirmation_TotalGrossBookingValue+"</td>");
								
					if(Integer.toString(Confirmation_TotalGrossBookingValue).equals(activitydetails.getConfirmationPage_TotalGrossValue())){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmationPage_TotalGrossValue()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getConfirmationPage_TotalGrossValue()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Taxes And Other Charges - Confirmation Page</td>");
					PrintWriter.append("<td>"+ConfirmationPage_TotalTax+"</td>");
								
					if(Integer.toString(ConfirmationPage_TotalTax).equals(activitydetails.getConfirmationPage_TotalTax())){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmationPage_TotalTax()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getConfirmationPage_TotalTax()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Package Booking Value - Confirmation Page</td>");
					PrintWriter.append("<td>"+ConfirmationPage_TotalPackageValue+"</td>");
								
					if(Integer.toString(ConfirmationPage_TotalPackageValue).equals(activitydetails.getConfirmationPage_TotalPackageBookingValue())){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmationPage_TotalPackageBookingValue()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getConfirmationPage_TotalPackageBookingValue()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					
					if (activitydetails.getPaymentDetails().equals("Pay Online")) {
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Amount being Processed Now - Confirmation Page</td>");
						PrintWriter.append("<td>"+ConfirmationPage_AmountProcessed+"</td>");
									
						if(Integer.toString(ConfirmationPage_AmountProcessed).equals(activitydetails.getConfirmationPage_AmountBeingProcessed())){
								
							PrintWriter.append("<td>"+activitydetails.getConfirmationPage_AmountBeingProcessed()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+activitydetails.getConfirmationPage_AmountBeingProcessed()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
					}else{
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Amount being Processed Now - Confirmation Page</td>");
						PrintWriter.append("<td>"+ConfirmationPage_TotalPackageValue+"</td>");
									
						if(Integer.toString(ConfirmationPage_TotalPackageValue).equals(activitydetails.getConfirmationPage_AmountBeingProcessed())){
								
							PrintWriter.append("<td>"+activitydetails.getConfirmationPage_AmountBeingProcessed()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+activitydetails.getConfirmationPage_AmountBeingProcessed()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
					}
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Amount due at Check-In - Confirmation Page</td>");
					PrintWriter.append("<td>"+ConfirmationPage_AmountCheckIn+"</td>");
								
					if(Integer.toString(ConfirmationPage_AmountCheckIn).equals(activitydetails.getConfirmationPage_AmountDueAtCheckin())){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmationPage_AmountDueAtCheckin()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getConfirmationPage_AmountDueAtCheckin()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					
					/////
					
					testCaseCount ++;			
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [First Name] - Confirmation Page</td>");
					PrintWriter.append("<td>"+activitydetails.getPaymentPage_FName()+"</td>");
								
					if(activitydetails.getPaymentPage_FName().equals(activitydetails.getConfirmationPage_FName())){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmationPage_FName()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getConfirmationPage_FName()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCount ++;			
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [Last Name] - Confirmation Page</td>");
					PrintWriter.append("<td>"+activitydetails.getPaymentPage_LName()+"</td>");
								
					if(activitydetails.getPaymentPage_LName().equals(activitydetails.getConfirmationPage_LName())){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmationPage_LName()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getConfirmationPage_LName()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCount ++;			
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [TP No] - Confirmation Page</td>");
					PrintWriter.append("<td>"+activitydetails.getPaymentPage_TP()+"</td>");
								
					if(activitydetails.getPaymentPage_TP().equals(activitydetails.getConfirmationPage_TP())){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmationPage_TP()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getConfirmationPage_TP()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCount ++;			
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [Email] - Confirmation Page</td>");
					PrintWriter.append("<td>"+activitydetails.getPaymentPage_Email()+"</td>");
								
					if(activitydetails.getPaymentPage_Email().equals(activitydetails.getConfirmationPage_Email())){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmationPage_Email()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getConfirmationPage_Email()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCount ++;			
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [Address] - Confirmation Page</td>");
					PrintWriter.append("<td>"+activitydetails.getPaymentPage_address()+"</td>");
								
					if(activitydetails.getPaymentPage_address().equals(activitydetails.getConfirmationPage_address())){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmationPage_address()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getConfirmationPage_address()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCount ++;			
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [Country] - Confirmation Page</td>");
					PrintWriter.append("<td>"+activitydetails.getPaymentPage_country()+"</td>");
								
					if(activitydetails.getPaymentPage_country().equals(activitydetails.getConfirmationPage_country())){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmationPage_country()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getConfirmationPage_country()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCount ++;			
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [City] - Confirmation Page</td>");
					PrintWriter.append("<td>"+activitydetails.getPaymentPage_city()+"</td>");
								
					if(activitydetails.getPaymentPage_city().equals(activitydetails.getConfirmationPage_city())){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmationPage_city()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getConfirmationPage_city()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					if (activitydetails.getConfirmationPage_country().equals("USA")) {
						
						testCaseCount ++;			
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [State] - Confirmation Page</td>");
						PrintWriter.append("<td>"+activitydetails.getPaymentPage_state()+"</td>");
									
						if(activitydetails.getPaymentPage_state().equals(activitydetails.getConfirmationPage_state())){
								
							PrintWriter.append("<td>"+activitydetails.getConfirmationPage_state()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+activitydetails.getConfirmationPage_state()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCount ++;			
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [Postal Code] - Confirmation Page</td>");
						PrintWriter.append("<td>"+activitydetails.getPaymentPage_PostelCode()+"</td>");
									
						if(activitydetails.getPaymentPage_PostelCode().equals(activitydetails.getConfirmationPage_PostalCode())){
								
							PrintWriter.append("<td>"+activitydetails.getConfirmationPage_PostalCode()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+activitydetails.getConfirmationPage_PostalCode()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
					
					}
					
					if (activitydetails.getConfirmationPage_country().equals("Canada")) {
						
						testCaseCount ++;			
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [State] - Confirmation Page</td>");
						PrintWriter.append("<td>"+activitydetails.getPaymentPage_state()+"</td>");
									
						if(activitydetails.getPaymentPage_state().equals(activitydetails.getConfirmationPage_state())){
								
							PrintWriter.append("<td>"+activitydetails.getConfirmationPage_state()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+activitydetails.getConfirmationPage_state()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCount ++;			
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [Postal Code] - Confirmation Page</td>");
						PrintWriter.append("<td>"+activitydetails.getPaymentPage_PostelCode()+"</td>");
									
						if(activitydetails.getPaymentPage_PostelCode().equals(activitydetails.getConfirmationPage_PostalCode())){
								
							PrintWriter.append("<td>"+activitydetails.getConfirmationPage_PostalCode()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+activitydetails.getConfirmationPage_PostalCode()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
					}
					
					if (activitydetails.getConfirmationPage_country().equals("Australia")) {
						
						testCaseCount ++;			
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [State] - Confirmation Page</td>");
						PrintWriter.append("<td>"+activitydetails.getPaymentPage_state()+"</td>");
									
						if(activitydetails.getPaymentPage_state().equals(activitydetails.getConfirmationPage_state())){
								
							PrintWriter.append("<td>"+activitydetails.getConfirmationPage_state()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+activitydetails.getConfirmationPage_state()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCount ++;			
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [Postal Code] - Confirmation Page</td>");
						PrintWriter.append("<td>"+activitydetails.getPaymentPage_PostelCode()+"</td>");
									
						if(activitydetails.getPaymentPage_PostelCode().equals(activitydetails.getConfirmationPage_PostalCode())){
								
							PrintWriter.append("<td>"+activitydetails.getConfirmationPage_PostalCode()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+activitydetails.getConfirmationPage_PostalCode()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
					
					}
					
					
					
					/////
					
					for (int j = 0; j < activitydetails.getConfirmationPage_cusTitle().size() ; j++) {
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer Title check - [Customer "+(j+1)+"]</td>");
						PrintWriter.append("<td>"+activitydetails.getConfirmationPage_cusTitle().get(j)+"</td>");
						
						if(activitydetails.getConfirmationPage_cusTitle().get(j).equals(activitydetails.getResultsPage_cusTitle().get(j))){
							
							PrintWriter.append("<td>"+activitydetails.getConfirmationPage_cusTitle().get(j)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
						}
						else{
							PrintWriter.append("<td>"+activitydetails.getConfirmationPage_cusTitle().get(j)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
						}
					}
					
					for (int j = 0; j < activitydetails.getConfirmationPage_cusFName().size() ; j++) {
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer First Name check - [Customer "+(j+1)+"]</td>");
						PrintWriter.append("<td>"+activitydetails.getConfirmationPage_cusFName().get(j)+"</td>");
						
						if(activitydetails.getConfirmationPage_cusFName().get(j).equals(activitydetails.getResultsPage_cusFName().get(j))){
							
							PrintWriter.append("<td>"+activitydetails.getConfirmationPage_cusFName().get(j)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
						}
						else{
							PrintWriter.append("<td>"+activitydetails.getConfirmationPage_cusFName().get(j)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
						}
					}
					
					for (int j = 0; j < activitydetails.getConfirmationPage_cusLName().size() ; j++) {
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer Last Name check - [Customer "+(j+1)+"]</td>");
						PrintWriter.append("<td>"+activitydetails.getConfirmationPage_cusLName().get(j)+"</td>");
						
						if(activitydetails.getConfirmationPage_cusLName().get(j).equals(activitydetails.getResultsPage_cusLName().get(j))){
							
							PrintWriter.append("<td>"+activitydetails.getConfirmationPage_cusLName().get(j)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
						}
						else{
							PrintWriter.append("<td>"+activitydetails.getConfirmationPage_cusLName().get(j)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
						}
					}
					
					////
					
					int bufferDates = Integer.parseInt(inventory.getCancellation_Apply()) + Integer.parseInt(inventory.getCancellation_Buffer());
					
					String DateTo = search.getDateTo();
					String activitySelectDate = search.getActivityDate();
					DateFormat dfTo = new SimpleDateFormat("dd-MMM-yyyy");
					SimpleDateFormat formatInTo = new SimpleDateFormat("MM/dd/yyyy");
					Date instanceTo = formatInTo.parse(DateTo);  		
					String reportDateTo = dfTo.format(instanceTo);  //DateTo   
					
					String DateFrom = search.getDateFrom();
					Date instance = formatInTo.parse(DateFrom);  		
					String reportDateFrom = dfTo.format(instance);  //Datefrom
					
					Date instanceMainPol = dfTo.parse(activitySelectDate); 
					Calendar calMainPolicy = Calendar.getInstance();
					calMainPolicy.setTime(instanceMainPol);
					calMainPolicy.add(Calendar.DATE, -1);			
					dateWithBufferDates_1 = dfTo.format(calMainPolicy.getTime());  // DateTo - 1
					
					Date instanceMainPolicy3 = dfTo.parse(currentDateforMatch);
					Calendar calMainPolicyto = Calendar.getInstance();
					calMainPolicyto.setTime(instanceMainPolicy3);
					calMainPolicyto.add(Calendar.DATE, +1);
					String currentDateforMatch_1 = dfTo.format(calMainPolicyto.getTime());   //Datefrom - 1
					
					Date instanceMainPolicy5 = dfTo.parse(activitySelectDate);
					Calendar policy1 = Calendar.getInstance();
					policy1.setTime(instanceMainPolicy5);
					policy1.add(Calendar.DATE, -(bufferDates+1));
					dateWithBufferDates_3 = dfTo.format(policy1.getTime());   //Datefrom - [Buffer]
					
					Date instanceMainPolicy6 = dfTo.parse(dateWithBufferDates_3);
					Calendar policy12 = Calendar.getInstance();
					policy12.setTime(instanceMainPolicy6);
					policy12.add(Calendar.DATE, +1);
					String dateWithBufferDates_66 = dfTo.format(policy12.getTime());
					
					Date instanceMainPolicy4 = dfTo.parse(activitySelectDate);
					Calendar calActivityDate = Calendar.getInstance();
					calActivityDate.setTime(instanceMainPolicy4);
					calActivityDate.add(Calendar.DATE, -1);
					String dateWithSelected = dfTo.format(calActivityDate.getTime()); //Activity - 1
			
					
					String StandardCancellation_based = inventory.getStandardCancellation_based();
					String Noshow_based = inventory.getNoshow_based();			
					double StandardCancellation_value = 0;				
					double Noshow_value = 0 ;
					
					
					if (StandardCancellation_based.equalsIgnoreCase("Percentage")) {
						StandardCancellation_value = (Double.parseDouble(inventory.getStandardCancellation_value()));
					}
					
					if (StandardCancellation_based.equalsIgnoreCase("value")) {
						StandardCancellation_value = Math.ceil(Double.parseDouble(inventory.getStandardCancellation_value()) + (Double.parseDouble(inventory.getStandardCancellation_value()) * ((double)pMarkup/100)));
					}
					
					if (Noshow_based.equalsIgnoreCase("Percentage")) {
						Noshow_value = (Double.parseDouble(inventory.getNoshow_value()));
					}
					
					if (Noshow_based.equalsIgnoreCase("value")) {
						Noshow_value = Math.ceil(Double.parseDouble(inventory.getNoshow_value()) + (Double.parseDouble(inventory.getNoshow_value()) * ((double)pMarkup/100)));
						
					}
			
					
					////  Cancel policy 1
					
					PrintWriter.append("<tr><td class='fontiiii'>Cancellation Policies</td><tr>"); 
					
					if (activitydetails.getCancelPolicy().size() == 2) {
						
						if (activitydetails.getCancelPolicy().get(0).contains(",")) {
							
							activityCanPolicy1 = activitydetails.getCancelPolicy().get(0).replace(",", "");
						}
						
						else {
							activityCanPolicy1 = activitydetails.getCancelPolicy().get(0);
						}
						
						
						if (StandardCancellation_based.contains("percentage")) {
							
							if (!(activityCanPolicy1.split(" ")[4].equals(currentDateforMatch))) {
								
								resPolist1 = "If you cancel between "+currentDateforMatch_1+" and "+dateWithBufferDates_1+" you will be charged "+StandardCancellation_value+" % of the purchase price.";
								
							}else {
								
								resPolist1 = "If you cancel between "+currentDateforMatch+" and "+dateWithBufferDates_1+" you will be charged "+StandardCancellation_value+" % of the purchase price.";						
							}
							
								
						} else {
		
							if (!(activityCanPolicy1.split(" ")[4].equals(currentDateforMatch))) {
								
								resPolist1 = "If you cancel between "+currentDateforMatch_1+" and "+dateWithBufferDates_1+" you will be charged "+search.getSellingCurrency()+" "+StandardCancellation_value+" .";
								
							}else {
								
								resPolist1 = "If you cancel between "+currentDateforMatch+" and "+dateWithBufferDates_1+" you will be charged "+search.getSellingCurrency()+" "+StandardCancellation_value+" .";
							}
							
						}
												
						testCaseCount++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity cancellation policy - 1</td>");
						PrintWriter.append("<td>"+resPolist1+"</td>");
						
						if(activityCanPolicy1.toLowerCase().equals(resPolist1.toLowerCase())){
							
							PrintWriter.append("<td>"+activityCanPolicy1+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+activityCanPolicy1+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}				
										
					}
					
					////
					
					if (activitydetails.getCancelPolicy().size() == 3) {
						
						if (activitydetails.getCancelPolicy().get(0).contains(",")) {
							
							activityMainPolicy = activitydetails.getCancelPolicy().get(0).replace(",", "");
						}
						
						else {
							activityMainPolicy = activitydetails.getCancelPolicy().get(0);
						}
						
						
						policyListOne = "If you cancel between "+currentDateforMatch+" and "+dateWithBufferDates_3+" you will be refunded your purchase price.";
						
						testCaseCount++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Refundable policy</td>");
						PrintWriter.append("<td>"+policyListOne+"</td>");
						
						if(policyListOne.toLowerCase().equals(activityMainPolicy.toLowerCase())){
							
							PrintWriter.append("<td>"+activityMainPolicy+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+activityMainPolicy+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
										
						
						
						if (activitydetails.getCancelPolicy().get(1).contains(",")) {
							
							activityCanPolicy2 = activitydetails.getCancelPolicy().get(1).replace(",", "");
						}
						
						else {
							activityCanPolicy2 = activitydetails.getCancelPolicy().get(1);
						}
						
						
						if (StandardCancellation_based.contains("percentage")) {
							
							policyListTwo = "If you cancel between "+dateWithBufferDates_66+" and "+dateWithSelected+" you will be charged "+StandardCancellation_value+" % of the purchase price.";	
						
						} else {
		
							policyListTwo = "If you cancel between "+dateWithBufferDates_66+" and "+dateWithSelected+" you will be charged "+search.getSellingCurrency()+" "+StandardCancellation_value+" .";
							
						}
									
						testCaseCount++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity cancellation policy - 1</td>");
						PrintWriter.append("<td>"+policyListTwo+"</td>");
						
						if(policyListTwo.toLowerCase().equals(activityCanPolicy2.toLowerCase())){
							
							PrintWriter.append("<td>"+activityCanPolicy2+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+activityCanPolicy2+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}		
						
					}
					
					
					//No Show Applying policy
					
					if (activitydetails.getCancelPolicy().size() == 2) {
						noShowMatching = activitydetails.getCancelPolicy().get(1);
					}
					
					if (activitydetails.getCancelPolicy().size() == 3) {
						noShowMatching = activitydetails.getCancelPolicy().get(2);
					}
					
					if (activitydetails.getCancelPolicy().size() == 4) {
						noShowMatching 	= activitydetails.getCancelPolicy().get(3);
					}
					
												
					double noShowValue = totalPayableforNoshow * (Double.parseDouble(inventory.getNoshow_value()) / 100);
					double roundUpValueNoShow = Math.ceil(noShowValue);
					
					if (Noshow_based.contains("percentage")) {
						
						noShowActual	= "If cancelled on or after the "+search.getActivityDate()+" No Show Fee "+search.getSellingCurrency()+" "+roundUpValueNoShow+" applies.";
						
					} else {
		
						noShowActual	= "If cancelled on or after the "+search.getActivityDate()+" No Show Fee "+search.getSellingCurrency()+" "+Noshow_value+" applies.";
						
					}
					
		
					testCaseCount++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity No-Show Fee policy</td>");
					PrintWriter.append("<td>"+noShowActual+"</td>");
					
					if(noShowMatching.toLowerCase().equals(noShowActual.toLowerCase())){
						
						PrintWriter.append("<td>"+noShowMatching+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+noShowMatching+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
				////////
					
					
					if (activitydetails.getPaymentCancelPolicy().size() == 2) {
					
						if (activitydetails.getPaymentCancelPolicy().get(0).contains(",")) {
							
							paymentCanPolicy1 = activitydetails.getPaymentCancelPolicy().get(0).replace(",", "");
						}
						
						else {
							paymentCanPolicy1 = activitydetails.getPaymentCancelPolicy().get(0);
						}
												
						testCaseCount++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity cancellation policy - 1 [Payment Page]</td>");
						PrintWriter.append("<td>"+resPolist1+"</td>");
						
						if(paymentCanPolicy1.toLowerCase().equals(resPolist1.toLowerCase())){
							
							PrintWriter.append("<td>"+paymentCanPolicy1+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+paymentCanPolicy1+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}				
										
					}
					
					if (activitydetails.getPaymentCancelPolicy().size() == 3) {
						
						if (activitydetails.getPaymentCancelPolicy().get(0).contains(",")) {
							
							paymentMainPolicy = activitydetails.getPaymentCancelPolicy().get(0).replace(",", "");
						}
						
						else {
							paymentMainPolicy = activitydetails.getPaymentCancelPolicy().get(0);
						}
						
						
						testCaseCount++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Refundable policy [Payment Page]</td>");
						PrintWriter.append("<td>"+policyListOne+"</td>");
						
						if(policyListOne.toLowerCase().equals(paymentMainPolicy.toLowerCase())){
							
							PrintWriter.append("<td>"+paymentMainPolicy+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+paymentMainPolicy+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
										
						
						
						if (activitydetails.getPaymentCancelPolicy().get(1).contains(",")) {
							
							paymentCanPolicy2 = activitydetails.getPaymentCancelPolicy().get(1).replace(",", "");
						}
						
						else {
							paymentCanPolicy2 = activitydetails.getPaymentCancelPolicy().get(1);
						}
						
									
						testCaseCount++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity cancellation policy - 1 [Payment Page]</td>");
						PrintWriter.append("<td>"+policyListTwo+"</td>");
						
						if(policyListTwo.toLowerCase().equals(paymentCanPolicy2.toLowerCase())){
							
							PrintWriter.append("<td>"+paymentCanPolicy2+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+paymentCanPolicy2+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}		
						
					}
					
					
					if (activitydetails.getPaymentCancelPolicy().size() == 2) {
						paymentNoShowMatching = activitydetails.getPaymentCancelPolicy().get(1);
					}
					
					if (activitydetails.getPaymentCancelPolicy().size() == 3) {
						paymentNoShowMatching = activitydetails.getPaymentCancelPolicy().get(2);
					}
					
					if (activitydetails.getPaymentCancelPolicy().size() == 4) {
						paymentNoShowMatching 	= activitydetails.getPaymentCancelPolicy().get(3);
					}
					
					testCaseCount++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity No-Show Fee policy [Payment Page]</td>");
					PrintWriter.append("<td>"+noShowActual+"</td>");
					
					if(paymentNoShowMatching.toLowerCase().equals(noShowActual.toLowerCase())){
						
						PrintWriter.append("<td>"+paymentNoShowMatching+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+paymentNoShowMatching+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
				
				///////
					
					if (activitydetails.getConfirmCancelPolicy().size() == 2) {
					
						if (activitydetails.getConfirmCancelPolicy().get(0).contains(",")) {
							
							confirmCanPolicy1 = activitydetails.getConfirmCancelPolicy().get(0).replace(",", "");
						}
						
						else {
							confirmCanPolicy1 = activitydetails.getConfirmCancelPolicy().get(0);
						}
												
						testCaseCount++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity cancellation policy - 1 [Confirmation Page]</td>");
						PrintWriter.append("<td>"+resPolist1+"</td>");
						
						if(confirmCanPolicy1.toLowerCase().equals(resPolist1.toLowerCase())){
							
							PrintWriter.append("<td>"+confirmCanPolicy1+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+confirmCanPolicy1+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}				
										
					}
					
					if (activitydetails.getConfirmCancelPolicy().size() == 3) {
						
						if (activitydetails.getConfirmCancelPolicy().get(0).contains(",")) {
							
							confirmMainPolicy = activitydetails.getConfirmCancelPolicy().get(0).replace(",", "");
						}
						
						else {
							confirmMainPolicy = activitydetails.getConfirmCancelPolicy().get(0);
						}
						
						
						testCaseCount++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Refundable policy [Confirmation Page]</td>");
						PrintWriter.append("<td>"+policyListOne+"</td>");
						
						if(policyListOne.toLowerCase().equals(confirmMainPolicy.toLowerCase())){
							
							PrintWriter.append("<td>"+confirmMainPolicy+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+confirmMainPolicy+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
										
						
						
						if (activitydetails.getConfirmCancelPolicy().get(1).contains(",")) {
							
							confirmCanPolicy2 = activitydetails.getConfirmCancelPolicy().get(1).replace(",", "");
						}
						
						else {
							confirmCanPolicy2 = activitydetails.getConfirmCancelPolicy().get(1);
						}
						
									
						testCaseCount++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity cancellation policy - 1 [Confirmation Page]</td>");
						PrintWriter.append("<td>"+policyListTwo+"</td>");
						
						if(policyListTwo.toLowerCase().equals(confirmCanPolicy2.toLowerCase())){
							
							PrintWriter.append("<td>"+confirmCanPolicy2+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+confirmCanPolicy2+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}		
						
					}
					
					
					if (activitydetails.getConfirmCancelPolicy().size() == 2) {
						confirmNoShowMatching = activitydetails.getConfirmCancelPolicy().get(1);
					}
					
					if (activitydetails.getConfirmCancelPolicy().size() == 3) {
						confirmNoShowMatching = activitydetails.getConfirmCancelPolicy().get(2);
					}
					
					if (activitydetails.getConfirmCancelPolicy().size() == 4) {
						confirmNoShowMatching 	= activitydetails.getConfirmCancelPolicy().get(3);
					}
					
					testCaseCount++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity No-Show Fee policy [Confirmation Page]</td>");
					PrintWriter.append("<td>"+noShowActual+"</td>");
					
					if(confirmNoShowMatching.toLowerCase().equals(noShowActual.toLowerCase())){
						
						PrintWriter.append("<td>"+confirmNoShowMatching+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+confirmNoShowMatching+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					
					
					PrintWriter.append("</table>");
					
					
					///////////////////////////////////////////////////////////////////////////////////////////////////////
					//Booking list report	
					
					PrintWriter.append("<br><br>");
					PrintWriter.append("<p class='fontStyles'>Booking List Report Summary</p>");
					
					testCaseCountBookingList = 1;
					PrintWriter.append("<br><table ><tr><th>Test Case No</th><th>Test Description</th><th>Expect Values - [Excel Value]</th><th>Actual Values</th><th>Test Status</th></tr>");
				
					if (bookingList.isBookingListReportLoaded() == true) {
						
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking Card - Reservation No</td>");
						PrintWriter.append("<td>"+activitydetails.getReservationNo()+"</td>");
						
						if(activitydetails.getReservationNo().equals(bookingList.getBCard_Res_ReservationNumber())){
							
							PrintWriter.append("<td>"+bookingList.getBCard_Res_ReservationNumber()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+bookingList.getBCard_Res_ReservationNumber()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						String Reservation_Date = bookingList.getBCard_Res_Reservation_Date();
						String FirstElement_Date = bookingList.getBCard_Res_FirstElement_Date();
						String Cancel_Deadline_Date = bookingList.getBCard_Res_Cancel_Deadline();
						String bookedDate = bookingList.getBCard_Activity_DatesBookedFor();
						
						String Reservation_DateNew = bookingList.getBList_Reservation_Date();
						String FirstElement_DateNew = bookingList.getBList_DateOf_FirstElement();
						String Cancel_Deadline_DateNew = bookingList.getBList_DateOf_Cxl_Deadline();
						
						DateFormat dateFForBooking = new SimpleDateFormat("dd-MMM-yyyy");
						SimpleDateFormat formatInToBooking = new SimpleDateFormat("dd/MM/yyyy");
									
						Date instanceReservation_Date = formatInToBooking.parse(Reservation_Date);  
						Date instanceFirstElement_Date = formatInToBooking.parse(FirstElement_Date);
						Date instanceToCancel_Deadline_Date = formatInToBooking.parse(Cancel_Deadline_Date);				
						Date instanceToCancel_bookedDate = formatInToBooking.parse(bookedDate);
				
						String Reservation_Date_con = dateFForBooking.format(instanceReservation_Date);
						String FirstElement_Date_con = dateFForBooking.format(instanceFirstElement_Date);
						String Cancel_Deadline_Date_con = dateFForBooking.format(instanceToCancel_Deadline_Date);
						String bookedDate_con = dateFForBooking.format(instanceToCancel_bookedDate);
						
						///
						
						testCaseCountBookingList++;
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking Card - Reservation Date</td>");
						PrintWriter.append("<td>"+currentDateforMatch+"</td>");
						
						if(currentDateforMatch.equals(Reservation_DateNew)){
							
							PrintWriter.append("<td>"+Reservation_DateNew+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+Reservation_DateNew+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCountBookingList++;
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking Card - Date Of the First Element Used</td>");
						PrintWriter.append("<td>"+search.getActivityDate()+"</td>");
						
						if(search.getActivityDate().equals(FirstElement_DateNew)){
							
							PrintWriter.append("<td>"+FirstElement_DateNew+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+FirstElement_DateNew+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCountBookingList++;
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking Card - Cancellation Deadline</td>");
						PrintWriter.append("<td>"+dateWithBufferDates_3+"</td>");
						
						if(dateWithBufferDates_3.equals(Cancel_Deadline_DateNew)){
							
							PrintWriter.append("<td>"+Cancel_Deadline_DateNew+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+Cancel_Deadline_DateNew+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						if (UserTypeDC_B2B == TourOperatorType.NETCREDITLPON || UserTypeDC_B2B == TourOperatorType.NETCREDITLPOY) {
													
							testCaseCountBookingList++;
							PrintWriter.append("<tr><td>" + testCaseCountBookingList + "</td> <td>Booking Card - Payment Type</td>");
							PrintWriter.append("<td>On Credit</td>");
							
							if ("On Credit".replaceAll(" ", "").equalsIgnoreCase(bookingList.getBList_PaymentType().replaceAll(" ", ""))) {

								PrintWriter.append("<td>" + bookingList.getBList_PaymentType() + "</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");
							}

							else {
								PrintWriter.append("<td>" + bookingList.getBList_PaymentType() + "</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
							
						}else{
							
							testCaseCountBookingList++;
							PrintWriter.append("<tr><td>" + testCaseCountBookingList + "</td> <td>Booking Card - Payment Type</td>");
							PrintWriter.append("<td>" + activitydetails.getPaymentType_bookingList() + "</td>");
							
							if (activitydetails.getPaymentType_bookingList().replaceAll(" ", "").equalsIgnoreCase(bookingList.getBList_PaymentType().replaceAll(" ", ""))) {

								PrintWriter.append("<td>" + bookingList.getBList_PaymentType() + "</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");
							}

							else {
								PrintWriter.append("<td>" + bookingList.getBList_PaymentType() + "</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
												
						}
						
						
						if (bookingList.getBList_Status().replaceAll(" ", "").toLowerCase().contains("req")) {
							
							testCaseCountBookingList++;
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking Card - Booking status</td>");
							PrintWriter.append("<td>"+activitydetails.getBookingStatus()+"</td>");
							
							if(activitydetails.getBookingStatus().toLowerCase().contains(bookingList.getBList_Status().toLowerCase())){
								
								PrintWriter.append("<td>"+bookingList.getBList_Status()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+bookingList.getBList_Status()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
						}
						
						if (bookingList.getBList_Status().replaceAll(" ", "").toLowerCase().contains("con")) {
							
							testCaseCountBookingList++;
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking Card - Booking status</td>");
							PrintWriter.append("<td>"+activitydetails.getBookingStatus()+"</td>");
							
							if(activitydetails.getBookingStatus().toLowerCase().contains(bookingList.getBList_Status().toLowerCase().replaceAll(" ", ""))){
								
								PrintWriter.append("<td>"+bookingList.getBList_Status()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+bookingList.getBList_Status()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
						}
						
						
						
						String fullName;
						fullName = activitydetails.getPaymentPage_Title() + activitydetails.getPaymentPage_FName() + " "+activitydetails.getPaymentPage_LName()+"";
						
						testCaseCountBookingList ++;			
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking Card - Customer Name</td>");
						PrintWriter.append("<td>"+fullName+"</td>");
									
						if(fullName.replaceAll(" ", "").equalsIgnoreCase(bookingList.getBList_Customer_Name().replaceAll(" ", ""))){
								
							PrintWriter.append("<td>"+bookingList.getBList_Customer_Name()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+bookingList.getBList_Customer_Name()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						String leadName = activitydetails.getResultsPage_cusTitle().get(0) + activitydetails.getResultsPage_cusFName().get(0) + activitydetails.getResultsPage_cusLName().get(0);
						
						testCaseCountBookingList ++;			
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking Card - Lead Guest Name</td>");
						PrintWriter.append("<td>"+leadName+"</td>");
									
						if(leadName.replaceAll(" ", "").equalsIgnoreCase(bookingList.getBList_LeadGuest_Name().replaceAll(" ", ""))){
								
							PrintWriter.append("<td>"+bookingList.getBList_LeadGuest_Name()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+bookingList.getBList_LeadGuest_Name()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						
						if (activitydetails.getPaymentDetails().equals("Pay Online")) {
							
							testCaseCountBookingList ++;			
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking Card - Invoice Issued</td>");
							PrintWriter.append("<td>Green</td>");
										
							if(bookingList.isBList_Invoice_Issued() == true){
									
								PrintWriter.append("<td>Green</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>Not match</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
							testCaseCountBookingList ++;			
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking Card - Payments Issued</td>");
							PrintWriter.append("<td>Green</td>");
										
							if(bookingList.isBList_Payment_Receieved() == true){
									
								PrintWriter.append("<td>Green</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>Not match</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
							testCaseCountBookingList ++;			
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking Card - Voucher Issued</td>");
							PrintWriter.append("<td>Green</td>");
										
							if(bookingList.isBList_Voucher_Issued() == true){
									
								PrintWriter.append("<td>Green</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>Not match</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
												
							
						}else{
							
							
							if(UserTypeDC_B2B == TourOperatorType.COMCASH || UserTypeDC_B2B == TourOperatorType.NETCASH){
								
								testCaseCountBookingList ++;			
								PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking Card - Invoice Issued</td>");
								PrintWriter.append("<td>Green</td>");
											
								if(bookingList.isBList_Invoice_Issued() == true){
										
									PrintWriter.append("<td>Green</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
								}
								
								else{
									PrintWriter.append("<td>Not match</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
								}
								
							}else{
								
								testCaseCountBookingList ++;			
								PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking Card - Invoice Issued</td>");
								PrintWriter.append("<td>Orange</td>");
											
								if(bookingList.isBList_Invoice_Issued() == false){
										
									PrintWriter.append("<td>Orange</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
								}
								
								else{
									PrintWriter.append("<td>Not match</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
								}
							}
							
							
							
							testCaseCountBookingList ++;			
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking Card - Payments Issued</td>");
							PrintWriter.append("<td>Orange</td>");
										
							if(bookingList.isBList_Payment_Receieved() == false){
									
								PrintWriter.append("<td>Orange</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>Not match</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
							testCaseCountBookingList ++;			
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking Card - Voucher Issued</td>");
							PrintWriter.append("<td>Orange</td>");
										
							if(bookingList.isBList_Voucher_Issued() == false){
									
								PrintWriter.append("<td>Orange</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>Not match</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
						}
						
						
								
						
						///
						
						testCaseCountBookingList++;
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Payment Method - Reservation Summary</td>");
						PrintWriter.append("<td>"+activitydetails.getPaymentType_bookingList()+"</td>");
						
						if(bookingList.getBCard_Res_PaymentMethod().toLowerCase().replaceAll(" ", "").contains(activitydetails.getPaymentType_bookingList().toLowerCase().replaceAll(" ", ""))){
							
							PrintWriter.append("<td>"+bookingList.getBCard_Res_PaymentMethod()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+bookingList.getBCard_Res_PaymentMethod()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCountBookingList++;
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Payment Reference - Reservation Summary</td>");
						PrintWriter.append("<td>"+activitydetails.getPaymentReference_bookingList()+"</td>");
						
						if(activitydetails.getPaymentReference_bookingList().equalsIgnoreCase(bookingList.getBCard_Res_PaymentReference())){
							
							PrintWriter.append("<td>"+bookingList.getBCard_Res_PaymentReference()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+bookingList.getBCard_Res_PaymentReference()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						////
						
						testCaseCountBookingList++;
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Reservation Date - Reservation Summary</td>");
						PrintWriter.append("<td>"+currentDateforMatch+"</td>");
						
						if(currentDateforMatch.equals(Reservation_Date_con)){
							
							PrintWriter.append("<td>"+Reservation_Date_con+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+Reservation_Date_con+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCountBookingList++;
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Date Of the First Element Used - Reservation Summary</td>");
						PrintWriter.append("<td>"+search.getActivityDate()+"</td>");
						
						if(search.getActivityDate().equals(FirstElement_Date_con)){
							
							PrintWriter.append("<td>"+FirstElement_Date_con+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+FirstElement_Date_con+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCountBookingList++;
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Cancellation Deadline - Reservation Summary</td>");
						PrintWriter.append("<td>"+dateWithBufferDates_3+"</td>");
						
						if(dateWithBufferDates_3.equals(Cancel_Deadline_Date_con)){
							
							PrintWriter.append("<td>"+Cancel_Deadline_Date_con+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+Cancel_Deadline_Date_con+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						if (bookingList.getBCard_Res_Booking_Status().toLowerCase().contains("req")) {
							
							testCaseCountBookingList++;
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Booking status - Reservation Summary</td>");
							PrintWriter.append("<td>"+activitydetails.getBookingStatus()+"</td>");
							
							if(activitydetails.getBookingStatus().toLowerCase().contains(bookingList.getBCard_Res_Booking_Status().toLowerCase())){
								
								PrintWriter.append("<td>"+bookingList.getBCard_Res_Booking_Status()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+bookingList.getBCard_Res_Booking_Status()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
						}
						
						if (bookingList.getBCard_Res_Booking_Status().toLowerCase().contains("con")) {
							
							testCaseCountBookingList++;
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Booking status - Reservation Summary</td>");
							PrintWriter.append("<td>"+activitydetails.getBookingStatus()+"</td>");
							
							if(activitydetails.getBookingStatus().toLowerCase().contains(bookingList.getBCard_Res_Booking_Status().toLowerCase())){
								
								PrintWriter.append("<td>"+bookingList.getBCard_Res_Booking_Status()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+bookingList.getBCard_Res_Booking_Status()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
						}
						
						
					
						testCaseCountBookingList++;
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Currency 1 - Reservation Summary</td>");
						PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
						
						if(bookingList.getBCard_Res_CurrencyType().contains(search.getSellingCurrency())){
							
							PrintWriter.append("<td>"+bookingList.getBCard_Res_CurrencyType()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+bookingList.getBCard_Res_CurrencyType()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						////
						
						
						String bookingSub = bookingList.getBCard_Res_SubTotal().replace(",", "").split("\\.")[0];
						
						testCaseCountBookingList ++;
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Sub Total - Reservation Summary</td>");
						PrintWriter.append("<td>"+(PaymentPage_SubTotal - discountedVale)+"</td>");
									
						
						if(Integer.toString(PaymentPage_SubTotal - discountedVale).equals(bookingSub)){
								
							PrintWriter.append("<td>"+bookingSub+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+bookingSub+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						String bookingTax = bookingList.getBCard_Res_Tax_Other().split("\\.")[0];
						
						testCaseCountBookingList ++;
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Tax and other charges - Reservation Summary</td>");
						PrintWriter.append("<td>"+PaymentPage_Taxes+"</td>");
								
						
						if(Integer.toString(PaymentPage_Taxes).equals(bookingTax)){
								
							PrintWriter.append("<td>"+bookingTax+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+bookingTax+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						creditTax = (int) Math.ceil(PaymentPage_CreditCardFee);
						String BCard_Res_Credit_Fee = bookingList.getBCard_Res_Credit_Fee().replace(",", "").split("\\.")[0];
						
						int totalTax = PaymentPage_Taxes + creditTax ;
						int totalAmount = PaymentPage_SubTotal + totalTax - discountedVale;
						
						
						String bookingTotalval = bookingList.getBCard_Res_Total_Booking_Fee().replace(",", "").split("\\.")[0];
						
						testCaseCountBookingList ++;
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Total Booking Value - Reservation Summary</td>");
						PrintWriter.append("<td>"+totalAmount+"</td>");
								
						
						if(Integer.toString(totalAmount).equals(bookingTotalval)){
								
							PrintWriter.append("<td>"+bookingTotalval+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+bookingTotalval+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						
						if (activitydetails.getPaymentDetails().equals("Pay Online")) {
							
							testCaseCountBookingList ++;
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Credit Card Tax - Reservation Summary</td>");
							PrintWriter.append("<td>"+creditTax+"</td>");
								
							
							if(Integer.toString(creditTax).equals(BCard_Res_Credit_Fee)){
									
								PrintWriter.append("<td>"+BCard_Res_Credit_Fee+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+BCard_Res_Credit_Fee+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
							
							String  BCard_Res_Amount_Payable_CheckIn = bookingList.getBCard_Res_Total_Booking_Fee().replace(",", "").split("\\.")[0];
							
							testCaseCountBookingList ++;
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Total Amount Payable Upfront - Reservation Summary</td>");
							PrintWriter.append("<td>"+totalAmount+"</td>");
								
							
							if(Integer.toString(totalAmount).equals(BCard_Res_Amount_Payable_CheckIn)){
									
								PrintWriter.append("<td>"+BCard_Res_Amount_Payable_CheckIn+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+BCard_Res_Amount_Payable_CheckIn+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
						}
					
						
						String amountDue = bookingList.getBCard_Res_Amount_Payable_CheckIn().split("\\.")[0];
						
						if (UserTypeDC_B2B == TourOperatorType.COMCASH || UserTypeDC_B2B == TourOperatorType.COMCREDITLPONO || UserTypeDC_B2B == TourOperatorType.COMCREDITLPOY) {
							
							if (activitydetails.getPaymentDetails().equals("Pay Online")) {
							
								testCaseCountBookingList ++;
								PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Amount due at Check-In - Reservation Summary</td>");
								PrintWriter.append("<td>"+toAgentCommission+"</td>");
									
								if(Integer.toString(toAgentCommission).equals(amountDue)){
										
									PrintWriter.append("<td>"+amountDue+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
								}
								
								else{
									PrintWriter.append("<td>"+amountDue+"</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
								}
							
							}else{
								
								testCaseCountBookingList ++;
								PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Amount due at Check-In - Reservation Summary</td>");
								PrintWriter.append("<td>"+PaymentPage_AmountCheckIn+"</td>");
									
								if(Integer.toString(PaymentPage_AmountCheckIn).equals(amountDue)){
										
									PrintWriter.append("<td>"+amountDue+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
								}
								
								else{
									PrintWriter.append("<td>"+amountDue+"</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
								}
							}
							
							
						}else{
						
							testCaseCountBookingList ++;
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Amount due at Check-In - Reservation Summary</td>");
							PrintWriter.append("<td>"+PaymentPage_AmountCheckIn+"</td>");
								
							if(Integer.toString(PaymentPage_AmountCheckIn).equals(amountDue)){
									
								PrintWriter.append("<td>"+amountDue+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+amountDue+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
						
						}
						
						
						
						/*if (activitydetails.getPaymentDetails().equals("Pay Offline")) {
							
							String amountPaid = bookingList.getBCard_Res_AmountPaid().split("\\.")[0];
							testCaseCountBookingList ++;
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Amount Paid</td>");
							PrintWriter.append("<td>"+amountPaid+"</td>");
								
							if(amountPaid.equals(amountPaid)){
									
								PrintWriter.append("<td>"+amountPaid+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+amountPaid+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
						}*/
						
						
						//////
						
						if (bookingList.isAgentShows() == true) {
							
							testCaseCountBookingList ++;			
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Agent Name</td>");
							PrintWriter.append("<td>"+PG_Properties.getProperty(inventory.getUserTypeDC_B2B().toString())+"</td>");
										
							if(PG_Properties.getProperty(inventory.getUserTypeDC_B2B().toString()).replaceAll(" ", "").equalsIgnoreCase(bookingList.getBCard_Agent_AgentName().replaceAll(" ", ""))){
									
								PrintWriter.append("<td>"+bookingList.getBCard_Agent_AgentName()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+bookingList.getBCard_Agent_AgentName()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
							testCaseCountBookingList ++;			
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - User Name</td>");
							PrintWriter.append("<td>"+PG_Properties.getProperty("DC_UserName")+"</td>");
										
							if(PG_Properties.getProperty("DC_UserName").equalsIgnoreCase(bookingList.getBCard_Agent_UserName())){
									
								PrintWriter.append("<td>"+bookingList.getBCard_Agent_UserName()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+bookingList.getBCard_Agent_UserName()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
							String toFromBList = bookingList.getBCard_Agent_AgentType().split(" ")[0];
							
							if (toFromBList.equalsIgnoreCase("Cash")) {
								
								testCaseCountBookingList ++;			
								PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Agent Type</td>");
								PrintWriter.append("<td>Cash To</td>");
											
								if(PG_Properties.getProperty(inventory.getUserTypeDC_B2B().toString()).replaceAll(" ", "").toLowerCase().contains(toFromBList.toLowerCase())){
										
									PrintWriter.append("<td>"+PG_Properties.getProperty(inventory.getUserTypeDC_B2B().toString())+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
								}
								
								else{
									PrintWriter.append("<td>"+PG_Properties.getProperty(inventory.getUserTypeDC_B2B().toString())+"</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
								}
								
								
							} else {
								
								testCaseCountBookingList ++;			
								PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Agent Type</td>");
								PrintWriter.append("<td>Credit To</td>");
											
								if(PG_Properties.getProperty(inventory.getUserTypeDC_B2B().toString()).replaceAll(" ", "").toLowerCase().contains(toFromBList.toLowerCase())){
										
									PrintWriter.append("<td>"+PG_Properties.getProperty(inventory.getUserTypeDC_B2B().toString())+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
								}
								
								else{
									PrintWriter.append("<td>"+PG_Properties.getProperty(inventory.getUserTypeDC_B2B().toString())+"</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
								}

							}
							
							if (UserTypeDC_B2B == TourOperatorType.COMCASH) {
								
								String toAgentTypePerOrValue = PG_Properties.getProperty(inventory.getUserTypeDC_B2B().toString() + "#Value");
								
								testCaseCountBookingList ++;			
								PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Commission Percentage</td>");
								PrintWriter.append("<td>"+toAgentTypePerOrValue+"</td>");
											
								if(bookingList.getBCard_Agent_CommissionPercentage().contains(toAgentTypePerOrValue)){
										
									PrintWriter.append("<td>"+bookingList.getBCard_Agent_CommissionPercentage()+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
								}
								
								else{
									PrintWriter.append("<td>"+bookingList.getBCard_Agent_CommissionPercentage()+"</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
								}
								
								testCaseCountBookingList ++;			
								PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Commission Amount</td>");
								PrintWriter.append("<td>"+toAgentCommission+"</td>");
											
								if(bookingList.getBCard_Agent_CommissionAmount().contains(Integer.toString(toAgentCommission))){
										
									PrintWriter.append("<td>"+bookingList.getBCard_Agent_CommissionAmount()+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
								}
								
								else{
									PrintWriter.append("<td>"+bookingList.getBCard_Agent_CommissionAmount()+"</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
								}
								
		
							}
							if (UserTypeDC_B2B == TourOperatorType.COMCREDITLPONO) {
								
								String toAgentTypePerOrValue = PG_Properties.getProperty(inventory.getUserTypeDC_B2B().toString() + "#Value");
								
								testCaseCountBookingList ++;			
								PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Commission Percentage</td>");
								PrintWriter.append("<td>"+toAgentTypePerOrValue+"</td>");
											
								if(bookingList.getBCard_Agent_CommissionPercentage().contains(toAgentTypePerOrValue)){
										
									PrintWriter.append("<td>"+bookingList.getBCard_Agent_CommissionPercentage()+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
								}
								
								else{
									PrintWriter.append("<td>"+bookingList.getBCard_Agent_CommissionPercentage()+"</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
								}
								
								testCaseCountBookingList ++;			
								PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Commission Amount</td>");
								PrintWriter.append("<td>"+toAgentCommission+"</td>");
											
								if(bookingList.getBCard_Agent_CommissionAmount().contains(Integer.toString(toAgentCommission))){
										
									PrintWriter.append("<td>"+bookingList.getBCard_Agent_CommissionAmount()+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
								}
								
								else{
									PrintWriter.append("<td>"+bookingList.getBCard_Agent_CommissionAmount()+"</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
								}
								
							}
							if (UserTypeDC_B2B == TourOperatorType.COMCREDITLPOY) {
								
								String toAgentTypePerOrValue = PG_Properties.getProperty(inventory.getUserTypeDC_B2B().toString() + "#Value");
								
								testCaseCountBookingList ++;			
								PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Commission Percentage</td>");
								PrintWriter.append("<td>"+toAgentTypePerOrValue+"</td>");
											
								if(bookingList.getBCard_Agent_CommissionPercentage().contains(toAgentTypePerOrValue)){
										
									PrintWriter.append("<td>"+bookingList.getBCard_Agent_CommissionPercentage()+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
								}
								
								else{
									PrintWriter.append("<td>"+bookingList.getBCard_Agent_CommissionPercentage()+"</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
								}
								
								testCaseCountBookingList ++;			
								PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Commission Amount</td>");
								PrintWriter.append("<td>"+toAgentCommission+"</td>");
											
								if(bookingList.getBCard_Agent_CommissionAmount().contains(Integer.toString(toAgentCommission))){
										
									PrintWriter.append("<td>"+bookingList.getBCard_Agent_CommissionAmount()+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
								}
								
								else{
									PrintWriter.append("<td>"+bookingList.getBCard_Agent_CommissionAmount()+"</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
								}
								
							}
							
						}
						
						/////
						
						
						testCaseCountBookingList ++;			
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Customer Name</td>");
						PrintWriter.append("<td>"+fullName+"</td>");
									
						if(fullName.toLowerCase().contains(bookingList.getBCard_Guest_FName().toLowerCase())){
								
							PrintWriter.append("<td>"+bookingList.getBCard_Guest_FName()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+bookingList.getBCard_Guest_FName()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						
						testCaseCountBookingList ++;			
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Customer - TP No</td>");
						PrintWriter.append("<td>"+activitydetails.getPaymentPage_TP()+"</td>");
									
						if(activitydetails.getPaymentPage_TP().equals(bookingList.getBCard_Guest_PhoneNumber())){
								
							PrintWriter.append("<td>"+bookingList.getBCard_Guest_PhoneNumber()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+bookingList.getBCard_Guest_PhoneNumber()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCountBookingList ++;			
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Customer Email</td>");
						PrintWriter.append("<td>"+activitydetails.getPaymentPage_Email()+"</td>");
									
						if(activitydetails.getPaymentPage_Email().equals(bookingList.getBCard_Guest_Email())){
								
							PrintWriter.append("<td>"+bookingList.getBCard_Guest_Email()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+bookingList.getBCard_Guest_Email()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						String add = activitydetails.getPaymentPage_address();
						
						testCaseCountBookingList ++;			
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Customer Address</td>");
						PrintWriter.append("<td>"+add+"</td>");
									
						if(bookingList.getBCard_Guest_Add().toLowerCase().contains(add.toLowerCase())){
								
							PrintWriter.append("<td>"+bookingList.getBCard_Guest_Add()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+bookingList.getBCard_Guest_Add()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCountBookingList ++;			
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Customer Country</td>");
						PrintWriter.append("<td>"+activitydetails.getPaymentPage_country()+"</td>");
									
						if(activitydetails.getPaymentPage_country().equals(bookingList.getBCard_Guest_Country())){
								
							PrintWriter.append("<td>"+bookingList.getBCard_Guest_Country()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+bookingList.getBCard_Guest_Country()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCountBookingList ++;			
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Customer - City</td>");
						PrintWriter.append("<td>"+activitydetails.getPaymentPage_city()+"</td>");
									
						if(activitydetails.getPaymentPage_city().equals(bookingList.getBCard_Guest_City())){
								
							PrintWriter.append("<td>"+bookingList.getBCard_Guest_City()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+bookingList.getBCard_Guest_City()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						/////
						
						if (bookingList.getBCard_Activity_BookingStatus().toLowerCase().contains("req")) {
							
							testCaseCountBookingList ++;			
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Activity Details - Booking status</td>");
							PrintWriter.append("<td>"+activitydetails.getBookingStatus()+"</td>");
										
							if(activitydetails.getBookingStatus().toLowerCase().contains(bookingList.getBCard_Activity_BookingStatus().toLowerCase())){
									
								PrintWriter.append("<td>"+bookingList.getBCard_Activity_BookingStatus()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+bookingList.getBCard_Activity_BookingStatus()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
						}
						
						if (bookingList.getBCard_Activity_BookingStatus().toLowerCase().contains("con")) {
							
							testCaseCountBookingList ++;			
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Activity Details - Booking status</td>");
							PrintWriter.append("<td>"+activitydetails.getBookingStatus()+"</td>");
										
							if(activitydetails.getBookingStatus().toLowerCase().contains(bookingList.getBCard_Activity_BookingStatus().toLowerCase())){
									
								PrintWriter.append("<td>"+bookingList.getBCard_Activity_BookingStatus()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+bookingList.getBCard_Activity_BookingStatus()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
						}
						
						
						testCaseCountBookingList ++;			
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Activity Name</td>");
						PrintWriter.append("<td>"+inventory.getActivityName()+"</td>");
									
						if(inventory.getActivityName().toLowerCase().contains(bookingList.getBCard_Activity_ActivityName().toLowerCase())){
								
							PrintWriter.append("<td>"+bookingList.getBCard_Activity_ActivityName()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+bookingList.getBCard_Activity_ActivityName()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						testCaseCountBookingList ++;			
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Activity Details - Date booked</td>");
						PrintWriter.append("<td>"+search.getActivityDate()+"</td>");
									
						if(search.getActivityDate().contains(bookedDate_con)){
								
							PrintWriter.append("<td>"+bookedDate_con+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+bookedDate_con+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						////
						
						testCaseCountBookingList++;
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Activity List Currency</td>");
						PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
						
						if(search.getSellingCurrency().equals(bookingList.getBCard_Activity_Currency())){
							
							PrintWriter.append("<td>"+bookingList.getBCard_Activity_Currency()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+bookingList.getBCard_Activity_Currency()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						String bookingSubTotal = bookingList.getBCard_Activity_SubTotal().split("\\.")[0];
						
						testCaseCountBookingList ++;
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Sub Total (Without tax)</td>");
						PrintWriter.append("<td>"+(PaymentPage_SubTotal - discountedVale)+"</td>");
									
						if(Integer.toString(PaymentPage_SubTotal - discountedVale).equals(bookingSubTotal)){
								
							PrintWriter.append("<td>"+bookingSubTotal+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+bookingSubTotal+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						int totalTaxForAct = PaymentPage_Taxes + creditTax;
						String bookingTaxOtther = bookingList.getBCard_Activity_Tax().split("\\.")[0];
						
						if (bookingList.getBCard_Activity_TotalBookingValue().contains(",")) {
							bookingTotalvalueForAct = bookingList.getBCard_Activity_TotalBookingValue().split("\\.")[0].replace(",", "");
						}else{
							bookingTotalvalueForAct = bookingList.getBCard_Activity_TotalBookingValue().split("\\.")[0];
						}
						
						
						testCaseCountBookingList ++;
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Tax & Other Charges</td>");
						PrintWriter.append("<td>"+(PaymentPage_Taxes)+"</td>");
									
						if(Integer.toString(PaymentPage_Taxes).equals(bookingTaxOtther)){
								
							PrintWriter.append("<td>"+bookingTaxOtther+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+bookingTaxOtther+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						////////
					
						if (activitydetails.getPaymentDetails().equals("Pay Online")) {
							
							
							String BCard_Activity_CreditFee = bookingList.getBCard_Res_Credit_Fee().split("\\.")[0];
							testCaseCountBookingList ++;
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Credit Card Fee</td>");
							PrintWriter.append("<td>"+(creditTax)+"</td>");
										
							if(Integer.toString(creditTax).equals(BCard_Activity_CreditFee)){
									
								PrintWriter.append("<td>"+BCard_Activity_CreditFee+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+BCard_Activity_CreditFee+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
						
							testCaseCountBookingList ++;
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Activity Total Booking Value</td>");
							PrintWriter.append("<td>"+(ResultsPage_TotalPayable - discountedVale)+"</td>");
										
							if(Integer.toString(ResultsPage_TotalPayable - discountedVale).equals(bookingList.getBCard_Activity_TotalBookingValue().split("\\.")[0])){
									
								PrintWriter.append("<td>"+bookingList.getBCard_Activity_TotalBookingValue().split("\\.")[0]+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+bookingList.getBCard_Activity_TotalBookingValue().split("\\.")[0]+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
							testCaseCountBookingList ++;
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Amount Chargeable Upfront</td>");
							PrintWriter.append("<td>"+(ResultsPage_TotalPayable - discountedVale)+"</td>");
										
							if(Integer.toString(ResultsPage_TotalPayable - discountedVale).equals(bookingList.getBCard_Activity_AmountChargeable().split("\\.")[0])){
									
								PrintWriter.append("<td>"+bookingList.getBCard_Activity_AmountChargeable().split("\\.")[0]+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+bookingList.getBCard_Activity_AmountChargeable().split("\\.")[0]+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
							/*testCaseCountBookingList ++;
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Amount Paid</td>");
							PrintWriter.append("<td>"+(ResultsPage_TotalPayable - discountedVale)+"</td>");
										
							if(Integer.toString(ResultsPage_TotalPayable - discountedVale).equals(bookingList.getBCard_Activity_AmountPaid().split("\\.")[0])){
									
								PrintWriter.append("<td>"+bookingList.getBCard_Activity_AmountPaid().split("\\.")[0]+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+bookingList.getBCard_Activity_AmountPaid().split("\\.")[0]+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}*/
							
							testCaseCountBookingList ++;
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Amount Due At Check-In</td>");
							PrintWriter.append("<td>"+PaymentPage_AmountCheckIn+"</td>");
										
							if(Integer.toString(PaymentPage_AmountCheckIn).equals(bookingList.getBCard_Activity_AmountDue().split("\\.")[0])){
									
								PrintWriter.append("<td>"+bookingList.getBCard_Activity_AmountDue().split("\\.")[0]+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+bookingList.getBCard_Activity_AmountDue().split("\\.")[0]+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
							
							
						}else{
							
							int creditNoFee = 0;
							
							String BCard_Activity_CreditFee = bookingList.getBCard_Res_Credit_Fee().split("\\.")[0];
							testCaseCountBookingList ++;
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Credit Card Fee</td>");
							PrintWriter.append("<td>"+(creditNoFee)+"</td>");
										
							if(Integer.toString(creditNoFee).equals(BCard_Activity_CreditFee)){
									
								PrintWriter.append("<td>"+BCard_Activity_CreditFee+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+BCard_Activity_CreditFee+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
						
							testCaseCountBookingList ++;
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Activity Total Booking Value</td>");
							PrintWriter.append("<td>"+(ResultsPage_TotalPayable + creditNoFee - discountedVale)+"</td>");
										
							if(Integer.toString(ResultsPage_TotalPayable + creditNoFee - discountedVale).equals(bookingList.getBCard_Activity_TotalBookingValue().split("\\.")[0])){
									
								PrintWriter.append("<td>"+bookingList.getBCard_Activity_TotalBookingValue().split("\\.")[0]+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+bookingList.getBCard_Activity_TotalBookingValue().split("\\.")[0]+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
							testCaseCountBookingList ++;
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Amount Chargeable Upfront</td>");
							PrintWriter.append("<td>"+(ResultsPage_TotalPayable + creditNoFee - discountedVale)+"</td>");
										
							if(Integer.toString(ResultsPage_TotalPayable + creditNoFee - discountedVale).equals(bookingList.getBCard_Activity_AmountChargeable().split("\\.")[0])){
									
								PrintWriter.append("<td>"+bookingList.getBCard_Activity_AmountChargeable().split("\\.")[0]+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+bookingList.getBCard_Activity_AmountChargeable().split("\\.")[0]+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
							
							int amountPaidBLROffLine = 0;
							
							/*testCaseCountBookingList ++;
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Amount Paid</td>");
							PrintWriter.append("<td>"+amountPaidBLROffLine+"</td>");
										
							if(Integer.toString(amountPaidBLROffLine).equals(bookingList.getBCard_Activity_AmountPaid().split("\\.")[0])){
									
								PrintWriter.append("<td>"+bookingList.getBCard_Activity_AmountPaid().split("\\.")[0]+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+bookingList.getBCard_Activity_AmountPaid().split("\\.")[0]+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}*/
							
							testCaseCountBookingList ++;
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Amount Due At Check-In</td>");
							PrintWriter.append("<td>"+PaymentPage_AmountCheckIn+"</td>");
										
							if(Integer.toString(PaymentPage_AmountCheckIn).equals(bookingList.getBCard_Activity_AmountDue().split("\\.")[0])){
									
								PrintWriter.append("<td>"+bookingList.getBCard_Activity_AmountDue().split("\\.")[0]+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+bookingList.getBCard_Activity_AmountDue().split("\\.")[0]+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
						}
						
						
						///////
						
						
						String personRate = bookingList.getBCard_Activity_TotalRate().split("\\.")[0];
						String acrate = " " + activitydetails.getActivityRate();
						testCaseCountBookingList ++;
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - Person Rate</td>");
						PrintWriter.append("<td>"+acrate+"</td>");
									
						if(acrate.equals(personRate)){
								
							PrintWriter.append("<td>"+personRate+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+personRate+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						int totalPax = Integer.parseInt(search.getAdults()) + Integer.parseInt(search.getChildren());
						String pax = " " + totalPax;
						
						testCaseCountBookingList ++;
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report - QTY</td>");
						PrintWriter.append("<td>"+pax+"</td>");
									
						if(pax.equals(bookingList.getBCard_Activity_qty())){
								
							PrintWriter.append("<td>"+bookingList.getBCard_Activity_qty()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+bookingList.getBCard_Activity_qty()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						///////
						
						testCaseCountBookingList ++;
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Payment Cancellation policy and Booking list Report cancellation policy match</td>");
						PrintWriter.append("<td>Payment Page :- "+activitydetails.getCancelPolicy().size()+"</td>");
									
						if(activitydetails.getCancelPolicy().size() == bookingList.getCancelPolicy().size()){
								
							PrintWriter.append("<td>Booking Report :- "+bookingList.getCancelPolicy().size()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>Booking Report :- "+bookingList.getCancelPolicy().size()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						
						if (bookingList.getCancelPolicy().size() == 2) {
							
							String policyList_1 = bookingList.getCancelPolicy().get(0).replace("\n", "");
							String noshowCancel_1 = bookingList.getCancelPolicy().get(1).replace(",", "");
							
							testCaseCountBookingList++;
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking list Report Activity cancellation policy - 1</td>");
							PrintWriter.append("<td>"+resPolist1+"</td>");
							
							try {
								if(policyList_1.toLowerCase().equals(resPolist1.toLowerCase())){
									
									PrintWriter.append("<td>"+policyList_1+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
								}
								
								
							} catch (Exception e) {
								PrintWriter.append("<td>"+e.getMessage()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
								
							}	
							
							
							testCaseCountBookingList++;
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking list Report Activity No-Show Fee policy</td>");
							PrintWriter.append("<td>"+noShowActual+"</td>");
							
							if(noshowCancel_1.toLowerCase().equals(noShowActual.toLowerCase())){
								
								PrintWriter.append("<td>"+noshowCancel_1+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							else{
								PrintWriter.append("<td>"+noshowCancel_1+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
						}
						
						if (bookingList.getCancelPolicy().size() == 3) {
							
							String policyList_21 = bookingList.getCancelPolicy().get(0).replace("\n", "");
							String policyList_22 = bookingList.getCancelPolicy().get(1).replace("\n", "");
							String noshowCancel_12 = bookingList.getCancelPolicy().get(2).replace(",", "");
							
							testCaseCountBookingList++;
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking list Report Activity Refundable policy</td>");
							PrintWriter.append("<td>"+policyListOne+"</td>");
							
							if(policyListOne.equals(policyList_21)){
								
								PrintWriter.append("<td>"+policyList_21+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							else{
								PrintWriter.append("<td>"+policyList_21+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
								
							}
								
							
							testCaseCountBookingList++;
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking list Report Activity cancellation policy - 1</td>");
							PrintWriter.append("<td>"+policyListTwo+"</td>");
							
							if(policyListTwo.contains(policyList_22)){
								
								PrintWriter.append("<td>"+policyList_22+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							else{
								PrintWriter.append("<td>"+policyList_22+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
								
							}
							
							testCaseCountBookingList++;
							PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking list Report Activity No-Show Fee policy</td>");
							PrintWriter.append("<td>"+noShowActual+"</td>");
							
							if(noshowCancel_12.toLowerCase().equals(noShowActual.toLowerCase())){
								
								PrintWriter.append("<td>"+noshowCancel_12+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							
							else{
								PrintWriter.append("<td>"+noshowCancel_12+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
									
						}
						customerQty = Integer.parseInt(search.getAdults()) + Integer.parseInt(search.getChildren());	
						
						
						if(UserTypeDC_B2B == TourOperatorType.NETCREDITLPON || UserTypeDC_B2B == TourOperatorType.NETCREDITLPOY || UserTypeDC_B2B == TourOperatorType.COMCREDITLPOY || UserTypeDC_B2B == TourOperatorType.COMCREDITLPONO){
					    	
					    	if (activitydetails.getPaymentDetails().equalsIgnoreCase("Pay Offline")) {
						
					    		int beforePaid = Integer.parseInt(bookingList.getBeforeCreditBalance());
					    		int totalAmountBookingList = Integer.parseInt(activitydetails.getPaymentPageAmountbeingProcessed());
					    		int balance = beforePaid - totalAmountBookingList; 
					    		
					    		try {
					    			
					    			if (UserTypeDC_B2B == TourOperatorType.NETCREDITLPON || UserTypeDC_B2B == TourOperatorType.NETCREDITLPOY) {
										
					    				testCaseCountBookingList++;
										PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking list Report - Balance Due</td>");
										PrintWriter.append("<td>"+inventory.getSupplierCurrency()+""+(PaymentPage_AmountProcessed)+"</td>");
										
										if(bookingList.getBalanceDue().contains(Integer.toString(PaymentPage_AmountProcessed))){
											
											PrintWriter.append("<td>"+bookingList.getBalanceDue()+"</td>");
											PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
										}
										
										
										else{
											PrintWriter.append("<td>"+bookingList.getBalanceDue()+"</td>");
											PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
										}
										
									} else {

										testCaseCountBookingList++;
										PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking list Report - Balance Due</td>");
										PrintWriter.append("<td>"+inventory.getSupplierCurrency()+""+(PaymentPage_AmountProcessed)+"</td>");
										
										if(bookingList.getBalanceDue().contains(Integer.toString(PaymentPage_AmountProcessed))){
											
											PrintWriter.append("<td>"+bookingList.getBalanceDue()+"</td>");
											PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
										}
										
										
										else{
											PrintWriter.append("<td>"+bookingList.getBalanceDue()+"</td>");
											PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
										}
										
									}
					    			
									
					    			
					    			
								} catch (Exception e) {
									
								}
					    		
					    		testCaseCountBookingList++;
								PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking list Report - TO Credit Balance</td>");
								PrintWriter.append("<td>"+beforePaid+"</td>");
								
								if(Integer.toString(beforePaid).equals(bookingList.getAfterPayCreditBalance())){
									
									PrintWriter.append("<td>"+bookingList.getAfterPayCreditBalance()+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
								}
								
								
								else{
									PrintWriter.append("<td>"+bookingList.getAfterPayCreditBalance()+"</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
								}
						
								
								
								if (UserTypeDC_B2B == TourOperatorType.NETCREDITLPOY || UserTypeDC_B2B == TourOperatorType.COMCREDITLPOY ) {
									
									testCaseCountBookingList++;
									PrintWriter.append("<tr><td>" + testCaseCountBookingList + "</td> <td>Booking list Report - LPO No</td>");
									PrintWriter.append("<td>" + bookingList.getBeforeLpoNo() + "</td>");
									
									if (bookingList.getBeforeLpoNo().equalsIgnoreCase(bookingList.getLpoNo())) {

										PrintWriter.append("<td>" + bookingList.getLpoNo() + "</td>");
										PrintWriter.append("<td class='Passed'>PASS</td><tr>");
									}

									else {
										PrintWriter.append("<td>" + bookingList.getLpoNo() + "</td>");
										PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
									}
									
								}
								
								
								testCaseCountBookingList ++;			
								PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking list Report - Invoice Issued [After Paid]</td>");
								PrintWriter.append("<td>Green</td>");
											
								if(bookingList.isPaid_Invoice_Issued() == true){
										
									PrintWriter.append("<td>Green</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
								}
								
								else{
									PrintWriter.append("<td>Not match</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
								}
								
								testCaseCountBookingList ++;			
								PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking list Report - Payments Issued [After Paid]</td>");
								PrintWriter.append("<td>Green</td>");
											
								if(bookingList.isPaid_Payment_Receieved() == true){
										
									PrintWriter.append("<td>Green</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
								}
								
								else{
									PrintWriter.append("<td>Not match</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
								}
								
								testCaseCountBookingList ++;			
								PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking list Report - Voucher Issued [After Paid]</td>");
								PrintWriter.append("<td>Green</td>");
											
								if(bookingList.isPaid_Voucher_Issued() == true){
										
									PrintWriter.append("<td>Green</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
								}
								
								else{
									PrintWriter.append("<td>Not match</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
								}
								
								
					    	}
						}
						
					}else{
						
						PrintWriter.append("<tr><td>"+testCaseCountBookingList+"</td> <td>Booking List Report</td>");
						PrintWriter.append("<td>Booking List Report should be available</td>");
						PrintWriter.append("<td>Not Available</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");		
					}
					
					
					
					PrintWriter.append("</table>");
					
					
					//Quotation Mail					
					getQuotationMail(driver);
					
					//Mails Info
					getMailsInfo(driver);
					
		
					
					/////////////////////////////////////////////////////////////////////////////////////
					//Supplier Mail
					
				
					PrintWriter.append("<br><br>");
					PrintWriter.append("<p class='fontStyles'>Email - Supplier Email</p>");
					
					testcaseSupplier = 1;
					PrintWriter.append("<br><table ><tr><th>Test Case No</th><th>Test Description</th><th>Expect Values - [Excel Value]</th><th>Actual Values</th><th>Test Status</th></tr>");
					
					if (confirmationDetails.isSupplier() == true) {
					
						PrintWriter.append("<tr><td>"+testcaseSupplier+"</td> <td>Supplier Mail - Booking No</td>");
						PrintWriter.append("<td>"+activitydetails.getReservationNo()+"</td>");
						
						if(activitydetails.getReservationNo().equals(confirmationDetails.getSupplierMAIL_BookingNo())){
							
							PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_BookingNo()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_BookingNo()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						String SupplierMAIL_IssueDate = currentDateforMatch;
						
						testcaseSupplier++;
						PrintWriter.append("<tr><td>"+testcaseSupplier+"</td> <td>Supplier Mail - Booking Issue Date</td>");
						PrintWriter.append("<td>"+SupplierMAIL_IssueDate+"</td>");
						
						if(confirmationDetails.getSupplierMAIL_IssueDate().replace("-", "").equals(SupplierMAIL_IssueDate.replace("-", ""))){
							
							PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_IssueDate()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_IssueDate()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testcaseSupplier++;
						PrintWriter.append("<tr><td>"+testcaseSupplier+"</td> <td>Supplier Mail - Acitivity Name</td>");
						PrintWriter.append("<td>"+inventory.getActivityName()+"</td>");
						
						if(inventory.getActivityName().toLowerCase().contains(confirmationDetails.getSupplierMAIL_ActivityName().toLowerCase())){
								
							PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_ActivityName()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_ActivityName()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						testcaseSupplier ++;
						PrintWriter.append("<tr><td>"+testcaseSupplier+"</td> <td>Supplier Mail - Booking Status</td>");
						PrintWriter.append("<td>"+activitydetails.getBookingStatus()+"</td>");
									
						if (activitydetails.getBookingStatus().toLowerCase().contains("request")) {
							
							if(activitydetails.getBookingStatus().toLowerCase().contains(confirmationDetails.getSupplierMAIL_BookingStatus().toLowerCase())){
								
								PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_BookingStatus()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else {
								
								PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_BookingStatus()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
							}
							
						} else {
		
							if(activitydetails.getBookingStatus().toLowerCase().contains(confirmationDetails.getSupplierMAIL_BookingStatus().toLowerCase())){
								
								PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_BookingStatus()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else {
								
								PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_BookingStatus()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
							}
						}
						
						
						testcaseSupplier ++;
						PrintWriter.append("<tr><td>"+testcaseSupplier+"</td> <td>Supplier Mail - Program City</td>");
						PrintWriter.append("<td>"+search.getDestination()+"</td>");
									
						if(search.getDestination().toLowerCase().contains(confirmationDetails.getSupplierMAIL_ProgramCity().toLowerCase())){
								
							PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_ProgramCity()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_ProgramCity()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						///
						
						testcaseSupplier ++;
						PrintWriter.append("<tr><td>"+testcaseSupplier+"</td> <td>Supplier Mail - Activity Duration</td>");
						PrintWriter.append("<td>"+activitydetails.getActivitySession()+"</td>");
									
						if(confirmationDetails.getSupplierMAIL_Duration().toLowerCase().contains(activitydetails.getActivitySession().toLowerCase())){
								
							PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_Duration()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_Duration()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						testcaseSupplier ++;
						PrintWriter.append("<tr><td>"+testcaseSupplier+"</td> <td>Supplier Mail - Activity QTY</td>");
						PrintWriter.append("<td>"+customerQty+"</td>");
									
						if(Integer.toString(customerQty).equals(confirmationDetails.getSupplierMAIL_QTY())){
								
							PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_QTY()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_QTY()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						String SupplierMAIL_ServicyDate = search.getActivityDate();
						
						testcaseSupplier ++;
						PrintWriter.append("<tr><td>"+testcaseSupplier+"</td> <td>Supplier Mail - Activity Service Date</td>");
						PrintWriter.append("<td>"+SupplierMAIL_ServicyDate+"</td>");
									
						if(SupplierMAIL_ServicyDate.replace("-", "").equals(confirmationDetails.getSupplierMAIL_ServicyDate().replace("-", ""))){
								
							PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_ServicyDate()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_ServicyDate()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						/////
						
						int activityrate = ((PaymentPage_SubTotal * 100 ) / (100 + pMarkup));
						
						testcaseSupplier ++;
						PrintWriter.append("<tr><td>"+testcaseSupplier+"</td> <td>Supplier Mail - Activity Rate [Without PM]</td>");
						PrintWriter.append("<td>"+activityrate+"</td>");
									
						if(Integer.toString(activityrate).equals(confirmationDetails.getSupplierMAIL_ActivityRate())){
								
							PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_ActivityRate()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_ActivityRate()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						testcaseSupplier ++;
						PrintWriter.append("<tr><td>"+testcaseSupplier+"</td> <td>Supplier Mail - Customer TP</td>");
						PrintWriter.append("<td>"+activitydetails.getPaymentPage_TP()+"</td>");
									
						if(activitydetails.getPaymentPage_TP().replaceAll("-", "").equals(confirmationDetails.getSupplierMAIL_CustomerTP())){
								
							PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_CustomerTP()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_CustomerTP()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						
						
						testcaseSupplier ++;			
						PrintWriter.append("<tr><td>"+testcaseSupplier+"</td> <td>Supplier Mail - Tel 1</td>");
						PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Tel")+"</td>");
									
						if(PG_Properties.getProperty("Portal.Tel").toLowerCase().equals(confirmationDetails.getSupplierMAIL_Tel_1().toLowerCase())){
								
							PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_Tel_1()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_Tel_1()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						testcaseSupplier ++;			
						PrintWriter.append("<tr><td>"+testcaseSupplier+"</td> <td>Supplier Mail - Email 1</td>");
						PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Email")+"</td>");
									
						if(PG_Properties.getProperty("Portal.Email").toLowerCase().equals(confirmationDetails.getSupplierMAIL_Email_1().toLowerCase())){
								
							PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_Email_1()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_Email_1()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						testcaseSupplier ++;			
						PrintWriter.append("<tr><td>"+testcaseSupplier+"</td> <td>Supplier Mail - Website</td>");
						PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Website")+"</td>");
									
						if(PG_Properties.getProperty("Portal.Website").toLowerCase().equals(confirmationDetails.getSupplierMAIL_Website().toLowerCase())){
								
							PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_Website()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_Website()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testcaseSupplier ++;			
						PrintWriter.append("<tr><td>"+testcaseSupplier+"</td> <td>Supplier Mail - Fax</td>");
						PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Fax")+"</td>");
									
						if(PG_Properties.getProperty("Portal.Fax").toLowerCase().equals(confirmationDetails.getSupplierMAIL_Fax().toLowerCase())){
								
							PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_Fax()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_Fax()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testcaseSupplier ++;			
						PrintWriter.append("<tr><td>"+testcaseSupplier+"</td> <td>Supplier Mail - Company Name</td>");
						PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Name")+"</td>");
									
						if(PG_Properties.getProperty("Portal.Name").toLowerCase().equals(confirmationDetails.getSupplierMAIL_CompanyName().toLowerCase())){
								
							PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_CompanyName()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+confirmationDetails.getSupplierMAIL_CompanyName()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
					
					}else{
						
						PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Email - Supplier Mail</td>");
						PrintWriter.append("<td>Supplier Mail should be available</td>");
						PrintWriter.append("<td>Not Available</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");		
						
						
					}
					
					PrintWriter.append("</table>");
					
					
					//ReservationReport
					getReservationReport(driver);
	
					
				}
			}
	
	}
	
	
	public void getMailsInfo(WebDriver Driver){
		
		PrintWriter.append("<br><br>");
		PrintWriter.append("<p class='fontStyles'>Email - Customer Confirmation Email</p>");
		
		testCaseCustomerMail = 1;
		PrintWriter.append("<br><table ><tr><th>Test Case No</th><th>Test Description</th><th>Expect Values - [Excel Value]</th><th>Actual Values</th><th>Test Status</th></tr>");
	
		if (confirmationDetails.isCustomerConfirmationMailLoaded() == true) {
			
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Booking No</td>");
			PrintWriter.append("<td>"+activitydetails.getReservationNo()+"</td>");
			
			if(activitydetails.getReservationNo().equals(confirmationDetails.getCCE_BookingNo())){
				
				PrintWriter.append("<td>"+confirmationDetails.getCCE_BookingNo()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_BookingNo()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
			String referenceName = activitydetails.getPaymentPage_Title() + activitydetails.getPaymentPage_FName() + activitydetails.getPaymentPage_LName();
			
			testCaseCustomerMail ++;
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Booking Reference</td>");
			PrintWriter.append("<td>"+referenceName+"</td>");
						
			if(confirmationDetails.getCCE_BookingRef().replaceAll(" ", "").contains(referenceName)){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_BookingRef()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_BookingRef()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
			
			testCaseCustomerMail++;
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Acitivity Voucher [Reference Number]</td>");
			PrintWriter.append("<td>"+activitydetails.getReservationNo()+"</td>");
			
			if(activitydetails.getReservationNo().equals(confirmationDetails.getActivityVaoucherRefNo())){
				
				PrintWriter.append("<td>"+confirmationDetails.getActivityVaoucherRefNo()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getActivityVaoucherRefNo()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCaseCustomerMail++;
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Acitivity Name</td>");
			PrintWriter.append("<td>"+inventory.getActivityName()+"</td>");
			
			if(inventory.getActivityName().toLowerCase().contains(confirmationDetails.getCCE_ActivityType().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_ActivityType()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getCCE_ActivityType()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			
			/////
			
			testCaseCustomerMail++;
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Booking Status </td>");
			PrintWriter.append("<td>"+activitydetails.getBookingStatus()+"</td>");
						
			if (activitydetails.getBookingStatus().toLowerCase().contains("request")) {
				
				if(activitydetails.getBookingStatus().replace("-", "").toLowerCase().contains(confirmationDetails.getCCE_BookingStatus().replace(" ", "").toLowerCase())){
					
					PrintWriter.append("<td>"+confirmationDetails.getCCE_BookingStatus()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+confirmationDetails.getCCE_BookingStatus()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
			} else {

				if(activitydetails.getBookingStatus().toLowerCase().contains(confirmationDetails.getCCE_BookingStatus().toLowerCase())){
					
					PrintWriter.append("<td>"+confirmationDetails.getCCE_BookingStatus()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+confirmationDetails.getCCE_BookingStatus()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
			}
			
			
			/////
			
			
			testCaseCustomerMail ++;
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Activity Rate type </td>");
			PrintWriter.append("<td>"+activitydetails.getActivityRateType()+"</td>");
						
			if(confirmationDetails.getCCE_RateType().toLowerCase().contains(activitydetails.getActivityRateType().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_RateType()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getCCE_RateType()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			testCaseCustomerMail ++;
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Activity Session</td>");
			PrintWriter.append("<td>"+activitydetails.getActivitySession()+"</td>");
						
			if(confirmationDetails.getCCE_Period().toLowerCase().contains(activitydetails.getActivitySession().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_Period()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getCCE_Period()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			testCaseCustomerMail ++;
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Activity Rate</td>");
			PrintWriter.append("<td>"+activitydetails.getActivityRate()+"</td>");
						
			if(confirmationDetails.getCCE_Rate().equals(activitydetails.getActivityRate())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_Rate()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getCCE_Rate()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			customerQty = Integer.parseInt(search.getAdults()) + Integer.parseInt(search.getChildren());
			
			testCaseCustomerMail ++;
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Activity QTY</td>");
			PrintWriter.append("<td>"+customerQty+"</td>");
						
			if(Integer.toString(customerQty).equals(confirmationDetails.getCCE_QTY())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_QTY()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getCCE_QTY()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			testCaseCustomerMail ++;
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Currency 1</td>");
			PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
						
			if(search.getSellingCurrency().toLowerCase().equals(confirmationDetails.getCCE_TotalValue_Currency().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_TotalValue_Currency()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_TotalValue_Currency()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCaseCustomerMail ++;
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Sub Total[Without Tax]</td>");
			PrintWriter.append("<td>"+PaymentPage_SubTotal+"</td>");
						
			if(Integer.toString(PaymentPage_SubTotal).equals(confirmationDetails.getCCE_TotalValue())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_TotalValue()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_TotalValue()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			//////
			
			testCaseCustomerMail ++;
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Currency 2</td>");
			PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
						
			if(search.getSellingCurrency().toLowerCase().equals(confirmationDetails.getCCE_CurrencyType_1().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_CurrencyType_1()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_CurrencyType_1()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCaseCustomerMail ++;
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Sub Total - 2[Without Tax]</td>");
			PrintWriter.append("<td>"+(PaymentPage_SubTotal - discountedVale)+"</td>");
						
			if(Integer.toString(PaymentPage_SubTotal - discountedVale).equals(confirmationDetails.getCCE_SubTotal())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_SubTotal()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_SubTotal()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
			
			if (activitydetails.getPaymentDetails().equals("Pay Online")) {
				
				int customerPageTotalTax = PaymentPage_Taxes + creditTax;
				
				testCaseCustomerMail ++;
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Total Tax and other charges</td>");
				PrintWriter.append("<td>"+(customerPageTotalTax)+"</td>");
							
				if(Integer.toString(customerPageTotalTax).equals(confirmationDetails.getCCE_TotalTaxOther_1())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_TotalTaxOther_1()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_TotalTaxOther_1()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				testCaseCustomerMail ++;
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Activity Total Booking Value</td>");
				PrintWriter.append("<td>"+(PaymentPage_SubTotal + customerPageTotalTax - discountedVale)+"</td>");
							
				if(Integer.toString(PaymentPage_SubTotal + customerPageTotalTax - discountedVale).equals(confirmationDetails.getCCE_TotalBookingValue())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_TotalBookingValue()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_TotalBookingValue()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCustomerMail ++;
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Amount payable now</td>");
				PrintWriter.append("<td>"+(PaymentPage_SubTotal + customerPageTotalTax - discountedVale)+"</td>");
							
				if(Integer.toString(PaymentPage_SubTotal + customerPageTotalTax - discountedVale).equals(confirmationDetails.getCCE_AmountPayableNow())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_AmountPayableNow()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_AmountPayableNow()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCustomerMail ++;
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Amount to be paid at utilization</td>");
				PrintWriter.append("<td>"+PaymentPage_AmountCheckIn+"</td>");
							
				if(Integer.toString(PaymentPage_AmountCheckIn).equals(confirmationDetails.getCCE_AmountDue())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_AmountDue()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_AmountDue()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
			}else{
				
				testCaseCustomerMail ++;
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Total Tax and other charges</td>");
				PrintWriter.append("<td>"+(PaymentPage_Taxes)+"</td>");
							
				if(Integer.toString(PaymentPage_Taxes).equals(confirmationDetails.getCCE_TotalTaxOther_1())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_TotalTaxOther_1()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_TotalTaxOther_1()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				testCaseCustomerMail ++;
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Activity Total Booking Value</td>");
				PrintWriter.append("<td>"+(PaymentPage_SubTotal + PaymentPage_Taxes - discountedVale)+"</td>");
							
				if(Integer.toString(PaymentPage_SubTotal + PaymentPage_Taxes - discountedVale).equals(confirmationDetails.getCCE_TotalBookingValue())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_TotalBookingValue()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_TotalBookingValue()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}	
				
				testCaseCustomerMail ++;
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Amount payable now</td>");
				PrintWriter.append("<td>"+(PaymentPage_SubTotal + PaymentPage_Taxes - discountedVale)+"</td>");
							
				if(Integer.toString(PaymentPage_SubTotal + PaymentPage_Taxes - discountedVale).equals(confirmationDetails.getCCE_AmountPayableNow())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_AmountPayableNow()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_AmountPayableNow()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCustomerMail ++;
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Amount to be paid at utilization</td>");
				PrintWriter.append("<td>"+PaymentPage_AmountCheckIn+"</td>");
							
				if(Integer.toString(PaymentPage_AmountCheckIn).equals(confirmationDetails.getCCE_AmountDue())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_AmountDue()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_AmountDue()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
			}
			
			
			
			
			
			//////
			
			if (activitydetails.getPaymentDetails().equals("Pay Online")) {
				
				int customerPageTotalTax = PaymentPage_Taxes + creditTax;
				
				
				
				testCaseCustomerMail ++;
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Total Booking Value - 2</td>");
				PrintWriter.append("<td>"+(PaymentPage_SubTotal + customerPageTotalTax - discountedVale)+"</td>");
							
				if(Integer.toString(PaymentPage_SubTotal + customerPageTotalTax - discountedVale).equals(confirmationDetails.getCCE_TotalBookingValue_3())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_TotalBookingValue_3()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_TotalBookingValue_3()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCustomerMail ++;
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Amount being Processed Now - 2</td>");
				PrintWriter.append("<td>"+(PaymentPage_SubTotal + customerPageTotalTax - discountedVale)+"</td>");
							
				if(Integer.toString(PaymentPage_SubTotal + customerPageTotalTax - discountedVale).equals(confirmationDetails.getCCE_AmountPocessed())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_AmountPocessed()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_AmountPocessed()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCustomerMail ++;
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Amount to be paid at utilization - 2</td>");
				PrintWriter.append("<td>"+PaymentPage_AmountCheckIn+"</td>");
							
				if(Integer.toString(PaymentPage_AmountCheckIn).equals(confirmationDetails.getCCE_AmountDue_2())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_AmountDue_2()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_AmountDue_2()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
			}else{
				
			
				testCaseCustomerMail ++;
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Total Booking Value - 2</td>");
				PrintWriter.append("<td>"+(PaymentPage_SubTotal + PaymentPage_Taxes - discountedVale)+"</td>");
							
				if(Integer.toString(PaymentPage_SubTotal + PaymentPage_Taxes - discountedVale).equals(confirmationDetails.getCCE_TotalBookingValue_3())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_TotalBookingValue_3()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_TotalBookingValue_3()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}	
				
				
				
				testCaseCustomerMail ++;
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Amount being Processed Now</td>");
				PrintWriter.append("<td>"+(PaymentPage_SubTotal + PaymentPage_Taxes - discountedVale)+"</td>");
							
				if(Integer.toString(PaymentPage_SubTotal + PaymentPage_Taxes - discountedVale).equals(confirmationDetails.getCCE_AmountPocessed())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_AmountPocessed()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_AmountPocessed()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCustomerMail ++;
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Amount to be paid at utilization</td>");
				PrintWriter.append("<td>"+PaymentPage_AmountCheckIn+"</td>");
							
				if(Integer.toString(PaymentPage_AmountCheckIn).equals(confirmationDetails.getCCE_AmountDue_2())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_AmountDue_2()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_AmountDue_2()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
			}
			
			testCaseCustomerMail ++;
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Currency 4</td>");
			PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
						
			if(search.getSellingCurrency().toLowerCase().equals(confirmationDetails.getCCE_TotalBookingValue_Currency().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_TotalBookingValue_Currency()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_TotalBookingValue_Currency()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			/////////
			
			
			if (UserTypeDC_B2B == TourOperatorType.DC){
			
				testCaseCustomerMail ++;			
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - First Name</td>");
				PrintWriter.append("<td>"+activitydetails.getPaymentPage_FName()+"</td>");
							
				if(activitydetails.getPaymentPage_FName().equals(confirmationDetails.getCCE_FName())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_FName()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_FName()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCustomerMail ++;			
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Last Name</td>");
				PrintWriter.append("<td>"+activitydetails.getPaymentPage_LName()+"</td>");
							
				if(activitydetails.getPaymentPage_LName().equals(confirmationDetails.getCCE_LName())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_LName()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_LName()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
			}else{
				
				testCaseCustomerMail ++;			
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - First Name</td>");
				PrintWriter.append("<td>"+activitydetails.getPaymentPage_FName()+"</td>");
							
				if(activitydetails.getPaymentPage_FName().equals(confirmationDetails.getCCE_FName())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_FName()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_FName()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCustomerMail ++;			
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Last Name</td>");
				PrintWriter.append("<td>"+activitydetails.getPaymentPage_LName()+"</td>");
							
				if(activitydetails.getPaymentPage_LName().equals(confirmationDetails.getCCE_LName())){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_LName()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_LName()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
			}
			
			
			
			testCaseCustomerMail ++;			
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - TP No</td>");
			PrintWriter.append("<td>"+activitydetails.getPaymentPage_TP()+"</td>");
						
			if(activitydetails.getPaymentPage_TP().equals(confirmationDetails.getCCE_TP())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_TP()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_TP()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCaseCustomerMail ++;			
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Email</td>");
			PrintWriter.append("<td>"+activitydetails.getPaymentPage_Email()+"</td>");
						
			if(confirmationDetails.getCCE_Email().contains(activitydetails.getPaymentPage_Email())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_Email()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_Email()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCaseCustomerMail ++;			
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Address</td>");
			PrintWriter.append("<td>"+activitydetails.getPaymentPage_address()+"</td>");
						
			if(activitydetails.getPaymentPage_address().equals(confirmationDetails.getCCE_address())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_address()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_address()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCaseCustomerMail ++;			
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Address 2</td>");
			PrintWriter.append("<td>"+activitydetails.getPaymentPage_address_2()+"</td>");
						
			if(activitydetails.getPaymentPage_address_2().equals(confirmationDetails.getCCE_address_2())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_address_2()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_address_2()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCaseCustomerMail ++;			
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Country</td>");
			PrintWriter.append("<td>"+activitydetails.getPaymentPage_country()+"</td>");
						
			if(activitydetails.getPaymentPage_country().equals(confirmationDetails.getCCE_country())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_country()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_country()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCaseCustomerMail ++;			
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - City</td>");
			PrintWriter.append("<td>"+activitydetails.getPaymentPage_city()+"</td>");
						
			if(activitydetails.getPaymentPage_city().equals(confirmationDetails.getCCE_city())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_city()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_city()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			/////
			
			for (int i = 0; i < activitydetails.getPaymenPagecusCount(); i++) {
				
				testCaseCustomerMail++;
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Counts - [Customer "+(i+1)+"] Title - Customer Confirmation Email]</td>");
				PrintWriter.append("<td>"+activitydetails.getResultsPage_cusTitle().get(i)+"</td>");
				
				if(activitydetails.getResultsPage_cusTitle().get(i).equals(confirmationDetails.getCCE_CusTitle().get(i))){
					
					PrintWriter.append("<td>"+confirmationDetails.getCCE_CusTitle().get(i)+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
				}
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_CusTitle().get(i)+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
			}
			
			for (int i = 0; i < activitydetails.getPaymenPagecusCount(); i++) {
				
				testCaseCustomerMail++;
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Counts - [Customer "+(i+1)+"] FName- Customer Confirmation Email]</td>");
				PrintWriter.append("<td>"+activitydetails.getResultsPage_cusFName().get(i)+"</td>");
				
				if(activitydetails.getResultsPage_cusFName().get(i).equals(confirmationDetails.getCCE_CusFName().get(i))){
					
					PrintWriter.append("<td>"+confirmationDetails.getCCE_CusFName().get(i)+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
				}
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_CusFName().get(i)+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
			}
			
			for (int i = 0; i < activitydetails.getPaymenPagecusCount(); i++) {
				
				testCaseCustomerMail++;
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Counts - [Customer "+(i+1)+"] LName - Customer Confirmation Email]</td>");
				PrintWriter.append("<td>"+activitydetails.getResultsPage_cusLName().get(i)+"</td>");
				
				if(activitydetails.getResultsPage_cusLName().get(i).equals(confirmationDetails.getCCE_CusLName().get(i))){
					
					PrintWriter.append("<td>"+confirmationDetails.getCCE_CusLName().get(i)+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
				}
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_CusLName().get(i)+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
			}
			
			/////
			
			if (activitydetails.getPaymentDetails().equals("Pay Online")) {
				
				if((PG_Properties.getProperty("PortalCurrency").toLowerCase().equals(inventory.getSupplierCurrency().toLowerCase()))){
					
					testCaseCustomerMail ++;			
					PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Total Value In USD</td>");
					PrintWriter.append("<td>"+PaymentPage_AmountProcessed+"</td>");
								
					if(Integer.toString(PaymentPage_AmountProcessed).equals(confirmationDetails.getCCE_PaymentTotalValue())){
							
						PrintWriter.append("<td>"+confirmationDetails.getCCE_PaymentTotalValue()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+confirmationDetails.getCCE_PaymentTotalValue()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
				}
				
				else{
					
					for(Entry<String, String> entry: currencyMap.entrySet()) {
						if(inventory.getSupplierCurrency().toLowerCase().equals(entry.getKey().toLowerCase())){
							
							rateConvertforUSD = Double.parseDouble(entry.getValue());
							
							int ResultsPage_TotalPayable_ForUSD = 0;
							
							if (!(UserTypeDC_B2B == TourOperatorType.DC)) {
															
								if (UserTypeDC_B2B == TourOperatorType.COMCASH || UserTypeDC_B2B == TourOperatorType.COMCREDITLPONO || UserTypeDC_B2B == TourOperatorType.COMCREDITLPOY) {
									
									ResultsPage_TotalPayable_ForUSD =  (int) Math.ceil((( Double.parseDouble(activitydetails.getToActivity_TotalTax()) + Double.parseDouble(inventory.getCreditCard_Fee()) + (Double.parseDouble(activitydetails.getToActivity_SubTotal()) - Double.parseDouble(activitydetails.getToAgentCommission())) ) / rateConvertforUSD ));																
									
								}
								if (UserTypeDC_B2B == TourOperatorType.NETCASH || UserTypeDC_B2B == TourOperatorType.NETCREDITLPON || UserTypeDC_B2B == TourOperatorType.NETCREDITLPOY) {
									
									ResultsPage_TotalPayable_ForUSD =  (int) Math.ceil((( Double.parseDouble(activitydetails.getToActivity_TotalTax()) + Double.parseDouble(inventory.getCreditCard_Fee()) + (Double.parseDouble(activitydetails.getToActivity_SubTotal()) ) ) / rateConvertforUSD ));																
																		
								}
								
							
							}else{
								
								ResultsPage_TotalPayable_ForUSD =  (int) Math.ceil((Double.parseDouble(inventory.getPaymentPage_TotalPackageValue()) / rateConvertforUSD ));
							}
							
							
							testCaseCustomerMail ++;			
							PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Total Value In USD</td>");
							PrintWriter.append("<td>USD - "+ResultsPage_TotalPayable_ForUSD+"</td>");
										
							if(Integer.toString(ResultsPage_TotalPayable_ForUSD).equals(confirmationDetails.getCCE_PaymentTotalValue())){
									
								PrintWriter.append("<td>USD - "+confirmationDetails.getCCE_PaymentTotalValue()+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>USD - "+confirmationDetails.getCCE_PaymentTotalValue()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
						}
					}
					
					
				}
			}
				
			testCaseCustomerMail ++;			
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Customer Notes</td>");
			PrintWriter.append("<td>"+activitydetails.getCustomerNotes()+"</td>");
						
			if(activitydetails.getCustomerNotes().equals(confirmationDetails.getCCE_CustomerNote())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_CustomerNote()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_CustomerNote()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
			//////////////////////////////
			
			testCaseCustomerMail ++;
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email and Payment page Cancellation policy</td>");
			PrintWriter.append("<td>Payment Page :- "+activitydetails.getCancelPolicy().size()+"</td>");
						
			if(activitydetails.getCancelPolicy().size() == confirmationDetails.getCanellationPolicy().size()){
					
				PrintWriter.append("<td>Confirmation Report :- "+confirmationDetails.getCanellationPolicy().size()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>Confirmation Report :- "+confirmationDetails.getCanellationPolicy().size()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			////
			
			if (activitydetails.getCancelPolicy().size() == 2) {
				
				testCaseCustomerMail ++;			
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Cancellation policy 1</td>");
				PrintWriter.append("<td>"+resPolist1+"</td>");
							
				if(resPolist1.equals(confirmationDetails.getCanellationPolicy().get(0))){
						
					PrintWriter.append("<td>"+confirmationDetails.getCanellationPolicy().get(0)+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCanellationPolicy().get(0)+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCustomerMail ++;			
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - No Show Fee Apply</td>");
				PrintWriter.append("<td>"+noShowActual+"</td>");
							
				if(noShowActual.equals(confirmationDetails.getCanellationPolicy().get(1))){
						
					PrintWriter.append("<td>"+confirmationDetails.getCanellationPolicy().get(1)+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCanellationPolicy().get(1)+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
			}
			
			if (activitydetails.getCancelPolicy().size() == 3) {
				
				testCaseCustomerMail ++;			
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Cancellation policy 1</td>");
				PrintWriter.append("<td>"+policyListOne+"</td>");
							
				if(policyListOne.equals(confirmationDetails.getCanellationPolicy().get(0))){
						
					PrintWriter.append("<td>"+confirmationDetails.getCanellationPolicy().get(0)+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCanellationPolicy().get(0)+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCustomerMail ++;			
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Cancellation policy 2</td>");
				PrintWriter.append("<td>"+policyListTwo+"</td>");
							
				if(policyListTwo.contains(confirmationDetails.getCanellationPolicy().get(1))){
						
					PrintWriter.append("<td>"+confirmationDetails.getCanellationPolicy().get(1)+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCanellationPolicy().get(1)+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCustomerMail ++;			
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - No Show Fee Apply</td>");
				PrintWriter.append("<td>"+noShowActual+"</td>");
							
				if(noShowActual.equals(confirmationDetails.getCanellationPolicy().get(2))){
						
					PrintWriter.append("<td>"+confirmationDetails.getCanellationPolicy().get(2)+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCanellationPolicy().get(2)+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
			}
			
			////////
			
			testCaseCustomerMail ++;			
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Tel 1</td>");
			PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Tel")+"</td>");
						
			if(PG_Properties.getProperty("Portal.Tel").toLowerCase().equals(confirmationDetails.getCCE_Tel_1().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_Tel_1()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_Tel_1()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCaseCustomerMail ++;			
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Tel 2</td>");
			PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Tel")+"</td>");
						
			if(PG_Properties.getProperty("Portal.Tel").toLowerCase().equals(confirmationDetails.getCCE_Tel_2().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_Tel_2()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_Tel_2()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCaseCustomerMail ++;			
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Email 1</td>");
			PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Email")+"</td>");
						
			if(PG_Properties.getProperty("Portal.Email").toLowerCase().equals(confirmationDetails.getCCE_Email_1().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_Email_1()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_Email_1()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCaseCustomerMail ++;			
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Email 2</td>");
			PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Email")+"</td>");
						
			if(PG_Properties.getProperty("Portal.Email").toLowerCase().equals(confirmationDetails.getCCE_Email_2().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_Email_2()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_Email_2()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCaseCustomerMail ++;			
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Website</td>");
			PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Website")+"</td>");
						
			if(PG_Properties.getProperty("Portal.Website").toLowerCase().equals(confirmationDetails.getCCE_Website().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_Website()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_Website()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCaseCustomerMail ++;			
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Fax</td>");
			PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Fax")+"</td>");
						
			if(PG_Properties.getProperty("Portal.Fax").toLowerCase().equals(confirmationDetails.getCCE_Fax().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_Fax()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_Fax()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCaseCustomerMail ++;			
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Company Name</td>");
			PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Name")+"</td>");
						
			if(PG_Properties.getProperty("Portal.Name").toLowerCase().equals(confirmationDetails.getCCE_CompanyName().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_CompanyName()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_CompanyName()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
		
		
		
		}else{
			
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Email - Customer Confirmation Mail</td>");
			PrintWriter.append("<td>Customer Confirmation Mail should be available</td>");
			PrintWriter.append("<td>Not Available</td>");
			PrintWriter.append("<td class='Failed'>FAIL</td><tr>");			
			
		}
		
		PrintWriter.append("</table>");	
		
		PrintWriter.append("<br><br>");
		PrintWriter.append("<p class='fontStyles'>Email - Customer Voucher Email</p>");
		
		testCaseVoucherMail = 1;
		PrintWriter.append("<br><table ><tr><th>Test Case No</th><th>Test Description</th><th>Expect Values - [Excel Value]</th><th>Actual Values</th><th>Test Status</th></tr>");
		
		if (confirmationDetails.isCustomerVoucherMailLoaded() == true) {

			String leadPassengr = confirmationDetails.getCVE_LeadPassenger().replace(" ", "");
			String cusName;
			
			if (UserTypeDC_B2B == TourOperatorType.DC) {
				cusName = activitydetails.getPaymentPage_Title() + activitydetails.getPaymentPage_FName() + activitydetails.getPaymentPage_LName();
				
			}else{
				cusName = activitydetails.getResultsPage_cusTitle().get(0) + activitydetails.getResultsPage_cusFName().get(0) + activitydetails.getResultsPage_cusLName().get(0);
				
			}
			
								
			PrintWriter.append("<tr><td>"+testCaseVoucherMail+"</td> <td>Customer Voucher EMail - Lead Passenger Name</td>");
			PrintWriter.append("<td>"+cusName+"</td>");
						
			if(leadPassengr.contains(cusName)){
					
				PrintWriter.append("<td>"+leadPassengr+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+leadPassengr+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCaseVoucherMail ++;	
			PrintWriter.append("<tr><td>"+testCaseVoucherMail+"</td> <td>Customer Voucher EMail - Booking No</td>");
			PrintWriter.append("<td>"+activitydetails.getReservationNo()+"</td>");
			
			if(activitydetails.getReservationNo().equals(confirmationDetails.getCVE_BookingNo())){
				
				PrintWriter.append("<td>"+confirmationDetails.getCVE_BookingNo()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCVE_BookingNo()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
			String cveIssueDate = currentDateforMatch;
			
			testCaseVoucherMail ++;	
			PrintWriter.append("<tr><td>"+testCaseVoucherMail+"</td> <td>Customer Voucher EMail - Booking Issue Date</td>");
			PrintWriter.append("<td>"+cveIssueDate+"</td>");
			
			if(cveIssueDate.replace("-", "").equals(confirmationDetails.getCVE_IssueDate().replace("-", ""))){
				
				PrintWriter.append("<td>"+confirmationDetails.getCVE_IssueDate()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCVE_IssueDate()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCaseVoucherMail++;
			PrintWriter.append("<tr><td>"+testCaseVoucherMail+"</td> <td>Customer Voucher EMail - Acitivity Name</td>");
			PrintWriter.append("<td>"+inventory.getActivityName()+"</td>");
			
			if(inventory.getActivityName().replaceAll(" ", "").toLowerCase().contains(confirmationDetails.getCVE_ActivityName().replaceAll(" ", "").toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCVE_ActivityName()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getCVE_ActivityName()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			///////
			testCaseVoucherMail ++;
			PrintWriter.append("<tr><td>"+testCaseVoucherMail+"</td> <td>Customer Voucher Email - Booking Status</td>");
			PrintWriter.append("<td>"+activitydetails.getBookingStatus()+"</td>");
			
			if (activitydetails.getBookingStatus().toLowerCase().contains("request")) {
				
				if(activitydetails.getBookingStatus().replace("-", "").toLowerCase().contains(confirmationDetails.getCVE_bookingStatus().replace(" ", "").toLowerCase())){
					
					PrintWriter.append("<td>"+confirmationDetails.getCVE_bookingStatus()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+confirmationDetails.getCVE_bookingStatus()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
			} else {

				if(activitydetails.getBookingStatus().toLowerCase().contains(confirmationDetails.getCVE_bookingStatus().toLowerCase())){
					
					PrintWriter.append("<td>"+confirmationDetails.getCVE_bookingStatus()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+confirmationDetails.getCVE_bookingStatus()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
			}
			
			///////
			
			
			
			testCaseVoucherMail ++;
			PrintWriter.append("<tr><td>"+testCaseVoucherMail+"</td> <td>Customer Voucher Email - Program City</td>");
			PrintWriter.append("<td>"+search.getDestination()+"</td>");
						
			if(search.getDestination().toLowerCase().contains(confirmationDetails.getCVE_ProgramCity().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCVE_ProgramCity()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getCVE_ProgramCity()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			////
			
			
			
			testCaseVoucherMail ++;
			PrintWriter.append("<tr><td>"+testCaseVoucherMail+"</td> <td>Customer Voucher Email - Supplier Address</td>");
			PrintWriter.append("<td>"+inventory.getSupplier_Add()+"</td>");
						
			if(confirmationDetails.getCVE_SupplierAdd().replaceAll(" ", "").toLowerCase().contains(inventory.getSupplier_Add().replaceAll(" ", "").toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCVE_SupplierAdd()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getCVE_SupplierAdd()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			testCaseVoucherMail ++;
			PrintWriter.append("<tr><td>"+testCaseVoucherMail+"</td> <td>Customer Voucher Email - Supplier TP</td>");
			PrintWriter.append("<td>"+inventory.getSupplier_TP()+"</td>");
						
			if(inventory.getSupplier_TP().replace("-", "").toLowerCase().equals(confirmationDetails.getCVE_SupplierTP().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCVE_SupplierTP()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getCVE_SupplierTP()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			////
			
			testCaseVoucherMail++;
			PrintWriter.append("<tr><td>"+testCaseVoucherMail+"</td> <td>Customer Voucher EMail - Acitivity Name - 2</td>");
			PrintWriter.append("<td>"+inventory.getActivityName()+"</td>");
			
			if(inventory.getActivityName().toLowerCase().contains(confirmationDetails.getCVE_ActivityTransferDes().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCVE_ActivityTransferDes()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getCVE_ActivityTransferDes()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			testCaseVoucherMail ++;
			PrintWriter.append("<tr><td>"+testCaseVoucherMail+"</td> <td>Customer Voucher Email - Activity Rate type </td>");
			PrintWriter.append("<td>"+activitydetails.getActivityRateType()+"</td>");
						
			if(confirmationDetails.getCVE_RatePlan().toLowerCase().contains(activitydetails.getActivityRateType().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCVE_RatePlan()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getCVE_RatePlan()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			testCaseVoucherMail ++;
			PrintWriter.append("<tr><td>"+testCaseVoucherMail+"</td> <td>Customer Voucher Email - Activity Session</td>");
			PrintWriter.append("<td>"+activitydetails.getActivitySession()+"</td>");
						
			if(confirmationDetails.getCVE_Duration().toLowerCase().contains(activitydetails.getActivitySession().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCVE_Duration()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getCVE_Duration()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			testCaseVoucherMail ++;
			PrintWriter.append("<tr><td>"+testCaseVoucherMail+"</td> <td>Customer Voucher Email - Activity QTY</td>");
			PrintWriter.append("<td>"+customerQty+"</td>");
						
			if(Integer.toString(customerQty).equals(confirmationDetails.getCVE_Qty())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCVE_Qty()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getCVE_Qty()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			String CVE_ServiceDateChanegd = search.getActivityDate();
		
			testCaseVoucherMail ++;
			PrintWriter.append("<tr><td>"+testCaseVoucherMail+"</td> <td>Customer Voucher Email - Activity Service Date</td>");
			PrintWriter.append("<td>"+CVE_ServiceDateChanegd+"</td>");
						
			if(CVE_ServiceDateChanegd.replace("-", "").equals(confirmationDetails.getCVE_ServiceDate().replace("-", ""))){
					
				PrintWriter.append("<td>"+confirmationDetails.getCVE_ServiceDate()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getCVE_ServiceDate()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			
//			testCaseVoucherMail ++;			
//			PrintWriter.append("<tr><td>"+testCaseVoucherMail+"</td> <td>Customer Voucher Email - Customer Notes</td>");
//			PrintWriter.append("<td>"+activitydetails.getCustomerNotes()+"</td>");
//						
//			if(confirmationDetails.getCVE_CustomerNotes().toLowerCase().contains(activitydetails.getCustomerNotes().toLowerCase())){
//					
//				PrintWriter.append("<td>"+confirmationDetails.getCVE_CustomerNotes()+"</td>");
//				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
//			}
//			
//			else{
//				PrintWriter.append("<td>"+confirmationDetails.getCVE_CustomerNotes()+"</td>");
//				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
//			}
//			
			
			
			testCaseVoucherMail ++;			
			PrintWriter.append("<tr><td>"+testCaseVoucherMail+"</td> <td>Customer Voucher Email - Tel 1</td>");
			PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Tel")+"</td>");
						
			if(PG_Properties.getProperty("Portal.Tel").toLowerCase().equals(confirmationDetails.getCVE_Tel_1().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCVE_Tel_1()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCVE_Tel_1()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
			testCaseVoucherMail ++;			
			PrintWriter.append("<tr><td>"+testCaseVoucherMail+"</td> <td>Customer Voucher Email - Email 1</td>");
			PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Email")+"</td>");
						
			if(PG_Properties.getProperty("Portal.Email").toLowerCase().equals(confirmationDetails.getCVE_Email_1().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCVE_Email_1()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCVE_Email_1()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
			testCaseVoucherMail ++;			
			PrintWriter.append("<tr><td>"+testCaseVoucherMail+"</td> <td>Customer Voucher Email - Website</td>");
			PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Website")+"</td>");
						
			if(PG_Properties.getProperty("Portal.Website").toLowerCase().equals(confirmationDetails.getCVE_Website().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCVE_Website()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCVE_Website()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCaseVoucherMail ++;			
			PrintWriter.append("<tr><td>"+testCaseVoucherMail+"</td> <td>Customer Voucher Email - Fax</td>");
			PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Fax")+"</td>");
						
			if(PG_Properties.getProperty("Portal.Fax").toLowerCase().equals(confirmationDetails.getCVE_Fax().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCVE_Fax()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCVE_Fax()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
			
			
			
			
		}else{
			
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Email - Customer Voucher Mail</td>");
			PrintWriter.append("<td>Customer Voucher Mail should be available</td>");
			PrintWriter.append("<td>Not Available</td>");
			PrintWriter.append("<td class='Failed'>FAIL</td><tr>");		
			
		}
		
		PrintWriter.append("</table>");
		
	}
	
	
	public void getReservationReport(WebDriver Driver){
		
		PrintWriter.append("<br><br>");
		PrintWriter.append("<p class='fontStyles'>Reservation Report Summary</p>");
		
		testCaseCountReservation = 1;
		PrintWriter.append("<br><table ><tr><th>Test Case No</th><th>Test Description</th><th>Expect Values - [Excel Value]</th><th>Actual Values</th><th>Test Status</th></tr>");
		
		if (reservationdetails.isReservationReportLoaded() == true) {
			
			PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Booking Date</td>");
			PrintWriter.append("<td>"+currentDateforMatch+"</td>");
						
			if(currentDateforMatch.equals(reservationdetails.getBookingDate())){
					
				PrintWriter.append("<td>"+reservationdetails.getBookingDate()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+reservationdetails.getBookingDate()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			
			if (activitydetails.getCancelPolicy().size() == 3) {
			
				testCaseCountReservation ++;
				PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Cancellation due Date</td>");
				PrintWriter.append("<td>"+dateWithBufferDates_3+"</td>");
							
				if(dateWithBufferDates_3.equals(reservationdetails.getCanPolicyDueDate())){
						
					PrintWriter.append("<td>"+reservationdetails.getCanPolicyDueDate()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+reservationdetails.getCanPolicyDueDate()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
			
			}else{
				
				testCaseCountReservation ++;
				PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Cancellation due Date</td>");
				PrintWriter.append("<td>"+dateWithBufferDates_1+"</td>");
							
				if(dateWithBufferDates_1.equals(reservationdetails.getCanPolicyDueDate())){
						
					PrintWriter.append("<td>"+reservationdetails.getCanPolicyDueDate()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+reservationdetails.getCanPolicyDueDate()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
			}
			
			
			if (UserTypeDC_B2B == TourOperatorType.DC) {
				
				String reservationCusName = ""+activitydetails.getPaymentPage_LName()+" "+activitydetails.getPaymentPage_FName()+"";
				
				testCaseCountReservation ++;
				PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Customer Name</td>");
				PrintWriter.append("<td>"+reservationCusName+"</td>");
							
				if(reservationCusName.equals(reservationdetails.getCustomerName_1())){
						
					PrintWriter.append("<td>"+reservationdetails.getCustomerName_1()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+reservationdetails.getCustomerName_1()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
			} else {

				testCaseCountReservation ++;			
				PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Customer Name</td>");
				PrintWriter.append("<td>"+PG_Properties.getProperty(inventory.getUserTypeDC_B2B().toString())+"</td>");
							
				if(PG_Properties.getProperty(inventory.getUserTypeDC_B2B().toString()).replaceAll(" ", "").equalsIgnoreCase(reservationdetails.getCustomerName_1().replaceAll(" ", ""))){
						
					PrintWriter.append("<td>"+reservationdetails.getCustomerName_1()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+reservationdetails.getCustomerName_1()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
			}
			
			
			testCaseCountReservation ++;
			PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Supplier Name</td>");
			PrintWriter.append("<td>"+inventory.getSupplier_Name()+"</td>");
						
			if(inventory.getSupplier_Name().equals(reservationdetails.getSupplierName())){
					
				PrintWriter.append("<td>"+reservationdetails.getSupplierName()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+reservationdetails.getSupplierName()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			testCaseCountReservation ++;
			PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Product Type</td>");
			PrintWriter.append("<td>Activities</td>");
						
			if(reservationdetails.getProductType().equalsIgnoreCase("Activities")){
					
				PrintWriter.append("<td>"+reservationdetails.getProductType()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+reservationdetails.getProductType()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			testCaseCountReservation ++;
			PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Booking Status</td>");
			PrintWriter.append("<td>Normal</td>");
						
			if(reservationdetails.getBookingStatus().equalsIgnoreCase("Normal")){
					
				PrintWriter.append("<td>"+reservationdetails.getBookingStatus()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+reservationdetails.getBookingStatus()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			testCaseCountReservation ++;
			PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Booking Channel</td>");
			PrintWriter.append("<td>Call Center</td>");
						
			if(reservationdetails.getBookingChannel().equalsIgnoreCase("Call Center")){
					
				PrintWriter.append("<td>"+reservationdetails.getBookingChannel()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+reservationdetails.getBookingChannel()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			
			
			
			testCaseCountReservation ++;
			PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Customer TP</td>");
			PrintWriter.append("<td>"+activitydetails.getPaymentPage_TP()+"</td>");
						
			if(activitydetails.getPaymentPage_TP().equals(reservationdetails.getCustomerTP())){
					
				PrintWriter.append("<td>"+reservationdetails.getCustomerTP()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+reservationdetails.getCustomerTP()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			
			
			if (activitydetails.getPaymentDetails().equals("Pay Online")) {
				
				testCaseCountReservation ++;
				PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Payment Type</td>");
				PrintWriter.append("<td>Credit Card</td>");
							
				if(reservationdetails.getPaymentType().replaceAll(" ", "").equalsIgnoreCase("Credit Card".replaceAll(" ", ""))){
						
					PrintWriter.append("<td>"+reservationdetails.getPaymentType()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+reservationdetails.getPaymentType()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				testCaseCountReservation ++;
				PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Payment Reference</td>");
				PrintWriter.append("<td>"+activitydetails.getPaymentID()+"</td>");
							
				if(activitydetails.getPaymentID().replaceAll(" ", "").equalsIgnoreCase(reservationdetails.getPayRefernce().replaceAll(" ", ""))){
						
					PrintWriter.append("<td>"+reservationdetails.getPayRefernce()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+reservationdetails.getPayRefernce()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				testCaseCountReservation ++;
				PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Auth Code</td>");
				PrintWriter.append("<td>"+activitydetails.getAuthentReference_bookingList()+"</td>");
							
				if(activitydetails.getAuthentReference_bookingList().replaceAll(" ", "").equalsIgnoreCase(reservationdetails.getAuthCode().replaceAll(" ", ""))){
						
					PrintWriter.append("<td>"+reservationdetails.getAuthCode()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+reservationdetails.getAuthCode()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				
				
				
			}else{
				
				if (UserTypeDC_B2B == TourOperatorType.NETCREDITLPON || UserTypeDC_B2B == TourOperatorType.NETCREDITLPOY) {
					
					testCaseCountReservation ++;
					PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Payment Type</td>");
					PrintWriter.append("<td>On Credit</td>");
								
					if(reservationdetails.getPaymentType().replaceAll(" ", "").equalsIgnoreCase("On Credit".replaceAll(" ", ""))){
							
						PrintWriter.append("<td>"+reservationdetails.getPaymentType()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {
						
						PrintWriter.append("<td>"+reservationdetails.getPaymentType()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					testCaseCountReservation ++;
					PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Payment Reference</td>");
					PrintWriter.append("<td>Not Available</td>");
								
					if(("-").equalsIgnoreCase(reservationdetails.getPayRefernce().replaceAll(" ", ""))){
							
						PrintWriter.append("<td>Not Available</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {
						
						PrintWriter.append("<td>"+reservationdetails.getPayRefernce()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
				}else{
					
					testCaseCountReservation ++;
					PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Payment Type</td>");
					PrintWriter.append("<td>Cash</td>");
								
					if(reservationdetails.getPaymentType().replaceAll(" ", "").equalsIgnoreCase("Cash".replaceAll(" ", ""))){
							
						PrintWriter.append("<td>"+reservationdetails.getPaymentType()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {
						
						PrintWriter.append("<td>"+reservationdetails.getPaymentType()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					testCaseCountReservation ++;
					PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Payment Reference</td>");
					PrintWriter.append("<td>"+activitydetails.getPaymentReference_bookingList()+"</td>");
								
					if(activitydetails.getPaymentReference_bookingList().replaceAll(" ", "").equalsIgnoreCase(reservationdetails.getPayRefernce().replaceAll(" ", ""))){
							
						PrintWriter.append("<td>"+reservationdetails.getPayRefernce()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {
						
						PrintWriter.append("<td>"+reservationdetails.getPayRefernce()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
				}
							
			}
			
			
			
			
			
			
			
			
			if (activitydetails.getPaymentDetails().equals("Pay Online")) {
				
				testCaseCountReservation ++;
				PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Invoice Date</td>");
				PrintWriter.append("<td>"+currentDateforMatch+"</td>");
							
				if(currentDateforMatch.equals(reservationdetails.getInvoiceIssuedDate())){
						
					PrintWriter.append("<td>"+reservationdetails.getInvoiceIssuedDate()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+reservationdetails.getInvoiceIssuedDate()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
					
			}
			
			testCaseCountReservation ++;
			PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Selling Currency - 1</td>");
			PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
						
			if(search.getSellingCurrency().toLowerCase().equals(reservationdetails.getSellingCurrency_1().toLowerCase())){
					
				PrintWriter.append("<td>"+reservationdetails.getSellingCurrency_1()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+reservationdetails.getSellingCurrency_1()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			if (UserTypeDC_B2B == TourOperatorType.COMCASH || UserTypeDC_B2B == TourOperatorType.COMCREDITLPONO || UserTypeDC_B2B == TourOperatorType.COMCREDITLPOY) {
				
				int totalRate = 0;
				if (activitydetails.getPaymentDetails().equals("Pay Online")) {
					totalRate = ResultsPage_TotalPayable - discountedVale; 
				}else{
					totalRate = ResultsPage_TotalPayable - discountedVale; 
					
				}
				
				testCaseCountReservation ++;
				PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Total Rate</td>");
				PrintWriter.append("<td>"+(totalRate)+"</td>");
							
				if(Integer.toString(totalRate).equals(reservationdetails.getTotalRate())){
						
					PrintWriter.append("<td>"+reservationdetails.getTotalRate()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+reservationdetails.getTotalRate()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				
			}else{
				
				int totalRate = 0;
				if (activitydetails.getPaymentDetails().equals("Pay Online")) {
					totalRate = ResultsPage_TotalPayable - discountedVale; 
				}else{
					totalRate = ResultsPage_TotalPayable - discountedVale; 
					
				}
				
				testCaseCountReservation ++;
				PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Total Rate</td>");
				PrintWriter.append("<td>"+totalRate+"</td>");
							
				if(Integer.toString(totalRate).equals(reservationdetails.getTotalRate())){
						
					PrintWriter.append("<td>"+reservationdetails.getTotalRate()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+reservationdetails.getTotalRate()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				
			}
			
			
			
			
			
			
			
			
			
			int onlinePaid = ResultsPage_TotalPayable + creditTax;
			
			if (activitydetails.getPaymentDetails().equals("Pay Online")) {
				
				testCaseCountReservation ++;
				PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - CreditCardFee</td>");
				PrintWriter.append("<td>"+creditTax+"</td>");
							
				if(Integer.toString(creditTax).equals(reservationdetails.getCreditCardFee())){
						
					PrintWriter.append("<td>"+reservationdetails.getCreditCardFee()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+reservationdetails.getCreditCardFee()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				
				
				if (UserTypeDC_B2B == TourOperatorType.COMCASH || UserTypeDC_B2B == TourOperatorType.COMCREDITLPONO || UserTypeDC_B2B == TourOperatorType.COMCREDITLPOY) {
					
					testCaseCountReservation ++;
					PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Amount paid</td>");
					PrintWriter.append("<td>"+(onlinePaid - toAgentCommission - discountedVale)+"</td>");
								
					if(Integer.toString(onlinePaid - toAgentCommission - discountedVale).equals(reservationdetails.getAmountPaid())){
							
						PrintWriter.append("<td>"+reservationdetails.getAmountPaid()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {
						
						PrintWriter.append("<td>"+reservationdetails.getAmountPaid()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					
				}else{
					
					testCaseCountReservation ++;
					PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Amount paid</td>");
					PrintWriter.append("<td>"+(onlinePaid - discountedVale)+"</td>");
								
					if(Integer.toString(onlinePaid - discountedVale).equals(reservationdetails.getAmountPaid())){
							
						PrintWriter.append("<td>"+reservationdetails.getAmountPaid()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {
						
						PrintWriter.append("<td>"+reservationdetails.getAmountPaid()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					
				}
				
			
			}
			
			
			int offlineCardFee = 0;
			int offlineAmountPaid = 0;
			int offlinePaid = ResultsPage_TotalPayable + offlineCardFee;
			
			if (activitydetails.getPaymentDetails().equals("Pay Offline")) { 
				
				testCaseCountReservation ++;
				PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - CreditCardFee</td>");
				PrintWriter.append("<td>"+offlineCardFee+"</td>");
							
				if(Integer.toString(offlineCardFee).equals(reservationdetails.getCreditCardFee())){
						
					PrintWriter.append("<td>"+reservationdetails.getCreditCardFee()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+reservationdetails.getCreditCardFee()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				
				testCaseCountReservation ++;
				PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Amount paid</td>");
				PrintWriter.append("<td>"+offlineAmountPaid+"</td>");
							
				if(Integer.toString(offlineAmountPaid).equals(reservationdetails.getAmountPaid())){
						
					PrintWriter.append("<td>"+reservationdetails.getAmountPaid()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+reservationdetails.getAmountPaid()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				
			}
			
			/////////
			
			testCaseCountReservation ++;
			PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Selling Currency - 2</td>");
			PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
						
			if(search.getSellingCurrency().toLowerCase().equals(reservationdetails.getSellingCurrency_2().toLowerCase())){
					
				PrintWriter.append("<td>"+reservationdetails.getSellingCurrency_2()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+reservationdetails.getSellingCurrency_2()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			
			if (activitydetails.getPaymentDetails().equals("Pay Online")) {
			
				
				if (UserTypeDC_B2B == TourOperatorType.COMCASH || UserTypeDC_B2B == TourOperatorType.COMCREDITLPONO || UserTypeDC_B2B == TourOperatorType.COMCREDITLPOY) {
					
					testCaseCountReservation ++;
					PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Gross order Value</td>");
					PrintWriter.append("<td>"+(onlinePaid - discountedVale)+"</td>");
								
					if(Integer.toString(onlinePaid - discountedVale).equals(reservationdetails.getGrossOrderValue())){
							
						PrintWriter.append("<td>"+reservationdetails.getGrossOrderValue()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {
						
						PrintWriter.append("<td>"+reservationdetails.getGrossOrderValue()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					
				}else{
					
					testCaseCountReservation ++;
					PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Gross order Value</td>");
					PrintWriter.append("<td>"+(onlinePaid - discountedVale)+"</td>");
								
					if(Integer.toString(onlinePaid - discountedVale).equals(reservationdetails.getGrossOrderValue())){
							
						PrintWriter.append("<td>"+reservationdetails.getGrossOrderValue()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {
						
						PrintWriter.append("<td>"+reservationdetails.getGrossOrderValue()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
				}
				
				
				
				int netorderValue = onlinePaid - Integer.parseInt(reservationdetails.getAgentCommission()) - discountedVale;
				
				testCaseCountReservation ++;
				PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Net order Value</td>");
				PrintWriter.append("<td>"+netorderValue+"</td>");
							
				if(Integer.toString(netorderValue).equals(reservationdetails.getNetOrderValue())){
						
					PrintWriter.append("<td>"+reservationdetails.getNetOrderValue()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+reservationdetails.getNetOrderValue()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				
				int onlineTotalCost = netRate + PaymentPage_TotalTax;
				
				testCaseCountReservation ++;
				PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Total Cost</td>");
				PrintWriter.append("<td>"+onlineTotalCost+"</td>");
							
				if(Integer.toString(onlineTotalCost).equals(reservationdetails.getTotalCost())){
						
					PrintWriter.append("<td>"+reservationdetails.getTotalCost()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+reservationdetails.getTotalCost()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
			}
			
			
			
			if (activitydetails.getPaymentDetails().equals("Pay Offline")) { 
				
				
				if (UserTypeDC_B2B == TourOperatorType.COMCASH || UserTypeDC_B2B == TourOperatorType.COMCREDITLPONO || UserTypeDC_B2B == TourOperatorType.COMCREDITLPOY) {
					
					testCaseCountReservation ++;
					PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Gross order Value</td>");
					PrintWriter.append("<td>"+(offlinePaid - discountedVale)+"</td>");
								
					if(Integer.toString(offlinePaid - discountedVale).equals(reservationdetails.getGrossOrderValue())){
							
						PrintWriter.append("<td>"+reservationdetails.getGrossOrderValue()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {
						
						PrintWriter.append("<td>"+reservationdetails.getGrossOrderValue()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					
				}else{
					
					testCaseCountReservation ++;
					PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Gross order Value</td>");
					PrintWriter.append("<td>"+(offlinePaid - discountedVale)+"</td>");
								
					if(Integer.toString(offlinePaid - discountedVale).equals(reservationdetails.getGrossOrderValue())){
							
						PrintWriter.append("<td>"+reservationdetails.getGrossOrderValue()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {
						
						PrintWriter.append("<td>"+reservationdetails.getGrossOrderValue()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
				}
				
				
				
				
				int netorderValue = offlinePaid - Integer.parseInt(reservationdetails.getAgentCommission()) - discountedVale ;
				
				testCaseCountReservation ++;
				PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Net order Value</td>");
				PrintWriter.append("<td>"+netorderValue+"</td>");
							
				if(Integer.toString(netorderValue).equals(reservationdetails.getNetOrderValue())){
						
					PrintWriter.append("<td>"+reservationdetails.getNetOrderValue()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+reservationdetails.getNetOrderValue()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				
				int offlineTotalCost = netRate + PaymentPage_TotalTax;
				
				testCaseCountReservation ++;
				PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Total Cost</td>");
				PrintWriter.append("<td>"+offlineTotalCost+"</td>");
							
				if(Integer.toString(offlineTotalCost).equals(reservationdetails.getTotalCost())){
						
					PrintWriter.append("<td>"+reservationdetails.getTotalCost()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+reservationdetails.getTotalCost()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
					
			}
			
			if (UserTypeDC_B2B == TourOperatorType.COMCASH || UserTypeDC_B2B == TourOperatorType.COMCREDITLPOY || UserTypeDC_B2B == TourOperatorType.COMCREDITLPONO) {
				
				testCaseCountReservation ++;
				PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Agent Commission</td>");
				PrintWriter.append("<td>"+toAgentCommission+"</td>");
							
				if(Integer.toString(toAgentCommission).equals(reservationdetails.getAgentCommission())){
						
					PrintWriter.append("<td>"+reservationdetails.getAgentCommission()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+reservationdetails.getAgentCommission()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
			}else{
				
				testCaseCountReservation ++;
				PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Agent Commission</td>");
				PrintWriter.append("<td>"+toAgentCommission+"</td>");
							
				if(Integer.toString(toAgentCommission).equals(reservationdetails.getAgentCommission())){
						
					PrintWriter.append("<td>"+reservationdetails.getAgentCommission()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+reservationdetails.getAgentCommission()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
			}
			
			//////
			
			if (activitydetails.getPaymentDetails().equals("Pay Online")) {
				
				totalCostInUsd = netRate + creditTax + ResultsPage_Taxes;
				
			}
			
			if (activitydetails.getPaymentDetails().equals("Pay Offline")) { 
				
				totalCostInUsd = netRate + offlineCardFee + ResultsPage_Taxes;
				
			}
		
			
			if((PG_Properties.getProperty("PortalCurrency").toLowerCase().equals(inventory.getSupplierCurrency().toLowerCase()))){
				
				testCaseCountReservation ++;			
				PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Net Order value In USD</td>");
				PrintWriter.append("<td>"+PaymentPage_AmountProcessed+"</td>");
							
				if(Integer.toString(PaymentPage_AmountProcessed).equals(reservationdetails.getBaseCurrency_NetOrgerValue())){
						
					PrintWriter.append("<td>"+reservationdetails.getBaseCurrency_NetOrgerValue()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+reservationdetails.getBaseCurrency_NetOrgerValue()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				
				testCaseCountReservation ++;			
				PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Total cost In USD</td>");
				PrintWriter.append("<td>"+totalCostInUsd+"</td>");
							
				if(Integer.toString(totalCostInUsd).equals(reservationdetails.getBaseCurrency_TotalCost())){
						
					PrintWriter.append("<td>"+reservationdetails.getBaseCurrency_TotalCost()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+reservationdetails.getBaseCurrency_TotalCost()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				
			}
			
			else{
				
				for(Entry<String, String> entry: currencyMap.entrySet()) {
					if(inventory.getSupplierCurrency().toLowerCase().equals(entry.getKey().toLowerCase())){
						
						rateConvertforUSD = Double.parseDouble(entry.getValue());
						int ResultsPage_TotalPayable_ForUSD = 0;
						
						if (!(UserTypeDC_B2B == TourOperatorType.DC)) {
							
							if (UserTypeDC_B2B == TourOperatorType.COMCASH || UserTypeDC_B2B == TourOperatorType.COMCREDITLPONO || UserTypeDC_B2B == TourOperatorType.COMCREDITLPOY) {
								
								ResultsPage_TotalPayable_ForUSD =  (int) Math.ceil((( Double.parseDouble(activitydetails.getToActivity_TotalValue()) + Double.parseDouble(inventory.getCreditCard_Fee()) +  - Double.parseDouble(activitydetails.getToAgentCommission()) ) / rateConvertforUSD ));																
								
							}
							if (UserTypeDC_B2B == TourOperatorType.NETCASH || UserTypeDC_B2B == TourOperatorType.NETCREDITLPON || UserTypeDC_B2B == TourOperatorType.NETCREDITLPOY) {
								
								ResultsPage_TotalPayable_ForUSD =  (int) Math.ceil((( Double.parseDouble(activitydetails.getToActivity_TotalValue()) + Double.parseDouble(inventory.getCreditCard_Fee())   ) / rateConvertforUSD ));																
																	
							}
							
						
						}else{
							
							ResultsPage_TotalPayable_ForUSD =  (int) Math.ceil((Double.parseDouble(inventory.getPaymentPage_TotalPackageValue()) / rateConvertforUSD ));
						}
						
						

						
						
						testCaseCountReservation ++;			
						PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Net Order value In USD</td>");
						PrintWriter.append("<td>"+ResultsPage_TotalPayable_ForUSD+"</td>");
									
						if(Integer.toString(ResultsPage_TotalPayable_ForUSD).equals(reservationdetails.getBaseCurrency_NetOrgerValue())){
								
							PrintWriter.append("<td>"+reservationdetails.getBaseCurrency_NetOrgerValue()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+reservationdetails.getBaseCurrency_NetOrgerValue()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						///
						
						int netRateForUSD = (int)(((Double.parseDouble(inventory.getResultsPage_SubTotal()) * 100 ) / (100 + pMarkup)));
						int PaymentPage_CreditCardFeeUSD =  (int) Math.ceil((Double.parseDouble(inventory.getCreditCard_Fee())));
						int	ResultsPage_TaxesUSD =  (int) Math.ceil((Double.parseDouble(inventory.getResultsPage_Taxes())));
					
						int tCostInUSD = (int) Math.ceil((netRateForUSD + PaymentPage_CreditCardFeeUSD + ResultsPage_TaxesUSD) / rateConvertforUSD );
						
						testCaseCountReservation ++;			
						PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report - Total cost In USD</td>");
						PrintWriter.append("<td>"+tCostInUSD+"</td>");
									
						if(Integer.toString(tCostInUSD).equals(reservationdetails.getBaseCurrency_TotalCost())){
								
							PrintWriter.append("<td>"+reservationdetails.getBaseCurrency_TotalCost()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+reservationdetails.getBaseCurrency_TotalCost()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
					}
				}
			}
			
		
		
		
		}else{
			
			PrintWriter.append("<tr><td>"+testCaseCountReservation+"</td> <td>Reservation report</td>");
			PrintWriter.append("<td>Reservation report availability</td>");
			PrintWriter.append("<td>Not Available</td>");
			PrintWriter.append("<td class='Failed'>FAIL</td><tr>");		
								
		}
	
		
		PrintWriter.append("</table>");	
		
		
	}
	
	
	public void getcancellationModificationSummary(WebDriver Driver) throws IOException{
		
		mailPrinter = new StringBuffer();
		mailPrinter.append(PrintWriter);
		
		if (activitydetails.isActivityResultsAvailble() == true) {
			
			if (activitydetails.isAddToCart() == true) {
				
				//Cancellation
				
				mailPrinter.append("<br><br>");
				mailPrinter.append("<p class='fontStyles'>Cancellation Summary</p>");
				
				testCaseCancel = 1;
				mailPrinter.append("<br><table ><tr><th>Test Case No</th><th>Test Description</th><th>Expect Values - [Excel Value]</th><th>Actual Values</th><th>Test Status</th></tr>");
				
				if (cancellationDetails.isCancelReportLoaded() == true) {
				
					mailPrinter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Reservation No</td>");
					mailPrinter.append("<td>"+activitydetails.getReservationNo()+"</td>");
					
					if(activitydetails.getReservationNo().contains(cancellationDetails.getReservationNo_1())){
						
						mailPrinter.append("<td>"+cancellationDetails.getReservationNo_1()+"</td>");
						mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						mailPrinter.append("<td>"+cancellationDetails.getReservationNo_1()+"</td>");
						mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					testCaseCancel++;
					mailPrinter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Cancellation Status</td>");
					
					if(cancellationDetails.getCancellationStatus().equals("Yes")){
						
						mailPrinter.append("<td>Yes</td>");
						mailPrinter.append("<td>"+cancellationDetails.getCancellationStatus()+"</td>");
						mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						
						mailPrinter.append("<td>No</td>");
						mailPrinter.append("<td>"+cancellationDetails.getCancellationStatus()+"</td>");
						mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					testCaseCancel++;
					mailPrinter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Cancellation Comments</td>");
					
					if(cancellationDetails.getCancellationComment().toLowerCase().contains("Cancellation can be done".toLowerCase())){
						
						mailPrinter.append("<td>Cancellation can be done</td>");
						mailPrinter.append("<td>"+cancellationDetails.getCancellationComment()+"</td>");
						mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						mailPrinter.append("<td>Cancellation cannot be done</td>");
						mailPrinter.append("<td>"+cancellationDetails.getCancellationComment()+"</td>");
						mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					
					if(cancellationDetails.getCancellationStatus().equals("Yes")){
						
						testCaseCancel++;
						mailPrinter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Customer Last Name</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_LName()+"</td>");
						
						if(activitydetails.getPaymentPage_LName().equals(cancellationDetails.getCustomerName())){
							
							mailPrinter.append("<td>"+cancellationDetails.getCustomerName()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+cancellationDetails.getCustomerName()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCancel++;
						mailPrinter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Reservation No [Cancel Reservations Page]</td>");
						mailPrinter.append("<td>"+activitydetails.getReservationNo()+"</td>");
						
						if(activitydetails.getReservationNo().contains(cancellationDetails.getRseervationNo_3())){
							
							mailPrinter.append("<td>"+cancellationDetails.getRseervationNo_3()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+cancellationDetails.getRseervationNo_3()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCancel++;
						mailPrinter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Payment Details</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentDetails()+"</td>");
						
						if(activitydetails.getPaymentDetails().toLowerCase().contains(cancellationDetails.getPaymentType().toLowerCase())){
							
							mailPrinter.append("<td>"+cancellationDetails.getPaymentType()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+cancellationDetails.getPaymentType()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCancel++;
						mailPrinter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Activity Program Name</td>");
						mailPrinter.append("<td>"+inventory.getActivityName()+"</td>");
						
						if(inventory.getActivityName().toLowerCase().contains(cancellationDetails.getProgramName_1().toLowerCase())){
							
							mailPrinter.append("<td>"+cancellationDetails.getProgramName_1()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+cancellationDetails.getProgramName_1()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCancel++;
						mailPrinter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Activity Name</td>");
						mailPrinter.append("<td>"+activitydetails.getActivityName_result()+"</td>");
						
						if(cancellationDetails.getActivityName_1().toLowerCase().contains(activitydetails.getActivityName_result().toLowerCase())){
							
							mailPrinter.append("<td>"+cancellationDetails.getActivityName_1()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+cancellationDetails.getActivityName_1()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCancel++;
						mailPrinter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Reservation Date</td>");
						mailPrinter.append("<td>"+activitydetails.getCurrentDate()+"</td>");
						
						if(activitydetails.getCurrentDate().equals(cancellationDetails.getReservationDate())){
							
							mailPrinter.append("<td>"+cancellationDetails.getReservationDate()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+cancellationDetails.getReservationDate()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCancel++;
						mailPrinter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Activity Session</td>");
						mailPrinter.append("<td>"+activitydetails.getActivitySession()+"</td>");
						
						if(cancellationDetails.getActivity_Session().contains(activitydetails.getActivitySession())){
							
							mailPrinter.append("<td>"+cancellationDetails.getActivity_Session()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+cancellationDetails.getActivity_Session()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCancel++;
						mailPrinter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Activity Program Name-2</td>");
						mailPrinter.append("<td>"+inventory.getActivityName()+"</td>");
						
						if(inventory.getActivityName().toLowerCase().contains(cancellationDetails.getProgramName_2().toLowerCase())){
							
							mailPrinter.append("<td>"+cancellationDetails.getProgramName_2()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+cancellationDetails.getProgramName_2()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						testCaseCancel++;
						mailPrinter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Activity Name in Rate Details</td>");
						mailPrinter.append("<td>"+activitydetails.getActivityName_result()+"</td>");
						
						if(cancellationDetails.getActivityName_2().toLowerCase().contains(activitydetails.getActivityName_result().toLowerCase())){
							
							mailPrinter.append("<td>"+cancellationDetails.getActivityName_2()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+cancellationDetails.getActivityName_2()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCancel++;
						mailPrinter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Activity Rate Plan</td>");
						mailPrinter.append("<td>"+activitydetails.getActivityRateType()+"</td>");
						
						if(cancellationDetails.getActivity_ratePlan().contains(activitydetails.getActivityRateType())){
							
							mailPrinter.append("<td>"+cancellationDetails.getActivity_ratePlan()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+cancellationDetails.getActivity_ratePlan()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						testCaseCancel++;
						mailPrinter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Activity QTY</td>");
						mailPrinter.append("<td>"+customerQty+"</td>");
						
						if(Integer.toString(customerQty).equals(cancellationDetails.getActivity_QTY())){
							
							mailPrinter.append("<td>"+cancellationDetails.getActivity_QTY()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+cancellationDetails.getActivity_QTY()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						///
						
						testCaseCancel++;
						mailPrinter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Activity Rate Value</td>");
						mailPrinter.append("<td>"+amount_onePerson+"</td>");
						
						if(Integer.toString(amount_onePerson).equals(cancellationDetails.getActivity_RateUSD())){
							
							mailPrinter.append("<td>"+cancellationDetails.getActivity_RateUSD()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+cancellationDetails.getActivity_RateUSD()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCancel++;
						mailPrinter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Activity Rate Currency</td>");
						mailPrinter.append("<td>"+search.getSellingCurrency()+"</td>");
						
						if(search.getSellingCurrency().toLowerCase().equals(cancellationDetails.getActivity_RateCurrency().toLowerCase())){
							
							mailPrinter.append("<td>"+cancellationDetails.getActivity_RateCurrency()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+cancellationDetails.getActivity_RateCurrency()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						////
						
						testCaseCancel++;
						mailPrinter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Total Rate Value</td>");
						mailPrinter.append("<td>"+ResultsPage_SubTotal+"</td>");
						
						if(Integer.toString(ResultsPage_SubTotal).equals(cancellationDetails.getActivity_TotalValue())){
							
							mailPrinter.append("<td>"+cancellationDetails.getActivity_TotalValue()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+cancellationDetails.getActivity_TotalValue()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCancel++;
						mailPrinter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Total Rate Currency</td>");
						mailPrinter.append("<td>"+search.getSellingCurrency()+"</td>");
						
						if(search.getSellingCurrency().toLowerCase().equals(cancellationDetails.getActivity_TotalValueCurrency().toLowerCase())){
							
							mailPrinter.append("<td>"+cancellationDetails.getActivity_TotalValueCurrency()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+cancellationDetails.getActivity_TotalValueCurrency()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCancel++;
						mailPrinter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Customer Email</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_Email()+"</td>");
						
						if(activitydetails.getPaymentPage_Email().toLowerCase().equals(cancellationDetails.getCustomer_Mail().toLowerCase())){
							
							mailPrinter.append("<td>"+cancellationDetails.getCustomer_Mail()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+cancellationDetails.getCustomer_Mail()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCancel++;
						mailPrinter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Internal Notes</td>");
						mailPrinter.append("<td>"+activitydetails.getInternalNotes()+"</td>");
						
						if(cancellationDetails.getInternalNotes().toLowerCase().contains(activitydetails.getInternalNotes().toLowerCase())){
							
							mailPrinter.append("<td>"+cancellationDetails.getInternalNotes()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+cancellationDetails.getInternalNotes()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
					}
					
					
					
				}else {
					mailPrinter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Report</td>");
					mailPrinter.append("<td>Cancellation report availability</td>");
					mailPrinter.append("<td>Not Available</td>");
					mailPrinter.append("<td class='Failed'>FAIL</td><tr>");		
					
					
				}
				
					
				mailPrinter.append("</table>");
	
				
				
				//////////////////////////////////////////////////////////////////////////
				//Modification
				
				
				mailPrinter.append("<br><br>");
				mailPrinter.append("<p class='fontStyles'>Modification Summary</p>");
				
				testCaseModify = 1;
				mailPrinter.append("<br><table><tr><th>Test Case No</th><th>Test Description</th><th>Expect Values - [Excel Value]</th><th>Actual Values</th><th>Test Status</th></tr>");
				
				if (modDetails.isModReportLoaded() == true) {
					
					mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification Summary - Modification Status</td>");
					
					if(modDetails.getModificationStatus().equals("Yes")){
						
						mailPrinter.append("<td>Yes</td>");
						mailPrinter.append("<td>"+modDetails.getModificationStatus()+"</td>");
						mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						
						mailPrinter.append("<td>No</td>");
						mailPrinter.append("<td>"+modDetails.getModificationStatus()+"</td>");
						mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					testCaseModify++;
					mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification Summary - Modification Comments</td>");
					
					if(modDetails.getModificationComment().toLowerCase().contains("Modification can be done".toLowerCase())){
						
						mailPrinter.append("<td>Modification can be done</td>");
						mailPrinter.append("<td>"+modDetails.getModificationComment()+"</td>");
						mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						mailPrinter.append("<td>Modification cannot be done</td>");
						mailPrinter.append("<td>"+modDetails.getModificationComment()+"</td>");
						mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					if(modDetails.getModificationStatus().equals("Yes")){
						
						mailPrinter.append("<tr><td class='fontiiii'>Modification - Summary</td><tr>"); 
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Customer Last Name</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_LName()+"</td>");
						
						if(activitydetails.getPaymentPage_LName().equals(modDetails.getSummary_GuestLastname())){
							
							mailPrinter.append("<td>"+modDetails.getSummary_GuestLastname()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_GuestLastname()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Reservation No</td>");
						mailPrinter.append("<td>"+activitydetails.getReservationNo()+"</td>");
						
						if(activitydetails.getReservationNo().contains(modDetails.getSummary_ReservNo())){
							
							mailPrinter.append("<td>"+modDetails.getSummary_ReservNo()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_ReservNo()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Reservation No [Booking Summary]</td>");
						mailPrinter.append("<td>"+activitydetails.getReservationNo()+"</td>");
						
						if(activitydetails.getReservationNo().contains(modDetails.getSummary_reservationNo())){
							
							mailPrinter.append("<td>"+modDetails.getSummary_reservationNo()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_reservationNo()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Booking Date [Booking Summary]</td>");
						mailPrinter.append("<td>"+activitydetails.getCurrentDate()+"</td>");
						
						if(activitydetails.getCurrentDate().equals(modDetails.getSummary_reservationDate())){
							
							mailPrinter.append("<td>"+modDetails.getSummary_reservationDate()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_reservationDate()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify ++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Activity program Name [Program Summary]</td>");
						mailPrinter.append("<td>"+inventory.getActivityName()+"</td>");
									
						if(inventory.getActivityName().toLowerCase().contains(modDetails.getSummary_ProgramName().toLowerCase())){
								
							mailPrinter.append("<td>"+modDetails.getSummary_ProgramName()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							mailPrinter.append("<td>"+modDetails.getSummary_ProgramName()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						testCaseModify ++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Supplier Name [Program Summary]</td>");
						mailPrinter.append("<td>"+inventory.getSupplier_Name()+"</td>");
									
						if(inventory.getSupplier_Name().toLowerCase().contains(modDetails.getSummary_SupplierName().toLowerCase())){
								
							mailPrinter.append("<td>"+modDetails.getSummary_SupplierName()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							mailPrinter.append("<td>"+modDetails.getSummary_SupplierName()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						
						testCaseModify ++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Booking Status [Program Summary]</td>");
						mailPrinter.append("<td>"+activitydetails.getBookingStatus()+"</td>");
									
						if(activitydetails.getBookingStatus().toLowerCase().contains(modDetails.getSummary_BookingStatus().toLowerCase())){
								
							mailPrinter.append("<td>"+modDetails.getSummary_BookingStatus()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							mailPrinter.append("<td>"+modDetails.getSummary_BookingStatus()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Activity Name [Activity Details]</td>");
						mailPrinter.append("<td>"+activitydetails.getActivityName_result()+"</td>");
						
						if(modDetails.getSummary_activityName_2().toLowerCase().contains(activitydetails.getActivityName_result().toLowerCase())){
							
							mailPrinter.append("<td>"+modDetails.getSummary_activityName_2()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_activityName_2()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Activity Rate [Activity Details]</td>");
						mailPrinter.append("<td>"+activitydetails.getActivityRateType()+"</td>");
						
						if(modDetails.getSummary_ratePlan().contains(activitydetails.getActivityRateType())){
							
							mailPrinter.append("<td>"+modDetails.getSummary_ratePlan()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_ratePlan()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Activity Date [Activity Details]</td>");
						mailPrinter.append("<td>"+search.getActivityDate()+"</td>");
						
						if(modDetails.getSummary_Date().equals(search.getActivityDate())){
							
							mailPrinter.append("<td>"+modDetails.getSummary_Date()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_Date()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Activity Session Type [Activity Details]</td>");
						mailPrinter.append("<td>"+activitydetails.getActivitySession()+"</td>");
						
						if(modDetails.getSummary_Session().contains(activitydetails.getActivitySession())){
							
							mailPrinter.append("<td>"+modDetails.getSummary_Session()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_Session()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Activity QTY [Activity Details]</td>");
						mailPrinter.append("<td>"+customerQty+"</td>");
						
						if(Integer.toString(customerQty).equals(modDetails.getSummary_QTY())){
							
							mailPrinter.append("<td>"+modDetails.getSummary_QTY()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_QTY()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Activity Rate [Activity Details]</td>");
						mailPrinter.append("<td>"+amount_onePerson+"</td>");
						
						if(Integer.toString(amount_onePerson).equals(modDetails.getSummary_RateUSD())){
							
							mailPrinter.append("<td>"+modDetails.getSummary_RateUSD()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_RateUSD()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Activity Rate Currency [Activity Details]</td>");
						mailPrinter.append("<td>"+search.getSellingCurrency()+"</td>");
						
						if(search.getSellingCurrency().toLowerCase().equals(modDetails.getSummary_RateCurrency().toLowerCase())){
							
							mailPrinter.append("<td>"+modDetails.getSummary_RateCurrency()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_RateCurrency()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Activity Total [Activity Details]</td>");
						mailPrinter.append("<td>"+PaymentPage_SubTotal+"</td>");
						
						if(Integer.toString(PaymentPage_SubTotal).equals(modDetails.getSummary_TotalValue())){
							
							mailPrinter.append("<td>"+modDetails.getSummary_TotalValue()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_TotalValue()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Activity Total Currency [Activity Details]</td>");
						mailPrinter.append("<td>"+search.getSellingCurrency()+"</td>");
						
						if(search.getSellingCurrency().toLowerCase().equals(modDetails.getSummary_TotalValueCurrency().toLowerCase())){
							
							mailPrinter.append("<td>"+modDetails.getSummary_TotalValueCurrency()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_TotalValueCurrency()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Program Total Value [Activity Details]</td>");
						mailPrinter.append("<td>"+PaymentPage_SubTotal+"</td>");
						
						if(Integer.toString(PaymentPage_SubTotal).equals(modDetails.getSummary_ProgramTotalValue())){
							
							mailPrinter.append("<td>"+modDetails.getSummary_ProgramTotalValue()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_ProgramTotalValue()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						totalCost = netRate;
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Program Total Cost [Activity Details]</td>");
						mailPrinter.append("<td>"+totalCost+"</td>");
						
						if(Integer.toString(totalCost).equals(modDetails.getSummary_ProgramTotalCost())){
							
							mailPrinter.append("<td>"+modDetails.getSummary_ProgramTotalCost()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_ProgramTotalCost()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						///////////
						
						
						testCaseModify ++;			
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Customer Notes [Modification Details]</td>");
						mailPrinter.append("<td>"+activitydetails.getCustomerNotes()+"</td>");
									
						if(activitydetails.getCustomerNotes().equals(modDetails.getSummary_CustomerNotes())){
								
							mailPrinter.append("<td>"+modDetails.getSummary_CustomerNotes()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_CustomerNotes()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						
						
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Customer Email [Modification Details]</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_Email()+"</td>");
						
						if(activitydetails.getPaymentPage_Email().toLowerCase().equals(modDetails.getSummary_CusEmail().toLowerCase())){
							
							mailPrinter.append("<td>"+modDetails.getSummary_CusEmail()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_CusEmail()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						//////////
						
						testCaseModify ++;			
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Customer Title</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_Title()+"</td>");
									
						if(activitydetails.getPaymentPage_Title().equals(modDetails.getSummary_Title())){
								
							mailPrinter.append("<td>"+modDetails.getSummary_Title()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_Title()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify ++;			
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Customer First Name</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_FName()+"</td>");
									
						if(activitydetails.getPaymentPage_FName().equals(modDetails.getSummary_FName())){
								
							mailPrinter.append("<td>"+modDetails.getSummary_FName()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_FName()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify ++;			
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Customer Last Name</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_LName()+"</td>");
									
						if(activitydetails.getPaymentPage_LName().equals(modDetails.getSummary_LName())){
								
							mailPrinter.append("<td>"+modDetails.getSummary_LName()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_LName()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify ++;			
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Customer TP No</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_TP()+"</td>");
									
						if(activitydetails.getPaymentPage_TP().equals(modDetails.getSummary_TP())){
								
							mailPrinter.append("<td>"+modDetails.getSummary_TP()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_TP()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify ++;			
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Customer Email</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_Email()+"</td>");
									
						if(activitydetails.getPaymentPage_Email().contains(modDetails.getSummary_Email())){
								
							mailPrinter.append("<td>"+modDetails.getSummary_Email()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_Email()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify ++;			
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Customer Address</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_address()+"</td>");
									
						if(activitydetails.getPaymentPage_address().equals(modDetails.getSummary_address())){
								
							mailPrinter.append("<td>"+modDetails.getSummary_address()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_address()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify ++;			
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Customer Address 2</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_address_2()+"</td>");
									
						if(activitydetails.getPaymentPage_address_2().equals(modDetails.getSummary_address_2())){
								
							mailPrinter.append("<td>"+modDetails.getSummary_address_2()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_address_2()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify ++;			
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Customer Country</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_country()+"</td>");
									
						if(activitydetails.getPaymentPage_country().equals(modDetails.getSummary_country())){
								
							mailPrinter.append("<td>"+modDetails.getSummary_country()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_country()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify ++;			
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Customer City</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_city()+"</td>");
									
						if(activitydetails.getPaymentPage_city().equals(modDetails.getSummary_city())){
								
							mailPrinter.append("<td>"+modDetails.getSummary_city()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getSummary_city()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						if (activitydetails.getConfirmationPage_country().equals("USA")) {
							
							testCaseModify ++;			
							mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Customer State</td>");
							mailPrinter.append("<td>"+activitydetails.getConfirmationPage_state()+"</td>");
										
							if(activitydetails.getPaymentPage_state().equals(modDetails.getSummary_state())){
									
								mailPrinter.append("<td>"+modDetails.getSummary_state()+"</td>");
								mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								mailPrinter.append("<td>"+modDetails.getSummary_state()+"</td>");
								mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
							}
						
						}
						
						if (activitydetails.getConfirmationPage_country().equals("Canada")) {
							
							testCaseModify ++;			
							mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Customer State</td>");
							mailPrinter.append("<td>"+activitydetails.getConfirmationPage_state()+"</td>");
										
							if(activitydetails.getPaymentPage_state().equals(modDetails.getSummary_state())){
									
								mailPrinter.append("<td>"+modDetails.getSummary_state()+"</td>");
								mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								mailPrinter.append("<td>"+modDetails.getSummary_state()+"</td>");
								mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
						}
						
						if (activitydetails.getConfirmationPage_country().equals("Australia")) {
							
							testCaseModify ++;			
							mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Customer State</td>");
							mailPrinter.append("<td>"+activitydetails.getConfirmationPage_state()+"</td>");
										
							if(activitydetails.getPaymentPage_state().equals(modDetails.getSummary_state())){
									
								mailPrinter.append("<td>"+modDetails.getSummary_state()+"</td>");
								mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								mailPrinter.append("<td>"+modDetails.getSummary_state()+"</td>");
								mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
							}
						
						}
						
						
						
						//Manager override
						
						mailPrinter.append("<tr><td class='fontiiii'>Modification - Manager override</td><tr>"); 
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Manager override - Customer Last Name</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_LName()+"</td>");
						
						if(activitydetails.getPaymentPage_LName().equals(modDetails.getMO_guestLastName())){
							
							mailPrinter.append("<td>"+modDetails.getMO_guestLastName()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getMO_guestLastName()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Manager override - Reservation No</td>");
						mailPrinter.append("<td>"+activitydetails.getReservationNo()+"</td>");
						
						if(activitydetails.getReservationNo().contains(modDetails.getMO_reservationNo_1())){
							
							mailPrinter.append("<td>"+modDetails.getMO_reservationNo_1()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getMO_reservationNo_1()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify ++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Manager override - Activity program Name [Program Summary]</td>");
						mailPrinter.append("<td>"+inventory.getActivityName()+"</td>");
									
						if(inventory.getActivityName().toLowerCase().contains(modDetails.getMO_programName_1().toLowerCase())){
								
							mailPrinter.append("<td>"+modDetails.getMO_programName_1()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							mailPrinter.append("<td>"+modDetails.getMO_programName_1()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Manager override - Activity Name [Activity Details]</td>");
						mailPrinter.append("<td>"+activitydetails.getActivityName_result()+"</td>");
						
						if(modDetails.getMO_activityName_1().toLowerCase().contains(activitydetails.getActivityName_result().toLowerCase())){
							
							mailPrinter.append("<td>"+modDetails.getMO_activityName_1()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getMO_activityName_1()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Manager override - Reservation Date [Activity Details]</td>");
						mailPrinter.append("<td>"+search.getActivityDate()+"</td>");
						
						if(search.getActivityDate().equals(modDetails.getMO_activityDate())){
							
							mailPrinter.append("<td>"+modDetails.getMO_activityDate()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getMO_activityDate()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Manager override - Activity Rate [Activity Details]</td>");
						mailPrinter.append("<td>"+activitydetails.getActivityRateType()+"</td>");
						
						if(modDetails.getMO_rate().contains(activitydetails.getActivityRateType())){
							
							mailPrinter.append("<td>"+modDetails.getMO_rate()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getMO_rate()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Manager override - Activity QTY [Activity Details]</td>");
						mailPrinter.append("<td>"+customerQty+"</td>");
						
						if(Integer.toString(customerQty).equals(modDetails.getMO_qty())){
							
							mailPrinter.append("<td>"+modDetails.getMO_qty()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getMO_qty()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Manager override - Activity Rate [Activity Details]</td>");
						mailPrinter.append("<td>"+amount_onePerson+"</td>");
						
						if(Integer.toString(amount_onePerson).equals(modDetails.getMO_activityRate())){
							
							mailPrinter.append("<td>"+modDetails.getMO_activityRate()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getMO_activityRate()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Manager override - Activity Rate Currency [Activity Details]</td>");
						mailPrinter.append("<td>"+search.getSellingCurrency()+"</td>");
						
						if(search.getSellingCurrency().toLowerCase().equals(modDetails.getMO_activityRateCurrency().toLowerCase())){
							
							mailPrinter.append("<td>"+modDetails.getMO_activityRateCurrency()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getMO_activityRateCurrency()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Manager override - Activity Total [Activity Details]</td>");
						mailPrinter.append("<td>"+PaymentPage_SubTotal+"</td>");
						
						if(Integer.toString(PaymentPage_SubTotal).equals(modDetails.getMO_Total())){
							
							mailPrinter.append("<td>"+modDetails.getMO_Total()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getMO_Total()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Manager override - Activity Total Currency [Activity Details]</td>");
						mailPrinter.append("<td>"+search.getSellingCurrency()+"</td>");
						
						if(search.getSellingCurrency().toLowerCase().equals(modDetails.getMO_TotalCurrency().toLowerCase())){
							
							mailPrinter.append("<td>"+modDetails.getMO_TotalCurrency()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getMO_TotalCurrency()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Manager override - Total Booking value [Activity Details]</td>");
						mailPrinter.append("<td>"+(PaymentPage_SubTotal - discountedVale)+"</td>");
						
						if(Integer.toString(PaymentPage_SubTotal - discountedVale).equals(modDetails.getMO_TotalBookingValue())){
							
							mailPrinter.append("<td>"+modDetails.getMO_TotalBookingValue()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getMO_TotalBookingValue()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Manager override - Total Cost [Activity Details]</td>");
						mailPrinter.append("<td>"+totalCost+"</td>");
						
						if(Integer.toString(totalCost).equals(modDetails.getMO_TotalCost())){
							
							mailPrinter.append("<td>"+modDetails.getMO_TotalCost()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getMO_TotalCost()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Manager override - Activity Name [Manager Overwrite]</td>");
						mailPrinter.append("<td>"+activitydetails.getActivityName_result()+"</td>");
						
						if(modDetails.getMO_ManagerActivityName_1().toLowerCase().contains(activitydetails.getActivityName_result().toLowerCase())){
							
							mailPrinter.append("<td>"+modDetails.getMO_ManagerActivityName_1()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getMO_ManagerActivityName_1()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Manager override - Reservation Date [Manager Overwrite]</td>");
						mailPrinter.append("<td>"+search.getActivityDate()+"</td>");
						
						if(search.getActivityDate().equals(modDetails.getMO_ManagerActivityDate())){
							
							mailPrinter.append("<td>"+modDetails.getMO_ManagerActivityDate()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getMO_ManagerActivityDate()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Manager override - Activity Rate [Manager Overwrite]</td>");
						mailPrinter.append("<td>"+activitydetails.getActivityRateType()+"</td>");
						
						if(modDetails.getMO_ManagerRate().contains(activitydetails.getActivityRateType())){
							
							mailPrinter.append("<td>"+modDetails.getMO_ManagerRate()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getMO_ManagerRate()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Manager override - Daily Rate Currency [Manager Overwrite]</td>");
						mailPrinter.append("<td>"+search.getSellingCurrency()+"</td>");
						
						if(search.getSellingCurrency().toLowerCase().equals(modDetails.getMO_DailyRateCurrency().toLowerCase())){
							
							mailPrinter.append("<td>"+modDetails.getMO_DailyRateCurrency()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getMO_DailyRateCurrency()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Manager override - New Total Booking Value [Manager Overwrite]</td>");
						mailPrinter.append("<td>"+PaymentPage_SubTotal+"</td>");
						
						if(Integer.toString(PaymentPage_SubTotal).equals(modDetails.getMO_NewTotalBookingValue())){
							
							mailPrinter.append("<td>"+modDetails.getMO_NewTotalBookingValue()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getMO_NewTotalBookingValue()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Manager override - New Total Booking Cost [Manager Overwrite]</td>");
						mailPrinter.append("<td>"+totalCost+"</td>");
						
						if(Integer.toString(totalCost).equals(modDetails.getMO_NewTotalBookingCost())){
							
							mailPrinter.append("<td>"+modDetails.getMO_NewTotalBookingCost()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getMO_NewTotalBookingCost()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						//Remove Activity
						mailPrinter.append("<tr><td class='fontiiii'>Modification - Remove Activity</td><tr>"); 
						
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Remove Activity - Customer Last Name</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_LName()+"</td>");
						
						if(activitydetails.getPaymentPage_LName().equals(modDetails.getRA_guestLastName())){
							
							mailPrinter.append("<td>"+modDetails.getRA_guestLastName()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getRA_guestLastName()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Remove Activity - Reservation No</td>");
						mailPrinter.append("<td>"+activitydetails.getReservationNo()+"</td>");
						
						if(activitydetails.getReservationNo().contains(modDetails.getRA_reservationNo_1())){
							
							mailPrinter.append("<td>"+modDetails.getRA_reservationNo_1()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getRA_reservationNo_1()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						///
						
						testCaseModify ++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Remove Activity - Activity program Name [Program Summary]</td>");
						mailPrinter.append("<td>"+inventory.getActivityName()+"</td>");
									
						if(inventory.getActivityName().toLowerCase().contains(modDetails.getRA_programName_1().toLowerCase())){
								
							mailPrinter.append("<td>"+modDetails.getRA_programName_1()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							mailPrinter.append("<td>"+modDetails.getRA_programName_1()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Remove Activity - Activity Name [Program Summary]</td>");
						mailPrinter.append("<td>"+activitydetails.getActivityName_result()+"</td>");
						
						if(modDetails.getRA_activityName_1().toLowerCase().contains(activitydetails.getActivityName_result().toLowerCase())){
							
							mailPrinter.append("<td>"+modDetails.getRA_activityName_1()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getRA_activityName_1()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Remove Activity - Reservation Date [Program Summary]</td>");
						mailPrinter.append("<td>"+search.getActivityDate()+"</td>");
						
						if(search.getActivityDate().equals(modDetails.getRA_activityDate())){
							
							mailPrinter.append("<td>"+modDetails.getRA_activityDate()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getRA_activityDate()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Remove Activity - Activity Session [Program Summary]</td>");
						mailPrinter.append("<td>"+activitydetails.getActivitySession()+"</td>");
						
						if(modDetails.getRA_session().contains(activitydetails.getActivitySession())){
							
							mailPrinter.append("<td>"+modDetails.getRA_session()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getRA_session()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Remove Activity - Activity Rate [Program Summary]</td>");
						mailPrinter.append("<td>"+activitydetails.getActivityRateType()+"</td>");
						
						if(modDetails.getRA_rate().contains(activitydetails.getActivityRateType())){
							
							mailPrinter.append("<td>"+modDetails.getRA_rate()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getRA_rate()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Remove Activity - Activity QTY [Program Summary]</td>");
						mailPrinter.append("<td>"+customerQty+"</td>");
						
						if(Integer.toString(customerQty).equals(modDetails.getRA_qty())){
							
							mailPrinter.append("<td>"+modDetails.getRA_qty()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getRA_qty()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Remove Activity - Activity Rate [Program Summary]</td>");
						mailPrinter.append("<td>"+amount_onePerson+"</td>");
						
						if(Integer.toString(amount_onePerson).equals(modDetails.getRA_activityRate())){
							
							mailPrinter.append("<td>"+modDetails.getRA_activityRate()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getRA_activityRate()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Remove Activity - Activity Rate Currency [Program Summary]</td>");
						mailPrinter.append("<td>"+search.getSellingCurrency()+"</td>");
						
						if(search.getSellingCurrency().toLowerCase().equals(modDetails.getRA_activityRateCurrency().toLowerCase())){
							
							mailPrinter.append("<td>"+modDetails.getRA_activityRateCurrency()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getRA_activityRateCurrency()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Remove Activity - Activity Total [Program Summary]</td>");
						mailPrinter.append("<td>"+PaymentPage_SubTotal+"</td>");
						
						if(Integer.toString(PaymentPage_SubTotal).equals(modDetails.getRA_Total())){
							
							mailPrinter.append("<td>"+modDetails.getRA_Total()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getRA_Total()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Remove Activity - Activity Total Currency [Program Summary]</td>");
						mailPrinter.append("<td>"+search.getSellingCurrency()+"</td>");
						
						if(search.getSellingCurrency().toLowerCase().equals(modDetails.getRA_TotalCurrency().toLowerCase())){
							
							mailPrinter.append("<td>"+modDetails.getRA_TotalCurrency()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getRA_TotalCurrency()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
		
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Remove Activity - Total Booking value [Program Summary]</td>");
						mailPrinter.append("<td>"+(PaymentPage_SubTotal - discountedVale)+"</td>");
						
						if(Integer.toString(PaymentPage_SubTotal - discountedVale).equals(modDetails.getRA_TotalBookingValue())){
							
							mailPrinter.append("<td>"+modDetails.getRA_TotalBookingValue()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getRA_TotalBookingValue()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Remove Activity - Total Cost [Program Summary]</td>");
						mailPrinter.append("<td>"+totalCost+"</td>");
						
						if(Integer.toString(totalCost).equals(modDetails.getRA_TotalCost())){
							
							mailPrinter.append("<td>"+modDetails.getRA_TotalCost()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getRA_TotalCost()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						//
						
						mailPrinter.append("<tr><td class='fontiiii'>Modification - Add Activity</td><tr>"); 
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Add Activity - Customer Last Name</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_LName()+"</td>");
						
						if(activitydetails.getPaymentPage_LName().equals(modDetails.getAA_guestLastName())){
							
							mailPrinter.append("<td>"+modDetails.getAA_guestLastName()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getAA_guestLastName()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Add Activity - Reservation No</td>");
						mailPrinter.append("<td>"+activitydetails.getReservationNo()+"</td>");
						
						if(activitydetails.getReservationNo().contains(modDetails.getAA_reservationNo_1())){
							
							mailPrinter.append("<td>"+modDetails.getAA_reservationNo_1()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getAA_reservationNo_1()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						///
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Add Activity - Current Date</td>");
						mailPrinter.append("<td>"+activitydetails.getCurrentDate()+"</td>");
						
						if(activitydetails.getCurrentDate().equals(modDetails.getAA_ReservationDate())){
							
							mailPrinter.append("<td>"+modDetails.getAA_ReservationDate()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getAA_ReservationDate()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						///
						
						testCaseModify ++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Add Activity - Activity program Name [Program Summary]</td>");
						mailPrinter.append("<td>"+inventory.getActivityName()+"</td>");
									
						if(inventory.getActivityName().toLowerCase().contains(modDetails.getAA_programName_1().toLowerCase())){
								
							mailPrinter.append("<td>"+modDetails.getAA_programName_1()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							mailPrinter.append("<td>"+modDetails.getAA_programName_1()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Add Activity - Activity Name [Program Summary]</td>");
						mailPrinter.append("<td>"+activitydetails.getActivityName_result()+"</td>");
						
						if(modDetails.getAA_activityName_1().toLowerCase().contains(activitydetails.getActivityName_result().toLowerCase())){
							
							mailPrinter.append("<td>"+modDetails.getAA_activityName_1()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getAA_activityName_1()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Add Activity - Reservation Date [Program Summary]</td>");
						mailPrinter.append("<td>"+search.getActivityDate()+"</td>");
						
						if(search.getActivityDate().equals(modDetails.getAA_activityDate())){
							
							mailPrinter.append("<td>"+modDetails.getAA_activityDate()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getAA_activityDate()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Add Activity - Activity Session [Program Summary]</td>");
						mailPrinter.append("<td>"+activitydetails.getActivitySession()+"</td>");
						
						if(modDetails.getAA_session().contains(activitydetails.getActivitySession())){
							
							mailPrinter.append("<td>"+modDetails.getAA_session()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getAA_session()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Add Activity - Activity Rate [Program Summary]</td>");
						mailPrinter.append("<td>"+activitydetails.getActivityRateType()+"</td>");
						
						if(modDetails.getAA_rate().contains(activitydetails.getActivityRateType())){
							
							mailPrinter.append("<td>"+modDetails.getAA_rate()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getAA_rate()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Add Activity - Activity QTY [Program Summary]</td>");
						mailPrinter.append("<td>"+customerQty+"</td>");
						
						if(Integer.toString(customerQty).equals(modDetails.getAA_qty())){
							
							mailPrinter.append("<td>"+modDetails.getAA_qty()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getAA_qty()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Add Activity - Activity Rate [Program Summary]</td>");
						mailPrinter.append("<td>"+amount_onePerson+"</td>");
						
						if(Integer.toString(amount_onePerson).equals(modDetails.getAA_activityRate())){
							
							mailPrinter.append("<td>"+modDetails.getAA_activityRate()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getAA_activityRate()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Add Activity - Activity Rate Currency [Program Summary]</td>");
						mailPrinter.append("<td>"+search.getSellingCurrency()+"</td>");
						
						if(search.getSellingCurrency().toLowerCase().equals(modDetails.getAA_activityRateCurrency().toLowerCase())){
							
							mailPrinter.append("<td>"+modDetails.getAA_activityRateCurrency()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getAA_activityRateCurrency()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Add Activity - Activity Total [Program Summary]</td>");
						mailPrinter.append("<td>"+PaymentPage_SubTotal+"</td>");
						
						if(Integer.toString(PaymentPage_SubTotal).equals(modDetails.getAA_Total())){
							
							mailPrinter.append("<td>"+modDetails.getAA_Total()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getAA_Total()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Add Activity - Activity Total Currency [Program Summary]</td>");
						mailPrinter.append("<td>"+search.getSellingCurrency()+"</td>");
						
						if(search.getSellingCurrency().toLowerCase().equals(modDetails.getAA_TotalCurrency().toLowerCase())){
							
							mailPrinter.append("<td>"+modDetails.getAA_TotalCurrency()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getAA_TotalCurrency()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
		
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Add Activity - Total Booking value [Program Summary]</td>");
						mailPrinter.append("<td>"+(PaymentPage_SubTotal - discountedVale)+"</td>");
						
						if(Integer.toString(PaymentPage_SubTotal - discountedVale).equals(modDetails.getAA_TotalBookingValue())){
							
							mailPrinter.append("<td>"+modDetails.getAA_TotalBookingValue()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getAA_TotalBookingValue()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Add Activity - Total Cost [Program Summary]</td>");
						mailPrinter.append("<td>"+totalCost+"</td>");
						
						if(Integer.toString(totalCost).equals(modDetails.getAA_TotalCost())){
							
							mailPrinter.append("<td>"+modDetails.getAA_TotalCost()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getAA_TotalCost()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						mailPrinter.append("<tr><td class='fontiiii'>Modification - Activity Date/Qty</td><tr>"); 
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Activity Date/Qty - Customer Last Name</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_LName()+"</td>");
						
						if(activitydetails.getPaymentPage_LName().equals(modDetails.getADQ_guestLastName())){
							
							mailPrinter.append("<td>"+modDetails.getADQ_guestLastName()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getADQ_guestLastName()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Activity Date/Qty - Reservation No</td>");
						mailPrinter.append("<td>"+activitydetails.getReservationNo()+"</td>");
						
						if(activitydetails.getReservationNo().contains(modDetails.getADQ_reservationNo_1())){
							
							mailPrinter.append("<td>"+modDetails.getADQ_reservationNo_1()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getADQ_reservationNo_1()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						///
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Activity Date/Qty - Current Date</td>");
						mailPrinter.append("<td>"+activitydetails.getCurrentDate()+"</td>");
						
						if(activitydetails.getCurrentDate().equals(modDetails.getADQ_ReservationDate())){
							
							mailPrinter.append("<td>"+modDetails.getADQ_ReservationDate()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getADQ_ReservationDate()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						///
						
						testCaseModify ++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Activity Date/Qty - Activity program Name [Program Summary]</td>");
						mailPrinter.append("<td>"+inventory.getActivityName()+"</td>");
									
						if(inventory.getActivityName().toLowerCase().contains(modDetails.getADQ_programName_1().toLowerCase())){
								
							mailPrinter.append("<td>"+modDetails.getADQ_programName_1()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							mailPrinter.append("<td>"+modDetails.getADQ_programName_1()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						
						testCaseModify ++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Activity Date/Qty - Supplier Name [Program Summary]</td>");
						mailPrinter.append("<td>"+inventory.getSupplier_Name()+"</td>");
									
						if(inventory.getSupplier_Name().toLowerCase().contains(modDetails.getADQ_SupplierName().toLowerCase())){
								
							mailPrinter.append("<td>"+modDetails.getADQ_SupplierName()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							mailPrinter.append("<td>"+modDetails.getADQ_SupplierName()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Activity Date/Qty - Activity Name [Program Summary]</td>");
						mailPrinter.append("<td>"+activitydetails.getActivityName_result()+"</td>");
						
						if(modDetails.getADQ_activityName_1().toLowerCase().contains(activitydetails.getActivityName_result().toLowerCase())){
							
							mailPrinter.append("<td>"+modDetails.getADQ_activityName_1()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getADQ_activityName_1()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Activity Date/Qty - Reservation Date [Program Summary]</td>");
						mailPrinter.append("<td>"+search.getActivityDate()+"</td>");
						
						if(search.getActivityDate().equals(modDetails.getADQ_activityDate())){
							
							mailPrinter.append("<td>"+modDetails.getADQ_activityDate()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getADQ_activityDate()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Activity Date/Qty - Activity Session [Program Summary]</td>");
						mailPrinter.append("<td>"+activitydetails.getActivitySession()+"</td>");
						
						if(modDetails.getADQ_session().contains(activitydetails.getActivitySession())){
							
							mailPrinter.append("<td>"+modDetails.getADQ_session()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getADQ_session()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Activity Date/Qty - Activity Rate [Program Summary]</td>");
						mailPrinter.append("<td>"+activitydetails.getActivityRateType()+"</td>");
						
						if(modDetails.getADQ_rate().contains(activitydetails.getActivityRateType())){
							
							mailPrinter.append("<td>"+modDetails.getADQ_rate()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getADQ_rate()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Activity Date/Qty - Activity QTY [Program Summary]</td>");
						mailPrinter.append("<td>"+customerQty+"</td>");
						
						if(Integer.toString(customerQty).equals(modDetails.getADQ_qty())){
							
							mailPrinter.append("<td>"+modDetails.getADQ_qty()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getADQ_qty()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Activity Date/Qty - Activity Rate [Program Summary]</td>");
						mailPrinter.append("<td>"+amount_onePerson+"</td>");
						
						if(Integer.toString(amount_onePerson).equals(modDetails.getADQ_activityRate())){
							
							mailPrinter.append("<td>"+modDetails.getADQ_activityRate()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getADQ_activityRate()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Activity Date/Qty - Activity Rate Currency [Program Summary]</td>");
						mailPrinter.append("<td>"+search.getSellingCurrency()+"</td>");
						
						if(search.getSellingCurrency().toLowerCase().equals(modDetails.getADQ_activityRateCurrency().toLowerCase())){
							
							mailPrinter.append("<td>"+modDetails.getADQ_activityRateCurrency()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getADQ_activityRateCurrency()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Activity Date/Qty - Activity Total [Program Summary]</td>");
						mailPrinter.append("<td>"+PaymentPage_SubTotal+"</td>");
						
						if(Integer.toString(PaymentPage_SubTotal).equals(modDetails.getADQ_Total())){
							
							mailPrinter.append("<td>"+modDetails.getADQ_Total()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getADQ_Total()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Modification - Activity Date/Qty - Total Booking value [Program Summary] - Activity Total Currency [Program Summary]</td>");
						mailPrinter.append("<td>"+search.getSellingCurrency()+"</td>");
						
						if(search.getSellingCurrency().toLowerCase().equals(modDetails.getADQ_TotalCurrency().toLowerCase())){
							
							mailPrinter.append("<td>"+modDetails.getADQ_TotalCurrency()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getADQ_TotalCurrency()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
		
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Activity Date/Qty - Total Booking value [Program Summary]</td>");
						mailPrinter.append("<td>"+(PaymentPage_SubTotal - discountedVale)+"</td>");
						
						if(Integer.toString(PaymentPage_SubTotal - discountedVale).equals(modDetails.getADQ_TotalBookingValue())){
							
							mailPrinter.append("<td>"+modDetails.getADQ_TotalBookingValue()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getADQ_TotalBookingValue()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Activity Date/Qty - Total Cost [Program Summary]</td>");
						mailPrinter.append("<td>"+totalCost+"</td>");
						
						if(Integer.toString(totalCost).equals(modDetails.getADQ_TotalCost())){
							
							mailPrinter.append("<td>"+modDetails.getADQ_TotalCost()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getADQ_TotalCost()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						
						mailPrinter.append("<tr><td class='fontiiii'>Modification - Guest Details</td><tr>"); 
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Guest Details - Customer Last Name</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_LName()+"</td>");
						
						if(activitydetails.getPaymentPage_LName().equals(modDetails.getGD_guestLastName())){
							
							mailPrinter.append("<td>"+modDetails.getGD_guestLastName()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getGD_guestLastName()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify++;
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Guest Details - Reservation No</td>");
						mailPrinter.append("<td>"+activitydetails.getReservationNo()+"</td>");
						
						if(activitydetails.getReservationNo().contains(modDetails.getGD_reservationNo_1())){
							
							mailPrinter.append("<td>"+modDetails.getGD_reservationNo_1()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getGD_reservationNo_1()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						
						testCaseModify ++;			
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Guest Details - Customer Title</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_Title()+"</td>");
									
						if(modDetails.getGD_Title().contains(activitydetails.getPaymentPage_Title())){
								
							mailPrinter.append("<td>"+modDetails.getGD_Title()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getGD_Title()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify ++;			
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Guest Details - Customer First Name</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_FName()+"</td>");
									
						if(activitydetails.getPaymentPage_FName().equals(modDetails.getGD_FName())){
								
							mailPrinter.append("<td>"+modDetails.getGD_FName()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getGD_FName()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify ++;			
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Guest Details - Customer Last Name</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_LName()+"</td>");
									
						if(activitydetails.getPaymentPage_LName().equals(modDetails.getGD_LName())){
								
							mailPrinter.append("<td>"+modDetails.getGD_LName()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getGD_LName()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify ++;			
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Guest Details - Customer TP No</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_TP()+"</td>");
									
						if(activitydetails.getPaymentPage_TP().equals(modDetails.getGD_TP())){
								
							mailPrinter.append("<td>"+modDetails.getGD_TP()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getGD_TP()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify ++;			
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Guest Details - Customer Email</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_Email()+"</td>");
									
						if(activitydetails.getPaymentPage_Email().contains(modDetails.getGD_Email())){
								
							mailPrinter.append("<td>"+modDetails.getGD_Email()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getGD_Email()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify ++;			
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Guest Details - Customer Address</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_address()+"</td>");
									
						if(activitydetails.getPaymentPage_address().equals(modDetails.getGD_address())){
								
							mailPrinter.append("<td>"+modDetails.getGD_address()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getGD_address()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify ++;			
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Guest Details - Customer Address 2</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_address_2()+"</td>");
									
						if(activitydetails.getPaymentPage_address_2().equals(modDetails.getGD_address_2())){
								
							mailPrinter.append("<td>"+modDetails.getGD_address_2()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getGD_address_2()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify ++;			
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Guest Details - Customer Country</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_country()+"</td>");
									
						if(activitydetails.getPaymentPage_country().equals(modDetails.getGD_country())){
								
							mailPrinter.append("<td>"+modDetails.getGD_country()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getGD_country()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify ++;			
						mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Guest Details - Customer City</td>");
						mailPrinter.append("<td>"+activitydetails.getPaymentPage_city()+"</td>");
									
						if(activitydetails.getPaymentPage_city().equals(modDetails.getGD_city())){
								
							mailPrinter.append("<td>"+modDetails.getGD_city()+"</td>");
							mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							mailPrinter.append("<td>"+modDetails.getGD_city()+"</td>");
							mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						if (activitydetails.getConfirmationPage_country().equals("USA")) {
							
							testCaseModify ++;			
							mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Guest Details - Customer State</td>");
							mailPrinter.append("<td>"+activitydetails.getConfirmationPage_state()+"</td>");
										
							if(activitydetails.getPaymentPage_state().equals(modDetails.getGD_state())){
									
								mailPrinter.append("<td>"+modDetails.getGD_state()+"</td>");
								mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								mailPrinter.append("<td>"+modDetails.getGD_state()+"</td>");
								mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
							}
						
						}
						
						if (activitydetails.getConfirmationPage_country().equals("Canada")) {
							
							testCaseModify ++;			
							mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Guest Details - Customer State</td>");
							mailPrinter.append("<td>"+activitydetails.getConfirmationPage_state()+"</td>");
										
							if(activitydetails.getPaymentPage_state().equals(modDetails.getGD_state())){
									
								mailPrinter.append("<td>"+modDetails.getGD_state()+"</td>");
								mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								mailPrinter.append("<td>"+modDetails.getGD_state()+"</td>");
								mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
						}
						
						if (activitydetails.getConfirmationPage_country().equals("Australia")) {
							
							testCaseModify ++;			
							mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Guest Details - Customer State</td>");
							mailPrinter.append("<td>"+activitydetails.getConfirmationPage_state()+"</td>");
										
							if(activitydetails.getPaymentPage_state().equals(modDetails.getGD_state())){
									
								mailPrinter.append("<td>"+modDetails.getGD_state()+"</td>");
								mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								mailPrinter.append("<td>"+modDetails.getGD_state()+"</td>");
								mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
							}
						
						}
							
					}
					
					
					
				} else {
					
					mailPrinter.append("<tr><td>"+testCaseModify+"</td> <td>Modification Summary - Modification Status</td>");
					mailPrinter.append("<td>Modification report availability</td>");
					mailPrinter.append("<td>Not Available</td>");
					mailPrinter.append("<td class='Failed'>FAIL</td><tr>");		
				}
				
			
				mailPrinter.append("</table>");
				
				
				
				//Inventory Report
				
				mailPrinter.append("<br><br>");
				mailPrinter.append("<p class='fontStyles'>Inventory Report Summary</p>");
				
				testCaseInventory = 1;
				mailPrinter.append("<br><table><tr><th>Test Case No</th><th>Test Description</th><th>Expect Values - [Excel Value]</th><th>Actual Values</th><th>Test Status</th></tr>");
				
				mailPrinter.append("<tr><td>"+testCaseInventory+"</td> <td>Inventory Report - Reservation Date</td>");
				mailPrinter.append("<td>"+search.getActivityDate()+"</td>");
				
				if(search.getActivityDate().equals(inventoryDetails.getActivityDate())){
					
					mailPrinter.append("<td>"+inventoryDetails.getActivityDate()+"</td>");
					mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					mailPrinter.append("<td>"+inventoryDetails.getActivityDate()+"</td>");
					mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				int totalInventory = Integer.parseInt(inventoryDetails.getTotalInventory());
				int inventoryCount = Integer.parseInt(inventory.getInventoryCount()); 
				
				int bookedBefore = Integer.parseInt(inventoryDetails.getBookedInventory().get(0));
				int bookedAfter = Integer.parseInt(inventoryDetails.getBookedInventory().get(1));
				
				int availableBefore = Integer.parseInt(inventoryDetails.getAvailableInventory().get(0));
				int availableAfter = Integer.parseInt(inventoryDetails.getAvailableInventory().get(1));
				
				int newBookedAfter = bookedBefore + inventoryCount;
				int newavailableAfter = availableBefore - inventoryCount;
				
				
				testCaseInventory++;
				mailPrinter.append("<tr><td>"+testCaseInventory+"</td> <td>Inventory Report - Booked Inventory</td>");
				mailPrinter.append("<td>"+newBookedAfter+"</td>");
				
				if(newBookedAfter == bookedAfter){
					
					mailPrinter.append("<td>"+bookedAfter+"</td>");
					mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					mailPrinter.append("<td>"+bookedAfter+"</td>");
					mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseInventory++;
				mailPrinter.append("<tr><td>"+testCaseInventory+"</td> <td>Inventory Report - Available Inventory</td>");
				mailPrinter.append("<td>"+newavailableAfter+"</td>");
				
				if(newavailableAfter == availableAfter){
					
					mailPrinter.append("<td>"+availableAfter+"</td>");
					mailPrinter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					mailPrinter.append("<td>"+availableAfter+"</td>");
					mailPrinter.append("<td class='Failed'>FAIL</td><tr>");
				}
			
	
				mailPrinter.append("</table>");
				
				
				
				/////
				
				mailPrinter.append("<br><br>");
				mailPrinter.append("<p class='fontStyles'>Response Times</p>");
				
				
				long timeDiff1 = activitydetails.getTimeResultsAvailable() - activitydetails.getTimeSearchButtClickTime();
				long timeDiff2 = activitydetails.getTimeAddedtoCart() - activitydetails.getTimeLoadpayment();
				long timeDiff3 = activitydetails.getTimeConfirmBooking() - activitydetails.getTimeAailableClick();
				long timeDiff4 = activitydetails.getTimeFirstResults() - activitydetails.getTimeFirstSearch();
				
				
				
				mailPrinter.append("<br><br><table><tr> <th>Description</th> <th>Time</th> </tr>");
				mailPrinter.append("<tr> <td>Initial Results Availability time - </td> <td>"+timeDiff4+"</td> <tr>");
				mailPrinter.append("<tr> <td>'Search Again' Results Availability Time - </td> <td>"+timeDiff1+"</td> <tr>");
				mailPrinter.append("<tr> <td>Add to cart time - </td> <td>"+timeDiff2+"</td> <tr>");
				mailPrinter.append("<tr> <td>Reservation No available time - </td> <td>"+timeDiff3+"</td> <tr>");
				mailPrinter.append("</table>");
				
				////
				
				mailPrinter.append("</body></html>");
				
			}else{
								
				/////  Program Page
				mailPrinter.append("<tr><td class='fontiiii'>Activity Programs Availability - Activity Types</td><tr>"); 
				mailPrinter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Types Availability</td>");
				mailPrinter.append("<td>Activity Types Should be available</td>");
				mailPrinter.append("<td>Activity Types are not available</td>");
				mailPrinter.append("<td class='Failed'>FAIL</td><tr>");	
				
				mailPrinter.append("<tr><td class='fontiiii'>Activity Add to the cart</td><tr>"); 
				mailPrinter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Cart</td>");
				mailPrinter.append("<td>Activity should be in the cart</td>");
				mailPrinter.append("<td>Activity cart is not available</td>");
				mailPrinter.append("<td class='Failed'>FAIL</td><tr>");	
				
			}
			
		}else{
			
			
			/////  Results Page
			mailPrinter.append("<tr><td class='fontiiii'>Results Page</td><tr>"); 
			mailPrinter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Results Availability</td>");
			mailPrinter.append("<td>Results Should be available</td>");
			mailPrinter.append("<td>Results are not available</td>");
			mailPrinter.append("<td class='Failed'>FAIL</td><tr>");			
			
	
		}
		
		BufferedWriter bwr = new BufferedWriter(new FileWriter(new File("Report/ReservationReport_"+search.getScenarioCount()+".html")));
		bwr.write(mailPrinter.toString());
		bwr.flush();
		bwr.close();
			
		
		}
	
	
	public void getQuotationDetails(){
					
			if (search.getQuotaionReq().equalsIgnoreCase("Yes")) {
				
				PrintWriter.append("<tr><td class='fontiiii'>Quotation Confirmation Page</td><tr>"); 
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Quotation Status -  Quotation Confirmation Page </td>");
				PrintWriter.append("<td>QUOTE</td>");
				
				if(activitydetails.getQuote_Status().toLowerCase().contains("QUOTE".toLowerCase())){
					
					PrintWriter.append("<td>"+activitydetails.getQuote_Status()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+activitydetails.getQuote_Status()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Currency - Quotation Confirmation Page</td>");
				PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
							
				if(search.getSellingCurrency().toLowerCase().equals(activitydetails.getQuote_Currency().toLowerCase())){
						
					PrintWriter.append("<td>"+activitydetails.getQuote_Currency()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+activitydetails.getQuote_Currency()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity qty - Quotation Confirmation Page</td>");
				PrintWriter.append("<td>"+payent_qty+"</td>");
							
				if(Integer.toString(payent_qty).equals(activitydetails.getQuote_QTY())){
						
					PrintWriter.append("<td>"+activitydetails.getQuote_QTY()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+activitydetails.getQuote_QTY()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Name - Quotation Confirmation Page</td>");
				PrintWriter.append("<td>"+search.getActivityName()+"</td>");
							
				if(search.getActivityName().toLowerCase().contains(activitydetails.getQuote_ActivityName().toLowerCase())){
						
					PrintWriter.append("<td>"+activitydetails.getQuote_ActivityName()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+activitydetails.getQuote_ActivityName()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				
			
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Sub Total - Quotation Confirmation Page</td>");
				PrintWriter.append("<td>"+PaymentPage_SubTotal+"</td>");
							
				if(Integer.toString(PaymentPage_SubTotal).equals(activitydetails.getQuote_subTotal())){
						
					PrintWriter.append("<td>"+activitydetails.getQuote_subTotal()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+activitydetails.getQuote_subTotal()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Tax - Quotation Confirmation Page</td>");
				PrintWriter.append("<td>"+PaymentPage_Taxes+"</td>");
							
				if(Integer.toString(PaymentPage_Taxes).equals(activitydetails.getQuote_taxandOther())){
						
					PrintWriter.append("<td>"+activitydetails.getQuote_taxandOther()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+activitydetails.getQuote_taxandOther()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Activity Booking Value - Quotation Confirmation Page</td>");
				PrintWriter.append("<td>"+(PaymentPage_TotalPayable+discountedVale)+"</td>");
							
				if(Integer.toString(PaymentPage_TotalPayable+discountedVale).equals(activitydetails.getQuote_totalValue())){
						
					PrintWriter.append("<td>"+activitydetails.getQuote_totalValue()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+activitydetails.getQuote_totalValue()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Gross Package Booking Value - Quotation Confirmation Page</td>");
				PrintWriter.append("<td>"+PaymentPage_SubTotal+"</td>");
							
				if(Integer.toString(PaymentPage_SubTotal).equals(activitydetails.getQuote_TotalGrossPackageValue())){
						
					PrintWriter.append("<td>"+activitydetails.getQuote_TotalGrossPackageValue()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+activitydetails.getQuote_TotalGrossPackageValue()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Taxes And Other Charges - Quotation Confirmation Page</td>");
				PrintWriter.append("<td>"+PaymentPage_Taxes+"</td>");
							
				if(Integer.toString(PaymentPage_Taxes).equals(activitydetails.getQuote_TotalTax())){
						
					PrintWriter.append("<td>"+activitydetails.getQuote_TotalTax()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+activitydetails.getQuote_TotalTax()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Package Booking Value - Quotation Confirmation Page</td>");
				PrintWriter.append("<td>"+(PaymentPage_TotalPayable+discountedVale)+"</td>");
							
				if(Integer.toString(PaymentPage_TotalPayable+discountedVale).equals(activitydetails.getQuote_TotalPackageValue())){
						
					PrintWriter.append("<td>"+activitydetails.getQuote_TotalPackageValue()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+activitydetails.getQuote_TotalPackageValue()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details - First Name [Quotation Confirmation Page]</td>");
				PrintWriter.append("<td>"+activitydetails.getPaymentPage_FName()+"</td>");
							
				if(activitydetails.getPaymentPage_FName().equals(activitydetails.getQuote_FName())){
						
					PrintWriter.append("<td>"+activitydetails.getQuote_FName()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+activitydetails.getQuote_FName()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details - Last Name [Quotation Confirmation Page]</td>");
				PrintWriter.append("<td>"+activitydetails.getPaymentPage_LName()+"</td>");
							
				if(activitydetails.getPaymentPage_LName().equals(activitydetails.getQuote_LName())){
						
					PrintWriter.append("<td>"+activitydetails.getQuote_LName()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+activitydetails.getQuote_LName()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details - TP No [Quotation Confirmation Page]</td>");
				PrintWriter.append("<td>"+activitydetails.getPaymentPage_TP()+"</td>");
							
				if(activitydetails.getPaymentPage_TP().equals(activitydetails.getQuote_TP())){
						
					PrintWriter.append("<td>"+activitydetails.getQuote_TP()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+activitydetails.getQuote_TP()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details - Email [Quotation Confirmation Page]</td>");
				PrintWriter.append("<td>"+activitydetails.getPaymentPage_Email()+"</td>");
							
				if(activitydetails.getPaymentPage_Email().equals(activitydetails.getQuote_Email())){
						
					PrintWriter.append("<td>"+activitydetails.getQuote_Email()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+activitydetails.getQuote_Email()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details - Address [Quotation Confirmation Page]</td>");
				PrintWriter.append("<td>"+activitydetails.getPaymentPage_address()+"</td>");
							
				if(activitydetails.getPaymentPage_address().equals(activitydetails.getQuote_address_1())){
						
					PrintWriter.append("<td>"+activitydetails.getQuote_address_1()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+activitydetails.getQuote_address_1()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details - Address 2 [Quotation Confirmation Page]</td>");
				PrintWriter.append("<td>"+activitydetails.getPaymentPage_address_2()+"</td>");
							
				if(activitydetails.getPaymentPage_address_2().equals(activitydetails.getQuote_address_2())){
						
					PrintWriter.append("<td>"+activitydetails.getQuote_address_2()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+activitydetails.getQuote_address_2()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details - Country [Quotation Confirmation Page]</td>");
				PrintWriter.append("<td>"+activitydetails.getPaymentPage_country()+"</td>");
							
				if(activitydetails.getPaymentPage_country().equals(activitydetails.getQuote_country())){
						
					PrintWriter.append("<td>"+activitydetails.getQuote_country()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+activitydetails.getQuote_country()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details - City [Quotation Confirmation Page]</td>");
				PrintWriter.append("<td>"+activitydetails.getPaymentPage_city()+"</td>");
							
				if(activitydetails.getPaymentPage_city().equals(activitydetails.getQuote_city())){
						
					PrintWriter.append("<td>"+activitydetails.getQuote_city()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+activitydetails.getQuote_city()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				String contryPayPage = activitydetails.getPaymentPage_country();
				
				if (contryPayPage.equalsIgnoreCase("USA") || contryPayPage.equalsIgnoreCase("Canada") || contryPayPage.equalsIgnoreCase("Australia")) {
					
					testCaseCount ++;			
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details - State [Quotation Confirmation Page]</td>");
					PrintWriter.append("<td>"+activitydetails.getPaymentPage_state()+"</td>");
								
					if(activitydetails.getPaymentPage_state().equals(activitydetails.getQuote_city())){
							
						PrintWriter.append("<td>"+activitydetails.getQuote_city()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getQuote_city()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCount ++;			
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details - Postal Code [Quotation Confirmation Page]</td>");
					PrintWriter.append("<td>"+activitydetails.getPaymentPage_PostelCode()+"</td>");
								
					if(activitydetails.getPaymentPage_PostelCode().equals(activitydetails.getQuote_city())){
							
						PrintWriter.append("<td>"+activitydetails.getQuote_city()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getQuote_city()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					
				}
				
				
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Name - View Full Details - [Quotation]</td>");
				PrintWriter.append("<td>"+inventory.getActivityName()+"</td>");
							
				if(inventory.getActivityName().toLowerCase().contains(activitydetails.getQuote_ViewFullDetails_ActivityName().toLowerCase())){
						
					PrintWriter.append("<td>"+activitydetails.getQuote_ViewFullDetails_ActivityName()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+activitydetails.getQuote_ViewFullDetails_ActivityName()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				/////
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Currency_1 - View Full Details - [Quotation]</td>");
				PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
							
				if(search.getSellingCurrency().toLowerCase().equals(activitydetails.getQuote_ViewFullDetails_Currency1().toLowerCase())){
						
					PrintWriter.append("<td>"+activitydetails.getQuote_ViewFullDetails_Currency1()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+activitydetails.getQuote_ViewFullDetails_Currency1()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Currency_2 - View Full Details - [Quotation]</td>");
				PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
							
				if(search.getSellingCurrency().toLowerCase().equals(activitydetails.getQuote_ViewFullDetails_Currency2().toLowerCase())){
						
					PrintWriter.append("<td>"+activitydetails.getQuote_ViewFullDetails_Currency2()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+activitydetails.getQuote_ViewFullDetails_Currency2()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				////
				
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Sub Total - View Full Details - [Quotation]</td>");
				PrintWriter.append("<td>"+PaymentPage_SubTotal+"</td>");
							
				if(Integer.toString(PaymentPage_SubTotal).equals(activitydetails.getQuote_ViewFullDetails_SubTotal())){
						
					PrintWriter.append("<td>"+activitydetails.getQuote_ViewFullDetails_SubTotal()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+activitydetails.getQuote_ViewFullDetails_SubTotal()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Tax - View Full Details - [Quotation]</td>");
				PrintWriter.append("<td>"+PaymentPage_Taxes+"</td>");
							
				if(Integer.toString(PaymentPage_Taxes).equals(activitydetails.getQuote_ViewFullDetails_Taxes())){
						
					PrintWriter.append("<td>"+activitydetails.getQuote_ViewFullDetails_Taxes()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+activitydetails.getQuote_ViewFullDetails_Taxes()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Activity Booking Value - View Full Details - [Quotation]</td>");
				PrintWriter.append("<td>"+(PaymentPage_TotalPayable+discountedVale)+"</td>");
							
				if(Integer.toString(PaymentPage_TotalPayable+discountedVale).equals(activitydetails.getQuote_ViewFullDetails_TotalPayable())){
						
					PrintWriter.append("<td>"+activitydetails.getQuote_ViewFullDetails_TotalPayable()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+activitydetails.getQuote_ViewFullDetails_TotalPayable()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				///
				
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Gross Package Booking Value - View Full Details - [Quotation]</td>");
				PrintWriter.append("<td>"+PaymentPage_TotalGrossBookingValue+"</td>");
							
				if(Integer.toString(PaymentPage_TotalGrossBookingValue).equals(activitydetails.getQuote_ViewFullDetails_TotalGrossBookingValue())){
						
					PrintWriter.append("<td>"+activitydetails.getQuote_ViewFullDetails_TotalGrossBookingValue()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+activitydetails.getQuote_ViewFullDetails_TotalGrossBookingValue()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Taxes And Other Charges - View Full Details - [Quotation]</td>");
				PrintWriter.append("<td>"+PaymentPage_Taxes+"</td>");
							
				if(Integer.toString(PaymentPage_Taxes).equals(activitydetails.getQuote_ViewFullDetails_TotalTax())){
						
					PrintWriter.append("<td>"+activitydetails.getQuote_ViewFullDetails_TotalTax()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+activitydetails.getQuote_ViewFullDetails_TotalTax()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Booking Value - View Full Details - [Quotation]</td>");
				PrintWriter.append("<td>"+(PaymentPage_TotalPayable+discountedVale)+"</td>");
							
				if(Integer.toString(PaymentPage_TotalPayable+discountedVale).equals(activitydetails.getQuote_ViewFullDetails_TotalPackageValue())){
						
					PrintWriter.append("<td>"+activitydetails.getQuote_ViewFullDetails_TotalPackageValue()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+activitydetails.getQuote_ViewFullDetails_TotalPackageValue()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Amount being Processed Now - View Full Details - [Quotation]</td>");
				PrintWriter.append("<td>"+(PaymentPage_TotalPayable+discountedVale)+"</td>");
							
				if(Integer.toString(PaymentPage_TotalPayable+discountedVale).equals(activitydetails.getQuote_ViewFullDetails_AmountProcessed())){
						
					PrintWriter.append("<td>"+activitydetails.getQuote_ViewFullDetails_AmountProcessed()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+activitydetails.getQuote_ViewFullDetails_AmountProcessed()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Amount due at Check-In - View Full Details - [Quotation]</td>");
				PrintWriter.append("<td>"+PaymentPage_AmountCheckIn+"</td>");
							
				if(Integer.toString(PaymentPage_AmountCheckIn).equals(activitydetails.getQuote_ViewFullDetails_AmountCheckIn())){
						
					PrintWriter.append("<td>"+activitydetails.getQuote_ViewFullDetails_AmountCheckIn()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+activitydetails.getQuote_ViewFullDetails_AmountCheckIn()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				
				
	
				
			}	
								
		}

	
	public void getQuotationMail(WebDriver Driver){
		
		if (search.getQuotaionReq().equalsIgnoreCase("yes")) {
			
			PrintWriter.append("<br><br>");
			PrintWriter.append("<p class='fontStyles'>Email - Quotation Email</p>");
			
			testQuote = 1;
			PrintWriter.append("<br><table ><tr><th>Test Case No</th><th>Test Description</th><th>Expect Values - [Excel Value]</th><th>Actual Values</th><th>Test Status</th></tr>");
		
			if (quotationDetails.isQuotationMailSent() == true) {
						
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Quotation Number</td>");
				PrintWriter.append("<td>"+activitydetails.getQuote_QuotationNo()+"</td>");
							
				if(quotationDetails.getRefNo().contains(activitydetails.getQuote_QuotationNo())){
						
					PrintWriter.append("<td>"+quotationDetails.getRefNo()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getRefNo()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
								
				String referenceName = activitydetails.getPaymentPage_Title() + activitydetails.getPaymentPage_FName() + activitydetails.getPaymentPage_LName();
				
				testQuote ++;
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Quotation Number</td>");
				PrintWriter.append("<td>"+referenceName+"</td>");
							
				if(quotationDetails.getReferenceName().contains(referenceName)){
						
					PrintWriter.append("<td>"+quotationDetails.getReferenceName()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getReferenceName()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testQuote++;
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Acitivity Name</td>");
				PrintWriter.append("<td>"+inventory.getActivityName()+"</td>");
				
				if(inventory.getActivityName().toLowerCase().contains(quotationDetails.getActivityName().toLowerCase())){
						
					PrintWriter.append("<td>"+quotationDetails.getActivityName()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+quotationDetails.getActivityName()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
								
				String servicyDate = search.getActivityDate();
				
				testQuote ++;
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Activity Usable on</td>");
				PrintWriter.append("<td>"+servicyDate+"</td>");
							
				if(servicyDate.replace("-", "").equals(quotationDetails.getActivityDate().replace(" ", ""))){
						
					PrintWriter.append("<td>"+quotationDetails.getActivityDate()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+quotationDetails.getActivityDate()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				
				testQuote ++;
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Activity Session</td>");
				PrintWriter.append("<td>"+activitydetails.getActivitySession()+"</td>");
							
				if(quotationDetails.getPeriodType().toLowerCase().contains(activitydetails.getActivitySession().toLowerCase())){
						
					PrintWriter.append("<td>"+quotationDetails.getPeriodType()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+quotationDetails.getPeriodType()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				testQuote ++;
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Activity Rate type</td>");
				PrintWriter.append("<td>"+activitydetails.getActivityRateType()+"</td>");
							
				if(quotationDetails.getRateType().toLowerCase().contains(activitydetails.getActivityRateType().toLowerCase())){
						
					PrintWriter.append("<td>"+quotationDetails.getRateType()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+quotationDetails.getRateType()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				testQuote ++;
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Booking Status</td>");
				PrintWriter.append("<td>Quote</td>");
							
				if(quotationDetails.getBookingStatus().equalsIgnoreCase("Quote")){
						
					PrintWriter.append("<td>"+quotationDetails.getBookingStatus()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+quotationDetails.getBookingStatus()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				
				testQuote ++;
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Activity Rate</td>");
				PrintWriter.append("<td>"+activitydetails.getActivityRate()+"</td>");
							
				if(quotationDetails.getPersonRate().equals(activitydetails.getActivityRate())){
						
					PrintWriter.append("<td>"+quotationDetails.getPersonRate()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+quotationDetails.getPersonRate()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				int customerQtyQuotation = Integer.parseInt(search.getAdults()) + Integer.parseInt(search.getChildren());
				
				testQuote ++;
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Activity QTY</td>");
				PrintWriter.append("<td>"+customerQtyQuotation+"</td>");
							
				if(Integer.toString(customerQtyQuotation).equals(quotationDetails.getQty())){
						
					PrintWriter.append("<td>"+quotationDetails.getQty()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+quotationDetails.getQty()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				
				testQuote ++;
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Total[Without Tax]</td>");
				PrintWriter.append("<td>"+PaymentPage_SubTotal+"</td>");
							
				if(Integer.toString(PaymentPage_SubTotal).equals(quotationDetails.getTotalRate())){
						
					PrintWriter.append("<td>"+quotationDetails.getTotalRate()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getTotalRate()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				testQuote ++;
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Total Currency</td>");
				PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
							
				if(quotationDetails.getCurrency().contains(search.getSellingCurrency())){
						
					PrintWriter.append("<td>"+quotationDetails.getCurrency()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getCurrency()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				testQuote ++;
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Currency 2</td>");
				PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
							
				if(quotationDetails.getCurrency2().contains(search.getSellingCurrency())){
						
					PrintWriter.append("<td>"+quotationDetails.getCurrency2()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getCurrency2()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				
				testQuote ++;
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Sub Total</td>");
				PrintWriter.append("<td>"+PaymentPage_SubTotal+"</td>");
							
				if(Integer.toString(PaymentPage_SubTotal).equals(quotationDetails.getSubTotal())){
						
					PrintWriter.append("<td>"+quotationDetails.getSubTotal()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getSubTotal()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}

				
				testQuote ++;
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Total Tax and other charges</td>");
				PrintWriter.append("<td>"+(PaymentPage_Taxes)+"</td>");
							
				if(Integer.toString(PaymentPage_Taxes).equals(quotationDetails.getTax())){
						
					PrintWriter.append("<td>"+quotationDetails.getTax()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getTax()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				testQuote ++;
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Activity Total Booking Value</td>");
				PrintWriter.append("<td>"+(PaymentPage_SubTotal + PaymentPage_Taxes)+"</td>");
							
				if(Integer.toString(PaymentPage_SubTotal + PaymentPage_Taxes).equals(quotationDetails.getTotalValue())){
						
					PrintWriter.append("<td>"+quotationDetails.getTotalValue()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getTotalValue()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}	
				
				testQuote ++;
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Amount payable when booking</td>");
				PrintWriter.append("<td>"+(PaymentPage_SubTotal + PaymentPage_Taxes)+"</td>");
							
				if(Integer.toString(PaymentPage_SubTotal + PaymentPage_Taxes).equals(quotationDetails.getAmountPayNow())){
						
					PrintWriter.append("<td>"+quotationDetails.getAmountPayNow()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getAmountPayNow()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testQuote ++;
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Amount payable at check-in</td>");
				PrintWriter.append("<td>"+PaymentPage_AmountCheckIn+"</td>");
							
				if(Integer.toString(PaymentPage_AmountCheckIn).equals(quotationDetails.getPayAtCheckIn())){
						
					PrintWriter.append("<td>"+quotationDetails.getPayAtCheckIn()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getPayAtCheckIn()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				testQuote ++;
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Sub Total</td>");
				PrintWriter.append("<td>"+PaymentPage_SubTotal+"</td>");
							
				if(Integer.toString(PaymentPage_SubTotal).equals(quotationDetails.getSubTotal2())){
						
					PrintWriter.append("<td>"+quotationDetails.getSubTotal2()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getSubTotal2()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}

				
				testQuote ++;
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Total Tax and other charges</td>");
				PrintWriter.append("<td>"+(PaymentPage_Taxes)+"</td>");
							
				if(Integer.toString(PaymentPage_Taxes).equals(quotationDetails.getTax2())){
						
					PrintWriter.append("<td>"+quotationDetails.getTax2()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getTax2()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				testQuote ++;
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Total Booking Value</td>");
				PrintWriter.append("<td>"+(PaymentPage_SubTotal + PaymentPage_Taxes)+"</td>");
							
				if(Integer.toString(PaymentPage_SubTotal + PaymentPage_Taxes).equals(quotationDetails.getTotalBookingValue2())){
						
					PrintWriter.append("<td>"+quotationDetails.getTotalBookingValue2()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getTotalBookingValue2()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}	
				
				testQuote ++;
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Amount payable when booking</td>");
				PrintWriter.append("<td>"+(PaymentPage_SubTotal + PaymentPage_Taxes)+"</td>");
							
				if(Integer.toString(PaymentPage_SubTotal + PaymentPage_Taxes).equals(quotationDetails.getAmountPayNow2())){
						
					PrintWriter.append("<td>"+quotationDetails.getAmountPayNow2()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getAmountPayNow2()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testQuote ++;
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Amount payable at check-in / Utilization</td>");
				PrintWriter.append("<td>"+PaymentPage_AmountCheckIn+"</td>");
							
				if(Integer.toString(PaymentPage_AmountCheckIn).equals(quotationDetails.getPayAtCheckIn2())){
						
					PrintWriter.append("<td>"+quotationDetails.getPayAtCheckIn2()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getPayAtCheckIn2()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				
				if (UserTypeDC_B2B == TourOperatorType.DC){
					
					testQuote ++;			
					PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - First Name</td>");
					PrintWriter.append("<td>"+activitydetails.getPaymentPage_FName()+"</td>");
								
					if(activitydetails.getPaymentPage_FName().equals(quotationDetails.getCus_FName())){
							
						PrintWriter.append("<td>"+quotationDetails.getCus_FName()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+quotationDetails.getCus_FName()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testQuote ++;			
					PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Last Name</td>");
					PrintWriter.append("<td>"+activitydetails.getPaymentPage_LName()+"</td>");
								
					if(activitydetails.getPaymentPage_LName().equals(quotationDetails.getCus_LName())){
							
						PrintWriter.append("<td>"+quotationDetails.getCus_LName()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+quotationDetails.getCus_LName()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
				}else{
					
					testQuote ++;			
					PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - First Name</td>");
					PrintWriter.append("<td>"+activitydetails.getPaymentPage_FName()+"</td>");
								
					if(activitydetails.getPaymentPage_FName().equals(quotationDetails.getCus_FName())){
							
						PrintWriter.append("<td>"+quotationDetails.getCus_FName()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+quotationDetails.getCus_FName()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testQuote ++;			
					PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Last Name</td>");
					PrintWriter.append("<td>"+activitydetails.getPaymentPage_LName()+"</td>");
								
					if(activitydetails.getPaymentPage_LName().equals(quotationDetails.getCus_LName())){
							
						PrintWriter.append("<td>"+quotationDetails.getCus_LName()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+quotationDetails.getCus_LName()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
				}
				
				
				
				testQuote ++;			
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - TP No</td>");
				PrintWriter.append("<td>"+activitydetails.getPaymentPage_TP()+"</td>");
							
				if(activitydetails.getPaymentPage_TP().equals(quotationDetails.getCusTP())){
						
					PrintWriter.append("<td>"+quotationDetails.getCusTP()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getCusTP()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				testQuote ++;			
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Emergency No</td>");
				PrintWriter.append("<td>"+activitydetails.getPaymentPage_TP()+"</td>");
							
				if(activitydetails.getPaymentPage_TP().equals(quotationDetails.getCusTP())){
						
					PrintWriter.append("<td>"+quotationDetails.getCusTP()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getCusTP()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				testQuote ++;			
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Email</td>");
				PrintWriter.append("<td>"+activitydetails.getPaymentPage_Email()+"</td>");
							
				if(quotationDetails.getCusEmail().contains(activitydetails.getPaymentPage_Email())){
						
					PrintWriter.append("<td>"+quotationDetails.getCusEmail()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getCusEmail()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testQuote ++;			
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Address</td>");
				PrintWriter.append("<td>"+activitydetails.getPaymentPage_address()+"</td>");
							
				if(activitydetails.getPaymentPage_address().equals(quotationDetails.getCusAddress())){
						
					PrintWriter.append("<td>"+quotationDetails.getCusAddress()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getCusAddress()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testQuote ++;			
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Address 2</td>");
				PrintWriter.append("<td>"+activitydetails.getPaymentPage_address_2()+"</td>");
							
				if(activitydetails.getPaymentPage_address_2().equals(quotationDetails.getCusAddress_2())){
						
					PrintWriter.append("<td>"+quotationDetails.getCusAddress_2()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getCusAddress_2()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testQuote ++;			
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Country</td>");
				PrintWriter.append("<td>"+activitydetails.getPaymentPage_country()+"</td>");
							
				if(activitydetails.getPaymentPage_country().equals(quotationDetails.getCuscountry())){
						
					PrintWriter.append("<td>"+quotationDetails.getCuscountry()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getCuscountry()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testQuote ++;			
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - City</td>");
				PrintWriter.append("<td>"+activitydetails.getPaymentPage_city()+"</td>");
							
				if(activitydetails.getPaymentPage_city().equals(quotationDetails.getCuscity())){
						
					PrintWriter.append("<td>"+quotationDetails.getCuscity()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getCuscity()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				
				
				testQuote ++;		
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Tel</td>");
				PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Tel")+"</td>");
							
				if(PG_Properties.getProperty("Portal.Tel").toLowerCase().equals(quotationDetails.getCompanyTel().toLowerCase())){
						
					PrintWriter.append("<td>"+quotationDetails.getCompanyTel()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getCompanyTel()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				

				testQuote ++;			
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Fax</td>");
				PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Fax")+"</td>");
							
				if(PG_Properties.getProperty("Portal.Fax").toLowerCase().equals(quotationDetails.getCompanyFax().toLowerCase())){
						
					PrintWriter.append("<td>"+quotationDetails.getCompanyFax()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getCompanyFax()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
												
				testQuote ++;			
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Company Name</td>");
				PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Name")+"</td>");
							
				if(PG_Properties.getProperty("Portal.Name").toLowerCase().equals(quotationDetails.getCompanyName().toLowerCase())){
						
					PrintWriter.append("<td>"+quotationDetails.getCompanyName()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getCompanyName()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testQuote ++;			
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Email</td>");
				PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Email")+"</td>");
							
				if(PG_Properties.getProperty("Portal.Email").toLowerCase().equals(quotationDetails.getCompanyEmail().toLowerCase())){
						
					PrintWriter.append("<td>"+quotationDetails.getCompanyEmail()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getCompanyEmail()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testQuote ++;			
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Quotation Email - Website</td>");
				PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Website")+"</td>");
							
				if(PG_Properties.getProperty("Portal.Website").toLowerCase().equals(quotationDetails.getCompanyWeb().toLowerCase())){
						
					PrintWriter.append("<td>"+quotationDetails.getCompanyWeb()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quotationDetails.getCompanyWeb()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
			}else{
				
				PrintWriter.append("<tr><td>"+testQuote+"</td> <td>Email - Quotation Mail</td>");
				PrintWriter.append("<td>Quotation Mail should be available</td>");
				PrintWriter.append("<td>Not Available</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");		
				
			}
			
			PrintWriter.append("</table>");
			
			
			
		}
				
	}
	
	
	
	public String getDateSuffix(String day) { 
		
        String dateChange = (day.substring(0, 2) + day.substring(5, 13)).replaceAll(" ", "");
		
        return dateChange;
	}
	
	public String getDayNumberSuffix(String date) {
		
		StringBuffer stringBuffer = new StringBuffer(date);		
		int day = Integer.parseInt(date.substring(0, 2));
				
	    if (day >= 11 && day <= 13) {
	        return stringBuffer.insert(2, "th").toString();
	    }
	    switch (day % 10) {
	    case 1:
	        return stringBuffer.insert(2, "st").toString();
	    case 2:
	        return stringBuffer.insert(2, "nd").toString();
	    case 3:
	        return stringBuffer.insert(2, "rd").toString();
	    default:
	        return stringBuffer.insert(2, "th ").toString();
	    }
	
	}
}
