package com.reader;

import java.text.DecimalFormat;

import org.openqa.selenium.WebDriver;

import com.controller.PG_Properties;
import com.model.ActivityDetails;
import com.model.ActivityInventoryRecords;
import com.model.Search;
import com.types.TourOperatorType;

public class TORatesLogics {
	
	private WebDriver driver = null;
	private ActivityDetails activityDetails;
	private Search search_Info;
	private ActivityInventoryRecords inventoryList;
	private int paxCnt, discountedVale;
	private double sellRate;
	private double netRate;
	
	
	public TORatesLogics(Search search, ActivityInventoryRecords inventoryList){
		this.search_Info = search;
		this.inventoryList = inventoryList;
		
	}
	
	public ActivityDetails getTORatesCalculation(){
		
		activityDetails = new ActivityDetails();
		
		if (! (inventoryList.getUserTypeDC_B2B() == TourOperatorType.DC)) {
			
			
			double totalNetRate_SalesTax = 0;
			double totalNetRate_MisFees = 0;
			
			paxCnt = Integer.parseInt(search_Info.getAdults()) + Integer.parseInt(search_Info.getChildren());		
			double pm = Double.parseDouble(inventoryList.getToActivityPM());
			netRate = Double.parseDouble(inventoryList.getToActivityNetRate());
			
			sellRate = (netRate* ((100 + pm) / 100)) * paxCnt;	
			double totalNetRate = (netRate * paxCnt);
					
			if (inventoryList.getToActivity_SalesTaxType().equalsIgnoreCase("percentage")) {
				
				double salesTax = Double.parseDouble(inventoryList.getToActivity_SalesTaxValue());
				totalNetRate_SalesTax = (totalNetRate* ((salesTax) / 100));				
			}
			
			if (inventoryList.getToActivity_SalesTaxType().equalsIgnoreCase("value")) {			
				double salesTax = Double.parseDouble(inventoryList.getToActivity_SalesTaxValue());
				totalNetRate_SalesTax = totalNetRate + salesTax;
			}
			
			if (inventoryList.getToActivity_MisFeeType().equalsIgnoreCase("percentage")) {
				
				double misFees = Double.parseDouble(inventoryList.getToActivity_MisFeeValue());
				totalNetRate_MisFees = (totalNetRate* ((misFees) / 100));				
			}
			
			if (inventoryList.getToActivity_MisFeeValue().equalsIgnoreCase("value")) {			
				double misFees = Double.parseDouble(inventoryList.getToActivity_MisFeeValue());
				totalNetRate_MisFees = totalNetRate + misFees;
			}
			
			int subTotal = (int) Math.ceil(sellRate);
			int totalTax = (int) Math.ceil(totalNetRate_SalesTax + totalNetRate_MisFees);
			
			
			if (inventoryList.getUserTypeDC_B2B()== TourOperatorType.COMCASH || inventoryList.getUserTypeDC_B2B()== TourOperatorType.COMCREDITLPONO || inventoryList.getUserTypeDC_B2B()== TourOperatorType.COMCREDITLPOY ) {
				
				int agentCommssion = 0;
				String toAgentTypePerOrValue = PG_Properties.getProperty(inventoryList.getUserTypeDC_B2B() + "#Type");
				
				if (toAgentTypePerOrValue.equalsIgnoreCase("percentage")) {
					
					double agentPercentage = Double.parseDouble(PG_Properties.getProperty(inventoryList.getUserTypeDC_B2B() + "#Value"));
					agentCommssion = (int)(sellRate* ((agentPercentage) / 100));	

				} else {
					
					double agentCommValue = Double.parseDouble(PG_Properties.getProperty(inventoryList.getUserTypeDC_B2B() + "#Value"));
					agentCommssion = (int) (sellRate + agentCommValue) ;	

				}
					

				activityDetails.setToAgentCommission(Integer.toString((int)Math.ceil(agentCommssion)));
				
			}
			
			int disValue = 0;
			int TotalValue = 0;
			
			if (inventoryList.getDiscountYesORNo().equalsIgnoreCase("Yes")) {
				
				if (PG_Properties.getProperty("Discount_Type").equalsIgnoreCase("percentage")) {
					
					disValue = Integer.parseInt(PG_Properties.getProperty("Discount_Value"));
					discountedVale = ((subTotal * disValue)/100);							
				}
				
				if (PG_Properties.getProperty("Discount_Type").equalsIgnoreCase("value")) {			
					
					disValue = Integer.parseInt(PG_Properties.getProperty("Discount_Value"));
					discountedVale = subTotal + disValue;
				}
				
				TotalValue = (int) Math.ceil(subTotal + totalTax - discountedVale);
				
				
			}else{
				
				TotalValue = (int) Math.ceil(subTotal + totalTax);
			}
			
			
													
			activityDetails.setToActivity_SubTotal(Double.toString(subTotal));
			activityDetails.setToActivity_TotalTax(Double.toString(totalTax));
			activityDetails.setToActivity_TotalValue(Double.toString(TotalValue));
				
		}
					
		return activityDetails;
		
	}
	

}
