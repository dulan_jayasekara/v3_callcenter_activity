package com.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.model.ActivityInventoryRecords;
import com.model.PaymentDetails;
import com.model.Search;

public class LoadDetails {
	
	Map<Integer, String> SearchMap 		= null;
	Map<Integer, String> CustomerMap 	= null;
	Map<Integer, String> InventoryTypeMap 	= null;

	public LoadDetails(ArrayList<Map<Integer, String>> sheetlist){
		
		SearchMap          		= sheetlist.get(0);
		CustomerMap          	= sheetlist.get(1);
		InventoryTypeMap		= sheetlist.get(2);
	}
	
	public ArrayList<Search> loadActivityReservation(){
		
		Iterator<Map.Entry<Integer, String>> it = SearchMap.entrySet().iterator();
		ArrayList<Search> SearchList = new ArrayList<Search>();
		
		while(it.hasNext()) {
			Search search = new Search();
			String[] values = it.next().getValue().split(",");
			
			search.setScenarioCount(values[0]);
			search.setSellingCurrency(values[1]);
			search.setCountry(values[2]);
			search.setDestination(values[3]);
			search.setDateFrom(values[4]);
			search.setDateTo(values[5]);
			search.setAdults(values[6]);
			search.setChildren(values[7]);
			search.setAgeOfChildren(values[8]);
			search.setProgramCategory(values[9]);
			search.setPreferCurrency(values[10]);
			search.setPromotionCode(values[11]);
			search.setActivityName(values[12]);
			search.setActivityDate(values[13]);
			search.setScenariotype(values[14]);
			search.setBooking_channel(values[15]);
			search.setQuotaionReq(values[16]);
			
			
			SearchList.add(search);
		}
		
		return SearchList;
	}	
	
	public ArrayList<PaymentDetails> loadPaymentDetails() {

		Iterator<Map.Entry<Integer, String>> it = CustomerMap.entrySet().iterator();
		ArrayList<PaymentDetails> paymentDetailsList = new ArrayList<PaymentDetails>();

		while (it.hasNext()) {
			PaymentDetails paymentDetails = new PaymentDetails();
			String[] values = it.next().getValue().split(",");

			paymentDetails.setCustomerTitle(values[0]);
			paymentDetails.setCustomerName(values[1]);
			paymentDetails.setCustomerLastName(values[2]);
			paymentDetails.setTel(values[3]);
			paymentDetails.setEmail(values[4]);
			paymentDetails.setAddress(values[5]);
			paymentDetails.setAddress_1(values[6]);
			paymentDetails.setCountry(values[7]);
			paymentDetails.setCity(values[8]);
			paymentDetails.setState(values[9]);
			
			
			paymentDetailsList.add(paymentDetails);

		}

		return paymentDetailsList;
	}
	
	
	public ArrayList<ActivityInventoryRecords> loadActivityInventoryRecords() {
		
		Iterator<Map.Entry<Integer, String>> it = InventoryTypeMap.entrySet().iterator();
		ArrayList<ActivityInventoryRecords> activityInventoryList = new ArrayList<ActivityInventoryRecords>();
		
		while (it.hasNext()) {
			
			ActivityInventoryRecords activityInventory = new ActivityInventoryRecords();
			String[] values = it.next().getValue().split(",");
			
			activityInventory.setScenarioCount(values[0]);
			activityInventory.setActivityName(values[1]);
			activityInventory.setSupplierCurrency(values[2]);
			activityInventory.setResultsPage_SubTotal(values[3]);	
			activityInventory.setResultsPage_Taxes(values[4]);	
			activityInventory.setResultsPage_TotalPayable(values[5]);	
			
			activityInventory.setPaymentPage_SubTotal(values[6]);	
			activityInventory.setPaymentPage_Taxes(values[7]);	
			activityInventory.setPaymentPage_TotalPayable(values[8]);	
			activityInventory.setPaymentPage_TotalGrossBookingValue(values[9]);	
			activityInventory.setPaymentPage_TotalTax(values[10]);
			activityInventory.setPaymentPage_TotalPackageValue(values[11]);	
			activityInventory.setPaymentPage_AmountProcessed(values[12]);
			activityInventory.setPaymentPage_AmountCheckIn(values[13]);
			
			activityInventory.setConfirmationPage_SubTotal(values[14]);	
			activityInventory.setConfirmationPage_Taxes(values[15]);
			activityInventory.setConfirmationPage_TotalPayable(values[16]);	
			activityInventory.setConfirmation_TotalGrossBookingValue(values[17]);	
			activityInventory.setConfirmationPage_TotalTax(values[18]);	
			activityInventory.setConfirmationPage_TotalPackageValue(values[19]);	
			activityInventory.setConfirmationPage_AmountProcessed(values[20]);
			activityInventory.setConfirmationPage_AmountCheckIn(values[21]);
			
			activityInventory.setStandardCancellation_based(values[22]);
			activityInventory.setStandardCancellation_value(values[23]);
			activityInventory.setNoshow_based(values[24]);
			activityInventory.setNoshow_value(values[25]);
			activityInventory.setCancellation_Apply(values[26]);
			activityInventory.setCancellation_Buffer(values[27]);
			activityInventory.setInventoryType(values[28]);
			activityInventory.setSupplier_Name(values[29]);
			activityInventory.setSupplier_Add(values[30]);
			activityInventory.setSupplier_TP(values[31]);
			activityInventory.setProfitmarkup(values[32]);
			activityInventory.setPaymentDetails(values[33]);
			activityInventory.setCreditCard_Fee(values[34]);
			activityInventory.setInventoryCount(values[35]);
			activityInventory.setActivityDescription(values[36]);
			
			activityInventory.setUserTypeDC_B2B(values[37]);
			activityInventory.setToActivityNetRate(values[38]);
			activityInventory.setToActivityPM(values[39]);
			activityInventory.setToActivity_SalesTaxType(values[40]);
			activityInventory.setToActivity_SalesTaxValue(values[41]);
			activityInventory.setToActivity_MisFeeType(values[42]);
			activityInventory.setToActivity_MisFeeValue(values[43]);
			
			activityInventory.setActiveManagerOverwrite(values[44]);
			activityInventory.setDiscountYesORNo(values[45]);
			
			activityInventoryList.add(activityInventory);
		}

		return activityInventoryList;
	}
	
	public static Map<Integer,ActivityInventoryRecords> convertIntoMap(java.util.List<ActivityInventoryRecords> inventoryList){
		
		Map<Integer,ActivityInventoryRecords> inventoryListMap=new HashMap<Integer, ActivityInventoryRecords>();
		
		for(ActivityInventoryRecords r:inventoryList){			
			inventoryListMap.put(Integer.parseInt(r.getScenarioCount()),r);
		}
		
		return inventoryListMap;
		
	}
	
	
}
